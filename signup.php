<!DOCTYPE html>
<html lang="en">
<head>
    <title>NetPro</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Logo-->
    <link rel="shortcut icon" href="images/logoIcon.png">
    <!--Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400italic,500,500italic,700,700italic,900,900italic,300italic,300" rel="stylesheet" type="text/css">    
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100" rel="stylesheet" type="text/css">
    <!--Font Awesome-->
    <link href="includes/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>       
    <!--Full slider -->
    <link href="includes/css/jquery.maximage.css" rel="stylesheet" type="text/css"/>
    <link href="includes/css/jquery.screensize.css" rel="stylesheet" type="text/css"/>
    <!--Animate Css -->
    <link href="includes/css/animate.css" rel="stylesheet" type="text/css"/>
    <!--Bootstrap-->
    <link href="includes/css/bootstrap.min.flatly.css" rel="stylesheet" type="text/css"/>
    <!--Custom CSS -->
    <link href="includes/css/stylesV2.css" rel="stylesheet" type="text/css"/>
    <!--Custom Css-->
    <link href="includes/css/homeV2.css" rel="stylesheet">
    <!--Media Queries -->
    <link href="includes/css/media-queries.css" rel="stylesheet" type="text/css"/>  
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">

    <style type="text/css">
        input[type="text"],input[type="email"]{
            font-size: 20px;
        }    

        #signUpBtn,.panel,.well{
          -webkit-box-shadow: 4px 1px 17px 0px rgba(0,0,0,0.39);
          -moz-box-shadow: 4px 1px 17px 0px rgba(0,0,0,0.39);
          box-shadow: 4px 1px 17px 0px rgba(0,0,0,0.39);
        }

        .address { font-size: 20px; }
        
        h1 { 
            font-family: "Open Sans";  font-style: normal; font-variant: normal; font-weight: 700;
        }
        h2 { 
            font-family: "Open Sans";  font-style: normal; font-variant: normal; font-weight: 700; 
        } 
        h3 { 
            font-family: "Open Sans";  font-style: normal; font-variant: normal; font-weight: 700; 
        } 
        h4 { 
            font-family: "Open Sans";  font-style: normal; font-variant: normal; font-weight: 700; 
        } 
        p { 
            font-family: "Open Sans";  font-style: normal; font-variant: normal; font-weight: 400;
        } 
        blockquote { 
            font-family: "Open Sans"; font-style: normal; font-variant: normal; font-weight: 400; 
        } 
        pre { 
            font-family: "Open Sans"; font-size: 13px; font-style: normal; font-variant: normal; font-weight: 400;  
        }

        p, span, h1,h2,h3,h4,h5{
            color:#5F6160;
        }
        .loginBtn{
            height: 30px !important;
            color: #fff !important;
            padding-top: 3px !important;
            margin-top: 3px !important;
        }
        .selectdiv:after {
            /*content: '\f078';*/
            content: url('images/backgrounds/dropdown.png');
            font: normal normal normal 17px/1 FontAwesome;
            color: #0ebeff;
            right: 25px;
            top: 2px;
            height: 34px;
            padding: 10px 0px 0px 8px;
            position: absolute;
            pointer-events: none;
        }

        /* IE11 hide native button (thanks Matt!) */
        select::-ms-expand {
        display: none;
        }

        .selectdiv select {
          -webkit-appearance: none;
          -moz-appearance: none;
          appearance: none;
          /* Add some styling */
          
          display: block;
          width: 100%;
          float: right;
          padding: 0px 12px;
          font-size: 20px;
          color: #333;
          background-color: #ffffff;
          background-image: none;
          -ms-word-break: normal;
          word-break: normal;
        }
        @media (min-width: 992px){
            .modal-lg {
                width: 1200px;
            }
        }

        .white-text{
            color:#fff;
        }

        .radio {
          padding-left: 20px; }
          .radio label {
            display: inline-block;
            position: relative;
            padding-left: 5px; }
            .radio label::before {
              content: "";
              display: inline-block;
              position: absolute;
              width: 27px;
              height: 27px;
              left: 0;
              margin-left: -20px;
              border: 1px solid #cccccc;
              border-radius: 50%;
              background-color: #fff;
              -webkit-transition: border 0.15s ease-in-out;
              -o-transition: border 0.15s ease-in-out;
              transition: border 0.15s ease-in-out; }
            .radio label::after {
              display: inline-block;
              position: absolute;
              content: " ";
              width: 17px;
              height: 17px;
              left: 5px;
              top: 5px;
              margin-left: -20px;
              border-radius: 50%;
              background-color: #555555;
              -webkit-transform: scale(0, 0);
              -ms-transform: scale(0, 0);
              -o-transform: scale(0, 0);
              transform: scale(0, 0);
              -webkit-transition: -webkit-transform 0.1s cubic-bezier(0.8, -0.33, 0.2, 1.33);
              -moz-transition: -moz-transform 0.1s cubic-bezier(0.8, -0.33, 0.2, 1.33);
              -o-transition: -o-transform 0.1s cubic-bezier(0.8, -0.33, 0.2, 1.33);
              transition: transform 0.1s cubic-bezier(0.8, -0.33, 0.2, 1.33); }
          .radio input[type="radio"] {
            opacity: 0; }
            .radio input[type="radio"]:focus + label::before {
              outline: thin dotted;
              outline: 5px auto -webkit-focus-ring-color;
              outline-offset: -2px; }
            .radio input[type="radio"]:checked + label::after {
              -webkit-transform: scale(1, 1);
              -ms-transform: scale(1, 1);
              -o-transform: scale(1, 1);
              transform: scale(1, 1); }
            .radio input[type="radio"]:disabled + label {
              opacity: 0.65; }
              .radio input[type="radio"]:disabled + label::before {
                cursor: not-allowed; }
          .radio.radio-inline {
            margin-top: 0; }

        .radio-primary input[type="radio"] + label::after {
          background-color: #428bca; }
        .radio-primary input[type="radio"]:checked + label::before {
          border-color: #428bca; }
        .radio-primary input[type="radio"]:checked + label::after {
          background-color: #428bca; }

        .radio-danger input[type="radio"] + label::after {
          background-color: #d9534f; }
        .radio-danger input[type="radio"]:checked + label::before {
          border-color: #d9534f; }
        .radio-danger input[type="radio"]:checked + label::after {
          background-color: #d9534f; }

        .radio-info input[type="radio"] + label::after {
          background-color: #5bc0de; }
        .radio-info input[type="radio"]:checked + label::before {
          border-color: #5bc0de; }
        .radio-info input[type="radio"]:checked + label::after {
          background-color: #5bc0de; }

        .radio-warning input[type="radio"] + label::after {
          background-color: #f0ad4e; }
        .radio-warning input[type="radio"]:checked + label::before {
          border-color: #f0ad4e; }
        .radio-warning input[type="radio"]:checked + label::after {
          background-color: #f0ad4e; }

        .radio-success input[type="radio"] + label::after {
          background-color: #5cb85c; }
        .radio-success input[type="radio"]:checked + label::before {
          border-color: #5cb85c; }
        .radio-success input[type="radio"]:checked + label::after {
          background-color: #5cb85c; }

          .no-border{
            border:0;
          }

        .form-horizontal .control-label {
            text-align: left;
        }

        .navbar {
            min-height: 90px;
            padding-top:20px;
        }
    </style>
 
</head>

<body style="background-image:url('images/backgrounds/green_bg.png');background-size: cover;">
    <div class="main-wrapper">
        <!-- Banner -->
        <section id="banner" style="min-height:60px;height: 60px;">

            <!-- Navbar-->
            <nav class="navbar navbar-inverse" id="navbar">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-navguest" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/">
                            <img src="images/netpro_logo.png" alt="Telstra Blog">
                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="collapse-navguest">           
                        <ul class="nav navbar-nav">
                        </ul>

                        <!-- <ul class="nav navbar-nav navbar-right">
                            <div class="block" style="margin-top:10px;">
                                <label><span style="color:#000 !important;font-weight: normal;padding-right:20px;position:relative;top:5px">Already have an account?</span></label>
                                <a data-toggle = 'modal' data-target = '#LoginModal' class="btn btn-success  loginBtn">Login</a>
                            </div>
                        </ul> -->
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>                
        </section>

        <div id='page-content'>
            <section id='initial'  style="background-image:url('images/backgrounds/green_bg.png');background-size: cover;zoom:85%;">
                <div class='container' style="width:790px;">
                    <h2 class="text-center white-text" style="font-size:1.86vw;padding-top:35px;">Complete your details to get NetPro activated</h2><br/>
                    <p class="white-text" style="color:#fff !important;font-size: 21px;text-align: justify;font-weight: bolder;">
                      Once set-up is completed, you'll receive an invoice for your NetPro monthly subscription, and you'll need to set up payment via Direct Debit.<br/><br/>In future, should you no longer want to use NetPro, get in touch and we'll cancel NetPro at the end of your current billing cycle.
                    </p>
                    <br/>
                    <div class='row' style="padding-top:20px;">
                    <div class="content col-xs-12 col-md-12 col-lg-12" id="app">
                        <form @submit.prevent="submit()" class="form-horizontal" role="form" id="form-signup" action="#">
                            <div class='well' style="border-radius: 15px;min-height: 130px;height: 130px;">                                
                                <div class="desc">

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Director Name:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" name="directorName" v-model="details.directorName" required="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 class="col-sm-4 control-label" style="text-align:left !important"><strong>Number of Stores:</strong></h4>
                                        <div class="col-sm-8">
                                            <div class="radio radio-info radio-inline pull-left">
                                                <input type="radio" id="inlineRadio1" v-model="storeCount" value="1" name="radioInline">
                                                <label for="inlineRadio1" style="padding-left:10px;font-size:22px;"> 1 </label>
                                            </div>
                                            <div class="radio radio-info radio-inline pull-left">
                                                <input type="radio" id="inlineRadio2" v-model="storeCount" value="2" name="radioInline">
                                                <label for="inlineRadio2" style="padding-left:10px;font-size:22px;"> 2 </label>
                                            </div>
                                            <div class="radio radio-info radio-inline pull-left">
                                                <input type="radio" id="inlineRadio3" v-model="storeCount" value="3" name="radioInline">
                                                <label for="inlineRadio3" style="padding-left:10px;font-size:22px;"> 3 </label>
                                            </div>
                                            <div class="radio radio-info radio-inline pull-left">
                                                <input type="radio" id="inlineRadio4" v-model="storeCount" value="4" name="radioInline">
                                                <label for="inlineRadio4" style="padding-left:10px;font-size:22px;"> 4 </label>
                                            </div>
                                            <div class="radio radio-info radio-inline pull-left">
                                                <input type="radio" id="inlineRadio5" v-model="storeCount" value="5" name="radioInline">
                                                <label for="inlineRadio5" style="padding-left:10px;font-size:22px;"> 5 </label>
                                            </div>
                                        </div>
                                  </div>
                                </div>
                            </div>

                            <div class="panel panel-default" style="border-radius: 15px;border:0;margin-top:35px;">
                                <div class="panel-heading no-border" style="background: #0888AF;border-top-right-radius:15px;border-top-left-radius:15px;height:50px;"><h3 class="white-text">Store 1 Details</h3></div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Store Name:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" v-model="details.store1.name" name="store[0][name]" required="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Store Type:</strong></h4>
                                        <div class="col-sm-8 selectdiv ">
                                            <select class="form-control" v-model="details.store1.type" name="store[0][type]" required="">
                                                <option selected="" value="">Please select</option>
                                                <option value="tls">TLS</option>
                                                <option value="tbc">TBC</option>
                                                <option value="others">Telstra Dealer</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Dealer Code:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" v-model="details.store1.dealer" name="store[0][dealerCode]" required="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Fixed Line Code:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" v-model="details.store1.line" name="store[0][lineCode]" required="">
                                        </div>
                                    </div>

                                    <hr style="height:2px;border:none;color:#E0E0E0;background-color:#E0E0E0;" />

                                    <h3 style="font-weight: 900">Billing Details</h3>
                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Company Name:</strong><br/><span style="font-size:14px;color:#0888AF;">(This will be the name on invoice)</span></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" v-model="details.store1.companyName" name="store[0][companyName]" required="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Company Address:</strong></h4>
                                        <div class="col-sm-8">
                                          <textarea rows="3" class="form-control address" v-model="details.store1.companyAddress" name="store[0][companyAddress]" required=""></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Contact Person Name:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" v-model="details.store1.contactName" name="store[0][contactName]" required="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Contact Person Email:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="email" class="form-control" id="inputEmail3" v-model="details.store1.contactNumber" name="store[0][contactEmail]" required="">
                                        </div>
                                    </div>
                              </div>
                            </div>

                            <div class="panel panel-default" style="border-radius: 15px;border:0;margin-top:35px;" v-if='storeCount > 1'>
                                <div class="panel-heading no-border" style="background: #038472;border-top-right-radius:15px;border-top-left-radius:15px;height:50px;"><h3 class="white-text">Store 2 Details</h3></div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Store Name:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" name="store[1][name]">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Store Type:</strong></h4>
                                        <div class="col-sm-8 selectdiv ">
                                            <select class="form-control" name="store[1][type]">
                                                <option>Please Select</option><option value="tls">TLS</option>
                                                <option value="tbc">TBC</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Dealer Code:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" name="store[1][dealerCode]">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Fixed Line Code:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" name="store[1][lineCode]">
                                        </div>
                                    </div>

                                    <hr style="height:2px;border:none;color:#E0E0E0;background-color:#E0E0E0;" />
                                    
                                    <h3 style="font-weight: 900">Billing Details</h3>
                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Company Name:</strong><br/><span style="font-size:14px;color:#0888AF;">(This will be the name on invoice)</span></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" name="store[1][companyName]">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Company Address:</strong></h4>
                                        <div class="col-sm-8">
                                          <textarea rows="3" class="form-control address" name="store[1][companyAddress]"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Contact Person Name:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" name="store[1][contactName]">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Contact Person Email:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="email" class="form-control" id="inputEmail3" name="store[1][contactEmail]">
                                        </div>
                                    </div>
                              </div>
                            </div>

                            <div class="panel panel-default" style="border-radius: 15px;border:0;margin-top:35px;" v-if='storeCount > 2'>
                                <div class="panel-heading no-border" style="background: #0888AF;border-top-right-radius:15px;border-top-left-radius:15px;height:50px;"><h3 class="white-text">Store 3 Details</h3></div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Store Name:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" name="store[2][name]">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Store Type:</strong></h4>
                                        <div class="col-sm-8 selectdiv ">
                                            <select class="form-control" name="store[2][type]">
                                                <option>Please Select</option>
                                                <option value="tls">TLS</option>
                                                <option value="tbc">TBC</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Dealer Code:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" name="store[2][dealerCode]">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Fixed Line Code:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" name="store[2][lineCode]">
                                        </div>
                                    </div>

                                    <hr style="height:2px;border:none;color:#E0E0E0;background-color:#E0E0E0;" />
                                    
                                    <h3 style="font-weight: 900">Billing Details</h3>
                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Company Name:</strong><br/><span style="font-size:14px;color:#0888AF;">(This will be the name on invoice)</span></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" name="store[2][companyName]">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Company Address:</strong></h4>
                                        <div class="col-sm-8">
                                          <textarea rows="3" class="form-control address" name="store[2][companyAddress]"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Contact Person Name:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" name="store[2][contactName]">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Contact Person Email:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="email" class="form-control" id="inputEmail3" name="store[2][contactEmail]">
                                        </div>
                                    </div>
                              </div>
                            </div>

                            <div class="panel panel-default" style="border-radius: 15px;border:0;margin-top:35px;" v-if='storeCount > 3'>
                                <div class="panel-heading no-border" style="background: #038472;border-top-right-radius:15px;border-top-left-radius:15px;height:50px;"><h3 class="white-text">Store 4 Details</h3></div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Store Name:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" name="store[3][name]">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Store Type:</strong></h4>
                                        <div class="col-sm-8 selectdiv ">
                                            <select class="form-control" name="store[3][type]">
                                                <option>Please Select</option>
                                                <option value="tls">TLS</option>
                                                <option value="tbc">TBC</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Dealer Code:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" name="store[3][dealerCode]">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Fixed Line Code:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" name="store[3][lineCode]">
                                        </div>
                                    </div>

                                    <hr style="height:2px;border:none;color:#E0E0E0;background-color:#E0E0E0;" />
                                    
                                    <h3 style="font-weight: 900">Billing Details</h3>
                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Company Name:</strong><br/><span style="font-size:14px;color:#0888AF;">(This will be the name on invoice)</span></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" name="store[3][companyName]">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Company Address:</strong></h4>
                                        <div class="col-sm-8">
                                          <textarea rows="3" class="form-control address" name="store[3][companyAddress]"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Contact Person Name:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" name="store[3][contactName]">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Contact Person Email:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="email" class="form-control" id="inputEmail3" name="store[3][contactEmail]">
                                        </div>
                                    </div>
                              </div>
                            </div>

                            <div class="panel panel-default" style="border-radius: 15px;border:0;margin-top:35px;" v-if='storeCount > 4'>
                                <div class="panel-heading no-border" style="background: #0888AF;border-top-right-radius:15px;border-top-left-radius:15px;height:50px;"><h3 class="white-text">Store 5 Details</h3></div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Store Name:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" name="store[4][name]">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Store Type:</strong></h4>
                                        <div class="col-sm-8 selectdiv ">
                                            <select class="form-control" name="store[4][type]">
                                                <option>Please Select</option>
                                                <option value="tls">TLS</option>
                                                <option value="tbc">TBC</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Dealer Code:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" name="store[4][dealerCode]">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Fixed Line Code:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" name="store[4][lineCode]">
                                        </div>
                                    </div>

                                    <hr style="height:2px;border:none;color:#E0E0E0;background-color:#E0E0E0;" />
                                    
                                    <h3 style="font-weight: 900">Billing Details</h3>
                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Company Name:</strong><br/><span style="font-size:14px;color:#0888AF;">(This will be the name on invoice)</span></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" name="store[4][companyName]">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Company Address:</strong></h4>
                                        <div class="col-sm-8">
                                          <textarea rows="3" class="form-control address" name="store[4][companyAddress]"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Contact Person Name:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" id="inputEmail3" name="store[4][contactName]">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h4 for="inputEmail3 pull-left" class="col-sm-4 control-label"><strong>Contact Person Email:</strong></h4>
                                        <div class="col-sm-8">
                                          <input type="email" class="form-control" id="inputEmail3" name="store[4][contactEmail]">
                                        </div>
                                    </div>
                              </div>
                            </div>

                            <input id="signUpBtn" type="submit" value="Submit my Details to get NetPro activated" class="btn btn-success" style="color:#62611E;width: 100%;font-size:37px;border-radius: 10px;background-color:#DDDB84;border-color: #EFEEB5;margin-top:15px">
                        </form>
                    </div>
                </div>
                <div class="container text-center col-lg-12" style="padding-top:0px;margin-top:60px;">                       
                    <h4 class="pull-left white-text footer-text">© Azenko Pty Ltd <?php echo date('Y');?></h4><h4 class="pull-right white-text footer-text" style="font-weight: normal;"> <a style="color:#fff" href="#" data-toggle="modal" data-target="#privacy">Privacy Policy</a> | Terms & Conditions</h4>                 
                </div>
            </section>

            <div id="privacy" class="modal fade" role="dialog">
              <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">

                  <div class="modal-body" style="height: 700px;overflow-y: scroll;">
                    <h3>Privacy Policy</h3>
                    <ul class="a">
                        <li>Azenko Pty Ltd A.C.N. 626 107 739 (Azenko) provides a wide range of IT products and services for small businesses specialising in a live reporting tools covering sales, stock and financials directly from a customer’s point of sale (POS) system, or that display a net promoter score.<br/>
                        By using, applying or signing up for any products or services with Azenko, including through mobile devices, you accept the terms of this Privacy Policy and consent to our collection, use, disclosure and retention of your information as described herein (including to contact you) and for all other purposes permitted under applicable personal information privacy statutes, credit bureau reporting rules, anti-spam legislation and consumer protection laws.</li>
                        <li>
                           <strong>1.  Introduction</span></strong>
                        </li>
                        <li>
                            <span class="listNumber">1.1</span>  We are committed to safeguarding the privacy of our website visitors and service users.
                        </li>
                        <li>
                            <span class="listNumber">1.2</span>  This policy applies where we are acting as a data controller with respect to the personal data of our website visitors and service users; in other words, where we determine the purposes and means of the processing of that personal data.
                        </li>
                        <li>
                            <span class="listNumber">1.3</span>  We use cookies on our website. Insofar as those cookies are not strictly necessary for the provision of our website and services, we will ask you to consent to our use of cookies when you first visit our website.
                        </li>
                        <li>
                            <span class="listNumber">1.4</span>  Our website incorporates privacy controls which affect how we will process your personal data. By using the privacy controls, you can specify whether you would like to receive direct marketing communications and limit the publication of your information. You can access the privacy controls via azenko.com.au.
                        </li>
                        <li>
                            <span class="listNumber">1.5</span>  In this policy, "we", "us" and "our" refer to Azenko Pty Ltd A.C.N. 626 107 739.
                        </li>
                        <li><strong>2.  How we collect and use your personal data</strong></li>
                        <li>
                            <span class="listNumber">2.1</span>  In this Section we have set out:
                            <ul>
                                <li>(a) the general categories of personal data that we may process;</li>
                                <li>(b) in the case of personal data that we did not obtain directly from you, the source and specific categories of that data;</li>
                                <li>(c) the purposes for which we may process personal data; and</li>
                                <li>(d) the legal bases of the processing.</li>
                            </ul>
                        </li>
                        <li><span class="listNumber">2.2</span>  We may process data about your use of our website and services ("usage data"). The usage data may include your IP address, geographical location, browser type and version, operating system, referral source, length of visit, page views and website navigation paths, as well as information about the timing, frequency and pattern of your service use. The source of the usage data is our analytics tracking system. This usage data may be processed for the purposes of analysing the use of the website and services. The legal basis for this processing is our legitimate interests, namely monitoring and improving our website and services.</li>
                        <li><span class="listNumber">2.3</span>  We may process your account data ("account data"). The account data may include your name and email address. The source of the account data is you. The account data may be processed for the purposes of operating our website, providing our services, ensuring the security of our website and services, maintaining back-ups of our databases and communicating with you. The legal basis for this processing is our legitimate interests, namely the proper administration of our website and business and the performance of a contract between you and us and/or taking steps, at your request, to enter into such a contract.</li>
                        <li><span class="listNumber">2.4</span>  We may process your information included in your personal profile on our website ("profile data"). The profile data may include your name, address, telephone number, email address, profile pictures, gender, date of birth, relationship status, interests and hobbies, educational details and employment details. The profile data may be processed for the purposes of enabling and monitoring your use of our website and services. The legal basis for this processing is our legitimate interests, namely the proper administration of our website and business and the performance of a contract between you and us and/or taking steps, at you request, to enter into such a contract.</li>
                        <li><span class="listNumber">2.5</span>  We may process information that you post for publication on our website or through our services ("publication data"). The publication data may be processed for the purposes of enabling such publication and administering our website and services. The legal basis for this processing is our legitimate interests, namely the proper administration of our website and business and the performance of a contract between you and us and/or taking steps, at your request, to enter into such a contract.</li>
                        <li><span class="listNumber">2.6</span>  We may process information contained in any enquiry you submit to us regarding goods and/or services ("enquiry data"). The enquiry data may be processed for the purposes of offering, marketing and selling relevant goods and/or services to you. The legal basis for this processing is consent.</li>
                        <li><span class="listNumber">2.7</span>  We may process information relating to our customer relationships, including customer contact information ("customer relationship data"). The customer relationship data may include your name, your employer, your job title or role, your contact details, and information contained in communications between us and you or your employer. The source of the customer relationship data is you or your employer. The customer relationship data may be processed for the purposes of managing our relationships with customers, communicating with customers, keeping records of those communications and promoting our products and services to customers. The legal basis for this processing is our legitimate interests, namely the proper management of our customer relationships.</li>
                        <li><span class="listNumber">2.8</span>  We may process information relating to transactions, including purchases of goods and services, that you enter into with us and/or through our website ("transaction data"). The transaction data may include your contact details, your card details and the transaction details. The transaction data may be processed for the purpose of supplying the purchased goods and services and keeping proper records of those transactions. The legal basis for this processing is the performance of a contract between you and us and/or taking steps, at your request, to enter into such a contract and our legitimate interests, namely the proper administration of our website and business.</li>
                        <li><span class="listNumber">2.9</span>  We may process information that you provide to us for the purpose of subscribing to our email notifications and/or newsletters ("notification data"). The notification data may be processed for the purposes of sending you the relevant notifications and/or newsletters. The legal basis for this processing is the performance of a contract between you and us and/or taking steps, at your request, to enter into such a contract.</li>
                        <li><span class="listNumber">2.10</span>    We may process information contained in or relating to any communication that you send to us ("correspondence data"). The correspondence data may include the communication content and metadata associated with the communication. Our website will generate the metadata associated with communications made using the website contact forms.  The correspondence data may be processed for the purposes of communicating with you and record-keeping. The legal basis for this processing is our legitimate interests, namely the proper administration of our website and business and communications with users.</li>
                        <li><span class="listNumber">2.11</span>    We may process any of your personal data identified in this policy where necessary for the establishment, exercise or defence of legal claims, whether in court proceedings or in an administrative or out-of-court procedure. The legal basis for this processing is our legitimate interests, namely the protection and assertion of our legal rights, your legal rights and the legal rights of others.</li>
                        <li><span class="listNumber">2.12</span>    We may process any of your personal data identified in this policy where necessary for the purposes of obtaining or maintaining insurance coverage, managing risks, or obtaining professional advice. The legal basis for this processing is our legitimate interests, namely the proper protection of our business against risks.</li>
                        <li><span class="listNumber">2.13</span>    In addition to the specific purposes for which we may process your personal data set out in this Section 3, we may also process any of your personal data where such processing is necessary for compliance with a legal obligation to which we are subject, or in order to protect your vital interests or the vital interests of another natural person.</li>
                        <li>
                            <span class="listNumber">2.14</span>    Please do not supply any other person's personal data to us, unless we prompt you to do so.
                        </li>
                        <li><span class="listNumber">2.15</span>    We collect information you provide when you download our app, apply or sign up for an account and when you provide information about yourself and individuals associated with your business as part of our identity verification process. We may collect, without limitation, the following information about you and individuals associated with your business:
                            <ul>
                                <li>•   Identification information, such as your name, email address, mailing address, phone number, photograph, birthdate and passport, driver’s licence, Medicare or other government-issued identification number];</li>
                                <li>•   Financial information, including bank account and payment card numbers;</li>
                                <li>•   Tax information, including withholding allowances and tax filing status; and</li>
                                <li>•   Other historical, contact and demographic information.</li>
                            </ul>
                            <li>We also collect information you upload to or send through our Services, including:</li>
                                <ul style="margin:0 30px;">
                                <li>•   Information about products and services you may sell (including inventory, pricing and other data);</li>
                                <li>•   Information you may provide about you or your business (including appointment, staffing, employee, payroll and third party contact data); and</li>
                                <li>•   Information you may provide to a seller using our Services (including hours worked and other timecard data).</li>
                            </ul>
                            <li>
                                Some of the information we collect is collected pursuant to applicable laws and regulations, including anti-money laundering laws (e.g., the Anti-Money Laundering and Counter-Terrorism Financing Act).
                            </li>
                            <li>We collect information you provide when you participate in contests or promotions offered by us or our partners, respond to our surveys or otherwise communicate with us.</li>
                        </li>
                        <li><strong>3.  Providing your personal data to others</strong></li>
                        <li><span class="listNumber">3.1</span>  We may disclose your personal data to any member of our group of companies (this means our subsidiaries, our ultimate holding company and all its subsidiaries) insofar as reasonably necessary for the purposes, and on the legal bases, set out in this policy.</li>
                        <li><span class="listNumber">3.2</span>  We may disclose your personal data to our insurers and/or professional advisers insofar as reasonably necessary for the purposes of obtaining or maintaining insurance coverage, managing risks, obtaining professional advice, or the establishment, exercise or defence of legal claims, whether in court proceedings or in an administrative or out-of-court procedure.</li>
                        <li><span class="listNumber">3.3</span>  We may disclose personal data to our suppliers or subcontractors insofar as reasonably necessary for us to provide our products and services to you.</li>
                        <li><span class="listNumber">3.4</span>  Financial transactions relating to our website and services are or may be handled by our payment services providers, Xero and Stripe. We will share transaction data with our payment services providers only to the extent necessary for the purposes of processing your payments, refunding such payments and dealing with complaints and queries relating to such payments and refunds. You can find information about the payment services providers' privacy policies and practices at https://www.xero.com/au/about/security/ and https://stripe.com/au/privacy</li>
                        <li><span class="listNumber">3.5</span>  We may disclose your enquiry data to one or more of those selected third party suppliers of goods and services identified on our website for the purpose of enabling them to contact you so that they can offer, market and sell to you relevant goods and/or services. Each such third party will act as a data controller in relation to the enquiry data that we supply to it; and upon contacting you, each such third party will supply to you a copy of its own privacy policy, which will govern that third party's use of your personal data.</li>
                        <li><span class="listNumber">3.6</span>  In addition to the specific disclosures of personal data set out in this Section 4, we may disclose your personal data where such disclosure is necessary for compliance with a legal obligation to which we are subject, or in order to protect your vital interests or the vital interests of another natural person. We may also disclose your personal data where such disclosure is necessary for the establishment, exercise or defence of legal claims, whether in court proceedings or in an administrative or out-of-court procedure.</li>
                        <li><strong>4.  International transfers of your personal data</strong></li>
                        <li><span class="listNumber">4.1</span>  In this section, we provide information about the circumstances in which your personal data may be transferred to countries outside of Australia.</li>
                        <li><span class="listNumber">4.2</span>  We have offices and facilities in Australia and Philippines. Transfers to each of these countries will be protected by appropriate safeguards, namely the use of standard data protection clauses.</li>
                        <li><span class="listNumber">4.3</span>  The hosting facilities for our website are situated in Australia only. </li>
                        <li><span class="listNumber">4.4</span>  You acknowledge that personal data that you submit for publication through our website or services may be available, via the internet, around the world. We cannot prevent the use (or misuse) of such personal data by others.</li>
                        <li><strong>5.  Retaining and deleting personal data</strong></li>
                        <li><span class="listNumber">5.1</span>  This section sets out our data retention policies and procedure, which are designed to help ensure that we comply with our legal obligations in relation to the retention and deletion of personal data.</li>
                        <li><span class="listNumber">5.2</span>  Personal data that we process for any purpose or purposes shall not be kept for longer than is necessary for that purpose or those purposes.</li>
                        <li><span class="listNumber">5.3</span>  We will retain your personal data for a minimum of 7 years for tax purposes.</li>
                        <li><span class="listNumber">5.4</span>  In some cases it is not possible for us to specify in advance the periods for which your personal data will be retained. </li>
                        <li><span class="listNumber">5.5</span>  Notwithstanding the other provisions of this Section 5, we may retain your personal data where such retention is necessary for compliance with a legal obligation to which we are subject, or in order to protect your vital interests or the vital interests of another natural person.</li>
                        <li><strong>6.  Amendments</strong></li>
                        <li><span class="listNumber">6.1</span>  We may update this policy from time to time by publishing a new version on our website.</li>
                        <li><span class="listNumber">6.2</span>  You should check this page occasionally to ensure you are happy with any changes to this policy.</li>
                        <li><span class="listNumber">6.3</span>  We may notify you of changes to this policy by email or through the private messaging system on our website.</li>
                        <li><strong>7.  Your rights</strong></li>
                        <li><span class="listNumber">7.1</span>  In this section, we have summarised the rights that you have under data protection law. Some of the rights are complex, and not all of the details have been included in our summaries. Accordingly, you should read the relevant laws and guidance from the regulatory authorities for a full explanation of these rights.</li>
                        <li>
                            <span class="listNumber">7.2</span>  Your principal rights under data protection law are:
                            <ul>
                                <li>(a) the right to access;</li>
                                <li>(b) the right to rectification;</li>
                                <li>(c) the right to erasure;</li>
                                <li>(d) the right to restrict processing;</li>
                                <li>(e) the right to object to processing;</li>
                                <li>(f) the right to data portability;</li>
                                <li>(g) the right to complain to a supervisory authority; and</li>
                                <li>(h) the right to withdraw consent.</li>
                            </ul>
                        </li>
                        <li><span class="listNumber">7.3</span>  You have the right to confirmation as to whether or not we process your personal data and, where we do, access to the personal data, together with certain additional information. That additional information includes details of the purposes of the processing, the categories of personal data concerned and the recipients of the personal data. Providing the rights and freedoms of others are not affected, we will supply to you a copy of your personal data. The first copy will be provided free of charge, but additional copies may be subject to a reasonable fee. You can request a copy of your personal data by emailing contactus@azenko.com.au.</li>
                        <li><span class="listNumber">7.4</span>  You have the right to have any inaccurate personal data about you rectified and, taking into account the purposes of the processing, to have any incomplete personal data about you completed.</li>
                        <li><span class="listNumber">7.5</span>  In some circumstances you have the right to the erasure of your personal data without undue delay. Those circumstances include: the personal data are no longer necessary in relation to the purposes for which they were collected or otherwise processed; you withdraw consent to consent-based processing; you object to the processing under certain rules of applicable data protection law; the processing is for direct marketing purposes; and the personal data have been unlawfully processed. However, there are exclusions of the right to erasure. The general exclusions include where processing is necessary: for exercising the right of freedom of expression and information; for compliance with a legal obligation; or for the establishment, exercise or defence of legal claims.</li>
                        <li><span class="listNumber">7.6</span>  In some circumstances you have the right to restrict the processing of your personal data. Those circumstances are: you contest the accuracy of the personal data; processing is unlawful but you oppose erasure; we no longer need the personal data for the purposes of our processing, but you require personal data for the establishment, exercise or defence of legal claims; and you have objected to processing, pending the verification of that objection. Where processing has been restricted on this basis, we may continue to store your personal data. However, we will only otherwise process it: with your consent; for the establishment, exercise or defence of legal claims; for the protection of the rights of another natural or legal person; or for reasons of important public interest.</li>
                        <li><span class="listNumber">7.7</span>  You have the right to object to our processing of your personal data on grounds relating to your particular situation, but only to the extent that the legal basis for the processing is that the processing is necessary for: the performance of a task carried out in the public interest or in the exercise of any official authority vested in us; or the purposes of the legitimate interests pursued by us or by a third party. If you make such an objection, we will cease to process the personal information unless we can demonstrate compelling legitimate grounds for the processing which override your interests, rights and freedoms, or the processing is for the establishment, exercise or defence of legal claims.</li>
                        <li><span class="listNumber">7.8</span>  You have the right to object to our processing of your personal data for direct marketing purposes (including profiling for direct marketing purposes). If you make such an objection, we will cease to process your personal data for this purpose.</li>
                        <li><span class="listNumber">7.9</span>  You have the right to object to our processing of your personal data for scientific or historical research purposes or statistical purposes on grounds relating to your particular situation, unless the processing is necessary for the performance of a task carried out for reasons of public interest.</li>
                        <li>
                            <span class="listNumber">7.10</span>    To the extent that the legal basis for our processing of your personal data is:
                            <ul>
                                <li>(a) consent; or</li>
                                <li>(b) that the processing is necessary for the performance of a contract to which you are party or in order to take steps at your request prior to entering into a contract,
                                    and such processing is carried out by automated means, you have the right to receive your personal data from us in a structured, commonly used and machine-readable format. However, this right does not apply where it would adversely affect the rights and freedoms of others.</li>
                            </ul>
                        </li>
                        <li><span class="listNumber">7.11</span> If you consider that our processing of your personal information infringes data protection laws, you have a legal right to lodge a complaint with a supervisory authority responsible for data protection. </li>
                        <li><span class="listNumber">7.12</span> To the extent that the legal basis for our processing of your personal information is consent, you have the right to withdraw that consent at any time. Withdrawal will not affect the lawfulness of processing before the withdrawal.</li>
                        <li><span class="listNumber">7.13</span> You may exercise any of your rights in relation to your personal data by written notice to us at contactus@azenko.com.au, in addition to the other methods specified in this Section 8.</li>
                        <li><strong>8. About cookies</strong></li>
                        <li><span class="listNumber">8.1</span> A cookie is a file containing an identifier (a string of letters and numbers) that is sent by a web server to a web browser and is stored by the browser. The identifier is then sent back to the server each time the browser requests a page from the server.</li>
                        <li><span class="listNumber">8.2</span> Cookies may be either "persistent" cookies or "session" cookies: a persistent cookie will be stored by a web browser and will remain valid until its set expiry date, unless deleted by the user before the expiry date; a session cookie, on the other hand, will expire at the end of the user session, when the web browser is closed.</li>
                        <li><span class="listNumber">8.3</span> Cookies do not typically contain any information that personally identifies a user, but personal information that we store about you may be linked to the information stored in and obtained from cookies.</li>
                        <li><strong>9.  Cookies that we use</strong></li>
                        <li>
                            <span class="listNumber">9.1</span> We use cookies for the following purposes:
                            <ul>
                                <li>(a) authentication - we use cookies to identify you when you visit our website and as you navigate our website;</li>
                                <li>(b) status - we use cookies to help us to determine if you are logged into our website;</li>
                                <li>(c) personalisation - we use cookies to store information about your preferences and to personalise the website for you;</li>
                                <li>(d) security - we use cookies as an element of the security measures used to protect user accounts, including preventing fraudulent use of login credentials, and to protect our website and services generally;</li>
                                <li>(e) advertising - we use cookies to help us to display advertisements that will be relevant to you;</li>
                                <li>(f) analysis - we use cookies to help us to analyse the use and performance of our website and services;</li>
                                <li>(g) cookie consent - we use cookies to store your preferences in relation to the use of cookies more generally.</li>
                            </ul>
                        </li>
                        <li><strong>10. Cookies used by our service providers</strong></li>
                        <li><span class="listNumber">10.1</span> Our service providers use cookies and those cookies may be stored on your computer when you visit our website.</li>
                        <li><span class="listNumber">10.2</span> We use Google Analytics to analyse the use of our website. Google Analytics gathers information about website use by means of cookies. The information gathered relating to our website is used to create reports about the use of our website. Google's privacy policy is available at: https://www.google.com/policies/privacy/.</li>
                        <li><span class="listNumber">10.3</span> We publish Google AdSense interest-based advertisements on our website. These are tailored by Google to reflect your interests. To determine your interests, Google will track your behaviour on our website and on other websites across the web using cookies. We may also publish Google AdSense advertisements on our website. To determine your interests, Google will track your behaviour on our website and on other websites across the web using cookies. This behaviour tracking allows Google to tailor the advertisements that you see on other websites to reflect your interests (but we do not publish interest-based advertisements on our website). You can view, delete or add interest categories associated with your browser by visiting: https://adssettings.google.com. You can also opt out of the AdSense partner network cookie using those settings or using the Network Advertising Initiative's multi-cookie opt-out mechanism at: http://optout.networkadvertising.org. However, these opt-out mechanisms themselves use cookies, and if you clear the cookies from your browser your opt-out will not be maintained. To ensure that an opt-out is maintained in respect of a particular browser, you may wish to consider using the Google browser plug-ins available at: https://support.google.com/ads/answer/7395996.</li>
                        <li><strong>11. Managing cookies</strong></li>
                        <li>
                            <span class="listNumber">11.1</span>    Most browsers allow you to refuse to accept cookies and to delete cookies. The methods for doing so vary from browser to browser, and from version to version. You can however obtain up-to-date information about blocking and deleting cookies via these links:
                            <ul>
                                <li>(a) https://support.google.com/chrome/answer/95647?hl=en (Chrome);</li>
                                <li>(b) https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences (Firefox);</li>
                                <li>(c) http://www.opera.com/help/tutorials/security/cookies/ (Opera);</li>
                                <li>(d) https://support.microsoft.com/en-gb/help/17442/windows-internet-explorer-delete- manage-cookies (Internet Explorer);</li>
                                <li>(e) https://support.apple.com/kb/PH21411 (Safari); and</li>
                                <li>(f) https://privacy.microsoft.com/en-us/windows-10-microsoft-edge-and-privacy (Edge).</li>
                            </ul>
                        </li>
                        <li><span class="listNumber">11.2</span>    Blocking all cookies will have a negative impact upon the usability of many websites.</li>
                        <li><span class="listNumber">11.3</span>    If you block cookies, you will not be able to use all the features on our website or app.</li>
                        <li><strong>12. Our details</strong></li>
                        <li><span class="listNumber">12.1</span>    This website and any associated application is owned and operated by Azenko.</li>
                        <li><span class="listNumber">12.2</span>    We are registered in Australia under registration number 626 107 739, and our registered office is at YK Partners, Level 2, 545 King Street, West Melbourne VIC 3003.</li>
                        <li><span class="listNumber">12.3</span>    Our principal place of business is at Level 1, 157 Given Terrace, Paddington QLD 4064.</li>
                        <li><span class="listNumber">12.4</span>    You can contact us: contactus@azenko.com.au
                            <ul>
                                <li>(a) by post, to the postal address given above;</li>
                                <li>(b)     by telephone, on the contact number published on our website from time to time; or</li>
                                <li>(c) by email, using the email address supplied above and published on our website from time to time.</li>
                            </ul>
                        </li>
                        <li><strong>13. Promotional Communications</strong></li>
                        <li><span class="listNumber">13.1</span>    You may opt out of receiving promotional messages by following the instructions in those messages or by making a request to us using the contact details below. If you decide to opt out, we may still send you non-promotional communications, such as digital receipts you request.</li>
                        <li><strong>14  Data protection officer</strong></li>
                        <li><span class="listNumber">14.1</span>    Our data protection officer's contact details are:  rachel@azenko.com.au.</li>

                    </ul>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>

            
        </div>
    </body>

    <!--Jquery-->
    <script  src="includes/js/jquery1.11.1.min.js" type="text/javascript"></script>
    <!--Full Slider-->
    <script src="includes/js/jquery.cycle.all.js" type="text/javascript"></script>
    <script src="includes/js/jquery.maximage.js" type="text/javascript"></script>    
    <!-- Bootstrap -->
    <script src="includes/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.5.1/vue-resource.min.js"></script>
    



    <script type="text/javascript" src="includes/js/jquery.multifield.min.js"></script>
    <script type="text/javascript">
        var app = new Vue({
          el: '#app',
          data: {
            storeCount: 1,
            details:{'directorName':'', store1:{'name': '', 'type': '', 'dealer' : '', 'line' : '', 'companyName': '', 'companyAddress' : '', 'contactName': '','contactNumber': ''} }
          },
          methods:{
            submit: function () {
                console.log(this.details);

                var url = 'https://team360.app:4443/api/netproSignUp';
                var contactData = $("#form-signup").serialize();
                var request = $.ajax({
                  url: url,
                  type: 'get',
                  data: contactData,
                  dataType: 'json',
                  success: function (result) {
                    console.log(result);
                      if(result.type == 'Success'){
                        console.log("success");
                        window.location.replace("https://netpro.azenko.com.au/success.php");
                      }
                      
                      
                  },
                  done: function (result) {
                    console.log(result);
                    if(result.type == 'Success'){
                        console.log("success 2");
                      }
                    document.getElementById('ajaxmsgs').style.display = 'block';
                    window.location.replace("https://netpro.azenko.com.au/success.php");
                    if (result.type === 'Success') {
                      //window.location.replace("https://netpro.azenko.com.au/success.php");

                      var link = $('<a href=' + result.URL + ' rel="lightbox" id="link">Click</a>');
                      $("body").append(link);
                      $("#link")[0].click();
                    }
                    else {
                      window.location.replace("https://netpro.azenko.com.au/success.php");
                      $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong><br>" + result.message + "</div>");
                    }
                  },
                  error: function () {
                      document.getElementById('ajaxmsgs').style.display = 'block';
                      $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
                  }
              });
                
            }
          }
        })
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120818313-3"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-120818313-3');
    </script>
    </html>
