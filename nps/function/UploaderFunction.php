<?php

include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/db/msconnection.php');
session_start();
/* Uploader Function for medallia Uploder */

//Insert values to t_surveypd
function InsertSurveyPD($sqlSurveyPD) {
    global $connection;

    $result = mysql_query($sqlSurveyPD, $connection);
    $returnInsertPD = array();
    if ($result && mysql_affected_rows() >= 1) {
        $returnInsertPD['message'] = "Uploaded " . mysql_affected_rows() . " new product details!";
    } else {
        $returnInsertPD['message'] = "Product details Insert Failed!" . mysql_error();
    }

    return $returnInsertPD;
}

//Insert values to t_surveysd
function InsertSurveySD($sqlSurveySD) {
    global $connection;

    // echo $sqlSurveySD;
    $result = mysql_query($sqlSurveySD, $connection);
    $returnInsertSD = array();
    if ($result && mysql_affected_rows() >= 1) {
        $returnInsertSD['message'] = "Uploaded " . mysql_affected_rows() . " new survey!";
    } else {
        $returnInsertSD['message'] = "Survey details Insert Failed!" . mysql_error();
    }

    return $returnInsertSD;
}

//Check if Customer Details Exisst
function getCustomerDetails($customerDetails) {
    global $connection;
    // echo $customerDetails['CustomerID'];
    $sqlCheckIfExist = "SELECT f_CustomerDetailsID from t_customerdetails WHERE f_CustomerID = '{$customerDetails['CustomerID']}'";
    $resulExist = mysql_query($sqlCheckIfExist, $connection);
    $data = mysql_fetch_assoc($resulExist);

    // echo $toSQL;
    if (is_null($data['f_CustomerDetailsID'])) {

        $toSQL = "";
        foreach ($customerDetails as $preparedData) {
            $toSQL .= "'" . mysql_real_escape_string($preparedData) . "',";
        }
        $toSQL = trim($toSQL, ",");

        $sqlInsert = "INSERT INTO `t_customerdetails`  (`f_CustomerID`,`f_CustomerSalutationTitle`,`f_CustomerFirstName`,`f_CustomerState`,`f_CustomerPostCode`)
                    VALUES ({$toSQL})";

        //`f_CustomerMobile`,`f_CustomerEmail`,`f_CustomerPhoneNumber`,

        $result = mysql_query($sqlInsert, $connection);

        if ($result && mysql_affected_rows() >= 1) {
            //  echo "here".mysql_insert_id()."-".$customerDetails['CustomerID']."<br>";
            $resultCustomerID = mysql_insert_id();
        }
    } else {
        $resultCustomerID = $data['f_CustomerDetailsID'];
    }

    return $resultCustomerID;
}

//Extract ID values on Database for SuveyPD
function getPreparedData($value) {
    global $connection;

//ID's for t_SurveyPD
    //Customer ID
    //  echo "Here";
    $customerDetails = getCustomerDetails($value['CustomerDetails']);
    //echo $customerDetails;
    $result['CustomerID'] = $customerDetails;

    //SurveyType
    $sqlSurveyType = "SELECT f_SurveyListID FROM t_surveylist WHERE UPPER(f_SurveyName) = UPPER('{$value['SurveyType']}') LIMIT 1 ";
    $querySurveyType = mysql_query($sqlSurveyType, $connection);
    $dataSurveyList = mysql_fetch_assoc($querySurveyType);
    //$result['SurveyType'] = $dataSurveyList['f_SurveyListID'];
    if (is_null($dataSurveyList['f_SurveyListID']) || $dataSurveyList['f_SurveyListID'] == 0) {
        $sqlInsertNewSurveyList = "INSERT INTO `t_surveylist` (`f_SurveyName`) VALUES ('{$value['SurveyType']}')";
        $resultNewSurveyList = mysql_query($sqlInsertNewSurveyList, $connection);
        if ($resultNewSurveyList && mysql_affected_rows() >= 1) {
            //  echo "here".mysql_insert_id()."-".$customerDetails['CustomerID']."<br>";
            $resultsurveyID = mysql_insert_id();
            $result['SurveyType'] = $resultsurveyID;
        }
    } else {
        $result['SurveyType'] = $dataSurveyList['f_SurveyListID'];
    }


    //TelstraSurveryProgramID
    $sqlTelstraSurveryProgramID = "SELECT f_TelstraSurveyProgramID FROM t_telstrasurveyprogram WHERE UPPER(f_TelstraSurveyProgramName) = UPPER('{$value['TelstraSurveyProgramID']}') LIMIT 1 ";
    $queryTelstraSurveryProgramID = mysql_query($sqlTelstraSurveryProgramID, $connection);
    $dataTelstraSurveryProgramID = mysql_fetch_assoc($queryTelstraSurveryProgramID);
    //$result['TelstraSurveyProgramID'] = $dataTelstraSurveryProgramID['f_TelstraSurveyProgramID'];
    if (is_null($dataTelstraSurveryProgramID['f_TelstraSurveyProgramID']) || $dataTelstraSurveryProgramID['f_TelstraSurveyProgramID'] == 0) {
        $sqlInsertNewSurveryProgramID = "INSERT INTO `t_telstrasurveyprogram` (`f_TelstraSurveyProgramName`) VALUES ('{$value['TelstraSurveyProgramID']}')";
        $resultNewSurveryProgramID = mysql_query($sqlInsertNewSurveryProgramID, $connection);
        if ($resultNewSurveryProgramID && mysql_affected_rows() >= 1) {
            //  echo "here".mysql_insert_id()."-".$customerDetails['CustomerID']."<br>";
            $resultprogramID = mysql_insert_id();
            $result['TelstraSurveyProgramID'] = $resultprogramID;
        }
    } else {
        $result['TelstraSurveyProgramID'] = $dataTelstraSurveryProgramID['f_TelstraSurveyProgramID'];
    }

    //SubSurveyID
    $sqlSubSurvey = "SELECT f_SubSurveyID FROM t_subsurveylist WHERE UPPER(f_SubSurveyName) = UPPER('{$value['SubSurveyName']}') LIMIT 1 ";
    $querySubSurvey = mysql_query($sqlSubSurvey, $connection);
    $dataSubSurvey = mysql_fetch_assoc($querySubSurvey);
    // $result['SubSurveyName'] = $dataSubSurvey['f_SubSurveyID'];
    if (is_null($dataSubSurvey['f_SubSurveyID']) || $dataSubSurvey['f_SubSurveyID'] == 0) {
        $sqlInsertNewSubSurvey = "INSERT INTO `t_subsurveylist` (`f_SubSurveyName`) VALUES ('{$value['SubSurveyName']}')";
        $resultNewSubSurvey = mysql_query($sqlInsertNewSubSurvey, $connection);
        if ($resultNewSubSurvey && mysql_affected_rows() >= 1) {
            //  echo "here".mysql_insert_id()."-".$customerDetails['CustomerID']."<br>";
            $resultsubsurveyID = mysql_insert_id();
            $result['SubSurveyName'] = $resultsubsurveyID;
        }
    } else {
        $result['SubSurveyName'] = $dataSubSurvey['f_SubSurveyID'];
    }

    //SurveyMethodID
    $sqlMethod = "SELECT f_SurveyMethodID FROM t_surveymethodlist WHERE UPPER(f_SurveyMethodName) = UPPER('{$value['SurveyMethodName']}') LIMIT 1 ";
    $querySurveyMethod = mysql_query($sqlMethod, $connection);
    $dataSurveyMethod = mysql_fetch_assoc($querySurveyMethod);
    //$result['SurveyMethodName'] = $dataSurveyMethod['f_SurveyMethodID'];
    if (is_null($dataSurveyMethod['f_SurveyMethodID']) || $dataSurveyMethod['f_SurveyMethodID'] == 0) {
        $sqlInsertNewSurveyMethod = "INSERT INTO `t_surveymethodlist` (`f_SurveyMethodName`) VALUES ('{$value['SurveyMethodName']}')";
        $resultNewSurveyMethod = mysql_query($sqlInsertNewSurveyMethod, $connection);
        if ($resultNewSurveyMethod && mysql_affected_rows() >= 1) {
            //  echo "here".mysql_insert_id()."-".$customerDetails['CustomerID']."<br>";
            $resultsurveymethodID = mysql_insert_id();
            $result['SurveyMethodName'] = $resultsurveymethodID;
        }
    } else {
        $result['SurveyMethodName'] = $dataSurveyMethod['f_SurveyMethodID'];
    }

    //EpisodeSourceID
    $sqlEpisodeSource = "SELECT f_EpisodeSourceID FROM t_episodesourcelist WHERE UPPER(f_EpisodeSourceName) = UPPER('{$value['EpisodeSourceName']}') LIMIT 1  ";
    $queryEpisodeSource = mysql_query($sqlEpisodeSource, $connection);
    $dataEpisodeSource = mysql_fetch_assoc($queryEpisodeSource);
    //$result['EpisodeSourceName'] = $dataEpisodeSource['f_EpisodeSourceID'];
    if (is_null($dataEpisodeSource['f_EpisodeSourceID']) || $dataEpisodeSource['f_EpisodeSourceID'] == 0) {
        $sqlInsertNewEpisodeSource = "INSERT INTO `t_episodesourcelist` (`f_EpisodeSourceName`) VALUES ('{$value['EpisodeSourceName']}')";
        $resultNewEpisodeSource = mysql_query($sqlInsertNewEpisodeSource, $connection);
        if ($resultNewEpisodeSource && mysql_affected_rows() >= 1) {
            //  echo "here".mysql_insert_id()."-".$customerDetails['CustomerID']."<br>";
            $resultepisodesourceID = mysql_insert_id();
            $result['EpisodeSourceName'] = $resultepisodesourceID;
        }
    } else {
        $result['EpisodeSourceName'] = $dataEpisodeSource['f_EpisodeSourceID'];
    }

    //CustomerBusinessUnitID
    $sqlCustomerBusinessUnit = "SELECT f_CustomerBusinessUnitListID FROM t_customerbusinessunitlist WHERE UPPER(f_CustomerBusinessUnitName) = UPPER('{$value['CustomerBusinessUnit']}') LIMIT 1 ";
    $queryCustomerBusinessUnit = mysql_query($sqlCustomerBusinessUnit, $connection);
    $dataCustomerBusinessUnit = mysql_fetch_assoc($queryCustomerBusinessUnit);
    //$result['CustomerBusinessUnit'] = $dataCustomerBusinessUnit['f_CustomerBusinessUnitListID'];
    if (is_null($dataCustomerBusinessUnit['f_CustomerBusinessUnitListID']) || $dataCustomerBusinessUnit['f_CustomerBusinessUnitListID'] == 0) {
        $sqlInsertNewCustomerBusinessUnit = "INSERT INTO `t_customerbusinessunitlist` (`f_CustomerBusinessUnitName`) VALUES ('{$value['CustomerBusinessUnit']}')";
        $resultNewCustomerBusinessUnit = mysql_query($sqlInsertNewCustomerBusinessUnit, $connection);
        if ($resultNewCustomerBusinessUnit && mysql_affected_rows() >= 1) {
            //  echo "here".mysql_insert_id()."-".$customerDetails['CustomerID']."<br>";
            $resultcustomerbusinessunitID = mysql_insert_id();
            $result['CustomerBusinessUnit'] = $resultcustomerbusinessunitID;
        }
    } else {
        $result['CustomerBusinessUnit'] = $dataCustomerBusinessUnit['f_CustomerBusinessUnitListID'];
    }

    //CustomerMBMSegmentID
    $sqlCustomerMBMSegment = "SELECT f_CustomerMBMSegmentID FROM t_customermbmsegmentlist WHERE UPPER(f_CustomerMBMSegmentName) = UPPER('{$value['CustomerMBMSegment']}') LIMIT 1 ";
    $queryCustomerMBMSegment = mysql_query($sqlCustomerMBMSegment, $connection);
    $dataCustomerMBMSegment = mysql_fetch_assoc($queryCustomerMBMSegment);
    // $result['CustomerMBMSegment'] = $dataCustomerMBMSegment['f_CustomerMBMSegmentID'];
    if (is_null($dataCustomerMBMSegment['f_CustomerMBMSegmentID']) || $dataCustomerMBMSegment['f_CustomerMBMSegmentID'] == 0) {
        $sqlInsertNewCustomerMBMSegment = "INSERT INTO `t_customermbmsegmentlist` (`f_CustomerMBMSegmentName`) VALUES ('{$value['CustomerMBMSegment']}')";
        $resultNewCustomerMBMSegment = mysql_query($sqlInsertNewCustomerMBMSegment, $connection);
        if ($resultNewCustomerMBMSegment && mysql_affected_rows() >= 1) {
            //  echo "here".mysql_insert_id()."-".$customerDetails['CustomerID']."<br>";
            $resultcustomermbmsegmentID = mysql_insert_id();
            $result['CustomerMBMSegment'] = $resultcustomermbmsegmentID;
        }
    } else {
        $result['CustomerMBMSegment'] = $dataCustomerMBMSegment['f_CustomerMBMSegmentID'];
    }

    //ProductDescriptionID
    $sqlProducDescription = "SELECT f_ProductID FROM t_productlist WHERE UPPER(f_ProductDescription) = UPPER('{$value['ProducDescription']}') LIMIT 1 ";
    $queryProducDescription = mysql_query($sqlProducDescription, $connection);
    $dataProducDescription = mysql_fetch_assoc($queryProducDescription);
    if (is_null($dataProducDescription['f_ProductID']) || $dataProducDescription['f_ProductID'] == 0 ) {
        $sqlInsertNewProduct = "INSERT INTO `t_productlist` (`f_ProductDescription`) VALUES ('{$value['ProducDescription']}')";
        $resultNewProduct = mysql_query($sqlInsertNewProduct, $connection);
        if ($resultNewProduct && mysql_affected_rows() >= 1) {
            //  echo "here".mysql_insert_id()."-".$customerDetails['CustomerID']."<br>";
            $resultproductID = mysql_insert_id();
            $result['ProducDescription'] = $resultproductID;
        }
    } else {
        $result['ProducDescription'] = $dataProducDescription['f_ProductID'];
    }

//ID's for t_SurveyPD
    //f_OriginationChannelID
    $sqlOriginationChannel = "SELECT f_OriginationChannelID FROM t_originationchannel  WHERE UPPER(f_OriginationChannelName) = UPPER('{$value['OriginationChannel']}') LIMIT 1 ";
    $queryOriginationChannel = mysql_query($sqlOriginationChannel, $connection);
    $dataOriginationChannel = mysql_fetch_assoc($queryOriginationChannel);
    //$result['OriginationChannel'] = $dataOriginationChannel['f_OriginationChannelID'];
    if (is_null($dataOriginationChannel['f_OriginationChannelID']) || $dataOriginationChannel['f_OriginationChannelID'] == 0) {
        $sqlInsertNewOriginationChannel = "INSERT INTO `t_originationchannel` (`f_OriginationChannelName`) VALUES ('{$value['OriginationChannel']}')";
        $resultNewOriginationChannel = mysql_query($sqlInsertNewOriginationChannel, $connection);
        if ($resultNewOriginationChannel && mysql_affected_rows() >= 1) {
            //  echo "here".mysql_insert_id()."-".$customerDetails['CustomerID']."<br>";
            $resultoriginationchannelID = mysql_insert_id();
            $result['OriginationChannel'] = $resultoriginationchannelID;
        }
    } else {
        $result['OriginationChannel'] = $dataOriginationChannel['f_OriginationChannelID'];
    }

    $querygetToMerge = "SELECT f_MergeToCompanyCode from t_mergereport";
    $resultToMerge = mysql_query($querygetToMerge, $connection);
    while ($row = mysql_fetch_assoc($resultToMerge)) {
        $merge[] = $row['f_MergeToCompanyCode'];
    }

    // echo $querygetToMerge;
    // var_dump($merge);
    if (in_array($value['StoreID'], $merge)) {
        $querygetToMergeID = "SELECT f_StoreID from t_mergereport WHERE UPPER(f_MergeToCompanyCode) = UPPER('{$value['StoreID']}') ";
        $resultToMergeID = mysql_query($querygetToMergeID, $connection);
        $dataStoreID = mysql_fetch_assoc($resultToMergeID);
        $result['StoreID'] = $dataStoreID['f_StoreID'];
        //  echo $result['StoreID'] ." = " .$querygetToMergeID. "<br>";
    } else {
        $sqlStoreID = "SELECT f_StoreID FROM t_storelist where UPPER(f_CompanyCode) = UPPER('{$value['StoreID']}') and f_Status = 1";
        $queryStoreID = mysql_query($sqlStoreID, $connection);
        $dataStoreID = mysql_fetch_assoc($queryStoreID);
        $result['StoreID'] = $dataStoreID['f_StoreID'];
    }
    return $result;
}

//Get Survey PDID
function getSurveypdID($data) {
    global $connection;

    $toSQL = "";
    foreach ($data as $key => $value) {
        $toSQL .= "'$key',";
    }
    $toSQL = trim($toSQL, ",");

    $sqlquery = "SELECT f_InvitationID,f_SurveyPDID FROM t_surveypd WHERE f_InvitationID IN ({$toSQL})";
    $result = mysql_query($sqlquery, $connection);
    while ($row = mysql_fetch_assoc($result)) {
        $returnSurveyPDID[$row['f_InvitationID']] = $row['f_SurveyPDID'];
    }
    return $returnSurveyPDID;
}

//Arrange excel data to array for insertion
function PrepareExcelArray($value) {
    global $connection;

    $sqlCheckExist = "SELECT COUNT(*) AS inserted FROM t_surveypd WHERE f_InvitationID = '{$value[0][0]}'";
    $resultCheckExist = mysql_query($sqlCheckExist, $connection);
    while ($data = mysql_fetch_assoc($resultCheckExist)) {
        $inserted = $data['inserted'];
    }
    // echo $inserted;
    if ($inserted >= 1) {
        $returnData['exisiting'] = 'yes';
    } else {
        $returnData['exisiting'] = 'no';
        //  echo "12312";
    }

    $returnData['f_CustomerID'] = $value[0][17];
    $returnData['f_CustomerSalutationTitle'] = $value[0][11];
    $returnData['f_CustomerFirstName'] = $value[0][12];
    $returnData['f_CustomerLastname'] = $value[0][13];
    $returnData['f_CustomerMobile'] = $value[0][14];
    $returnData['f_CustomerEmail'] = $value[0][15];
    $returnData['f_CustomerPhoneNumber'] = $value[0][16];
    $returnData['f_CustomerState'] = $value[0][20];
    $returnData['f_CustomerPostCode'] = $value[0][21];

    $getDataPD = array(
        'CustomerID' => $value[0][17],
        'SurveyType' => $value[0][1],
        'TelstraSurveyProgramID' => $value[0][2],
        'SubSurveyName' => $value[0][3],
        'SurveyMethodName' => $value[0][8],
        'EpisodeSourceName' => $value[0][5],
        'CustomerBusinessUnit' => $value[0][19],
        'CustomerMBMSegment' => $value[0][22],
        'ProducDescription' => $value[0][24],
        'OriginationChannel' => $value[0][47],
        'StoreID' => $value[0][46],
        'CustomerDetails' => array('CustomerID' => $returnData['f_CustomerID'],
            'CustomerSalutationTitle' => $returnData['f_CustomerSalutationTitle'],
            'CustomerFirstName' => $returnData['f_CustomerFirstName'],
            //'CustomerLastname' => $returnData['f_CustomerLastname'],
            //'CustomerMobile' => $returnData['f_CustomerMobile'],
            //'CustomerEmail' => $returnData['f_CustomerEmail'],
            //'CustomerPhoneNumber' => $returnData['f_CustomerPhoneNumber'],
            'CustomerState' => $returnData['f_CustomerState'],
            'CustomerPostCode' => $returnData['f_CustomerPostCode']
        )
    );

    $preparedData = getPreparedData($getDataPD);

//ID's for t_SurveyPD
    $returnData['f_CustomerDetailsID'] = $preparedData['CustomerID'];
    $returnData['f_SurveyListID'] = $preparedData['SurveyType'];
    $returnData['f_TelstraSurveyProgramID'] = $preparedData['TelstraSurveyProgramID'];
    $returnData['f_SubSurveyID'] = $preparedData['SubSurveyName'];
    $returnData['f_EpisodeSourceID'] = $preparedData['EpisodeSourceName'];
    $returnData['f_SurveyMethodID'] = $preparedData['SurveyMethodName'];
    $returnData['f_CustomerBusinessUnitID'] = $preparedData['CustomerBusinessUnit'];
    $returnData['f_CustomerMBMSegment'] = $preparedData['CustomerMBMSegment'];
    $returnData['f_ProductID'] = $preparedData['ProducDescription'];

//ID's for t_SurveySD
    $returnData['f_OriginationChannelID'] = $preparedData['OriginationChannel'];
    $returnData['f_StoreID'] = $preparedData['StoreID'];

//t_SurveyPD Fields
    $returnData['f_InvitationID'] = $value[0][0];
    //f_CustomerDetailsID
    //f_SurveyListID
    //f_TelstraSurveyProgramID
    //f_SubSurveyID
    $returnData['f_EpisodeReferenceNo'] = mysql_real_escape_string($value[0][4]);
    //f_EpisodeSourceID
    $returnData['f_EpisodeStartDate'] = $value[0][6];
    $returnData['f_EpisodeEndDate'] = $value[0][7];
    //f_SurveyMethodID
    $returnData['f_AskFieldQuestion'] = $value[0][9];
    $returnData['f_AskRetialQuestion'] = $value[0][10];
    $returnData['f_CustomerBillingAccountID'] = $value[0][18];
    //f_CustomerBusinessUnitID
    //f_CustomerMBMSegmentID
    $returnData['f_Product1'] = $value[0][23];
    $returnData['f_Product2'] = $value[0][25];
    $returnData['f_Product3'] = $value[0][26];
    $returnData['f_Product4'] = $value[0][27];
    $returnData['f_Product5'] = $value[0][28];
    $returnData['f_Product6'] = $value[0][29];
    $returnData['f_Product7'] = $value[0][30];
    $returnData['f_Product8'] = $value[0][31];
    $returnData['f_Product9'] = $value[0][32];
    $returnData['f_Product10'] = $value[0][33];
    //f_ProductID
//t_SurveySD Fields
    //f_SurveyPDID
    $returnData['f_UserID1'] = $value[0][34];
    //  $returnData['f_UserID2'] = $value[0][35];
    // $returnData['f_UserID3'] = $value[0][36];
    //$returnData['f_UserID4'] = $value[0][37];
    //$returnData['f_UserID5'] = $value[0][38];
    //$returnData['f_UserID6'] = $value[0][39];
    $returnData['f_UserID7'] = $value[0][40];
    $returnData['f_UserID8'] = $value[0][41];
    $returnData['f_UserID9'] = $value[0][42];
    $returnData['f_UserID10'] = $value[0][43];
    $returnData['f_UserID11'] = $value[0][44];
    $returnData['f_UserID12'] = $value[0][45];
    $returnData['f_DealerCode'] = $value[0][46]; //StoreID
    //f_StoreID
    //f_OriginationChannelID
    $returnData['f_ActivationExtra4'] = $value[0][48];

    if ((empty($value[0][49]) && strlen(trim($value[0][49])) < 0 ) || $value[0][49] == "") {
        $start = 'Order Userid : ';
        $end = ';';
        $staffNames = '';
        $match = '';

        $pattern = sprintf(
                '/%s(.+?)%s/ims', preg_quote($start, '/'), preg_quote($end, '/')
        );

        if (preg_match($pattern, $value[0][62], $matches)) {
            list(, $match) = $matches;
            $returnData['f_ActivationExtra5'] = $match;
        } else {

            $startStaff = 'Staff_Name:';
            $endStaff = ',';
            $patternStaff = sprintf(
                    '/%s(.+?)%s/ims', preg_quote($startStaff, '/'), preg_quote($endStaff, '/')
            );

            if (preg_match($patternStaff, $value[0][62] . $endStaff, $staffNames)) {
                list(, $staffname) = $staffNames;
                $staffNames = strtolower(trim($staffname));

                $sqlGetPnumber = "select b.f_EmpPNumber from t_emplist a
                    left join t_empnumber b on a.f_EmpID = b.f_EmpID
                    where LOWER(Concat(f_EmpFirstName,' ',f_EmpLastName)) = '{$staffNames}'
                    and f_StoreID = '{$returnData['f_StoreID']}'";
                $resultGetPnumber = mysql_query($sqlGetPnumber, $connection);
                while ($rowPnumber = mysql_fetch_assoc($resultGetPnumber)) {
                    $returnData['f_ActivationExtra5'] = $rowPnumber['f_EmpPNumber'];
                }
            } else {
                if (!empty($returnData['f_CustomerMobile']) or $returnData['f_CustomerMobile'] != "") {
                    //$dbh->query();
                } else {
                    $returnData['f_ActivationExtra5'] = "";
                }
            }
        }
    } else {
        $returnData['f_ActivationExtra5'] = $value[0][49];
    }



    $returnData['f_BillFormat'] = $value[0][50];
    $returnData['f_BillMethod'] = $value[0][51];
    $returnData['f_AssuranceLoggingSystem'] = $value[0][52];
    $returnData['f_AssuranceArea'] = $value[0][53];
    $returnData['f_AssuranceSubArea'] = $value[0][54];
    $returnData['f_ComplaintTIOPriorityLevel'] = $value[0][56];
    $returnData['f_ComplaintArea'] = $value[0][57];
    $returnData['f_ComplaintReason'] = $value[0][58];
    $returnData['f_ComplaintSubReason'] = $value[0][59];
    $returnData['f_ComplaintSource'] = $value[0][60];
    $returnData['f_DisconnectionStatus'] = $value[0][61];
    $returnData['f_InteractionLog'] = $value[0][62];
    $returnData['f_SurveyID'] = $value[0][63];
    $returnData['f_DateTimeCreatedMelbourne'] = $value[0][64];
    $returnData['f_DateTimeResponseMelbourne'] = $value[0][65];
    $returnData['f_IssueResolved'] = $value[0][66];
    if ((empty($value[0][67]) && strlen(trim($value[0][67])) < 0 ) || $value[0][67] == "") {
        $value[0][67] = NULL;
    }
    $returnData['f_RecommendRate'] = $value[0][67];
    $returnData['f_CustomerReason'] = $value[0][68];
    $returnData['f_ResolutionPreference'] = $value[0][69];
    $returnData['f_FollowUp'] = $value[0][70];
    $returnData['f_CallbackNumberCombined'] = $value[0][71];
    $returnData['f_IssueDescription'] = $value[0][72];

//Added Missing fields for t_surveySD
    $returnData['f_AssuranceSymptom'] = $value[0][55];
    if ((empty($value[0][73]) && strlen(trim($value[0][73])) < 0 ) || $value[0][73] == "") {
        $value[0][73] = NULL;
    }
    if ((empty($value[0][74]) && strlen(trim($value[0][74])) < 0) || $value[0][74] == "") {
        $value[0][74] = NULL;
    }
    if ((empty($value[0][75]) && strlen(trim($value[0][75])) < 0) || $value[0][75] == "") {
        $value[0][75] = NULL;
    }
    $returnData['f_LikelihoodToRecommendField'] = $value[0][73];
    $returnData['f_LikelihoodToRecommendRetail'] = $value[0][74];
    $returnData['f_LikelihoodToRecommendFollowUp'] = $value[0][75];
    $returnData['f_IndustryCode1'] = $value[0][76];
    $returnData['f_IndustryCode2'] = $value[0][77];
    $returnData['f_SalesPortfolioCode'] = $value[0][78];
    $returnData['f_SalesGroup'] = $value[0][79];
    $returnData['f_BusinessUltimateCIDN'] = $value[0][80];
    $returnData['f_BusinessMetroRegional'] = $value[0][81];
    $returnData['f_BusinessManagedStatus'] = $value[0][82];
    $returnData['f_LikelihoodToRecommendAE'] = $value[0][83];
    $returnData['f_ReportingFlagDashboards'] = $value[0][84];
    $returnData['f_TimeWaiting'] = $value[0][85];

    return $returnData;
}

function CheckLockFile() {
    global $connection;
    // $inserted = array();
    $filename = $_SERVER['DOCUMENT_ROOT'] . "/tmp/lockfile.txt";
    $sqlCheck = "SELECT * from t_lockfile";
    $resultCheckExist = mysql_query($sqlCheck, $connection);
    while ($data = mysql_fetch_assoc($resultCheckExist)) {
        $inserted = $data;
    }
    if (file_exists($filename) || !is_null($inserted['f_LockFileBy'])) {
        $return['exists'] = true;
    } else {
        $return['exists'] = false;
    }
    $return['details'] = $inserted;
    return $return;
}

function CreateLockFile() {
    global $connection;
    session_start();
    $filename = $_SERVER['DOCUMENT_ROOT'] . "/tmp/lockfile.txt";
    $lockFileBy = array();
    $lockFile['Firstname'] = $_SESSION['FirstName'];
    $lockFile['Lastname'] = $_SESSION['Lastname'];
    $lockFile['UserName'] = $_SESSION['UserName'];
    $lockFile['UserRoleID'] = $_SESSION['UserRoleID'];
    $lockFile['LoginID'] = $_SESSION['LoginID'];
    $dt = new DateTime();

    $content = "File Created: {$lockFile['UserName']} <br> DateTime: {$dt->format('Y-m-d H:i:s')}";
    $fp = fopen($filename, "w");
    fwrite($fp, $content);
    fclose($fp);

    $sqlInsert = "INSERT INTO t_lockfile (f_LockFileBy) VALUES ('{$lockFile['UserName']}')";
    $resultsqlInsert = mysql_query($sqlInsert, $connection);

    if (($resultsqlInsert && mysql_affected_rows() >= 1) && file_exists($filename)) {
        $return = "Lock File successfully created";
    } else {
        $return = $sqlInsert;
    }
    return $return;
}

function DeleteLockFile() {
    global $connection;

    $checkLockFile = CheckLockFile();
    $lockFile['UserName'] = $_SESSION['UserName'];
    $filename = $_SERVER['DOCUMENT_ROOT'] . "/tmp/lockfile.txt";
    $roleID = $_SESSION['UserRoleID'];
    if ($roleID == 1 || ($checkLockFile['details']['f_LockFileBy'] == $_SESSION['UserName'])) {
        $sqlDelete = "DELETE FROM t_lockfile ";
        $resultsqlInsert = mysql_query($sqlDelete, $connection);
        unlink($filename);
    }
}
