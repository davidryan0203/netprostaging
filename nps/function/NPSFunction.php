<?php

include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
session_start();

 
//Function for getting MyNPS Profile Details
function getEmployeeDetails($empID = NULL) {
    global $connection;
    $EmpDetails = array();
    if (!is_null($empID) && $empID != 'ALL') {
        $sqlEmp = "WHERE a.f_EmpID = {$empID} ";
    } else {
        $sqlEmp = " ";
    }

    $sqlEmpDetails = "SELECT a.f_EmpID,concat(a.f_EmpFirstName, ' ', a.f_EmpLastName) as EmpName ,c.f_EmpPNumber
            FROM t_emplist a
			LEFT JOIN t_empnumber c on a.f_EmpID = c.f_EmpID
        {$sqlEmp}";

    $resultEmpDetails = mysql_query($sqlEmpDetails, $connection);

    while ($rowEmpDetails = mysql_fetch_assoc($resultEmpDetails)) {
        $EmpDetails[$rowEmpDetails['f_EmpPNumber']][] = $rowEmpDetails;
    }

    return $EmpDetails;
}

//Function for Computing Store Interaction and Episode Scores
function getStoreNps($shared, $profileID, $empID, $to, $from = NULL, $role = NULL, $storeType = NULL) {
    global $connection;

    $role = $_SESSION['UserRoleID'];


    date_default_timezone_set('Australia/Melbourne');
    if (!is_null($to) && !is_null($from)) {

        $to = date("Y-m-d", strtotime($to));
        $from = date("Y-m-d", strtotime($from));
        $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
    } else {
        $sqlWhen = " AND MONTH(a.f_DateTimeResponseMelbourne) = MONTH(CURDATE())
                     AND YEAR(a.f_DateTimeResponseMelbourne) = YEAR(CURDATE())";
    }
    $sqlStoreType = " ";
    if (!is_null($storeType) && $storeType != "") {
        $sqlStoreType = " AND c.f_StoreTypeID = '{$storeType}'";
    }
    switch ($role) {
        case '1':
            $sqlShared = " ";
            break;
        case '2':
        case '3':
            if ($shared == "no") {

                $selectCollab = "SELECT f_StoreCollabWithUserID as 'CollabWith' FROM t_storecollab WHERE f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}'";
                $resultCollab = mysql_query($selectCollab, $connection);
                while ($row = mysql_fetch_assoc($resultCollab)) {
                    $CollabWith[] = $row['CollabWith'];
                }
                if (count($CollabWith) >= 1) {
                    array_push($CollabWith, $_SESSION['UserProfileID']);
                    $whereInCollab = implode(', ', $CollabWith);
                    $sqlShared = " AND c.f_StoreOwnerUserID in ({$whereInCollab})";
                } else {
                    $sqlShared = " AND c.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
                }
            } else {
                $sqlShared = " ";
            }
            break;
        case '4':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $data = $row['f_StoreOwnerUserID'];
            }
            $sqlShared = " AND c.f_StoreOwnerUserID = '{$data}' ";
            break;
        case '5':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $OwnerID = $row['f_StoreOwnerUserID'];
            }

            if ($shared != "no") {
                $sqlgetPartners = "select a.f_StoreOwnerUserID FROM t_storelist a
            LEFT JOIN t_sharedstore b on a.f_StoreID = b.f_StoreID
            WHERE b.f_ShareToUserID = '{$OwnerID}' group by a.f_StoreOwnerUserID";
                $result = mysql_query($sqlgetPartners, $connection);
                while ($row = mysql_fetch_assoc($result)) {
                    $storePartners[] = $row['f_StoreOwnerUserID'];
                }
                foreach ($storePartners as $data) {
                    $quotedString .= "'$data',";
                }
                $quotedString .= "'{$_SESSION['UserProfileID']}',";
                $quotedString = trim($quotedString, ",");
// var_dump($storePartners);
                $sqlShared = " ";
            } else {
                $sqlShared = " AND c.f_StoreOwnerUserID = '{$OwnerID}' ";
            }
            break;
        case '6':
            $sqlShared = " AND c.f_StoreOwnerUserID = '{$profileID}' ";
            break;
    }

//  echo $sqlWhen;
    $sqlStoreNps = "SELECT DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%m-%d-%Y') as Date , c.f_StoreID,
        CASE
            WHEN c.f_StoreListName IS NULL
            THEN a.f_DealerCode ELSE c.f_StoreListName
        END AS StoreName,
        SUM(CASE
          WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '9' and '10')
          AND (a.f_ActivationExtra5 != 'P000000') THEN  1
          ELSE 0
        END) AS InteractionTotalAdvocate,
        SUM(CASE
            WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '7' and '8')
            AND (a.f_ActivationExtra5 != 'P000000') THEN  1
            ELSE 0
        END) AS InteractionTotalPassive,
        SUM(CASE
            WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '0' and '6')
            AND (a.f_ActivationExtra5 != 'P000000') THEN  1
            ELSE 0
        END) AS InteractionTotalDetractors,
        SUM(CASE
            WHEN a.f_LikelihoodToRecommendRetail between 0 and 10
            AND  a.f_ActivationExtra5 != 'P000000' THEN 1  else 0
        END) as InteractionTotalSurvey,
        SUM(CASE
            WHEN a.f_LikelihoodToRecommendRetail is null
            AND  a.f_ActivationExtra5 != 'P000000' THEN 1  else 0
        END) as InteractionNoScore,
        SUM(CASE
          WHEN a.f_RecommendRate BETWEEN '9' and '10' THEN  1
          ELSE 0
        END) AS EpisodeTotalAdvocate,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '7' and '8' THEN  1
            ELSE 0
        END) AS EpisodeTotalPassive,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '0' and '6' THEN  1
            ELSE 0
        END) AS EpisodeTotalDetractors,
        SUM(CASE
            WHEN a.f_RecommendRate between 0 and 10 THEN 1  else 0
        END) as EpisodeTotalSurvey,
        SUM(CASE
            WHEN (a.f_RecommendRate is NULL or a.f_RecommendRate = '') THEN 1  else 0
        END) as EpisodeNoScore,
        SUM(CASE
            WHEN a.f_ActivationExtra5 = 'P000000' THEN 1 else 0 end) as PZero,
        SUM(CASE
            WHEN a.f_ActivationExtra5 ='' THEN 1 else 0
        END) as UnAssigned,
        COUNT(a.f_SurveySDID) as RawTotal
        FROM t_surveysd a
        LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c on a.f_StoreID = c.f_StoreID
        WHERE 1 = 1 AND c.f_Status = 1 {$sqlShared} {$sqlWhen} {$sqlStoreType}
        GROUP BY c.f_StoreID";
    $resultStoreNps = mysql_query($sqlStoreNps, $connection);

//echo "<pre>"; echo $sqlStoreNps;

    $i = 0;
    while ($rowStoreNps = mysql_fetch_assoc($resultStoreNps)) {

        $InteractionData[$i] = $rowStoreNps;
        $StoreNpsAdvocatePercentage = $rowStoreNps['InteractionTotalAdvocate'] / $rowStoreNps['InteractionTotalSurvey'] * 100;
        $InteractionData[$i]['InteractionAdvocatesPercentage'] = $StoreNpsAdvocatePercentage;
        $StoreNpsPassivesPercentage = $rowStoreNps['InteractionTotalPassive'] / $rowStoreNps['InteractionTotalSurvey'] * 100;
        $InteractionData[$i]['InteractionPassivesPercentage'] = $StoreNpsPassivesPercentage;
        $StoreNpsDetractorsPercentage = $rowStoreNps['InteractionTotalDetractors'] / $rowStoreNps['InteractionTotalSurvey'] * 100;
        $InteractionData[$i]['InteractionDetractorsPercentage'] = $StoreNpsDetractorsPercentage;
        $InteractionData[$i]['InteractionNpsScore'] = number_format($StoreNpsAdvocatePercentage - $StoreNpsDetractorsPercentage, npsDecimal());
        $InteractionData[$i]['blanks'] = $rowStoreNps['blanks'];
        $InteractionData[$i]['PZero'] = $rowStoreNps['PZero'];


        if ($storeType == 1) {
            if ($InteractionData[$i]['InteractionNpsScore'] >= 80) {
                $InteractionData[$i]['InteractionTierLevel'] = 'Tier 1';
            } elseif ($InteractionData[$i]['InteractionNpsScore'] >= 75 AND $InteractionData[$i]['InteractionNpsScore'] <= 79) {
                $InteractionData[$i]['InteractionTierLevel'] = 'Tier 2';
            } elseif ($InteractionData[$i]['InteractionNpsScore'] >= 68 AND $InteractionData[$i]['InteractionNpsScore'] <= 74) {
                $InteractionData[$i]['InteractionTierLevel'] = 'Tier 3';
            } elseif ($InteractionData[$i]['InteractionNpsScore'] >= 58 AND $InteractionData[$i]['InteractionNpsScore'] <= 67) {
                $InteractionData[$i]['InteractionTierLevel'] = 'Tier 4';
            } elseif ($InteractionData[$i]['InteractionNpsScore'] < 58) {
                $InteractionData[$i]['InteractionTierLevel'] = 'Tier 5';
            }
        } elseif ($storeType == 2) {
            if ($InteractionData[$i]['InteractionTierLevel'] >= 38) {
                $InteractionData[$i]['InteractionTierLevel'] = 'Qualified';
            } elseif ($InteractionData[$i]['InteractionNpsScore'] < 38) {
                $InteractionData[$i]['InteractionTierLevel'] = 'Not Qualified';
            }
        }

        $EpisodeData[$i] = $rowStoreNps;
        $EpisodeAdvocatePercentage = intval($rowStoreNps['EpisodeTotalAdvocate']) / intval($rowStoreNps['EpisodeTotalSurvey']) * 100;
        $EpisodeData[$i]['EpisodeAdvocatesPercentage'] = $EpisodeAdvocatePercentage;
        $EpisodePassivesPercentage = $rowStoreNps['EpisodeTotalPassive'] / $rowStoreNps['EpisodeTotalSurvey'] * 100;
        $EpisodeData[$i]['EpisodePassivesPercentage'] = $EpisodePassivesPercentage;
        $EpisodeDetractorsPercentage = $rowStoreNps['EpisodeTotalDetractors'] / $rowStoreNps['EpisodeTotalSurvey'] * 100;
        $EpisodeData[$i]['EpisodeDetractorsPercentage'] = $EpisodeDetractorsPercentage;
        $EpisodeData[$i]['EpisodeNpsScore'] = number_format($EpisodeAdvocatePercentage - $EpisodeDetractorsPercentage, npsDecimal());

        if ($EpisodeData[$i]['EpisodeNpsScore'] >= 50) {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 1';
        } elseif ($EpisodeData[$i]['EpisodeNpsScore'] >= 40 AND $EpisodeData[$i]['EpisodeNpsScore'] <= 49) {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 2';
        } elseif ($EpisodeData[$i]['EpisodeNpsScore'] >= 30 AND $EpisodeData[$i]['EpisodeNpsScore'] <= 39) {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 3';
        } elseif ($EpisodeData[$i]['EpisodeNpsScore'] >= 0 AND $EpisodeData[$i]['EpisodeNpsScore'] <= 29) {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 4';
        } else {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 4';
        }

        $MergeData[$i] = array('StoreName' => $rowStoreNps['StoreName'],
            'EpisodeNPS' => $EpisodeData[$i]['EpisodeNpsScore'],
            'EpisodeTier' => $EpisodeData[$i]['EpisodeTierLevel'],
            'Advocates' => $EpisodeData[$i]['EpisodeTotalAdvocate'],
            'Pasives' => $EpisodeData[$i]['EpisodeTotalPassive'],
            'Detractors' => $EpisodeData[$i]['EpisodeTotalDetractors'],
            'AdvocatesPercentage' => $EpisodeData[$i]['EpisodeAdvocatesPercentage'],
            'PasivesPercentage' => $EpisodeData[$i]['EpisodePassivesPercentage'],
            'DetractorsPercentage' => $EpisodeData[$i]['EpisodeDetractorsPercentage'],
            'TotalSurvey' => $EpisodeData[$i]['EpisodeTotalSurvey'],
            'InteractionNPS' => $InteractionData[$i]['InteractionNpsScore'],
            'InteractionTier' => $InteractionData[$i]['InteractionTierLevel'],
            'InteractionAdvocates' => $InteractionData[$i]['InteractionTotalAdvocate'],
            'InteractionPasives' => $InteractionData[$i]['InteractionTotalPassive'],
            'InteractionDetractors' => $InteractionData[$i]['InteractionTotalDetractors'],
            'InteractionAdvocatesPercentage' => $InteractionData[$i]['InteractionAdvocatesPercentage'],
            'InteractionPasivesPercentage' => $InteractionData[$i]['InteractionPassivesPercentage'],
            'InteractionDetractorsPercentage' => $InteractionData[$i]['InteractionDetractorsPercentage'],
            'BlankSurveys' => $InteractionData[$i]['blanks'],
            'P000000' => $InteractionData[$i]['PZero'],
            'InteractionTotalSurvey' => $InteractionData[$i]['InteractionTotalSurvey'],
            'RawTotal' => $rowStoreNps['RawTotal']
        );
        $i++;
    }

    foreach ($InteractionData as $sort) {
        $toSortNpsScoreInteraction[] = $sort['InteractionNpsScore'];
    }

    foreach ($EpisodeData as $sort) {
        $toSortNpsScoreEpisode[] = $sort['EpisodeNpsScore'];
    }

    foreach ($MergeData as $sort) {
        $toSortInteraction[] = $sort['InteractionNPS'];
        $toSortEpisode[] = $sort['EpisodeNPS'];
    }

    array_multisort($toSortNpsScoreInteraction, SORT_DESC, $InteractionData);
    array_multisort($toSortNpsScoreEpisode, SORT_DESC, $EpisodeData);
    array_multisort($toSortInteraction, SORT_DESC, $toSortEpisode, SORT_DESC, $MergeData);

    $returnData = array('Interaction' => $InteractionData, 'Episode' => $EpisodeData, 'Merged' => $MergeData);

// var_dump($rowStoreNps);
    return $returnData;
}

function checkStoreTypes() {
    global $connection;
    $role = $_SESSION['UserRoleID'];
    $returnData = false;
    switch ($role) {
        case '1':
        case '2':
            $returnData = true;
            break;
        case '3':
            $sqlGetStores = "SELECT count(*) as StoreTypesCount FROM t_storelist where f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' and f_StoreTypeID != '1'";
            $result = mysql_query($sqlGetStores, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $storeTypesCount = $row['StoreTypesCount'];
            }
            if ($storeTypesCount > 0) {
                $returnData = true;
            }
            break;
        case '4':
        case '5':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $ownerID = $row['f_StoreOwnerUserID'];
            }
            $sqlGetStores = "SELECT count(*) as StoreTypesCount FROM t_storelist where f_StoreOwnerUserID = '{$ownerID}' and f_StoreTypeID != '1'";
            $resultStores = mysql_query($sqlGetStores, $connection);
            while ($row = mysql_fetch_assoc($resultStores)) {
                $storeTypesCount = $row['StoreTypesCount'];
            }
            if ($storeTypesCount > 0) {
                $returnData = true;
            }
//$sqlShared = " AND c.f_StoreOwnerUserID = '{$data}' ";
            break;
    }
    return $returnData;
}

function checkStoreTypesLocal() {
    global $connection;
    $role = $_SESSION['UserRoleID'];
    $returnData = false;
    switch ($role) {
        case '1':
        case '2':
        case '3':
            $sqlGetStores = "SELECT count(*) as StoreTypesCount FROM t_storelist where f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' and f_StoreTypeID != '1'";
            $result = mysql_query($sqlGetStores, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $storeTypesCount = $row['StoreTypesCount'];
            }
            if ($storeTypesCount > 0) {
                $returnData = true;
            }
            break;
        case '4':
        case '5':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $ownerID = $row['f_StoreOwnerUserID'];
            }
            $sqlGetStores = "SELECT count(*) as StoreTypesCount FROM t_storelist where f_StoreOwnerUserID = '{$ownerID}' and f_StoreTypeID != '1'";
            $resultStores = mysql_query($sqlGetStores, $connection);
            while ($row = mysql_fetch_assoc($resultStores)) {
                $storeTypesCount = $row['StoreTypesCount'];
            }
            if ($storeTypesCount > 0) {
                $returnData = true;
            }
//$sqlShared = " AND c.f_StoreOwnerUserID = '{$data}' ";
            break;
    }
    return $returnData;
}

function getStoreNpsTBC($shared, $profileID, $empID, $to, $from, $role, $storeType = 2, $rangeType = NULL, $tierSort = 'Episode') {
    global $connection;

    date_default_timezone_set('Australia/Melbourne');
    $sqlReportType = " ";

    $sqlReportMobile = " AND b.f_ProductID in ('1','2')";
    $sqlReportFixed = " AND b.f_ProductID in ('5','6','7','8','9','10','11','12','13','16','17','18','19','20')";
    $sqlReportInteraction = " ";
    $sqlReportEpisode = " ";

    $lastDate = date('Y-m-t', strtotime($to));
//        echo  date('Y-m-01', strtotime("$lastData + 2  months ago"));
//        echo date('Y-m-01', strtotime("$lastData + 5  months ago"));

    if ($rangeType == 'mtd') {
        $sqlWhenInteraction = " AND a.f_DateTimeResponseMelbourne between   '{$from} 00:00:00' AND '{$to} 23:59:59'";
        $sqlWhenMobile = " AND a.f_DateTimeResponseMelbourne between   '{$from} 00:00:00' AND '{$to} 23:59:59'";
        $sqlWhenFixed = " AND a.f_DateTimeResponseMelbourne between  '{$from} 00:00:00' AND '{$to} 23:59:59'";
        $sqlWhenEpisode = " AND a.f_DateTimeResponseMelbourne between  '{$from} 00:00:00' AND '{$to} 23:59:59'";
    } else {
        if (!is_null($to) || !is_null($from)) {
//echo $to;

            $lastDate = date('Y-m-t', strtotime($to));
//
//            // $firstday = date("Y-m-01", strtotime($to));
//            $mobileday = date('Y-m-01', strtotime("$lastData + 2  months ago"));
//            $fixedday = date('Y-m-01', strtotime("$lastData + 5  months ago"));
//            $sqlWhenInteraction = " AND a.f_DateTimeResponseMelbourne between   '{$mobileday} 00:00:00' AND '{$lastData} 23:59:59'";
//            $sqlWhenMobile = " AND a.f_DateTimeResponseMelbourne between   '{$mobileday} 00:00:00' AND '{$lastData} 23:59:59'";
//            $sqlWhenFixed = " AND a.f_DateTimeResponseMelbourne between  '{$fixedday} 00:00:00' AND '{$lastData} 23:59:59'";
            $sqlWhenInteraction = " AND a.f_DateTimeResponseMelbourne BETWEEN CONCAT(DATE_FORMAT(DATE_SUB('{$lastDate}', INTERVAL 2 MONTH) ,'%Y-%m-01'),' ','00:00:00') AND '{$lastDate} 23:59:59'";
            $sqlWhenEpisode = " AND a.f_DateTimeResponseMelbourne BETWEEN CONCAT(DATE_FORMAT(DATE_SUB('{$lastDate}', INTERVAL 2 MONTH) ,'%Y-%m-01'),' ','00:00:00') AND '{$lastDate} 23:59:59'";
            $sqlWhenMobile = " AND a.f_DateTimeResponseMelbourne BETWEEN CONCAT(DATE_FORMAT(DATE_SUB('{$lastDate}', INTERVAL 2 MONTH) ,'%Y-%m-01'),' ','00:00:00') AND '{$lastDate} 23:59:59'";
            $sqlWhenFixed = " AND a.f_DateTimeResponseMelbourne BETWEEN CONCAT(DATE_FORMAT(DATE_SUB('{$lastDate}', INTERVAL 5 MONTH) ,'%Y-%m-01'),' ','00:00:00') AND'{$lastDate} 23:59:59'";
        } else {
            $sqlWhenMobile = " AND a.f_DateTimeResponseMelbourne BETWEEN DATE_SUB(LAST_DAY(CURDATE()), INTERVAL 3 MONTH) AND LAST_DAY(CURDATE())";
            $sqlWhenEpisode = " AND a.f_DateTimeResponseMelbourne BETWEEN DATE_SUB(LAST_DAY(CURDATE()), INTERVAL 3 MONTH) AND LAST_DAY(CURDATE())";
            $sqlWhenFixed = "AND a.f_DateTimeResponseMelbourne BETWEEN DATE_SUB(LAST_DAY(CURDATE()), INTERVAL 6 MONTH) AND LAST_DAY(CURDATE())";
            $sqlWhenInteraction = " AND a.f_DateTimeResponseMelbourne BETWEEN DATE_SUB(LAST_DAY(CURDATE()), INTERVAL 1 MONTH) AND LAST_DAY(CURDATE())";
        }
    }


// echo $sqlWhenInteraction;

    $role = $_SESSION['UserRoleID'];

    $sqlStoreType = " ";
    $sqlStoreType = " AND c.f_StoreTypeID = '2'";
   
    switch ($role) {
        case '1':
            $sqlShared = " ";
            break;
        case '2':
        case '3':
            if ($shared == "no") {
                $selectCollab = "SELECT f_StoreCollabWithUserID as 'CollabWith' FROM t_storecollab WHERE f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}'";
                $resultCollab = mysql_query($selectCollab, $connection);
                while ($row = mysql_fetch_assoc($resultCollab)) {
                    $CollabWith[] = $row['CollabWith'];
                }
                if (count($CollabWith) >= 1) {
                    array_push($CollabWith, $_SESSION['UserProfileID']);
                    $whereInCollab = implode(', ', $CollabWith);
                    $sqlShared = " AND c.f_StoreOwnerUserID in ({$whereInCollab})";
                } else {
                    $sqlShared = " AND c.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
                }
            } else {
                $sqlShared = " ";
            }
            break;
        case '4':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $data = $row['f_StoreOwnerUserID'];
            }
            $sqlShared = " AND c.f_StoreOwnerUserID = '{$data}' ";
            break;
        case '5':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $OwnerID = $row['f_StoreOwnerUserID'];
            }

            if ($shared != "no") {
                $sqlgetPartners = "select a.f_StoreOwnerUserID FROM t_storelist a
            LEFT JOIN t_sharedstore b on a.f_StoreID = b.f_StoreID
            WHERE b.f_ShareToUserID = '{$OwnerID}' group by a.f_StoreOwnerUserID";
                $result = mysql_query($sqlgetPartners, $connection);
                while ($row = mysql_fetch_assoc($result)) {
                    $storePartners[] = $row['f_StoreOwnerUserID'];
                }
                foreach ($storePartners as $data) {
                    $quotedString .= "'$data',";
                }
                $quotedString .= "'{$_SESSION['UserProfileID']}',";
                $quotedString = trim($quotedString, ",");
// var_dump($storePartners);
                $sqlShared = " ";
            } else {
                $sqlShared = " AND c.f_StoreOwnerUserID = '{$OwnerID}' ";
            }
            break;
        case '6':
            $sqlShared = " AND c.f_StoreOwnerUserID = '{$profileID}' ";
            break;
    }

    $sqlStoreNps = "SELECT
    c.f_StoreID, c.f_StoreListName as StoreName, w.*, x.* , z.* ,y.*
FROM
    t_surveysd a
        LEFT JOIN
    t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN
    t_storelist c ON a.f_StoreID = c.f_StoreID
        LEFT JOIN
    (SELECT
        DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%m-%d-%Y') AS InteractionDate,
            c.f_StoreID as 'InteractionStoreID',
            SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                        AND (a.f_ActivationExtra5 != 'P000000')
                THEN
                    1
                ELSE 0
            END) AS InteractionTotalAdvocate,
            SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '7' AND '8')
                        AND (a.f_ActivationExtra5 != 'P000000')
                THEN
                    1
                ELSE 0
            END) AS InteractionTotalPassive,
            SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                        AND (a.f_ActivationExtra5 != 'P000000')
                THEN
                    1
                ELSE 0
            END) AS InteractionTotalDetractors,
            SUM(CASE
                WHEN
                    a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                        AND a.f_ActivationExtra5 != 'P000000'
                THEN
                    1
                ELSE 0
            END) AS InteractionTotalSurvey,
            SUM(CASE
                WHEN
                     (a.f_LikelihoodToRecommendRetail IS NULL)
                        AND a.f_ActivationExtra5 != 'P000000'
                THEN
                    1
                ELSE 0
            END) AS InteractionNoScore,
            SUM(CASE
                WHEN a.f_ActivationExtra5 = 'P000000' THEN 1
                ELSE 0
            END) AS InteractionPZero,
            SUM(CASE
                WHEN a.f_ActivationExtra5 = '' THEN 1
                ELSE 0
            END) AS InteractionUnAssigned,
            COUNT(a.f_SurveySDID) AS InteractionRawTotal
    FROM
        t_surveysd a
    LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
    LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID
    WHERE
        1 = 1 AND c.f_Status = 1  AND c.f_StoreTypeID = '2' AND b.f_CustomerBusinessUnitID = 2
        {$sqlWhenInteraction} {$sqlReportInteraction}
    GROUP BY c.f_StoreID) AS w ON c.f_StoreID = w.InteractionStoreID
        LEFT JOIN
    (SELECT
        DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%m-%d-%Y') AS MobileTotalDate,
            c.f_StoreID as 'MobileStoreID',
            SUM(CASE
                WHEN a.f_RecommendRate BETWEEN '9' AND '10' THEN 1
                ELSE 0
            END) AS MobileTotalAdvocate,
            SUM(CASE
                WHEN a.f_RecommendRate BETWEEN '7' AND '8' THEN 1
                ELSE 0
            END) AS MobileTotalPassive,
            SUM(CASE
                WHEN a.f_RecommendRate BETWEEN '0' AND '6' THEN 1
                ELSE 0
            END) AS MobileTotalDetractors,
            SUM(CASE
                WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
                ELSE 0
            END) AS MobileTotalSurvey,
            SUM(CASE
                WHEN (a.f_RecommendRate IS NULL) THEN 1
                ELSE 0
            END) AS MobileNoScore,
            SUM(CASE
                WHEN a.f_ActivationExtra5 = 'P000000' THEN 1
                ELSE 0
            END) AS MobilePZero,
            SUM(CASE
                WHEN a.f_ActivationExtra5 = '' THEN 1
                ELSE 0
            END) AS MobileUnAssigned,
            COUNT(a.f_SurveySDID) AS MobileRawTotal
    FROM
        t_surveysd a
    LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
    LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID
    WHERE
        1 = 1 AND c.f_Status = 1 AND c.f_StoreTypeID = '2' AND b.f_CustomerBusinessUnitID = 2
             {$sqlWhenMobile} {$sqlReportMobile} {$sqlShared}
    GROUP BY c.f_StoreID) AS x ON c.f_StoreID = x.MobileStoreID
        LEFT JOIN
    (SELECT
        c.f_StoreID as 'FixedStoreID',
            DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%m-%d-%Y') AS FixedDate,
            (SUM(CASE
                WHEN a.f_RecommendRate BETWEEN '9' AND '10' THEN 1
                ELSE 0
            END)) AS FixedTotalAdvocate,
            (SUM(CASE
                WHEN a.f_RecommendRate BETWEEN '7' AND '8' THEN 1
                ELSE 0
            END)) AS FixedTotalPassive,
            (SUM(CASE
                WHEN a.f_RecommendRate BETWEEN '0' AND '6' THEN 1
                ELSE 0
            END)) AS FixedTotalDetractors,
            SUM(CASE
                WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
                ELSE 0
            END) AS FixedEpisodeTotalSurvey,
            (SUM(CASE
                WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
                ELSE 0
            END) ) AS FixedTotalSurvey,
            (SUM(CASE
                WHEN (a.f_RecommendRate IS NULL) THEN 1
                ELSE 0
            END)) AS FixedNoScore,
            SUM(CASE
                WHEN a.f_ActivationExtra5 = 'P000000' THEN 1
                ELSE 0
            END) AS FixedPZero,
            SUM(CASE
                WHEN a.f_ActivationExtra5 = '' THEN 1
                ELSE 0
            END) AS FixedUnAssigned,
            COUNT(a.f_SurveySDID) AS FixedRawTotal
    FROM
        t_surveysd a
    LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
    LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID
    WHERE
        1 = 1 AND c.f_Status = 1 AND c.f_StoreTypeID = '2' AND b.f_CustomerBusinessUnitID = 2
           {$sqlWhenFixed} {$sqlReportFixed}  {$sqlShared}
    GROUP BY c.f_StoreID) AS z ON c.f_StoreID = z.FixedStoreID
    LEFT JOIN
        (SELECT
            DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%m-%d-%Y') AS EpisodeTotalDate,
                c.f_StoreID as 'EpisodeStoreID',
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '9' AND '10' THEN 1
                    ELSE 0
                END) AS EpisodeTotalAdvocate,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '7' AND '8' THEN 1
                    ELSE 0
                END) AS EpisodeTotalPassive,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '0' AND '6' THEN 1
                    ELSE 0
                END) AS EpisodeTotalDetractors,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
                    ELSE 0
                END) AS EpisodeTotalSurvey,
                SUM(CASE
                    WHEN (a.f_RecommendRate IS NULL) THEN 1
                    ELSE 0
                END) AS EpisodeNoScore,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = 'P000000' THEN 1
                    ELSE 0
                END) AS EpisodePZero,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = '' THEN 1
                    ELSE 0
                END) AS EpisodeUnAssigned,
                COUNT(a.f_SurveySDID) AS EpisodeRawTotal
        FROM
            t_surveysd a
        LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID
        WHERE
            1 = 1 AND c.f_Status = 1 AND c.f_StoreTypeID = '2'  AND b.f_CustomerBusinessUnitID = 2
                 {$sqlWhenEpisode}  {$sqlShared}
        GROUP BY c.f_StoreID) AS y ON c.f_StoreID = y.EpisodeStoreID

WHERE
    1 = 1 AND c.f_Status = 1 AND c.f_StoreTypeID = '2' {$sqlShared}
GROUP BY c.f_StoreID";
    mysql_query("SELECT 1=1;", $connection);

//    if ($tierSort == "Episode") {
//        echo $sqlStoreNps;
//        exit;
//    }
    $resultStoreNps = mysql_query($sqlStoreNps, $connection);
//echo "<pre>"; echo $sqlStoreNps;
//exit;
    $i = 0;
    while ($rowStoreNps = mysql_fetch_assoc($resultStoreNps)) {

        $InteractionData[$i] = $rowStoreNps;
        $StoreNpsAdvocatePercentage = $rowStoreNps['InteractionTotalAdvocate'] / $rowStoreNps['InteractionTotalSurvey'] * 100;
        $InteractionData[$i]['InteractionAdvocatesPercentage'] = $StoreNpsAdvocatePercentage;
        $StoreNpsPassivesPercentage = $rowStoreNps['InteractionTotalPassive'] / $rowStoreNps['InteractionTotalSurvey'] * 100;
        $InteractionData[$i]['InteractionPassivesPercentage'] = $StoreNpsPassivesPercentage;
        $StoreNpsDetractorsPercentage = $rowStoreNps['InteractionTotalDetractors'] / $rowStoreNps['InteractionTotalSurvey'] * 100;
        $InteractionData[$i]['InteractionDetractorsPercentage'] = $StoreNpsDetractorsPercentage;
        $InteractionData[$i]['InteractionNpsScore'] = number_format($StoreNpsAdvocatePercentage - $StoreNpsDetractorsPercentage, npsDecimal());

        $InteractionData[$i]['InteractionTierLevel'] = " ";

        if ($InteractionData[$i]['InteractionNpsScore'] >= 38) {
            $InteractionData[$i]['InteractionTierLevel'] = 'Qualified';
        } elseif ($InteractionData[$i]['InteractionNpsScore'] < 38) {
            $InteractionData[$i]['InteractionTierLevel'] = 'Not Qualified';
        }

        $MobileData[$i] = $rowStoreNps;
        $MobileAdvocatePercentage = intval($rowStoreNps['MobileTotalAdvocate']) / intval($rowStoreNps['MobileTotalSurvey']) * 100;
        $MobileData[$i]['MobileAdvocatesPercentage'] = $MobileAdvocatePercentage;
        $MobilePassivesPercentage = $rowStoreNps['MobileTotalPassive'] / $rowStoreNps['MobileTotalSurvey'] * 100;
        $MobileData[$i]['MobilePassivesPercentage'] = $MobilePassivesPercentage;
        $MobileDetractorsPercentage = $rowStoreNps['MobileTotalDetractors'] / $rowStoreNps['MobileTotalSurvey'] * 100;
        $MobileData[$i]['MobileDetractorsPercentage'] = $MobileDetractorsPercentage;
        $MobileData[$i]['MobileNpsScore'] = number_format($MobileAdvocatePercentage - $MobileDetractorsPercentage, npsDecimal());

        if ($MobileData[$i]['MobileNpsScore'] >= 59) {
            $MobileData[$i]['MobileTierLevel'] = 'Tier 1';
        } elseif ($MobileData[$i]['MobileNpsScore'] >= 52 ) {
            $MobileData[$i]['MobileTierLevel'] = 'Tier 2';
        } elseif ($MobileData[$i]['MobileNpsScore'] >= 45 ) {
            $MobileData[$i]['MobileTierLevel'] = 'Tier 3';
        } elseif ($MobileData[$i]['MobileNpsScore'] >= 35 ) {
            $MobileData[$i]['MobileTierLevel'] = 'Tier 4';
        } elseif ($MobileData[$i]['MobileNpsScore'] < 35) {
            $MobileData[$i]['MobileTierLevel'] = 'Tier 5';
        } else {
            $MobileData[$i]['MobileTierLevel'] = 'Tier 5';
        }

        $FixedData[$i] = $rowStoreNps;
        $FixedAdvocatePercentage = intval($rowStoreNps['FixedTotalAdvocate']) / intval($rowStoreNps['FixedTotalSurvey']) * 100;
        $FixedData[$i]['FixedAdvocatesPercentage'] = $FixedAdvocatePercentage;
        $FixedPassivesPercentage = $rowStoreNps['FixedTotalPassive'] / $rowStoreNps['FixedTotalSurvey'] * 100;
        $FixedData[$i]['FixedPassivesPercentage'] = $FixedPassivesPercentage;
        $FixedDetractorsPercentage = $rowStoreNps['FixedTotalDetractors'] / intval($rowStoreNps['FixedTotalSurvey']) * 100;
        $FixedData[$i]['FixedDetractorsPercentage'] = $FixedDetractorsPercentage;
        $FixedData[$i]['FixedNpsScore'] = number_format($FixedAdvocatePercentage - $FixedDetractorsPercentage, npsDecimal());

        if ($FixedData[$i]['FixedNpsScore'] >= 15) {
            $FixedData[$i]['FixedTierLevel'] = 'Tier 1';
        } elseif ($FixedData[$i]['FixedNpsScore'] >= 0 ) {
            $FixedData[$i]['FixedTierLevel'] = 'Tier 2';
        } elseif ($FixedData[$i]['FixedNpsScore'] >= -13 ) {
            $FixedData[$i]['FixedTierLevel'] = 'Tier 3';
        } elseif ($FixedData[$i]['FixedNpsScore'] >= -35 ) {
            $FixedData[$i]['FixedTierLevel'] = 'Tier 4';
        } elseif ($FixedData[$i]['FixedNpsScore'] < -35) {
            $FixedData[$i]['FixedTierLevel'] = 'Tier 5';
        } else {
            $FixedData[$i]['FixedTierLevel'] = 'Tier 5';
        }

        $EpisodeData[$i] = $rowStoreNps;
        $EpisodeAdvocatePercentage = intval($rowStoreNps['EpisodeTotalAdvocate']) / intval($rowStoreNps['EpisodeTotalSurvey']) * 100;
        $EpisodeData[$i]['EpisodeAdvocatesPercentage'] = $EpisodeAdvocatePercentage;
        $EpisodePassivesPercentage = $rowStoreNps['EpisodeTotalPassive'] / $rowStoreNps['EpisodeTotalSurvey'] * 100;
        $EpisodeData[$i]['EpisodePassivesPercentage'] = $EpisodePassivesPercentage;
        $EpisodeDetractorsPercentage = $rowStoreNps['EpisodeTotalDetractors'] / $rowStoreNps['EpisodeTotalSurvey'] * 100;
        $EpisodeData[$i]['EpisodeDetractorsPercentage'] = $EpisodeDetractorsPercentage;
        $EpisodeData[$i]['EpisodeNpsScore'] = number_format($EpisodeAdvocatePercentage - $EpisodeDetractorsPercentage, npsDecimal());

        if ($EpisodeData[$i]['EpisodeNpsScore'] >= 71) {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 1';
        } elseif ($EpisodeData[$i]['EpisodeNpsScore'] >= 64 ) {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 2';
        } elseif ($EpisodeData[$i]['EpisodeNpsScore'] >= 46 ) {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 3';
        } elseif ($EpisodeData[$i]['EpisodeNpsScore'] >= 27 ) {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 4';
        } else {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 5';
        }

        switch ($tierSort) {
            case 'Interaction':
                $weight[$i] = $InteractionData[$i]['InterationNpsScore'];
                break;
            case 'Mobile':
                $weight[$i] = $MobileData[$i]['MobileNpsScore'];
                break;
            case 'Fixed':
                $weight[$i] = $FixedData[$i]['FixedNpsScore'];
                break;
            case 'Episode':
                $weight[$i] = $EpisodeData[$i]['EpisodeNpsScore'];  
                break;
            default:
                $weight[$i] = ($MobileData[$i]['MobileNpsScore'] - $FixedData[$i]['FixedNpsScore']) / 2;
                break;
        }



        $MergeData[$i] = array('StoreName' => $rowStoreNps['StoreName'],
            'InteractionNPS' => $InteractionData[$i]['InteractionNpsScore'],
            'InteractionTier' => $InteractionData[$i]['InteractionTierLevel'],
            'InteractionAdvocates' => $InteractionData[$i]['InteractionTotalAdvocate'],
            'InteractionPasives' => $InteractionData[$i]['InteractionTotalPassive'],
            'InteractionDetractors' => $InteractionData[$i]['InteractionTotalDetractors'],
            'InteractionAdvocatesPercentage' => $InteractionData[$i]['InteractionAdvocatesPercentage'],
            'InteractionPasivesPercentage' => $InteractionData[$i]['InteractionPassivesPercentage'],
            'InteractionDetractorsPercentage' => $InteractionData[$i]['InteractionDetractorsPercentage'],
            'InteractionTotalSurvey' => $InteractionData[$i]['InteractionTotalSurvey'],
            'MobileNPS' => $MobileData[$i]['MobileNpsScore'],
            'MobileTier' => $MobileData[$i]['MobileTierLevel'],
            'MobileAdvocates' => $MobileData[$i]['MobileTotalAdvocate'],
            'MobilePasives' => $MobileData[$i]['MobileTotalPassive'],
            'MobileDetractors' => $MobileData[$i]['MobileTotalDetractors'],
            'MobileAdvocatesPercentage' => $MobileData[$i]['MobileAdvocatesPercentage'],
            'MobilePasivesPercentage' => $MobileData[$i]['MobilePassivesPercentage'],
            'MobileDetractorsPercentage' => $MobileData[$i]['MobileDetractorsPercentage'],
            'MobileTotalSurvey' => $MobileData[$i]['MobileTotalSurvey'],
            'FixedNPS' => $FixedData[$i]['FixedNpsScore'],
            'FixedTier' => $FixedData[$i]['FixedTierLevel'],
            'FixedAdvocates' => $FixedData[$i]['FixedTotalAdvocate'],
            'FixedPasives' => $FixedData[$i]['FixedTotalPassive'],
            'FixedDetractors' => $FixedData[$i]['FixedTotalDetractors'],
            'FixedAdvocatesPercentage' => $FixedData[$i]['FixedAdvocatesPercentage'],
            'FixedPasivesPercentage' => $FixedData[$i]['FixedPassivesPercentage'],
            'FixedDetractorsPercentage' => $FixedData[$i]['FixedDetractorsPercentage'],
            'FixedTotalSurvey' => $FixedData[$i]['FixedTotalSurvey'],
            'EpisodeNPS' => $EpisodeData[$i]['EpisodeNpsScore'],
            'EpisodeTier' => $EpisodeData[$i]['EpisodeTierLevel'],
            'EpisodeAdvocates' => $EpisodeData[$i]['EpisodeTotalAdvocate'],
            'EpisodePasives' => $EpisodeData[$i]['EpisodeTotalPassive'],
            'EpisodeDetractors' => $EpisodeData[$i]['EpisodeTotalDetractors'],
            'EpisodeAdvocatesPercentage' => $EpisodeData[$i]['EpisodeAdvocatesPercentage'],
            'EpisodePasivesPercentage' => $EpisodeData[$i]['EpisodePassivesPercentage'],
            'EpisodeDetractorsPercentage' => $EpisodeData[$i]['EpisodeDetractorsPercentage'],
            'EpisodeTotalSurvey' => $EpisodeData[$i]['EpisodeTotalSurvey'],
            'weight' => $weight[$i]
        );
        $i++;
    }

    foreach ($InteractionData as $sort) {
        $toSortNpsScoreInteraction[] = $sort['InteractionNpsScore'];
    }

    foreach ($MobileData as $sort) {
        $toSortNpsScoreMobile[] = $sort['MobileNpsScore'];
    }

    foreach ($FixedData as $sort) {
        $toSortNpsScoreFixed[] = $sort['FixedNpsScore'];
    }

    foreach ($EpisodeData as $sort) {
        $toSortNpsScoreEpisode[] = $sort['EpisodeNpsScore'];
    }

    foreach ($MergeData as $sort) {
        $toWeight[] = $sort['weight'];
    }

    array_multisort($toSortNpsScoreInteraction, SORT_DESC, $InteractionData);
    array_multisort($toSortNpsScoreMobile, SORT_DESC, $MobileData);
    array_multisort($toSortNpsScoreFixed, SORT_DESC, $FixedData);
    array_multisort($toSortNpsScoreEpisode, SORT_DESC, $EpisodeData);
    array_multisort($toWeight, SORT_DESC, $MergeData);
//  array_multisort($toSortNpsScoreInteraction, SORT_DESC, $toSortNpsScoreMobile, SORT_DESC, $FixedData, SORT_ASC, $MergeData);

    $returnData = array('Interaction' => $InteractionData, 'Mobile' => $MobileData, 'Fixed' => $FixedData, 'Merged' => $MergeData, 'Episode' => $EpisodeData);

// var_dump($rowStoreNps);
    return $returnData;
}

function getStoreSurveyNps($shared, $profileID, $empID, $to, $from, $role = NULL, $storeType = NULL, $surveyType = NULL, $subsurveyType = NULL, $storeID = NULL) {
    global $connection;

    $role = $_SESSION['UserRoleID'];
    $groupby = array();

    if ($storeID == 'AllStores') {
        $storeIDsql = " ";
        $groupby[] = "c.f_StoreID";
    } elseif ($storeID == 'Merged') {
        $storeIDsql = " ";
    } else {
        $storeIDsql = " AND a.f_StoreID = '{$storeID}'";
    }


    $surveyTypesql = " ";
    $subsurveyTypesql = " ";
    $subSurveyGroup = " ";
    if (!is_null($surveyType)) {
        if ($surveyType == 'AllType') {
            $subSurveyGroup = ",b.f_SubSurveyID ";
        } else {
            $surveyTypesql = " AND b.f_SurveyID = '{$surveyType}' ";
        }
    }

    if (!is_null($subsurveyType)) {
        if ($subsurveyType == 'AllSub') {
            $groupby[] = "b.f_SubSurveyID";
        } else {
            $subsurveyTypesql = " AND b.f_SubSurveyID = '{$surveyType}' ";
        }
    }

    $groupto = implode(", ", $groupby);

    date_default_timezone_set('Australia/Melbourne');
    if (!is_null($to) && !is_null($from)) {
        $to = date("Y-m-d", strtotime($to));
        $from = date("Y-m-d", strtotime($from));
        $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
    } else {
        $sqlWhen = " AND MONTH(a.f_DateTimeResponseMelbourne) = MONTH(CURDATE())
                     AND YEAR(a.f_DateTimeResponseMelbourne) = YEAR(CURDATE())";
    }
    $sqlStoreType = " ";
    if (!is_null($storeType) && $storeType != "") {
        $sqlStoreType = " AND c.f_StoreTypeID = '{$storeType}'";
    }
    switch ($role) {
        case '1':
            $sqlShared = " ";
            break;
        case '2':
        case '3':
            if ($shared == "no") {
                $selectCollab = "SELECT f_StoreCollabWithUserID as 'CollabWith' FROM t_storecollab WHERE f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}'";
                $resultCollab = mysql_query($selectCollab, $connection);
                while ($row = mysql_fetch_assoc($resultCollab)) {
                    $CollabWith[] = $row['CollabWith'];
                }
                if (count($CollabWith) >= 1) {
                    array_push($CollabWith, $_SESSION['UserProfileID']);
                    $whereInCollab = implode(', ', $CollabWith);
                    $sqlShared = " AND c.f_StoreOwnerUserID in ({$whereInCollab})";
                } else {
                    $sqlShared = " AND c.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
                }
            } else {
                $sqlShared = " ";
            }
            break;
        case '4':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $data = $row['f_StoreOwnerUserID'];
            }
            $sqlShared = " AND c.f_StoreOwnerUserID = '{$data}' ";
            break;
        case '5':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $OwnerID = $row['f_StoreOwnerUserID'];
            }

            if ($shared != "no") {
                $sqlgetPartners = "select a.f_StoreOwnerUserID FROM t_storelist a
            LEFT JOIN t_sharedstore b on a.f_StoreID = b.f_StoreID
            WHERE b.f_ShareToUserID = '{$OwnerID}' group by a.f_StoreOwnerUserID";
                $result = mysql_query($sqlgetPartners, $connection);
                while ($row = mysql_fetch_assoc($result)) {
                    $storePartners[] = $row['f_StoreOwnerUserID'];
                }
                foreach ($storePartners as $data) {
                    $quotedString .= "'$data',";
                }
                $quotedString .= "'{$_SESSION['UserProfileID']}',";
                $quotedString = trim($quotedString, ",");
// var_dump($storePartners);
                $sqlShared = " ";
            } else {
                $sqlShared = " AND c.f_StoreOwnerUserID = '{$OwnerID}' ";
            }
            break;
    }

//  echo $sqlWhen;
    $sqlStoreNps = "SELECT  d.f_SurveyName as SurveyName, d.f_SurveyListID  ,
        e.f_SubSurveyName as SubSurveyName, e.f_SubSurveyID ,
        DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%m-%d-%Y') as Date , c.f_StoreID,
        CASE
            WHEN c.f_StoreListName IS NULL
            THEN a.f_DealerCode ELSE c.f_StoreListName
        END AS StoreName,
        SUM(CASE
          WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '9' and '10')
          AND (a.f_ActivationExtra5 != 'P000000') THEN  1
          ELSE 0
        END) AS InteractionTotalAdvocate,
        SUM(CASE
            WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '7' and '8')
            AND (a.f_ActivationExtra5 != 'P000000') THEN  1
            ELSE 0
        END) AS InteractionTotalPassive,
        SUM(CASE
            WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '0' and '6')
            AND (a.f_ActivationExtra5 != 'P000000') THEN  1
            ELSE 0
        END) AS InteractionTotalDetractors,
        SUM(CASE
            WHEN a.f_LikelihoodToRecommendRetail between 0 and 10
            AND  a.f_ActivationExtra5 != 'P000000' THEN 1  else 0
        END) as InteractionTotalSurvey,
        SUM(CASE
            WHEN a.f_LikelihoodToRecommendRetail is null
            AND  a.f_ActivationExtra5 != 'P000000' THEN 1  else 0
        END) as InteractionNoScore,
        SUM(CASE
          WHEN a.f_RecommendRate BETWEEN '9' and '10' THEN  1
          ELSE 0
        END) AS EpisodeTotalAdvocate,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '7' and '8' THEN  1
            ELSE 0
        END) AS EpisodeTotalPassive,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '0' and '6' THEN  1
            ELSE 0
        END) AS EpisodeTotalDetractors,
        SUM(CASE
            WHEN a.f_RecommendRate between 0 and 10 THEN 1  else 0
        END) as EpisodeTotalSurvey,
        SUM(CASE
            WHEN (a.f_RecommendRate is NULL or a.f_RecommendRate = '') THEN 1  else 0
        END) as EpisodeNoScore,
        SUM(CASE
            WHEN a.f_ActivationExtra5 = 'P000000' THEN 1 else 0 end) as PZero,
        SUM(CASE
            WHEN a.f_ActivationExtra5 ='' THEN 1 else 0
        END) as UnAssigned,
        COUNT(a.f_SurveySDID) as RawTotal ,   w.IntTotalAdvo, w.IntTotalPas, w.IntTotalDet, w.IntTotalSurvey , w.EpiTotalAdvo , w.EpiTotalPas, w.EpiTotalDet , w.EpiTotalSurvey ,  w.InterationNPS, w.EpisodeNPS
        FROM t_surveysd a
        left join t_surveypd b on a.f_SurveyPDID =  b.f_SurveyPDID
        LEFT JOIN t_storelist c on a.f_StoreID = c.f_StoreID
        left join t_surveylist d on b.f_SurveyID = d.f_SurveyListID
        left join t_subsurveylist e on b.f_SubSurveyID = e.f_SubSurveyID
           LEFT JOIN
        (SELECT
            SUM(CASE
                    WHEN
                        (a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                            AND (a.f_ActivationExtra5 != 'P000000')
                    THEN
                        1
                    ELSE 0
                END) AS IntTotalAdvo,
                SUM(CASE
                    WHEN
                        (a.f_LikelihoodToRecommendRetail BETWEEN '7' AND '8')
                            AND (a.f_ActivationExtra5 != 'P000000')
                    THEN
                        1
                    ELSE 0
                END) AS IntTotalPas,
                SUM(CASE
                    WHEN
                        (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                            AND (a.f_ActivationExtra5 != 'P000000')
                    THEN
                        1
                    ELSE 0
                END) AS IntTotalDet,
                SUM(CASE
                    WHEN
                        a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                            AND a.f_ActivationExtra5 != 'P000000'
                    THEN
                        1
                    ELSE 0
                END) AS IntTotalSurvey,
                SUM(CASE
                    WHEN
                        a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
                    ELSE 0
                END) AS EpiTotalSurvey,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '9' AND '10' THEN 1
                    ELSE 0
                END) AS EpiTotalAdvo,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '7' AND '8' THEN 1
                    ELSE 0
                END) AS EpiTotalPas,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '0' AND '6' THEN 1
                    ELSE 0
                END) AS EpiTotalDet,
                ((SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '9' AND '10' THEN 1
                    ELSE 0
                END) - SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '0' AND '6' THEN 1
                    ELSE 0
                END)) * 100 / SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
                    ELSE 0
                END)) AS EpisodeNPS,
                ((SUM(CASE
                    WHEN
                        (a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                            AND a.f_ActivationExtra5 != 'P000000'
                    THEN
                        1
                    ELSE 0
                END) - SUM(CASE
                    WHEN
                        (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                            AND a.f_ActivationExtra5 != 'P000000'
                    THEN
                        1
                    ELSE 0
                END)) * 100 / SUM(CASE
                    WHEN
                        a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                            AND a.f_ActivationExtra5 != 'P000000'
                    THEN
                        1
                    ELSE 0
                END)) AS InterationNPS,
                c.f_StoreID
    FROM
        t_surveysd a
    LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
    LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID
    WHERE
        1 = 1 AND c.f_Status = 1
           {$sqlShared} {$sqlWhen} {$sqlStoreType} {$storeIDsql}
    GROUP BY c.f_StoreID) AS w ON c.f_StoreID = w.f_StoreID
        WHERE 1 = 1 AND c.f_Status = 1 {$sqlShared} {$sqlWhen} {$sqlStoreType} {$storeIDsql} {$surveyTypesql} {$subsurveyTypesql}
        GROUP BY $groupto
        order by c.f_StoreID,b.f_SurveyID";
    $resultStoreNps = mysql_query($sqlStoreNps, $connection);

//echo "<pre>";
//echo $sqlStoreNps;

    $i = 0;
    while ($rowStoreNps = mysql_fetch_assoc($resultStoreNps)) {
        $InteractionData[$i] = $rowStoreNps;
        $StoreNpsAdvocatePercentage = $rowStoreNps['InteractionTotalAdvocate'] / $rowStoreNps['InteractionTotalSurvey'] * 100;
        $InteractionData[$i]['InteractionAdvocatesPercentage'] = $StoreNpsAdvocatePercentage;
        $StoreNpsPassivesPercentage = $rowStoreNps['InteractionTotalPassive'] / $rowStoreNps['InteractionTotalSurvey'] * 100;
        $InteractionData[$i]['InteractionPassivesPercentage'] = $StoreNpsPassivesPercentage;
        $StoreNpsDetractorsPercentage = $rowStoreNps['InteractionTotalDetractors'] / $rowStoreNps['InteractionTotalSurvey'] * 100;
        $InteractionData[$i]['InteractionDetractorsPercentage'] = $StoreNpsDetractorsPercentage;
        $InteractionData[$i]['InteractionNpsScore'] = number_format($StoreNpsAdvocatePercentage - $StoreNpsDetractorsPercentage, npsDecimal());
        $InteractionData[$i]['blanks'] = $rowStoreNps['blanks'];
        $InteractionData[$i]['PZero'] = $rowStoreNps['PZero'];

//Prepare Data needed for impact computation
        $NewIntStoreAdvocate = intval($rowStoreNps['IntTotalAdvo']) - intval($rowStoreNps['InteractionTotalAdvocate']);
        $NewIntStorePassive = intval($rowStoreNps['IntTotalPas']) - intval($rowStoreNps['InteractionTotalPassive']);
        $NewIntStoreDetractor = intval($rowStoreNps['IntTotalDet']) - intval($rowStoreNps['InteractionTotalDetractors']);
        $NewIntStoreTotalSurvey = intval($NewIntStoreAdvocate) + intval($NewIntStoreDetractor) + intval($NewIntStorePassive);
        $NewIntStoreNPS = (($NewIntStoreAdvocate - $NewIntStoreDetractor) / $NewIntStoreTotalSurvey) * 100;
        $IntStoreNPS = $rowStoreNps['InterationNPS'];
//Employee Impact
        $IntImpact = $IntStoreNPS - $NewIntStoreNPS;

        $InteractionData[$i]['IntImpact'] = $IntImpact;

        $EpisodeData[$i] = $rowStoreNps;
        $EpisodeAdvocatePercentage = intval($rowStoreNps['EpisodeTotalAdvocate']) / intval($rowStoreNps['EpisodeTotalSurvey']) * 100;
        $EpisodeData[$i]['EpisodeAdvocatesPercentage'] = $EpisodeAdvocatePercentage;
        $EpisodePassivesPercentage = $rowStoreNps['EpisodeTotalPassive'] / $rowStoreNps['EpisodeTotalSurvey'] * 100;
        $EpisodeData[$i]['EpisodePassivesPercentage'] = $EpisodePassivesPercentage;
        $EpisodeDetractorsPercentage = $rowStoreNps['EpisodeTotalDetractors'] / $rowStoreNps['EpisodeTotalSurvey'] * 100;
        $EpisodeData[$i]['EpisodeDetractorsPercentage'] = $EpisodeDetractorsPercentage;
        $EpisodeData[$i]['EpisodeNpsScore'] = number_format($EpisodeAdvocatePercentage - $EpisodeDetractorsPercentage, npsDecimal());

        $NewEpiStoreAdvocate = intval($rowStoreNps['EpiTotalAdvo']) - intval($rowStoreNps['EpisodeTotalAdvocate']);
        $NewEpiStorePassive = intval($rowStoreNps['EpiTotalPas']) - intval($rowStoreNps['EpisodeTotalPassive']);
        $NewEpiStoreDetractor = intval($rowStoreNps['EpiTotalDet']) - intval($rowStoreNps['EpisodeTotalDetractors']);
        $NewEpiStoreTotalSurvey = intval($NewEpiStoreAdvocate) + intval($NewEpiStoreDetractor) + intval($NewEpiStorePassive);
        $NewEpiStoreNPS = (($NewEpiStoreAdvocate - $NewEpiStoreDetractor) / $NewEpiStoreTotalSurvey) * 100;
        $EpiStoreNPS = $rowStoreNps['EpisodeNPS'];
//Employee Impact
        $EpiImpact = $EpiStoreNPS - $NewEpiStoreNPS;

        $EpisodeData[$i]['EpiImpact'] = $EpiImpact;

        $MergeData[$i] = array('StoreName' => $rowStoreNps['StoreName'],
            'EpisodeNPS' => $EpisodeData[$i]['EpisodeNpsScore'],
            'EpisodeTier' => $EpisodeData[$i]['EpisodeTierLevel'],
            'Advocates' => $EpisodeData[$i]['EpisodeTotalAdvocate'],
            'Pasives' => $EpisodeData[$i]['EpisodeTotalPassive'],
            'Detractors' => $EpisodeData[$i]['EpisodeTotalDetractors'],
            'AdvocatesPercentage' => $EpisodeData[$i]['EpisodeAdvocatesPercentage'],
            'PasivesPercentage' => $EpisodeData[$i]['EpisodePassivesPercentage'],
            'DetractorsPercentage' => $EpisodeData[$i]['EpisodeDetractorsPercentage'],
            'TotalSurvey' => $EpisodeData[$i]['EpisodeTotalSurvey'],
            'InteractionNPS' => $InteractionData[$i]['InteractionNpsScore'],
            'InteractionTier' => $InteractionData[$i]['InteractionTierLevel'],
            'InteractionAdvocates' => $InteractionData[$i]['InteractionTotalAdvocate'],
            'InteractionPasives' => $InteractionData[$i]['InteractionTotalPassive'],
            'InteractionDetractors' => $InteractionData[$i]['InteractionTotalDetractors'],
            'InteractionAdvocatesPercentage' => $InteractionData[$i]['InteractionAdvocatesPercentage'],
            'InteractionPasivesPercentage' => $InteractionData[$i]['InteractionPassivesPercentage'],
            'InteractionDetractorsPercentage' => $InteractionData[$i]['InteractionDetractorsPercentage'],
            'BlankSurveys' => $InteractionData[$i]['blanks'],
            'P000000' => $InteractionData[$i]['PZero'],
            'InteractionTotalSurvey' => $InteractionData[$i]['InteractionTotalSurvey'],
            'RawTotal' => $rowStoreNps['RawTotal']
        );
        $i++;
    }

    foreach ($InteractionData as $sort) {
        $toSortNpsScoreInteraction[] = $sort['InteractionNpsScore'];
    }

    foreach ($EpisodeData as $sort) {
        $toSortNpsScoreEpisode[] = $sort['EpisodeNpsScore'];
    }

    foreach ($MergeData as $sort) {
        $toSortInteraction[] = $sort['InteractionNPS'];
        $toSortEpisode[] = $sort['EpisodeNPS'];
    }

//    array_multisort($toSortNpsScoreInteraction, SORT_DESC, $InteractionData);
//    array_multisort($toSortNpsScoreEpisode, SORT_DESC, $EpisodeData);
//    array_multisort($toSortInteraction, SORT_DESC, $toSortEpisode, SORT_DESC, $MergeData);

    $returnData = array('Interaction' => $InteractionData, 'Episode' => $EpisodeData, 'Merged' => $MergeData);

// var_dump($rowStoreNps);
    return $returnData;
}

function getSurveyList($shared = "no", $from = NULL, $to = NULL) {
    global $connection;
    session_start();
    $shared = "no";
    $role = $_SESSION['UserRoleID'];
    switch ($role) {
        case '1':
            $sqlShared = " ";
            break;
        case '2':
        case '3':
            if ($shared == "no") {
                $sqlShared = " AND e.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
            } else {
                $sqlShared = " ";
            }
            break;
        case '4':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $data = $row['f_StoreOwnerUserID'];
            }
            $sqlShared = " AND e.f_StoreOwnerUserID = '{$data}' ";
            break;
        case '5':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $OwnerID = $row['f_StoreOwnerUserID'];
            }
//  echo $sqlGetOwnerID;
            if ($shared != "no") {
                $sqlgetPartners = "select a.f_StoreOwnerUserID FROM t_storelist a
            LEFT JOIN t_sharedstore b on a.f_StoreID = b.f_StoreID
            WHERE b.f_ShareToUserID = '{$OwnerID}' group by a.f_StoreOwnerUserID";
                $result = mysql_query($sqlgetPartners, $connection);
                while ($row = mysql_fetch_assoc($result)) {
                    $storePartners[] = $row['f_StoreOwnerUserID'];
                }
                foreach ($storePartners as $data) {
                    $quotedString .= "'$data',";
                }
                $quotedString .= "'{$_SESSION['UserProfileID']}',";
                $quotedString = trim($quotedString, ",");
// var_dump($storePartners);
                $sqlShared = " ";
            } else {
                $sqlShared = " AND e.f_StoreOwnerUserID = '{$OwnerID}' ";
            }
            break;
    }

    $sqlGetSurveyList = "SELECT
                        c.f_SurveyName, c.f_SurveyListID
                      FROM
                          t_surveypd a
                              LEFT JOIN
                          t_surveysd b ON a.f_SurveyPDID = b.f_SurveyPDID
                              LEFT JOIN
                          t_surveylist c ON a.f_SurveyID = c.f_SurveyListID
                              LEFT JOIN
                          t_subsurveylist d ON a.f_SubSurveyID = d.f_SubSurveyID
                              LEFT JOIN
                          t_storelist e ON b.f_StoreID = e.f_StoreID
                      WHERE
                          1=1 {$sqlShared} and c.f_SurveyListID is not null and d.f_SubSurveyID is not null
                      GROUP BY a.f_SurveyID ";
    $resultGetSurvey = mysql_query($sqlGetSurveyList, $connection);
    while ($dataGetSurvey = mysql_fetch_assoc($resultGetSurvey)) {
        $surveyList[] = $dataGetSurvey;
    }
//echo $sqlGetSurveyList;
    return $surveyList;
}

function PieChartSurvey($shared, $profileID, $empID, $to, $from, $role = NULL, $storeType = NULL, $surveyType = NULL, $subsurveyType = NULL, $storeID = NULL) {
    global $connection;
    date_default_timezone_set('Australia/Melbourne');
    $role = $_SESSION['UserRoleID'];
    $groupbyStore = " ";
    $joinExtension = " ";

    if ($storeID == 'AllStores') {
        $storeIDsql = " ";
        $groupbyStore = ",c.f_StoreID";
        $joinExtension = "  and c.f_StoreID = w.f_StoreID";
    } elseif ($storeID == 'Merged') {
        $storeIDsql = " ";
    } else {
        $storeIDsql = " AND a.f_StoreID = '{$storeID}'";
        $joinExtension = "  and c.f_StoreID = w.f_StoreID";
    }


    $surveyTypesql = " ";
    $subsurveyTypesql = " ";
    $subSurveyGroup = " ";
    if (!is_null($surveyType)) {
        if ($surveyType == 'AllType') {
            $subSurveyGroup = ",b.f_SubSurveyID ";
        } else {
            $surveyTypesql = " AND b.f_SurveyID = '{$surveyType}' ";
        }
    }

    if (!is_null($subsurveyType)) {
        if ($subsurveyType == 'AllSub') {
            $groupby[] = "b.f_SubSurveyID";
        } else {
            $subsurveyTypesql = " AND b.f_SubSurveyID = '{$surveyType}' ";
        }
    }
    if (!is_null($to) && !is_null($from)) {
        $to = date("Y-m-d", strtotime($to));
        $from = date("Y-m-d", strtotime($from));
        $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
    } else {
        $sqlWhen = " AND MONTH(a.f_DateTimeResponseMelbourne) = MONTH(CURDATE())
                     AND YEAR(a.f_DateTimeResponseMelbourne) = YEAR(CURDATE())";
    }

    $sqlStoreType = " ";
    if (!is_null($storeType) && $storeType != "") {
        $sqlStoreType = " AND c.f_StoreTypeID = '{$storeType}'";
    }
    switch ($role) {
        case '1':
            $sqlShared = " ";
            break;
        case '2':
        case '3':
            if ($shared == "no") {
                $sqlShared = " AND c.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
            } else {
                $sqlShared = " ";
            }
            break;
        case '4':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $data = $row['f_StoreOwnerUserID'];
            }
            $sqlShared = " AND c.f_StoreOwnerUserID = '{$data}' ";
            break;
        case '5':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $OwnerID = $row['f_StoreOwnerUserID'];
            }

            if ($shared != "no") {
                $sqlgetPartners = "select a.f_StoreOwnerUserID FROM t_storelist a
            LEFT JOIN t_sharedstore b on a.f_StoreID = b.f_StoreID
            WHERE b.f_ShareToUserID = '{$OwnerID}' group by a.f_StoreOwnerUserID";
                $result = mysql_query($sqlgetPartners, $connection);
                while ($row = mysql_fetch_assoc($result)) {
                    $storePartners[] = $row['f_StoreOwnerUserID'];
                }
                foreach ($storePartners as $data) {
                    $quotedString .= "'$data',";
                }
                $quotedString .= "'{$_SESSION['UserProfileID']}',";
                $quotedString = trim($quotedString, ",");
// var_dump($storePartners);
                $sqlShared = " ";
            } else {
                $sqlShared = " AND c.f_StoreOwnerUserID = '{$OwnerID}' ";
            }
            break;
    }


    $sqlPieChartSurvey = "SELECT
                            w.*,
                            e.f_SubSurveyName AS SubSurveyName,
                            e.f_SubSurveyID,
                            COUNT(a.f_SurveySDID) AS SubSurveyCount,
                            d.f_SurveyListID,
                            c.f_StoreID,c.f_StoreListName as StoreName,
                            ((SUM(CASE
                                WHEN
                                    (a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                                    AND a.f_ActivationExtra5 != 'P000000'
                                THEN 1 ELSE 0
                            END) - SUM(CASE
                                WHEN
                                    (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                                    AND a.f_ActivationExtra5 != 'P000000'
                                THEN 1 ELSE 0
                            END)) * 100 / SUM(CASE
                                WHEN
                                    a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                                    AND a.f_ActivationExtra5 != 'P000000'
                                THEN 1 ELSE 0
                            END)) as SubSurveyNPS,
                            ((SUM(CASE
                                WHEN
                                    (a.f_RecommendRate BETWEEN '9' AND '10')

                                THEN 1 ELSE 0
                            END) - SUM(CASE
                                WHEN
                                    (a.f_RecommendRate BETWEEN '0' AND '6')

                                THEN 1 ELSE 0
                            END)) * 100 / SUM(CASE
                                WHEN
                                    a.f_RecommendRate BETWEEN 0 AND 10

                                THEN 1 ELSE 0
                            END)) as SubSurveyEpisodeNPS
                        FROM
                            t_surveysd a
                                LEFT JOIN
                            t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
                                LEFT JOIN
                            t_storelist c ON a.f_StoreID = c.f_StoreID
                                LEFT JOIN
                            t_surveylist d ON b.f_SurveyID = d.f_SurveyListID
                                LEFT JOIN
                            t_subsurveylist e ON b.f_SubSurveyID = e.f_SubSurveyID
                                LEFT JOIN
                            (SELECT
                                d.f_SurveyName AS SurveyName,
                                    d.f_SurveyListID,
                                    COUNT(a.f_SurveySDID) AS SurveyCount,c.f_StoreID,
                                    ((SUM(CASE
                                        WHEN
                                            (a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                                            AND a.f_ActivationExtra5 != 'P000000'
                                        THEN 1 ELSE 0
                                    END) - SUM(CASE
                                        WHEN
                                            (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                                            AND a.f_ActivationExtra5 != 'P000000'
                                        THEN 1 ELSE 0
                                    END)) * 100 / SUM(CASE
                                        WHEN
                                            a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                                            AND a.f_ActivationExtra5 != 'P000000'
                                        THEN 1 ELSE 0
                                    END)) as MainSurveyNPS,
                                    ((SUM(CASE
                                        WHEN
                                            (a.f_RecommendRate BETWEEN '9' AND '10')

                                        THEN 1 ELSE 0
                                    END) - SUM(CASE
                                        WHEN
                                            (a.f_RecommendRate BETWEEN '0' AND '6')

                                        THEN 1 ELSE 0
                                    END)) * 100 / SUM(CASE
                                        WHEN
                                            a.f_RecommendRate BETWEEN 0 AND 10
                                        THEN 1 ELSE 0
                                    END)) as MainSurveyEpisodeNPS
                            FROM
                                t_surveysd a
                            LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
                            LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID
                            LEFT JOIN t_surveylist d ON b.f_SurveyID = d.f_SurveyListID
                            LEFT JOIN t_subsurveylist e ON b.f_SubSurveyID = e.f_SubSurveyID
                            WHERE
                                1 = 1 AND c.f_Status = 1
                                     {$sqlShared} {$sqlWhen} {$sqlStoreType} {$storeIDsql} {$surveyTypesql} {$subsurveyTypesql}
                            GROUP BY b.f_SurveyID{$groupbyStore}) AS w ON d.f_SurveyListID = w.f_SurveyListID {$joinExtension}
                        WHERE
                            1 = 1 AND c.f_Status = 1
                               {$sqlShared} {$sqlWhen} {$sqlStoreType} {$storeIDsql} {$surveyTypesql} {$subsurveyTypesql}
                        GROUP BY b.f_SubSurveyID{$groupbyStore}
                        order by c.f_StoreID,b.f_SurveyID";
    $resultPieChartSurvey = mysql_query($sqlPieChartSurvey, $connection);
    while ($dataPieChart = mysql_fetch_assoc($resultPieChartSurvey)) {

//               if($storeID == 'Merged'){
//                   $returnPieChart['Merged'][] = $dataPieChart;
//               }else{
//                   $returnPieChart[$dataPieChart['StoreName']][] = $dataPieChart;
//               }
        $returnPieChart[] = $dataPieChart;
    }
// echo $sqlPieChartSurvey;
    return $returnPieChart;
}

function getStoreNpsTLS($shared = NULL, $profileID = NULL, $empID = NULL, $to = NULL, $from = NULL, $role = NULL, $storeType = 1, $rangeType = NULL, $tierSort = 'Episode', $tierReport = NULL, $quarter = NULL) {

    global $connection;

    if ($shared != 'no') {
        $privateStore = " and c.f_StoreOwnerUserID NOT IN (SELECT f_StoreOwnerUserID FROM t_privateowner) ";
    }

    date_default_timezone_set('Australia/Melbourne');
    $sqlReportType = " ";

    if ($quarter == "Q3") {
        $sqlReportInteraction = " AND b.f_SurveyID IN ('1','2','3','4','5','6') ";
        $sqlReportFixed = " AND b.f_SurveyID IN ('1','2','3','4','6','13','14','15')
                         AND b.f_ProductID IN ('4','5','6','7','9','10','11','12','13','15','17','18','22','30','37','38','41','19') ";
        $sqlReportMobile = " AND b.f_SurveyID IN ('1','2','3','4','13','14','15')
                         AND b.f_ProductID IN ('1','2','3','14','39','40','36')";
    } else {
        $sqlReportInteraction = " AND b.f_SurveyID IN ('1','2','3','4','5','6') ";
        $sqlReportFixed = " AND b.f_SurveyID IN ('1','2','3','4','6','13','14','15')
                         AND b.f_ProductID IN ('4','5','6','7','9','10','11','12','13','15','17','18','22','30','37','38','41','19','42') ";
        $sqlReportMobile = " AND b.f_SurveyID IN ('1','2','3','4','13','14','15')
                         AND b.f_ProductID IN ('1','2','3','14','39','40','36')";
    }



    if ($quarter == NULL) {

        if (!is_null($to) && !is_null($from)) {
            $to = date("Y-m-d", strtotime($to));
            $from = date("Y-m-d", strtotime($from));
            $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
        } else {
            $sqlWhen = " AND MONTH(a.f_DateTimeResponseMelbourne) = MONTH(CURDATE())
                     AND YEAR(a.f_DateTimeResponseMelbourne) = YEAR(CURDATE())";
        }
    } else {

        if ($quarter == 'Q2') {
            $from = date("Y-10-01");
            $to = date("Y-12-31");
        } elseif ($quarter == 'Q3') {
            $from = date("Y-01-01");
            $to = date("Y-03-31");
        } elseif ($quarter == 'Q4') {
            $from = date("Y-04-01");
            $to = date("Y-06-30");
        } elseif ($quarter == 'Q1') {
            $from = date("Y-07-01");
            $to = date("Y-09-30");
        } elseif ($quarter == 'OldQ2') {
            $from = date("Y-10-01", strtotime("-1 year"));
            $to = date("Y-12-31", strtotime("-1 year"));
//            $from = date("Y-10-01");
//            $to = date("Y-12-31");
        } elseif ($quarter == 'H2') {  
            $from = date("Y-01-01");
            $to = date("Y-06-30");
            $quarter = "Q3";
        } elseif ($quarter == 'H1') {
            $from = date("Y-07-01");
            $to = date("Y-12-31");
            $quarter = "Q1";
        }

        $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
    }



    $role = $_SESSION['UserRoleID'];

  
    $sqlStoreType = " AND c.f_StoreTypeID = '1'";
    
    switch ($role) {
        case '1':
            $sqlShared = " ";
            break;
        case '2':

        case '3':
            if ($shared == "no") {

                $selectCollab = "SELECT f_StoreCollabWithUserID as 'CollabWith' FROM t_storecollab WHERE f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}'";
                $resultCollab = mysql_query($selectCollab, $connection);
                while ($row = mysql_fetch_assoc($resultCollab)) {
                    $CollabWith[] = $row['CollabWith'];
                }
                if (count($CollabWith) >= 1) {
                    array_push($CollabWith, $_SESSION['UserProfileID']);
                    $whereInCollab = implode(', ', $CollabWith);
                    $sqlShared = " AND c.f_StoreOwnerUserID in ({$whereInCollab})";
                } else {
                    $sqlShared = " AND c.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
                }
            } else {
                $sqlShared = " ";
            }
            break;
        case '4':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $data = $row['f_StoreOwnerUserID'];
            }
            $sqlShared = " AND c.f_StoreOwnerUserID = '{$data}' ";
            break;
        case '5':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $OwnerID = $row['f_StoreOwnerUserID'];
            }

            if ($shared != "no") {
                $sqlgetPartners = "select a.f_StoreOwnerUserID FROM t_storelist a
            LEFT JOIN t_sharedstore b on a.f_StoreID = b.f_StoreID
            WHERE b.f_ShareToUserID = '{$OwnerID}' group by a.f_StoreOwnerUserID";
                $result = mysql_query($sqlgetPartners, $connection);
                while ($row = mysql_fetch_assoc($result)) {
                    $storePartners[] = $row['f_StoreOwnerUserID'];
                }
                foreach ($storePartners as $data) {
                    $quotedString .= "'$data',";
                }
                $quotedString .= "'{$_SESSION['UserProfileID']}',";
                $quotedString = trim($quotedString, ",");
// var_dump($storePartners);
                $sqlShared = " ";
            } else {
                $sqlShared = " AND c.f_StoreOwnerUserID = '{$OwnerID}' ";
            }
            break;
        case '6':
            $sqlShared = " AND c.f_StoreOwnerUserID = '{$profileID}' ";
            break;
    }

    if (is_null($quarter)) {
        $qT = " and f_Quarter = 'Q1'";
    } elseif ($quarter == 'OldQ2') {
        $qT = " and f_Quarter = 'Q2'";
    } else {
        $qT = " and f_Quarter = '{$quarter}'";
    }


    $sqlGetTiers = "SELECT * From t_storeTier where f_StoreTypeID = 1 {$qT}";
    $tierResult = mysql_query($sqlGetTiers, $connection);
    while ($row = mysql_fetch_assoc($tierResult)) {
        $Tiers[$row['f_NpsType']][] = array("max" => $row["f_NpsMax"], "min" => $row["f_NpsMin"],
            "tier" => $row["f_TierLevel"], "bapps" => $row["f_BappsPoints"], "bappsPercent" => $row["f_BappsPercentage"]);
    }

    $sqlStoreNps = "SELECT
    c.f_StoreID, c.f_StoreListName as StoreName, w.*, x.* , z.* , y.*
    FROM
        t_surveysd a
            LEFT JOIN
        t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
            LEFT JOIN
        t_storelist c ON a.f_StoreID = c.f_StoreID
            LEFT JOIN
        (SELECT
            DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%m-%d-%Y') AS InteractionDate,
                c.f_StoreID as 'InteractionStoreID',
                SUM(CASE
                    WHEN
                        (a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                            AND (a.f_ActivationExtra5 != 'P000000')
                    THEN
                        1
                    ELSE 0
                END) AS InteractionTotalAdvocate,
                SUM(CASE
                    WHEN
                        (a.f_LikelihoodToRecommendRetail BETWEEN '7' AND '8')
                            AND (a.f_ActivationExtra5 != 'P000000')
                    THEN
                        1
                    ELSE 0
                END) AS InteractionTotalPassive,
                SUM(CASE
                    WHEN
                        (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                            AND (a.f_ActivationExtra5 != 'P000000')
                    THEN
                        1
                    ELSE 0
                END) AS InteractionTotalDetractors,
                SUM(CASE
                    WHEN
                        a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                            AND a.f_ActivationExtra5 != 'P000000'
                    THEN
                        1
                    ELSE 0
                END) AS InteractionTotalSurvey,
                SUM(CASE
                    WHEN
                         ( trim(a.f_LikelihoodToRecommendRetail) IS NULL)
                            AND a.f_ActivationExtra5 != 'P000000'
                    THEN
                        1
                    ELSE 0
                END) AS InteractionNoScore,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = 'P000000' THEN 1
                    ELSE 0
                END) AS InteractionPZero,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = '' THEN 1
                    ELSE 0
                END) AS InteractionUnAssigned,
                COUNT(a.f_SurveySDID) AS InteractionRawTotal
        FROM
            t_surveysd a
        LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID
        WHERE
            1 = 1 AND c.f_Status = 1  AND c.f_StoreTypeID = '1'
            {$sqlWhen} {$sqlReportInteraction} {$sqlShared}
        GROUP BY c.f_StoreID) AS w ON c.f_StoreID = w.InteractionStoreID
            LEFT JOIN
        (SELECT
            DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%m-%d-%Y') AS MobileTotalDate,
                c.f_StoreID as 'MobileStoreID',
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '9' AND '10' THEN 1
                    ELSE 0
                END) AS MobileTotalAdvocate,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '7' AND '8' THEN 1
                    ELSE 0
                END) AS MobileTotalPassive,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '0' AND '6' THEN 1
                    ELSE 0
                END) AS MobileTotalDetractors,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
                    ELSE 0
                END) AS MobileTotalSurvey,
                SUM(CASE
                    WHEN (a.f_RecommendRate IS NULL) THEN 1
                    ELSE 0
                END) AS MobileNoScore,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = 'P000000' THEN 1
                    ELSE 0
                END) AS MobilePZero,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = '' THEN 1
                    ELSE 0
                END) AS MobileUnAssigned,
                COUNT(a.f_SurveySDID) AS MobileRawTotal
        FROM
            t_surveysd a
        LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID
        WHERE
            1 = 1 AND c.f_Status = 1 AND c.f_StoreTypeID = '1'
                 {$sqlWhen} {$sqlReportMobile} {$sqlShared}
        GROUP BY c.f_StoreID) AS x ON c.f_StoreID = x.MobileStoreID
            LEFT JOIN
        (SELECT
            c.f_StoreID as 'FixedStoreID',
                DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%m-%d-%Y') AS FixedDate,
                 SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '9' AND '10' THEN 1
                    ELSE 0
                END) AS FixedTotalAdvocate,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '7' AND '8' THEN 1
                    ELSE 0
                END) AS FixedTotalPassive,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '0' AND '6' THEN 1
                    ELSE 0
                END) AS FixedTotalDetractors,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
                    ELSE 0
                END) AS FixedTotalSurvey,
                SUM(CASE
                    WHEN (a.f_RecommendRate IS NULL) THEN 1
                    ELSE 0
                END) AS FixedNoScore,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = 'P000000' THEN 1
                    ELSE 0
                END) AS FixedPZero,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = '' THEN 1
                    ELSE 0
                END) AS FixedUnAssigned,
                COUNT(a.f_SurveySDID) AS FixedRawTotal

        FROM
            t_surveysd a
        LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID
        WHERE
            1 = 1 AND c.f_Status = 1 AND c.f_StoreTypeID = '1'
               {$sqlWhen} {$sqlReportFixed} {$sqlShared}
        GROUP BY c.f_StoreID) AS z ON c.f_StoreID = z.FixedStoreID
        LEFT JOIN
        (SELECT
            DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%m-%d-%Y') AS EpisodeTotalDate,
                c.f_StoreID as 'EpisodeStoreID',
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '9' AND '10' THEN 1
                    ELSE 0
                END) AS EpisodeTotalAdvocate,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '7' AND '8' THEN 1
                    ELSE 0
                END) AS EpisodeTotalPassive,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '0' AND '6' THEN 1
                    ELSE 0
                END) AS EpisodeTotalDetractors,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
                    ELSE 0
                END) AS EpisodeTotalSurvey,
                SUM(CASE
                    WHEN (a.f_RecommendRate IS NULL) THEN 1
                    ELSE 0
                END) AS EpisodeNoScore,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = 'P000000' THEN 1
                    ELSE 0
                END) AS EpisodePZero,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = '' THEN 1
                    ELSE 0
                END) AS EpisodeUnAssigned,
                COUNT(a.f_SurveySDID) AS EpisodeRawTotal
        FROM
            t_surveysd a
        LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID
        WHERE
            1 = 1 AND c.f_Status = 1 AND c.f_StoreTypeID = '1'
                 {$sqlWhen} {$sqlShared}
        GROUP BY c.f_StoreID) AS y ON c.f_StoreID = y.EpisodeStoreID
    WHERE
        1 = 1 AND c.f_Status = 1 AND c.f_StoreTypeID = '1' {$sqlShared} {$privateStore}
    GROUP BY c.f_StoreID";

    mysql_query("SELECT 1=1;", $connection);


    $resultStoreNps = mysql_query($sqlStoreNps, $connection);

    $i = 0;
    while ($rowStoreNps = mysql_fetch_assoc($resultStoreNps)) {

        $InteractionData[$i] = $rowStoreNps;
        $StoreNpsAdvocatePercentage = $rowStoreNps['InteractionTotalAdvocate'] / $rowStoreNps['InteractionTotalSurvey'] * 100;
        $InteractionData[$i]['InteractionAdvocatesPercentage'] = $StoreNpsAdvocatePercentage;
        $StoreNpsPassivesPercentage = $rowStoreNps['InteractionTotalPassive'] / $rowStoreNps['InteractionTotalSurvey'] * 100;
        $InteractionData[$i]['InteractionPassivesPercentage'] = $StoreNpsPassivesPercentage;
        $StoreNpsDetractorsPercentage = $rowStoreNps['InteractionTotalDetractors'] / $rowStoreNps['InteractionTotalSurvey'] * 100;
        $InteractionData[$i]['InteractionDetractorsPercentage'] = $StoreNpsDetractorsPercentage;
        $InteractionData[$i]['InteractionNpsScore'] = number_format($StoreNpsAdvocatePercentage - $StoreNpsDetractorsPercentage, npsDecimal());

        $InteractionData[$i]['InteractionTierLevel'] = " ";

        $FixedData[$i] = $rowStoreNps;
        $FixedAdvocatePercentage = intval($rowStoreNps['FixedTotalAdvocate']) / intval($rowStoreNps['FixedTotalSurvey']) * 100;
        $FixedData[$i]['FixedAdvocatesPercentage'] = $FixedAdvocatePercentage;
        $FixedPassivesPercentage = $rowStoreNps['FixedTotalPassive'] / $rowStoreNps['FixedTotalSurvey'] * 100;
        $FixedData[$i]['FixedPassivesPercentage'] = $FixedPassivesPercentage;
        $FixedDetractorsPercentage = $rowStoreNps['FixedTotalDetractors'] / intval($rowStoreNps['FixedTotalSurvey']) * 100;
        $FixedData[$i]['FixedDetractorsPercentage'] = $FixedDetractorsPercentage;
        $FixedData[$i]['FixedNpsScore'] = number_format($FixedAdvocatePercentage - $FixedDetractorsPercentage, npsDecimal());

        $MobileData[$i] = $rowStoreNps;
        $MobileAdvocatePercentage = intval($rowStoreNps['MobileTotalAdvocate']) / intval($rowStoreNps['MobileTotalSurvey']) * 100;
        $MobileData[$i]['MobileAdvocatesPercentage'] = $MobileAdvocatePercentage;
        $MobilePassivesPercentage = $rowStoreNps['MobileTotalPassive'] / $rowStoreNps['MobileTotalSurvey'] * 100;
        $MobileData[$i]['MobilePassivesPercentage'] = $MobilePassivesPercentage;
        $MobileDetractorsPercentage = $rowStoreNps['MobileTotalDetractors'] / $rowStoreNps['MobileTotalSurvey'] * 100;
        $MobileData[$i]['MobileDetractorsPercentage'] = $MobileDetractorsPercentage;
        $MobileData[$i]['MobileNpsScore'] = number_format($MobileAdvocatePercentage - $MobileDetractorsPercentage, npsDecimal());

        $EpisodeData[$i] = $rowStoreNps;
        $EpisodeAdvocatePercentage = intval($rowStoreNps['EpisodeTotalAdvocate']) / intval($rowStoreNps['EpisodeTotalSurvey']) * 100;
        $EpisodeData[$i]['EpisodeAdvocatesPercentage'] = $EpisodeAdvocatePercentage;
        $EpisodePassivesPercentage = $rowStoreNps['EpisodeTotalPassive'] / $rowStoreNps['EpisodeTotalSurvey'] * 100;
        $EpisodeData[$i]['EpisodePassivesPercentage'] = $EpisodePassivesPercentage;
        $EpisodeDetractorsPercentage = $rowStoreNps['EpisodeTotalDetractors'] / $rowStoreNps['EpisodeTotalSurvey'] * 100;
        $EpisodeData[$i]['EpisodeDetractorsPercentage'] = $EpisodeDetractorsPercentage;
        $EpisodeData[$i]['EpisodeNpsScore'] = number_format($EpisodeAdvocatePercentage - $EpisodeDetractorsPercentage, npsDecimal());


        if ($InteractionData[$i]['InteractionNpsScore'] >= 80) {
            $InteractionData[$i]['InteractionTierLevel'] = 'Tier 1';
        } elseif ($InteractionData[$i]['InteractionNpsScore'] >= 75 AND $InteractionData[$i]['InteractionNpsScore'] <= 79) {
            $InteractionData[$i]['InteractionTierLevel'] = 'Tier 2';
        } elseif ($InteractionData[$i]['InteractionNpsScore'] >= 68 AND $InteractionData[$i]['InteractionNpsScore'] <= 74) {
            $InteractionData[$i]['InteractionTierLevel'] = 'Tier 3';
        } elseif ($InteractionData[$i]['InteractionNpsScore'] >= 58 AND $InteractionData[$i]['InteractionNpsScore'] <= 67) {
            $InteractionData[$i]['InteractionTierLevel'] = 'Tier 4';
        } elseif ($InteractionData[$i]['InteractionNpsScore'] < 58) {
            $InteractionData[$i]['InteractionTierLevel'] = 'Tier 5';
        }

        if ($EpisodeData[$i]['EpisodeNpsScore'] >= 50) {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 1';
        } elseif ($EpisodeData[$i]['EpisodeNpsScore'] >= 40 AND $EpisodeData[$i]['EpisodeNpsScore'] <= 49) {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 2';
        } elseif ($EpisodeData[$i]['EpisodeNpsScore'] >= 30 AND $EpisodeData[$i]['EpisodeNpsScore'] <= 39) {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 3';
        } elseif ($EpisodeData[$i]['EpisodeNpsScore'] >= 0 AND $EpisodeData[$i]['EpisodeNpsScore'] <= 29) {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 4';
        } else {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 5';
        }

        $fixCounter = 0;
        foreach ($Tiers["Fixed"] as $TierData) {


            if ($FixedData[$i]['FixedTotalSurvey'] == 0) {
                $FixedData[$i]['FixedTierLevel'] = "No NPS Score";
                $FixedData[$i]['FixedPoints'] = 0;
            } else {
                if ($fixCounter == 0) {
                    if ($FixedData[$i]['FixedNpsScore'] >= $TierData['min']) {
                        $FixedData[$i]['FixedTierLevel'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
                        $FixedData[$i]['FixedPoints'] = $TierData['bapps'];
                    }
                } elseif ($fixCounter == count($TierData)) {
                    if ($FixedData[$i]['FixedNpsScore'] < $TierData['max']) {
                        $FixedData[$i]['FixedTierLevel'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
                        $FixedData[$i]['FixedPoints'] = $TierData['bapps'];
                    }
                } else {
                    if ($FixedData[$i]['FixedNpsScore'] >= $TierData['min'] && $FixedData[$i]['FixedNpsScore'] < $TierData['max']) {
                        $FixedData[$i]['FixedTierLevel'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
                        $FixedData[$i]['FixedPoints'] = $TierData['bapps'];
                    }
                }
            }

            $fixCounter = $fixCounter + 1;
        }


        // echo 'was here';


        $mobCounter = 0;
        foreach ($Tiers["Mobile"] as $TierData) {
            //  print_r($TierData);
            if ($MobileData[$i]['MobileTotalSurvey'] == 0) {
                $MobileData[$i]['MobileTierLevel'] = "No NPS Score";
                $MobileData[$i]['MobilePoints'] = 0;
            } else {
                if ($mobCounter == 0) {
                    if ($MobileData[$i]['MobileNpsScore'] >= $TierData['min']) {
                        $MobileData[$i]['MobileTierLevel'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
                        $MobileData[$i]['MobilePoints'] = $TierData['bapps'];
                    }
                } elseif ($mobCounter == count($TierData)) {
                    if ($MobileData[$i]['MobileNpsScore'] < $TierData['max']) {
                        $MobileData[$i]['MobileTierLevel'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
                        $MobileData[$i]['MobilePoints'] = $TierData['bapps'];
                    }
                } else {
                    if ($MobileData[$i]['MobileNpsScore'] >= $TierData['min'] AND $MobileData[$i]['MobileNpsScore'] < $TierData['max']) {
                        $MobileData[$i]['MobileTierLevel'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
                        $MobileData[$i]['MobilePoints'] = $TierData['bapps'];
                    }
                }
            }

            $mobCounter = $mobCounter + 1;
        }

        /**
          if ($FixedData[$i]['FixedNpsScore'] >= 58) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 1 - 140% ';
          $FixedData[$i]['FixedPoints'] = 50;
          } elseif ($FixedData[$i]['FixedNpsScore'] >= 51 AND $FixedData[$i]['FixedNpsScore'] <= 57) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 2 - 120% ';
          $FixedData[$i]['FixedPoints'] = 45;
          } elseif ($FixedData[$i]['FixedNpsScore'] >= 45 AND $FixedData[$i]['FixedNpsScore'] <= 50) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 3 - 110% ';
          $FixedData[$i]['FixedPoints'] = 40;
          } elseif ($FixedData[$i]['FixedNpsScore'] >= 39 AND $FixedData[$i]['FixedNpsScore'] <= 44) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 4 - 100% ';
          $FixedData[$i]['FixedPoints'] = 30;
          } elseif ($FixedData[$i]['FixedNpsScore'] < 39) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 5 - 60%';
          $FixedData[$i]['FixedPoints'] = 15;
          } */
        /** if ($MobileData[$i]['MobileNpsScore'] >= 72) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 1  - 140%';
          $MobileData[$i]['MobilePoints'] = 50;
          } elseif ($MobileData[$i]['MobileNpsScore'] >= 68 AND $MobileData[$i]['MobileNpsScore'] <= 71) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 2  - 120%';
          $MobileData[$i]['MobilePoints'] = 45;
          } elseif ($MobileData[$i]['MobileNpsScore'] >= 64 AND $MobileData[$i]['MobileNpsScore'] <= 67) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 3 - 110% ';
          $MobileData[$i]['MobilePoints'] = 40;
          } elseif ($MobileData[$i]['MobileNpsScore'] >= 59 AND $MobileData[$i]['MobileNpsScore'] <= 63) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 4 - 100%';
          $MobileData[$i]['MobilePoints'] = 30;
          } elseif ($MobileData[$i]['MobileNpsScore'] < 59) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 5 - 60%';
          $MobileData[$i]['MobilePoints'] = 15;
          } */
        $PointsTotal[$i] = $MobileData[$i]['MobilePoints'] + $FixedData[$i]['FixedPoints'];

        /** else {

          if ($FixedData[$i]['FixedNpsScore'] >= 46) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 1';
          } elseif ($FixedData[$i]['FixedNpsScore'] >= 41 AND $FixedData[$i]['FixedNpsScore'] <= 45) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 2';
          } elseif ($FixedData[$i]['FixedNpsScore'] >= 31 AND $FixedData[$i]['FixedNpsScore'] <= 40) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 3';
          } elseif ($FixedData[$i]['FixedNpsScore'] >= 16 AND $FixedData[$i]['FixedNpsScore'] <= 30) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 4';
          } elseif ($FixedData[$i]['FixedNpsScore'] < 16) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 5';
          }

          if ($MobileData[$i]['MobileNpsScore'] >= 67) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 1';
          } elseif ($MobileData[$i]['MobileNpsScore'] >= 62 AND $MobileData[$i]['MobileNpsScore'] <= 66) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 2';
          } elseif ($MobileData[$i]['MobileNpsScore'] >= 53 AND $MobileData[$i]['MobileNpsScore'] <= 61) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 3';
          } elseif ($MobileData[$i]['MobileNpsScore'] >= 44 AND $MobileData[$i]['MobileNpsScore'] <= 52) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 4';
          } elseif ($MobileData[$i]['MobileNpsScore'] < 44) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 5';
          }
          } */
        switch ($tierSort) {
            case 'Interaction':
                $weight[$i] = $InteractionData[$i]['InteractionNpsScore'];
                break;
            case 'Episode':
                $weight[$i] = $EpisodeData[$i]['EpisodeNpsScore'];
                break;
            case 'Mobile':
                $weight[$i] = $MobileData[$i]['MobileNpsScore'];
                break;
            case 'Fixed':
                $weight[$i] = $FixedData[$i]['FixedNpsScore'];
                break;
            default:
                $weight[$i] = $InteractionData[$i]['EpisodeNpsScore'];
                break;
        }


        $MergeData[$i] = array('StoreName' => $rowStoreNps['StoreName'],
            'InteractionNPS' => $InteractionData[$i]['InteractionNpsScore'],
            'InteractionTier' => $InteractionData[$i]['InteractionTierLevel'],
            'InteractionAdvocates' => $InteractionData[$i]['InteractionTotalAdvocate'],
            'InteractionPasives' => $InteractionData[$i]['InteractionTotalPassive'],
            'InteractionDetractors' => $InteractionData[$i]['InteractionTotalDetractors'],
            'InteractionAdvocatesPercentage' => $InteractionData[$i]['InteractionAdvocatesPercentage'],
            'InteractionPasivesPercentage' => $InteractionData[$i]['InteractionPassivesPercentage'],
            'InteractionDetractorsPercentage' => $InteractionData[$i]['InteractionDetractorsPercentage'],
            'InteractionTotalSurvey' => $InteractionData[$i]['InteractionTotalSurvey'],
            'FixedNPS' => $FixedData[$i]['FixedNpsScore'],
            'FixedTier' => $FixedData[$i]['FixedTierLevel'],
            'FixedAdvocates' => $FixedData[$i]['FixedTotalAdvocate'],
            'FixedPasives' => $FixedData[$i]['FixedTotalPassive'],
            'FixedDetractors' => $FixedData[$i]['FixedTotalDetractors'],
            'FixedAdvocatesPercentage' => $FixedData[$i]['FixedAdvocatesPercentage'],
            'FixedPasivesPercentage' => $FixedData[$i]['FixedPassivesPercentage'],
            'FixedDetractorsPercentage' => $FixedData[$i]['FixedDetractorsPercentage'],
            'FixedTotalSurvey' => $FixedData[$i]['FixedTotalSurvey'],
            'FixedTotalPoints' => $FixedData[$i]['FixedPoints'],
            'MobileNPS' => $MobileData[$i]['MobileNpsScore'],
            'MobileTier' => $MobileData[$i]['MobileTierLevel'],
            'MobileAdvocates' => $MobileData[$i]['MobileTotalAdvocate'],
            'MobilePasives' => $MobileData[$i]['MobileTotalPassive'],
            'MobileDetractors' => $MobileData[$i]['MobileTotalDetractors'],
            'MobileAdvocatesPercentage' => $MobileData[$i]['MobileAdvocatesPercentage'],
            'MobilePasivesPercentage' => $MobileData[$i]['MobilePassivesPercentage'],
            'MobileDetractorsPercentage' => $MobileData[$i]['MobileDetractorsPercentage'],
            'MobileTotalSurvey' => $MobileData[$i]['MobileTotalSurvey'],
            'MobileTotalPoints' => $MobileData[$i]['MobilePoints'],
            'EpisodeNPS' => $EpisodeData[$i]['EpisodeNpsScore'],
            'EpisodeTier' => $EpisodeData[$i]['EpisodeTierLevel'],
            'EpisodeAdvocates' => $EpisodeData[$i]['EpisodeTotalAdvocate'],
            'EpisodePasives' => $EpisodeData[$i]['EpisodeTotalPassive'],
            'EpisodeDetractors' => $EpisodeData[$i]['EpisodeTotalDetractors'],
            'EpisodeAdvocatesPercentage' => $EpisodeData[$i]['EpisodeAdvocatesPercentage'],
            'EpisodePasivesPercentage' => $EpisodeData[$i]['EpisodePassivesPercentage'],
            'EpisodeDetractorsPercentage' => $EpisodeData[$i]['EpisodeDetractorsPercentage'],
            'EpisodeTotalSurvey' => $EpisodeData[$i]['EpisodeTotalSurvey'],
            'PointsTotal' => $PointsTotal[$i],
            'weight' => $weight[$i]
        );
        $i++;
    }

    foreach ($InteractionData as $sort) {
        $toSortNpsScoreInteraction[] = $sort['InteractionNpsScore'];
    }

    foreach ($MobileData as $sort) {
        $toSortNpsScoreMobile[] = $sort['MobileNpsScore'];
    }

    foreach ($FixedData as $sort) {
        $toSortNpsScoreFixed[] = $sort['FixedNpsScore'];
    }
    foreach ($EpisodeData as $sort) {
        $toSortNpsScoreEpisode[] = $sort['EpisodeNpsScore'];
    }

    foreach ($MergeData as $sort) {
        $toWeight[] = $sort['weight'];
    }

    array_multisort($toSortNpsScoreInteraction, SORT_DESC, $InteractionData);
    array_multisort($toSortNpsScoreMobile, SORT_DESC, $MobileData);
    array_multisort($toSortNpsScoreFixed, SORT_DESC, $FixedData);
    array_multisort($toSortNpsScoreEpisode, SORT_DESC, $EpisodeData);
    array_multisort($toWeight, SORT_DESC, $MergeData);
// array_multisort($toSortNpsScoreInteraction, SORT_DESC, $toSortNpsScoreMobile, SORT_DESC, $FixedData, SORT_ASC, $MergeData);

    $returnData = array('Interaction' => $InteractionData, 'Episode' => $EpisodeData, 'Mobile' => $MobileData, 'Fixed' => $FixedData, 'Merged' => $MergeData);

//    echo "<pre> here" . $tierSort;
//    print_r($weight);
//    //  var_dump($returnData);
//    exit;
    return $returnData;
}

function getStoreNpsOthersOld($shared = NULL, $profileID = NULL, $empID = NULL, $to = NULL, $from = NULL, $role = NULL, $storeType = 3, $rangeType = NULL, $tierSort = 'Episode', $tierReport = NULL, $quarter = NULL) {
    global $connection;

    date_default_timezone_set('Australia/Melbourne');
    $sqlReportType = " ";

    $sqlReportInteraction = " AND b.f_SurveyID IN ('1','2','3','4','5','6') ";
    $sqlReportFixed = " AND b.f_SurveyID IN ('1','2','3','4','6','13','14','15')
                         AND b.f_ProductID IN ('4','5','6','7','9','10','11','12','13','15','17','18','22','30','37','38','41','19') ";
    $sqlReportMobile = " AND b.f_SurveyID IN ('1','2','3','4','13','14','15')
                         AND b.f_ProductID IN ('1','2','3','14','39','40','36')";

//    if (!is_null($to) && !is_null($from)) {
//        $to = date("Y-m-d", strtotime($to));
//        $from = date("Y-m-d", strtotime($from));
//        $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
//    } else {
//        $sqlWhen = " AND MONTH(a.f_DateTimeResponseMelbourne) = MONTH(CURDATE())
//                     AND YEAR(a.f_DateTimeResponseMelbourne) = YEAR(CURDATE())";
//    }

    $from = "2019-07-01";
    $to = "2019-09-30";
    $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";


    $role = $_SESSION['UserRoleID'];

    $sqlStoreType = " ";
    if (!is_null($storeType) && $storeType != "") {
        $sqlStoreType = " AND c.f_StoreTypeID = '{$storeType}'";
    }
    switch ($role) {
        case '1':
            $sqlShared = " ";
            break;
        case '2':

        case '3':
            if ($shared == "no") {

                $selectCollab = "SELECT f_StoreCollabWithUserID as 'CollabWith' FROM t_storecollab WHERE f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}'";
                $resultCollab = mysql_query($selectCollab, $connection);
                while ($row = mysql_fetch_assoc($resultCollab)) {
                    $CollabWith[] = $row['CollabWith'];
                }
                if (count($CollabWith) >= 1) {
                    array_push($CollabWith, $_SESSION['UserProfileID']);
                    $whereInCollab = implode(', ', $CollabWith);
                    $sqlShared = " AND c.f_StoreOwnerUserID in ({$whereInCollab})";
                } else {
                    $sqlShared = " AND c.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
                }
            } else {
                $sqlShared = " ";
            }
            break;
        case '4':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $data = $row['f_StoreOwnerUserID'];
            }
            $sqlShared = " AND c.f_StoreOwnerUserID = '{$data}' ";
            break;
        case '5':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $OwnerID = $row['f_StoreOwnerUserID'];
            }

            if ($shared != "no") {
                $sqlgetPartners = "select a.f_StoreOwnerUserID FROM t_storelist a
            LEFT JOIN t_sharedstore b on a.f_StoreID = b.f_StoreID
            WHERE b.f_ShareToUserID = '{$OwnerID}' group by a.f_StoreOwnerUserID";
                $result = mysql_query($sqlgetPartners, $connection);
                while ($row = mysql_fetch_assoc($result)) {
                    $storePartners[] = $row['f_StoreOwnerUserID'];
                }
                foreach ($storePartners as $data) {
                    $quotedString .= "'$data',";
                }
                $quotedString .= "'{$_SESSION['UserProfileID']}',";
                $quotedString = trim($quotedString, ",");
// var_dump($storePartners);
                $sqlShared = " ";
            } else {
                $sqlShared = " AND c.f_StoreOwnerUserID = '{$OwnerID}' ";
            }
            break;
        case '6':
            $sqlShared = " AND c.f_StoreOwnerUserID = '{$profileID}' ";
            break;
    }

    $sqlStoreNps = "SELECT
    c.f_StoreID, c.f_StoreListName as StoreName, w.*, x.* , z.* , y.*
    FROM
        t_surveysd a
            LEFT JOIN
        t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
            LEFT JOIN
        t_storelist c ON a.f_StoreID = c.f_StoreID
            LEFT JOIN
        (SELECT
            DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%m-%d-%Y') AS InteractionDate,
                c.f_StoreID as 'InteractionStoreID',
                SUM(CASE
                    WHEN
                        (a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                            AND (a.f_ActivationExtra5 != 'P000000')
                    THEN
                        1
                    ELSE 0
                END) AS InteractionTotalAdvocate,
                SUM(CASE
                    WHEN
                        (a.f_LikelihoodToRecommendRetail BETWEEN '7' AND '8')
                            AND (a.f_ActivationExtra5 != 'P000000')
                    THEN
                        1
                    ELSE 0
                END) AS InteractionTotalPassive,
                SUM(CASE
                    WHEN
                        (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                            AND (a.f_ActivationExtra5 != 'P000000')
                    THEN
                        1
                    ELSE 0
                END) AS InteractionTotalDetractors,
                SUM(CASE
                    WHEN
                        a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                            AND a.f_ActivationExtra5 != 'P000000'
                    THEN
                        1
                    ELSE 0
                END) AS InteractionTotalSurvey,
                SUM(CASE
                    WHEN
                         ( trim(a.f_LikelihoodToRecommendRetail) IS NULL)
                            AND a.f_ActivationExtra5 != 'P000000'
                    THEN
                        1
                    ELSE 0
                END) AS InteractionNoScore,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = 'P000000' THEN 1
                    ELSE 0
                END) AS InteractionPZero,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = '' THEN 1
                    ELSE 0
                END) AS InteractionUnAssigned,
                COUNT(a.f_SurveySDID) AS InteractionRawTotal
        FROM
            t_surveysd a
        LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID
        WHERE
            1 = 1 AND c.f_Status = 1  AND c.f_StoreTypeID = '3'
            {$sqlWhen} {$sqlReportInteraction}
        GROUP BY c.f_StoreID) AS w ON c.f_StoreID = w.InteractionStoreID
            LEFT JOIN
        (SELECT
            DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%m-%d-%Y') AS MobileTotalDate,
                c.f_StoreID as 'MobileStoreID',
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '9' AND '10' THEN 1
                    ELSE 0
                END) AS MobileTotalAdvocate,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '7' AND '8' THEN 1
                    ELSE 0
                END) AS MobileTotalPassive,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '0' AND '6' THEN 1
                    ELSE 0
                END) AS MobileTotalDetractors,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
                    ELSE 0
                END) AS MobileTotalSurvey,
                SUM(CASE
                    WHEN (a.f_RecommendRate IS NULL) THEN 1
                    ELSE 0
                END) AS MobileNoScore,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = 'P000000' THEN 1
                    ELSE 0
                END) AS MobilePZero,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = '' THEN 1
                    ELSE 0
                END) AS MobileUnAssigned,
                COUNT(a.f_SurveySDID) AS MobileRawTotal
        FROM
            t_surveysd a
        LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID
        WHERE
            1 = 1 AND c.f_Status = 1 AND c.f_StoreTypeID = '3'
                 {$sqlWhen} {$sqlReportMobile}
        GROUP BY c.f_StoreID) AS x ON c.f_StoreID = x.MobileStoreID
            LEFT JOIN
        (SELECT
            c.f_StoreID as 'FixedStoreID',
                DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%m-%d-%Y') AS FixedDate,
                 SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '9' AND '10' THEN 1
                    ELSE 0
                END) AS FixedTotalAdvocate,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '7' AND '8' THEN 1
                    ELSE 0
                END) AS FixedTotalPassive,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '0' AND '6' THEN 1
                    ELSE 0
                END) AS FixedTotalDetractors,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
                    ELSE 0
                END) AS FixedTotalSurvey,
                SUM(CASE
                    WHEN (a.f_RecommendRate IS NULL) THEN 1
                    ELSE 0
                END) AS FixedNoScore,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = 'P000000' THEN 1
                    ELSE 0
                END) AS FixedPZero,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = '' THEN 1
                    ELSE 0
                END) AS FixedUnAssigned,
                COUNT(a.f_SurveySDID) AS FixedRawTotal

        FROM
            t_surveysd a
        LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID
        WHERE
            1 = 1 AND c.f_Status = 1 AND c.f_StoreTypeID = '3'
               {$sqlWhen} {$sqlReportFixed}
        GROUP BY c.f_StoreID) AS z ON c.f_StoreID = z.FixedStoreID
        LEFT JOIN
        (SELECT
            DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%m-%d-%Y') AS EpisodeTotalDate,
                c.f_StoreID as 'EpisodeStoreID',
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '9' AND '10' THEN 1
                    ELSE 0
                END) AS EpisodeTotalAdvocate,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '7' AND '8' THEN 1
                    ELSE 0
                END) AS EpisodeTotalPassive,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '0' AND '6' THEN 1
                    ELSE 0
                END) AS EpisodeTotalDetractors,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
                    ELSE 0
                END) AS EpisodeTotalSurvey,
                SUM(CASE
                    WHEN (a.f_RecommendRate IS NULL) THEN 1
                    ELSE 0
                END) AS EpisodeNoScore,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = 'P000000' THEN 1
                    ELSE 0
                END) AS EpisodePZero,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = '' THEN 1
                    ELSE 0
                END) AS EpisodeUnAssigned,
                COUNT(a.f_SurveySDID) AS EpisodeRawTotal
        FROM
            t_surveysd a
        LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID
        WHERE
            1 = 1 AND c.f_Status = 1 AND c.f_StoreTypeID = '3'
                 {$sqlWhen}
        GROUP BY c.f_StoreID) AS y ON c.f_StoreID = y.EpisodeStoreID
    WHERE
        1 = 1 AND c.f_Status = 1 AND c.f_StoreTypeID = '3' {$sqlShared}
    GROUP BY c.f_StoreID";
//    echo "<pre>";
//    echo $sqlStoreNps;
//    exit;
    mysql_query("SELECT 1=1;", $connection);
    $resultStoreNps = mysql_query($sqlStoreNps, $connection);

    $i = 0;
    while ($rowStoreNps = mysql_fetch_assoc($resultStoreNps)) {

        $InteractionData[$i] = $rowStoreNps;
        $StoreNpsAdvocatePercentage = $rowStoreNps['InteractionTotalAdvocate'] / $rowStoreNps['InteractionTotalSurvey'] * 100;
        $InteractionData[$i]['InteractionAdvocatesPercentage'] = $StoreNpsAdvocatePercentage;
        $StoreNpsPassivesPercentage = $rowStoreNps['InteractionTotalPassive'] / $rowStoreNps['InteractionTotalSurvey'] * 100;
        $InteractionData[$i]['InteractionPassivesPercentage'] = $StoreNpsPassivesPercentage;
        $StoreNpsDetractorsPercentage = $rowStoreNps['InteractionTotalDetractors'] / $rowStoreNps['InteractionTotalSurvey'] * 100;
        $InteractionData[$i]['InteractionDetractorsPercentage'] = $StoreNpsDetractorsPercentage;
        $InteractionData[$i]['InteractionNpsScore'] = number_format($StoreNpsAdvocatePercentage - $StoreNpsDetractorsPercentage, npsDecimal());

        $InteractionData[$i]['InteractionTierLevel'] = " ";

        $FixedData[$i] = $rowStoreNps;
        $FixedAdvocatePercentage = intval($rowStoreNps['FixedTotalAdvocate']) / intval($rowStoreNps['FixedTotalSurvey']) * 100;
        $FixedData[$i]['FixedAdvocatesPercentage'] = $FixedAdvocatePercentage;
        $FixedPassivesPercentage = $rowStoreNps['FixedTotalPassive'] / $rowStoreNps['FixedTotalSurvey'] * 100;
        $FixedData[$i]['FixedPassivesPercentage'] = $FixedPassivesPercentage;
        $FixedDetractorsPercentage = $rowStoreNps['FixedTotalDetractors'] / intval($rowStoreNps['FixedTotalSurvey']) * 100;
        $FixedData[$i]['FixedDetractorsPercentage'] = $FixedDetractorsPercentage;
        $FixedData[$i]['FixedNpsScore'] = number_format($FixedAdvocatePercentage - $FixedDetractorsPercentage, npsDecimal());

        $MobileData[$i] = $rowStoreNps;
        $MobileAdvocatePercentage = intval($rowStoreNps['MobileTotalAdvocate']) / intval($rowStoreNps['MobileTotalSurvey']) * 100;
        $MobileData[$i]['MobileAdvocatesPercentage'] = $MobileAdvocatePercentage;
        $MobilePassivesPercentage = $rowStoreNps['MobileTotalPassive'] / $rowStoreNps['MobileTotalSurvey'] * 100;
        $MobileData[$i]['MobilePassivesPercentage'] = $MobilePassivesPercentage;
        $MobileDetractorsPercentage = $rowStoreNps['MobileTotalDetractors'] / $rowStoreNps['MobileTotalSurvey'] * 100;
        $MobileData[$i]['MobileDetractorsPercentage'] = $MobileDetractorsPercentage;
        $MobileData[$i]['MobileNpsScore'] = number_format($MobileAdvocatePercentage - $MobileDetractorsPercentage, npsDecimal());


        $EpisodeData[$i] = $rowStoreNps;
        $EpisodeAdvocatePercentage = intval($rowStoreNps['EpisodeTotalAdvocate']) / intval($rowStoreNps['EpisodeTotalSurvey']) * 100;
        $EpisodeData[$i]['EpisodeAdvocatesPercentage'] = $EpisodeAdvocatePercentage;
        $EpisodePassivesPercentage = $rowStoreNps['EpisodeTotalPassive'] / $rowStoreNps['EpisodeTotalSurvey'] * 100;
        $EpisodeData[$i]['EpisodePassivesPercentage'] = $EpisodePassivesPercentage;
        $EpisodeDetractorsPercentage = $rowStoreNps['EpisodeTotalDetractors'] / $rowStoreNps['EpisodeTotalSurvey'] * 100;
        $EpisodeData[$i]['EpisodeDetractorsPercentage'] = $EpisodeDetractorsPercentage;
        $EpisodeData[$i]['EpisodeNpsScore'] = number_format($EpisodeAdvocatePercentage - $EpisodeDetractorsPercentage, npsDecimal());


        if ($InteractionData[$i]['InteractionNpsScore'] >= 80) {
            $InteractionData[$i]['InteractionTierLevel'] = 'Tier 1';
        } elseif ($InteractionData[$i]['InteractionNpsScore'] >= 75 AND $InteractionData[$i]['InteractionNpsScore'] <= 79) {
            $InteractionData[$i]['InteractionTierLevel'] = 'Tier 2';
        } elseif ($InteractionData[$i]['InteractionNpsScore'] >= 68 AND $InteractionData[$i]['InteractionNpsScore'] <= 74) {
            $InteractionData[$i]['InteractionTierLevel'] = 'Tier 3';
        } elseif ($InteractionData[$i]['InteractionNpsScore'] >= 58 AND $InteractionData[$i]['InteractionNpsScore'] <= 67) {
            $InteractionData[$i]['InteractionTierLevel'] = 'Tier 4';
        } elseif ($InteractionData[$i]['InteractionNpsScore'] < 58) {
            $InteractionData[$i]['InteractionTierLevel'] = 'Tier 5';
        }

        if ($EpisodeData[$i]['EpisodeNpsScore'] >= 50) {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 1';
        } elseif ($EpisodeData[$i]['EpisodeNpsScore'] >= 40 AND $EpisodeData[$i]['EpisodeNpsScore'] <= 49) {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 2';
        } elseif ($EpisodeData[$i]['EpisodeNpsScore'] >= 30 AND $EpisodeData[$i]['EpisodeNpsScore'] <= 39) {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 3';
        } elseif ($EpisodeData[$i]['EpisodeNpsScore'] >= 0 AND $EpisodeData[$i]['EpisodeNpsScore'] <= 29) {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 4';
        } else {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 5';
        }



        if ($FixedData[$i]['FixedNpsScore'] >= 58) {
            $FixedData[$i]['FixedTierLevel'] = 'Tier 1 - 140% ';
            $FixedData[$i]['FixedPoints'] = 50;
        } elseif ($FixedData[$i]['FixedNpsScore'] >= 51 AND $FixedData[$i]['FixedNpsScore'] <= 57) {
            $FixedData[$i]['FixedTierLevel'] = 'Tier 2 - 120% ';
            $FixedData[$i]['FixedPoints'] = 45;
        } elseif ($FixedData[$i]['FixedNpsScore'] >= 45 AND $FixedData[$i]['FixedNpsScore'] <= 50) {
            $FixedData[$i]['FixedTierLevel'] = 'Tier 3 - 110% ';
            $FixedData[$i]['FixedPoints'] = 40;
        } elseif ($FixedData[$i]['FixedNpsScore'] >= 39 AND $FixedData[$i]['FixedNpsScore'] <= 44) {
            $FixedData[$i]['FixedTierLevel'] = 'Tier 4 - 100% ';
            $FixedData[$i]['FixedPoints'] = 30;
        } elseif ($FixedData[$i]['FixedNpsScore'] < 39) {
            $FixedData[$i]['FixedTierLevel'] = 'Tier 5 - 60%';
            $FixedData[$i]['FixedPoints'] = 15;
        }

        if ($MobileData[$i]['MobileNpsScore'] >= 72) {
            $MobileData[$i]['MobileTierLevel'] = 'Tier 1  - 140%';
            $MobileData[$i]['MobilePoints'] = 50;
        } elseif ($MobileData[$i]['MobileNpsScore'] >= 68 AND $MobileData[$i]['MobileNpsScore'] <= 71) {
            $MobileData[$i]['MobileTierLevel'] = 'Tier 2  - 120%';
            $MobileData[$i]['MobilePoints'] = 45;
        } elseif ($MobileData[$i]['MobileNpsScore'] >= 64 AND $MobileData[$i]['MobileNpsScore'] <= 67) {
            $MobileData[$i]['MobileTierLevel'] = 'Tier 3 - 110% ';
            $MobileData[$i]['MobilePoints'] = 40;
        } elseif ($MobileData[$i]['MobileNpsScore'] >= 59 AND $MobileData[$i]['MobileNpsScore'] <= 63) {
            $MobileData[$i]['MobileTierLevel'] = 'Tier 4 - 100%';
            $MobileData[$i]['MobilePoints'] = 30;
        } elseif ($MobileData[$i]['MobileNpsScore'] < 59) {
            $MobileData[$i]['MobileTierLevel'] = 'Tier 5 - 60%';
            $MobileData[$i]['MobilePoints'] = 15;
        }

        $PointsTotal[$i] = $MobileData[$i]['MobilePoints'] + $FixedData[$i]['FixedPoints'];
        /* else {

          if ($FixedData[$i]['FixedNpsScore'] >= 46) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 1';
          } elseif ($FixedData[$i]['FixedNpsScore'] >= 41 AND $FixedData[$i]['FixedNpsScore'] <= 45) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 2';
          } elseif ($FixedData[$i]['FixedNpsScore'] >= 31 AND $FixedData[$i]['FixedNpsScore'] <= 40) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 3';
          } elseif ($FixedData[$i]['FixedNpsScore'] >= 16 AND $FixedData[$i]['FixedNpsScore'] <= 30) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 4';
          } elseif ($FixedData[$i]['FixedNpsScore'] < 16) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 5';
          }

          if ($MobileData[$i]['MobileNpsScore'] >= 67) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 1';
          } elseif ($MobileData[$i]['MobileNpsScore'] >= 62 AND $MobileData[$i]['MobileNpsScore'] <= 66) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 2';
          } elseif ($MobileData[$i]['MobileNpsScore'] >= 53 AND $MobileData[$i]['MobileNpsScore'] <= 61) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 3';
          } elseif ($MobileData[$i]['MobileNpsScore'] >= 44 AND $MobileData[$i]['MobileNpsScore'] <= 52) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 4';
          } elseif ($MobileData[$i]['MobileNpsScore'] < 44) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 5';
          }
          } */

        switch ($tierSort) {
            case 'Interaction':
                $weight[$i] = $InteractionData[$i]['InteractionNpsScore'];
                break;
            case 'Episode':
                $weight[$i] = $EpisodeData[$i]['EpisodeNpsScore'];
                break;
            case 'Mobile':
                $weight[$i] = $MobileData[$i]['MobileNpsScore'];
                break;
            case 'Fixed':
                $weight[$i] = $FixedData[$i]['FixedNpsScore'];
                break;
            default:
                $weight[$i] = $InteractionData[$i]['EpisodeNpsScore'];
                break;
        }


        $MergeData[$i] = array('StoreName' => $rowStoreNps['StoreName'],
            'InteractionNPS' => $InteractionData[$i]['InteractionNpsScore'],
            'InteractionTier' => $InteractionData[$i]['InteractionTierLevel'],
            'InteractionAdvocates' => $InteractionData[$i]['InteractionTotalAdvocate'],
            'InteractionPasives' => $InteractionData[$i]['InteractionTotalPassive'],
            'InteractionDetractors' => $InteractionData[$i]['InteractionTotalDetractors'],
            'InteractionAdvocatesPercentage' => $InteractionData[$i]['InteractionAdvocatesPercentage'],
            'InteractionPasivesPercentage' => $InteractionData[$i]['InteractionPassivesPercentage'],
            'InteractionDetractorsPercentage' => $InteractionData[$i]['InteractionDetractorsPercentage'],
            'InteractionTotalSurvey' => $InteractionData[$i]['InteractionTotalSurvey'],
            'FixedNPS' => $FixedData[$i]['FixedNpsScore'],
            'FixedTier' => $FixedData[$i]['FixedTierLevel'],
            'FixedAdvocates' => $FixedData[$i]['FixedTotalAdvocate'],
            'FixedPasives' => $FixedData[$i]['FixedTotalPassive'],
            'FixedDetractors' => $FixedData[$i]['FixedTotalDetractors'],
            'FixedAdvocatesPercentage' => $FixedData[$i]['FixedAdvocatesPercentage'],
            'FixedPasivesPercentage' => $FixedData[$i]['FixedPassivesPercentage'],
            'FixedDetractorsPercentage' => $FixedData[$i]['FixedDetractorsPercentage'],
            'FixedTotalSurvey' => $FixedData[$i]['FixedTotalSurvey'],
            'FixedTotalPoints' => $FixedData[$i]['FixedPoints'],
            'MobileNPS' => $MobileData[$i]['MobileNpsScore'],
            'MobileTier' => $MobileData[$i]['MobileTierLevel'],
            'MobileAdvocates' => $MobileData[$i]['MobileTotalAdvocate'],
            'MobilePasives' => $MobileData[$i]['MobileTotalPassive'],
            'MobileDetractors' => $MobileData[$i]['MobileTotalDetractors'],
            'MobileAdvocatesPercentage' => $MobileData[$i]['MobileAdvocatesPercentage'],
            'MobilePasivesPercentage' => $MobileData[$i]['MobilePassivesPercentage'],
            'MobileDetractorsPercentage' => $MobileData[$i]['MobileDetractorsPercentage'],
            'MobileTotalSurvey' => $MobileData[$i]['MobileTotalSurvey'],
            'MobileTotalPoints' => $MobileData[$i]['MobilePoints'],
            'EpisodeNPS' => $EpisodeData[$i]['EpisodeNpsScore'],
            'EpisodeTier' => $EpisodeData[$i]['EpisodeTierLevel'],
            'EpisodeAdvocates' => $EpisodeData[$i]['EpisodeTotalAdvocate'],
            'EpisodePasives' => $EpisodeData[$i]['EpisodeTotalPassive'],
            'EpisodeDetractors' => $EpisodeData[$i]['EpisodeTotalDetractors'],
            'EpisodeAdvocatesPercentage' => $EpisodeData[$i]['EpisodeAdvocatesPercentage'],
            'EpisodePasivesPercentage' => $EpisodeData[$i]['EpisodePassivesPercentage'],
            'EpisodeDetractorsPercentage' => $EpisodeData[$i]['EpisodeDetractorsPercentage'],
            'EpisodeTotalSurvey' => $EpisodeData[$i]['EpisodeTotalSurvey'],
            'PointsTotal' => $PointsTotal[$i],
            'weight' => $weight[$i]
        );
        $i++;
    }

    foreach ($InteractionData as $sort) {
        $toSortNpsScoreInteraction[] = $sort['InteractionNpsScore'];
    }

    foreach ($MobileData as $sort) {
        $toSortNpsScoreMobile[] = $sort['MobileNpsScore'];
    }

    foreach ($FixedData as $sort) {
        $toSortNpsScoreFixed[] = $sort['FixedNpsScore'];
    }
    foreach ($EpisodeData as $sort) {
        $toSortNpsScoreEpisode[] = $sort['EpisodeNpsScore'];
    }

    foreach ($MergeData as $sort) {
        $toWeight[] = $sort['weight'];
    }

    array_multisort($toSortNpsScoreInteraction, SORT_DESC, $InteractionData);
    array_multisort($toSortNpsScoreMobile, SORT_DESC, $MobileData);
    array_multisort($toSortNpsScoreFixed, SORT_DESC, $FixedData);
    array_multisort($toSortNpsScoreEpisode, SORT_DESC, $EpisodeData);
    array_multisort($toWeight, SORT_DESC, $MergeData);
// array_multisort($toSortNpsScoreInteraction, SORT_DESC, $toSortNpsScoreMobile, SORT_DESC, $FixedData, SORT_ASC, $MergeData);

    $returnData = array('Interaction' => $InteractionData, 'Episode' => $EpisodeData, 'Mobile' => $MobileData, 'Fixed' => $FixedData, 'Merged' => $MergeData);

//    echo "<pre> here" . $tierSort;
//    print_r($weight);
//    //  var_dump($returnData);
//    exit;
    return $returnData;
}

function getStoreNpsTLSOld($shared = NULL, $profileID = NULL, $empID = NULL, $to = NULL, $from = NULL, $role = NULL, $storeType = 1, $rangeType = NULL, $tierSort = 'Episode', $tierReport = NULL, $quarter = NULL) {
    global $connection;

    if ($shared != 'no') {
        $privateStore = " and c.f_StoreOwnerUserID NOT IN (SELECT f_StoreOwnerUserID FROM t_privateowner) ";
    }

    date_default_timezone_set('Australia/Melbourne');
    $sqlReportType = " ";

    $sqlReportInteraction = " AND b.f_SurveyID IN ('1','2','3','4','5','6') ";
    $sqlReportFixed = " AND b.f_SurveyID IN ('1','2','3','4','6','13','14','15')
                         AND b.f_ProductID IN ('4','5','6','7','9','10','11','12','13','15','17','18','22','30','37','38','41','19') ";
    $sqlReportMobile = " AND b.f_SurveyID IN ('1','2','3','4','13','14','15')
                         AND b.f_ProductID IN ('1','2','3','14','39','40','36')";

//    if (!is_null($to) && !is_null($from)) {
//        $to = date("Y-m-d", strtotime($to));
//        $from = date("Y-m-d", strtotime($from));
//
//        $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
//    } else {
//        $sqlWhen = " AND MONTH(a.f_DateTimeResponseMelbourne) = MONTH(CURDATE())
//                     AND YEAR(a.f_DateTimeResponseMelbourne) = YEAR(CURDATE())";
//    }
    $from = "2019-07-01";
    $to = "2019-09-30";
    $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";

    $role = $_SESSION['UserRoleID'];

    $sqlStoreType = " ";
    if (!is_null($storeType) && $storeType != "") {
        $sqlStoreType = " AND c.f_StoreTypeID = '{$storeType}'";
    }
    switch ($role) {
        case '1':
            $sqlShared = " ";
            break;
        case '2':

        case '3':
            if ($shared == "no") {

                $selectCollab = "SELECT f_StoreCollabWithUserID as 'CollabWith' FROM t_storecollab WHERE f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}'";
                $resultCollab = mysql_query($selectCollab, $connection);
                while ($row = mysql_fetch_assoc($resultCollab)) {
                    $CollabWith[] = $row['CollabWith'];
                }
                if (count($CollabWith) >= 1) {
                    array_push($CollabWith, $_SESSION['UserProfileID']);
                    $whereInCollab = implode(', ', $CollabWith);
                    $sqlShared = " AND c.f_StoreOwnerUserID in ({$whereInCollab})";
                } else {
                    $sqlShared = " AND c.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
                }
            } else {
                $sqlShared = " ";
            }
            break;
        case '4':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $data = $row['f_StoreOwnerUserID'];
            }
            $sqlShared = " AND c.f_StoreOwnerUserID = '{$data}' ";
            break;
        case '5':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $OwnerID = $row['f_StoreOwnerUserID'];
            }

            if ($shared != "no") {
                $sqlgetPartners = "select a.f_StoreOwnerUserID FROM t_storelist a
            LEFT JOIN t_sharedstore b on a.f_StoreID = b.f_StoreID
            WHERE b.f_ShareToUserID = '{$OwnerID}' group by a.f_StoreOwnerUserID";
                $result = mysql_query($sqlgetPartners, $connection);
                while ($row = mysql_fetch_assoc($result)) {
                    $storePartners[] = $row['f_StoreOwnerUserID'];
                }
                foreach ($storePartners as $data) {
                    $quotedString .= "'$data',";
                }
                $quotedString .= "'{$_SESSION['UserProfileID']}',";
                $quotedString = trim($quotedString, ",");
// var_dump($storePartners);
                $sqlShared = " ";
            } else {
                $sqlShared = " AND c.f_StoreOwnerUserID = '{$OwnerID}' ";
            }
            break;
        case '6':
            $sqlShared = " AND c.f_StoreOwnerUserID = '{$profileID}' ";
            break;
    }
    $sqlGetTiers = "SELECT * From t_storeTier where f_StoreTypeID = 1";
    $tierResult = mysql_query($sqlGetTiers, $connection);
    while ($row = mysql_fetch_assoc($tierResult)) {
        $Tiers[$row['f_NpsType']][] = array("max" => $row["f_NpsMax"], "min" => $row["f_NpsMin"],
            "tier" => $row["f_TierLevel"], "bapps" => $row["f_BappsPoints"], "bappsPercent" => $row["f_BappsPercentage"]);
    }

    $sqlStoreNps = "SELECT
    c.f_StoreID, c.f_StoreListName as StoreName, w.*, x.* , z.* , y.*
    FROM
        t_surveysd a
            LEFT JOIN
        t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
            LEFT JOIN
        t_storelist c ON a.f_StoreID = c.f_StoreID
            LEFT JOIN
        (SELECT
            DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%m-%d-%Y') AS InteractionDate,
                c.f_StoreID as 'InteractionStoreID',
                SUM(CASE
                    WHEN
                        (a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                            AND (a.f_ActivationExtra5 != 'P000000')
                    THEN
                        1
                    ELSE 0
                END) AS InteractionTotalAdvocate,
                SUM(CASE
                    WHEN
                        (a.f_LikelihoodToRecommendRetail BETWEEN '7' AND '8')
                            AND (a.f_ActivationExtra5 != 'P000000')
                    THEN
                        1
                    ELSE 0
                END) AS InteractionTotalPassive,
                SUM(CASE
                    WHEN
                        (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                            AND (a.f_ActivationExtra5 != 'P000000')
                    THEN
                        1
                    ELSE 0
                END) AS InteractionTotalDetractors,
                SUM(CASE
                    WHEN
                        a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                            AND a.f_ActivationExtra5 != 'P000000'
                    THEN
                        1
                    ELSE 0
                END) AS InteractionTotalSurvey,
                SUM(CASE
                    WHEN
                         ( trim(a.f_LikelihoodToRecommendRetail) IS NULL)
                            AND a.f_ActivationExtra5 != 'P000000'
                    THEN
                        1
                    ELSE 0
                END) AS InteractionNoScore,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = 'P000000' THEN 1
                    ELSE 0
                END) AS InteractionPZero,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = '' THEN 1
                    ELSE 0
                END) AS InteractionUnAssigned,
                COUNT(a.f_SurveySDID) AS InteractionRawTotal
        FROM
            t_surveysd a
        LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID
        WHERE
            1 = 1 AND c.f_Status = 1  AND c.f_StoreTypeID = '1'
            {$sqlWhen} {$sqlReportInteraction}
        GROUP BY c.f_StoreID) AS w ON c.f_StoreID = w.InteractionStoreID
            LEFT JOIN
        (SELECT
            DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%m-%d-%Y') AS MobileTotalDate,
                c.f_StoreID as 'MobileStoreID',
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '9' AND '10' THEN 1
                    ELSE 0
                END) AS MobileTotalAdvocate,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '7' AND '8' THEN 1
                    ELSE 0
                END) AS MobileTotalPassive,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '0' AND '6' THEN 1
                    ELSE 0
                END) AS MobileTotalDetractors,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
                    ELSE 0
                END) AS MobileTotalSurvey,
                SUM(CASE
                    WHEN (a.f_RecommendRate IS NULL) THEN 1
                    ELSE 0
                END) AS MobileNoScore,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = 'P000000' THEN 1
                    ELSE 0
                END) AS MobilePZero,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = '' THEN 1
                    ELSE 0
                END) AS MobileUnAssigned,
                COUNT(a.f_SurveySDID) AS MobileRawTotal
        FROM
            t_surveysd a
        LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID
        WHERE
            1 = 1 AND c.f_Status = 1 AND c.f_StoreTypeID = '1'
                 {$sqlWhen} {$sqlReportMobile}
        GROUP BY c.f_StoreID) AS x ON c.f_StoreID = x.MobileStoreID
            LEFT JOIN
        (SELECT
            c.f_StoreID as 'FixedStoreID',
                DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%m-%d-%Y') AS FixedDate,
                 SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '9' AND '10' THEN 1
                    ELSE 0
                END) AS FixedTotalAdvocate,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '7' AND '8' THEN 1
                    ELSE 0
                END) AS FixedTotalPassive,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '0' AND '6' THEN 1
                    ELSE 0
                END) AS FixedTotalDetractors,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
                    ELSE 0
                END) AS FixedTotalSurvey,
                SUM(CASE
                    WHEN (a.f_RecommendRate IS NULL) THEN 1
                    ELSE 0
                END) AS FixedNoScore,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = 'P000000' THEN 1
                    ELSE 0
                END) AS FixedPZero,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = '' THEN 1
                    ELSE 0
                END) AS FixedUnAssigned,
                COUNT(a.f_SurveySDID) AS FixedRawTotal

        FROM
            t_surveysd a
        LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID
        WHERE
            1 = 1 AND c.f_Status = 1 AND c.f_StoreTypeID = '1'
               {$sqlWhen} {$sqlReportFixed}
        GROUP BY c.f_StoreID) AS z ON c.f_StoreID = z.FixedStoreID
        LEFT JOIN
        (SELECT
            DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%m-%d-%Y') AS EpisodeTotalDate,
                c.f_StoreID as 'EpisodeStoreID',
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '9' AND '10' THEN 1
                    ELSE 0
                END) AS EpisodeTotalAdvocate,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '7' AND '8' THEN 1
                    ELSE 0
                END) AS EpisodeTotalPassive,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '0' AND '6' THEN 1
                    ELSE 0
                END) AS EpisodeTotalDetractors,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
                    ELSE 0
                END) AS EpisodeTotalSurvey,
                SUM(CASE
                    WHEN (a.f_RecommendRate IS NULL) THEN 1
                    ELSE 0
                END) AS EpisodeNoScore,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = 'P000000' THEN 1
                    ELSE 0
                END) AS EpisodePZero,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = '' THEN 1
                    ELSE 0
                END) AS EpisodeUnAssigned,
                COUNT(a.f_SurveySDID) AS EpisodeRawTotal
        FROM
            t_surveysd a
        LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID
        WHERE
            1 = 1 AND c.f_Status = 1 AND c.f_StoreTypeID = '1'
                 {$sqlWhen}
        GROUP BY c.f_StoreID) AS y ON c.f_StoreID = y.EpisodeStoreID
    WHERE
        1 = 1 AND c.f_Status = 1 AND c.f_StoreTypeID = '1' {$sqlShared} {$privateStore}
    GROUP BY c.f_StoreID";
//    echo "<pre>";
//    echo $sqlStoreNps;
//    exit;
    mysql_query("SELECT 1=1;", $connection);
    $resultStoreNps = mysql_query($sqlStoreNps, $connection);

    $i = 0;
    while ($rowStoreNps = mysql_fetch_assoc($resultStoreNps)) {

        $InteractionData[$i] = $rowStoreNps;
        $StoreNpsAdvocatePercentage = $rowStoreNps['InteractionTotalAdvocate'] / $rowStoreNps['InteractionTotalSurvey'] * 100;
        $InteractionData[$i]['InteractionAdvocatesPercentage'] = $StoreNpsAdvocatePercentage;
        $StoreNpsPassivesPercentage = $rowStoreNps['InteractionTotalPassive'] / $rowStoreNps['InteractionTotalSurvey'] * 100;
        $InteractionData[$i]['InteractionPassivesPercentage'] = $StoreNpsPassivesPercentage;
        $StoreNpsDetractorsPercentage = $rowStoreNps['InteractionTotalDetractors'] / $rowStoreNps['InteractionTotalSurvey'] * 100;
        $InteractionData[$i]['InteractionDetractorsPercentage'] = $StoreNpsDetractorsPercentage;
        $InteractionData[$i]['InteractionNpsScore'] = number_format($StoreNpsAdvocatePercentage - $StoreNpsDetractorsPercentage, npsDecimal());

        $InteractionData[$i]['InteractionTierLevel'] = " ";

        $FixedData[$i] = $rowStoreNps;
        $FixedAdvocatePercentage = intval($rowStoreNps['FixedTotalAdvocate']) / intval($rowStoreNps['FixedTotalSurvey']) * 100;
        $FixedData[$i]['FixedAdvocatesPercentage'] = $FixedAdvocatePercentage;
        $FixedPassivesPercentage = $rowStoreNps['FixedTotalPassive'] / $rowStoreNps['FixedTotalSurvey'] * 100;
        $FixedData[$i]['FixedPassivesPercentage'] = $FixedPassivesPercentage;
        $FixedDetractorsPercentage = $rowStoreNps['FixedTotalDetractors'] / intval($rowStoreNps['FixedTotalSurvey']) * 100;
        $FixedData[$i]['FixedDetractorsPercentage'] = $FixedDetractorsPercentage;
        $FixedData[$i]['FixedNpsScore'] = number_format($FixedAdvocatePercentage - $FixedDetractorsPercentage, npsDecimal());

        $MobileData[$i] = $rowStoreNps;
        $MobileAdvocatePercentage = intval($rowStoreNps['MobileTotalAdvocate']) / intval($rowStoreNps['MobileTotalSurvey']) * 100;
        $MobileData[$i]['MobileAdvocatesPercentage'] = $MobileAdvocatePercentage;
        $MobilePassivesPercentage = $rowStoreNps['MobileTotalPassive'] / $rowStoreNps['MobileTotalSurvey'] * 100;
        $MobileData[$i]['MobilePassivesPercentage'] = $MobilePassivesPercentage;
        $MobileDetractorsPercentage = $rowStoreNps['MobileTotalDetractors'] / $rowStoreNps['MobileTotalSurvey'] * 100;
        $MobileData[$i]['MobileDetractorsPercentage'] = $MobileDetractorsPercentage;
        $MobileData[$i]['MobileNpsScore'] = number_format($MobileAdvocatePercentage - $MobileDetractorsPercentage, npsDecimal());

        $EpisodeData[$i] = $rowStoreNps;
        $EpisodeAdvocatePercentage = intval($rowStoreNps['EpisodeTotalAdvocate']) / intval($rowStoreNps['EpisodeTotalSurvey']) * 100;
        $EpisodeData[$i]['EpisodeAdvocatesPercentage'] = $EpisodeAdvocatePercentage;
        $EpisodePassivesPercentage = $rowStoreNps['EpisodeTotalPassive'] / $rowStoreNps['EpisodeTotalSurvey'] * 100;
        $EpisodeData[$i]['EpisodePassivesPercentage'] = $EpisodePassivesPercentage;
        $EpisodeDetractorsPercentage = $rowStoreNps['EpisodeTotalDetractors'] / $rowStoreNps['EpisodeTotalSurvey'] * 100;
        $EpisodeData[$i]['EpisodeDetractorsPercentage'] = $EpisodeDetractorsPercentage;
        $EpisodeData[$i]['EpisodeNpsScore'] = number_format($EpisodeAdvocatePercentage - $EpisodeDetractorsPercentage, npsDecimal());


        if ($InteractionData[$i]['InteractionNpsScore'] >= 80) {
            $InteractionData[$i]['InteractionTierLevel'] = 'Tier 1';
        } elseif ($InteractionData[$i]['InteractionNpsScore'] >= 75 AND $InteractionData[$i]['InteractionNpsScore'] <= 79) {
            $InteractionData[$i]['InteractionTierLevel'] = 'Tier 2';
        } elseif ($InteractionData[$i]['InteractionNpsScore'] >= 68 AND $InteractionData[$i]['InteractionNpsScore'] <= 74) {
            $InteractionData[$i]['InteractionTierLevel'] = 'Tier 3';
        } elseif ($InteractionData[$i]['InteractionNpsScore'] >= 58 AND $InteractionData[$i]['InteractionNpsScore'] <= 67) {
            $InteractionData[$i]['InteractionTierLevel'] = 'Tier 4';
        } elseif ($InteractionData[$i]['InteractionNpsScore'] < 58) {
            $InteractionData[$i]['InteractionTierLevel'] = 'Tier 5';
        }

        if ($EpisodeData[$i]['EpisodeNpsScore'] >= 50) {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 1';
        } elseif ($EpisodeData[$i]['EpisodeNpsScore'] >= 40 AND $EpisodeData[$i]['EpisodeNpsScore'] <= 49) {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 2';
        } elseif ($EpisodeData[$i]['EpisodeNpsScore'] >= 30 AND $EpisodeData[$i]['EpisodeNpsScore'] <= 39) {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 3';
        } elseif ($EpisodeData[$i]['EpisodeNpsScore'] >= 0 AND $EpisodeData[$i]['EpisodeNpsScore'] <= 29) {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 4';
        } else {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 5';
        }



//        $fixCounter = 0;
//        foreach ($Tiers["Fixed"] as $TierData) {
//
//            if ($fixCounter == 0) {
//                if ($FixedData[$i]['FixedNpsScore'] >= $TierData['min']) {
//                    $FixedData[$i]['FixedTierLevel'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
//                    $FixedData[$i]['FixedPoints'] = $TierData['bapps'];
//                }
//            } elseif ($fixCounter == count($TierData) ) {
//                if ($FixedData[$i]['FixedNpsScore'] < $TierData['min']) {
//                    $FixedData[$i]['FixedTierLevel'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
//                    $FixedData[$i]['FixedPoints'] = $TierData['bapps'];
//                }
//            } else {
//                if ($FixedData[$i]['FixedNpsScore'] >= $TierData['min'] AND $FixedData[$i]['FixedNpsScore'] <= $TierData['max']) {
//                    $FixedData[$i]['FixedTierLevel'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
//                    $FixedData[$i]['FixedPoints'] = $TierData['bapps'];
//                }
//            }
//            $fixCounter = $fixCounter + 1;
//        }
//
//        $mobCounter = 0;
//        foreach ($Tiers["Mobile"] as $TierData) {
//
//            if ($mobCounter == 0) {
//                if ($MobileData[$i]['MobileNpsScore'] >= $TierData['min']) {
//                    $MobileData[$i]['MobileTierLevel'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
//                    $MobileData[$i]['MobilePoints'] = $TierData['bapps'];
//                }
//            } elseif ($mobCounter == count($TierData) ) {
//                if ($MobileData[$i]['MobileNpsScore'] < $TierData['min']) {
//                    $MobileData[$i]['MobileTierLevel'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
//                    $MobileData[$i]['MobilePoints'] = $TierData['bapps'];
//                }
//            } else {
//                if ($MobileData[$i]['MobileNpsScore'] >= $TierData['min'] AND $MobileData[$i]['MobileNpsScore'] <= $TierData['max']) {
//                    $MobileData[$i]['MobileTierLevel'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
//                    $MobileData[$i]['MobilePoints'] = $TierData['bapps'];
//                }
//            }
//            $mobCounter = $mobCounter + 1;
//        }


        if ($FixedData[$i]['FixedNpsScore'] >= 58) {
            $FixedData[$i]['FixedTierLevel'] = 'Tier 1 - 140% ';
            $FixedData[$i]['FixedPoints'] = 50;
        } elseif ($FixedData[$i]['FixedNpsScore'] >= 51 AND $FixedData[$i]['FixedNpsScore'] <= 57) {
            $FixedData[$i]['FixedTierLevel'] = 'Tier 2 - 120% ';
            $FixedData[$i]['FixedPoints'] = 45;
        } elseif ($FixedData[$i]['FixedNpsScore'] >= 45 AND $FixedData[$i]['FixedNpsScore'] <= 50) {
            $FixedData[$i]['FixedTierLevel'] = 'Tier 3 - 110% ';
            $FixedData[$i]['FixedPoints'] = 40;
        } elseif ($FixedData[$i]['FixedNpsScore'] >= 39 AND $FixedData[$i]['FixedNpsScore'] <= 44) {
            $FixedData[$i]['FixedTierLevel'] = 'Tier 4 - 100% ';
            $FixedData[$i]['FixedPoints'] = 30;
        } elseif ($FixedData[$i]['FixedNpsScore'] < 39) {
            $FixedData[$i]['FixedTierLevel'] = 'Tier 5 - 60%';
            $FixedData[$i]['FixedPoints'] = 15;
        }
        if ($MobileData[$i]['MobileNpsScore'] >= 72) {
            $MobileData[$i]['MobileTierLevel'] = 'Tier 1  - 140%';
            $MobileData[$i]['MobilePoints'] = 50;
        } elseif ($MobileData[$i]['MobileNpsScore'] >= 68 AND $MobileData[$i]['MobileNpsScore'] <= 71) {
            $MobileData[$i]['MobileTierLevel'] = 'Tier 2  - 120%';
            $MobileData[$i]['MobilePoints'] = 45;
        } elseif ($MobileData[$i]['MobileNpsScore'] >= 64 AND $MobileData[$i]['MobileNpsScore'] <= 67) {
            $MobileData[$i]['MobileTierLevel'] = 'Tier 3 - 110% ';
            $MobileData[$i]['MobilePoints'] = 40;
        } elseif ($MobileData[$i]['MobileNpsScore'] >= 59 AND $MobileData[$i]['MobileNpsScore'] <= 63) {
            $MobileData[$i]['MobileTierLevel'] = 'Tier 4 - 100%';
            $MobileData[$i]['MobilePoints'] = 30;
        } elseif ($MobileData[$i]['MobileNpsScore'] < 59) {
            $MobileData[$i]['MobileTierLevel'] = 'Tier 5 - 60%';
            $MobileData[$i]['MobilePoints'] = 15;
        }
        $PointsTotal[$i] = $MobileData[$i]['MobilePoints'] + $FixedData[$i]['FixedPoints'];

        /** else {

          if ($FixedData[$i]['FixedNpsScore'] >= 46) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 1';
          } elseif ($FixedData[$i]['FixedNpsScore'] >= 41 AND $FixedData[$i]['FixedNpsScore'] <= 45) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 2';
          } elseif ($FixedData[$i]['FixedNpsScore'] >= 31 AND $FixedData[$i]['FixedNpsScore'] <= 40) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 3';
          } elseif ($FixedData[$i]['FixedNpsScore'] >= 16 AND $FixedData[$i]['FixedNpsScore'] <= 30) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 4';
          } elseif ($FixedData[$i]['FixedNpsScore'] < 16) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 5';
          }

          if ($MobileData[$i]['MobileNpsScore'] >= 67) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 1';
          } elseif ($MobileData[$i]['MobileNpsScore'] >= 62 AND $MobileData[$i]['MobileNpsScore'] <= 66) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 2';
          } elseif ($MobileData[$i]['MobileNpsScore'] >= 53 AND $MobileData[$i]['MobileNpsScore'] <= 61) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 3';
          } elseif ($MobileData[$i]['MobileNpsScore'] >= 44 AND $MobileData[$i]['MobileNpsScore'] <= 52) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 4';
          } elseif ($MobileData[$i]['MobileNpsScore'] < 44) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 5';
          }
          } */
        switch ($tierSort) {
            case 'Interaction':
                $weight[$i] = $InteractionData[$i]['InteractionNpsScore'];
                break;
            case 'Episode':
                $weight[$i] = $EpisodeData[$i]['EpisodeNpsScore'];
                break;
            case 'Mobile':
                $weight[$i] = $MobileData[$i]['MobileNpsScore'];
                break;
            case 'Fixed':
                $weight[$i] = $FixedData[$i]['FixedNpsScore'];
                break;
            default:
                $weight[$i] = $InteractionData[$i]['EpisodeNpsScore'];
                break;
        }


        $MergeData[$i] = array('StoreName' => $rowStoreNps['StoreName'],
            'InteractionNPS' => $InteractionData[$i]['InteractionNpsScore'],
            'InteractionTier' => $InteractionData[$i]['InteractionTierLevel'],
            'InteractionAdvocates' => $InteractionData[$i]['InteractionTotalAdvocate'],
            'InteractionPasives' => $InteractionData[$i]['InteractionTotalPassive'],
            'InteractionDetractors' => $InteractionData[$i]['InteractionTotalDetractors'],
            'InteractionAdvocatesPercentage' => $InteractionData[$i]['InteractionAdvocatesPercentage'],
            'InteractionPasivesPercentage' => $InteractionData[$i]['InteractionPassivesPercentage'],
            'InteractionDetractorsPercentage' => $InteractionData[$i]['InteractionDetractorsPercentage'],
            'InteractionTotalSurvey' => $InteractionData[$i]['InteractionTotalSurvey'],
            'FixedNPS' => $FixedData[$i]['FixedNpsScore'],
            'FixedTier' => $FixedData[$i]['FixedTierLevel'],
            'FixedAdvocates' => $FixedData[$i]['FixedTotalAdvocate'],
            'FixedPasives' => $FixedData[$i]['FixedTotalPassive'],
            'FixedDetractors' => $FixedData[$i]['FixedTotalDetractors'],
            'FixedAdvocatesPercentage' => $FixedData[$i]['FixedAdvocatesPercentage'],
            'FixedPasivesPercentage' => $FixedData[$i]['FixedPassivesPercentage'],
            'FixedDetractorsPercentage' => $FixedData[$i]['FixedDetractorsPercentage'],
            'FixedTotalSurvey' => $FixedData[$i]['FixedTotalSurvey'],
            'FixedTotalPoints' => $FixedData[$i]['FixedPoints'],
            'MobileNPS' => $MobileData[$i]['MobileNpsScore'],
            'MobileTier' => $MobileData[$i]['MobileTierLevel'],
            'MobileAdvocates' => $MobileData[$i]['MobileTotalAdvocate'],
            'MobilePasives' => $MobileData[$i]['MobileTotalPassive'],
            'MobileDetractors' => $MobileData[$i]['MobileTotalDetractors'],
            'MobileAdvocatesPercentage' => $MobileData[$i]['MobileAdvocatesPercentage'],
            'MobilePasivesPercentage' => $MobileData[$i]['MobilePassivesPercentage'],
            'MobileDetractorsPercentage' => $MobileData[$i]['MobileDetractorsPercentage'],
            'MobileTotalSurvey' => $MobileData[$i]['MobileTotalSurvey'],
            'MobileTotalPoints' => $MobileData[$i]['MobilePoints'],
            'EpisodeNPS' => $EpisodeData[$i]['EpisodeNpsScore'],
            'EpisodeTier' => $EpisodeData[$i]['EpisodeTierLevel'],
            'EpisodeAdvocates' => $EpisodeData[$i]['EpisodeTotalAdvocate'],
            'EpisodePasives' => $EpisodeData[$i]['EpisodeTotalPassive'],
            'EpisodeDetractors' => $EpisodeData[$i]['EpisodeTotalDetractors'],
            'EpisodeAdvocatesPercentage' => $EpisodeData[$i]['EpisodeAdvocatesPercentage'],
            'EpisodePasivesPercentage' => $EpisodeData[$i]['EpisodePassivesPercentage'],
            'EpisodeDetractorsPercentage' => $EpisodeData[$i]['EpisodeDetractorsPercentage'],
            'EpisodeTotalSurvey' => $EpisodeData[$i]['EpisodeTotalSurvey'],
            'PointsTotal' => $PointsTotal[$i],
            'weight' => $weight[$i]
        );
        $i++;
    }

    foreach ($InteractionData as $sort) {
        $toSortNpsScoreInteraction[] = $sort['InteractionNpsScore'];
    }

    foreach ($MobileData as $sort) {
        $toSortNpsScoreMobile[] = $sort['MobileNpsScore'];
    }

    foreach ($FixedData as $sort) {
        $toSortNpsScoreFixed[] = $sort['FixedNpsScore'];
    }
    foreach ($EpisodeData as $sort) {
        $toSortNpsScoreEpisode[] = $sort['EpisodeNpsScore'];
    }

    foreach ($MergeData as $sort) {
        $toWeight[] = $sort['weight'];
    }

    array_multisort($toSortNpsScoreInteraction, SORT_DESC, $InteractionData);
    array_multisort($toSortNpsScoreMobile, SORT_DESC, $MobileData);
    array_multisort($toSortNpsScoreFixed, SORT_DESC, $FixedData);
    array_multisort($toSortNpsScoreEpisode, SORT_DESC, $EpisodeData);
    array_multisort($toWeight, SORT_DESC, $MergeData);
// array_multisort($toSortNpsScoreInteraction, SORT_DESC, $toSortNpsScoreMobile, SORT_DESC, $FixedData, SORT_ASC, $MergeData);

    $returnData = array('Interaction' => $InteractionData, 'Episode' => $EpisodeData, 'Mobile' => $MobileData, 'Fixed' => $FixedData, 'Merged' => $MergeData);

//    echo "<pre> here" . $tierSort;
//    print_r($weight);
//    //  var_dump($returnData);
//    exit;
    return $returnData;
}

function getStoreNpsOthers($shared = NULL, $profileID = NULL, $empID = NULL, $to = NULL, $from = NULL, $role = NULL, $storeType = 3, $rangeType = NULL, $tierSort = 'Episode', $tierReport = NULL, $quarter = NULL) {

    global $connection;


    if ($shared != 'no') {
        $privateStore = " and c.f_StoreOwnerUserID NOT IN (SELECT f_StoreOwnerUserID FROM t_privateowner) ";
    }

    date_default_timezone_set('Australia/Melbourne');
    $sqlReportType = " ";

    if ($quarter == "Q3") {
        $sqlReportInteraction = " AND b.f_SurveyID IN ('1','2','3','4','5','6') ";
        $sqlReportFixed = " AND b.f_SurveyID IN ('1','2','3','4','6','13','14','15')
                         AND b.f_ProductID IN ('4','5','6','7','9','10','11','12','13','15','17','18','22','30','37','38','41','19') ";
        $sqlReportMobile = " AND b.f_SurveyID IN ('1','2','3','4','13','14','15')
                         AND b.f_ProductID IN ('1','2','3','14','39','40','36')";
    } else {
        $sqlReportInteraction = " AND b.f_SurveyID IN ('1','2','3','4','5','6') ";
        $sqlReportFixed = " AND b.f_SurveyID IN ('1','2','3','4','6','13','14','15')
                         AND b.f_ProductID IN ('4','5','6','7','9','10','11','12','13','15','17','18','22','30','37','38','41','19','42') ";
        $sqlReportMobile = " AND b.f_SurveyID IN ('1','2','3','4','13','14','15')
                         AND b.f_ProductID IN ('1','2','3','14','39','40','36')";
    }

    if ($quarter == NULL) {

        if (!is_null($to) && !is_null($from)) {
            $to = date("Y-m-d", strtotime($to));
            $from = date("Y-m-d", strtotime($from));
            $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
        } else {
            $sqlWhen = " AND MONTH(a.f_DateTimeResponseMelbourne) = MONTH(CURDATE())
                     AND YEAR(a.f_DateTimeResponseMelbourne) = YEAR(CURDATE())";
        }
    } else {

        if ($quarter == 'Q2') {
            $from = date("Y-10-01");
            $to = date("Y-12-31");
        } elseif ($quarter == 'Q3') {
            $from = date("Y-01-01");
            $to = date("Y-03-31");
        } elseif ($quarter == 'Q4') {
            $from = date("Y-04-01");
            $to = date("Y-06-30");
        } elseif ($quarter == 'Q1') {
            $from = date("Y-07-01");
            $to = date("Y-09-30");
        } elseif ($quarter == 'OldQ2') {
            $from = date("Y-10-01", strtotime("-1 year"));
            $to = date("Y-12-31", strtotime("-1 year"));
//            $from = date("Y-10-01");
//            $to = date("Y-12-31");
        } elseif ($quarter == 'H2') {  
            $from = date("Y-01-01");
            $to = date("Y-06-30");
            $quarter = "Q3";
        } elseif ($quarter == 'H1') {
            $from = date("Y-07-01");
            $to = date("Y-12-31");
            $quarter = "Q1";
        }

        $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
    }



    $role = $_SESSION['UserRoleID'];

    $sqlStoreType = " ";
    if (!is_null($storeType) && $storeType != "") {
        $sqlStoreType = " AND c.f_StoreTypeID = '{$storeType}'";
    }
    switch ($role) {
        case '1':
            $sqlShared = " ";
            break;
        case '2':

        case '3':
            if ($shared == "no") {

                $selectCollab = "SELECT f_StoreCollabWithUserID as 'CollabWith' FROM t_storecollab WHERE f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}'";
                $resultCollab = mysql_query($selectCollab, $connection);
                while ($row = mysql_fetch_assoc($resultCollab)) {
                    $CollabWith[] = $row['CollabWith'];
                }
                if (count($CollabWith) >= 1) {
                    array_push($CollabWith, $_SESSION['UserProfileID']);
                    $whereInCollab = implode(', ', $CollabWith);
                    $sqlShared = " AND c.f_StoreOwnerUserID in ({$whereInCollab})";
                } else {
                    $sqlShared = " AND c.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
                }
            } else {
                $sqlShared = " ";
            }
            break;
        case '4':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $data = $row['f_StoreOwnerUserID'];
            }
            $sqlShared = " AND c.f_StoreOwnerUserID = '{$data}' ";
            break;
        case '5':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $OwnerID = $row['f_StoreOwnerUserID'];
            }

            if ($shared != "no") {
                $sqlgetPartners = "select a.f_StoreOwnerUserID FROM t_storelist a
            LEFT JOIN t_sharedstore b on a.f_StoreID = b.f_StoreID
            WHERE b.f_ShareToUserID = '{$OwnerID}' group by a.f_StoreOwnerUserID";
                $result = mysql_query($sqlgetPartners, $connection);
                while ($row = mysql_fetch_assoc($result)) {
                    $storePartners[] = $row['f_StoreOwnerUserID'];
                }
                foreach ($storePartners as $data) {
                    $quotedString .= "'$data',";
                }
                $quotedString .= "'{$_SESSION['UserProfileID']}',";
                $quotedString = trim($quotedString, ",");
// var_dump($storePartners);
                $sqlShared = " ";
            } else {
                $sqlShared = " AND c.f_StoreOwnerUserID = '{$OwnerID}' ";
            }
            break;
        case '6':
            $sqlShared = " AND c.f_StoreOwnerUserID = '{$profileID}' ";
            break;
    }

    if (is_null($quarter)) {
        $qT = " and f_Quarter = 'Q1'";
    } elseif ($quarter == 'OldQ2') {
        $qT = " and f_Quarter = 'Q2'";
    } else {
        $qT = " and f_Quarter = '{$quarter}'";
    }


    $sqlGetTiers = "SELECT * From t_storeTier where f_StoreTypeID = 3 {$qT}";
    $tierResult = mysql_query($sqlGetTiers, $connection);
    while ($row = mysql_fetch_assoc($tierResult)) {
        $Tiers[$row['f_NpsType']][] = array("max" => $row["f_NpsMax"], "min" => $row["f_NpsMin"],
            "tier" => $row["f_TierLevel"], "bapps" => $row["f_BappsPoints"], "bappsPercent" => $row["f_BappsPercentage"]);
    }

    $sqlStoreNps = "SELECT
    c.f_StoreID, c.f_StoreListName as StoreName, w.*, x.* , z.* , y.*
    FROM
        t_surveysd a
            LEFT JOIN
        t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
            LEFT JOIN
        t_storelist c ON a.f_StoreID = c.f_StoreID
            LEFT JOIN
        (SELECT
            DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%m-%d-%Y') AS InteractionDate,
                c.f_StoreID as 'InteractionStoreID',
                SUM(CASE
                    WHEN
                        (a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                            AND (a.f_ActivationExtra5 != 'P000000')
                    THEN
                        1
                    ELSE 0
                END) AS InteractionTotalAdvocate,
                SUM(CASE
                    WHEN
                        (a.f_LikelihoodToRecommendRetail BETWEEN '7' AND '8')
                            AND (a.f_ActivationExtra5 != 'P000000')
                    THEN
                        1
                    ELSE 0
                END) AS InteractionTotalPassive,
                SUM(CASE
                    WHEN
                        (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                            AND (a.f_ActivationExtra5 != 'P000000')
                    THEN
                        1
                    ELSE 0
                END) AS InteractionTotalDetractors,
                SUM(CASE
                    WHEN
                        a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                            AND a.f_ActivationExtra5 != 'P000000'
                    THEN
                        1
                    ELSE 0
                END) AS InteractionTotalSurvey,
                SUM(CASE
                    WHEN
                         ( trim(a.f_LikelihoodToRecommendRetail) IS NULL)
                            AND a.f_ActivationExtra5 != 'P000000'
                    THEN
                        1
                    ELSE 0
                END) AS InteractionNoScore,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = 'P000000' THEN 1
                    ELSE 0
                END) AS InteractionPZero,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = '' THEN 1
                    ELSE 0
                END) AS InteractionUnAssigned,
                COUNT(a.f_SurveySDID) AS InteractionRawTotal
        FROM
            t_surveysd a
        LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID
        WHERE
            1 = 1 AND c.f_Status = 1  AND c.f_StoreTypeID = '3'
            {$sqlWhen} {$sqlReportInteraction}
        GROUP BY c.f_StoreID) AS w ON c.f_StoreID = w.InteractionStoreID
            LEFT JOIN
        (SELECT
            DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%m-%d-%Y') AS MobileTotalDate,
                c.f_StoreID as 'MobileStoreID',
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '9' AND '10' THEN 1
                    ELSE 0
                END) AS MobileTotalAdvocate,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '7' AND '8' THEN 1
                    ELSE 0
                END) AS MobileTotalPassive,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '0' AND '6' THEN 1
                    ELSE 0
                END) AS MobileTotalDetractors,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
                    ELSE 0
                END) AS MobileTotalSurvey,
                SUM(CASE
                    WHEN (a.f_RecommendRate IS NULL) THEN 1
                    ELSE 0
                END) AS MobileNoScore,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = 'P000000' THEN 1
                    ELSE 0
                END) AS MobilePZero,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = '' THEN 1
                    ELSE 0
                END) AS MobileUnAssigned,
                COUNT(a.f_SurveySDID) AS MobileRawTotal
        FROM
            t_surveysd a
        LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID
        WHERE
            1 = 1 AND c.f_Status = 1 AND c.f_StoreTypeID = '3'
                 {$sqlWhen} {$sqlReportMobile}
        GROUP BY c.f_StoreID) AS x ON c.f_StoreID = x.MobileStoreID
            LEFT JOIN
        (SELECT
            c.f_StoreID as 'FixedStoreID',
                DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%m-%d-%Y') AS FixedDate,
                 SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '9' AND '10' THEN 1
                    ELSE 0
                END) AS FixedTotalAdvocate,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '7' AND '8' THEN 1
                    ELSE 0
                END) AS FixedTotalPassive,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '0' AND '6' THEN 1
                    ELSE 0
                END) AS FixedTotalDetractors,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
                    ELSE 0
                END) AS FixedTotalSurvey,
                SUM(CASE
                    WHEN (a.f_RecommendRate IS NULL) THEN 1
                    ELSE 0
                END) AS FixedNoScore,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = 'P000000' THEN 1
                    ELSE 0
                END) AS FixedPZero,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = '' THEN 1
                    ELSE 0
                END) AS FixedUnAssigned,
                COUNT(a.f_SurveySDID) AS FixedRawTotal

        FROM
            t_surveysd a
        LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID
        WHERE
            1 = 1 AND c.f_Status = 1 AND c.f_StoreTypeID = '3'
               {$sqlWhen} {$sqlReportFixed}
        GROUP BY c.f_StoreID) AS z ON c.f_StoreID = z.FixedStoreID
        LEFT JOIN
        (SELECT
            DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%m-%d-%Y') AS EpisodeTotalDate,
                c.f_StoreID as 'EpisodeStoreID',
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '9' AND '10' THEN 1
                    ELSE 0
                END) AS EpisodeTotalAdvocate,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '7' AND '8' THEN 1
                    ELSE 0
                END) AS EpisodeTotalPassive,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '0' AND '6' THEN 1
                    ELSE 0
                END) AS EpisodeTotalDetractors,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
                    ELSE 0
                END) AS EpisodeTotalSurvey,
                SUM(CASE
                    WHEN (a.f_RecommendRate IS NULL) THEN 1
                    ELSE 0
                END) AS EpisodeNoScore,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = 'P000000' THEN 1
                    ELSE 0
                END) AS EpisodePZero,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = '' THEN 1
                    ELSE 0
                END) AS EpisodeUnAssigned,
                COUNT(a.f_SurveySDID) AS EpisodeRawTotal
        FROM
            t_surveysd a
        LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID
        WHERE
            1 = 1 AND c.f_Status = 1 AND c.f_StoreTypeID = '3'
                 {$sqlWhen}
        GROUP BY c.f_StoreID) AS y ON c.f_StoreID = y.EpisodeStoreID
    WHERE
        1 = 1 AND c.f_Status = 1 AND c.f_StoreTypeID = '3' {$sqlShared} {$privateStore}
    GROUP BY c.f_StoreID";

    mysql_query("SELECT 1=1;", $connection);
    $resultStoreNps = mysql_query($sqlStoreNps, $connection);

    $i = 0;
    while ($rowStoreNps = mysql_fetch_assoc($resultStoreNps)) {

        $InteractionData[$i] = $rowStoreNps;
        $StoreNpsAdvocatePercentage = $rowStoreNps['InteractionTotalAdvocate'] / $rowStoreNps['InteractionTotalSurvey'] * 100;
        $InteractionData[$i]['InteractionAdvocatesPercentage'] = $StoreNpsAdvocatePercentage;
        $StoreNpsPassivesPercentage = $rowStoreNps['InteractionTotalPassive'] / $rowStoreNps['InteractionTotalSurvey'] * 100;
        $InteractionData[$i]['InteractionPassivesPercentage'] = $StoreNpsPassivesPercentage;
        $StoreNpsDetractorsPercentage = $rowStoreNps['InteractionTotalDetractors'] / $rowStoreNps['InteractionTotalSurvey'] * 100;
        $InteractionData[$i]['InteractionDetractorsPercentage'] = $StoreNpsDetractorsPercentage;
        $InteractionData[$i]['InteractionNpsScore'] = number_format($StoreNpsAdvocatePercentage - $StoreNpsDetractorsPercentage, npsDecimal());

        $InteractionData[$i]['InteractionTierLevel'] = " ";

        $FixedData[$i] = $rowStoreNps;
        $FixedAdvocatePercentage = intval($rowStoreNps['FixedTotalAdvocate']) / intval($rowStoreNps['FixedTotalSurvey']) * 100;
        $FixedData[$i]['FixedAdvocatesPercentage'] = $FixedAdvocatePercentage;
        $FixedPassivesPercentage = $rowStoreNps['FixedTotalPassive'] / $rowStoreNps['FixedTotalSurvey'] * 100;
        $FixedData[$i]['FixedPassivesPercentage'] = $FixedPassivesPercentage;
        $FixedDetractorsPercentage = $rowStoreNps['FixedTotalDetractors'] / intval($rowStoreNps['FixedTotalSurvey']) * 100;
        $FixedData[$i]['FixedDetractorsPercentage'] = $FixedDetractorsPercentage;
        $FixedData[$i]['FixedNpsScore'] = number_format($FixedAdvocatePercentage - $FixedDetractorsPercentage, npsDecimal());

        $MobileData[$i] = $rowStoreNps;
        $MobileAdvocatePercentage = intval($rowStoreNps['MobileTotalAdvocate']) / intval($rowStoreNps['MobileTotalSurvey']) * 100;
        $MobileData[$i]['MobileAdvocatesPercentage'] = $MobileAdvocatePercentage;
        $MobilePassivesPercentage = $rowStoreNps['MobileTotalPassive'] / $rowStoreNps['MobileTotalSurvey'] * 100;
        $MobileData[$i]['MobilePassivesPercentage'] = $MobilePassivesPercentage;
        $MobileDetractorsPercentage = $rowStoreNps['MobileTotalDetractors'] / $rowStoreNps['MobileTotalSurvey'] * 100;
        $MobileData[$i]['MobileDetractorsPercentage'] = $MobileDetractorsPercentage;
        $MobileData[$i]['MobileNpsScore'] = number_format($MobileAdvocatePercentage - $MobileDetractorsPercentage, npsDecimal());

        $EpisodeData[$i] = $rowStoreNps;
        $EpisodeAdvocatePercentage = intval($rowStoreNps['EpisodeTotalAdvocate']) / intval($rowStoreNps['EpisodeTotalSurvey']) * 100;
        $EpisodeData[$i]['EpisodeAdvocatesPercentage'] = $EpisodeAdvocatePercentage;
        $EpisodePassivesPercentage = $rowStoreNps['EpisodeTotalPassive'] / $rowStoreNps['EpisodeTotalSurvey'] * 100;
        $EpisodeData[$i]['EpisodePassivesPercentage'] = $EpisodePassivesPercentage;
        $EpisodeDetractorsPercentage = $rowStoreNps['EpisodeTotalDetractors'] / $rowStoreNps['EpisodeTotalSurvey'] * 100;
        $EpisodeData[$i]['EpisodeDetractorsPercentage'] = $EpisodeDetractorsPercentage;
        $EpisodeData[$i]['EpisodeNpsScore'] = number_format($EpisodeAdvocatePercentage - $EpisodeDetractorsPercentage, npsDecimal());


        if ($InteractionData[$i]['InteractionNpsScore'] >= 80) {
            $InteractionData[$i]['InteractionTierLevel'] = 'Tier 1';
        } elseif ($InteractionData[$i]['InteractionNpsScore'] >= 75 AND $InteractionData[$i]['InteractionNpsScore'] <= 79) {
            $InteractionData[$i]['InteractionTierLevel'] = 'Tier 2';
        } elseif ($InteractionData[$i]['InteractionNpsScore'] >= 68 AND $InteractionData[$i]['InteractionNpsScore'] <= 74) {
            $InteractionData[$i]['InteractionTierLevel'] = 'Tier 3';
        } elseif ($InteractionData[$i]['InteractionNpsScore'] >= 58 AND $InteractionData[$i]['InteractionNpsScore'] <= 67) {
            $InteractionData[$i]['InteractionTierLevel'] = 'Tier 4';
        } elseif ($InteractionData[$i]['InteractionNpsScore'] < 58) {
            $InteractionData[$i]['InteractionTierLevel'] = 'Tier 5';
        }

        if ($EpisodeData[$i]['EpisodeNpsScore'] >= 50) {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 1';
        } elseif ($EpisodeData[$i]['EpisodeNpsScore'] >= 40 AND $EpisodeData[$i]['EpisodeNpsScore'] <= 49) {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 2';
        } elseif ($EpisodeData[$i]['EpisodeNpsScore'] >= 30 AND $EpisodeData[$i]['EpisodeNpsScore'] <= 39) {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 3';
        } elseif ($EpisodeData[$i]['EpisodeNpsScore'] >= 0 AND $EpisodeData[$i]['EpisodeNpsScore'] <= 29) {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 4';
        } else {
            $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 5';
        }



        $fixCounter = 0;
        foreach ($Tiers["Fixed"] as $TierData) {


            if ($FixedData[$i]['FixedTotalSurvey'] == 0 ) {
                $FixedData[$i]['FixedTierLevel'] = "No NPS Score";
                $FixedData[$i]['FixedPoints'] = 0;
            } else {
                if ($fixCounter == 0) {
                    if ($FixedData[$i]['FixedNpsScore'] >= $TierData['min']) {
                        $FixedData[$i]['FixedTierLevel'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
                        $FixedData[$i]['FixedPoints'] = $TierData['bapps'];
                    }
                } elseif ($fixCounter == count($TierData)) {
                    if ($FixedData[$i]['FixedNpsScore'] < $TierData['max']) {
                        $FixedData[$i]['FixedTierLevel'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
                        $FixedData[$i]['FixedPoints'] = $TierData['bapps'];
                    }
                } else {
                    if ($FixedData[$i]['FixedNpsScore'] >= $TierData['min'] AND $FixedData[$i]['FixedNpsScore'] < $TierData['max']) {
                        $FixedData[$i]['FixedTierLevel'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
                        $FixedData[$i]['FixedPoints'] = $TierData['bapps'];
                    }
                }
            }


            $fixCounter = $fixCounter + 1;
        }


        $mobCounter = 0;
        foreach ($Tiers["Mobile"] as $TierData) {
            if ($MobileData[$i]['MobileTotalSurvey'] == 0) {
                $MobileData[$i]['MobileTierLevel'] = "No NPS Score";
                $MobileData[$i]['MobilePoints'] = 0;
            } else {
                if ($mobCounter == 0) {
                    if ($MobileData[$i]['MobileNpsScore'] >= $TierData['min']) {
                        $MobileData[$i]['MobileTierLevel'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
                        $MobileData[$i]['MobilePoints'] = $TierData['bapps'];
                    }
                } elseif ($mobCounter == count($TierData)) {
                    if ($MobileData[$i]['MobileNpsScore'] < $TierData['max']) {
                        $MobileData[$i]['MobileTierLevel'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
                        $MobileData[$i]['MobilePoints'] = $TierData['bapps'];
                    }
                } else {
                    if ($MobileData[$i]['MobileNpsScore'] >= $TierData['min'] AND $MobileData[$i]['MobileNpsScore'] < $TierData['max']) {
                        $MobileData[$i]['MobileTierLevel'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
                        $MobileData[$i]['MobilePoints'] = $TierData['bapps'];
                    }
                }
            }

            $mobCounter = $mobCounter + 1;
        }

        /**
          if ($FixedData[$i]['FixedNpsScore'] >= 58) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 1 - 140% ';
          $FixedData[$i]['FixedPoints'] = 50;
          } elseif ($FixedData[$i]['FixedNpsScore'] >= 51 AND $FixedData[$i]['FixedNpsScore'] <= 57) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 2 - 120% ';
          $FixedData[$i]['FixedPoints'] = 45;
          } elseif ($FixedData[$i]['FixedNpsScore'] >= 45 AND $FixedData[$i]['FixedNpsScore'] <= 50) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 3 - 110% ';
          $FixedData[$i]['FixedPoints'] = 40;
          } elseif ($FixedData[$i]['FixedNpsScore'] >= 39 AND $FixedData[$i]['FixedNpsScore'] <= 44) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 4 - 100% ';
          $FixedData[$i]['FixedPoints'] = 30;
          } elseif ($FixedData[$i]['FixedNpsScore'] < 39) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 5 - 60%';
          $FixedData[$i]['FixedPoints'] = 15;
          } */
        /** if ($MobileData[$i]['MobileNpsScore'] >= 72) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 1  - 140%';
          $MobileData[$i]['MobilePoints'] = 50;
          } elseif ($MobileData[$i]['MobileNpsScore'] >= 68 AND $MobileData[$i]['MobileNpsScore'] <= 71) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 2  - 120%';
          $MobileData[$i]['MobilePoints'] = 45;
          } elseif ($MobileData[$i]['MobileNpsScore'] >= 64 AND $MobileData[$i]['MobileNpsScore'] <= 67) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 3 - 110% ';
          $MobileData[$i]['MobilePoints'] = 40;
          } elseif ($MobileData[$i]['MobileNpsScore'] >= 59 AND $MobileData[$i]['MobileNpsScore'] <= 63) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 4 - 100%';
          $MobileData[$i]['MobilePoints'] = 30;
          } elseif ($MobileData[$i]['MobileNpsScore'] < 59) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 5 - 60%';
          $MobileData[$i]['MobilePoints'] = 15;
          } */
        $PointsTotal[$i] = $MobileData[$i]['MobilePoints'] + $FixedData[$i]['FixedPoints'];

        /** else {

          if ($FixedData[$i]['FixedNpsScore'] >= 46) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 1';
          } elseif ($FixedData[$i]['FixedNpsScore'] >= 41 AND $FixedData[$i]['FixedNpsScore'] <= 45) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 2';
          } elseif ($FixedData[$i]['FixedNpsScore'] >= 31 AND $FixedData[$i]['FixedNpsScore'] <= 40) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 3';
          } elseif ($FixedData[$i]['FixedNpsScore'] >= 16 AND $FixedData[$i]['FixedNpsScore'] <= 30) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 4';
          } elseif ($FixedData[$i]['FixedNpsScore'] < 16) {
          $FixedData[$i]['FixedTierLevel'] = 'Tier 5';
          }

          if ($MobileData[$i]['MobileNpsScore'] >= 67) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 1';
          } elseif ($MobileData[$i]['MobileNpsScore'] >= 62 AND $MobileData[$i]['MobileNpsScore'] <= 66) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 2';
          } elseif ($MobileData[$i]['MobileNpsScore'] >= 53 AND $MobileData[$i]['MobileNpsScore'] <= 61) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 3';
          } elseif ($MobileData[$i]['MobileNpsScore'] >= 44 AND $MobileData[$i]['MobileNpsScore'] <= 52) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 4';
          } elseif ($MobileData[$i]['MobileNpsScore'] < 44) {
          $MobileData[$i]['MobileTierLevel'] = 'Tier 5';
          }
          } */
        switch ($tierSort) {
            case 'Interaction':
                $weight[$i] = $InteractionData[$i]['InteractionNpsScore'];
                break;
            case 'Episode':
                $weight[$i] = $EpisodeData[$i]['EpisodeNpsScore'];
                break;
            case 'Mobile':
                $weight[$i] = $MobileData[$i]['MobileNpsScore'];
                break;
            case 'Fixed':
                $weight[$i] = $FixedData[$i]['FixedNpsScore'];
                break;
            default:
                $weight[$i] = $InteractionData[$i]['EpisodeNpsScore'];
                break;
        }


        $MergeData[$i] = array('StoreName' => $rowStoreNps['StoreName'],
            'InteractionNPS' => $InteractionData[$i]['InteractionNpsScore'],
            'InteractionTier' => $InteractionData[$i]['InteractionTierLevel'],
            'InteractionAdvocates' => $InteractionData[$i]['InteractionTotalAdvocate'],
            'InteractionPasives' => $InteractionData[$i]['InteractionTotalPassive'],
            'InteractionDetractors' => $InteractionData[$i]['InteractionTotalDetractors'],
            'InteractionAdvocatesPercentage' => $InteractionData[$i]['InteractionAdvocatesPercentage'],
            'InteractionPasivesPercentage' => $InteractionData[$i]['InteractionPassivesPercentage'],
            'InteractionDetractorsPercentage' => $InteractionData[$i]['InteractionDetractorsPercentage'],
            'InteractionTotalSurvey' => $InteractionData[$i]['InteractionTotalSurvey'],
            'FixedNPS' => $FixedData[$i]['FixedNpsScore'],
            'FixedTier' => $FixedData[$i]['FixedTierLevel'],
            'FixedAdvocates' => $FixedData[$i]['FixedTotalAdvocate'],
            'FixedPasives' => $FixedData[$i]['FixedTotalPassive'],
            'FixedDetractors' => $FixedData[$i]['FixedTotalDetractors'],
            'FixedAdvocatesPercentage' => $FixedData[$i]['FixedAdvocatesPercentage'],
            'FixedPasivesPercentage' => $FixedData[$i]['FixedPassivesPercentage'],
            'FixedDetractorsPercentage' => $FixedData[$i]['FixedDetractorsPercentage'],
            'FixedTotalSurvey' => $FixedData[$i]['FixedTotalSurvey'],
            'FixedTotalPoints' => $FixedData[$i]['FixedPoints'],
            'MobileNPS' => $MobileData[$i]['MobileNpsScore'],
            'MobileTier' => $MobileData[$i]['MobileTierLevel'],
            'MobileAdvocates' => $MobileData[$i]['MobileTotalAdvocate'],
            'MobilePasives' => $MobileData[$i]['MobileTotalPassive'],
            'MobileDetractors' => $MobileData[$i]['MobileTotalDetractors'],
            'MobileAdvocatesPercentage' => $MobileData[$i]['MobileAdvocatesPercentage'],
            'MobilePasivesPercentage' => $MobileData[$i]['MobilePassivesPercentage'],
            'MobileDetractorsPercentage' => $MobileData[$i]['MobileDetractorsPercentage'],
            'MobileTotalSurvey' => $MobileData[$i]['MobileTotalSurvey'],
            'MobileTotalPoints' => $MobileData[$i]['MobilePoints'],
            'EpisodeNPS' => $EpisodeData[$i]['EpisodeNpsScore'],
            'EpisodeTier' => $EpisodeData[$i]['EpisodeTierLevel'],
            'EpisodeAdvocates' => $EpisodeData[$i]['EpisodeTotalAdvocate'],
            'EpisodePasives' => $EpisodeData[$i]['EpisodeTotalPassive'],
            'EpisodeDetractors' => $EpisodeData[$i]['EpisodeTotalDetractors'],
            'EpisodeAdvocatesPercentage' => $EpisodeData[$i]['EpisodeAdvocatesPercentage'],
            'EpisodePasivesPercentage' => $EpisodeData[$i]['EpisodePassivesPercentage'],
            'EpisodeDetractorsPercentage' => $EpisodeData[$i]['EpisodeDetractorsPercentage'],
            'EpisodeTotalSurvey' => $EpisodeData[$i]['EpisodeTotalSurvey'],
            'PointsTotal' => $PointsTotal[$i],
            'weight' => $weight[$i]
        );
        $i++;
    }

    foreach ($InteractionData as $sort) {
        $toSortNpsScoreInteraction[] = $sort['InteractionNpsScore'];
    }

    foreach ($MobileData as $sort) {
        $toSortNpsScoreMobile[] = $sort['MobileNpsScore'];
    }

    foreach ($FixedData as $sort) {
        $toSortNpsScoreFixed[] = $sort['FixedNpsScore'];
    }
    foreach ($EpisodeData as $sort) {
        $toSortNpsScoreEpisode[] = $sort['EpisodeNpsScore'];
    }

    foreach ($MergeData as $sort) {
        $toWeight[] = $sort['weight'];
    }

    array_multisort($toSortNpsScoreInteraction, SORT_DESC, $InteractionData);
    array_multisort($toSortNpsScoreMobile, SORT_DESC, $MobileData);
    array_multisort($toSortNpsScoreFixed, SORT_DESC, $FixedData);
    array_multisort($toSortNpsScoreEpisode, SORT_DESC, $EpisodeData);
    array_multisort($toWeight, SORT_DESC, $MergeData);
// array_multisort($toSortNpsScoreInteraction, SORT_DESC, $toSortNpsScoreMobile, SORT_DESC, $FixedData, SORT_ASC, $MergeData);

    $returnData = array('Interaction' => $InteractionData, 'Episode' => $EpisodeData, 'Mobile' => $MobileData, 'Fixed' => $FixedData, 'Merged' => $MergeData);

//    echo "<pre> here" . $tierSort;
//    print_r($weight);
//    //  var_dump($returnData);
//    exit;
    return $returnData;
}

function getClientNps($shared = NULL, $to = NULL, $from = NULL, $storeType = 1, $tierSort = 'Episode', $quarter = NULL) {

    global $connection;

    if ($shared != 'no') {
        $privateStore = " and c.f_StoreOwnerUserID NOT IN (SELECT f_StoreOwnerUserID FROM t_privateowner) ";
    }

    date_default_timezone_set('Australia/Melbourne');
    $sqlReportType = " ";

    if ($quarter == "Q3") {
        $sqlReportInteraction = " AND b.f_SurveyID IN ('1','2','3','4','5','6') ";
        $sqlReportFixed = " AND b.f_SurveyID IN ('1','2','3','4','6','13','14','15')
                         AND b.f_ProductID IN ('4','5','6','7','9','10','11','12','13','15','17','18','22','30','37','38','41','19') ";
        $sqlReportMobile = " AND b.f_SurveyID IN ('1','2','3','4','13','14','15')
                         AND b.f_ProductID IN ('1','2','3','14','39','40','36')";
    } else {
        $sqlReportInteraction = " AND b.f_SurveyID IN ('1','2','3','4','5','6') ";
        $sqlReportFixed = " AND b.f_SurveyID IN ('1','2','3','4','6','13','14','15')
                         AND b.f_ProductID IN ('4','5','6','7','9','10','11','12','13','15','17','18','22','30','37','38','41','19','42') ";
        $sqlReportMobile = " AND b.f_SurveyID IN ('1','2','3','4','13','14','15')
                         AND b.f_ProductID IN ('1','2','3','14','39','40','36')";
    }



    if ($quarter == NULL) {

        if (!is_null($to) && !is_null($from)) {
            $to = date("Y-m-d", strtotime($to));
            $from = date("Y-m-d", strtotime($from));
            $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
        } else {
            $sqlWhen = " AND MONTH(a.f_DateTimeResponseMelbourne) = MONTH(CURDATE())
                     AND YEAR(a.f_DateTimeResponseMelbourne) = YEAR(CURDATE())";
        }
    } else {

        if ($quarter == 'Q2') {
            $from = date("Y-10-01");
            $to = date("Y-12-31");
        } elseif ($quarter == 'Q3') {
            $from = date("Y-01-01");
            $to = date("Y-03-31");
        } elseif ($quarter == 'Q4') {
            $from = date("Y-04-01");
            $to = date("Y-06-30");
        } elseif ($quarter == 'Q1') {
            $from = date("Y-07-01");
            $to = date("Y-09-30");
        } elseif ($quarter == 'OldQ2') {
            $from = date("Y-10-01", strtotime("-1 year"));
            $to = date("Y-12-31", strtotime("-1 year"));
//            $from = date("Y-10-01");
//            $to = date("Y-12-31");
        }

        $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
    }

    $role = $_SESSION['UserRoleID'];

    $sqlStoreType = " ";
    if (!is_null($storeType) && $storeType != "") {
        $sqlStoreType = " AND c.f_StoreTypeID = '{$storeType}'";
    }

    /** switch ($role) {
      case '1':
      $sqlShared = " ";
      $sqlClientID = " ";
      break;
      case '2':
      case '3':
      case '4':
      case '5':
      if ($shared != "on") {
      $ownerID = $_SESSION['ClientDetails']['clientOwnerUserID'];
      $clientID = $_SESSION['ClientDetails']['clientID'];
      $sqlShared = " AND c.f_StoreOwnerUserID = '{$ownerID}' ";
      $sqlClientID = " AND d.f_ClientID = '{$clientID}'";
      } else {
      $sqlShared = " ";
      $sqlClientID = " ";
      }
      break;
      } */
    $sqlShared = " ";
    $sqlClientID = " AND d.f_ClientID IN (1,3,2,4)";

    if (is_null($quarter)) {
        $qT = " and f_Quarter = 'Q1'";
    } elseif ($quarter == 'OldQ2') {
        $qT = " and f_Quarter = 'Q2'";
    } else {
        $qT = " and f_Quarter = '{$quarter}'";
    }


    $sqlGetTiers = "SELECT * From t_storeTier where f_StoreTypeID = 1 {$qT}";
    $tierResult = mysql_query($sqlGetTiers, $connection);
    while ($row = mysql_fetch_assoc($tierResult)) {
        $Tiers[$row['f_NpsType']][] = array("max" => $row["f_NpsMax"], "min" => $row["f_NpsMin"],
            "tier" => $row["f_TierLevel"], "bapps" => $row["f_BappsPoints"], "bappsPercent" => $row["f_BappsPercentage"]);
    }

    $sqlStoreNps = "SELECT
    c.f_StoreID, c.f_StoreListName as StoreName, x.* , z.* 
    FROM
        t_surveysd a
            LEFT JOIN
        t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
            LEFT JOIN
        t_storelist c ON a.f_StoreID = c.f_StoreID    
            LEFT JOIN 
        t_clientlist d ON d.f_ClientID = c.f_ClientID       
            LEFT JOIN
        (SELECT
            DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%m-%d-%Y') AS MobileTotalDate,
                c.f_StoreID as 'MobileStoreID',
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '9' AND '10' THEN 1
                    ELSE 0
                END) AS MobileTotalAdvocate,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '7' AND '8' THEN 1
                    ELSE 0
                END) AS MobileTotalPassive,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '0' AND '6' THEN 1
                    ELSE 0
                END) AS MobileTotalDetractors,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
                    ELSE 0
                END) AS MobileTotalSurvey,
                SUM(CASE
                    WHEN (a.f_RecommendRate IS NULL) THEN 1
                    ELSE 0
                END) AS MobileNoScore,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = 'P000000' THEN 1
                    ELSE 0
                END) AS MobilePZero,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = '' THEN 1
                    ELSE 0
                END) AS MobileUnAssigned,
                COUNT(a.f_SurveySDID) AS MobileRawTotal
        FROM
            t_surveysd a
        LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID
        LEFT JOIN t_clientlist d ON d.f_ClientID = c.f_ClientID  
        WHERE
            1 = 1 AND c.f_Status = 1 AND d.f_Status = 1 {$sqlStoreType}
                 {$sqlWhen} {$sqlReportMobile} {$sqlClientID}
        GROUP BY c.f_StoreID) AS x ON c.f_StoreID = x.MobileStoreID
            LEFT JOIN
        (SELECT
            c.f_StoreID as 'FixedStoreID',
                DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%m-%d-%Y') AS FixedDate,
                 SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '9' AND '10' THEN 1
                    ELSE 0
                END) AS FixedTotalAdvocate,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '7' AND '8' THEN 1
                    ELSE 0
                END) AS FixedTotalPassive,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN '0' AND '6' THEN 1
                    ELSE 0
                END) AS FixedTotalDetractors,
                SUM(CASE
                    WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
                    ELSE 0
                END) AS FixedTotalSurvey,
                SUM(CASE
                    WHEN (a.f_RecommendRate IS NULL) THEN 1
                    ELSE 0
                END) AS FixedNoScore,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = 'P000000' THEN 1
                    ELSE 0
                END) AS FixedPZero,
                SUM(CASE
                    WHEN a.f_ActivationExtra5 = '' THEN 1
                    ELSE 0
                END) AS FixedUnAssigned,
                COUNT(a.f_SurveySDID) AS FixedRawTotal

        FROM
            t_surveysd a
        LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID
        LEFT JOIN t_clientlist d ON d.f_ClientID = c.f_ClientID
        WHERE
            1 = 1 AND c.f_Status = 1 AND d.f_Status = 1 {$sqlStoreType}
               {$sqlWhen} {$sqlReportFixed} {$sqlClientID}
        GROUP BY c.f_StoreID) AS z ON c.f_StoreID = z.FixedStoreID        
    WHERE
        1 = 1 AND c.f_Status = 1 and d.f_Status = 1 AND c.f_StoreTypeID = '1' {$sqlShared} {$privateStore} {$sqlClientID}
    GROUP BY c.f_StoreID";

    mysql_query("SELECT 1=1;", $connection);
    $resultStoreNps = mysql_query($sqlStoreNps, $connection);

    // echo $sqlStoreNps;

    $i = 0;
    while ($rowStoreNps = mysql_fetch_assoc($resultStoreNps)) {

        /** interaction     
          $InteractionData[$i] = $rowStoreNps;
          $StoreNpsAdvocatePercentage = $rowStoreNps['InteractionTotalAdvocate'] / $rowStoreNps['InteractionTotalSurvey'] * 100;
          $InteractionData[$i]['InteractionAdvocatesPercentage'] = $StoreNpsAdvocatePercentage;
          $StoreNpsPassivesPercentage = $rowStoreNps['InteractionTotalPassive'] / $rowStoreNps['InteractionTotalSurvey'] * 100;
          $InteractionData[$i]['InteractionPassivesPercentage'] = $StoreNpsPassivesPercentage;
          $StoreNpsDetractorsPercentage = $rowStoreNps['InteractionTotalDetractors'] / $rowStoreNps['InteractionTotalSurvey'] * 100;
          $InteractionData[$i]['InteractionDetractorsPercentage'] = $StoreNpsDetractorsPercentage;
          $InteractionData[$i]['InteractionNpsScore'] = number_format($StoreNpsAdvocatePercentage - $StoreNpsDetractorsPercentage, npsDecimal());

          $InteractionData[$i]['InteractionTierLevel'] = " "; */
        $FixedData[$i] = $rowStoreNps;
        $FixedAdvocatePercentage = intval($rowStoreNps['FixedTotalAdvocate']) / intval($rowStoreNps['FixedTotalSurvey']) * 100;
        $FixedData[$i]['FixedAdvocatesPercentage'] = $FixedAdvocatePercentage;
        $FixedPassivesPercentage = $rowStoreNps['FixedTotalPassive'] / $rowStoreNps['FixedTotalSurvey'] * 100;
        $FixedData[$i]['FixedPassivesPercentage'] = $FixedPassivesPercentage;
        $FixedDetractorsPercentage = $rowStoreNps['FixedTotalDetractors'] / intval($rowStoreNps['FixedTotalSurvey']) * 100;
        $FixedData[$i]['FixedDetractorsPercentage'] = $FixedDetractorsPercentage;
        $FixedData[$i]['FixedNpsScore'] = number_format($FixedAdvocatePercentage - $FixedDetractorsPercentage, npsDecimal());

        $MobileData[$i] = $rowStoreNps;
        $MobileAdvocatePercentage = intval($rowStoreNps['MobileTotalAdvocate']) / intval($rowStoreNps['MobileTotalSurvey']) * 100;
        $MobileData[$i]['MobileAdvocatesPercentage'] = $MobileAdvocatePercentage;
        $MobilePassivesPercentage = $rowStoreNps['MobileTotalPassive'] / $rowStoreNps['MobileTotalSurvey'] * 100;
        $MobileData[$i]['MobilePassivesPercentage'] = $MobilePassivesPercentage;
        $MobileDetractorsPercentage = $rowStoreNps['MobileTotalDetractors'] / $rowStoreNps['MobileTotalSurvey'] * 100;
        $MobileData[$i]['MobileDetractorsPercentage'] = $MobileDetractorsPercentage;
        $MobileData[$i]['MobileNpsScore'] = number_format($MobileAdvocatePercentage - $MobileDetractorsPercentage, npsDecimal());


        $fixCounter = 0;
        foreach ($Tiers["Fixed"] as $TierData) {


            if ($FixedData[$i]['FixedTotalSurvey'] == 0) {
                $FixedData[$i]['FixedTierLevel'] = "No NPS Score";
                $FixedData[$i]['FixedPoints'] = 0;
            } else {
                if ($fixCounter == 0) {
                    if ($FixedData[$i]['FixedNpsScore'] >= $TierData['min']) {
                        $FixedData[$i]['FixedTierLevel'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
                        $FixedData[$i]['FixedPoints'] = $TierData['bapps'];
                    }
                } elseif ($fixCounter == count($TierData)) {
                    if ($FixedData[$i]['FixedNpsScore'] < $TierData['max']) {
                        $FixedData[$i]['FixedTierLevel'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
                        $FixedData[$i]['FixedPoints'] = $TierData['bapps'];
                    }
                } else {
                    if ($FixedData[$i]['FixedNpsScore'] >= $TierData['min'] && $FixedData[$i]['FixedNpsScore'] < $TierData['max']) {
                        $FixedData[$i]['FixedTierLevel'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
                        $FixedData[$i]['FixedPoints'] = $TierData['bapps'];
                    }
                }
            }

            $fixCounter = $fixCounter + 1;
        }


        // echo 'was here';


        $mobCounter = 0;
        foreach ($Tiers["Mobile"] as $TierData) {
            //  print_r($TierData);
            if ($MobileData[$i]['MobileTotalSurvey'] == 0) {
                $MobileData[$i]['MobileTierLevel'] = "No NPS Score";
                $MobileData[$i]['MobilePoints'] = 0;
            } else {
                if ($mobCounter == 0) {
                    if ($MobileData[$i]['MobileNpsScore'] >= $TierData['min']) {
                        $MobileData[$i]['MobileTierLevel'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
                        $MobileData[$i]['MobilePoints'] = $TierData['bapps'];
                    }
                } elseif ($mobCounter == count($TierData)) {
                    if ($MobileData[$i]['MobileNpsScore'] < $TierData['max']) {
                        $MobileData[$i]['MobileTierLevel'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
                        $MobileData[$i]['MobilePoints'] = $TierData['bapps'];
                    }
                } else {
                    if ($MobileData[$i]['MobileNpsScore'] >= $TierData['min'] AND $MobileData[$i]['MobileNpsScore'] < $TierData['max']) {
                        $MobileData[$i]['MobileTierLevel'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
                        $MobileData[$i]['MobilePoints'] = $TierData['bapps'];
                    }
                }
            }

            $mobCounter = $mobCounter + 1;
        }

        $PointsTotal[$i] = $MobileData[$i]['MobilePoints'] + $FixedData[$i]['FixedPoints'];


        switch ($tierSort) {
            case 'Interaction':
                $weight[$i] = $InteractionData[$i]['InteractionNpsScore'];
                break;
            case 'Episode':
                $weight[$i] = $EpisodeData[$i]['EpisodeNpsScore'];
                break;
            case 'Mobile':
                $weight[$i] = $MobileData[$i]['MobileNpsScore'];
                break;
            case 'Fixed':
                $weight[$i] = $FixedData[$i]['FixedNpsScore'];
                break;
            default:
                $weight[$i] = $InteractionData[$i]['EpisodeNpsScore'];
                break;
        }


        $MergeData[$i] = array('StoreName' => $rowStoreNps['StoreName'],
            'FixedNPS' => $FixedData[$i]['FixedNpsScore'],
            'FixedTier' => $FixedData[$i]['FixedTierLevel'],
            'FixedAdvocates' => $FixedData[$i]['FixedTotalAdvocate'],
            'FixedPasives' => $FixedData[$i]['FixedTotalPassive'],
            'FixedDetractors' => $FixedData[$i]['FixedTotalDetractors'],
            'FixedAdvocatesPercentage' => $FixedData[$i]['FixedAdvocatesPercentage'],
            'FixedPasivesPercentage' => $FixedData[$i]['FixedPassivesPercentage'],
            'FixedDetractorsPercentage' => $FixedData[$i]['FixedDetractorsPercentage'],
            'FixedTotalSurvey' => $FixedData[$i]['FixedTotalSurvey'],
            'FixedTotalPoints' => $FixedData[$i]['FixedPoints'],
            'MobileNPS' => $MobileData[$i]['MobileNpsScore'],
            'MobileTier' => $MobileData[$i]['MobileTierLevel'],
            'MobileAdvocates' => $MobileData[$i]['MobileTotalAdvocate'],
            'MobilePasives' => $MobileData[$i]['MobileTotalPassive'],
            'MobileDetractors' => $MobileData[$i]['MobileTotalDetractors'],
            'MobileAdvocatesPercentage' => $MobileData[$i]['MobileAdvocatesPercentage'],
            'MobilePasivesPercentage' => $MobileData[$i]['MobilePassivesPercentage'],
            'MobileDetractorsPercentage' => $MobileData[$i]['MobileDetractorsPercentage'],
            'MobileTotalSurvey' => $MobileData[$i]['MobileTotalSurvey'],
            'MobileTotalPoints' => $MobileData[$i]['MobilePoints'],
            'weight' => $weight[$i]
        );
        $i++;
    }

    foreach ($MobileData as $sort) {
        $toSortNpsScoreMobile[] = $sort['MobileNpsScore'];
    }

    foreach ($FixedData as $sort) {
        $toSortNpsScoreFixed[] = $sort['FixedNpsScore'];
    }


    foreach ($MergeData as $sort) {
        $toWeight[] = $sort['weight'];
    }

    // array_multisort($toSortNpsScoreInteraction, SORT_DESC, $InteractionData);
    array_multisort($toSortNpsScoreMobile, SORT_DESC, $MobileData);
    array_multisort($toSortNpsScoreFixed, SORT_DESC, $FixedData);

    array_multisort($toWeight, SORT_DESC, $MergeData);
// array_multisort($toSortNpsScoreInteraction, SORT_DESC, $toSortNpsScoreMobile, SORT_DESC, $FixedData, SORT_ASC, $MergeData);

    $returnData = array('Mobile' => $MobileData, 'Fixed' => $FixedData, 'Merged' => $MergeData);

//    echo "<pre> here" . $tierSort;
//    print_r($weight);
//    //  var_dump($returnData);
//    exit;
    return $returnData;
}

function getNpsVolumeTLS($storeID, $npsType, $dateFrom, $dateTo, $quarter = 'Q1') {
    global $connection;

    $role = $_SESSION['UserRoleID'];

    if ($storeID == "CombinedTLS") {
        $storeName = "   'Combined' as StoreName,";
        $sqlStoreID = " ";
        $storeTypeID = 1;
    } elseif ($storeID == "CombinedTBC") {
        $storeName = "   'Combined' as StoreName,";
        $sqlStoreID = " ";
        $storeTypeID = 2;
    } elseif ($storeID == "CombinedOthers") {
        $storeName = "   'Combined' as StoreName,";
        $sqlStoreID = " ";
        $storeTypeID = 3;
    } else {
        $storeName = "   c.f_StoreListName as StoreName,";
        $sqlStoreID = "  AND c.f_StoreID = {$storeID}";

        $sqlCheckStoreType = "SELECT f_StoreTypeID FROM t_storelist where f_StoreID = '{$storeID}' and f_Status=1 LIMIT 1";
        $resultCheckStoreType = mysql_query($sqlCheckStoreType, $connection);
        $dataCheckStoreType = mysql_fetch_assoc($resultCheckStoreType);
        $storeTypeID = $dataCheckStoreType['f_StoreTypeID'];
    }

    switch ($role) {
        case '1':
            $sqlShared = " ";
            break;
        case '2':
        case '3':
            $sqlShared = " AND c.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
            break;
        case '4':
        case '5':
            $query = $_SESSION['EmpProfileID'];
//    $type = "StoreLeader";
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $data = $row['f_StoreOwnerUserID'];
            }
            $sqlShared = " AND c.f_StoreOwnerUserID = '{$data}' ";
            break;
    }




    if ($storeTypeID == '1' || $storeTypeID == '3') {
        if ($npsType == 'Mobile') {
            $sqlNpsType = " AND b.f_SurveyID IN ('1','2','3','4','13','14','15')
                         AND b.f_ProductID IN ('1','2','3','14','39','40','36') ";
        } elseif ($npsType == 'Fixed') {
            $sqlNpsType = " AND b.f_SurveyID IN ('1','2','3','4','6','13','14','15')
                         AND b.f_ProductID IN ('4','5','6','7','9','10','11','12','13','15','17','18','22','30','37','38','41','19','42') ";
        } else {
            $sqlNpsType = "";
        }
    } elseif ($storeTypeID == '2') {
        if ($npsType == 'Mobile') {
            $sqlNpsType = " AND b.f_ProductID in ('1','2')";
        } elseif ($npsType == 'Fixed') {
            $sqlNpsType = " AND b.f_ProductID in ('5','6','7','8','9','10','11','12','13','16','17','18','19','20')";
        } else {
            $sqlNpsType = "";
        }
    }

    $sqlGetTiers = "SELECT * From t_storeTier where f_StoreTypeID = '{$storeTypeID}' and f_Quarter = 'Q1' ";
    $tierResult = mysql_query($sqlGetTiers, $connection);
    while ($row = mysql_fetch_assoc($tierResult)) {
        $Tiers[$row['f_NpsType']][] = array("max" => $row["f_NpsMax"], "min" => $row["f_NpsMin"],
            "tier" => $row["f_TierLevel"], "bapps" => $row["f_BappsPoints"], "bappsPercent" => $row["f_BappsPercentage"]);
    }

    $select = "SELECT  
           {$storeName}
            DATE_FORMAT(CASE
                WHEN (WEEKDAY(a.f_DateTimeResponseMelbourne) <= 3) THEN DATE(a.f_DateTimeResponseMelbourne + INTERVAL (3 - WEEKDAY(a.f_DateTimeResponseMelbourne)) DAY)
                ELSE DATE(a.f_DateTimeResponseMelbourne + INTERVAL (3 + 7 - WEEKDAY(a.f_DateTimeResponseMelbourne)) DAY)
            END ,'%e-%b') as DateStamp,
            c.f_StoreID AS 'MobileStoreID',
            SUM(CASE
                WHEN (a.f_RecommendRate BETWEEN '9' AND '10') THEN 1
                ELSE 0
            END) AS StoreAdvocate,
            SUM(CASE
                WHEN (a.f_RecommendRate BETWEEN '7' AND '8') THEN 1
                ELSE 0
            END) AS StorePassive,
            SUM(CASE
                WHEN (a.f_RecommendRate BETWEEN '0' AND '6') THEN 1
                ELSE 0
            END) AS StoreDetractor,
            SUM(CASE
                WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
                ELSE 0
            END) AS StoreTotalSurvey,
            ((SUM(CASE
                WHEN (a.f_RecommendRate BETWEEN '9' AND '10') THEN 1
                ELSE 0
            END) - SUM(CASE
                WHEN (a.f_RecommendRate BETWEEN '0' AND '6') THEN 1
                ELSE 0
            END)) * 100 / SUM(CASE
                WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
                ELSE 0
            END)) AS StoreMobile
        FROM
            t_surveysd a
                LEFT JOIN
            t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
                LEFT JOIN
            t_storelist c ON a.f_StoreID = c.f_StoreID
        WHERE
            1 = 1 AND c.f_Status = 1 {$sqlShared} {$sqlStoreID} {$sqlNpsType}   
                AND a.f_DateTimeResponseMelbourne BETWEEN '{$dateFrom} 00:00:00' AND '{$dateTo} 23:59:59'
        GROUP BY DateStamp {$groupby}
        order by f_DateTimeResponseMelbourne";
    $result = mysql_query($select, $connection);

    $i = 0;
    $advo = 0;
    $pass = 0;
    $det = 0;
    $total = 0;
    while ($row = mysql_fetch_assoc($result)) {

        $StoreNpsData[$row['StoreName']][$row['DateStamp']] = intval($row['StoreMobile']);
        $StoreVolumeData[$row['StoreName']][$row['DateStamp']] = intval($row['StoreTotalSurvey']);
        $StoreTierData[$row['StoreName']][$row['DateStamp']]['NPS'] = intval($row['StoreMobile']);
        $StoreTierData[$row['StoreName']][$row['DateStamp']]['Volume'] = intval($row['StoreTotalSurvey']);
        $newAdvo = intval($row['StoreAdvocate']) + $advo;
        $newPass = intval($row['StorePassive']) + $pass;
        $newDet = intval($row['StoreDetractor']) + $det;
        $total = $newAdvo + $newPass + $newDet;
        $newNPS = ($newAdvo - $newDet) * 100 / $total;
        $StoreTierData[$row['StoreName']][$row['DateStamp']]['WeekNPS'] = intval($newNPS);

        $mobCounter = 0;
        foreach ($Tiers[$npsType] as $TierData) {
            //  print_r($TierData);
            if ($StoreNpsData[$row['StoreName']][$row['DateStamp']] == 'nan' || $StoreNpsData[$row['StoreName']][$row['DateStamp']] == '' || empty($StoreNpsData[$row['StoreName']][$row['DateStamp']])) {
                $StoreTierData[$row['StoreName']][$row['DateStamp']]['Tier'] = 5;
            } else {
                if ($mobCounter == 0) {
                    if ($StoreNpsData[$row['StoreName']][$row['DateStamp']] >= $TierData['min']) {
                        $StoreTierData[$row['StoreName']][$row['DateStamp']]['Tier'] = $TierData['tier'];
                    }
                } elseif ($mobCounter == count($TierData)) {
                    if ($StoreNpsData[$row['StoreName']][$row['DateStamp']] < $TierData['max']) {
                        $StoreTierData[$row['StoreName']][$row['DateStamp']]['Tier'] = $TierData['tier'];
                    }
                } else {
                    if ($StoreNpsData[$row['StoreName']][$row['DateStamp']] >= $TierData['min'] AND $StoreNpsData[$row['StoreName']][$row['DateStamp']] < $TierData['max']) {
                        $StoreTierData[$row['StoreName']][$row['DateStamp']]['Tier'] = $TierData['tier'];
                    }
                }
            }
            $mobCounter = $mobCounter + 1;
        }

        $WeekCounter = 0;
        foreach ($Tiers[$npsType] as $TierData) {
            //  print_r($TierData);
            if ($StoreTierData[$row['StoreName']][$row['DateStamp']]['WeekNPS'] == 'nan' || $StoreTierData[$row['StoreName']][$row['DateStamp']]['WeekNPS'] == '' || empty($StoreTierData[$row['StoreName']][$row['DateStamp']]['WeekNPS'])) {
                $StoreTierData[$row['StoreName']][$row['DateStamp']]['WeekTier'] = 5;
            } else {
                if ($WeekCounter == 0) {
                    if ($StoreTierData[$row['StoreName']][$row['DateStamp']]['WeekNPS'] >= $TierData['min']) {
                        $StoreTierData[$row['StoreName']][$row['DateStamp']]['WeekTier'] = $TierData['tier'];
                    }
                } elseif ($WeekCounter == count($TierData)) {
                    if ($StoreTierData[$row['StoreName']][$row['DateStamp']]['WeekNPS'] < $TierData['max']) {
                        $StoreTierData[$row['StoreName']][$row['DateStamp']]['WeekTier'] = $TierData['tier'];
                    }
                } else {
                    if ($StoreTierData[$row['StoreName']][$row['DateStamp']]['WeekNPS'] >= $TierData['min'] AND $StoreTierData[$row['StoreName']][$row['DateStamp']]['WeekNPS'] < $TierData['max']) {
                        $StoreTierData[$row['StoreName']][$row['DateStamp']]['WeekTier'] = $TierData['tier'];
                    }
                }
            }
            $WeekCounter = $WeekCounter + 1;
        }

        $category[] = $row['DateStamp'];

        $advo = $newAdvo;
        $pass = $newPass;
        $det = $newDet;
    }

    $o = 0;
    foreach ($StoreNpsData as $key => $storeValue) {

        ${'nps' . $o}['name'] = $key;
        foreach ($category as $month) {
            //   ${'row' . $o}['data'][] = intval($value[$getNPSdata]);
            if (is_null($storeValue[$month])) {
                ${'nps' . $o}['data'][] = null;
            } else {
                ${'nps' . $o}['data'][] = $storeValue[$month];
            }
        }
        $nps[] = ${'nps' . $o};
        $o++;
    }

    $w = 0;
    foreach ($StoreVolumeData as $key => $storeVolume) {
        ${'volume' . $w}['name'] = $key;
        foreach ($category as $month) {
            //   ${'row' . $o}['data'][] = intval($value[$getNPSdata]);
            if (is_null($storeVolume[$month])) {
                ${'volume' . $w}['data'][] = null;
            } else {
                ${'volume' . $w}['data'][] = $storeVolume[$month];
            }
        }
        $volume[] = ${'volume' . $w};
        $w++;
    }

    $y = 0;

    foreach ($StoreTierData as $key => $storeVolume) {

        ${'$zone' . $y}['name'] = $key;
        ${'$zoneX' . $y}['name'] = $key;
        ${'$zoneW' . $y}['name'] = $key;
        ${'$weeklyNps' . $y}['name'] = $key;
        $x = 1;
        foreach ($category as $month) {
            //   ${'row' . $o}['data'][] = intval($value[$getNPSdata]);
            if (is_null($storeVolume[$month])) {
                ${'$zone' . $y}['data'][] = null;
            } else {
                // ${'tier' . $y}['data'][] = $storeVolume[$month];

                if ($storeVolume[$month]['WeekTier'] == 1) {
                    $colorWeekTier[$x] = "#348939";
                } elseif ($storeVolume[$month]['WeekTier'] == 2) {
                    $colorWeekTier[$x] = "#FDBF02";
                } elseif ($storeVolume[$month]['WeekTier'] == 3) {
                    $colorWeekTier[$x] = "#FDBF02";
                } elseif ($storeVolume[$month]['WeekTier'] == 4) {
                    $colorWeekTier[$x] = "#FE7E03";
                } elseif ($storeVolume[$month]['WeekTier'] == 5) {
                    $colorWeekTier[$x] = "#9B1D1E";
                } else {
                    $colorWeekTier[$x] = "#9B1D1E";
                }

                if ($storeVolume[$month]['Tier'] == 1) {
                    $color[$x] = "#348939";
                } elseif ($storeVolume[$month]['Tier'] == 2) {
                    $color[$x] = "#FDBF02";
                } elseif ($storeVolume[$month]['Tier'] == 3) {
                    $color[$x] = "#FDBF02";
                } elseif ($storeVolume[$month]['Tier'] == 4) {
                    $color[$x] = "#FE7E03";
                } elseif ($storeVolume[$month]['Tier'] == 5) {
                    $color[$x] = "#9B1D1E";
                } else {
                    $color[$x] = "#9B1D1E";
                }

                ${'$zone' . $y}['data'][] = array('y' => $storeVolume[$month]['Volume'], 'tierLevel' => $storeVolume[$month]['Tier']);
                ${'$zoneX' . $y}['data'][] = array('y' => $storeVolume[$month]['NPS'], 'color' => $color[$x]);
                ${'$weeklyNps' . $y}['data'][] = array('y' => $storeVolume[$month]['WeekNPS'], 'color' => $colorWeekTier[$x], 'tierLevel' => $storeVolume[$month]['Tier']);
                ${'$zoneW' . $y}['data'][] = array('value' => $x + .00001, 'color' => $color[$x], 'tierLevel' => $storeVolume[$month]['Tier']);
            }
            $x++;
        }
        $zone[] = ${'$zone' . $y};
        $zoneX[] = ${'$zoneX' . $y};
        $zoneW[] = ${'$zoneW' . $y};
        $weeklyNps[] = ${'$weeklyNps' . $y};
        $y++;
    }

    $returnData = array('volume' => $zone, 'nps' => $zoneX, 'category' => $category, 'weeklyNps' => $weeklyNps);

    return $returnData;
}
