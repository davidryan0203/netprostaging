<?php
header('Content-type: text/html');
header('Access-Control-Allow-Origin: *');
//sleep(2);
//error_reporting(0);
session_start();
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
$when = $_GET['when'];
$shared = $_GET['shared'];

if ($when == "lastmonth") {
    $sqlWhen = "AND  MONTH(a.f_DateTimeCreatedMelbourne) = MONTH(CURDATE()  - INTERVAL 1 MONTH)
                            AND YEAR(a.f_DateTimeCreatedMelbourne) = YEAR(CURDATE()  - INTERVAL 1 MONTH)";
} else {
    $sqlWhen = "AND  MONTH(a.f_DateTimeCreatedMelbourne) = MONTH(CURDATE())
                            AND YEAR(a.f_DateTimeCreatedMelbourne) = YEAR(CURDATE())";
}

if ($shared == "no") {
    $sqlShared = " AND b.f_StoreOwnerUserID = {$_SESSION['UserProfileID']}";
} else {
    $sqlShared = " ";
}

$sqlSummary = "SELECT
                DATE_FORMAT(a.f_DateTimeCreatedMelbourne, '%m-%d-%Y') as Date ,
                b.f_StoreListName ,
                SUM(CASE
                        WHEN (a.f_RecommendRate IS not NULL AND a.f_RecommendRate != '')
                            and a.f_RecommendRate BETWEEN '9' and '10'   THEN  1
                        ELSE 0
                    END) AS EpisodeTotalAdvocate,
                SUM(CASE
                        WHEN (a.f_RecommendRate IS not NULL AND a.f_RecommendRate != '')
                            and a.f_RecommendRate BETWEEN '7' and '8' THEN  1
                        ELSE 0
                END) AS EpisodeTotalPassive,
                SUM(CASE
                        WHEN (a.f_RecommendRate IS not NULL AND a.f_RecommendRate != '')
                            and a.f_RecommendRate  BETWEEN '0' and '6' THEN  1
                        ELSE 0
                END) AS EpisodeTotalDetractors,
                SUM(CASE
                        WHEN a.f_RecommendRate IS NULL OR a.f_RecommendRate = '' THEN  1
                        ELSE 0
                END) AS EpisodeBlankCount,
                SUM(CASE
                        WHEN (a.f_LikelihoodToRecommendRetail IS not NULL AND a.f_LikelihoodToRecommendRetail != '')
                                and a.f_LikelihoodToRecommendRetail BETWEEN '9' and '10'   THEN  1
                        ELSE 0
                    END) AS InteractionTotalAdvocate,
                SUM(CASE
                        WHEN  (a.f_LikelihoodToRecommendRetail IS not NULL AND a.f_LikelihoodToRecommendRetail != '')
                                        and a.f_LikelihoodToRecommendRetail BETWEEN '7' and '8' THEN  1
                        ELSE 0
                END) AS InteractionTotalPassive,
                SUM(CASE
                        WHEN  (a.f_LikelihoodToRecommendRetail IS not NULL AND a.f_LikelihoodToRecommendRetail != '')
                                and a.f_LikelihoodToRecommendRetail BETWEEN '0' and '6' THEN  1
                        ELSE 0
                END) AS InteractionTotalDetractors,
                SUM(CASE
                        WHEN  a.f_LikelihoodToRecommendRetail IS NULL OR a.f_LikelihoodToRecommendRetail = ''  THEN  1
                        ELSE 0
                END) AS InteractionBlankCount,
                count(*) as TotalSummaryCount
                FROM t_surveysd a
                LEFT JOIN t_storelist b on a.f_StoreID = b.f_StoreID
                WHERE 1 = 1 {$sqlWhen}
                    {$sqlShared}
                Group by a.f_StoreID
                ORDER BY b.f_StoreListName DESC";
$resultSummary = mysql_query($sqlSummary, $connection);
$i = 0;
while ($rowSummary = mysql_fetch_assoc($resultSummary)) {
    $stores[] = $rowSummary['f_StoreListName'];
    $InteractionData[$i] = $rowSummary;
    $InteractionAdvocatePercentage = $rowSummary['InteractionTotalAdvocate'] / $rowSummary['TotalSummaryCount'] * 100;
    $InteractionData[$i]['InteractionAdvocatesPercentage'] = $InteractionAdvocatePercentage;
    $InteractionPassivesPercentage = $rowSummary['InteractionTotalPassive'] / $rowSummary['TotalSummaryCount'] * 100;
    $InteractionData[$i]['InteractionPassivesPercentage'] = $InteractionPassivesPercentage;
    $InteractionDetractorsPercentage = $rowSummary['InteractionTotalDetractors'] / $rowSummary['TotalSummaryCount'] * 100;
    $InteractionData[$i]['InteractionDetractorsPercentage'] = $InteractionDetractorsPercentage;
    $InteractionData[$i]['InteractionNpsScorePercentage'] = number_format($InteractionAdvocatePercentage, npsDecimal()) - number_format($InteractionDetractorsPercentage, npsDecimal());


    if ($InteractionData[$i]['InteractionNpsScorePercentage'] >= 75) {
        $InteractionData[$i]['InteractionTierLevel'] = 'Tier 1';
    } elseif ($InteractionData[$i]['InteractionNpsScorePercentage'] >= 65 AND $InteractionData[$i]['InteractionNpsScorePercentage'] <= 74) {
        $InteractionData[$i]['InteractionTierLevel'] = 'Tier 2';
    } elseif ($InteractionData[$i]['InteractionNpsScorePercentage'] >= 55 AND $InteractionData[$i]['InteractionNpsScorePercentage'] <= 64) {
        $InteractionData[$i]['InteractionTierLevel'] = 'Tier 3';
    } elseif ($InteractionData[$i]['InteractionNpsScorePercentage'] >= 45 AND $InteractionData[$i]['InteractionNpsScorePercentage'] <= 54) {
        $InteractionData[$i]['InteractionTierLevel'] = 'Tier 4';
    } else {
        $InteractionData[$i]['InteractionTierLevel'] = 'Tier 5';
    }

    $EpisodeData[$i] = $rowSummary;
    $EpisodeAdvocatePercentage = intval($rowSummary['EpisodeTotalAdvocate']) / intval($rowSummary['TotalSummaryCount']) * 100;
    $EpisodeData[$i]['EpisodeAdvocatesPercentage'] = $EpisodeAdvocatePercentage;
    $EpisodePassivesPercentage = $rowSummary['EpisodeTotalPassive'] / $rowSummary['TotalSummaryCount'] * 100;
    $EpisodeData[$i]['EpisodePassivesPercentage'] = $EpisodePassivesPercentage;
    $EpisodeDetractorsPercentage = $rowSummary['EpisodeTotalDetractors'] / $rowSummary['TotalSummaryCount'] * 100;
    $EpisodeData[$i]['EpisodeDetractorsPercentage'] = $EpisodeDetractorsPercentage;
    $EpisodeData[$i]['EpisodeNpsScorePercentage'] = number_format($EpisodeAdvocatePercentage, npsDecimal()) - number_format($EpisodeDetractorsPercentage, npsDecimal());

    if ($EpisodeData[$i]['EpisodeNpsScorePercentage'] >= 50) {
        $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 1';
    } elseif ($EpisodeData[$i]['EpisodeNpsScorePercentage'] >= 40 AND $EpisodeData[$i]['EpisodeNpsScorePercentage'] <= 49) {
        $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 2';
    } elseif ($EpisodeData[$i]['EpisodeNpsScorePercentage'] >= 30 AND $EpisodeData[$i]['EpisodeNpsScorePercentage'] <= 39) {
        $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 3';
    } elseif ($EpisodeData[$i]['EpisodeNpsScorePercentage'] >= 0 AND $EpisodeData[$i]['EpisodeNpsScorePercentage'] <= 29) {
        $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 4';
    } else {
        $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 4';
    }

    $i++;
}
echo $sqlSummary;

//$i = 0;
//while ($r = mysql_fetch_assoc($query)) {
//    $rows[$i]['name'] = $r['StoreName'];
//    $rows[$i]['data'][] = $r['TierLevel'];
//    $i++;
//}

function getColor($num) {
    $hash = md5('color' . $num); // modify 'color' to get a different palette
    return array(
        hexdec(substr($hash, 0, 2)), // r
        hexdec(substr($hash, 2, 2)), // g
        hexdec(substr($hash, 4, 2))); //b
}

$background_colors = array('#51c892,', '#bfd674', '#47cf71', '#d383f4', '#95f6e8');
foreach ($EpisodeData as $EpisodeValue) {
    $color = getColor(rand());
    $EpsideEncodeArray[] = array('y' => $EpisodeValue['EpisodeNpsScorePercentage'], 'tierlevel' => $EpisodeValue['EpisodeTierLevel'], 'color' => "rgba(" . $color[0] . "," . $color[1] . "," . $color[2] . ",0.8)");
}
foreach ($InteractionData as $InteractionValue) {
    $color = getColor(rand());
    $InteractionEncodeArray[] = array('y' => $InteractionValue['InteractionNpsScorePercentage'], 'tierlevel' => $InteractionValue['InteractionTierLevel'], 'color' => "rgba(" . $color[0] . "," . $color[1] . "," . $color[2] . ",0.9)");
}
$category = json_encode($stores);
$interaction = json_encode($InteractionEncodeArray);
$episode = json_encode($EpsideEncodeArray);
?>

<script type="text/javascript">
    $(function() {
        // Radialize the colors
        Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function(color) {
            return {
                radialGradient: {cx: 0.5, cy: 0.3, r: 0.7},
                stops: [
                    [0, color],
                    [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
                ]
            };
        });

        var chart;
        $(document).ready(function() {


            chart = new Highcharts.Chart({
                credits: {
                    enabled: false
                },
                chart: {
                    renderTo: 'PrevTier',
                    type: 'column',
                    marginRight: 20,
                    marginBottom: 75
                            // backgroundColor:'#39cefe'
                },
                title: {
                    text: 'Store Tier Level',
                    x: -20 //center
                },
                subtitle: {
                    text: '',
                    x: -20
                },
                yAxis: [{
                        allowDecimals: false,
                        // "id": "0",
                        //reversed: true,
                        min: 0,
                        max: 100,
                        title: {
                            text: ''
                        }}],
                legend: {
                    shadow: false
                },
                xAxis: {
                    "id": "0",
                    categories: <?php echo $category; ?>
                    //reversed: true
                },
                tooltip: {
                    formatter: function() {
                        return '<b>' + this.series.name + '</b><br>' +
                                this.x + ': ' + this.point.tierlevel;
                    }

                },
                plotOptions: {
                    series: {
                        fillOpacity: 0.5
                    },
                    column: {
                        grouping: false,
                        shadow: false,
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true, color: 'white',
                            verticalAlign: "top",
                            overflow: true,
                            crop: false,
                            inside: true,
                            // y: -5,
                            style: {
                                fontSize: '12px',
                                fontFamily: 'Verdana, sans-serif',
                                textShadow: '0 0 5px black'
                            }
                        }
                    }
                },
                series: [{
                        name: 'Episode',
                        color: 'rgba(165,170,217,1)',
                        data: <?php echo $episode; ?>,
                        pointPadding: 0.3,
                        pointPlacement: -0.2
                    }, {
                        name: 'Intercation',
                        color: 'rgba(126,86,134,.9)',
                        data: <?php echo $interaction; ?>,
                        pointPadding: 0.4,
                        pointPlacement: -0.2
                    }]
            });


        });

    });
</script>

<div id="PrevTier" style="min-width: 100%; height: 350px; margin: -14px;"></div><?php
echo mysql_error();
var_dump($interaction);
