<?php
session_start();
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/nps/function/NPSFunction.php');
date_default_timezone_set('America/Denver');
//$to = date("Y-m-d", strtotime($_GET['to']));
//$from = date("Y-m-d", strtotime($_GET['from']));
$shared = $_GET['shared'];
$role = $_GET['role'];
$range = date("d/m/Y", strtotime($_GET['from'])) . " to " . date("d/m/Y", strtotime($_GET['to']));
$storeType = $_GET['storeType'];
//if (!is_null($to) && !is_null($from)) {
//    $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
//} else {
//    $sqlWhen = " AND MONTH(a.f_DateTimeResponseMelbourne) = MONTH(CURDATE())
//AND YEAR(a.f_DateTimeResponseMelbourne) = YEAR(CURDATE())";
//}


if (isset($_GET['weekly']) && $_GET['weekly'] == "on") {
    $to = date("Y-m-d");
    //   $from = date("Y-m-d", strtotime('4 weeks ago'));
    $from = date("Y-m-d", strtotime('-27 days'));
    $tableID = "EmpNpsWeeklyEpisode";
} elseif (isset($_GET['htd']) && $_GET['htd'] == "on") {
    $currentMonth = date('m');
    if (($currentMonth >= 1) && ($currentMonth <= 6)) {
        $from = date("Y-01-01");
        $to = date("Y-06-30");
    } else {
        $from = date("Y-07-01");
        $to = date("Y-12-31");
    }

    $tableID = "EmpNpsHtdEpisode";
} elseif (isset($_GET['3mma']) && $_GET['3mma'] == "on") {
    $to = $_GET['to'];
    $from = date("Y-m-01", strtotime('first day of -2 Month', strtotime($_GET['to'])));
    $tableID = "EmpNPS3MMAEpisode";
} else {
    $tableID = "EmpNpsEpisode";
    if (isset($_GET['from']) && isset($_GET['to'])) {
        $from = date("Y-m-d", strtotime($_GET['from']));
        $to = date("Y-m-d", strtotime($_GET['to']));
    } else {
        $from = NULL;
        $to = NULL;
    }
}


//echo $role . "<br>".$shared;
switch ($role) {
    case '1':
        $sqlShared = " ";
        break;
    case '2':
    case '3':
        if ($shared == "no") {
            $sqlShared = " AND c.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
        } else {
            $sqlShared = " ";
        }
        break;
    case '4':
    case '5':
        $query = $_SESSION['EmpProfileID'];
//    $type = "StoreLeader";
        $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
        $result = mysql_query($sqlGetOwnerID, $connection);
        while ($row = mysql_fetch_assoc($result)) {
            $data = $row['f_StoreOwnerUserID'];
        }
        $sqlShared = " AND c.f_StoreOwnerUserID = '{$data}' ";
        break;
}
$empid = $_SESSION['EmpProfileID'];
if (isset($_GET['reconOwnerID']) && !is_null($_GET['reconOwnerID'])) {
    $ownerReconID = $_GET['reconOwnerID'];
} else {
    $ownerReconID = NULL;
}
$tierSort = $_GET['tiersort'];

$StoreNpsScores = getStoreNpsOthers($shared, $ownerReconID, $empid, $to, $from, $role, 1, NULL, $tierSort);
// getStoreNpsTLS($shared = NULL, $profileID = NULL, $empID = NULL, $to = NULL, $from = NULL, $role = NULL, $storeType = 1, $rangeType = NULL, $tierSort = 'Episode')
//echo "<pre>";var_dump($StoreNpsScores['Merged']);
//$StoreNpsScores = getStoreNpsTBC($shared, $ownerReconID, $empid, $to, $from, $role, $storeType, $rangeType);

foreach ($StoreNpsScores['Merged'] as $value) {
    $storeList[] = $value['StoreName'];
    $MobileEncodeArray[] = array('y' => intval($value['MobileNPS']), 'tierlevel' => $value['MobileTier'], 'color' => "#fe8e20");
    // $EpisodeEncodeArray[] = array('y' => intval($value['EpisodeNPS']), 'tierlevel' => $value['EpisodeTier'], 'color' => "#ffd700");
    $FixedEncodeArray[] = array('y' => intval($value['FixedNPS']), 'tierlevel' => $value['FixedTier'], 'color' => "#2090fe");
}

$category = json_encode($storeList);
//$episode = json_encode($EpisodeEncodeArray);
$mobile = json_encode($MobileEncodeArray);
$fixed = json_encode($FixedEncodeArray);
//echo $category;
?>

<script type="text/javascript">
    $(function() {
        // Radialize the colors
        Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function(color) {
            return {
                radialGradient: {cx: 0.5, cy: 0.3, r: 0.7},
                stops: [
                    [0, color],
                    [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
                ]
            };
        });

        var chart2;
        $(document).ready(function() {


            chart2 = new Highcharts.Chart({
                credits: {
                    enabled: false
                },
                chart: {
                    renderTo: 'Tier',
                    type: 'column',
                    marginRight: 20,
                    marginBottom: 75,
                    backgroundColor: 'rgba(255, 255, 255, 0.67)'
                },
                title: {
                    style: {
                        color: '#141B50',
                        font: 'bold 17px "Trebuchet MS", Verdana, sans-serif'
                    },
                    text: 'Mobility (On the go) vs Fixed (Home)',
                    x: -20 //center
                },
                subtitle: {
                    text: '',
                    x: -20
                },
                yAxis: [{
                        allowDecimals: false,
                        // "id": "0",
                        //reversed: true,
                        min: -30,
                        max: 100,
                        title: {
                            text: ''
                        }}],
                legend: {
                    shadow: false
                },
                xAxis: {
                    min: 0,
                    max: 5,
                    //"id": "0",
                    categories: <?php echo $category; ?>
                    //reversed: true
                }, scrollbar: {
                    enabled: true
                },
                tooltip: {
                    formatter: function() {
                        return '<b>' + this.series.name + ' : ' + this.y + '</b><br>' +
                                this.x + ': ' + this.point.tierlevel;
                    }

                },
                plotOptions: {
                    series: {
                        fillOpacity: 0.5
                    },
                    column: {
                        grouping: false,
                        shadow: false,
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true, color: 'white',
                            verticalAlign: "top",
                            overflow: true,
                            crop: false,
                            inside: true,
                            // x: -5,
                            style: {
                                fontSize: '12px',
                                fontFamily: 'Verdana, sans-serif',
                                textShadow: '0 0 5px black'
                            }
                        }
                    }
                },
                series: [
                    {
                        name: 'Fixed (Home)',
                        color: '#2090fe',
                        data: <?php echo $fixed; ?>,
                        pointPadding: 0.2
                        , pointPlacement: -0.2
                    },
//                    {
//                        name: 'Episode',
//                        color: '#F1B330',
//                        data: <?php echo $episode; ?>,
//                        pointPadding: 0.25,
//                        pointPlacement: -0.2
//                    },
                    {
                        name: 'Mobility (On the go)',
                        color: '#fe8e20',
                        data: <?php echo $mobile; ?>,
                        pointPadding: 0.35,
                        pointPlacement: -0.2
                    }

                ]
            });


        });

    });
</script>

<div class="" id="Tier"></div>



