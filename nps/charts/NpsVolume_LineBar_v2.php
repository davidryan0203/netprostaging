<?php
session_start();
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/nps/function/NPSFunction.php');
date_default_timezone_set('America/Denver');
//$to = date("Y-m-d", strtotime($_GET['to']));
//$from = date("Y-m-d", strtotime($_GET['from']));
$shared = $_GET['shared'];
$role = $_GET['role'];
$range = date("d/m/Y", strtotime($_GET['from'])) . " to " . date("d/m/Y", strtotime($_GET['to']));
$storeID = $_GET['storeID'];
$dateTo = $_GET['to'];
$dateFrom = $_GET['from'];
$reportView = $_GET['reportView'];

if ($storeID == "SelectStore") {
    echo "<div><center><i>Please Select Store to proceed with the report</i></center></div>";
    exit;
}

$fixed = getNpsVolumeTLS($storeID, 'Fixed', $dateFrom, $dateTo, $quarter = 'Q4');
$mobile = getNpsVolumeTLS($storeID, 'Mobile', $dateFrom, $dateTo, $quarter = 'Q4');

$fixedVolume = json_encode($fixed['volume'][0]['data']);
$fixedNps = json_encode($fixed['weeklyNps'][0]['data']);
$fixedCategory = json_encode($fixed['category']);

$mobileVolume = json_encode($mobile['volume'][0]['data']);
$mobileNps = json_encode($mobile['weeklyNps'][0]['data']);
$mobiledCategory = json_encode($mobile['category']);
?>

<script type="text/javascript">
<?php if ($reportView == 1) { ?>

        $(document).ready(function () {

            var chart4;
            chart4 = new Highcharts.Chart({
                chart: {
                    renderTo: 'MobileChart',
                    zoomType: 'xy',
                    backgroundColor: 'rgba(255, 255, 255, 0.67)'
                },
                title: {
                    text: 'Mobile Store NPS vs Volume'
                },
                xAxis: [{
                        categories: <?php echo $mobiledCategory; ?>,
                        crosshair: true
                    }],
                yAxis: [{// Primary yAxis
                        labels: {
                            format: '{value}',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        },
                        title: {
                            text: 'Survey Volume',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        }
                    }, {// Secondary yAxis
                        title: {
                            text: 'NPS Score',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        labels: {
                            format: '{value}',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        opposite: true
                    }],
                tooltip: {
                    shared: true
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 0
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            align: 'left',
                            style: {
                                fontWeight: 'bold',
                                fontFamily: 'Verdana, sans-serif',
                                textOverflow: 'ellipsis'
                            }
                        },
                        lineColor: '#C0C0C0'
                    },
                    column: {
                        grouping: false,
                        shadow: false,
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true, color: 'white',
                            verticalAlign: "middle",
                            overflow: true,
                            crop: false,
                            inside: true,
                            // x: -5,
                            style: {
                                fontSize: '12px',
                                fontFamily: 'Verdana, sans-serif',
                                textShadow: '0 0 5px black'
                            }
                        }
                    }
                },
                series: [{
                        name: 'NPS Score',
                        type: 'column',
                        yAxis: 1,
                        data: <?php echo $mobileNps; ?>,

                    }, {
                        name: 'Survey Volume',
                        type: 'spline',
                        data: <?php echo $mobileVolume; ?>,

                    }
                ]
            });



            var chart5;
            chart5 = new Highcharts.Chart({
                chart: {
                    renderTo: 'FixedChart',
                    zoomType: 'xy',
                    backgroundColor: 'rgba(255, 255, 255, 0.67)'
                },
                title: {
                    text: 'Fixed Store NPS vs Volume'
                },
                xAxis: [{
                        categories: <?php echo $fixedCategory; ?>,
                        crosshair: true
                    }],
                yAxis: [{// Primary yAxis
                        labels: {
                            format: '{value}',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        },
                        title: {
                            text: 'Survey Volume',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        }
                    }, {// Secondary yAxis
                        title: {
                            text: 'NPS Score',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        labels: {
                            format: '{value}',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        opposite: true
                    }],
                tooltip: {
                    shared: true
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 0
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            align: 'left',
                            style: {
                                fontWeight: 'bold',
                                fontFamily: 'Verdana, sans-serif',
                                textOverflow: 'ellipsis'
                            }
                        },
                        lineColor: '#C0C0C0'
                    },
                    column: {
                        grouping: false,
                        shadow: false,
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true, color: 'white',
                            verticalAlign: "middle",
                            overflow: true,
                            crop: false,
                            inside: true,
                            // x: -5,
                            style: {
                                fontSize: '12px',
                                fontFamily: 'Verdana, sans-serif',
                                textShadow: '0 0 5px black'
                            }
                        }
                    }
                },
                series: [{
                        name: 'NPS Score',
                        type: 'column',
                        yAxis: 1,
                        data: <?php echo $fixedNps; ?>,

                    }, {
                        name: 'Survey Volume',
                        type: 'spline',
                        data: <?php echo $fixedVolume; ?>,

                    }
                ]
            });
        });
<?php } else { ?>

        $(document).ready(function () {

            var chart4;
            chart4 = new Highcharts.Chart({
                chart: {
                    renderTo: 'MobileChart',
                    zoomType: 'xy',
                    backgroundColor: 'rgba(255, 255, 255, 0.67)'
                },
                title: {
                    text: 'Mobile Store Volume vs NPS'
                },
                xAxis: [{
                        categories: <?php echo $mobiledCategory; ?>,
                        crosshair: true
                    }],
                yAxis: [{// Primary yAxis
                        labels: {
                            format: '{value}',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        },
                        title: {
                            text: 'Survey Volume',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        }
                    }, {// Secondary yAxis
                        title: {
                            text: 'NPS Score',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        labels: {
                            format: '{value}',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        opposite: true
                    }],
                tooltip: {
                    shared: true
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 0
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,                          
                            style: {
                                fontWeight: 'bold',
                                fontFamily: 'Verdana, sans-serif',
                                textOverflow: 'ellipsis'                              
                            }
                        },
                        lineColor: '#C0C0C0'
                    },
                    column: {
                        grouping: false,
                        shadow: false,
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true, color: 'white',
                            verticalAlign: "middle",
                            overflow: true,
                            crop: false,
                            inside: true,
                            // x: -5,
                            style: {
                                fontSize: '12px',
                                fontFamily: 'Verdana, sans-serif',
                                textShadow: '0 0 5px black'
                            }
                        }
                    }
                },
                series: [{
                        name: 'Survey Volume',
                        type: 'column',
                        data: <?php echo $mobileVolume; ?>,
                        color: '#87CEEB'
                    }, {
                        name: 'NPS Score',
                        type: 'spline',
                        yAxis: 1,
                        data: <?php echo $mobileNps; ?>,
                        color: '#348939'
                    }
                ]
            });



            var chart5;
            chart5 = new Highcharts.Chart({
                chart: {
                    renderTo: 'FixedChart',
                    zoomType: 'xy',
                    backgroundColor: 'rgba(255, 255, 255, 0.67)'
                },
                title: {
                    text: 'Fixed Store Volume vs NPS'
                },
                xAxis: [{
                        categories: <?php echo $fixedCategory; ?>,
                        crosshair: true
                    }],
                yAxis: [{// Primary yAxis
                        labels: {
                            format: '{value}',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        },
                        title: {
                            text: 'Survey Volume',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        }
                    }, {// Secondary yAxis
                        title: {
                            text: 'NPS Score',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        labels: {
                            format: '{value}',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        opposite: true
                    }],
                 tooltip: {
                  shared: true
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 0
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,                           
                            style: {
                                fontWeight: 'bold',
                                fontFamily: 'Verdana, sans-serif',
                                textOverflow: 'ellipsis',                           
                            }
                        },
                        lineColor: '#C0C0C0'
                    },
                    column: {
                        grouping: false,
                        shadow: false,
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            color: 'white',
                            verticalAlign: "middle",
                            overflow: true,
                            crop: false,
                            inside: true,                            
                            // x: -5,
                            style: {
                                align: 'center',
                                fontSize: '12px',
                                fontFamily: 'Verdana, sans-serif',
                                textShadow: '0 0 5px black'
                            }
                        }
                    }
                },
                series: [{
                        name: 'Survey Volume',
                        type: 'column',
                        data: <?php echo $fixedVolume; ?>,
                        color: '#87CEEB'

                    }, {
                        name: 'NPS Score',
                        type: 'spline',
                        yAxis: 1,
                        data: <?php echo $fixedNps; ?>,
                        color: '#348939'
                    },
                ]
            });


        });
<?php } ?>
</script>
<div>
    <center>
        <table border="0"> 
            <span style="background-color:#348939;color: white;"> <i>Tier 1</i> </span><span style="background-color:#FDBF02; color: white;"> <i>Tier 2 - 3</i> </span><span style="background-color:#FE7E03; color: white;"> <i>Tier 4</i> </span><span style="background-color:#9B1D1E; color: white;"> <i>Tier 5-6 </i> </span>

        </table>
    </center>

</div>

<div class="" id="MobileChart"></div>
<div class="" id="FixedChart"></div>



