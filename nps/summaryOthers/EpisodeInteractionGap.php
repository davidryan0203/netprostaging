<?php
error_reporting(E_ALL);
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');

$sqlGap = "SELECT 
DATE_FORMAT(a.f_DateTimeCreatedMelbourne, '%m-%d-%Y') as Date ,
                b.f_StoreListName ,
                SUM(CASE
                        WHEN (a.f_RecommendRate IS not NULL AND a.f_RecommendRate != '')
                            and a.f_RecommendRate BETWEEN '9' and '10'   THEN  1
                        ELSE 0
                    END) AS EpisodeTotalAdvocate,
                SUM(CASE
                        WHEN (a.f_RecommendRate IS not NULL AND a.f_RecommendRate != '') 
                            and a.f_RecommendRate BETWEEN '7' and '8' THEN  1
                        ELSE 0
                END) AS EpisodeTotalPassive,
                SUM(CASE
                        WHEN (a.f_RecommendRate IS not NULL AND a.f_RecommendRate != '') 
                            and a.f_RecommendRate  BETWEEN '0' and '6' THEN  1
                        ELSE 0
                END) AS EpisodeTotalDetractors,
                SUM(CASE
                        WHEN a.f_RecommendRate IS NULL OR a.f_RecommendRate = '' THEN  1
                        ELSE 0
                END) AS EpisodeBlankCount,               
                SUM(CASE
                        WHEN (a.f_LikelihoodToRecommendRetail IS not NULL AND a.f_LikelihoodToRecommendRetail != '')
                                and a.f_LikelihoodToRecommendRetail BETWEEN '9' and '10'   THEN  1
                        ELSE 0
                    END) AS InteractionTotalAdvocate,
                SUM(CASE
                        WHEN  (a.f_LikelihoodToRecommendRetail IS not NULL AND a.f_LikelihoodToRecommendRetail != '')
                                        and a.f_LikelihoodToRecommendRetail BETWEEN '7' and '8' THEN  1
                        ELSE 0
                END) AS InteractionTotalPassive,
                SUM(CASE
                        WHEN  (a.f_LikelihoodToRecommendRetail IS not NULL AND a.f_LikelihoodToRecommendRetail != '') 
                                        and a.f_LikelihoodToRecommendRetail BETWEEN '0' and '6' THEN  1
                        ELSE 0
                END) AS InteractionTotalDetractors,
                SUM(CASE
                        WHEN  a.f_LikelihoodToRecommendRetail IS NULL OR a.f_LikelihoodToRecommendRetail = ''  THEN  1
                        ELSE 0
                END) AS InteractionBlankCount,
                count(*) as TotalSurvey
                FROM t_surveysd a 
                LEFT JOIN t_storelist b on a.f_StoreID =  b.f_StoreID
                WHERE 1 = 1 
                Group by a.f_StoreID";

$resultGap = mysql_query($sqlGap, $connection);
$i = 0;
while ($rowGap = mysql_fetch_assoc($resultGap)) {
    $InteractionData[$i] = $rowGap;
    $InteractionAdvocatePercentage = $rowInteraction['InteractionTotalAdvocate'] / $rowInteraction['InteractionTotalSurvey'] * 100;
    $InteractionData[$i]['InteractionAdvocatesPercentage'] = $AdvocatePercentage;
    $InteractionPassivesPercentage = $rowInteraction['InteractionTotalPassive'] / $rowInteraction['InteractionTotalSurvey'] * 100;
    $InteractionData[$i]['InteractionPassivesPercentage'] = $InteractionPassivesPercentage;
    $InteractionDetractorsPercentage = $rowInteraction['InteractionTotalDetractors'] / $rowInteraction['InteractionTotalSurvey'] * 100;
    $InteractionData[$i]['InteractionDetractorsPercentage'] = $InteractionDetractorsPercentage;
    $InteractionData[$i]['InteractionNpsScorePercentage'] = $InteractionAdvocatePercentage - $InteractionDetractorsPercentage;

    if ($InteractionData[$i]['InteractionNpsScorePercentage'] >= 75) {
        $InteractionData[$i]['InteractionTierLevel'] = 'Tier 1';
    } elseif ($InteractionData[$i]['InteractionNpsScorePercentage'] >= 65 AND $InteractionData[$i]['InteractionNpsScorePercentage'] <= 74) {
        $InteractionData[$i]['InteractionTierLevel'] = 'Tier 2';
    } elseif ($InteractionData[$i]['InteractionNpsScorePercentage'] >= 55 AND $InteractionData[$i]['InteractionNpsScorePercentage'] <= 64) {
        $InteractionData[$i]['InteractionTierLevel'] = 'Tier 3';
    } elseif ($InteractionData[$i]['InteractionNpsScorePercentage'] >= 45 AND $InteractionData[$i]['InteractionNpsScorePercentage'] <= 54) {
        $InteractionData[$i]['InteractionTierLevel'] = 'Tier 4';
    } else {
        $InteractionData[$i]['InteractionTierLevel'] = 'Tier 5';
    }

    $EpisodeData[$i] = $rowEpisode;
    $EpisodeAdvocatePercentage = $rowEpisode['EpisodeTotalAdvocate'] / $rowEpisode['EpisodeTotalSurvey'] * 100;
    $EpisodeData[$i]['EpisodeAdvocatesPercentage'] = $EpisodeAdvocatePercentage;
    $EpisodePassivesPercentage = $rowEpisode['EpisodeTotalPassive'] / $rowEpisode['EpisodeTotalSurvey'] * 100;
    $EpisodeData[$i]['EpisodePassivesPercentage'] = $EpisodePassivesPercentage;
    $EpisodeDetractorsPercentage = $rowEpisode['EpisodeTotalDetractors'] / $rowEpisode['EpisodeTotalSurvey'] * 100;
    $EpisodeData[$i]['EpisodeDetractorsPercentage'] = $EpisodeDetractorsPercentage;
    $EpisodeData[$i]['EpisodeNpsScorePercentage'] = $EpisodeAdvocatePercentage - $EpisodeDetractorsPercentage;

    if ($EpisodeData[$i]['EpisodeNpsScorePercentage'] >= 50) {
        $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 1';
    } elseif ($EpisodeData[$i]['EpisodeNpsScorePercentage'] >= 40 AND $EpisodeData[$i]['EpisodeNpsScorePercentage'] <= 49) {
        $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 2';
    } elseif ($EpisodeData[$i]['EpisodeNpsScorePercentage'] >= 30 AND $EpisodeData[$i]['EpisodeNpsScorePercentage'] <= 39) {
        $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 3';
    } else {
        $EpisodeData[$i]['EpisodeTierLevel'] = 'Tier 4';
    }
    $i++;
}
print_r($resultGap);
echo mysql_error() ."Here";
//
//echo "<pre>";
//print_r($InteractionData);
exit;
?>


<script type="text/javascript">
    $(document).ready(function () {

        var table = $('#Interaction').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            columnDefs: [
                {sortable: false, targets: -1},
                // {"width": "8%", "targets": all}
            ],
            colReorder: {
                fixedColumnsRight: 1
            },
            "iDisplayLength": 15,
            "aLengthMenu": [[15, 50, 100, -1], [15, 50, 100, "All"]],
            //"bStateSave" : true,
            "oColVis": {
                "buttonText": "Hide Columns"
            },
            initComplete: function () {
                var api = this.api();

                api.columns().indexes().flatten().each(function (i) {
                    var column = api.column(i);
                    var select = $('<select><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );

                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + stripHTML(d).trim() + '">' + stripHTML(d).trim() + '</option>');
                    });
                });
            },
            "sScrollY": "400px",
            "sScrollX": false,
            "bScrollCollapse": true,
            "bPaginate": false,
            "bJQueryUI": true,
            "aoColumnDefs": [
                {"sWidth": "10%", "aTargets": [-1]}
            ]

        });
    });

    function stripHTML(dirtyString) {
        var container = document.createElement('div');
        container.innerHTML = dirtyString;
        return container.textContent;
    }
</script>

<script type="text/javascript">
    $(document).ready(function () {

        $('#ShareType').change(function () {
            alert($(this).bootstrapSwitch);
        });

    });
</script>

<div class="row">
    <div class="col-lg-12">
        <h3 class="page-header">Episode and Interaction Gap 1</h3>   
        <div class="pull-right" style="margin-top: -4%;">  
            <input type="checkbox" id="ShareType" name="ShareType" class="ShareType" 
                   checked data-toggle="toggle" data-style="ios"  data-onstyle="success"  
                   data-offstyle="danger" data-on="Shared" data-off="Mine">  
        </div>
    </div><!--End page-header-->
</div>

<div class="row">
    <div class="col-lg-12">
        <table id="Interaction" class="table table-bordered table-hover table-responsive"  cellspacing="0">
            <thead>
                <tr>
                    <th>Store</th>
                    <th>AD</th>
                    <th>PA</th>
                    <th>D</th>
                    <th>AD%</th>
                    <th>PA%</th>
                    <th>D%</th>
                    <th>Total Surveys</th>
                    <th>Episode Score</th>
                    <th>NPS Tier</th>
                    <th>Store</th>
                    <th>AD</th>
                    <th>PA</th>
                    <th>D</th>
                    <th>AD%</th>
                    <th>PA%</th>
                    <th>D%</th>
                    <th>Total Surveys</th>
                    <th>Episode Score</th>
                    <th>NPS Tier</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Store</th>
                    <th>AD</th>
                    <th>PA</th>
                    <th>D</th>
                    <th>AD%</th>
                    <th>PA%</th>
                    <th>D%</th>
                    <th>Total Surveys</th>
                    <th>Episode Score</th>
                    <th>NPS Tier</th>
                    <th>Store</th>
                    <th>AD</th>
                    <th>PA</th>
                    <th>D</th>
                    <th>AD%</th>
                    <th>PA%</th>
                    <th>D%</th>
                    <th>Total Surveys</th>
                    <th>Episode Score</th>
                    <th>NPS Tier</th>
                </tr>
            </tfoot>
            <tbody>
                <?php
                foreach ($InteractionData as $value) {
                    // var_dump($value);
                    // exit;
                    ?>
                    <tr>  
                        <td><?php echo $value['InteractionStoreName']; ?></td>
                        <td><?php echo $value['InteractionTotalAdvocate']; ?></td>
                        <td><?php echo $value['InteractionTotalPassive']; ?></td>                  
                        <td><?php echo $value['InteractionTotalDetractors']; ?></td>
                        <td><?php echo number_format($value['InteractionAdvocatesPercentage'], 2); ?></td>
                        <td><?php echo number_format($value['InteractionPassivesPercentage'], 2); ?></td>                  
                        <td><?php echo number_format($value['InteractionDetractorsPercentage'], 2); ?></td>
                        <td><?php echo $value['InteractionTotalSurvey']; ?></td>
                        <td><?php echo number_format($value['InteractionNpsScorePercentage'], 2); ?></td>
                        <td><?php echo $value['InteractionTierLevel']; ?></td>
                        <td><?php echo $value['EpisodeStoreName']; ?></td>
                        <td><?php echo $value['EpisodeTotalAdvocate']; ?></td>
                        <td><?php echo $value['EpisodeTotalPassive']; ?></td>                  
                        <td><?php echo $value['EpisodeTotalDetractors']; ?></td>
                        <td><?php echo $value['EpisodeTotalSurvey']; ?></td>
                        <td><?php echo number_format($value['EpisodeAdvocatesPercentage'], 2); ?>%</td>
                        <td><?php echo number_format($value['EpisodePassivesPercentage'], 2); ?>%</td>                  
                        <td><?php echo number_format($value['EpisodeDetractorsPercentage'], 2); ?>%</td>                
                        <td><?php echo number_format($value['EpisodeNpsScorePercentage'], 2); ?>%</td>
                        <td><?php echo $value['EpisodeTierLevel']; ?></td>
                    </tr>   
                <?php } ?>
            </tbody> 
        </table>
    </div>
</div>