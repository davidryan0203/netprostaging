<?php
session_start();
include($_SERVER['DOCUMENT_ROOT'] . '/nps/function/NPSFunction.php');


if (isset($_GET['reconOwnerID']) && !is_null($_GET['reconOwnerID'])) {
    $profileID = $_GET['reconOwnerID'];
} else {
    $profileID = $_SESSION['UserProfileID'];
}

$to = $_GET['to'];
$from = $_GET['from'];

if (isset($_GET['weekly']) && $_GET['weekly'] == "on") {
    $to = date("Y-m-d");
    //  $from = date("Y-m-d", strtotime('4 weeks ago'));
    $from = date("Y-m-d", strtotime('-27 days'));
    $addonStoreName = "wkly";
    // $tableID = "tblInteractionNpsWeekly";
} else {
    // $tableID = "tblInteractionNps";
    $addonStoreName = "";
    if (isset($_GET['from']) && isset($_GET['to'])) {
        $to = $_GET['to'];
        $from = $_GET['from'];
    } else {
        $from = NULL;
        $to = NULL;
    }
}

$shared = $_GET['shared'];
$role = $_GET['role'];
$empID = $_SESSION['EmpProfileID'];
$storeType = $_GET['storeType'];
//Get Employee Store Score Data
$StoreNps = array();


$storeNpsData = getStoreNps($shared, $profileID, $empID, $to, $from, $role, $storeType);
//var_dump($storeNpsData);
foreach ($storeNpsData['Merged'] as $storeData) {
    $StoreNps[] = array(
        'StoreName' => $storeData['StoreName'],
        //episode
        'EpisodeScore' => number_format($storeData['EpisodeNPS'], npsDecimal()),
        'EpisodeTier' => $storeData['EpisodeTier'],
        'EpisodeAdvocate' => $storeData['Advocates'],
        'EpisodePassive' => $storeData['Pasives'],
        'EpisodeDetractor' => $storeData['Detractors'],
        'EpisodeTotalSurvey' => $storeData['TotalSurvey'],
        //interaction
        'InteractionScore' => number_format($storeData['InteractionNPS'], npsDecimal()),
        'InteractionTier' => $storeData['InteractionTier'],
        'InteractionTotalSurvey' => $storeData['InteractionTotalSurvey'],
        'InteractionAdvocate' => $storeData['InteractionAdvocates'],
        'InteractionPassive' => $storeData['InteractionPasives'],
        'InteractionDetractor' => $storeData['InteractionDetractors']
    );
}
//echo "<pre>";
//var_dump($StoreNps);
?>


<?php if (count($StoreNps) > 0) { ?>
    <div class="row">
        <?php foreach ($StoreNps as $value) { ?>
            <div class='col-lg-4 col-md-4 col-sm-6 col-xs-6'>
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="content-header"><i class="fa fa-Home fa-fw"></i><?php
                            echo $value['StoreName'];
                            $storeName = $value['StoreName'] . $addonStoreName;
                            $storeName = str_replace(' ', '', $storeName);
                            $storeName = str_replace('&', '', $storeName);
                            ?>
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nps-summary-target"  style="text-align: center;">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Interaction
                            </div>
                            <div class="panel-body panel-small">
                                <h3> <?php echo $value['InteractionScore']; ?> </h3>
                                <p><i class="fa fa-trophy"></i> <?php echo $value['InteractionTier']; ?> </p>
                            </div>
                            <div class="panel-footer">
                                Surveys: <?php echo $value['InteractionTotalSurvey']; ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nps-summary-target"  style="text-align: center;">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Episode
                            </div>
                            <div class="panel-body panel-small">
                                <h3> <?php echo $value['EpisodeScore']; ?> </h3>
                                <p><i class="fa fa-trophy"></i> <?php echo $value['EpisodeTier']; ?> </p>
                            </div>
                            <div class="panel-footer">
                                Surveys: <?php echo $value['EpisodeTotalSurvey']; ?>
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">
                        $(document).ready(function() {

                            Number.prototype.between = function(min, max) {
                                return this >= min && this <= max;
                            };

                            var NewTotalSurvey2<?php echo $storeName; ?> = <?php echo $value['EpisodeTotalSurvey']; ?>;
                            var newInterAdvoTotal2<?php echo $storeName; ?> = <?php echo $value['EpisodeAdvocate']; ?>;
                            var InteractionTotalDetractorTotal2<?php echo $storeName; ?> = <?php echo $value['EpisodeDetractor']; ?>;

                            var counter2<?php echo $storeName; ?> = 0;
                            var newDetPercentage2<?php echo $storeName; ?> = 0;
                            var newAdvoPercentage2<?php echo $storeName; ?> = 0;
                            var newIntercationNPS2<?php echo $storeName; ?> = 0;
                            var roundedstoreNPS = 0;

                            while (newIntercationNPS2<?php echo $storeName; ?> < 50) {
                                counter2<?php echo $storeName; ?> += 1;
                                newAdvoPercentage2<?php echo $storeName; ?> = (newInterAdvoTotal2<?php echo $storeName; ?> / NewTotalSurvey2<?php echo $storeName; ?>) * 100;
                                newDetPercentage2<?php echo $storeName; ?> = (InteractionTotalDetractorTotal2<?php echo $storeName; ?> / NewTotalSurvey2<?php echo $storeName; ?>) * 100;
                                newIntercationNPS2<?php echo $storeName; ?> = newAdvoPercentage2<?php echo $storeName; ?> - newDetPercentage2<?php echo $storeName; ?>;
                                roundedstoreNPS = Math.round(newIntercationNPS2<?php echo $storeName; ?>);


                                if (roundedstoreNPS < 29) {
                                    $("#EpisodeT3<?php echo $storeName; ?>").empty();
                                    $("#EpisodeT3<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                } else if ((roundedstoreNPS).between(30, 39)) {
                                    $("#EpisodeT2<?php echo $storeName; ?>").empty();
                                    $("#EpisodeT2<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                } else if ((roundedstoreNPS).between(40, 49)) {
                                    $("#EpisodeT1<?php echo $storeName; ?>").empty();
                                    $("#EpisodeT1<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                }
                                newInterAdvoTotal2<?php echo $storeName; ?> += 1;
                                NewTotalSurvey2<?php echo $storeName; ?> += 1;
                            }


                        });

                        $(document).ready(function() {

                            var NewTotalSurveyInt<?php echo $storeName; ?> = <?php echo $value['InteractionTotalSurvey']; ?>;
                            var newInterAdvoTotalInt<?php echo $storeName; ?> = <?php echo $value['InteractionAdvocate']; ?>;
                            var InteractionTotalDetractorTotalInt<?php echo $storeName; ?> = <?php echo $value['InteractionDetractor']; ?>;

                            var counterInt<?php echo $storeName; ?> = 0;
                            var newDetPercentageInt<?php echo $storeName; ?> = 0;
                            var newAdvoPercentageInt<?php echo $storeName; ?> = 0;
                            var newIntercationNPSInt<?php echo $storeName; ?> = 0;
                            var roundedstoreNPS = 0;

                            newAdvoPercentageInt<?php echo $storeName; ?> = (newInterAdvoTotalInt<?php echo $storeName; ?> / NewTotalSurveyInt<?php echo $storeName; ?>) * 100;
                            newDetPercentageInt<?php echo $storeName; ?> = (InteractionTotalDetractorTotalInt<?php echo $storeName; ?> / NewTotalSurveyInt<?php echo $storeName; ?>) * 100;
                            newIntercationNPSInt<?php echo $storeName; ?> = newAdvoPercentageInt<?php echo $storeName; ?> - newDetPercentageInt<?php echo $storeName; ?>;
                            //console.log(newIntercationNPSInt<?php echo $storeName; ?> + 'current');
                            while (newIntercationNPSInt<?php echo $storeName; ?> < 80) {
                                counterInt<?php echo $storeName; ?> += 1;
                                newAdvoPercentageInt<?php echo $storeName; ?> = (newInterAdvoTotalInt<?php echo $storeName; ?> / NewTotalSurveyInt<?php echo $storeName; ?>) * 100;
                                newDetPercentageInt<?php echo $storeName; ?> = (InteractionTotalDetractorTotalInt<?php echo $storeName; ?> / NewTotalSurveyInt<?php echo $storeName; ?>) * 100;
                                newIntercationNPSInt<?php echo $storeName; ?> = newAdvoPercentageInt<?php echo $storeName; ?> - newDetPercentageInt<?php echo $storeName; ?>;

                                roundedstoreNPS = Math.round(newIntercationNPSInt<?php echo $storeName; ?>);

                                if (roundedstoreNPS < 58) {
                                    $("#InteractionT4<?php echo $storeName; ?>").empty();
                                    $("#InteractionT4<?php echo $storeName; ?>").append(counterInt<?php echo $storeName; ?>);
                                } else if ((roundedstoreNPS).between(58, 67)) {
                                    $("#InteractionT3<?php echo $storeName; ?>").empty();
                                    $("#InteractionT3<?php echo $storeName; ?>").append(counterInt<?php echo $storeName; ?>);
                                } else if ((roundedstoreNPS).between(68, 74)) {
                                    $("#InteractionT2<?php echo $storeName; ?>").empty();
                                    $("#InteractionT2<?php echo $storeName; ?>").append(counterInt<?php echo $storeName; ?>);
                                } else if ((roundedstoreNPS).between(75, 79)) {
                                    $("#InteractionT1<?php echo $storeName; ?>").empty();
                                    $("#InteractionT1<?php echo $storeName; ?>").append(counterInt<?php echo $storeName; ?>);
                                }
                                newInterAdvoTotalInt<?php echo $storeName; ?> += 1;
                                NewTotalSurveyInt<?php echo $storeName; ?> += 1;


                            }

                        });
                    </script>
                    <table class="table table-bordered table-hover table-condensed"  cellspacing="0" style="  width: 92% !important; margin: 0 auto; padding-bottom: 20px;" >
                        <thead>
                            <tr>
                                <th>Needed</th>
                                <th>T1</th>
                                <th>T2</th>
                                <th>T3</th>
                                <th>T4</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Interaction</td>
                                <td><div id="InteractionT1<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="InteractionT2<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="InteractionT3<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="InteractionT4<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>

                            </tr>
                            <tr>
                                <td>Episode</td>
                                <td><div id="EpisodeT1<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="EpisodeT2<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="EpisodeT3<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="EpisodeT4<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>

                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php } ?>
    </div>

<?php } else { ?>
    No Records yet.
<?php } ?>
