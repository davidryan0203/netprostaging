<?php
session_start();

include($_SERVER['DOCUMENT_ROOT'] . '/nps/function/NPSFunction.php');

if (isset($_GET['reconOwnerID']) && !is_null($_GET['reconOwnerID'])) {
    $profileID = $_GET['reconOwnerID'];
} else {
    $profileID = $_SESSION['UserProfileID'];
}

$to = $_GET['to'];
$from = $_GET['from'];

if (isset($_GET['weekly']) && $_GET['weekly'] == "on") {
    $to = date("Y-m-d");
    // $from = date("Y-m-d", strtotime('4 weeks ago'));
    $from = date("Y-m-d", strtotime('-27 days'));
    $addonStoreName = "wklyQ3";
    // $tableID = "tblInteractionNpsWeekly";
} elseif (isset($_GET['3months']) && $_GET['3months'] == "on") {
    $to = $_GET['to'];
    $from = date("Y-m-01", strtotime('-3 Months'));
    $tableID = "3MonthsQ3";
} elseif (isset($_GET['htd']) && $_GET['htd'] == "on") {
    $currentMonth = date('m');

    if ($_GET['quarter'] == '2') {
        $from = date("Y-10-01");
        $to = date("Y-12-31");
    } elseif ($_GET['quarter'] == '3') {
        $from = date("Y-01-01");
        $to = date("Y-03-31");
    } elseif ($_GET['quarter'] == '4') {
        $from = date("Y-04-01");
        $to = date("Y-06-30");
    } elseif ($_GET['quarter'] == '1') {
        $from = date("Y-07-01");
        $to = date("Y-09-30");
    }

    $tableID = "htdQ3";
} elseif (isset($_GET['3mma']) && $_GET['3mma'] == "on") {
    $to = $_GET['to'];
    $from = date("Y-m-01", strtotime('first day of -2 Month', strtotime($_GET['to'])));
    $tableID = "3MMAQ3";
} else {
    // $tableID = "tblInteractionNps";
    $addonStoreName = "";
    if (isset($_GET['from']) && isset($_GET['to'])) {
        $to = $_GET['to'];
        $from = $_GET['from'];
    } else {
        $from = NULL;
        $to = NULL;
    }
}

$shared = $_GET['shared'];
$role = $_GET['role'];
$empID = $_SESSION['EmpProfileID'];
$storeType = $_GET['storeType'];
//Get Employee Store Score Data
$StoreNps = array();

//print_r($_GET);


$storeNpsDataQ3 = getStoreNpsOthers($shared, $ownerReconID, $empID, $to, $from, $role, $storeType, $rangeType, "Mobile", "3MMA", "Q3");
$storeNpsDataQ4 = getStoreNpsOthers($shared, $ownerReconID, $empID, $to, $from, $role, $storeType, $rangeType, "Mobile", "3MMA", "Q4");

foreach ($storeNpsDataQ4['Merged'] as $storeData) {
    $old[$storeData['StoreName']] = array(
        //episode
        'Q4MobileScore' => number_format($storeData['MobileNPS'], npsDecimal()),
        'Q4MobileTier' => $storeData['MobileTier'],
        'Q4MobileTotalSurvey' => $storeData['MobileTotalSurvey'],
        'Q4MobileAdvocates' => $storeData['MobileAdvocates'],
        'Q4MobilePasives' => $storeData['MobilePasives'],
        'Q4MobileDetractors' => $storeData['MobileDetractors'],
        'Q4MobilePoints' => $storeData['MobileTotalPoints'],
        //interaction
//        'InteractionScore' => number_format($storeData['InteractionNPS'], 0),
//        'InteractionTier' => $storeData['InteractionTier'],
//        'InteractionTotalSurvey' => $storeData['InteractionTotalSurvey'],
//        'InteractionAdvocates' => $storeData['InteractionAdvocates'],
//        'InteractionPasives' => $storeData['InteractionPasives'],
//        'InteractionDetractors' => $storeData['InteractionDetractors'],
        //fixed
        'Q4FixedScore' => number_format($storeData['FixedNPS'], npsDecimal()),
        'Q4FixedTier' => $storeData['FixedTier'],
        'Q4FixedTotalSurvey' => $storeData['FixedTotalSurvey'],
        'Q4FixedAdvocates' => $storeData['FixedAdvocates'],
        'Q4FixedPasives' => $storeData['FixedPasives'],
        'Q4FixedDetractors' => $storeData['FixedDetractors'],
        'Q4FixedPoints' => $storeData['FixedTotalPoints'],
        //episode
        'Q4EpisodeScore' => number_format($storeData['EpisodeNPS'], npsDecimal()),
        'Q4EpisodeTier' => $storeData['EpisodeTier'],
        'Q4EpisodeTotalSurvey' => $storeData['EpisodeTotalSurvey'],
        'Q4EpisodeAdvocates' => $storeData['EpisodeAdvocates'],
        'Q4EpisodePasives' => $storeData['EpisodePasives'],
        'Q4EpisodeDetractors' => $storeData['EpisodeDetractors'],
        'Q4PointsTotal' => $storeData['PointsTotal']
    );
}

foreach ($storeNpsDataQ3['Merged'] as $storeData) {
    $new[$storeData['StoreName']] = array(
        //episode
        'MobileScore' => number_format($storeData['MobileNPS'], npsDecimal()),
        'MobileTier' => $storeData['MobileTier'],
        'MobileTotalSurvey' => $storeData['MobileTotalSurvey'],
        'MobileAdvocates' => $storeData['MobileAdvocates'],
        'MobilePasives' => $storeData['MobilePasives'],
        'MobileDetractors' => $storeData['MobileDetractors'],
        'MobilePoints' => $storeData['MobileTotalPoints'],
        //interaction
//        'InteractionScore' => number_format($storeData['InteractionNPS'], 0),
//        'InteractionTier' => $storeData['InteractionTier'],
//        'InteractionTotalSurvey' => $storeData['InteractionTotalSurvey'],
//        'InteractionAdvocates' => $storeData['InteractionAdvocates'],
//        'InteractionPasives' => $storeData['InteractionPasives'],
//        'InteractionDetractors' => $storeData['InteractionDetractors'],
        //fixed
        'FixedScore' => number_format($storeData['FixedNPS'], npsDecimal()),
        'FixedTier' => $storeData['FixedTier'],
        'FixedTotalSurvey' => $storeData['FixedTotalSurvey'],
        'FixedAdvocates' => $storeData['FixedAdvocates'],
        'FixedPasives' => $storeData['FixedPasives'],
        'FixedDetractors' => $storeData['FixedDetractors'],
        'FixedPoints' => $storeData['FixedTotalPoints'],
        //episode
        'EpisodeScore' => number_format($storeData['EpisodeNPS'], npsDecimal()),
        'EpisodeTier' => $storeData['EpisodeTier'],
        'EpisodeTotalSurvey' => $storeData['EpisodeTotalSurvey'],
        'EpisodeAdvocates' => $storeData['EpisodeAdvocates'],
        'EpisodePasives' => $storeData['EpisodePasives'],
        'EpisodeDetractors' => $storeData['EpisodeDetractors'],
        'PointsTotal' => $storeData['PointsTotal']
    );
}
$StoreNps = array_merge_recursive($old, $new);

foreach ($StoreNps as $storeKey => $value) {



    if ($storeNps[$storeKey['Q4FixedTotalSurvey']] == 0 && $storeNps[$storeKey['Q4MobileTotalSurvey']] == 0) {

        $pointsTotalQ2 = 0;
        $pointsPercQ2 = 0;
        $StoreNps[$storeKey]['Q4PointsTotal'] = intval($value['Q4FixedPoints']) + intval($value['Q4MobilePoints']);
        $StoreNps[$storeKey]['Q4PointsPerc'] = $StoreNps[$storeKey]['Q4PointsTotal'];


        $pointsTotal = 0;
        $pointsPerc = 0;
        $StoreNps[$storeKey]['PointsTotal'] = intval($value['FixedPoints']) + intval($value['MobilePoints']);
        $StoreNps[$storeKey]['PointsPerc'] = $StoreNps[$storeKey]['PointsTotal'];

        $StoreNps[$storeKey]['BappsScore'] = $StoreNps[$storeKey]['PointsPerc'];
        $sortBapps[] = $StoreNps[$storeKey]['PointsPerc'];
    } else {

        $pointsTotalQ2 = 0;
        $pointsPercQ2 = 0;
        $StoreNps[$storeKey]['Q4PointsTotal'] = intval($value['Q4FixedPoints']) + intval($value['Q4MobilePoints']);
        $StoreNps[$storeKey]['Q4PointsPerc'] = $StoreNps[$storeKey]['Q4PointsTotal'] * .5;


        $pointsTotal = 0;
        $pointsPerc = 0;
        $StoreNps[$storeKey]['PointsTotal'] = intval($value['FixedPoints']) + intval($value['MobilePoints']);
        $StoreNps[$storeKey]['PointsPerc'] = $StoreNps[$storeKey]['PointsTotal'] * .5;


        $StoreNps[$storeKey]['BappsScore'] = $StoreNps[$storeKey]['PointsPerc'] + $StoreNps[$storeKey]['Q4PointsPerc'];
        $sortBapps[] = $StoreNps[$storeKey]['PointsPerc'] + $StoreNps[$storeKey]['Q4PointsPerc'];
    }
}

array_multisort($sortBapps, SORT_DESC, $StoreNps);

function clean($string) {
    $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
?>
<center><h5><i>Report Range <bR> Q3: 2020-01-01 - 2020-03-31 <br>
            Q4:  2020-04-01 - 2020-06-30 </i></h5></center>
<table class="table-bordered table-hover Q3Target" style="margin:auto;" width="60%">
    <thead>
        <tr>
            <th></th>
            <th>Metric</th>
            <th>T1</th>
            <th>T2</th>
            <th>T3</th>
            <th>T4</th>
            <th>T5</th>
            <th>T6</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th rowspan="2">Q3</th>
            <th>Fixed (Home)^</th>
            <td>&ge; 60</td>
            <td>&ge; 53 and &lt; 60</td>
            <td>&ge; 47 and &lt; 53</td>
            <td>&ge; 41 and &lt; 47</td>
            <td>&ge; 23 and &lt; 41</td>
            <td>&lt; 23</td>
        </tr>
        <tr>
            <th>Mobile (On the go)</th>
            <td>&ge; 74</td>
            <td>&ge; 70 and &lt; 74</td>
            <td>&ge; 66 and &lt; 70</td>
            <td>&ge; 61 and &lt; 66</td>
            <td>&ge; 51 and &lt; 61</td>
            <td>&lt; 51</td>
        </tr>
        <tr>
            <th rowspan="2">Q4</th>
            <th>Fixed (Home)^</th>
            <td>&ge; 60</td>
            <td>&ge; 53 and &lt; 60</td>
            <td>&ge; 47 and &lt; 53</td>
            <td>&ge; 41 and &lt; 47</td>
            <td>&ge; 23 and &lt; 41</td>
            <td>&lt; 23</td>
        </tr>
        <tr>
            <th>Mobile (On the go)</th>
            <td>&ge; 74</td>
            <td>&ge; 70 and &lt; 74</td>
            <td>&ge; 66 and &lt; 70</td>
            <td>&ge; 61 and &lt; 66</td>
            <td>&ge; 51 and &lt; 61</td>
            <td>&lt; 51</td>
        </tr>
        <tr><td  colspan="8"><i>^Metric gates shown are for TLS (as a guide for Dealers)</i></td></tr>

    </tbody>
<!--    <tr><td><b><h5>Fixed (Home)</h5></b></td><td>T1 &lt; 39, T2 &ge; 39 and &lt; 45, T3 &ge; 45 and &lt; 51, T4 &ge; 51 and &lt; 58, T5  &ge; 58</td></tr>
    <tr><td><b><h5>Mobile (on the go)<h5></b></td><td>T1 &lt; 59, T2 &ge; 59 and &lt; 64, T3 &ge; 64 and &lt; 68, T4 &ge; 68 and &lt; 72, T5 &ge; 72</td></tr>-->
</table>
<script>
    var divList = $(".listing-item");

    function sortMobile() {
        divList.sort(function (a, b) {
            return $(b).data("mobile") - $(a).data("mobile")
        });
        $("#sortNPS").html(divList);
        $("#sortFixed").removeClass('active');
        $("#sortMobile").addClass('active');
    }

    function sortFixed() {
        divList.sort(function (a, b) {
            return $(b).data("fixed") - $(a).data("fixed")
        });
        $("#sortNPS").html(divList);
        $("#sortMobile").removeClass('active');
        $("#sortFixed").addClass('active');
    }

</script>
<center>
    <button class="btn btn-xs btn-warning active" id="sortMobile" onclick="sortMobile()">Sort by Mobile</button>
    <button class="btn btn-xs btn-info" id="sortFixed" onclick="sortFixed()">Sort by Fixed</button>
</center>
<?php if (count($StoreNps) > 0) { ?>
    <div class="row" id="sortNPS" style="padding-bottom: 15px;">
        <?php
        foreach ($StoreNps as $storeKey => $value) {

            //print_r($value);
//            if (is_null($value['InteractionTotalSurvey'])) {
//                $value['InteractionTotalSurvey'] = 0;
//                $value['InteractionAdvocates'] = 0;
//                $value['InteractionDetractors'] = 0;
//            }
            if (is_null($value['MobileTotalSurvey']) || empty($value['MobileTotalSurvey']) || $value['MobileTotalSurvey'] == 'nan' || $value['MobileTotalSurvey'] == '') {

                $value['MobileTotalSurvey'] = 0;
                $value['MobileAdvocates'] = 0;
                $value['MobileDetractors'] = 0;
            }
            if (is_null($value['FixedTotalSurvey']) || empty($value['FixedTotalSurvey']) || $value['FixedTotalSurvey'] == 'nan' || $value['FixedTotalSurvey'] == '') {

                $value['FixedTotalSurvey'] = 0;
                $value['FixedAdvocates'] = 0;
                $value['FixedDetractors'] = 0;
            }
            if (is_null($value['Q4EpisodeTotalSurvey'])) {

                $value['Q4EpisodeTotalSurvey'] = 0;
                $value['Q4EpisodeAdvocates'] = 0;
                $value['Q4EpisodeDetractors'] = 0;
            }
            if (is_null($value['Q4MobileTotalSurvey']) || empty($value['Q4MobileTotalSurvey']) || $value['Q4MobileTotalSurvey'] == 'nan' || $value['Q4MobileTotalSurvey'] == '') {

                $value['Q4MobileTotalSurvey'] = 0;
                $value['Q4MobileAdvocates'] = 0;
                $value['Q4MobileDetractors'] = 0;
            }
            if (is_null($value['Q4FixedTotalSurvey']) || empty($value['Q4FixedTotalSurvey']) || $value['Q4FixedTotalSurvey'] == 'nan' || $value['Q4FixedTotalSurvey'] == '') {

                $value['Q4FixedTotalSurvey'] = 0;
                $value['Q4FixedAdvocates'] = 0;
                $value['Q4FixedDetractors'] = 0;
            }
            if (is_null($value['Q4EpisodeTotalSurvey'])) {

                $value['Q4EpisodeTotalSurvey'] = 0;
                $value['Q4EpisodeAdvocates'] = 0;
                $value['Q4EpisodeDetractors'] = 0;
            }
            ?>
            <div class='col-lg-6 col-md-4 col-sm-6 col-xs-6 listing-item' data-fixed="<?php echo $value['FixedScore']; ?>" data-mobile="<?php echo $value['MobileScore']; ?>" style="text-align: center;margin-top: 32px;">

                <div class="row">

                    <div class="col-lg-12">
                        <table border="3" class="col-lg-12 table-bordered">
                            <tr>
                                <td colspan="3"><h4 class="pull-left col-lg-12 col-md-12"><i class="fa fa-Home fa-fw"></i><?php
                                        /**   $pointsTotalQ2 = 0;
                                          $pointsPercQ2 = 0;
                                          $pointsTotalQ2 = intval($value['Q2FixedPoints']) + intval($value['Q2MobilePoints']);
                                          $pointsPercQ2 = $pointsTotalQ2 * .5;


                                          $pointsTotal = 0;
                                          $pointsPerc = 0;
                                          $pointsTotal = intval($value['FixedPoints']) + intval($value['MobilePoints']);
                                          $pointsPerc = $pointsTotal * .5;

                                          $bappsScore = $pointsPerc + $pointsPercQ2; */
                                        echo $storeKey;
                                        if (isset($_GET['htd']) && $_GET['htd'] == "on") {
                                            echo " (<i>{$value['BappsScore']} Points</i>)";
                                        }
                                        $storeName = clean($storeKey);
                                        ?>
                                    </h4>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><h3>Q3</h3></td>
                                <td><h3>Q4</h3></td>
                            </tr>
                            <tr>
                                <td rowspan="2" style="width:10%">
                                    <SPAN STYLE="writing-mode: vertical-lr;
                                          -ms-writing-mode: tb-rl;
                                          transform: rotate(180deg);">Fixed (Home)
                                    </SPAN>
                                </td>
                                <td style="width:40%">
                                    <h3> <?php echo $value['FixedScore']; ?> </h3>
                                    <p> <?php echo $value['FixedTier']; ?> </p>
                                    <?php if (isset($_GET['htd']) && $_GET['htd'] == "on") { ?>  <p><i><?php echo $value['FixedPoints']; ?> Points </i></p> <?php } ?>
                                </td>
                                <td style="width:40%">
                                    <h3> <?php echo $value['Q4FixedScore']; ?> </h3>
                                    <p> <?php echo $value['Q4FixedTier']; ?> </p>
                                    <?php if (isset($_GET['htd']) && $_GET['htd'] == "on") { ?>  <p><i><?php echo $value['Q4FixedPoints']; ?> Points </i></p> <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td> Survey: <?php echo $value['FixedTotalSurvey']; ?></td>
                                <td> Survey: <?php echo $value['Q4FixedTotalSurvey']; ?></td>
                            </tr>

                        </table>
                        <table border="3" class="col-lg-12 table-bordered" >
                            <tr>
                                <td rowspan="2" style="width:10%">
                                    <SPAN STYLE="writing-mode: vertical-lr;
                                          -ms-writing-mode: tb-rl;
                                          transform: rotate(180deg);">Mobile (OTG)
                                    </SPAN>
                                </td>
                                <td style="width:40%">
                                    <h3> <?php echo $value['MobileScore']; ?> </h3>
                                    <p><?php echo $value['MobileTier']; ?> </p>
                                    <?php if (isset($_GET['htd']) && $_GET['htd'] == "on") { ?>  <p><i><?php echo $value['MobilePoints']; ?> Points </i></p> <?php } ?>
                                </td>
                                <td style="width:40%">
                                    <h3> <?php echo $value['Q4MobileScore']; ?> </h3>
                                    <p><?php echo $value['Q4MobileTier']; ?> </p>
                                    <?php if (isset($_GET['htd']) && $_GET['htd'] == "on") { ?>  <p><i><?php echo $value['Q4MobilePoints']; ?> Points </i></p> <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td> Survey: <?php echo $value['MobileTotalSurvey']; ?></td>
                                <td> Survey: <?php echo $value['Q4MobileTotalSurvey']; ?></td>
                            </tr>
                            
                            <tr>
                                <th colspan="3">BAPPS Score: <?php echo $value['BappsScore']; ?> </th>
                            </tr>
                        </table >
                        <script type="text/javascript">
                            $(document).ready(function () {


                                Number.prototype.between = function (min, max) {
                                    return this >= min && this <= max;
                                };

                                var NewTotalSurvey2<?php echo $storeName; ?> = <?php echo $value['Q4MobileTotalSurvey']; ?>;
                                var newInterAdvoTotal2<?php echo $storeName; ?> = <?php echo $value['Q4MobileAdvocates']; ?>;
                                var InteractionTotalDetractorTotal2<?php echo $storeName; ?> = <?php echo $value['Q4MobileDetractors']; ?>;
                                var counter2<?php echo $storeName; ?> = 0;
                                var newDetPercentage2<?php echo $storeName; ?> = 0;
                                var newAdvoPercentage2<?php echo $storeName; ?> = 0;
                                var newIntercationNPS2<?php echo $storeName; ?> = 0;
                                var roundedstoreNPS = 0;
                                while (newIntercationNPS2<?php echo $storeName; ?> < 74) {
                                    counter2<?php echo $storeName; ?> += 1;
                                    newAdvoPercentage2<?php echo $storeName; ?> = (newInterAdvoTotal2<?php echo $storeName; ?> / NewTotalSurvey2<?php echo $storeName; ?>) * 100;
                                    newDetPercentage2<?php echo $storeName; ?> = (InteractionTotalDetractorTotal2<?php echo $storeName; ?> / NewTotalSurvey2<?php echo $storeName; ?>) * 100;
                                    newIntercationNPS2<?php echo $storeName; ?> = newAdvoPercentage2<?php echo $storeName; ?> - newDetPercentage2<?php echo $storeName; ?>;
                                    roundedstoreNPS = newIntercationNPS2<?php echo $storeName; ?>;
                                    roundedstoreNPS = roundedstoreNPS.toFixed(1);
                                    if (roundedstoreNPS < 51) {
                                        $("#Q4MobileT5<?php echo $storeName; ?>").empty();
                                        $("#Q4MobileT5<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                        //        console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
                                    } else if (roundedstoreNPS >= 51 && roundedstoreNPS < 61) {
                                        $("#Q4MobileT4<?php echo $storeName; ?>").empty();
                                        $("#Q4MobileT4<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                        //        console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
                                    } else if (roundedstoreNPS >= 61 && roundedstoreNPS < 66) {
                                        $("#Q4MobileT3<?php echo $storeName; ?>").empty();
                                        $("#Q4MobileT3<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                        //        console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
                                    } else if (roundedstoreNPS >= 66 && roundedstoreNPS < 70) {
                                        $("#Q4MobileT2<?php echo $storeName; ?>").empty();
                                        $("#Q4MobileT2<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                        ;
                                        //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 2");
                                    } else if (roundedstoreNPS >= 70 && roundedstoreNPS < 74) {
                                        $("#Q4MobileT1<?php echo $storeName; ?>").empty();
                                        $("#Q4MobileT1<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                        //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 1");
                                    }
                                    newInterAdvoTotal2<?php echo $storeName; ?> += 1;
                                    NewTotalSurvey2<?php echo $storeName; ?> += 1;
                                }

                                var NewTotalSurveyFix<?php echo $storeName; ?> = <?php echo $value['Q4FixedTotalSurvey']; ?>;
                                var newInterAdvoTotalFix<?php echo $storeName; ?> = <?php echo $value['Q4FixedAdvocates']; ?>;
                                var FixedTotalDetractorTotalFix<?php echo $storeName; ?> = <?php echo $value['Q4FixedDetractors']; ?>;
                                var counterFix<?php echo $storeName; ?> = 0;
                                var newDetPercentageFix<?php echo $storeName; ?> = 0;
                                var newAdvoPercentageFix<?php echo $storeName; ?> = 0;
                                var newIntercationNPSFix<?php echo $storeName; ?> = 0;
                                var roundedstoreNPS = 0;
                                var ft4 = true;
                                var ft3 = true;
                                var ft2 = true;
                                var ft1 = true;
                                newAdvoPercentageFix<?php echo $storeName; ?> = (newInterAdvoTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                                newDetPercentageFix<?php echo $storeName; ?> = (FixedTotalDetractorTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                                newIntercationNPSFix<?php echo $storeName; ?> = newAdvoPercentageFix<?php echo $storeName; ?> - newDetPercentageFix<?php echo $storeName; ?>;
                                //console.log(newIntercationNPSFix<?php echo $storeName; ?> + 'current');
                                while (newIntercationNPSFix<?php echo $storeName; ?> < 60) {

                                    counterFix<?php echo $storeName; ?> += 1;

                                    newAdvoPercentageFix<?php echo $storeName; ?> = (newInterAdvoTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                                    newDetPercentageFix<?php echo $storeName; ?> = (FixedTotalDetractorTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                                    newIntercationNPSFix<?php echo $storeName; ?> = newAdvoPercentageFix<?php echo $storeName; ?> - newDetPercentageFix<?php echo $storeName; ?>;
                                    roundedstoreNPS = newIntercationNPSFix<?php echo $storeName; ?>;
                                    roundedstoreNPS = roundedstoreNPS.toFixed(1);
                                    //console.log('Store Name: <?php echo $storeName; ?> ' + counterFix<?php echo $storeName; ?> + " NPS: " + newIntercationNPSFix<?php echo $storeName; ?> + " for tier Int")
                                    if (roundedstoreNPS < 23) {
                                        $("#Q4FixedT4<?php echo $storeName; ?>").empty();
                                        $("#Q4FixedT4<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                        ft4 = false;
                                        //  console.log(counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier 4");
                                    } else if (roundedstoreNPS >= 23 && roundedstoreNPS < 41) {
                                        $("#Q4FixedT4<?php echo $storeName; ?>").empty();
                                        $("#Q4FixedT4<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                        ft3 = false;
                                        //console.log(counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier 3");
                                    } else if (roundedstoreNPS >= 41 && roundedstoreNPS < 47) {
                                        $("#Q4FixedT3<?php echo $storeName; ?>").empty();
                                        $("#Q4FixedT3<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                        ft3 = false;
                                        //console.log(counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier 3");
                                    } else if (roundedstoreNPS >= 47 && roundedstoreNPS < 53) {
                                        $("#Q4FixedT2<?php echo $storeName; ?>").empty();
                                        $("#Q4FixedT2<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                        ft2 = false;
                                        //   console.log(counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier Int");
                                    } else if (roundedstoreNPS >= 53 && roundedstoreNPS < 60) {
                                        $("#Q4FixedT1<?php echo $storeName; ?>").empty();
                                        $("#Q4FixedT1<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                        ft1 = false;
                                        // console.log(counterFix<?php echo $storeName; ?> + " NPS: " + newIntercationNPSFix<?php echo $storeName; ?> + " for tier 1");
                                    }

                                    newInterAdvoTotalFix<?php echo $storeName; ?> += 1;
                                    NewTotalSurveyFix<?php echo $storeName; ?> += 1;
                                }
                            });
                        </script>
                        <table class="col-lg-12 table-bordered TierTargetHTDQ4" style="padding-bottom: 20px;">
                            <thead>
                                <tr>
                                    <th>Needed</th>
                                    <th>T1</th>
                                    <th>T2</th>
                                    <th>T3</th>
                                    <th>T4</th>
                                    <th>T5</th>
                                    <th>T6</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Fixed (Home)</td>
                                    <td><div id="Q4FixedT1<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="Q4FixedT2<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="Q4FixedT3<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="Q4FixedT4<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="Q4FixedT5<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="Q4FixedT6<?php echo $storeName; ?>"><?php
                                            if ($value['FixedTier'] == 'No Nps Score') {
                                                echo 1;
                                            } else {
                                                ?><i class="fa fa-check"><?php } ?></i></div></td>
                                </tr>
                                <tr>
                                    <td>Mobility (On the go)</td>
                                    <td><div id="Q4MobileT1<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="Q4MobileT2<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="Q4MobileT3<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="Q4MobileT4<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="Q4MobileT5<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="Q4MobileT6<?php echo $storeName; ?>"><?php
                                            if ($value['MobileTier'] == 'No Nps Score') {
                                                echo 1;
                                            } else {
                                                ?><i class="fa fa-check"><?php } ?></i></div></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

<?php } else { ?>
    No Records yet.
<?php } ?>
