<?php
include('db/connection.php');
include ('nps/function/NPSFunction.php');

$StoreInteraction = getStoreNps();

//echo "<pre>";
//var_dump($StoreInteraction);
?>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#gap').DataTable({
            "sDom": '<"top">"RC<bottom">',
            "oColVis": {
                "buttonText": "Show Columns"
            },
            initComplete: function () {
                var api = this.api();

                api.columns().indexes().flatten().each(function (i) {
                    var column = api.column(i);
                    var select = $('<select><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );

                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + stripHTML(d).trim() + '">' + stripHTML(d).trim() + '</option>');
                    });
                });
            },
            "autoWidth": false,
            "aoColumnDefs": [
                //  {"sWidth": "10%", "aTargets": [-1]}
                {"bVisible": false, "aTargets": [1, 2, 3, 4, 5, 6, 12, 13, 14, 15, 16, 17],
                    "scrollY": "50px"}
            ]
        });
    });

    function stripHTML(dirtyString) {
        var container = document.createElement('div');
        container.innerHTML = dirtyString;
        return container.textContent;
    }
</script>

<style>
    .red{ 
        background-color: #ff4d4d !important;
    }
    .LightGreen{ 
        background-color: #a5cc6b !important;
    }
    .yellow{ 
        background-color: #ffd700 !important;
    }
    .orange{ 
        background-color: #ff9934 !important;
    }

    #tab-border{
        border: 1px solid #ccc;
        border-radius: 2px;
        padding: 20px 20px 20px 20px;
        background-color: rgba(255, 255, 255, 0.7);
        min-height: 278px;
        max-height: 40%;
    }

    .InteractionHeading{
        background-color: #fe7c53;
        background-image: -webkit-gradient(linear, left top, left bottom, from(#ffd700), to(#ffc200));
        color: white;
        font-size: 20px;
        text-align: center;
    }    

    .EpisodeHeading{
        background-color: #ffd700;
        background-image: -webkit-gradient(linear, left top, left bottom, from(#fe7c53), to(#fe8e20));
        color: white;
        font-size: 20px;
        text-align: center;
    }

    .table-heading, .gap{
        background: #BFDC7A;
        background-image: -webkit-linear-gradient(top, #BFDC7A, #8EBF45);

    }
    .table-body td{
        background: #7abfdc;
        background-image: -webkit-linear-gradient(top, 7abfdc, #7abfdc);
        text-align: center;
    }#tab-border{
        border: 1px solid #ccc;
        border-radius: 2px;
        padding: 20px 20px 20px 20px;
        background-color: rgba(255, 255, 255, 0.7);
        min-height: 278px;
        max-height: 40%;
    }

    .InteractionHeading{
        background-color: #fe7c53;
        background-image: -webkit-gradient(linear, left top, left bottom, from(#ffd700), to(#ffc200));
        color: white;
        font-size: 20px;
        text-align: center;
    }    

    .EpisodeHeading{
        background-color: #ffd700;
        background-image: -webkit-gradient(linear, left top, left bottom, from(#fe7c53), to(#fe8e20));
        color: white;
        font-size: 20px;
        text-align: center;
    }

    .table-heading, .gap{
        background: #BFDC7A;
        background-image: -webkit-linear-gradient(top, #BFDC7A, #8EBF45);

    }
    .table-body td{
        background: #7abfdc;
        background-image: -webkit-linear-gradient(top, 7abfdc, #7abfdc);
        text-align: center;
    }
</style>

<div class="row">
    <h4 class="page-header pull-left" style="margin-top: 7px; margin-bottom: 0px; padding-bottom: 0px;">
        <i class="fa fa-shopping-cart fa-fw"></i>Episode and Interaction Gap
    </h4>
</div>

<div class='row'>
    <h4 class="latest-record pull-left" style=""> 
        <?php
        echo $latestRec['latest_record'];
        ?> 
    </h4>
</div>

<div class="main-content">
    <div class="row">
        <div class="table table-responsive col-lg-7">
            <table id="gap" class="table-hover table-bordered"  cellspacing="0" style="color: white;">
                <thead>
                    <tr>                   
                        <th colspan="9"  class="EpisodeHeading">EPISODE</th>
                        <th colspan="1"  rowspan="2" class="gap" style="background: #01b1ea !important; font-size: 20px;"> GAP </th>
                        <th colspan="9"  class="InteractionHeading">INTERACTION</th>
                    </tr>                    
                    <tr class="table-heading">                    
                        <th>Store</th>                    
                        <th>AD</th>
                        <th>PA</th>
                        <th>D</th>
                        <th>AD%</th>
                        <th>PA%</th>
                        <th>D%</th> 
                        <th>Episode </th>
                        <th>Tier</th>
                        <th>Tier</th>
                        <th>Interaction</th>
                        <th>AD</th>
                        <th>PA</th>
                        <th>D</th> 
                        <th>AD%</th>  
                        <th>PA%</th>
                        <th>D%</th>
                        <th>Surveys</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    foreach ($StoreInteraction['Merged'] as $value) {
                        $gap = $value['EpisodeNPS'] - $value['InteractionNPS'];
                        //echo "<pre>";
                        //print_r($value);
                        ?>

                        <tr class="table-body">                        
                            <td style="text-align: left !important;"><?php echo $value['StoreName']; ?></td>                    
                            <td><?php echo $value['Advocates']; ?></td>
                            <td><?php echo $value['Pasives']; ?></td>
                            <td><?php echo $value['Detractors']; ?></td>
                            <td><?php echo number_format($value['AdvocatesPercentage'], 0); ?>%</td>
                            <td><?php echo number_format($value['PasivesPercentage'], 0); ?>%</td>
                            <td><?php echo number_format($value['DetractorsPercentage'], 0); ?>%</td> 

                            <?php
                            if ($value['EpisodeNPS'] >= 50) {
                                $class = "class='LightGreen'";
                            } elseif ($value['EpisodeNPS'] >= 40 AND $value['EpisodeNPS'] <= 49) {
                                $class = "class='yellow'";
                            } elseif ($value['EpisodeNPS'] >= 30 AND $value['EpisodeNPS'] <= 39) {
                                $class = "class='Orange'";
                            } elseif ($value['EpisodeNPS'] >= 0 AND $value['EpisodeNPS'] <= 29) {
                                $class = "class='red'";
                            } else {
                                $class = "class='red'";
                            }
                            ?>
                            <td <?php echo $class; ?>><?php echo number_format($value['EpisodeNPS'], 0); ?></td>

                            <?php
                            if ($value['EpisodeTier'] == "Tier 1") {
                                $class = "class='LightGreen'";
                            } elseif ($value['EpisodeTier'] == "Tier 2") {
                                $class = "class='yellow'";
                            } elseif ($value['EpisodeTier'] == "Tier 3") {
                                $class = "class='Orange'";
                            } else {
                                $class = "class='red'";
                            }
                            ?>
                            <td <?php echo $class; ?>><?php echo $value['EpisodeTier']; ?></td> 

                            <?php
                            if ($gap >= -20) {
                                $class = "class='LightGreen'";
                            } else {
                                $class = "class='red'";
                            }
                            ?> 
                            <td <?php echo $class; ?>><?php echo $gap; ?></td>

                            <?php
                            if ($value['InteractionTier'] == "Tier 1") {
                                $class = "class='LightGreen'";
                            } elseif ($value['InteractionTier'] == "Tier 2") {
                                $class = "class='yellow'";
                            } elseif ($value['InteractionTier'] == "Tier 3") {
                                $class = "class='Orange'";
                            } elseif ($value['InteractionTier'] == "Tier 4") {
                                $class = "class='red'";
                            } else {
                                $class = "class='red'";
                            }
                            ?>
                            <td <?php echo $class; ?>><?php echo $value['InteractionTier']; ?></td>

                            <?php
                            if ($value['InteractionNPS'] >= 75) {
                                $class = "class='LightGreen'";
                            } elseif ($value['InteractionNPS'] >= 65 AND $value['InteractionNPS'] <= 74) {
                                $class = "class='yellow'";
                            } elseif ($value['InteractionNPS'] >= 55 AND $value['InteractionNPS'] <= 64) {
                                $class = "class='orange'";
                            } elseif ($value['InteractionNPS'] >= 45 AND $value['InteractionNPS'] <= 54) {
                                $class = "class='red'";
                            } else {
                                $class = "class='red'";
                            }
                            ?>
                            <td <?php echo $class; ?>><?php echo number_format($value['InteractionNPS'], 0); ?></td>
                            <td><?php echo $value['InteractionAdvocates']; ?></td> 
                            <td><?php echo $value['InteractionPasives']; ?></td>
                            <td><?php echo $value['InteractionDetractors']; ?></td>
                            <td><?php echo number_format($value['InteractionAdvocatesPercentage'], 0); ?>%</td>                            
                            <td><?php echo number_format($value['InteractionPasivesPercentage'], 0); ?>%</td>
                            <td><?php echo number_format($value['InteractionDetractorsPercentage'], 0); ?>%</td>
                            <td><?php echo $value['TotalSurvey']; ?></td>
                        </tr>   
                    <?php } ?>
                </tbody> 
            </table>
        </div>
    </div>
</div>
