<?php
error_reporting(0);
session_start();
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/dashboard/function/getChamp.php');

if (isset($_GET['weekly']) && $_GET['weekly'] == "on") {
    $to = date("Y-m-d");
    // $from = date("Y-m-d", strtotime('4 weeks ago'));
    $from = date("Y-m-d", strtotime('-27 days'));
    $tableID = "WeeklyMergedMobile";
} elseif (isset($_GET['3mma']) && $_GET['3mma'] == "on") {
    $to = $_GET['to'];
    $from = date("Y-m-01", strtotime('first day of -2 Month', strtotime($_GET['to'])));
    $tableID = "3MMAMergedMobile";
} else {
    $tableID = "MergedMobile";
    if (isset($_GET['from']) && isset($_GET['to'])) {
        $from = date("Y-m-d", strtotime($_GET['from']));
        $to = date("Y-m-d", strtotime($_GET['to']));
    } else {
        $from = NULL;
        $to = NULL;
    }
}

$sharedType = $_GET['shared'];
$storeType = $_GET['storeType'];
if (isset($_GET['reconOwnerID']) && !is_null($_GET['reconOwnerID'])) {
    $ownerReconID = $_GET['reconOwnerID'];
} else {
    $ownerReconID = NULL;
}
$fromNpsPage = 'Yes';
$EmployeeNps = getChampTLS('Mobile', 'AllEmployees', $sharedType, $from, $to, 'IncludeALLEpisode', $storeType, 'Yes', NULL, $ownerReconID, $fromNpsPage);
//echo "<pre>";
//var_dump($EmployeeNps);
?>


<script type="text/javascript">
    $(document).ready(function() {
        //Refresh Modal Content
        $('a.feedback<?php echo $tableID; ?>').click(function(ev) { // prepare modal for new data
            ev.preventDefault();
            var target = $(this).attr('href');
            $("#feedbackmodal<?php echo $tableID; ?> .modal-content<?php echo $tableID; ?>").load(target, function() {
                $("#feedbackmodal<?php echo $tableID; ?>").modal("show");
            });
        });
    });

<?php if ($sharedType != 'no') { ?>
        $("#<?php echo $tableID; ?> a").removeAttr("href data-target data-toggle class").css("cursor", "pointer");
<?php } ?>

    function sortNumbersIgnoreText(a, b, high) {
        var reg = /[+-]?((\d+(\.\d*)?)|\.\d+)([eE][+-]?[0-9]+)?/;
        a = a.match(reg);
        a = a !== null ? parseFloat(a[0]) : high;
        b = b.match(reg);
        b = b !== null ? parseFloat(b[0]) : high;
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    }

    function stripHTML(dirtyString) {
        var container = document.createElement('div');
        container.innerHTML = dirtyString;
        return container.textContent;
    }

    jQuery.extend(jQuery.fn.dataTableExt.oSort, {
        "sort-numbers-ignore-text-asc": function(a, b) {
            return sortNumbersIgnoreText(a, b, Number.POSITIVE_INFINITY);
        },
        "sort-numbers-ignore-text-desc": function(a, b) {
            return sortNumbersIgnoreText(a, b, Number.NEGATIVE_INFINITY) * -1;
        }
    });

    $(document).ready(function() {
        var tblname = "#<?php echo $tableID; ?>";
        var t = $(tblname).DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            "iDisplayLength": 15,
            "aLengthMenu": [[15, 50, 100, -1], [15, 50, 100, "All"]],
            columnDefs: [
//                {sortable: false, targets: -1},
//                {"width": "8%", "targets": -1}

            ],
            "oColVis": {
                "buttonText": "Hide Columns"
            },
            //Footer Sorting
            initComplete: function() {
                var api = this.api();

                api.columns().indexes().flatten().each(function(i) {
                    var column = api.column(i);
                    var select = $('<select><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function() {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );

                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });

                    column.data().unique().sort().each(function(d, j) {
                        select.append('<option value="' + stripHTML(d).trim() + '">' + stripHTML(d).trim() + '</option>');
                    });
                });
            },
            "autoWidth": true,
            "sScrollY": "519px",
            "bScrollCollapse": true,
            "bPaginate": true
        });

        t.on('order.dt search.dt', function() {
            t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        //Remove Heading Sorting
//        $("th").unbind();
//        $("th").removeClass("sorting");
//        $("th").removeClass("sorting_asc");

        $(".feedbackmodal<?php echo $tableID; ?>").on("hidden.bs.modal", function() {
            $(".modal-content<?php echo $tableID; ?>").html("");
        });
    });
</script>

<div class="modal fade" id="feedbackmodal<?php echo $tableID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content<?php echo $tableID; ?>"></div>
    </div>
</div> <!-- /Feedback Modal -->

<table id="<?php echo $tableID; ?>" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
    <thead>
        <tr>
            <th style="width:20px; text-align: center;">Rank</th>
            <th>Name</th>
            <th>Owner</th>
            <th>AD</th>
            <th>PA</th>
            <th>D</th>
            <th>No Score</th>
            <th>Total</th>
            <th>NPS</th>
            <th>Without you</th>
            <th>Impact</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th style="width:20px; text-align: center;">Rank</th>
            <th>Name</th>
            <th>Owner</th>
            <th>AD</th>
            <th>PA</th>
            <th>D</th>
            <th>No Score</th>
            <th>Total</th>
            <th>NPS</th>
            <th>Without you</th>
            <th>Impact</th>
        </tr>
    </tfoot>
    <tbody>
        <?php
        foreach ($EmployeeNps[0] as $value) {
            //echo $key;
            ?>
            <tr>
                <td style="width:20px; text-align: center;"></td>
                <td><?php echo $value['Data']['EmpName']; ?></td>
                <td><?php echo $value['Data']['StoreOwnerName']; ?></td>
                <td>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps/function/FeedbackModalTLS.php?rate=advo&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&type=mobile&frompage=empnpsmerged<?php echo $tableID; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpAdvocate']; ?>
                    </a>
                </td>
                <td>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps/function/FeedbackModalTLS.php?rate=pass&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&type=mobile&frompage=empnpsmerged<?php echo $tableID; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpPassive']; ?>
                    </a>
                </td>
                <td>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps/function/FeedbackModalTLS.php?rate=detract&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&type=mobile&frompage=empnpsmerged<?php echo $tableID; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpDetractor']; ?>
                    </a>
                </td>
                <td>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps/function/FeedbackModalTLS.php?empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&type=mobile&rate=noScore&frompage=empnpsmerged<?php echo $tableID; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpNoScore']; ?></a>
                </td>
                <td>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps/function/FeedbackModalTLS.php?empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&modulator=episode&type=mobile&frompage=empnpsmerged<?php echo $tableID; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpTotalSurvey']; ?>
                    </a>
                </td>
                <td><?php echo number_format($value['Data']['EmpMobile'], npsDecimal()); ?></td>
                <td><?php echo number_format($value['EmpNotInStore'], 0); ?></td>
                <td><?php echo number_format($value['EmpImpact'], 2); ?></td>
            </tr>
        <?php }
        ?>
    </tbody>
</table>











