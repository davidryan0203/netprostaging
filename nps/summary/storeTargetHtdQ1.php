<?php
session_start();

include($_SERVER['DOCUMENT_ROOT'] . '/nps/function/NPSFunction.php');

if (isset($_GET['reconOwnerID']) && !is_null($_GET['reconOwnerID'])) {
    $profileID = $_GET['reconOwnerID'];
} else {
    $profileID = $_SESSION['UserProfileID'];
}

$to = $_GET['to'];
$from = $_GET['from'];

if (isset($_GET['weekly']) && $_GET['weekly'] == "on") {
    $to = date("Y-m-d");
    // $from = date("Y-m-d", strtotime('4 weeks ago'));
    $from = date("Y-m-d", strtotime('-27 days'));
    $addonStoreName = "wklyQ3";
    // $tableID = "tblInteractionNpsWeekly";
} elseif (isset($_GET['3months']) && $_GET['3months'] == "on") {
    $to = $_GET['to'];
    $from = date("Y-m-01", strtotime('-3 Months'));
    $tableID = "3MonthsQ3";
} elseif (isset($_GET['htd']) && $_GET['htd'] == "on") {
    $currentMonth = date('m');

    if ($_GET['quarter'] == '2') {
        $from = date("Y-10-01");
        $to = date("Y-12-31");
    } elseif ($_GET['quarter'] == '3') {
        $from = date("Y-01-01");
        $to = date("Y-03-31");
    } elseif ($_GET['quarter'] == '4') {
        $from = date("Y-04-01");
        $to = date("Y-06-30");
    } elseif ($_GET['quarter'] == '1') {
        $from = date("Y-07-01");
        $to = date("Y-09-30");
    }

    $tableID = "htdQ3";
} elseif (isset($_GET['3mma']) && $_GET['3mma'] == "on") {
    $to = $_GET['to'];
    $from = date("Y-m-01", strtotime('first day of -2 Month', strtotime($_GET['to'])));
    $tableID = "3MMAQ3";
} else {
    // $tableID = "tblInteractionNps";
    $addonStoreName = "";
    if (isset($_GET['from']) && isset($_GET['to'])) {
        $to = $_GET['to'];
        $from = $_GET['from'];
    } else {
        $from = NULL;
        $to = NULL;
    }
}

$shared = $_GET['shared'];
$role = $_GET['role'];
$empID = $_SESSION['EmpProfileID'];
$storeType = $_GET['storeType'];
//Get Employee Store Score Data
$StoreNps = array();

//print_r($_GET);

$fromH1 = date('Y-07-01');
$toH1 = date('Y-12-31');


$storeNpsDataQ1 = getStoreNpsTLS($shared, $ownerReconID, $empID, $to, $from, $role, $storeType, $rangeType, "Mobile", "3MMA", "Q1");
$storeNpsDataQ2 = getStoreNpsTLS($shared, $ownerReconID, $empID, $to, $from, $role, $storeType, $rangeType, "Mobile", "3MMA", "Q2");
$storeNpsDataH1 = getStoreNpsTLS($shared, $ownerReconID, $empID, $toH1, $fromH1, $role, $storeType, $rangeType, "Mobile", "3MMA", "H1");


foreach ($storeNpsDataH1['Merged'] as $storeData) {
    $h1[$storeData['StoreName']] = array(
        //episode
        'H1MobileScore' => number_format($storeData['MobileNPS'], npsDecimal()),
        'H1MobileTier' => $storeData['MobileTier'],
        'H1MobileTotalSurvey' => $storeData['MobileTotalSurvey'],
        'H1MobileAdvocates' => $storeData['MobileAdvocates'],
        'H1MobilePasives' => $storeData['MobilePasives'],
        'H1MobileDetractors' => $storeData['MobileDetractors'],
        'H1MobilePoints' => $storeData['MobileTotalPoints'],
        //interaction
//        'InteractionScore' => number_format($storeData['InteractionNPS'], 0),
//        'InteractionTier' => $storeData['InteractionTier'],
//        'InteractionTotalSurvey' => $storeData['InteractionTotalSurvey'],
//        'InteractionAdvocates' => $storeData['InteractionAdvocates'],
//        'InteractionPasives' => $storeData['InteractionPasives'],
//        'InteractionDetractors' => $storeData['InteractionDetractors'],
        //fixed
        'H1FixedScore' => number_format($storeData['FixedNPS'], npsDecimal()),
        'H1FixedTier' => $storeData['FixedTier'],
        'H1FixedTotalSurvey' => $storeData['FixedTotalSurvey'],
        'H1FixedAdvocates' => $storeData['FixedAdvocates'],
        'H1FixedPasives' => $storeData['FixedPasives'],
        'H1FixedDetractors' => $storeData['FixedDetractors'],
        'H1FixedPoints' => $storeData['FixedTotalPoints'],
        //episode
        'H1EpisodeScore' => number_format($storeData['EpisodeNPS'], npsDecimal()),
        'H1EpisodeTier' => $storeData['EpisodeTier'],
        'H1EpisodeTotalSurvey' => $storeData['EpisodeTotalSurvey'],
        'H1EpisodeAdvocates' => $storeData['EpisodeAdvocates'],
        'H1EpisodePasives' => $storeData['EpisodePasives'],
        'H1EpisodeDetractors' => $storeData['EpisodeDetractors'],
        'H1PointsTotal' => $storeData['PointsTotal']
    );
}


foreach ($storeNpsDataQ2['Merged'] as $storeData) {
    $old[$storeData['StoreName']] = array(
        //episode
        'Q2MobileScore' => number_format($storeData['MobileNPS'], npsDecimal()),
        'Q2MobileTier' => $storeData['MobileTier'],
        'Q2MobileTotalSurvey' => $storeData['MobileTotalSurvey'],
        'Q2MobileAdvocates' => $storeData['MobileAdvocates'],
        'Q2MobilePasives' => $storeData['MobilePasives'],
        'Q2MobileDetractors' => $storeData['MobileDetractors'],
        'Q2MobilePoints' => $storeData['MobileTotalPoints'],
        //interaction
//        'InteractionScore' => number_format($storeData['InteractionNPS'], 0),
//        'InteractionTier' => $storeData['InteractionTier'],
//        'InteractionTotalSurvey' => $storeData['InteractionTotalSurvey'],
//        'InteractionAdvocates' => $storeData['InteractionAdvocates'],
//        'InteractionPasives' => $storeData['InteractionPasives'],
//        'InteractionDetractors' => $storeData['InteractionDetractors'],
        //fixed
        'Q2FixedScore' => number_format($storeData['FixedNPS'], npsDecimal()),
        'Q2FixedTier' => $storeData['FixedTier'],
        'Q2FixedTotalSurvey' => $storeData['FixedTotalSurvey'],
        'Q2FixedAdvocates' => $storeData['FixedAdvocates'],
        'Q2FixedPasives' => $storeData['FixedPasives'],
        'Q2FixedDetractors' => $storeData['FixedDetractors'],
        'Q2FixedPoints' => $storeData['FixedTotalPoints'],
        //episode
        'Q2EpisodeScore' => number_format($storeData['EpisodeNPS'], npsDecimal()),
        'Q2EpisodeTier' => $storeData['EpisodeTier'],
        'Q2EpisodeTotalSurvey' => $storeData['EpisodeTotalSurvey'],
        'Q2EpisodeAdvocates' => $storeData['EpisodeAdvocates'],
        'Q2EpisodePasives' => $storeData['EpisodePasives'],
        'Q2EpisodeDetractors' => $storeData['EpisodeDetractors'],
        'Q2PointsTotal' => $storeData['PointsTotal']
    );
}

foreach ($storeNpsDataQ1['Merged'] as $storeData) {
    $new[$storeData['StoreName']] = array(
        //episode
        'MobileScore' => number_format($storeData['MobileNPS'], npsDecimal()),
        'MobileTier' => $storeData['MobileTier'],
        'MobileTotalSurvey' => $storeData['MobileTotalSurvey'],
        'MobileAdvocates' => $storeData['MobileAdvocates'],
        'MobilePasives' => $storeData['MobilePasives'],
        'MobileDetractors' => $storeData['MobileDetractors'],
        'MobilePoints' => $storeData['MobileTotalPoints'],
        //interaction
//        'InteractionScore' => number_format($storeData['InteractionNPS'], 0),
//        'InteractionTier' => $storeData['InteractionTier'],
//        'InteractionTotalSurvey' => $storeData['InteractionTotalSurvey'],
//        'InteractionAdvocates' => $storeData['InteractionAdvocates'],
//        'InteractionPasives' => $storeData['InteractionPasives'],
//        'InteractionDetractors' => $storeData['InteractionDetractors'],
        //fixed
        'FixedScore' => number_format($storeData['FixedNPS'], npsDecimal()),
        'FixedTier' => $storeData['FixedTier'],
        'FixedTotalSurvey' => $storeData['FixedTotalSurvey'],
        'FixedAdvocates' => $storeData['FixedAdvocates'],
        'FixedPasives' => $storeData['FixedPasives'],
        'FixedDetractors' => $storeData['FixedDetractors'],
        'FixedPoints' => $storeData['FixedTotalPoints'],
        //episode
        'EpisodeScore' => number_format($storeData['EpisodeNPS'], npsDecimal()),
        'EpisodeTier' => $storeData['EpisodeTier'],
        'EpisodeTotalSurvey' => $storeData['EpisodeTotalSurvey'],
        'EpisodeAdvocates' => $storeData['EpisodeAdvocates'],
        'EpisodePasives' => $storeData['EpisodePasives'],
        'EpisodeDetractors' => $storeData['EpisodeDetractors'],
        'PointsTotal' => $storeData['PointsTotal']
    );
}
$StoreNps = array_merge_recursive($old, $new, $h1);

foreach ($StoreNps as $storeKey => $value) {



    if ($value['Q2FixedTotalSurvey'] == 0 && $value['Q2MobileTotalSurvey'] == 0) {

        $pointsTotalQ2 = 0;
        $pointsPercQ2 = 0;
        $StoreNps[$storeKey]['Q2PointsTotal'] = intval($value['Q2FixedPoints']) + intval($value['Q2MobilePoints']);
        $StoreNps[$storeKey]['Q2PointsPerc'] = $StoreNps[$storeKey]['Q2PointsTotal'];


        $pointsTotal = 0;
        $pointsPerc = 0;
        $StoreNps[$storeKey]['PointsTotal'] = intval($value['FixedPoints']) + intval($value['MobilePoints']);
        $StoreNps[$storeKey]['PointsPerc'] = $StoreNps[$storeKey]['PointsTotal'];

        $StoreNps[$storeKey]['BappsScore'] = $StoreNps[$storeKey]['H1PointsTotal'];
        $sortBapps[] = $StoreNps[$storeKey]['BappsScore'];
    } else {

        $pointsTotalQ2 = 0;
        $pointsPercQ2 = 0;
        $StoreNps[$storeKey]['Q2PointsTotal'] = intval($value['Q2FixedPoints']) + intval($value['Q2MobilePoints']);
        $StoreNps[$storeKey]['Q2PointsPerc'] = $StoreNps[$storeKey]['Q2PointsTotal'] * .5;


        $pointsTotal = 0;
        $pointsPerc = 0;
        $StoreNps[$storeKey]['PointsTotal'] = intval($value['FixedPoints']) + intval($value['MobilePoints']);
        $StoreNps[$storeKey]['PointsPerc'] = $StoreNps[$storeKey]['PointsTotal'] * .5;


        $StoreNps[$storeKey]['BappsScore'] = $StoreNps[$storeKey]['H1PointsTotal'];
        $sortBapps[] = $StoreNps[$storeKey]['BappsScore'];
    }
}

array_multisort($sortBapps, SORT_DESC, $StoreNps);

function clean($string) {
    $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
?>
<center><h5><i>Report Range <bR> Q1: 2020-07-01 - 2020-09-30 <br>
            Q2:  2020-10-01 - 2020-12-31 </i></h5></center>
<table class="table-bordered table-hover Q3Target" style="margin:auto;" width="60%">
    <thead>
        <tr>
            <th></th>
            <th>Metric</th>
            <th>T1</th>
            <th>T2</th>
            <th>T3</th>
            <th>T4</th>
            <th>T5</th>
            <th>T6</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th rowspan="2">Q1</th>
            <th>Fixed (Home)</th>
            <td>&ge; 60</td>
            <td>&ge; 53 and &lt; 60</td>
            <td>&ge; 47 and &lt; 53</td>
            <td>&ge; 36 and &lt; 47</td>
            <td>&ge; 13 and &lt; 36</td>
            <td>&lt; 13</td>
        </tr>
        <tr>
            <th>Mobile (OTG)</th>
            <td>&ge; 74</td>
            <td>&ge; 70 and &lt; 74</td>
            <td>&ge; 66 and &lt; 70</td>
            <td>&ge; 56 and &lt; 66</td>
            <td>&ge; 46 and &lt; 56</td>
            <td>&lt; 46</td>
        </tr>
<!--        <tr>
            <th rowspan="2">Q2</th>
            <th>Fixed Home</th>
            <td>&ge; 60</td>
            <td>&ge; 53 and &lt; 60</td>
            <td>&ge; 47 and &lt; 53</td>
            <td>&ge; 41 and &lt; 47</td>
            <td>&ge; 23 and &lt; 41</td>
            <td>&lt; 23</td>
        </tr>
        <tr>
            <th>Mobile (on the go)</th>
            <td>&ge; 74</td>
            <td>&ge; 70 and &lt; 74</td>
            <td>&ge; 66 and &lt; 70</td>
            <td>&ge; 61 and &lt; 66</td>
            <td>&ge; 51 and &lt; 61</td>
            <td>&lt; 51</td>
        </tr>-->

    </tbody>
<!--    <tr><td><b><h5>Fixed (Home)</h5></b></td><td>T1 &lt; 39, T2 &ge; 39 and &lt; 45, T3 &ge; 45 and &lt; 51, T4 &ge; 51 and &lt; 58, T5  &ge; 58</td></tr>
    <tr><td><b><h5>Mobile (on the go)<h5></b></td><td>T1 &lt; 59, T2 &ge; 59 and &lt; 64, T3 &ge; 64 and &lt; 68, T4 &ge; 68 and &lt; 72, T5 &ge; 72</td></tr>-->
</table>
<script>
    var divList = $(".listing-item");

    function sortMobileFY21() {
        divList.sort(function (a, b) {
            return $(b).data("mobile") - $(a).data("mobile")
        });
        $("#sortNPSFY21").html(divList);
        $("#sortFixedFY21").removeClass('active');
        $("#sortMobileFY21").addClass('active');
    }

    function sortFixedFY21() {
        divList.sort(function (a, b) {
            return $(b).data("fixed") - $(a).data("fixed")
        });
        $("#sortNPSFY21").html(divList);
        $("#sortMobileFY21").removeClass('active');
        $("#sortFixedFY21").addClass('active');
    }

</script>
<center>
    <button class="btn btn-xs btn-warning active" id="sortMobileFY21" onclick="sortMobileFY21()">Sort by Mobile</button>
    <button class="btn btn-xs btn-info" id="sortFixedFY21" onclick="sortFixedFY21()">Sort by Fixed</button>
</center>
<?php if (count($StoreNps) > 0) { ?>
    <div class="row" id="sortNPSFY21" style="padding-bottom: 15px;">
        <?php
        foreach ($StoreNps as $storeKey => $value) {

            //print_r($value);
//            if (is_null($value['InteractionTotalSurvey'])) {
//                $value['InteractionTotalSurvey'] = 0;
//                $value['InteractionAdvocates'] = 0;
//                $value['InteractionDetractors'] = 0;
//            }
            if (is_null($value['MobileTotalSurvey']) || empty($value['MobileTotalSurvey']) || $value['MobileTotalSurvey'] == 'nan' || $value['MobileTotalSurvey'] == '') {

                $value['MobileTotalSurvey'] = 0;
                $value['MobileAdvocates'] = 0;
                $value['MobileDetractors'] = 0;
            }
            if (is_null($value['FixedTotalSurvey']) || empty($value['FixedTotalSurvey']) || $value['FixedTotalSurvey'] == 'nan' || $value['FixedTotalSurvey'] == '') {

                $value['FixedTotalSurvey'] = 0;
                $value['FixedAdvocates'] = 0;
                $value['FixedDetractors'] = 0;
            }
            if (is_null($value['Q2EpisodeTotalSurvey'])) {

                $value['Q2EpisodeTotalSurvey'] = 0;
                $value['Q2EpisodeAdvocates'] = 0;
                $value['Q2EpisodeDetractors'] = 0;
            }
            if (is_null($value['Q2MobileTotalSurvey']) || empty($value['Q2MobileTotalSurvey']) || $value['Q2MobileTotalSurvey'] == 'nan' || $value['Q2MobileTotalSurvey'] == '') {

                $value['Q2MobileTotalSurvey'] = 0;
                $value['Q2MobileAdvocates'] = 0;
                $value['Q2MobileDetractors'] = 0;
            }
            if (is_null($value['Q2FixedTotalSurvey']) || empty($value['Q2FixedTotalSurvey']) || $value['Q2FixedTotalSurvey'] == 'nan' || $value['Q2FixedTotalSurvey'] == '') {

                $value['Q2FixedTotalSurvey'] = 0;
                $value['Q2FixedAdvocates'] = 0;
                $value['Q2FixedDetractors'] = 0;
            }
            if (is_null($value['Q2EpisodeTotalSurvey'])) {

                $value['Q2EpisodeTotalSurvey'] = 0;
                $value['Q2EpisodeAdvocates'] = 0;
                $value['Q2EpisodeDetractors'] = 0;
            }if (is_null($value['H1MobileTotalSurvey']) || empty($value['H1MobileTotalSurvey']) || $value['H1MobileTotalSurvey'] == 'nan' || $value['H1MobileTotalSurvey'] == '') {

                $value['H1MobileTotalSurvey'] = 0;
                $value['H1MobileAdvocates'] = 0;
                $value['H1MobileDetractors'] = 0;
            }
            if (is_null($value['H1FixedTotalSurvey']) || empty($value['H1FixedTotalSurvey']) || $value['H1FixedTotalSurvey'] == 'nan' || $value['H1FixedTotalSurvey'] == '') {

                $value['H1FixedTotalSurvey'] = 0;
                $value['H1FixedAdvocates'] = 0;
                $value['H1FixedDetractors'] = 0;
            }
            ?>
            <div class='col-lg-6 col-md-4 col-sm-6 col-xs-6 listing-item' data-fixed="<?php echo $value['FixedScore']; ?>" data-mobile="<?php echo $value['MobileScore']; ?>" style="text-align: center;margin-top: 32px;">

                <div class="row">

                    <div class="col-lg-12">
                        <table border="3" class="col-lg-12 table-bordered">
                            <tr>
                                <td colspan="4"><h3 class="pull-left col-lg-12 col-md-12"><i class="fa fa-Home fa-fw"></i><?php
                                        /**   $pointsTotalQ2 = 0;
                                          $pointsPercQ2 = 0;
                                          $pointsTotalQ2 = intval($value['Q2FixedPoints']) + intval($value['Q2MobilePoints']);
                                          $pointsPercQ2 = $pointsTotalQ2 * .5;


                                          $pointsTotal = 0;
                                          $pointsPerc = 0;
                                          $pointsTotal = intval($value['FixedPoints']) + intval($value['MobilePoints']);
                                          $pointsPerc = $pointsTotal * .5;

                                          $bappsScore = $pointsPerc + $pointsPercQ2; */
                                        echo $storeKey;
                                        if (isset($_GET['htd']) && $_GET['htd'] == "on") {
                                            echo " (<i>{$value['BappsScore']} Points</i>)";
                                        }
                                        $storeName = clean($storeKey);
                                        ?>
                                    </h3>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><h4>Q1</h4></td>
                                <td><h4>Q2</h4></td>
                                <td><h4>HTD</h4></td>
                            </tr>
                            <tr>
                                <td rowspan="2" style="width:10%">
                                    <SPAN STYLE="writing-mode: vertical-lr;
                                          -ms-writing-mode: tb-rl;
                                          transform: rotate(180deg);">Fixed (Home)
                                    </SPAN>
                                </td>
                                <td style="width:30%">
                                    <h3> <?php echo $value['FixedScore']; ?> </h3>
                                    <p> <?php echo $value['FixedTier']; ?> </p>
                                    <?php if (isset($_GET['htd']) && $_GET['htd'] == "on") { ?>  <p><i><?php echo $value['FixedPoints']; ?> Points </i></p> <?php } ?>
                                </td>
                                <td style="width:30%">
                                    <h3> <?php echo $value['Q2FixedScore']; ?> </h3>
                                    <p> <?php echo $value['Q2FixedTier']; ?> </p>
                                    <?php if (isset($_GET['htd']) && $_GET['htd'] == "on") { ?>  <p><i><?php echo $value['Q2FixedPoints']; ?> Points </i></p> <?php } ?>
                                </td>
                                <td style="width:30%">
                                    <h3> <?php echo $value['H1FixedScore']; ?> </h3>
                                    <p> <?php echo $value['H1FixedTier']; ?> </p>
                                    <?php if (isset($_GET['htd']) && $_GET['htd'] == "on") { ?>  <p><i><?php echo $value['H1FixedPoints']; ?> Points </i></p> <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td> Survey: <?php echo $value['FixedTotalSurvey']; ?></td>
                                <td> Survey: <?php echo $value['Q2FixedTotalSurvey']; ?></td>
                                <td> Survey: <?php echo $value['H1FixedTotalSurvey']; ?></td>
                            </tr>

                        </table>
                        <table border="3" class="col-lg-12 table-bordered" >
                            <tr>
                                <td rowspan="2" style="width:10%">
                                    <SPAN STYLE="writing-mode: vertical-lr;
                                          -ms-writing-mode: tb-rl;
                                          transform: rotate(180deg);">Mobile (OTG)
                                    </SPAN>
                                </td>
                                <td style="width:30%">
                                    <h3> <?php echo $value['MobileScore']; ?> </h3>
                                    <p><?php echo $value['MobileTier']; ?> </p>
                                    <?php if (isset($_GET['htd']) && $_GET['htd'] == "on") { ?>  <p><i><?php echo $value['MobilePoints']; ?> Points </i></p> <?php } ?>
                                </td>
                                <td style="width:30%">
                                    <h3> <?php echo $value['Q2MobileScore']; ?> </h3>
                                    <p><?php echo $value['Q2MobileTier']; ?> </p>
                                    <?php if (isset($_GET['htd']) && $_GET['htd'] == "on") { ?>  <p><i><?php echo $value['Q2MobilePoints']; ?> Points </i></p> <?php } ?>
                                </td>
                                <td style="width:30%">
                                    <h3> <?php echo $value['H1MobileScore']; ?> </h3>
                                    <p><?php echo $value['H1MobileTier']; ?> </p>
                                    <?php if (isset($_GET['htd']) && $_GET['htd'] == "on") { ?>  <p><i><?php echo $value['H1MobilePoints']; ?> Points </i></p> <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td> Survey: <?php echo $value['MobileTotalSurvey']; ?></td>
                                <td> Survey: <?php echo $value['Q2MobileTotalSurvey']; ?></td>
                                <td> Survey: <?php echo $value['H1MobileTotalSurvey']; ?></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="3" style="width:100%">
                                    Home: <?php echo $value['H1FixedPoints']; ?> Points
                                    <br> OTG: <?php echo $value['H1MobilePoints']; ?>  Points

                                    <br> Total: <?php echo $value['H1PointsTotal']; ?>
                                </td>   
                            </tr>
                            <tr>
                                <th colspan="4">BAPPS Score: <?php echo $value['BappsScore']; ?> </th>
                            </tr>
                        </table >





                        <script type="text/javascript">
                            $(document).ready(function () {


                                Number.prototype.between = function (min, max) {
                                    return this >= min && this <= max;
                                };

                                var NewTotalSurvey2<?php echo $storeName; ?> = <?php echo $value['H1MobileTotalSurvey']; ?>;
                                var newInterAdvoTotal2<?php echo $storeName; ?> = <?php echo $value['H1MobileAdvocates']; ?>;
                                var InteractionTotalDetractorTotal2<?php echo $storeName; ?> = <?php echo $value['H1MobileDetractors']; ?>;
                                var counter2<?php echo $storeName; ?> = 0;
                                var newDetPercentage2<?php echo $storeName; ?> = 0;
                                var newAdvoPercentage2<?php echo $storeName; ?> = 0;
                                var newIntercationNPS2<?php echo $storeName; ?> = 0;
                                var roundedstoreNPS = 0;

                                newAdvoPercentageFix<?php echo $storeName; ?> = (newInterAdvoTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                                newDetPercentageFix<?php echo $storeName; ?> = (FixedTotalDetractorTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                                newIntercationNPSFix<?php echo $storeName; ?> = newAdvoPercentageFix<?php echo $storeName; ?> - newDetPercentageFix<?php echo $storeName; ?>;

                                while (newIntercationNPS2<?php echo $storeName; ?> < 74) {
                                    counter2<?php echo $storeName; ?> += 1;
                                    newAdvoPercentage2<?php echo $storeName; ?> = (newInterAdvoTotal2<?php echo $storeName; ?> / NewTotalSurvey2<?php echo $storeName; ?>) * 100;
                                    newDetPercentage2<?php echo $storeName; ?> = (InteractionTotalDetractorTotal2<?php echo $storeName; ?> / NewTotalSurvey2<?php echo $storeName; ?>) * 100;
                                    newIntercationNPS2<?php echo $storeName; ?> = newAdvoPercentage2<?php echo $storeName; ?> - newDetPercentage2<?php echo $storeName; ?>;
                                    roundedstoreNPS = newIntercationNPS2<?php echo $storeName; ?>;
                                    roundedstoreNPS = roundedstoreNPS.toFixed(1);
                                    if (roundedstoreNPS < 46) {
                                        $("#H1MobileT5<?php echo $storeName; ?>").empty();
                                        $("#H1MobileT5<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                        //        console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
                                    } else if (roundedstoreNPS >= 46 && roundedstoreNPS < 56) {
                                        $("#H1MobileT4<?php echo $storeName; ?>").empty();
                                        $("#H1MobileT4<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                        //        console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
                                    } else if (roundedstoreNPS >= 56 && roundedstoreNPS < 66) {
                                        $("#H1MobileT3<?php echo $storeName; ?>").empty();
                                        $("#H1MobileT3<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                        //        console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
                                    } else if (roundedstoreNPS >= 66 && roundedstoreNPS < 70) {
                                        $("#H1MobileT2<?php echo $storeName; ?>").empty();
                                        $("#H1MobileT2<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                        ;
                                        //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 2");
                                    } else if (roundedstoreNPS >= 70 && roundedstoreNPS < 74) {
                                        $("#H1MobileT1<?php echo $storeName; ?>").empty();
                                        $("#H1MobileT1<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                        //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 1");
                                    }
                                    newInterAdvoTotal2<?php echo $storeName; ?> += 1;
                                    NewTotalSurvey2<?php echo $storeName; ?> += 1;
                                }

                                var NewTotalSurveyFix<?php echo $storeName; ?> = <?php echo $value['H1FixedTotalSurvey']; ?>;
                                var newInterAdvoTotalFix<?php echo $storeName; ?> = <?php echo $value['H1FixedAdvocates']; ?>;
                                var FixedTotalDetractorTotalFix<?php echo $storeName; ?> = <?php echo $value['H1FixedDetractors']; ?>;
                                var counterFix<?php echo $storeName; ?> = 0;
                                var newDetPercentageFix<?php echo $storeName; ?> = 0;
                                var newAdvoPercentageFix<?php echo $storeName; ?> = 0;
                                var newIntercationNPSFix<?php echo $storeName; ?> = 0;
                                var roundedstoreNPS = 0;
                                var ft4 = true;
                                var ft3 = true;
                                var ft2 = true;
                                var ft1 = true;
                                newAdvoPercentageFix<?php echo $storeName; ?> = (newInterAdvoTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                                newDetPercentageFix<?php echo $storeName; ?> = (FixedTotalDetractorTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                                newIntercationNPSFix<?php echo $storeName; ?> = newAdvoPercentageFix<?php echo $storeName; ?> - newDetPercentageFix<?php echo $storeName; ?>;
                                //console.log(newIntercationNPSFix<?php echo $storeName; ?> + 'current');
                                while (newIntercationNPSFix<?php echo $storeName; ?> < 60) {

                                    counterFix<?php echo $storeName; ?> += 1;

                                    newAdvoPercentageFix<?php echo $storeName; ?> = (newInterAdvoTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                                    newDetPercentageFix<?php echo $storeName; ?> = (FixedTotalDetractorTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                                    newIntercationNPSFix<?php echo $storeName; ?> = newAdvoPercentageFix<?php echo $storeName; ?> - newDetPercentageFix<?php echo $storeName; ?>;
                                    roundedstoreNPS = newIntercationNPSFix<?php echo $storeName; ?>;
                                    roundedstoreNPS = roundedstoreNPS.toFixed(1);
                                    //console.log('Store Name: <?php echo $storeName; ?> ' + counterFix<?php echo $storeName; ?> + " NPS: " + newIntercationNPSFix<?php echo $storeName; ?> + " for tier Int")
                                    if (roundedstoreNPS < 13) {
                                        $("#H1FixedT4<?php echo $storeName; ?>").empty();
                                        $("#H1FixedT4<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                        ft4 = false;
                                        //  console.log(counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier 4");
                                    } else if (roundedstoreNPS >= 13 && roundedstoreNPS < 36) {
                                        $("#H1FixedT4<?php echo $storeName; ?>").empty();
                                        $("#H1FixedT4<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                        ft3 = false;
                                        //console.log(counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier 3");
                                    } else if (roundedstoreNPS >= 36 && roundedstoreNPS < 47) {
                                        $("#H1FixedT3<?php echo $storeName; ?>").empty();
                                        $("#H1FixedT3<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                        ft3 = false;
                                        //console.log(counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier 3");
                                    } else if (roundedstoreNPS >= 47 && roundedstoreNPS < 53) {
                                        $("#H1FixedT2<?php echo $storeName; ?>").empty();
                                        $("#H1FixedT2<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                        ft2 = false;
                                        //   console.log(counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier Int");
                                    } else if (roundedstoreNPS >= 53 && roundedstoreNPS < 60) {
                                        $("#H1FixedT1<?php echo $storeName; ?>").empty();
                                        $("#H1FixedT1<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                        ft1 = false;
                                        // console.log(counterFix<?php echo $storeName; ?> + " NPS: " + newIntercationNPSFix<?php echo $storeName; ?> + " for tier 1");
                                    }

                                    newInterAdvoTotalFix<?php echo $storeName; ?> += 1;
                                    NewTotalSurveyFix<?php echo $storeName; ?> += 1;
                                }
                            });
                        </script>
                        <table class="col-lg-12 table-bordered TierTargetHTDH1" style="padding-bottom: 20px;">
                            <thead>
                                <tr>
                                    <th>Needed</th>
                                    <th>T1</th>
                                    <th>T2</th>
                                    <th>T3</th>
                                    <th>T4</th>
                                    <th>T5</th>
                                    <th>T6</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Fixed (Home)</td>
                                    <td><div id="H1FixedT1<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="H1FixedT2<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="H1FixedT3<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="H1FixedT4<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="H1FixedT5<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="H1FixedT6<?php echo $storeName; ?>"><?php
                                            if ($value['FixedTier'] == 'No NPS Score') {
                                                echo 1;
                                            } else {
                                                ?><i class="fa fa-check"><?php } ?></i></div></td>
                                </tr>
                                <tr>
                                    <td>Mobility (OTG)</td>
                                    <td><div id="H1MobileT1<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="H1MobileT2<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="H1MobileT3<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="H1MobileT4<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="H1MobileT5<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="H1MobileT6<?php echo $storeName; ?>"><?php
                                            if ($value['MobileTier'] == 'No NPS Score') {
                                                echo 1;
                                            } else {
                                                ?><i class="fa fa-check"><?php } ?></i></div></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

<?php } else { ?>
    No Records yet.
<?php } ?>
