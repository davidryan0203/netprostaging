<?php
session_start();

include($_SERVER['DOCUMENT_ROOT'] . '/nps/function/NPSFunction.php');

$profileID = $_SESSION['UserProfileID'];
$to = $_GET['toTBC'];
//$to = $to;
$format = 'M-y';
$date = DateTime::createFromFormat($format, $to);

$shared = $_GET['shared'];
$role = $_GET['role'];
$empID = $_SESSION['EmpProfileID'];
$storeType = $_GET['storeType'];
$rangeType = $_GET['rangeType'];
//Get Employee Store Score Data
$StoreNps = array();
if ($rangeType == 'mtd') {
    $to = mysql_real_escape_string($_GET['mtdTo']);
    $from = mysql_real_escape_string($_GET['mtdFrom']);
    // $storeNpsData = getStoreNpsTBC($shared, $profileID, $empID, $mtdTo, $mtdFrom, $role, $storeType, $rangeType);
} else {
    $to = $date->format('Y-m-d');
    $from = $_GET['from'];
}
if (isset($_GET['reconOwnerID']) && !is_null($_GET['reconOwnerID'])) {
    $ownerReconID = $_GET['reconOwnerID'];
} else {
    $ownerReconID = NULL;
}
$storeNpsData = getStoreNpsTBC($shared, $ownerReconID, $empID, $to, $from, $role, $storeType, $rangeType);
//var_dump($storeNpsData);
foreach ($storeNpsData['Merged'] as $storeData) {
    $StoreNps[] = array(
        'StoreName' => $storeData['StoreName'],
        //episode
        'MobileScore' => number_format($storeData['MobileNPS'], npsDecimal()),
        'MobileTier' => $storeData['MobileTier'],
        'MobileTotalSurvey' => $storeData['MobileTotalSurvey'],
        'MobileAdvocates' => $storeData['MobileAdvocates'],
        'MobilePasives' => $storeData['MobilePasives'],
        'MobileDetractors' => $storeData['MobileDetractors'],
        //interaction
        'EpisodeScore' => number_format($storeData['EpisodeNPS'], npsDecimal()),
        'EpisodeTier' => $storeData['EpisodeTier'],
        'EpisodeTotalSurvey' => $storeData['EpisodeTotalSurvey'],
        'EpisodeAdvocates' => $storeData['EpisodeAdvocates'],
        'EpisodePasives' => $storeData['EpisodePasives'],
        'EpisodeDetractors' => $storeData['EpisodeDetractors'],
        //fixed
        'FixedScore' => number_format($storeData['FixedNPS'], npsDecimal()),
        'FixedTier' => $storeData['FixedTier'],
        'FixedTotalSurvey' => $storeData['FixedTotalSurvey'],
        'FixedAdvocates' => $storeData['FixedAdvocates'],
        'FixedPasives' => $storeData['FixedPasives'],
        'FixedDetractors' => $storeData['FixedDetractors']
    );
}

//echo "<pre>";
//var_dump($StoreNps);
function clean($string) {
    $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
?>

<table class="table-bordered table-hover Q3Target" style="margin:auto;text-align: center; 
    vertical-align: middle;" width="50%">
    <thead>
        <tr>
            <th></th>       
            <th>T1</th>
            <th>T2</th>
            <th>T3</th>
            <th>T4</th>
            <th>T5</th>
        </tr>
    </thead>
    <tbody> 
         <tr>         
            <th>Episode</th>
            <td>&ge; 71</td>
            <td>&ge; 64 and &lt; 71</td>
            <td>&ge; 46 and &lt; 64</td>
            <td>&ge; 27 and &lt; 46</td>
            <td>&lt; 27 </td>          
        </tr>    
        <tr>
            <th>Mobile Episode</th>
            <td>&ge; 59</td>
            <td>&ge; 52 and &lt; 59</td>
            <td>&ge; 45 and &lt; 52</td>
            <td>&ge; 35 and &lt; 45</td>          
            <td>&lt; 35</td>
        </tr>
        <tr>         
            <th>Fixed Episode</th>
            <td>&ge; 15</td>
            <td>&ge; 0 and &lt; 15</td>
            <td>&ge; -13 and &lt; 0</td>
            <td>&ge; -35 and &lt; -13</td>           
            <td>&lt; -35</td>
        </tr>

    </tbody>
<!--    <tr><td><b><h5>Fixed (Home)</h5></b></td><td>T1 &lt; 39, T2 &ge; 39 and &lt; 45, T3 &ge; 45 and &lt; 51, T4 &ge; 51 and &lt; 58, T5  &ge; 58</td></tr>
    <tr><td><b><h5>Mobile (on the go)<h5></b></td><td>T1 &lt; 59, T2 &ge; 59 and &lt; 64, T3 &ge; 64 and &lt; 68, T4 &ge; 68 and &lt; 72, T5 &ge; 72</td></tr>-->
</table>
<br>

<?php if (count($StoreNps) > 0) { ?>
    <div class="row" style="padding-bottom: 15px;">
        <?php
        foreach ($StoreNps as $value) {

            if (is_null($value['EpisodeTotalSurvey'])) {
                $value['EpisodeTotalSurvey'] = 0;
                $value['EpisodeAdvocates'] = 0;
                $value['EpisodeDetractors'] = 0;
            }
            if (is_null($value['MobileTotalSurvey'])) {

                $value['MobileTotalSurvey'] = 0;
                $value['MobileAdvocates'] = 0;
                $value['MobileDetractors'] = 0;
            }
            if (is_null($value['FixedTotalSurvey'])) {

                $value['FixedTotalSurvey'] = 0;
                $value['FixedAdvocates'] = 0;
                $value['FixedDetractors'] = 0;
            }
            ?>
            <div class='col-lg-6 col-md-4 col-sm-6 col-xs-6' style="text-align: center;">
                <div class="row">
                    <h4 class="pull-left col-lg-12 col-md-12"><i class="fa fa-Home fa-fw"></i><?php
                        echo $value['StoreName'];
                        $storeName = clean($value['StoreName']);
                        ?>
                    </h4>
                </div>
                <div class="row">
                    <div class="col-12-lg">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nps-summary-target"  style="text-align: center;">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Episode
                                </div>
                                <div class="panel-body panel-small">
                                    <h3> <?php echo $value['EpisodeScore']; ?> </h3>
                                    <p><i class="fa fa-trophy"></i> <?php echo $value['EpisodeTier']; ?> </p>
                                </div>
                                <div class="panel-footer">
                                    Surveys: <?php echo $value['EpisodeTotalSurvey']; ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nps-summary-target"  style="text-align: center;">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Mobility Episode
                                </div>
                                <div class="panel-body panel-small">
                                    <h3> <?php echo $value['MobileScore']; ?> </h3>
                                    <p><i class="fa fa-trophy"></i> <?php echo $value['MobileTier']; ?> </p>
                                </div>
                                <div class="panel-footer">
                                    Surveys: <?php echo $value['MobileTotalSurvey']; ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 nps-summary-target"  style="text-align: center;">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Fixed Episode
                                </div>
                                <div class="panel-body panel-small">
                                    <h3> <?php echo $value['FixedScore']; ?> </h3>
                                    <p><i class="fa fa-trophy"></i> <?php echo $value['FixedTier']; ?> </p>
                                </div>
                                <div class="panel-footer">
                                    Surveys: <?php echo $value['FixedTotalSurvey']; ?>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                            $(document).ready(function() {


                                Number.prototype.between = function(min, max) {
                                    return this >= min && this <= max;
                                };

                                var NewTotalSurveyInt<?php echo $storeName; ?> = <?php echo $value['EpisodeTotalSurvey']; ?>;
                                var newInterAdvoTotalInt<?php echo $storeName; ?> = <?php echo $value['EpisodeAdvocates']; ?>;
                                var InteractionTotalDetractorTotalInt<?php echo $storeName; ?> = <?php echo $value['EpisodeDetractors']; ?>;

                                var counterEpi<?php echo $storeName; ?> = 0;
                                var newDetPercentageInt<?php echo $storeName; ?> = 0;
                                var newAdvoPercentageInt<?php echo $storeName; ?> = 0;
                                var newIntercationNPSInt<?php echo $storeName; ?> = 0;
                                var roundedstoreNPS = 0;

                                newAdvoPercentageInt<?php echo $storeName; ?> = (newInterAdvoTotalInt<?php echo $storeName; ?> / NewTotalSurveyInt<?php echo $storeName; ?>) * 100;
                                newDetPercentageInt<?php echo $storeName; ?> = (InteractionTotalDetractorTotalInt<?php echo $storeName; ?> / NewTotalSurveyInt<?php echo $storeName; ?>) * 100;
                                newIntercationNPSInt<?php echo $storeName; ?> = newAdvoPercentageInt<?php echo $storeName; ?> - newDetPercentageInt<?php echo $storeName; ?>;
                                //console.log(newIntercationNPSInt<?php echo $storeName; ?> + 'current <?php echo $storeName; ?>');

                                while (newIntercationNPSInt<?php echo $storeName; ?> < 71) {
                                    counterEpi<?php echo $storeName; ?> += 1;
                                    newAdvoPercentageInt<?php echo $storeName; ?> = (newInterAdvoTotalInt<?php echo $storeName; ?> / NewTotalSurveyInt<?php echo $storeName; ?>) * 100;
                                    newDetPercentageInt<?php echo $storeName; ?> = (InteractionTotalDetractorTotalInt<?php echo $storeName; ?> / NewTotalSurveyInt<?php echo $storeName; ?>) * 100;
                                    newIntercationNPSInt<?php echo $storeName; ?> = newAdvoPercentageInt<?php echo $storeName; ?> - newDetPercentageInt<?php echo $storeName; ?>;

                                    roundedstoreNPS = Math.round(newIntercationNPSInt<?php echo $storeName; ?>);
                                    if (roundedstoreNPS < 27) {
                                        $("#EpisodeT4<?php echo $storeName; ?>").empty();
                                        $("#EpisodeT4<?php echo $storeName; ?>").append(counterEpi<?php echo $storeName; ?>);
                                        //        console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
                                    } else if ((roundedstoreNPS).between(27, 45)) {
                                        $("#EpisodeT3<?php echo $storeName; ?>").empty();
                                        $("#EpisodeT3<?php echo $storeName; ?>").append(counterEpi<?php echo $storeName; ?>);
                                        //        console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
                                    } else if ((roundedstoreNPS).between(46, 63)) {
                                        $("#EpisodeT2<?php echo $storeName; ?>").empty();
                                        $("#EpisodeT2<?php echo $storeName; ?>").append(counterEpi<?php echo $storeName; ?>);
                                        ;
                                        //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 2");
                                    } else if ((roundedstoreNPS).between(64, 70)) {
                                        $("#EpisodeT1<?php echo $storeName; ?>").empty();
                                        $("#EpisodeT1<?php echo $storeName; ?>").append(counterEpi<?php echo $storeName; ?>);
                                        //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 1");
                                    }

                                    newInterAdvoTotalInt<?php echo $storeName; ?> += 1;
                                    NewTotalSurveyInt<?php echo $storeName; ?> += 1;
                                    //console.log(NewTotalSurveyInt + " -" + newInterAdvoTotalInt + "  +" + counterInt + " " + newIntercationNPSInt);

                                    //npscore = InteractionTotalDetractorPercentage +  newAdvoPercentage + InteractionPassivePercentage;
                                    // console.log(counterInt + " NPS: "+ newIntercationNPSInt +" for tier 4");

                                }


                                //    var needed = counter - newInterAdvoTotal;
                                //$("#InteractionToGet<?php echo $spanID; ?>").append(counter2 + " Advocates</strong> to Tier 1.");
                            });
                        </script>
                        <script type="text/javascript">
                            $(document).ready(function() {

                                Number.prototype.between = function(min, max) {
                                    return this >= min && this <= max;
                                };

                                var NewTotalSurveyMob<?php echo $storeName; ?> = <?php echo $value['MobileTotalSurvey']; ?>;
                                var newInterAdvoTotalMob<?php echo $storeName; ?> = <?php echo $value['MobileAdvocates']; ?>;
                                var InteractionTotalDetractorTotalMob<?php echo $storeName; ?> = <?php echo $value['MobileDetractors']; ?>;

                                var counterMob<?php echo $storeName; ?> = 0;
                                var newDetPercentageMob<?php echo $storeName; ?> = 0;
                                var newAdvoPercentageMob<?php echo $storeName; ?> = 0;
                                var newIntercationNPSMob<?php echo $storeName; ?> = 0;
                                var roundedstoreNPS = 0;

                                newAdvoPercentageMob<?php echo $storeName; ?> = (newInterAdvoTotalMob<?php echo $storeName; ?> / NewTotalSurveyMob<?php echo $storeName; ?>) * 100;
                                newDetPercentageMob<?php echo $storeName; ?> = (InteractionTotalDetractorTotalMob<?php echo $storeName; ?> / NewTotalSurveyMob<?php echo $storeName; ?>) * 100;
                                newIntercationNPSMob<?php echo $storeName; ?> = newAdvoPercentageMob<?php echo $storeName; ?> - newDetPercentageMob<?php echo $storeName; ?>;

                                //  console.log(newIntercationNPSMob<?php echo $storeName; ?> + 'current <?php echo $storeName; ?> MOB');

                                while (newIntercationNPSMob<?php echo $storeName; ?> < 59) {
                                    counterMob<?php echo $storeName; ?> += 1;
                                    newAdvoPercentageMob<?php echo $storeName; ?> = (newInterAdvoTotalMob<?php echo $storeName; ?> / NewTotalSurveyMob<?php echo $storeName; ?>) * 100;
                                    newDetPercentageMob<?php echo $storeName; ?> = (InteractionTotalDetractorTotalMob<?php echo $storeName; ?> / NewTotalSurveyMob<?php echo $storeName; ?>) * 100;
                                    newIntercationNPSMob<?php echo $storeName; ?> = newAdvoPercentageMob<?php echo $storeName; ?> - newDetPercentageMob<?php echo $storeName; ?>;

                                    roundedstoreNPS = Math.round(newIntercationNPSMob<?php echo $storeName; ?>);

                                    if (roundedstoreNPS < 35) {
                                        $("#MobileT4<?php echo $storeName; ?>").empty();
                                        $("#MobileT4<?php echo $storeName; ?>").append(counterMob<?php echo $storeName; ?>);
                                        //        console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
                                    } else if ((roundedstoreNPS).between(35, 44)) {
                                        $("#MobileT3<?php echo $storeName; ?>").empty();
                                        $("#MobileT3<?php echo $storeName; ?>").append(counterMob<?php echo $storeName; ?>);
                                        //        console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
                                    } else if ((roundedstoreNPS).between(45, 51)) {
                                        $("#MobileT2<?php echo $storeName; ?>").empty();
                                        $("#MobileT2<?php echo $storeName; ?>").append(counterMob<?php echo $storeName; ?>);
                                        ;
                                        //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 2");
                                    } else if ((roundedstoreNPS).between(52, 58)) {
                                        $("#MobileT1<?php echo $storeName; ?>").empty();
                                        $("#MobileT1<?php echo $storeName; ?>").append(counterMob<?php echo $storeName; ?>);
                                        //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 1");
                                    }
                                    newInterAdvoTotalMob<?php echo $storeName; ?> += 1;
                                    NewTotalSurveyMob<?php echo $storeName; ?> += 1;
                                }
                                //    var needed = counter - newInterAdvoTotal;

                            });
                        </script>

                        <script type="text/javascript">
                            $(document).ready(function() {

                                var NewTotalSurveyFix<?php echo $storeName; ?> = <?php echo $value['FixedTotalSurvey']; ?>;
                                var newInterAdvoTotalFix<?php echo $storeName; ?> = <?php echo $value['FixedAdvocates']; ?>;
                                var FixedTotalDetractorTotalFix<?php echo $storeName; ?> = <?php echo $value['FixedDetractors']; ?>;

                                var counterFix<?php echo $storeName; ?> = 0;
                                var newDetPercentageFix<?php echo $storeName; ?> = 0;
                                var newAdvoPercentageFix<?php echo $storeName; ?> = 0;
                                var newIntercationNPSFix<?php echo $storeName; ?> = 0;
                                var roundedstoreNPS = 0;

                                var ft4 = true;
                                var ft3 = true;
                                var ft2 = true;
                                var ft1 = true;

                                newAdvoPercentageFix<?php echo $storeName; ?> = (newInterAdvoTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                                newDetPercentageFix<?php echo $storeName; ?> = (FixedTotalDetractorTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                                // newIntercationNPSFix<?php echo $storeName; ?> = newAdvoPercentageFix<?php echo $storeName; ?> - newDetPercentageFix<?php echo $storeName; ?>;

                                newIntercationNPSFix<?php echo $storeName; ?> = (isNaN(newAdvoPercentageFix<?php echo $storeName; ?> - newDetPercentageFix<?php echo $storeName; ?>)) ? 0 : newAdvoPercentageFix<?php echo $storeName; ?> - newDetPercentageFix<?php echo $storeName; ?>;
                                //  console.log(newIntercationNPSFix<?php echo $storeName; ?> + 'current <?php echo $storeName; ?> FIX');
                                while (newIntercationNPSFix<?php echo $storeName; ?> < 15) {
                                    counterFix<?php echo $storeName; ?> += 1;
                                    newAdvoPercentageFix<?php echo $storeName; ?> = (newInterAdvoTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                                    newDetPercentageFix<?php echo $storeName; ?> = (FixedTotalDetractorTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                                    newIntercationNPSFix<?php echo $storeName; ?> = (isNaN(newAdvoPercentageFix<?php echo $storeName; ?> - newDetPercentageFix<?php echo $storeName; ?>)) ? 0 : newAdvoPercentageFix<?php echo $storeName; ?> - newDetPercentageFix<?php echo $storeName; ?>;


                                    roundedstoreNPS = Math.round(newIntercationNPSFix<?php echo $storeName; ?>);

                                    if (roundedstoreNPS < -35 && ft4) {
                                        $("#FixedT4<?php echo $storeName; ?>").empty();
                                        $("#FixedT4<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                        ft4 = false;
                                        //  console.log(counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier 4");
                                    } else if ((roundedstoreNPS).between(-35, -12) && ft3) {
                                        $("#FixedT3<?php echo $storeName; ?>").empty();
                                        $("#FixedT3<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                        ft3 = false;
                                        //  console.log(counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier 3");
                                    } else if ((roundedstoreNPS).between(-13, -1) && ft2) {
                                        $("#FixedT2<?php echo $storeName; ?>").empty();
                                        $("#FixedT2<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                        ft2 = false;
                                        //  console.log(counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier Int");
                                    } else if ((roundedstoreNPS).between(0, 14) && ft1) {
                                        $("#FixedT1<?php echo $storeName; ?>").empty();
                                        $("#FixedT1<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                        // console.log(counterFix<?php echo $storeName; ?> + " NPS: " + newIntercationNPSFix<?php echo $storeName; ?> + " for tier 1");
                                        ft1 = false;
                                    }
                                    newInterAdvoTotalFix<?php echo $storeName; ?> += 1;
                                    NewTotalSurveyFix<?php echo $storeName; ?> += 1;

                                }

                            });
                        </script>
                    </div>
                    <table class="table table-bordered table-hover table-condensed"  cellspacing="0" style="  width: 92% !important; margin: 0 auto; padding-bottom: 20px;" >
                        <thead>
                            <tr>
                                <th>Needed</th>
                                <th>T1</th>
                                <th>T2</th>
                                <th>T3</th>
                                <th>T4</th>
                                <th>T5</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Episode</td>
                                <td><div id="EpisodeT1<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="EpisodeT2<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="EpisodeT3<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="EpisodeT4<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="EpisodeT5<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                            </tr>
                            <tr>
                                <td>Mobility Episode</td>
                                <td><div id="MobileT1<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="MobileT2<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="MobileT3<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="MobileT4<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="MobileT5<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                            </tr>
                            <tr>
                                <td>Fixed Episode</td>
                                <td><div id="FixedT1<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="FixedT2<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="FixedT3<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="FixedT4<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="FixedT5<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php } ?>
    </div>
<?php } else { ?>
    No Records yet.
<?php } ?>
