<?php
error_reporting(0);
session_start();
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/dashboard/function/getChamp.php');

$sharedType = $_GET['shared'];
$storeType = $_GET['storeType'];
$rangeType = $_GET['rangeType'];

if ($rangeType == 'mtd') {
    $to = mysql_real_escape_string($_GET['mtdTo']);
    $from = mysql_real_escape_string($_GET['mtdFrom']);
} else {
    $to = $_GET['toTBC'];
    $format = 'M-y';
    $date = DateTime::createFromFormat($format, $to);
    $to = $date->format('Y-m-d');
    $from = date('Y-m-01', strtotime("$to + 2  months ago"));
}

if (isset($_GET['reconOwnerID']) && !is_null($_GET['reconOwnerID'])) {
    $ownerReconID = $_GET['reconOwnerID'];
} else {
    $ownerReconID = NULL;
}
$EmployeeNps = getChamp('Interaction', 'AllEmployees', $sharedType, $from, $to, 'IncludeALL', $storeType, 'Yes', NULL, $ownerReconID);
//echo "<pre>";
//var_dump($EmployeeNps);
?>


<script type="text/javascript">
    $(document).ready(function() {
        //Refresh Modal Content
        $('a.feedback').click(function(ev) {
            ev.preventDefault();
            var target = $(this).attr('href');

            $("#feedbackmodal .modal-content").load(target, function() {
                $("#feedbackmodal").modal("show");
            });
        });
    });

    function sortNumbersIgnoreText(a, b, high) {
        var reg = /[+-]?((\d+(\.\d*)?)|\.\d+)([eE][+-]?[0-9]+)?/;
        a = a.match(reg);
        a = a !== null ? parseFloat(a[0]) : high;
        b = b.match(reg);
        b = b !== null ? parseFloat(b[0]) : high;
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    }

    function stripHTML(dirtyString) {
        var container = document.createElement('div');
        container.innerHTML = dirtyString;
        return container.textContent;
    }

    jQuery.extend(jQuery.fn.dataTableExt.oSort, {
        "sort-numbers-ignore-text-asc": function(a, b) {
            return sortNumbersIgnoreText(a, b, Number.POSITIVE_INFINITY);
        },
        "sort-numbers-ignore-text-desc": function(a, b) {
            return sortNumbersIgnoreText(a, b, Number.NEGATIVE_INFINITY) * -1;
        }
    });

    $(document).ready(function() {
        var t = $('#EmpNps2').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            "iDisplayLength": 15,
            "aLengthMenu": [[15, 50, 100, -1], [15, 50, 100, "All"]],
            columnDefs: [
                {sortable: false, targets: -1},
                {"width": "8%", "targets": -1}

            ],
            "oColVis": {
                "buttonText": "Hide Columns"
            },
            //Footer Sorting
            initComplete: function() {
                var api = this.api();

                api.columns().indexes().flatten().each(function(i) {
                    var column = api.column(i);
                    var select = $('<select><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function() {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );

                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });

                    column.data().unique().sort().each(function(d, j) {
                        select.append('<option value="' + stripHTML(d).trim() + '">' + stripHTML(d).trim() + '</option>');
                    });
                });
            },
            "autoWidth": true,
            "sScrollY": "519px",
            "bScrollCollapse": true,
            "bPaginate": true
        });

        t.on('order.dt search.dt', function() {
            t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        //Remove Heading Sorting
        $("th").unbind();
        $("th").removeClass("sorting");
        $("th").removeClass("sorting_asc");
    });
</script>

<div class="modal fade" id="feedbackmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal  CreateStoreModal -->

<table id="EmpNps2" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
    <thead>
        <tr>
            <th style="width:20px; text-align: center;">Rank</th>
            <th>Name</th>
            <th>Owner</th>
            <th>AD</th>
            <th>PA</th>
            <th>D</th>
            <th>No Score</th>
            <th>Total</th>
            <th>NPS</th>
            <th>Without you</th>
            <th>Impact</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th style="width:20px; text-align: center;">Rank</th>
            <th>Name</th>
            <th>Owner</th>
            <th>AD</th>
            <th>PA</th>
            <th>D</th>
            <th>No Score</th>
            <th>Total</th>
            <th>NPS</th>
            <th>Without you</th>
            <th>Impact</th>
        </tr>
    </tfoot>
    <tbody>
        <?php
        foreach ($EmployeeNps[0] as $value) {
            //echo $key;
            ?>
            <tr>
                <td style="width:20px; text-align: center;"></td>
                <td><?php echo $value['Data']['EmpName']; ?></td>
                <td><?php echo $value['Data']['StoreOwnerName']; ?></td>
                <td>
                    <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModalTBC.php?rate=advo&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&type=interaction&storeOwnerID=<?php echo $value['Data']['f_UserID']; ?>&rangeType=<?php echo $rangeType; ?>&merge=yes" data-target="#feedbackmodal">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpAdvocate']; ?>
                    </a>
                </td>
                <td>
                    <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModalTBC.php?rate=pass&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&type=interaction&storeOwnerID=<?php echo $value['Data']['f_UserID']; ?>&rangeType=<?php echo $rangeType; ?>&merge=yes" data-target="#feedbackmodal">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpPassive']; ?>
                    </a>
                </td>
                <td>
                    <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModalTBC.php?rate=detract&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&type=interaction&storeOwnerID=<?php echo $value['Data']['f_UserID']; ?>&rangeType=<?php echo $rangeType; ?>&merge=yes" data-target="#feedbackmodal">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpDetractor']; ?>
                    </a>
                </td>
                <td>
                    <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModalTBC.php?rate=noScore&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&type=interaction&storeOwnerID=<?php echo $value['Data']['f_UserID']; ?>&rangeType=<?php echo $rangeType; ?>&merge=yes" data-target="#feedbackmodal">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpNoScore']; ?></a>
                </td>
                <td>
                    <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModalTBC.php?empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&modulator=exlcude&type=interaction&storeOwnerID=<?php echo $value['Data']['f_UserID']; ?>&rangeType=<?php echo $rangeType; ?>&merge=yes" data-target="#feedbackmodal">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpTotalSurvey']; ?>
                    </a>
                </td>
                <td><?php echo number_format($value['Data']['EmpInteraction'], npsDecimal()); ?></td>
                <td><?php echo number_format($value['EmpNotInStore'], 0); ?></td>
                <td><?php echo number_format($value['EmpImpact'], 2); ?></td>
            </tr>
        <?php }
        ?>
    </tbody>
</table>











