<?php
ini_set('max_execution_time', 300);
include($_SERVER['DOCUMENT_ROOT'] . '/functions/cloudrevision.php');
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
date_default_timezone_set('America/Denver');
$sharedType = $_GET['shared'];
$to = mysql_real_escape_string($_GET['to']);
$from = mysql_real_escape_string($_GET['from']);
$role = mysql_real_escape_string($_GET['role']);
$TBC = mysql_real_escape_string($_GET['toTBC']);
//print_r($_GET);
if ($_GET['mtdSet'] == 'yes') {
    $rangeType = 'yes';
    $mtdTo = mysql_real_escape_string($_GET['mtdTo']);
    $mtdFrom = mysql_real_escape_string($_GET['mtdFrom']);
} else {
    $mtdTo = NULL;
    $mtdFrom = NULL;
}

$first_day_this_month = date('01/m/Y'); // hard-coded '01' for first day
$last_day_this_month = date('t/m/Y');
$storeType = $_GET['storeType'];

if (($to == $last_day_this_month) && ($from == $first_day_this_month)) {
    $dateNow = "Current Month";
} else {
    $dateNow = $from . " - " . $to;
}

$toTBC = date("Y/m/t", strtotime($TBC));

if (isset($_GET['reconOwnerID']) && !is_null($_GET['reconOwnerID'])) {
    $ownerReconID = $_GET['reconOwnerID'];
} else {
    $ownerReconID = NULL;
}
//echo "<br>".$last_day_this_month."-$last_day_this_month <br>".$dateNow."here$to - $from";
?>
<style>table.dataTable tbody th,
    table.dataTable tbody td {
        white-space: nowrap;
    }</style>
<!--TBC nps reports-->
<section class='reports'>

    <div class='filters'>
        <input type="hidden" id="from" name="from" value="<?php echo $from; ?>"/>
        <input type="hidden" id="toTBC" name="toTBC"  value="<?php echo $toTBC; ?>"/>
        <input type="hidden" id="role" name="role" value="<?php echo $role; ?>"/>
        <input type="hidden" id="mtdTo" name="mtdTo" value="<?php echo $mtdTo; ?>" />
        <input type="hidden" id="mtdFrom" name="mtdFrom" value="<?php echo $mtdFrom; ?>" />

        <input type="checkbox" style="display:none" <?php
        if ($sharedType == 'yes') {
            echo "checked=\"checked\" ";
        }
        ?> id="shareType" />

        <input type="hidden" id="from" name="from" value="<?php echo $from; ?>"/>
        <input type="hidden" id="toTBC" name="toTBC"  value="<?php echo $toTBC; ?>"/>
        <input type="hidden" id="role" name="role" value="<?php echo $role; ?>"/>
        <input type="hidden" id="mtdTo" name="mtdTo" value="<?php echo $mtdTo; ?>" />
        <input type="hidden" id="mtdFrom" name="mtdFrom" value="<?php echo $mtdFrom; ?>" />
        <input type="checkbox" style="display:none" <?php
        if ($sharedType == 'yes') {
            echo "checked=\"checked\" ";
        }
        ?> id="shareType" />

        <input type="checkbox" style="display:none" <?php
        if ($rangeType == 'yes') {
            echo "checked=\"checked\" ";
        }
        ?> id="MtdType" />
        <input type="hidden" id="storeType" name="storeType" value="<?php echo $storeType; ?>" />
        <input type="hidden" id="reconOwnerID" name="reconOwnerID" value="<?php echo $ownerReconID; ?>" />

        <input type="hidden" id="storeType" name="storeType" value="<?php echo $storeType; ?>" />
    </div>
    <div class="panel-group" role="tablist" id='accordion'>
        <div class="main-content" id="StoreTargetContent">
            <h4 class="sub-header"><i class="fa fa-bar-chart-o fa-fw"></i>Store Target</h4>
            <div class="loader" id="StoreTargetloader"></div>
            <div  id="StoreTarget"></div>
        </div>

        <!--    <div class="main-content">

                <div class="panel-heading" role="tab" id="headingTwo">
                    <a>
                        <h4 class="sub-header"><i class="fa fa-bar-chart-o fa-fw"></i>Store Tier Gap</h4>
                    </a>
                    <div class="btn-group group1 pull-right" id="storetier-group" role="group" aria-label="...">
                                        <button type="button" id="StoreTierIntNps" data-text="Interaction" data-target="storetierint" class="btn btn-xs btn-primary ">Interaction</button>
                        <button type="button" id="StoreTierMobNps" data-text="Mobility (On the go)"  data-target="storetiermob" class="btn btn-xs btn-warning">Mobile</button>
                        <button type="button" id="StoreTierFixNps" data-text="Fixed (Home)"  data-target="storetierfix" class="btn btn-xs btn-info">Fixed</button>
                        <button type="button" id="StoreTierFixNps" data-text="Default"  data-target="storetierfix" class="btn btn-xs btn-default">Default</button>
                    </div>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel">
                    <div class="panel-body">
                        <div class="loader" id='CurrentMonthTierLoader'></div>
                        <div id="CurrentMonthTier"></div>
                    </div>
                </div>
            </div>-->

        <!--Store Tier Gap-->
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <a role="button" data-toggle="collapse" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" class="accordion-toggle">
                    <h4 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i>Store <tier>Episode</tier> Tier Gap</h4>
                </a>
                <div class="btn-group group1 pull-right" id="storetier-group" role="group" aria-label="..." >
                    <button type="button" id="StoreTierMobNps" data-text="Mobility (OTG)"  data-target="storetiermob" class="btn btn-xs btn-warning"  data-tooltip="tooltip" title="Sort Tier by Mobile">Mobility</button>
                    <button type="button" id="StoreTierFixNps" data-text="Fixed (Home)"  data-target="storetierfix" class="btn btn-xs btn-info"  data-tooltip="tooltip" title="Sort Tier by Fixed">Fixed</button>
                    <button type="button" id="StoreTierEpiNps" data-text="Episode" data-target="storetierepi" class="btn btn-xs btn-success " data-tooltip="tooltip" title="Sort Tier by Episode">Episode</button>
                </div>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel">
                <div class="panel-body">
                    <div class="loader" id='CurrentMonthTierLoader'></div>
                    <div id="CurrentMonthTier"></div>
                </div>
            </div>
        </div>

        <div class="main-content">
            <h4 class="sub-header"><i class="fa fa-bar-chart-o fa-fw"></i> Store Episode NPS</h4>
            <div class="loader" id="EpisodeNpsloader"></div>
            <div class="table table-responsive" id="EpisodeNps"></div>
        </div>

        <div class="main-content">
            <h4 class="sub-header"><i class="fa fa-bar-chart-o fa-fw"></i> Store Mobile Episode NPS</h4>
            <div class="loader" id="EpisodeMobileNpsloader"></div>
            <div class="table table-responsive" id="EpisodeMobileNps"></div>
        </div>

        <div class="main-content">
            <h4 class="sub-header"><i class="fa fa-bar-chart-o fa-fw"></i> Store Fixed Episode NPS</h4>
            <div class="loader" id="FixedNpsloader"></div>
            <div class="table table-responsive" id="FixedNps"></div>
        </div>

        <div class="main-content" id="EmpIEpisode">
            <h4 class="sub-header"><i class="fa fa-bar-chart-o fa-fw"></i> Employee Episode NPS</h4>
            <div class="loader" id="EmployeeNpsloader"></div>
            <div class="table table-responsive" id="EmployeeNps"></div>
        </div>

        <div class="main-content" id="EmpMobile">
            <h4 class="sub-header"><i class="fa fa-bar-chart-o fa-fw"></i> Employee Mobile Episode NPS</h4>
            <div class="loader" id="EmployeeNpsloaderMobile"></div>
            <div class="table table-responsive" id="EmployeeNpsMobile"></div>
        </div>

        <div class="main-content" id="EmpFixed">
            <h4 class="sub-header"><i class="fa fa-bar-chart-o fa-fw"></i> Employee Fixed Episode NPS</h4>
            <div class="loader" id="EmployeeNpsloaderFixed"></div>
            <div class="table table-responsive" id="EmployeeNpsFixed"></div>
        </div>


        <div class="main-content"  id="MergeEmpEpisode">
            <h4 class="sub-header"><i class="fa fa-bar-chart-o fa-fw"></i> Merged Employee Episode NPS</h4>
            <div class="loader" id="MergedEmployeeNpsloader"></div>
            <div class="table table-responsive" id="MergedEmployeeNps"></div>
        </div>

        <div class="main-content"  id="MergeEmpMobileEpisode">
            <h4 class="sub-header"><i class="fa fa-bar-chart-o fa-fw"></i> Merged Employee Mobile Episode NPS</h4>
            <div class="loader" id="MergedEmployeeNpsMobileloader"></div>
            <div class="table table-responsive" id="MergedEmployeeMobileNps"></div>
        </div>

        <div class="main-content"  id="MergeEmpFixed">
            <h4 class="sub-header"><i class="fa fa-bar-chart-o fa-fw"></i> Merged Employee Fixed Episode NPS</h4>
            <div class="loader" id="MergedEmployeeNpsFixedloader"></div>
            <div class="table table-responsive" id="MergedEmployeeFixedNps"></div>
        </div>
    </div>
</section>

<script src="../includes/js/custom.js" type="text/javascript"></script>

<script type="text/javascript">

    $(document).ready(function() {

        var role = $("#role").val();
        var shared = "";
        var storeType = "";
        var mtdSet = "";
        var mtdFrom = "";
        var mtdTo = "";
        var mtdRange = "";

        function setLoad() {
            var from = $("#from").val().split("/").reverse().join("-");
            var to = $("#toTBC").val().split("/").reverse().join("-");
            if ($('#ShareType').prop('checked')) {
                shared = 'yes';
            } else {
                shared = 'no';
            }

            if ($('#StoreType').prop('checked')) {
                storeType = '2';
                if ($('#MtdType').prop('checked')) {
                    $('#tbcPicker').hide();
                    $('#mtdPicker').show();
                    mtdSet = '&rangeType=mtd';
                    mtdFrom = $("#mtdFrom").val().split("/").reverse().join("-");
                    mtdTo = $("#mtdTo").val().split("/").reverse().join("-");

                    mtdRange = "&mtdFrom=" + mtdFrom + "&mtdTo=" + mtdTo;

                } else {
                    $('#mtdPicker').hide();
                    $('#tbcPicker').show();
                    mtdSet = '&rangeType=default';
                }
            } else {
                storeType = '1';
            }

            var reconOwnerID = $("#reconOwnerID").val();

            var addonParams = 'shared=' + shared + '&toTBC=' + to + '&from=' + from + '&role=' + role + '&storeType=' + storeType + mtdSet + mtdRange + '&reconOwnerID=' + reconOwnerID;
            // console.log(addonParams);
            return addonParams;
        }

        if ($('#ShareType').prop('checked')) {
            $('#StoreTargetContent,#EmpMobile,#EmpFixed').hide();
        } else {
            $('#StoreTarget', function() {
                var addonParams = setLoad();
                var urlToLoad = "nps/summaryTBC/storeTargetTBC.php?" + addonParams;
                var divToHide = "#StoreTargetloader";
                var divToShow = "#StoreTarget";
                reload(urlToLoad, divToHide, divToShow);
            });
        }

        $('#EpisodeNps', function() {
            var addonParams = setLoad();
            var urlToLoad = "nps/summaryTBC/episode.php?" + addonParams;
            var divToHide = "#EpisodeNpsloader";
            var divToShow = "#EpisodeNps";
            reload(urlToLoad, divToHide, divToShow);
        });

        $('#EpisodeMobileNps', function() {
            var addonParams = setLoad();
            var urlToLoad = "nps/summaryTBC/episodeMobile.php?" + addonParams;
            var divToHide = "#EpisodeMobileNpsloader";
            var divToShow = "#EpisodeMobileNps";
            reload(urlToLoad, divToHide, divToShow);

        });

        $('#FixedNps', function() {
            var addonParams = setLoad();
            var urlToLoad = "nps/summaryTBC/fixedNPS.php?" + addonParams;
            var divToHide = "#FixedNpsloader";
            var divToShow = "#FixedNps";
            reload(urlToLoad, divToHide, divToShow);
        });

        $('#EmployeeNps', function() {
            var addonParams = setLoad();
            var urlToLoad = "nps/summaryTBC/employeeNpsEpisode.php?" + addonParams;
            var divToHide = "#EmployeeNpsloader";
            var divToShow = "#EmployeeNps";
            reload(urlToLoad, divToHide, divToShow);
        });

        $('#EmployeeNpsMobile', function() {
            var addonParams = setLoad();
            var urlToLoad = "nps/summaryTBC/employeeNpsMobile.php?" + addonParams;
            var divToHide = "#EmployeeNpsloaderMobile";
            var divToShow = "#EmployeeNpsMobile";
            reload(urlToLoad, divToHide, divToShow);
        });

        $('#EmployeeNpsFixed', function() {
            var addonParams = setLoad();
            var urlToLoad = "nps/summaryTBC/employeeNpsFixed.php?" + addonParams;
            var divToHide = "#EmployeeNpsloaderFixed";
            var divToShow = "#EmployeeNpsFixed";
            reload(urlToLoad, divToHide, divToShow);
        });

        $('#MergedEmployeeNps', function() {
            var addonParams = setLoad();
            var urlToLoad = "nps/summaryTBC/employeeNpsEpisodeMerged.php?" + addonParams;
            var divToHide = "#MergedEmployeeNpsloader";
            var divToShow = "#MergedEmployeeNps";
            reload(urlToLoad, divToHide, divToShow);
        });

        $('#MergedEmployeeMobileNps', function() {
            var addonParams = setLoad();
            var urlToLoad = "nps/summaryTBC/employeeNpsMobileMerged.php?" + addonParams;
            var divToHide = "#MergedEmployeeNpsMobileloader";
            var divToShow = "#MergedEmployeeMobileNps";
            reload(urlToLoad, divToHide, divToShow);
        });


        $('#MergedEmployeeFixedNps', function() {
            var addonParams = setLoad();
            var urlToLoad = "nps/summaryTBC/employeeNpsFixedMerged.php?" + addonParams;
            var divToHide = "#MergedEmployeeNpsFixedloader";
            var divToShow = "#MergedEmployeeFixedNps";
            reload(urlToLoad, divToHide, divToShow);
        });


        $('#CurrentMonthTier', function() {
            var addonParams = setLoad();
            var urlToLoad = "nps/charts/TierLevel_Bar_TBC.php?" + addonParams;
            var divToHide = "#CurrentMonthTierLoader";
            var divToShow = "#CurrentMonthTier";
            reload(urlToLoad, divToHide, divToShow);
        });

        $('#storetier-group button').click(function() {
            var textval = $(this).data("text");
            var loader = "#CurrentMonthTierLoader";
            var content = "#CurrentMonthTier";
            var params = setLoad();
            var setUrl = "";
            var url = "";

            switch (textval) {
                case 'Interaction':
                    setUrl = "nps/charts/TierLevel_Bar_TBC.php?tiersort=Interaction&" + params;
                    break;
                case 'Mobility (OTG)':
                    setUrl = "nps/charts/TierLevel_Bar_TBC.php?tiersort=Mobile&" + params;
                    break;
                case 'Fixed (Home)':
                    setUrl = "nps/charts/TierLevel_Bar_TBC.php?tiersort=Fixed&" + params;
                    break;
                case 'Episode':
                    setUrl = "nps/charts/TierLevel_Bar_TBC.php?tiersort=Episode&" + params;
                    break;
                default:
                    setUrl = "nps/charts/TierLevel_Bar_TBC.php?tiersort=Episode&" + params;
                    break;
            }
            //console.log(setUrl);
            // $("threemonth").html(textval);
            url = setUrl + params;
            reload(url, loader, content);

        });

        function setLoad() {
            var from = $("#from").val().split("/").reverse().join("-");
            var to = $("#toTBC").val().split("/").reverse().join("-");
            if ($('#ShareType').prop('checked')) {
                shared = 'yes';
            } else {
                shared = 'no';
            }

            if ($('#StoreType').prop('checked')) {
                storeType = '2';
                if ($('#MtdType').prop('checked')) {
                    $('#tbcPicker').hide();
                    $('#mtdPicker').show();
                    mtdSet = '&rangeType=mtd';
                    mtdFrom = $("#mtdFrom").val().split("/").reverse().join("-");
                    mtdTo = $("#mtdTo").val().split("/").reverse().join("-");

                    mtdRange = "&mtdFrom=" + mtdFrom + "&mtdTo=" + mtdTo;

                } else {
                    $('#mtdPicker').hide();
                    $('#tbcPicker').show();
                    mtdSet = '&rangeType=default';
                }
            } else {
                storeType = '1';
            }


            var addonParams = 'shared=' + shared + '&toTBC=' + to + '&from=' + from + '&role=' + role + '&storeType=' + storeType + mtdSet + mtdRange;
            // console.log(addonParams);
            return addonParams;
        }

//        function reload(urlToLoad, divToHide, divToShow) {
//            var urldata = urlToLoad;
//            var page = divToShow;
//            $.get(urldata).success(
//                    function(response, status, jqXhr) {
//                        $(divToHide).hide();
//                        $(page).show();
//                        $(page).empty().append(response);
//                    }).error(function(response, status, jqXhr) {
//                $(divToHide).show();
//                reload(urldata, divToHide, divToShow);
//            }).complete(function(response, status, jqXhr) {
//            });
//        }

        function reload(paramurl, loader, content) {
            $(content).html('');
            $(loader).show();
            $.ajax({
                url: paramurl,
                cache: false,
                success: function(html) {
                    $(content).html('');
                    $(content).append(html);
                },
                complete: function() {
                    $(loader).hide();
                }
            });
        }
    });
</script>
