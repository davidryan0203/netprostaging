<?php
ini_set('max_execution_time', 300);
include($_SERVER['DOCUMENT_ROOT'] . '/functions/cloudrevision.php');
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
date_default_timezone_set("America/Denver");
$sharedType = $_GET['shared'];
$to = mysql_real_escape_string($_GET['to']);
$from = mysql_real_escape_string($_GET['from']);
$role = mysql_real_escape_string($_GET['role']);
$first_day_this_month = date('01/m/Y'); // hard-coded '01' for first day
$last_day_this_month = date('t/m/Y');
$storeType = 3;

if (($to == $last_day_this_month) && ($from == $first_day_this_month)) {
    $dateNow = "Current Month";
} else {
    $dateNow = $from . " - " . $to;
}

if ($sharedType == 'yes') {
    $sharedStatus = "checked=\"checked\" ";
}

if (isset($_GET['reconOwnerID']) && !is_null($_GET['reconOwnerID'])) {
    $ownerReconID = $_GET['reconOwnerID'];
} else {
    $ownerReconID = NULL;
}
//echo $sharedType."ere".$role;
//echo "<br>".$last_day_this_month."-$last_day_this_month <br>".$dateNow."here$to - $from";
?>

<script type="text/javascript">

    $(document).ready(function() {
        $(".toggle:contains('MTD')").attr('id', 'hideMTD').hide();
    });


</script>

<style> .panel-heading .accordion-toggle:after {
        /* symbol for "opening" panels */
        font-family: 'Glyphicons Halflings';  /* essential for enabling glyphicon */
        content: "\e114";    /* adjust as needed, taken from bootstrap.css */

        color: grey;         /* adjust as needed */
    }
    .panel-heading .accordion-toggle.collapsed:after {
        /* symbol for "collapsed" panels */
        content: "\e080";    /* adjust as needed, taken from bootstrap.css */
    }

    button[data-text="Episode"] {
        background-color: #f3bd12  !important;
        border-color: #f3bd12  !important;
    }
    button[data-text="Episode"]:hover:active {
        background-color: #f1aa22 !important;
        border-color: #f1aa22 !important;
    }

</style>

<!--tls nps reports-->
<section class='reports'>

    <div class='filters'>
        <input type="hidden" id="from" name="from" value="<?php echo $from; ?>"/>
        <input type="hidden" id="to" name="to"  value="<?php echo $to; ?>"/>
        <input type="hidden" id="role" name="role" value="<?php echo $role; ?>"/>
        <input type="hidden" id="storeType" name="storeType" value="<?php echo $storeType; ?>" />
        <input type="checkbox" style="display:none" <?php echo $sharedStatus; ?> id="shareType"/>
    </div>

    <div class="panel-group" role="tablist" id='accordion'>

        <!--Store Targets-->
        <div class="panel panel-default"  id="StoreTargetContent">
            <div class="panel-heading" role="tab" id="headingOne">
                <a role="button" data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="accordion-toggle collapsed" id="store3MmaTargetToggle">
                    <h4 class="panel-title">
                        <i class="fa fa-bullseye fa-fw"></i>Store <rolling></rolling> Target
                    </h4>
                </a>

                <div class="btn-group group1 pull-right" id="targetrolling-group" role="group" aria-label="...">
                    <button type="button" id="BtnStoreTarget3mma" data-text="3 Months Moving Average"  data-target="ThreeMonthsAverage" class="btn btn-xs btn-danger StoreTargetBtn">3 MMA</button>
                    <button type="button" id="BtnStoreTargetMtd" data-text="MTD" data-target="CurrentMonth" class="btn btn-xs btn-warning StoreTargetBtn">MTD</button>
                    <button type="button" id="BtnStoreTarget4week" data-text="4 Weeks"  data-target="FourWeekRolling" class="btn btn-xs btn-primary StoreTargetBtn">4 Weeks</button>
                    <!--<button type="button" id="StoreEpiNpsWeekly" data-text="3 Months"  data-target="NinetyDayRolling" class="btn btn-xs btn-info">3 Months</button>-->
                    <button type="button" id="BtnStoreTargetHtd" data-text="HTD"  data-target="HalfYearRolling" class="btn btn-xs btn-success StoreTargetBtn">HTD</button>

                </div>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel">
                <div class="panel-body">
                    <div class="loader" id="StoreTargetloader"></div>
                    <div class="targetcontainer" id="CurrentMonth">
                        <div  id="StoreTargetContainer"></div>
                    </div>
                </div>
            </div>
        </div>

<!--        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <a role="button" data-toggle="collapse" href="#collapseFourTeen" aria-expanded="false" aria-controls="collapseFourTeen" class="accordion-toggle  collapsed" id="h1Toggle">
                    <h4 class="panel-title"><i class="fa fa-archive fa-fw"></i>H1 FY21 BAPPS Score</h4>
                </a>
                                <div class="btn-group group1 pull-right" id="storetier-group" role="group" aria-label="..." >
                                    <button type="button" id="BtnStoreTierMobNps" data-text="Mobility (OTG)"  data-target="storetiermob" class="btn btn-xs btn-warning"  data-tooltip="tooltip" title="Sort Tier by Mobile">Mobility (OTG)</button>
                                    <button type="button" id="BtnStoreTierFixNps" data-text="Fixed (Home)"  data-target="storetierfix" class="btn btn-xs btn-info"  data-tooltip="tooltip" title="Sort Tier by Fixed">Fixed (Home)</button>
                                    <button type="button" id="BtnStoreTierEpiNps" data-text="Episode" data-target="storetierepi" class="btn btn-xs btn-success " data-tooltip="tooltip" title="Sort Tier by Episode">Episode</button>
                                </div>
            </div>
            <div id="collapseFourTeen" class="panel-collapse collapse " role="tabpanel">
                <div class="panel-body">
                    <div class="loader" id='H1Loader'></div>
                    <div id="H1Container"></div>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <a role="button" data-toggle="collapse" href="#collapseSevenTeen" aria-expanded="false" aria-controls="collapseSevenTeen" class="accordion-toggle  collapsed" id="h2Toggle">
                    <h4 class="panel-title"><i class="fa fa-archive fa-fw"></i>H2 FY20 BAPPS Score</h4>
                </a>
                                <div class="btn-group group1 pull-right" id="storetier-group" role="group" aria-label="..." >
                                    <button type="button" id="BtnStoreTierMobNps" data-text="Mobility (OTG)"  data-target="storetiermob" class="btn btn-xs btn-warning"  data-tooltip="tooltip" title="Sort Tier by Mobile">Mobility (OTG)</button>
                                    <button type="button" id="BtnStoreTierFixNps" data-text="Fixed (Home)"  data-target="storetierfix" class="btn btn-xs btn-info"  data-tooltip="tooltip" title="Sort Tier by Fixed">Fixed (Home)</button>
                                    <button type="button" id="BtnStoreTierEpiNps" data-text="Episode" data-target="storetierepi" class="btn btn-xs btn-success " data-tooltip="tooltip" title="Sort Tier by Episode">Episode</button>
                                </div>
            </div>
            <div id="collapseSevenTeen" class="panel-collapse collapse " role="tabpanel">
                <div class="panel-body">
                    <div class="loader" id='H2Loader'></div>
                    <div id="H2Container"></div>
                </div>
            </div>
        </div>-->

        <!--Store Tier Gap-->
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <a role="button" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="accordion-toggle  collapsed" id="store3MmaTierToggle">
                    <h4 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i>Store 3 Months Moving Average <tier></tier> Tiers</h4>
                </a>
                <div class="btn-group group1 pull-right" id="storetier-group" role="group" aria-label="..." >
                    <button type="button" id="BtnStoreTierMobNps" data-text="Mobility (OTG)"  data-target="storetiermob" class="btn btn-xs btn-warning"  data-tooltip="tooltip" title="Sort Tier by Mobile">Mobility (OTG)</button>
                    <!--<button type="button" id="BtnStoreTierFixNps" data-text="Fixed (Home)"  data-target="storetierfix" class="btn btn-xs btn-info"  data-tooltip="tooltip" title="Sort Tier by Fixed">Fixed (Home)</button>-->
                    <button type="button" id="BtnStoreTierEpiNps" data-text="Episode" data-target="storetierepi" class="btn btn-xs btn-success " data-tooltip="tooltip" title="Sort Tier by Episode">Episode</button>
                </div>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse " role="tabpanel">
                <div class="panel-body">
                    <div class="loader" id='StoreNPSTierLoader'></div>
                    <div id="StoreNPSTier"></div>
                </div>
            </div>
        </div>

        <!--Store 3 3MMA NPS-->
        <div class="panel panel-default"  id="3MMAStoreMobile">
            <div class="panel-heading" role="tab" id="headingThirteen">
                <a role="button" data-toggle="collapse" href="#collapseThirteen" aria-expanded="true" aria-controls="collapseThirteen" class="accordion-toggle collapsed" id="store3MmaNpsToggle" >
                    <h4 class="panel-title"><i class="fa fa-calendar fa-fw"></i>Store 3 Months Moving Average <mma3></mma3> NPS</h4>
                </a>
                <div class="btn-group group1 pull-right" id="3MMA-group" role="group" aria-label="...">
                    <!--<button type="button" id="StoreIntNpsWeekly" data-text="Interaction" data-target="storeweeklyint" class="btn btn-xs btn-primary ">Interaction</button>-->
                    <button type="button" id="BtnStoreMobNps3MMA" data-text="Mobility (OTG)"  data-target="store3mmamob" class="btn btn-xs btn-warning">Mobility (OTG)</button>
                    <button type="button" id="BtnStoreFixNps3MMA" data-text="Fixed (Home)"  data-target="store3mmafix" class="btn btn-xs btn-info ">Fixed (Home)</button>
                    <button type="button" id="BtnStoreEpiNps3MMA" data-text="Episode"  data-target="storeweeklyepi" class="btn btn-xs btn-success">Episode</button>
                </div>
            </div>
            <div id="collapseThirteen" class="panel-collapse collapse " role="tabpanel">
                <div class="panel-body">
                    <div class="loader" id="StoreNps3MMAloader"></div>
                    <div class="Monthcontainer" id="store3MMAint">
                        <div class="table table-responsive" id="StoreNps3MMAContainer"></div>
                    </div>
                </div>
            </div>
        </div>

        <!--Store 4 Weeks NPS-->
        <div class="panel panel-default"  id="WeeklyEmployeeInteraction">
            <div class="panel-heading" role="tab" id="headingFour">
                <a role="button" data-toggle="collapse" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour" class="accordion-toggle collapsed" id="storeWeeklyToggle">
                    <h4 class="panel-title"><i class="fa fa-calendar fa-fw"></i>Store 4 Weeks <storeweekly></storeweekly> NPS</h4>
                </a>
                <div class="btn-group group1 pull-right" id="storeweekly-group" role="group" aria-label="...">
                    <!--<button type="button" id="StoreIntNpsWeekly" data-text="Interaction" data-target="storeweeklyint" class="btn btn-xs btn-primary ">Interaction</button>-->
                    <button type="button" id="BtnStoreMobNpsWeekly" data-text="Mobility (OTG)"  data-target="storeweeklymob" class="btn btn-xs btn-warning ">Mobility (OTG)</button>
                    <button type="button" id="BtnStoreFixNpsWeekly" data-text="Fixed (Home)"  data-target="storeweeklyfix" class="btn btn-xs btn-info ">Fixed (Home)</button>
                    <button type="button" id="BtnStoreEpiNpsWeekly" data-text="Episode"  data-target="storeweeklyepi" class="btn btn-xs btn-success">Episode</button>
                </div>
            </div>
            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel">
                <div class="panel-body">
                    <div class="loader" id="StoreNpsWeeklyloader"></div>
                    <div class="storeweeklycontainer" id="storeweeklyint">
                        <div class="table table-responsive" id="StoreNpsWeeklyContainer"></div>
                    </div>
                </div>
            </div>
        </div>

        <!--Store 3 Month NPS-->
        <!--        <div class="panel panel-default"  id="3MonthStoreMobile">
                    <div class="panel-heading" role="tab" id="headingFive">
                        <a role="button" data-toggle="collapse" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive" class="accordion-toggle collapsed" id="nps3months" >
                            <h4 class="panel-title"><i class="fa fa-calendar fa-fw"></i>Store 3 Months <month3>Mobility (OTG)</month3> NPS</h4>
                        </a>
                        <div class="btn-group group1 pull-right" id="3Month-group" role="group" aria-label="...">
                            <button type="button" id="StoreIntNpsWeekly" data-text="Interaction" data-target="storeweeklyint" class="btn btn-xs btn-primary ">Interaction</button>
                            <button type="button" id="StoreMobNps3Month" data-text="Mobility (OTG)"  data-target="store3monthmob" class="btn btn-xs btn-warning">Mobility (OTG)</button>
                            <button type="button" id="StoreFixNps3Month" data-text="Fixed (Home)"  data-target="store3monthfix" class="btn btn-xs btn-info">Fixed (Home)</button>
                            <button type="button" id="StoreEpiNpsWeekly" data-text="Episode"  data-target="storeweeklyepi" class="btn btn-xs btn-success">Episode</button>
                        </div>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body">
                            <div class="loader" id="StoreNps3Monthloader"></div>
                            <div class="Monthcontainer" id="store3Monthint">
                                <div class="table table-responsive" id="StoreNps3MonthContainer"></div>
                            </div>
                        </div>
                    </div>
                </div>-->

        <!--Store MTD NPS-->
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
                <a role="button" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" class="accordion-toggle collapsed" id="storeMtdToggle">
                    <h4 class="panel-title"><i class="fa fa-calendar fa-fw"></i>Store MTD <store></store> NPS</h4>
                </a>
                <div class="btn-group group1 pull-right" id="store-group" role="group" aria-label="...">
                    <!--<button type="button" id="StoreIntNps" data-text="Interaction" data-target="storeint" class="btn btn-xs btn-primary ">Interaction</button>-->
                    <button type="button" id="BtnStoreMtdMobNps" data-text="Mobility (OTG)"  data-target="storemob" class="btn btn-xs btn-warning ">Mobility (OTG)</button>
                    <button type="button" id="BtnStoreMtdFixNps" data-text="Fixed (Home)"  data-target="storefix" class="btn btn-xs btn-info">Fixed (Home)</button>
                    <button type="button" id="BtnStoreMtdEpiNps" data-text="Episode"  data-target="storeepi" class="btn btn-xs btn-success">Episode</button>

                </div>
            </div>

            <div id="collapseThree" class="panel-collapse collapse " role="tabpanel">
                <div class="panel-body">
                    <div class="loader" id="StoreMtdNpsloader"></div>
                    <div class="storecontainer" id="storeint">
                        <div class="table table-responsive" id="StoreMtdNpsContainer"></div>
                    </div>
                </div>
            </div>
        </div>

        <!--Store HTD NPS-->
        <div class="panel panel-default"  id="WeeklyEmployeeInteraction">
            <div class="panel-heading" role="tab" id="headingSix">
                <a role="button" data-toggle="collapse" href="#collapseSix" aria-expanded="true" aria-controls="collapseSix" class="accordion-toggle collapsed" id="storeNPSHtdToggle">
                    <h4 class="panel-title"><i class="fa fa-calendar fa-fw"></i>Store HTD <storehtd></storehtd> NPS</h4>
                </a>
                <div class="btn-group group1 pull-right" id="storehtd-group" role="group" aria-label="...">
                    <!--<button type="button" id="StoreIntNpsHTD" data-text="Interaction" data-target="storehtdlyint" class="btn btn-xs btn-primary ">Interaction</button>-->
                    <button type="button" id="BtnStoreHtdMobNps" data-text="Mobility (OTG)"  data-target="storehtdlymob" class="btn btn-xs btn-warning">Mobility (OTG)</button>
                    <button type="button" id="BtnStoreHtdFixNps" data-text="Fixed (Home)"  data-target="storehtdlyfix" class="btn btn-xs btn-info ">Fixed (Home)</button>
                    <button type="button" id="BtnStoreHtdEpiNps" data-text="Episode"  data-target="storehtdlyepi" class="btn btn-xs btn-success">Episode</button>
                </div>
            </div>
            <div id="collapseSix" class="panel-collapse collapse" role="tabpanel">
                <div class="panel-body">
                    <div class="loader" id="StoreNpsHTDloader"></div>
                    <div class="storehtdcontainer" id="storehtdint">
                        <div class="table table-responsive" id="StoreNpsHTDContainer"></div>
                    </div>
                </div>
            </div>
        </div>

        <!--Employee Combined MTD NPS-->
        <div class="panel panel-default"  id="EmployeeCombined">
            <div class="panel-heading" role="tab" id="headingSixteen">
                <a role="button" data-toggle="collapse" href="#collapseSixteen" aria-expanded="falkse" aria-controls="collapseSixteen" class="accordion-toggle collapsed" id="combinedEmpNpsToggle">
                    <h4 class="panel-title"><i class="fa fa-users fa-fw"></i> Employee Combined Mobility (OTG) & Fixed (Home) NPS</h4>
                </a>
            </div>
            <div id="collapseSixteen" class="panel-collapse collapse " role="tabpanel">
                <div class="panel-body">
                    <div class="loader" id="EmployeeCombinedNpsLoader"></div>

                    <div class="empnpscontainer" id="empnpsint" >
                        <div class="table table-responsive" id="EmployeeCombinedNpsContainer"></div>
                    </div>
                </div>
            </div>
        </div>

        <!--Employee 3 months NPS -->
        <!--<div class="panel panel-default" id="3MonthsEmployeeInteraction">
                    <div class="panel-heading" role="tab" id="headingNine">
                        <a role="button" data-toggle="collapse" href="#collapseNine" aria-expanded="true" aria-controls="collapseNine" class="accordion-toggle collapsed" id="emp3months">
                            <h4 class="panel-title"><i class="fa fa-user fa-fw"></i>Employee 3 Months <threemonth>Mobility (OTG)</threemonth> NPS</h4>
                        </a>
                        <div class="btn-group group1 pull-right" id="3mnth-group" role="group" aria-label="...">
                            <button type="button" id="WeekRollingInteraction" data-text="Interaction" data-target="weeklyint" class="btn btn-xs btn-primary ">Interaction</button>
                            <button type="button" id="WeekRollingMobile" data-text="Mobility (OTG)"  data-target="3mnthmob" class="btn btn-xs btn-warning">Mobility (OTG)</button>
                            <button type="button" id="WeekRollingFix" data-text="Fixed (Home)"  data-target="3mnthfix" class="btn btn-xs btn-info">Fixed (Home)</button>
                            <button type="button" id="WeekRollingEpisode" data-text="Episode"  data-target="weeklyepi" class="btn btn-xs btn-success">Episode</button>
                        </div>
                    </div>
                    <div id="collapseNine" class="panel-collapse collapse" role="tabpanel">
                        <div class="panel-body">
                            <div class="loader" id="3MonthsEmployeeNpsloader"></div>
                            <div class="mnth3container" id="weeklyint">
                                <div class="table table-responsive" id="3MonthsEmployeeNpsContainer"></div>
                            </div>
                        </div>
                    </div>
                </div>-->

        <!--Employee 3 months average NPS-->
        <div class="panel panel-default" id="3MMaEmployeeInteraction">
            <div class="panel-heading" role="tab" id="headingforthteen">
                <a role="button" data-toggle="collapse" href="#collapseFourteen" aria-expanded="false" aria-controls="collapseFourteen" class="accordion-toggle collapsed" id="emp3mmatoggle">
                    <h4 class="panel-title"><i class="fa fa-user fa-fw"></i>Employee 3 Months Moving Average <threemma></threemma> NPS</h4>
                </a>
                <div class="btn-group group1 pull-right" id="emp-3mma-group" role="group" aria-label="...">
                    <!--<button type="button" id="WeekRollingInteraction" data-text="Interaction" data-target="weeklyint" class="btn btn-xs btn-primary ">Interaction</button>-->
                    <button type="button" id="BtnEmpNps3mmaMobile" data-text="Mobility (OTG)"  data-target="3mmamob" class="btn btn-xs btn-warning">Mobility (OTG)</button>
                    <button type="button" id="BtnEmpNps3mmaFix" data-text="Fixed (Home)"  data-target="3mmafix" class="btn btn-xs btn-info ">Fixed (Home)</button>
                    <button type="button" id="BtnEmpNps3mmaEpi" data-text="Episode"  data-target="weeklyepi" class="btn btn-xs btn-success">Episode</button>
                </div>
            </div>
            <div id="collapseFourteen" class="panel-collapse collapse " role="tabpanel">
                <div class="panel-body">
                    <div class="loader" id="3mmaEmployeeNpsloader"></div>
                    <div class="emp3mmacontainer" id="weeklyint">
                        <div class="table table-responsive" id="3MMAEmployeeNpsContainer"></div>
                    </div>
                </div>
            </div>
        </div>

        <!--Employee MTD NPS-->
        <div class="panel panel-default"  id="EmployeeInteraction">
            <div class="panel-heading" role="tab" id="headingSeven">
                <a role="button" data-toggle="collapse" href="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven" class="accordion-toggle collapsed"  id="empMtdToggle">
                    <h4 class="panel-title"><i class="fa fa-users fa-fw"></i> Employee MTD <empnps></empnps> NPS</h4>
                </a>
                <div class="btn-group group2 pull-right" id="empnps-group" role="group" aria-label="...">
                    <!--<button type="button" id="DefaultEmpInteraction" data-text="Interaction" data-target="empnpsint" class="btn btn-xs btn-primary ">Interaction</button>-->
                    <button type="button" id="BtnEmpMtdNpsMob" data-text="Mobility (OTG)"  data-target="empnpsmob" class="btn btn-xs btn-warning">Mobility (OTG)</button>
                    <button type="button" id="BtnEmpMtdNpsFix" data-text="Fixed (Home)"  data-target="empnpsfix" class="btn btn-xs btn-info ">Fixed (Home)</button>
                    <button type="button" id="BtnEmpMtdNpsEpi" data-text="Episode"  data-target="empnpsepi" class="btn btn-xs btn-success">Episode</button>
                </div>
            </div>
            <div id="collapseSeven" class="panel-collapse collapse " role="tabpanel">
                <div class="panel-body">
                    <div class="loader" id="EmployeeMtdNpsloader"></div>

                    <div class="empnpscontainer" id="empnpsint" >
                        <div class="table table-responsive" id="EmployeeMtdNpsContainer"></div>
                    </div>
                </div>
            </div>
        </div>

        <!--Employee Four Weeks NPS-->
        <div class="panel panel-default" id="WeeklyEmployeeInteraction">
            <div class="panel-heading" role="tab" id="headingEight">
                <a role="button" data-toggle="collapse" href="#collapseEight" aria-expanded="true" aria-controls="collapseEight" class="accordion-toggle collapsed" id="emp4WeekToggle">
                    <h4 class="panel-title"><i class="fa fa-user fa-fw"></i>Employee 4 Weeks <weekly></weekly> NPS</h4>
                </a>
                <div class="btn-group group1 pull-right" id="weekly-group" role="group" aria-label="...">
                    <!--<button type="button" id="WeekRollingInteraction" data-text="Interaction" data-target="weeklyint" class="btn btn-xs btn-primary ">Interaction</button>-->
                    <button type="button" id="BtnWeekRollingMobile" data-text="Mobility (OTG)"  data-target="weeklymob" class="btn btn-xs btn-warning">Mobility (OTG)</button>
                    <button type="button" id="BtnWeekRollingFix" data-text="Fixed (Home)"  data-target="weeklyfix" class="btn btn-xs btn-info">Fixed (Home)</button>
                    <button type="button" id="BtnWeekRollingEpisode" data-text="Episode"  data-target="weeklyepi" class="btn btn-xs btn-success">Episode</button>
                </div>
            </div>
            <div id="collapseEight" class="panel-collapse collapse" role="tabpanel">
                <div class="panel-body">
                    <div class="loader" id="WeeklyEmployeeNpsloader"></div>
                    <div class="weeklycontainer" id="weeklyint">
                        <div class="table table-responsive" id="WeeklyEmployeeNpsContainer"></div>
                    </div>
                </div>
            </div>
        </div>

        <!--Employee HTD NPS-->
        <div class="panel panel-default"  id="EmployeeInteraction">
            <div class="panel-heading" role="tab" id="headingTen">
                <a role="button" data-toggle="collapse" href="#collapseTen" aria-expanded="true" aria-controls="collapseTen" class="accordion-toggle collapsed" id="empHTDToggle">
                    <h4 class="panel-title"><i class="fa fa-users fa-fw"></i> Employee HTD <emphtdnps></emphtdnps> NPS</h4>
                </a>
                <div class="btn-group group2 pull-right" id="emphtdnps-group" role="group" aria-label="...">
                    <!--<button type="button" id="DefaultEmpInteraction" data-text="Interaction" data-target="empnpsint" class="btn btn-xs btn-primary ">Interaction</button>-->
                    <button type="button" id="BtnEmpNpsHtdMob" data-text="Mobility (OTG)"  data-target="empnpsmon" class="btn btn-xs btn-warning">Mobility (OTG)</button>
                    <button type="button" id="BtnEmpNpsHtdFix" data-text="Fixed (Home)"  data-target="empnpsfix" class="btn btn-xs btn-info ">Fixed (Home)</button>
                    <button type="button" id="BtnEmpNpsHtdEpi" data-text="Episode"  data-target="empnpsepi" class="btn btn-xs btn-success">Episode</button>
                </div>
            </div>
            <div id="collapseTen" class="panel-collapse collapse" role="tabpanel">
                <div class="panel-body">
                    <div class="loader" id="EmployeeHtdNpsloader"></div>

                    <div class="emphtdnpscontainer" id="empnpsint" >
                        <div class="table table-responsive" id="EmployeeHtdNpsContainer"></div>
                    </div>
                </div>
            </div>
        </div>

        <!--Employee 3 MMA Merged NPS-->
        <div class="panel panel-default" id="FourWeekEmployeeMergedInteraction">
            <div class="panel-heading" role="tab" id="headingFifteen">
                <a role="button" data-toggle="collapse" href="#collapseFifteen" aria-expanded="true" aria-controls="collapseFifteen" class="accordion-toggle collapsed" id="3mmaEmpmergeToggle">
                    <h4 class="panel-title"><i class="fa fa-newspaper-o fa-fw"></i>Employee with Multiple Stores Merged 3 Months Moving Average <mma3merge></mma3merge> NPS</h4>
                </a>
                <div class="btn-group group2 pull-right" id="3mmamerge-group" role="group" aria-label="...">

                    <button type="button" id="Btn3mmaEmpMergeMob" data-text="Mobility (OTG)"  data-target="fourweekmergemob" class="btn btn-xs btn-warning">Mobility (OTG)</button>
                    <button type="button" id="Btn3mmaEmpMergeFix" data-text="Fixed (Home)"  data-target="fourweekmergefix" class="btn btn-xs btn-info ">Fixed (Home)</button>
                    <button type="button" id="Btn3mmaEmpMergeEpi" data-text="Episode"  data-target="fourweekmergefix" class="btn btn-xs btn-success ">Episode</button>
                </div>
            </div>
            <div id="collapseFifteen" class="panel-collapse collapse " role="tabpanel">
                <div class="panel-body">
                    <div class="loader" id="3MMAMergeEmployeeNpsloader"></div>
                    <div class="weekmergecontainer" id="4mmamergeint">
                        <div class="table table-responsive" id="3MMAMergeEmployeeNpsContainer"></div>
                    </div>
                </div>
            </div>
        </div>

        <!--Employee MTD Merged NPS-->
        <div class="panel panel-default" id="EmployeeMergedInteraction">
            <div class="panel-heading" role="tab" id="headingEleven">
                <a role="button" data-toggle="collapse" href="#collapseEleven" aria-expanded="true" aria-controls="collapseEleven" class="accordion-toggle collapsed" id="mtdEmpmergeToggle">
                    <h4 class="panel-title"><i class="fa fa-calculator fa-fw"></i>Employee with Multiple Stores Merged MTD <merge></merge> NPS</h4>
                </a>
                <div class="btn-group group2 pull-right" id="merge-group" role="group" aria-label="...">
                    <!--<button type="button" id="MergeInteraction" data-text="Interaction" data-target="mergeint" class="btn btn-xs btn-primary ">Interaction</button>-->
                    <button type="button" id="BtnMtdEmpMergeMob" data-text="Mobility (OTG)"  data-target="mergemob" class="btn btn-xs btn-warning">Mobility (OTG)</button>
                    <button type="button" id="BtnMtdEmpMergeFix" data-text="Fixed (Home)"  data-target="mergefix" class="btn btn-xs btn-info ">Fixed (Home)</button>
                    <button type="button" id="BtnMtdEmpMergeEpi" data-text="Episode"  data-target="mergeepi" class="btn btn-xs btn-success">Episode</button>
                </div>
            </div>
            <div id="collapseEleven" class="panel-collapse collapse " role="tabpanel">
                <div class="panel-body">
                    <div class="loader" id="MergedMtdEmployeeNpsloader"></div>
                    <div class="mergecontainer" id="mergeint">
                        <div class="table table-responsive" id="MergedMtdEmployeeNpsContainer"></div>
                    </div>
                </div>
            </div>
        </div>

        <!--Employee 4 weeks Merged NPS-->
        <div class="panel panel-default" id="FourWeekEmployeeMergedInteraction">
            <div class="panel-heading" role="tab" id="headingTwelve">
                <a role="button" data-toggle="collapse" href="#collapseTwelve" aria-expanded="true" aria-controls="collapseTwelve" class="accordion-toggle collapsed" id="weekEmpmergeToggle">
                    <h4 class="panel-title"><i class="fa fa-newspaper-o fa-fw"></i>Employee with Multiple Stores Merged 4 Weeks <weekmerge></weekmerge> NPS</h4>
                </a>
                <div class="btn-group group2 pull-right" id="weekmerge-group" role="group" aria-label="...">
                    <!--<button type="button" id="FourWeekMergeInteraction" data-text="Interaction" data-target="fourweekmergeint" class="btn btn-xs btn-primary ">Interaction</button>-->
                    <button type="button" id="BtnFourWeekMergeMob" data-text="Mobility (OTG)"  data-target="fourweekmergemob" class="btn btn-xs btn-warning">Mobility (OTG)</button>
                    <button type="button" id="BtnFourWeekMergeFix" data-text="Fixed (Home)"  data-target="fourweekmergefix" class="btn btn-xs btn-info ">Fixed (Home)</button>
                    <button type="button" id="BtnFourWeekMergeEpi" data-text="Episode"  data-target="fourweekmergeepi" class="btn btn-xs btn-success">Episode</button>
                </div>
            </div>
            <div id="collapseTwelve" class="panel-collapse collapse " role="tabpanel">
                <div class="panel-body">
                    <div class="loader" id="FourWeekMergedEmployeeNpsloader"></div>
                    <div class="weekmergecontainer" id="fourweekmergeint">
                        <div class="table table-responsive" id="FourWeekMergedEmployeeNpsContainer"></div>
                    </div>
                </div>
            </div>
        </div>



    </div>
</section>

<script src="../includes/js/custom.js" type="text/javascript"></script>

<script type="text/javascript">

    $(document).ready(function() {
        //console.log('others')
        //Parameter Variables
        var from = $("#from").val().split("/").reverse().join("-");
        var to = $("#to").val().split("/").reverse().join("-");
        var role = $("#role").val();
        var shared = "";
        var storeType = "3";

        //Hide loader and load contents function
        function loadContents(paramurl, loader, content) {
            $(content).html('');
            $(loader).show();
            $.ajax({
                url: paramurl,
                cache: false,
                success: function(html) {
                    $(content).html('');
                    $(content).append(html);
                },
                complete: function() {
                    $(loader).hide();
                }
            });
        }

        //Define parameters for each reports function
        function setParams() {
            var from = $("#from").val().split("/").reverse().join("-");
            var to = $("#to").val().split("/").reverse().join("-");
            if ($('#ShareType').prop('checked')) {
                shared = 'yes';
            } else {
                shared = 'no';
            }



            var reconOwnerID = $("#reconOwnerID").val();
            var params = 'shared=' + shared + '&to=' + to + '&from=' + from + '&role=' + role + '&storeType=' + storeType + '&reconOwnerID=' + reconOwnerID;
            return params;
        }

        //Check ShareType Value
        if ($('#shareType').prop('checked')) {
            //Hide following reports if shared is checked
            $('#WeeklyEmployeeInteraction,#WeeklyEmployeeEpisode,#StoreTargetContent,#MergedEmployeeNpsContent,#EmployeeInteraction,#EmployeeMergedInteraction,#EmployeeMergedEpisode').hide();
        } else {
            //Show only two reports for shared view
            /*  $('#StoreTargetContainer', function() {
             var params = setParams();
             var url = "nps/summaryOthers/storeTargetMma.php?3mma=on&" + params;
             var hide = "#StoreTargetloader";
             var show = "#StoreTargetContainer";
             loadContents(url, hide, show);
             }); */
        }


        $('#targetrolling-group button').click(function() {
            var textval = $(this).data("text");
            var loader = "#StoreTargetloader";
            var content = "#StoreTargetContainer";
            var params = setParams();
            var setUrl = "";
            var url = "";

            switch (textval) {
                case 'MTD':
                    setUrl = "nps/summaryOthers/storeTargetMma.php?";
                    break;
                case '4 Weeks':
                    setUrl = "nps/summaryOthers/storeTargetMma.php?weekly=on&";
                    break;
                case '3 Months':
                    setUrl = "nps/summaryOthers/storeTargetMma.php?3months=on&";
                    break;
                case 'HTD':
                    setUrl = "nps/summaryOthers/storeTargetMma.php?htd=on&";
                    break;
                case '3 Months Moving Average':
                    setUrl = "nps/summaryOthers/storeTargetMma.php?3mma=on&";
                    break;
            }

            $("#targetrolling-group button").removeClass("active");
            $(this).addClass("active");
            var isCollapsed = $("#store3MmaTargetToggle").hasClass("collapsed");
            $("#store3MmaTargetToggle").addClass('loaded');
            if (isCollapsed) {
                $("#store3MmaTargetToggle").click();
            }


            $("rolling").html(textval);
            url = setUrl + params;
            loadContents(url, loader, content);

        });
        $('#store3MmaTargetToggle').click(function() {
            /*   var isLoaded = $(this).hasClass("loaded");
             var textval = $('#targetrolling-group button').data("text");
             var loader = "#StoreTargetloader";
             var content = "#StoreTargetContainer";
             var params = setParams();
             var setUrl = "";
             var url = "";

             if (!isLoaded) {
             setUrl = "nps/summaryOthers/storeTargetMma.php?3mma=on&";
             $("#targetrolling-group button").removeClass("active");
             textval = $("#BtnStoreTarget3mma").data("text");
             $("#BtnStoreTarget3mma").addClass("active");
             $("rolling").html(textval);
             url = setUrl + params;
             loadContents(url, loader, content);
             $(this).addClass("loaded");
             } */
            var isLoaded = $(this).hasClass("loaded");
            if (!isLoaded) {
                $("#BtnStoreTarget3mma").trigger("click");
            }
        });

        $('#h1Toggle').click(function() {
            var isLoaded = $(this).hasClass("loaded");

            var loader = "#H1Loader";
            var content = "#H1Container";
            var params = setParams();
            var setUrl = "";
            var url = "";

            if (!isLoaded) {
                setUrl = "nps/summaryOthers/storeTargetHtdH1.php?quarter=1&";
                url = setUrl + params;
                loadContents(url, loader, content);
                $(this).addClass("loaded");
            }
            var isLoaded = $(this).hasClass("loaded");
//            if (!isLoaded) {
//                $("#BtnStoreTarget3mma").trigger("click");
//            }
        });

        $('#h2Toggle').click(function() {
            var isLoaded = $(this).hasClass("loaded");

            var loader = "#H2Loader";
            var content = "#H2Container";
            var params = setParams();
            var setUrl = "";
            var url = "";

            if (!isLoaded) {
                setUrl = "nps/summaryOthers/storeTargetHtdQ3.php?&quarter=3&";
                url = setUrl + params;
                loadContents(url, loader, content);
                $(this).addClass("loaded");
            }
            var isLoaded = $(this).hasClass("loaded");
//            if (!isLoaded) {
//                $("#BtnStoreTarget3mma").trigger("click");
//            }
        });

        $('#storetier-group button').click(function() {
            var textval = $(this).data("text");
            var loader = "#StoreNPSTierLoader";
            var content = "#StoreNPSTier";
            var params = setParams();
            var setUrl = "";
            var url = "";

            switch (textval) {
                case 'Interaction':
                    setUrl = "nps/charts/TierLevel_Bar_Others.php?tiersort=Interaction&3mma=on&" + params;
                    break;
                case 'Mobility (OTG)':
                    setUrl = "nps/charts/TierLevel_Bar_Others.php?tiersort=Mobile&3mma=on&" + params;
                    break;
                case 'Fixed (Home)':
                    setUrl = "nps/charts/TierLevel_Bar_Others.php?tiersort=Fixed&3mma=on&" + params;
                    break;
                case 'Episode':
                    setUrl = "nps/charts/TierLevel_Bar_Others.php?tiersort=Episode&3mma=on&" + params;
                    break;
            }

            $("#storetier-group button").removeClass("active");
            $(this).addClass("active");
            var isCollapsed = $("#store3MmaTierToggle").hasClass("collapsed");
            $("#store3MmaTierToggle").addClass('loaded');
            if (isCollapsed) {
                $("#store3MmaTierToggle").click();
            }

            //console.log(setUrl)
            $("tier").html(textval);
            url = setUrl + params;
            loadContents(url, loader, content);

        });
        $('#store3MmaTierToggle').click(function() {
            /*
             var isLoaded = $(this).hasClass("loaded");
             var textval = $('#storetier-group button').data("text");
             var loader = "#StoreNPSTierLoader";
             var content = "#StoreNPSTier";
             var params = setParams();
             var setUrl = "";
             var url = "";

             if (!isLoaded) {
             setUrl = "nps/charts/TierLevel_Bar.php?tiersort=Fixed&3mma=on&" + params;
             textval = $("#BtnStoreTierFixNps").data("text");
             $("#BtnStoreTierFixNps").addClass("active");
             $("tier").html(textval);
             url = setUrl + params;
             loadContents(url, loader, content);
             $(this).addClass("loaded");
             }
             */
            var isLoaded = $(this).hasClass("loaded");
            if (!isLoaded) {
                $("#BtnStoreTierFixNps").trigger("click");
            }

        });

        $('#3MMA-group button').click(function() {
            var textval = $(this).data("text");
            var loader = "#StoreNps3MMAloader";
            var content = "#StoreNps3MMAContainer";
            var params = setParams();
            var setUrl = "";
            var url = "";

            switch (textval) {
                case 'Mobility (OTG)':
                    setUrl = "nps/summaryOthers/Mobile.php?3mma=on&";
                    break;
                case 'Fixed (Home)':
                    setUrl = "nps/summaryOthers/Fixed.php?3mma=on&";
                    break;
                case 'Episode':
                    setUrl = "nps/summaryOthers/episode.php?3mma=on&";
                    break;
            }

            $("#3MMA-group button").removeClass("active");
            $(this).addClass("active");
            var isCollapsed = $("#store3MmaNpsToggle").hasClass("collapsed");
            $("#store3MmaNpsToggle").addClass('loaded');
            if (isCollapsed) {
                $("#store3MmaNpsToggle").click();
            }

            $("mma3").html(textval);
            url = setUrl + params;
            loadContents(url, loader, content);

        });
        $('#store3MmaNpsToggle').click(function() {
            /*
             var isLoaded = $(this).hasClass("loaded");
             var textval = $('#3MMA-group button').data("text");
             var loader = "#StoreNps3MMAloader";
             var content = "#StoreNps3MMAContainer";
             var params = setParams();
             var setUrl = "";
             var url = "";

             if (!isLoaded) {
             setUrl = "nps/summaryOthers/Fixed.php?3mma=on&";
             textval = $("#BtnStoreFixNps3MMA").data("text");
             $("#BtnStoreFixNps3MMA").addClass("active");
             $("mma3").html(textval);
             url = setUrl + params;
             loadContents(url, loader, content);
             $(this).addClass("loaded");
             }
             */
            var isLoaded = $(this).hasClass("loaded");
            if (!isLoaded) {
                $("#BtnStoreFixNps3MMA").trigger("click");
            }


        });

        $('#storeweekly-group button').click(function() {
            var textval = $(this).data("text");
            var loader = "#StoreNpsWeeklyloader";
            var content = "#StoreNpsWeeklyContainer";
            var params = setParams();
            var setUrl = "";
            var url = "";

            switch (textval) {
                case 'Interaction':
                    setUrl = "nps/summaryOthers/Interaction.php?weekly=on&";
                    break;
                case 'Mobility (OTG)':
                    setUrl = "nps/summaryOthers/Mobile.php?weekly=on&";
                    break;
                case 'Fixed (Home)':
                    setUrl = "nps/summaryOthers/Fixed.php?weekly=on&";
                    break;
                case 'Episode':
                    setUrl = "nps/summaryOthers/episode.php?weekly=on&";
                    break;
            }

            $("#storeweekly-group button").removeClass("active");
            $(this).addClass("active");
            var isCollapsed = $("#storeWeeklyToggle").hasClass("collapsed");
            $("#storeWeeklyToggle").addClass('loaded');
            if (isCollapsed) {
                $("#storeWeeklyToggle").click();
            }

            $("storeweekly").html(textval);
            url = setUrl + params;
            loadContents(url, loader, content);

        });
        $('#storeWeeklyToggle').click(function() {
            /*
             var isLoaded = $(this).hasClass("loaded");
             var textval = $('#storeweekly-group button').data("text");
             var loader = "#StoreNpsWeeklyloader";
             var content = "#StoreNpsWeeklyContainer";
             var params = setParams();
             var setUrl = "";
             var url = "";

             if (!isLoaded) {
             setUrl = "nps/summaryOthers/Fixed.php?weekly=on&";
             textval = $("#BtnStoreFixNpsWeekly").data("text");
             $("#BtnStoreFixNpsWeekly").addClass("active");
             $("storeweekly").html(textval);
             url = setUrl + params;
             loadContents(url, loader, content);
             $(this).addClass("loaded");
             }
             */
            var isLoaded = $(this).hasClass("loaded");
            if (!isLoaded) {
                $("#BtnStoreFixNpsWeekly").trigger("click");
            }


        });

        $('#store-group button').click(function() {
            var textval = $(this).data("text");
            var loader = "#StoreMtdNpsloader";
            var content = "#StoreMtdNpsContainer";
            var params = setParams();
            var setUrl = "";
            var url = "";

            switch (textval) {
                case 'Interaction':
                    setUrl = "nps/summaryOthers/Interaction.php?";
                    break;
                case 'Mobility (OTG)':
                    setUrl = "nps/summaryOthers/Mobile.php?";
                    break;
                case 'Fixed (Home)':
                    setUrl = "nps/summaryOthers/Fixed.php?";
                    break;
                case 'Episode':
                    setUrl = "nps/summaryOthers/episode.php?";
                    break;
            }

            $("#store-group button").removeClass("active");
            $(this).addClass("active");
            var isCollapsed = $("#storeMtdToggle").hasClass("collapsed");
            $("#storeMtdToggle").addClass('loaded');
            if (isCollapsed) {
                $("#storeMtdToggle").click();
            }

            $("store").html(textval);
            url = setUrl + params;
            loadContents(url, loader, content);

        });
        $('#storeMtdToggle').click(function() {
            /*
             var isLoaded = $(this).hasClass("loaded");
             var textval = $('#store-group button').data("text");
             var loader = "#StoreMtdNpsloader";
             var content = "#StoreMtdNpsContainer";
             var params = setParams();
             var setUrl = "";
             var url = "";

             if (!isLoaded) {
             setUrl = "nps/summaryOthers/Fixed.php?";
             textval = $("#BtnStoreMtdFixNps").data("text");
             $("#BtnStoreMtdFixNps").addClass("active");
             $("store").html(textval);
             url = setUrl + params;
             loadContents(url, loader, content);
             $(this).addClass("loaded");
             }
             */
            var isLoaded = $(this).hasClass("loaded");
            if (!isLoaded) {
                $("#BtnStoreMtdFixNps").trigger("click");
            }


        });

        $('#storehtd-group button').click(function() {
            var textval = $(this).data("text");
            var loader = "#StoreNpsHTDloader";
            var content = "#StoreNpsHTDContainer";
            var params = setParams();
            var setUrl = "";
            var url = "";

            switch (textval) {
                case 'Interaction':
                    setUrl = "nps/summaryOthers/Interaction.php?htd=on&";
                    break;
                case 'Mobility (OTG)':
                    setUrl = "nps/summaryOthers/Mobile.php?htd=on&";
                    break;
                case 'Fixed (Home)':
                    setUrl = "nps/summaryOthers/Fixed.php?htd=on&";
                    break;
                case 'Episode':
                    setUrl = "nps/summaryOthers/episode.php?htd=on&";
                    break;
            }
            $("#storehtd-group button").removeClass("active");
            $(this).addClass("active");
            var isCollapsed = $("#storeNPSHtdToggle").hasClass("collapsed");
            $("#storeNPSHtdToggle").addClass('loaded');
            if (isCollapsed) {
                $("#storeNPSHtdToggle").click();
            }

            $("storehtd").html(textval);
            url = setUrl + params;
            loadContents(url, loader, content);

        });
        $('#storeNPSHtdToggle').click(function() {
            /*
             var isLoaded = $(this).hasClass("loaded");
             var textval = $('#storehtd-group button').data("text");
             var loader = "#StoreNpsHTDloader";
             var content = "#StoreNpsHTDContainer";
             var params = setParams();
             var setUrl = "";
             var url = "";

             if (!isLoaded) {
             setUrl = "nps/summaryOthers/Fixed.php?htd=on&";
             textval = $("#BtnStoreHtdFixNps").data("text");
             $("#BtnStoreHtdFixNps").addClass("active");
             $("storehtd").html(textval);
             url = setUrl + params;
             loadContents(url, loader, content);
             $(this).addClass("loaded");
             }
             */
            var isLoaded = $(this).hasClass("loaded");
            if (!isLoaded) {
                $("#BtnStoreHtdFixNps").trigger("click");
            }

        });

        $('#combinedEmpNpsToggle').click(function() {
            var isLoaded = $(this).hasClass("loaded");
            var loader = "#EmployeeCombinedNpsLoader";
            var content = "#EmployeeCombinedNpsContainer";
            var params = setParams();
            var setUrl = "";
            var url = "";

            if (!isLoaded) {
                setUrl = "nps/summaryOthers/employeeMobileFixed.php?mtd=on&";
                url = setUrl + params;
                loadContents(url, loader, content);
                $(this).addClass("loaded");
            }
        });

        $('#emp-3mma-group button').click(function() {
            var textval = $(this).data("text");
            var loader = "#3mmaEmployeeNpsloader";
            var content = "#3MMAEmployeeNpsContainer";
            var params = setParams();
            var setUrl = "";
            var url = "";

            switch (textval) {
                case 'Interaction':
                    setUrl = "nps/summaryOthers/employeeNps.php?3mma=on&";
                    break;
                case 'Mobility (OTG)':
                    setUrl = "nps/summaryOthers/employeeNpsMobile.php?3mma=on&";
                    break;
                case 'Fixed (Home)':
                    setUrl = "nps/summaryOthers/employeeNpsFixed.php?3mma=on&";
                    break;
                case 'Episode':
                    setUrl = "nps/summaryOthers/employeeNpsEpisode.php?3mma=on&";
                    break;
            }

            $("#emp-3mma-group button").removeClass("active");
            $(this).addClass("active");
            var isCollapsed = $("#emp3mmatoggle").hasClass("collapsed");
            $("#emp3mmatoggle").addClass('loaded');
            if (isCollapsed) {
                $("#emp3mmatoggle").click();
            }

            $("threemma").html(textval);
            url = setUrl + params;
            loadContents(url, loader, content);

        });
        $('#emp3mmatoggle').click(function() {
            /*
             var isLoaded = $(this).hasClass("loaded");
             var textval = $('#storehtd-group button').data("text");
             var loader = "#3mmaEmployeeNpsloader";
             var content = "#3MMAEmployeeNpsContainer";
             var params = setParams();
             var setUrl = "";
             var url = "";

             if (!isLoaded) {
             setUrl = "nps/summaryOthers/employeeNpsFixed.php?3mma=on&";
             textval = $("#BtnEmpNps3mmaFix").data("text");
             $("#BtnEmpNps3mmaFix").addClass("active");
             $("threemma").html(textval);
             url = setUrl + params;
             loadContents(url, loader, content);
             $(this).addClass("loaded");
             }
             */
            var isLoaded = $(this).hasClass("loaded");
            if (!isLoaded) {
                $("#BtnEmpNps3mmaFix").trigger("click");
            }

        });

        $('#empnps-group button').click(function() {
            var textval = $(this).data("text");
            var loader = "#EmployeeMtdNpsloader";
            var content = "#EmployeeMtdNpsContainer";
            var params = setParams();
            var setUrl = "";
            var url = "";

            switch (textval) {
                case 'Interaction':
                    setUrl = "nps/summaryOthers/employeeNps.php?";
                    break;
                case 'Mobility (OTG)':
                    setUrl = "nps/summaryOthers/employeeNpsMobile.php?";
                    break;
                case 'Fixed (Home)':
                    setUrl = "nps/summaryOthers/employeeNpsFixed.php?";
                    break;
                case 'Episode':
                    setUrl = "nps/summaryOthers/employeeNpsEpisode.php?";
                    break;
            }



            $("#empnps-group button").removeClass("active");
            $(this).addClass("active");
            var isCollapsed = $("#empMtdToggle").hasClass("collapsed");
            $("#empMtdToggle").addClass('loaded');
            if (isCollapsed) {
                $("#empMtdToggle").click();
            }

            $("empnps").html(textval);
            url = setUrl + params;
            loadContents(url, loader, content);

        });
        $('#empMtdToggle').click(function() {
            /*
             var isLoaded = $(this).hasClass("loaded");
             var textval = $('#empnps-group button').data("text");
             var loader = "#EmployeeMtdNpsloader";
             var content = "#EmployeeMtdNpsContainer";
             var params = setParams();
             var setUrl = "";
             var url = "";

             if (!isLoaded) {
             setUrl = "nps/summaryOthers/employeeNpsFixed.php?";
             textval = $("#BtnEmpMtdNpsFix").data("text");
             $("#BtnEmpMtdNpsFix").addClass("active");
             $("empnps").html(textval);
             url = setUrl + params;
             loadContents(url, loader, content);
             $(this).addClass("loaded");
             }
             */
            var isLoaded = $(this).hasClass("loaded");
            if (!isLoaded) {
                $("#BtnEmpMtdNpsFix").trigger("click");
            }

        });

        $('#weekly-group button').click(function() {
            var textval = $(this).data("text");
            var loader = "#WeeklyEmployeeNpsloader";
            var content = "#WeeklyEmployeeNpsContainer";
            var params = setParams();
            var setUrl = "";
            var url = "";

            switch (textval) {
                case 'Interaction':
                    setUrl = "nps/summary/employeeNps.php?weekly=on&";
                    break;
                case 'Mobility (OTG)':
                    setUrl = "nps/summary/employeeNpsMobile.php?weekly=on&";
                    break;
                case 'Fixed (Home)':
                    setUrl = "nps/summary/employeeNpsFixed.php?weekly=on&";
                    break;
                case 'Episode':
                    setUrl = "nps/summary/employeeNpsEpisode.php?weekly=on&";
                    break;
            }

            $("#weekly-group button").removeClass("active");
            $(this).addClass("active");
            var isCollapsed = $("#emp4WeekToggle").hasClass("collapsed");
            $("#emp4WeekToggle").addClass('loaded');
            if (isCollapsed) {
                $("#emp4WeekToggle").click();
            }

            $("weekly").html(textval);
            url = setUrl + params;
            loadContents(url, loader, content);

        });

        $('#emp4WeekToggle').click(function() {
            /*
             var isLoaded = $(this).hasClass("loaded");
             var textval = $('#emphtdnps-group button').data("text");
             var loader = "#EmployeeHtdNpsloader";
             var content = "#EmployeeHtdNpsContainer";
             var params = setParams();
             var setUrl = "";
             var url = "";

             if (!isLoaded) {
             setUrl = "nps/summary/employeeNpsFixed.php?htd=on&";
             textval = $("#BtnEmpNpsHtdFix").data("text");
             $("#BtnEmpNpsHtdFix").addClass("active");
             $("emphtdnps").html(textval);
             url = setUrl + params;
             loadContents(url, loader, content);
             $(this).addClass("loaded");
             }
             */
            var isLoaded = $(this).hasClass("loaded");
            if (!isLoaded) {
                $("#BtnWeekRollingFix").trigger("click");
            }


        });

        $('#emphtdnps-group button').click(function() {
            var textval = $(this).data("text");
            var loader = "#EmployeeHtdNpsloader";
            var content = "#EmployeeHtdNpsContainer";
            var params = setParams();
            var setUrl = "";
            var url = "";

            switch (textval) {
                case 'Interaction':
                    setUrl = "nps/summaryOthers/employeeNps.php?htd=on&";
                    break;
                case 'Mobility (OTG)':
                    setUrl = "nps/summaryOthers/employeeNpsMobile.php?htd=on&";
                    break;
                case 'Fixed (Home)':
                    setUrl = "nps/summaryOthers/employeeNpsFixed.php?htd=on&";
                    break;
                case 'Episode':
                    setUrl = "nps/summaryOthers/employeeNpsEpisode.php?htd=on&";
                    break;
            }

            $("#emphtdnps-group button").removeClass("active");
            $(this).addClass("active");
            var isCollapsed = $("#empHTDToggle").hasClass("collapsed");
            $("#empHTDToggle").addClass('loaded');
            if (isCollapsed) {
                $("#empHTDToggle").click();
            }

            $("emphtdnps").html(textval);
            url = setUrl + params;
            loadContents(url, loader, content);

        });
        $('#empHTDToggle').click(function() {
            /*
             var isLoaded = $(this).hasClass("loaded");
             var textval = $('#emphtdnps-group button').data("text");
             var loader = "#EmployeeHtdNpsloader";
             var content = "#EmployeeHtdNpsContainer";
             var params = setParams();
             var setUrl = "";
             var url = "";

             if (!isLoaded) {
             setUrl = "nps/summaryOthers/employeeNpsFixed.php?htd=on&";
             textval = $("#BtnEmpNpsHtdFix").data("text");
             $("#BtnEmpNpsHtdFix").addClass("active");
             $("emphtdnps").html(textval);
             url = setUrl + params;
             loadContents(url, loader, content);
             $(this).addClass("loaded");
             }
             */
            var isLoaded = $(this).hasClass("loaded");
            if (!isLoaded) {
                $("#BtnEmpNpsHtdFix").trigger("click");
            }
        });

        $('#3mmamerge-group button').click(function() {
            var textval = $(this).data("text");
            var loader = "#3MMAMergeEmployeeNpsloader";
            var content = "#3MMAMergeEmployeeNpsContainer";
            var params = setParams();
            var setUrl = "";
            var url = "";

            switch (textval) {
                case 'Interaction':
                    setUrl = "nps/summaryOthers/employeeMergedNps.php?3mma=on&";
                    break;
                case 'Mobility (OTG)':
                    setUrl = "nps/summaryOthers/employeeMergedNpsMobile.php?3mma=on&";
                    break;
                case 'Fixed (Home)':
                    setUrl = "nps/summaryOthers/employeeMergedNpsFixed.php?3mma=on&";
                    break;
                case 'Episode':
                    setUrl = "nps/summaryOthers/employeeMergedNpsEpisode.php?3mma=on&";
                    break;
            }

            $("#3mmamerge-group button").removeClass("active");
            $(this).addClass("active");
            var isCollapsed = $("#3mmaEmpmergeToggle").hasClass("collapsed");
            $("#3mmaEmpmergeToggle").addClass('loaded');
            if (isCollapsed) {
                $("#3mmaEmpmergeToggle").click();
            }

            $("mma3merge").html(textval);
            url = setUrl + params;
            loadContents(url, loader, content);

        });
        $('#3mmaEmpmergeToggle').click(function() {

            /*
             var isLoaded = $(this).hasClass("loaded");
             var textval = $('#3mmamerge-group button').data("text");
             var loader = "#3MMAMergeEmployeeNpsloader";
             var content = "#3MMAMergeEmployeeNpsContainer";
             var params = setParams();
             var setUrl = "";
             var url = "";

             if (!isLoaded) {
             setUrl = "nps/summaryOthers/employeeMergedNpsFixed.php?3mma=on&";
             textval = $("#Btn3mmaEmpMergeFix").data("text");
             $("#Btn3mmaEmpMergeFix").addClass("active");
             $("mma3merge").html(textval);
             url = setUrl + params;
             loadContents(url, loader, content);
             $(this).addClass("loaded");
             }
             */
            var isLoaded = $(this).hasClass("loaded");
            if (!isLoaded) {
                $("#Btn3mmaEmpMergeFix").trigger("click");
            }
        });

        $('#merge-group button').click(function() {
            var textval = $(this).data("text");
            var loader = "#MergedMtdEmployeeNpsloader";
            var content = "#MergedMtdEmployeeNpsContainer";
            var params = setParams();
            var setUrl = "";
            var url = "";

            switch (textval) {
                case 'Interaction':
                    setUrl = "nps/summaryOthers/employeeMergedNps.php?";
                    break;
                case 'Mobility (OTG)':
                    setUrl = "nps/summaryOthers/employeeMergedNpsMobile.php?";
                    break;
                case 'Fixed (Home)':
                    setUrl = "nps/summaryOthers/employeeMergedNpsFixed.php?";
                    break;
                case 'Episode':
                    setUrl = "nps/summaryOthers/employeeMergedNpsEpisode.php?";
                    break;
            }

            $("#merge-group button").removeClass("active");
            $(this).addClass("active");
            var isCollapsed = $("#mtdEmpmergeToggle").hasClass("collapsed");
            $("#mtdEmpmergeToggle").addClass('loaded');
            if (isCollapsed) {
                $("#mtdEmpmergeToggle").click();
            }

            $("merge").html(textval);
            url = setUrl + params;
            loadContents(url, loader, content);

        });
        $('#mtdEmpmergeToggle').click(function() {
            /* var isLoaded = $(this).hasClass("loaded");
             var textval = $('#3mmamerge-group button').data("text");
             var loader = "#MergedMtdEmployeeNpsloader";
             var content = "#MergedMtdEmployeeNpsContainer";
             var params = setParams();
             var setUrl = "";
             var url = "";

             if (!isLoaded) {
             setUrl = "nps/summaryOthers/employeeMergedNpsFixed.php?";
             textval = $("#BtnMtdEmpMergeFix").data("text");
             $("#BtnMtdEmpMergeFix").addClass("active");
             $("merge").html(textval);
             url = setUrl + params;
             loadContents(url, loader, content);
             $(this).addClass("loaded");
             } */
            var isLoaded = $(this).hasClass("loaded");
            if (!isLoaded) {
                $("#BtnMtdEmpMergeFix").trigger("click");
            }
        });


        $('#weekmerge-group button').click(function() {
            var textval = $(this).data("text");
            var loader = "#FourWeekMergedEmployeeNpsloader";
            var content = "#FourWeekMergedEmployeeNpsContainer";
            var params = setParams();
            var setUrl = "";
            var url = "";

            switch (textval) {
                case 'Interaction':
                    setUrl = "nps/summaryOthers/employeeMergedNps.php?weekly=on&";
                    break;
                case 'Mobility (OTG)':
                    setUrl = "nps/summaryOthers/employeeMergedNpsMobile.php?weekly=on&";
                    break;
                case 'Fixed (Home)':
                    setUrl = "nps/summaryOthers/employeeMergedNpsFixed.php?weekly=on&";
                    break;
                case 'Episode':
                    setUrl = "nps/summaryOthers/employeeMergedNpsEpisode.php?weekly=on&";
                    break;
            }

            $("#weekmerge-group button").removeClass("active");
            $(this).addClass("active");
            var isCollapsed = $("#weekEmpmergeToggle").hasClass("collapsed");
            $("#weekEmpmergeToggle").addClass('loaded');
            if (isCollapsed) {
                $("#weekEmpmergeToggle").click();
            }

            $("weekmerge").html(textval);
            url = setUrl + params;
            loadContents(url, loader, content);

        });
        $('#weekEmpmergeToggle').click(function() {
            var isLoaded = $(this).hasClass("loaded");
            if (!isLoaded) {
                $("#BtnFourWeekMergeFix").trigger("click");
            }
        });


        /*  $(".btn").on('click', function()
         {
         var active = $(this).hasClass('active');
         console.log(active);
         var panels = localStorage.panels === undefined ? new Array() : JSON.parse(localStorage.panels);
         if ($.inArray(active, panels) == -1) //check that the element is not in the array
         panels.push(active);
         localStorage.panels = JSON.stringify(panels);
         });

         $(".btn").on('click', function()
         {
         var active = $(this).attr('id');
         // console.log(this);
         var panels = localStorage.panels === undefined ? new Array() : JSON.parse(localStorage.panels);
         var elementIndex = $.inArray(active, panels);
         if (elementIndex !== -1) //check the array
         {
         panels.splice(elementIndex, 1); //remove item from array
         }
         localStorage.panels = JSON.stringify(panels); //save array on localStorage
         });

         var panels = localStorage.panels === undefined ? new Array() : JSON.parse(localStorage.panels); //get all panels
         for (var i in panels) { //<-- panel is the name of the cookie
         if ($("#" + panels[i]).hasClass('panel-collapse')) // check if this is a panel
         {
         $("#" + panels[i]).collapse("show");
         }
         } */

        /* $('#StoreNPSTier', function() {
         var params = setParams();
         var url = "nps/charts/TierLevel_Bar.php?tiersort=Fixed&3mma=on&" + params;
         var hide = "#StoreNPSTierLoader";
         var show = "#StoreNPSTier";

         loadContents(url, hide, show);
         });

         $('#StoreMtdNpsContainer', function() {
         var params = setParams();
         var url = "nps/summaryOthers/Fixed.php?" + params;
         var hide = "#StoreMtdNpsloader";
         var show = "#StoreMtdNpsContainer";
         loadContents(url, hide, show);
         });

         $('#StoreNpsWeeklyContainer', function() {
         var params = setParams();
         var url = "nps/summaryOthers/episode.php?weekly=on&" + params;
         var hide = "#StoreNpsWeeklyloader";
         var show = "#StoreNpsWeeklyContainer";
         loadContents(url, hide, show);
         });

         $('#EmployeeMtdNpsContainer', function() {
         var params = setParams();
         var url = "nps/summaryOthers/employeeNpsEpisode.php?" + params;
         var hide = "#EmployeeMtdNpsloader";
         var show = "#EmployeeMtdNpsContainer";
         loadContents(url, hide, show);
         });

         $('#WeeklyEmployeeNpsContainer', function() {
         var params = setParams();
         var url = "nps/summaryOthers/employeeNpsEpisode.php?weekly=on&" + params;
         var hide = "#WeeklyEmployeeNpsloader";
         var show = "#WeeklyEmployeeNpsContainer";
         loadContents(url, hide, show);
         });

         $('#weekly-group button').click(function() {
         var textval = $(this).data("text");
         var loader = "#WeeklyEmployeeNpsloader";
         var content = "#WeeklyEmployeeNpsContainer";
         var params = setParams();
         var setUrl = "";
         var url = "";

         switch (textval) {
         case 'Interaction':
         setUrl = "nps/summaryOthers/employeeNps.php?weekly=on&";
         break;
         case 'Mobility (OTG)':
         setUrl = "nps/summaryOthers/employeeNpsMobile.php?weekly=on&";
         break;
         case 'Fixed (Home)':
         setUrl = "nps/summaryOthers/employeeNpsFixed.php?weekly=on&";
         break;
         case 'Episode':
         setUrl = "nps/summaryOthers/employeeNpsEpisode.php?weekly=on&";
         break;
         }


         $("#weekly-group button").removeClass("active");
         $(this).addClass("active");


         $("weekly").html(textval);
         url = setUrl + params;
         loadContents(url, loader, content);

         });

         $('#MergedMtdEmployeeNpsloader', function() {
         var params = setParams();
         var url = "nps/summaryOthers/employeeMergedNpsEpisode.php?" + params;
         var hide = "#MergedMtdEmployeeNpsloader";
         var show = "#MergedMtdEmployeeNpsContainer";
         loadContents(url, hide, show);
         });

         $('#FourWeekMergedEmployeeNps', function() {
         var params = setParams();
         var url = "nps/summaryOthers/employeeMergedNpsEpisode.php?weekly=on&" + params;
         var hide = "#FourWeekMergedEmployeeNpsloader";
         var show = "#FourWeekMergedEmployeeNpsContainer";
         loadContents(url, hide, show);
         });

         $('#StoreNps3MonthContainer', function() {
         var params = setParams();
         var url = "nps/summaryOthers/Mobile.php?3months=on&" + params;
         var hide = "#StoreNps3Monthloader";
         var show = "#StoreNps3MonthContainer";
         loadContents(url, hide, show);
         });
         //removed July 09,
         $('#nps3months').click(function() {
         var isLoaded = $(this).hasClass("loaded");
         var textval = $('#3Month-group button').data("text");
         var loader = "#StoreNps3Monthloader";
         var content = "#StoreNps3MonthContainer";
         var params = setParams();
         var setUrl = "";
         var url = "";

         if (!isLoaded) {
         switch (textval) {

         case 'Mobility (OTG)':
         setUrl = "nps/summaryOthers/Mobile.php?3months=on&";
         break;
         case 'Fixed (Home)':
         setUrl = "nps/summaryOthers/Fixed.php?3months=on&";
         break;
         }

         $("month3").html(textval);
         url = setUrl + params;
         loadContents(url, loader, content);
         $(this).addClass("loaded");
         }
         });

         $('#3Month-group button').click(function() {
         var textval = $(this).data("text");
         var loader = "#StoreNps3Monthloader";
         var content = "#StoreNps3MonthContainer";
         var params = setParams();
         var setUrl = "";
         var url = "";

         switch (textval) {

         case 'Mobility (OTG)':
         setUrl = "nps/summaryOthers/Mobile.php?3months=on&";
         break;
         case 'Fixed (Home)':
         setUrl = "nps/summaryOthers/Fixed.php?3months=on&";
         break;
         }

         $("#3Month-group button").removeClass("active");
         $(this).addClass("active");

         $("month3").html(textval);
         url = setUrl + params;
         loadContents(url, loader, content);

         });

         $('#StoreNps3MMAContainer', function() {
         var params = setParams();
         var url = "nps/summaryOthers/Mobile.php?3mma=on&" + params;
         var hide = "#StoreNps3MMAloader";
         var show = "#StoreNps3MMAContainer";
         loadContents(url, hide, show);
         });

         $('#3MonthsEmployeeNpsContainer', function() {
         var params = setParams();
         var url = "nps/summaryOthers/employeeNpsMobile.php?3months=on&" + params;
         var hide = "#3MonthsEmployeeNpsloader";
         var show = "#3MonthsEmployeeNpsContainer";
         loadContents(url, hide, show);
         });

         $('#emp3months').click(function() {
         var isLoaded = $(this).hasClass("loaded");
         var textval = $('#3mnth-group button').data("text");
         var loader = "#3MonthsEmployeeNpsloader";
         var content = "#3MonthsEmployeeNpsContainer";
         var params = setParams();
         var setUrl = "";
         var url = "";

         if (!isLoaded) {
         switch (textval) {
         case 'Interaction':
         setUrl = "nps/summaryOthers/employeeNps.php?3months=on&";
         break;
         case 'Mobility (OTG)':
         setUrl = "nps/summaryOthers/employeeNpsMobile.php?3months=on&";
         break;
         case 'Fixed (Home)':
         setUrl = "nps/summaryOthers/employeeNpsFixed.php?3months=on&";
         break;
         case 'Episode':
         setUrl = "nps/summaryOthers/employeeNpsEpisode.php?3months=on&";
         break;
         }

         $("threemonth").html(textval);
         url = setUrl + params;
         loadContents(url, loader, content);
         $(this).addClass("loaded");
         }
         });

         $('#3mnth-group button').click(function() {
         var textval = $(this).data("text");
         var loader = "#3MonthsEmployeeNpsloader";
         var content = "#3MonthsEmployeeNpsContainer";
         var params = setParams();
         var setUrl = "";
         var url = "";

         switch (textval) {
         case 'Interaction':
         setUrl = "nps/summaryOthers/employeeNps.php?3months=on&";
         break;
         case 'Mobility (OTG)':
         setUrl = "nps/summaryOthers/employeeNpsMobile.php?3months=on&";
         break;
         case 'Fixed (Home)':
         setUrl = "nps/summaryOthers/employeeNpsFixed.php?3months=on&";
         break;
         case 'Episode':
         setUrl = "nps/summaryOthers/employeeNpsEpisode.php?3months=on&";
         break;
         }

         $("threemonth").html(textval);
         url = setUrl + params;
         loadContents(url, loader, content);

         });

         $('#StoreNpsHTDContainer', function() {
         var params = setParams();
         var url = "nps/summaryOthers/episode.php?htd=on&" + params;
         var hide = "#StoreNpsHTDloader";
         var show = "#StoreNpsHTDContainer";
         loadContents(url, hide, show);
         });

         $('#EmployeeHtdNpsContainer', function() {
         var params = setParams();
         var url = "nps/summaryOthers/employeeNpsEpisode.php?htd=on&" + params;
         var hide = "#EmployeeHtdNpsloader";
         var show = "#EmployeeHtdNpsContainer";
         loadContents(url, hide, show);
         });

         $('#3MMAEmployeeNpsContainer', function() {
         var params = setParams();
         var url = "nps/summaryOthers/employeeNpsFixed.php?3mma=on&" + params;
         var hide = "#3mmaEmployeeNpsloader";
         var show = "#3MMAEmployeeNpsContainer";
         loadContents(url, hide, show);
         });

         $('#3MMAMergeEmployeeNpsContainer', function() {
         var params = setParams();
         var url = "nps/summaryOthers/employeeMergedNpsMobile.php?3mma=on&" + params;
         var hide = "#3MMAMergeEmployeeNpsloader";
         var show = "#3MMAMergeEmployeeNpsContainer";
         loadContents(url, hide, show);
         });

         $('#EmployeeCombinedNpsContainer', function() {
         var params = setParams();
         var url = "nps/summaryOthers/employeeMobileFixed.php?mtd=on&" + params;
         var hide = "#EmployeeCombinedNpsLoader";
         var show = "#EmployeeCombinedNpsContainer";
         loadContents(url, hide, show);
         }); */


    });
</script>
