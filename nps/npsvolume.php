<?php
sleep(2);
session_start();
error_reporting(0);
include('admin/function/SubFunction.php');
$first_day_this_month = date('01/m/Y'); // hard-coded '01' for first day
$last_day_this_month = date('d/m/Y');
$empID = $_SESSION['EmpProfileID'];
$role = $_SESSION['UserRoleID'];
$withAdminTracker = array('1', '2', '3', '5');


$param = 'All';
if ($role != 1) {
    $param = 'Own';
}
$storeList = SelectStoreList($param);
$storeTypes = SelectStoreTypes($param);
?>

<script type="text/javascript">
    $(document).ready(function () {

        $("#from").datepicker({
            showOn: "button",
            buttonImage: "https://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
            buttonImageOnly: true, dateFormat: 'dd/mm/yy',
            numberOfMonths: 2,
            onSelect: function (selected) {
                $("#to").datepicker("option", "minDate", selected);
            }
        });

        $("#to").datepicker({
            showOn: "button",
            buttonImage: "https://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
            buttonImageOnly: true, dateFormat: 'dd/mm/yy',
            numberOfMonths: 2,
//            onSelect: function (selected) {
//                $("#from").datepicker("option", "maxDate", selected);
//            }
        });

        $('#NpsVolumeContent', function () {
            var params = setParams();
            var urlToLoad = "nps/charts/NpsVolume_LineBar_v2.php?" + params;
            var divToHide = "#LogLoader";
            var divToShow = "#NpsVolumeContent";
            loadContents(urlToLoad, divToHide, divToShow);
        });

        $('#refresh').click(function () {
            var params = setParams();
            var urlToLoad = "nps/charts/NpsVolume_LineBar_v2.php?" + params;
            var divToHide = "#LogLoader";
            var divToShow = "#NpsVolumeContent";
            loadContents(urlToLoad, divToHide, divToShow);
        });

        $('#3MMARolling').click(function () {
            var currentTo = $("#to").datepicker('getDate');
            if ($('#StoreType').prop('checked')) {
                $("#to").datepicker('setDate', new Date(), 0);

                currentTo.setMonth(currentTo.getMonth() - 2, 0);
                currentTo.setDate(1);
            } else {
                currentTo.setMonth(currentTo.getMonth() - 2);
                currentTo.setDate(1);
            }
            console.log(currentTo);

            $("#from").datepicker('setDate', currentTo);

        });

        $('#MtdRolling').click(function () {
            var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            $("#from").datepicker('setDate', firstDay);
            $("#to").datepicker('setDate', lastDay);
        });

        $('#HTDRolling').click(function () {
            var date = new Date();
            //var output = (moment(date).quarter() <= 2 ? "H1" : "H2") + ' ' + moment(date).format('YYYY');
            // consol.log(output);
            var curMonth = date.getMonth() + 1;
            var curYear = date.getFullYear();
            console.log(curMonth);
            if (curMonth >= 1 && curMonth <= 6) {
                var from = "01/01/" + curYear;
                var to = "06/30/" + curYear;
                $("#from").datepicker('setDate', new Date(from));
                $("#to").datepicker('setDate', new Date(to));
            } else
            {
                var from = "07/01/" + curYear;
                var to = "12/31/" + curYear;
                $("#from").datepicker('setDate', new Date(from));
                $("#to").datepicker('setDate', new Date(to));
            }

        });


    });


    function setParams() {
        var dateFrom = $("#from").val().split("/").reverse().join("-");
        var dateTo = $("#to").val().split("/").reverse().join("-");
        var reportView = $('#reportView').val();

        var storeID = $('#storeID').val();

        //empID = JSON.stringify(empID);
        var params = "to=" + dateTo + "&from=" + dateFrom + "&storeID=" + storeID + "&reportView=" + reportView;
        //console.log(params);
        return params;
    }

    function loadContents(paramurl, loader, content) {
        $(content).html('');
        $(loader).show();
        $.ajax({
            url: paramurl,
            cache: false,
            success: function (html) {
                $(content).append(html);
            },
            complete: function () {
                $(loader).hide();
            }
        });
    }
    $(function () {
        $('[data-tooltip="tooltip"]').tooltip();
    });
</script>
<input type="hidden" name="empID" id="empID" value="<?php echo $empID; ?>"/>
<input type="hidden" name="role" id="role" value="<?php echo $role; ?>"/>

<div  id="profileWell"  class="profileWell">


    <div class="row">
        <h4 class="page-header"><i class="fa fa-calculator"></i>Store NPS vs Volume</h4>
        <div class="filters col-lg-12 col-sm-12 col-md-12">
            <div class="pull-left">
                <p class="latest-record">
                    <?php
                    echo $latestRec['latest_record'];
                    ?>
                </p>
            </div>
            <div class="filters col-lg-10">
                <div class="" style="padding-left:10%">   
                    <div class="btn-group group1" id="weekly-group" role="group" aria-label="...">                   
                        <button type="button" id="3MMARolling" data-text="3 Month Moving Average"  class="btn btn-xs btn-danger">3 MMA</button> 
                        <!--<button type="button" id="DefaultRolling" data-text="4 Weeks" class="btn btn-xs btn-primary ">Default</button>-->
                        <button type="button" id="MtdRolling"  data-text="Month to Date" class="btn btn-xs btn-warning">MTD</button>
                        <!--<button type="button" id="FourWeekRolling" data-text="4 Weeks"  class="btn btn-xs btn-primary">4 Weeks</button>-->
                        <button type="button" id="HTDRolling" data-text="HTD"  class="btn btn-xs btn-success">HTD</button>
                        <!--<button type="button" id="FTDRolling" data-text="FTD"  class="btn btn-xs btn-default">FTD</button>-->
                    </div>
                    <input type="text" id="from" name="from" class="form-control" value="<?php echo $first_day_this_month; ?>" readonly="true"/>
                    <input type="text" id="to" name="to"  class="form-control" value="<?php echo $last_day_this_month; ?>" readonly="true"/>                    
                    <select name="StoreID" id="storeID" class="form-control">
                        <option value="SelectStore">Please Select Store</option>
                        <?php
                        foreach ($storeList as $value) {
                            echo '<option value="' . $value['f_StoreID'] . '">' . $value['f_StoreListName'] . '</option>';
                        }
                        
                        if($storeTypes['StoreTypes'] == 'Only TLS'){
                            echo  "<option value='CombinedTLS'>Combined</option>";
                        }elseif($storeTypes['StoreTypes'] == 'Only Others'){
                            echo  "<option value='CombinedOthers'>Combined</option>";
                        }elseif($storeTypes['StoreTypes'] == 'Only TBC'){
                            echo  "<option value='CombinedTBC'>Combined</option>";
                        }else{
                            echo  "";
                        }
                        ?>                       
                        
                      
                    </select>
                    <select name="reportView" id="reportView" class="form-control" style="width:25%">                       
                        <option value="1">NPS vs Volume</option>                       
                        <option value="2">Volume vs NPS</option>                       
                    </select>  
                    <button type="submit" class="btn btn-warning btn-circle pull-rigth" id="refresh"
                            data-tooltip="tooltip" data-placement="bottom" title="Reload">
                        <i class="fa fa-refresh"></i>
                    </button>
                </div>
            </div> 
        </div>
       
    </div>
    <div class="main-content" >
        <div class="loader" id="LogLoader"></div>
        <div id="NpsVolumeContent"> </div>
    </div>
</div>


