<?php
date_default_timezone_set('Australia/Melbourne');
ini_set('max_execution_time', 300);

$role = $_SESSION['UserRoleID'];
$first_day_this_month = date('01/m/Y'); // hard-coded '01' for first day
$last_day_this_month = date('d/m/Y');

$date = date('M-y');




include('function/NPSFunction.php');
require($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');

$SelectStoreTypes = SelectStoreTypes();
$blankPnumber = GetBlankPnumberList();

$countBlank = count($blankPnumber);
?>

<script type="text/javascript">
//test commit
    $(document).ready(function () {

<?php if ($SelectStoreTypes['StoreTypes'] == 'Only TBC') { ?>
            $("#StoreType").trigger('click');
<?php } ?>

        var from = $("#from").val();
        var to = $("#to").val();
        var role = $("#ShareType").attr('role');

        var shared = "";
        var storeType = "";
        var toURL = "";

        function setLoad() {
            var mtdFrom = $("#mtdfrom").val();
            var mtdTo = $("#mtdto").val();
            var mtdSet = '';

            var toTBC = $("#TBC").val();

            if ($('#ShareType').prop('checked')) {
                shared = 'yes';
            } else {
                shared = 'no';
            }


            if ($('#StoreType').prop('checked')) {


                storeType = 'yes';
                toURL = "nps/summarycloudTBC.php?shared=";
                $('#tlsPicker').hide();
                $('#tbcPicker').show();
                $('#hideMTD').show();

                if ($('#MtdType').prop('checked')) {
                    $('#tbcPicker').hide();
                    $('#mtdPicker').show();
                    mtdSet = '&mtdSet=yes';
                } else {
                    $('#mtdPicker').hide();
                    $('#tbcPicker').show();
                    mtdSet = '';
                }

            } else {

                if ($('#StoreType').data('store-type') == 'others') {

                    toURL = "nps/summarycloudOthers.php?shared=";
                } else {
                    toURL = "nps/summarycloud.php?shared=";
                }


                storeType = '3';

                $('#tlsPicker').show();
                $('#mtdPicker').hide();
                $('#tbcPicker').hide();
                $('#hideMTD').hide();

            }



            var toLoad = toURL + shared + '&to=' + to + '&from=' + from + '&role=' + role + '&storeType=' + storeType + '&toTBC=' + toTBC + '&mtdTo=' + mtdTo + '&mtdFrom=' + mtdFrom + mtdSet;
            // console.log(toLoad);
            return toLoad;
        }

        $('#page-contents', function () {
            var toLoad = setLoad();
            var toLoadextended = toLoad;
            $.get(toLoadextended).success(
                    function (response, status, jqXhr) {
                        window.stop();
                        $('#page-contents').html();
                        $('#page-contents').html(response); // append();
                    }).error(function (response, status, jqXhr) {
            }).complete(function (response, status, jqXhr) {
            });
        });


        $("#toTBC").datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'M-y',
            onClose: function (dateText, inst) {
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('option', 'defaultDate', new Date(year, month, 1));
                $(this).datepicker('setDate', new Date(year, month, 1));
                //  console.log($(this).val());
            }
        });

        $("#from").datepicker({
            showOn: "button",
            buttonImage: "https://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
            buttonImageOnly: true, dateFormat: 'dd/mm/yy',
            numberOfMonths: 2,
            onSelect: function (selected) {
                $("#to").datepicker("option", "minDate", selected);
            }
        });

        $("#to").datepicker({
            showOn: "button",
            buttonImage: "https://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
            buttonImageOnly: true, dateFormat: 'dd/mm/yy',
            numberOfMonths: 2,
//            onSelect: function (selected) {
//                $("#from").datepicker("option", "maxDate", selected);
//            }
        });


        $("#mtdfrom").datepicker({
            showOn: "button",
            buttonImage: "https://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
            buttonImageOnly: true, dateFormat: 'dd/mm/yy',
            numberOfMonths: 2,
            onSelect: function (selected) {
                $("#mtdto").datepicker("option", "minDate", selected);
            }
        });

        $("#mtdto").datepicker({
            showOn: "button",
            buttonImage: "https://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
            buttonImageOnly: true, dateFormat: 'dd/mm/yy',
            numberOfMonths: 2,
//            onSelect: function (selected) {
//                $("#mtdfrom").datepicker("option", "maxDate", selected);
//            }
        });


        $('#refresh').click(function () {
            var toLoad = setLoad();
            var from = $("#from").val().split("/").reverse().join("-");
            var to = $("#to").val().split("/").reverse().join("-");

//            $.post("admin/function/SubFunction.php",
//                    {
//                        to: to,
//                        from: from,
//                        count: "on"
//                    },
//                    function(data, status) {
//                        console.log(data);
//                    });
            var params = '&to=' + to + '&from=' + from;
            console.log($("#from").val().split("/").reverse().join("-"));
            $.ajax({

                type: 'GET',
                dataType: 'json',
                url: 'admin/function/SubFunction.php?count=on' + params,
                success: function (data) {
                    // console.log('success', data);
                    $('unassigned').html(data.count);
                }
            });

            $('#page-contents').load(toLoad);
        });

        $('#ShareType').change(function () {
            var toLoad = setLoad();
            $('#page-contents').load(toLoad);
        });

        $('#StoreType').change(function () {
            var toLoad = setLoad();
            $('#page-contents').load(toLoad);
        });

        $('#MtdType').change(function () {

            if ($('#MtdType').prop('checked')) {

                $('#mtdPicker').show();
                $('#tbcPicker').hide();
            } else {
                $('#mtdPicker').hide();
                $('#tbcPicker').show();
                $('#MtdType').hide();
            }
        });

    });

    $(function () {
        $('[data-tooltip="tooltip"]').tooltip();
    });

<?php if ($_SESSION['isPrivate']) { ?>
        $("#ShareType").hide();
        console.log('hidden');
<?php } ?>


</script>


<div class="row">
    <h4 class="page-header"><i class="fa fa-shopping-cart fa-fw"></i>NPS Summary </h4>

    <div class="filters">

        <w id="tlsPicker"> <input type="text" id="from" name="from" class="form-control" value="<?php echo $first_day_this_month; ?>" readonly="true"/>
            <input type="text" id="to" name="to"  class="form-control" value="<?php echo $last_day_this_month; ?>" readonly="true"/> </w>
        <w id="mtdPicker"> <input type="text" id="mtdfrom" name="mtdfrom" class="form-control" value="<?php echo $first_day_this_month; ?>" readonly="true"/>
            <input type="text" id="mtdto" name="mtdto"  class="form-control" value="<?php echo $last_day_this_month; ?>" readonly="true"/> </w>
        <w id="tbcPicker"> <input type="text" id="toTBC" name="toTBC"  class="form-control" value="<?php echo $date; ?>" readonly="true"/> </w>

        <button type="submit" class="btn btn-warning btn-circle" id="refresh" data-tooltip="tooltip" data-placement="bottom" title="Reload">
            <i class="fa fa-refresh"></i>
        </button>

        <?php
        if ($role == '1') {
            $attritubes = "style=\"display:none\" checked=\"checked\" role=\"1\" ";
        } elseif ($role == '2') {
            $attritubes = " role=\"2\" ";
        } elseif ($role == '3') {
            $attritubes = " role=\"3\" ";
        } elseif ($role == '4') {
            $attritubes = "style=\"display:none\" role=\"4\" ";
        } elseif ($role == '5') {
            $attritubes = " role=\"5\" ";
        } else {
            
        }
        $withShared = array('2', '3', '5');

//  $autoStoreType = array('1','2');
        //$SelectStoreTypes;

        if ($_SESSION['HasStoreType'] == 'Only Others') {
            ?>
            <input type="checkbox" style="display:none" <?php echo $attritubes; ?> class="StoreType" name="StoreType" id="StoreType" data-store-type="others" />
            <?php
        } else {
            if (in_array($role, $withShared) && !$_SESSION['isPrivate']) {
                ?>
                <input type="checkbox" style="display:none" <?php echo $attritubes; ?> class="ShareType" name="ShareType" id="ShareType" />
                   <!--<input type="checkbox" id="ShareType" name="ShareType" class="ShareType" data-toggle="toggle" data-style="ios" data-onstyle="success"  data-offstyle="info" data-on="Shared" data-off="Mine" <?php echo $attritubes; ?>/>-->
            <?php } else { ?>
                <input type="checkbox" style="display:none" <?php echo $attritubes; ?> class="ShareType" name="ShareType" id="ShareType" />
                <?php
            }

            if ($SelectStoreTypes['StoreTypes'] == 'Both Stores') {
                ?>                
                <input type="checkbox" id="StoreType" name="StoreType" class="StoreType" data-toggle="toggle" data-style="ios"  data-onstyle="info"
                       data-offstyle="success" data-on="TBTC" data-off="TLS" />
                <input type="checkbox" id="MtdType" name="MtdType" class="MtdType" data-toggle="toggle" data-style="ios"  data-onstyle="success"
                       data-offstyle="warning" data-on="MTD" data-off="Default" />
                   <?php } elseif ($SelectStoreTypes['StoreTypes'] == 'Only TBC') { ?> 
                <input type="checkbox" id="MtdType" name="MtdType" class="MtdType" data-toggle="toggle" data-style="ios"  data-onstyle="success"
                       data-offstyle="warning" data-on="MTD" data-off="Default" />
                <input type="checkbox" style="display:none"  id="StoreType" name="StoreType" class="StoreType" />
            <?php } else { ?>
                <input type="checkbox" style="display:none"  id="StoreType" name="StoreType" class="StoreType" />

                <?php
            }
        }
        ?>

    </div>
</div>

<?php
if ($countBlank >= 1) {
    $animated = "animated";
} else {
    $animated = " ";
    $countBlank = "";
}
?>
<div class="row">
    <p class="latest-record">
        <?php echo $latestRec['latest_record']; ?>
    </p>
    <br>
    <a class='btn btn-success btn-xs infinite rubberBand' href="nps/function/SurveyTabs.php"  data-toggle="modal" data-target="#latestSurveysModal"
       data-tooltip="tooltip" data-placement="bottom" title="Latest Inserted Surveys Today" style='margin-top: -5px;margin-left: 10px;padding: 0 5px;'>
        <i class="fa fa-microphone "style='font-size: 17px; padding: 0px 1px; padding-top: 3px;'></i>
    </a>
    <a class='btn btn-info btn-xs infinite rubberBand animated' href="nps/function/npsNews.php"  data-toggle="modal" data-target="#npsChangesNews"
       data-tooltip="tooltip" data-placement="bottom" title="Lastest NPS News" style='margin-top: -5px;margin-left: 10px;padding: 0 5px;'>
        <i class="fa fa-paper-plane-o "style='font-size: 17px; padding: 0px 1px; padding-top: 3px;'></i>
    </a>
    <a class='btn btn-danger btn-xs infinite rubberBand <?php echo $animated; ?>' href="public_html.php?i=6"
       data-tooltip="tooltip" data-placement="bottom" title="Unassigned Survey Count" style='margin-top: -5px;margin-left: 10px;padding: 0 5px;'>
        <i class="fa fa-user "style='font-size: 17px; padding: 0px 1px; padding-top: 3px;'> </i><unassigned><?php echo $countBlank; ?> </unassigned>
    </a>
</div>




<div  id="page-contents" class="row page-contents" ></div>


<div class="modal fade" id="latestSurveysModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div> <!-- Feedback Modal -->

<div class="modal fade" id="npsChangesNews" tabindex="-1" role="dialog" aria-labelledby="npsChangesNews" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div> <!-- Feedback Modal -->

<div class="modal fade" id="resolutionModal" tabindex="-1" role="dialog" aria-labelledby="resolutionModal" aria-hidden="true">
    <div class="modal-dialog" style="width: 700px">
        <div class="modal-content" id="resoulutionContent"></div>
    </div>
</div> <!-- Feedback Modal -->



<script type="text/javascript">

    $('a#resolutionFeed').click(function (ev) { // prepare modal for new data
        ev.preventDefault();
        var target = $(this).attr('href');
        console.log(target);
        $("#resoulutionContent").load(target, function () {
            $("#resolutionModal").modal("show");
        });
    });

    $("#resolutionModal").on("hidden.bs.modal", function () {
        $("#resoulutionContent").html("");
    });
</script>