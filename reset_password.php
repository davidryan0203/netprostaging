<?php
//require($_SERVER['DOCUMENT_ROOT'] . '/dashboard/function/rawFunction.php');
// $stores = getPartnerStores();
// $users = getUsers();
// $directors = getDirectors();
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <title>NetPro</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Logo-->
        <link rel="shortcut icon" href="images/logoIcon.png">
        <!--Custom Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,400italic,500,500italic,700,700italic,900,900italic,300italic,300" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
        <!--Font Awesome-->
        <link href="includes/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <!--Full slider -->
        <link href="includes/css/jquery.maximage.css" rel="stylesheet" type="text/css"/>
        <link href="includes/css/jquery.screensize.css" rel="stylesheet" type="text/css"/>
        <!--Animate Css -->
        <link href="includes/css/animate.css" rel="stylesheet" type="text/css"/>
        <!--Bootstrap-->
        <link href="includes/css/bootstrap.min.flatly.css" rel="stylesheet" type="text/css"/>
        <!--Custom CSS -->
        <link href="includes/css/stylesV2.css" rel="stylesheet" type="text/css"/>
        <!--Custom Css-->
        <link href="includes/css/homeV2.css" rel="stylesheet">
        <!--Media Queries -->
        <link href="includes/css/media-queries.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">

    </head>
    <Style>
        p,h1,h2,h3,h4,h5{
            color:#4b4b4b;
        }

        h1 {
            font-family: "Open Sans";  font-style: normal; font-variant: normal; font-weight: 700;
        }
        h3 {
            font-family: "Open Sans";  font-style: normal; font-variant: normal; font-weight: 700;
        }
        p {
            font-family: "Open Sans";  font-style: normal; font-variant: normal; font-weight: 400;
        }
        blockquote {
            font-family: "Open Sans"; font-style: normal; font-variant: normal; font-weight: 400;
        }
        pre {
            font-family: "Open Sans"; font-size: 13px; font-style: normal; font-variant: normal; font-weight: 400;
        }

        .loginBtn{
            color: #fff !important;
            float: right;
            margin-top: -5px;
            height: 30px;
            padding-top:3px;
        }

        input[type="text"],[type="email"],[type="password"]{
            font-size: 20px;
        }

        #yourMessage { font-size: 20px; }


        @media (min-width: 992px){

            .white-text{
                color:#fff;
            }

            .modal-lg {
                width: 1200px;
            }

            .uppercase{
                text-transform: uppercase;
                color:#39C7F5;
            }

            #banner{
                /*height: 85vh;*/
                height: 0vh;
            }

            .subheading{
                font-weight: normal;
                color:#9E9E9E;
            }

            .trialBtn{
                min-width: 100%;
                font-size: 26px;
                padding:0px;
                font-weight:600;
            }

            .btn{
                -webkit-box-shadow: 0px 0px 0px rgba(4, 4, 4, 0.3);
                -moz-box-shadow:    0px 0px 0px rgba(4, 4, 4, 0.3);
            }

            .navHover:hover{
                border-top:2px solid #08B99F;
            }

            .navbar-inverse .navbar-nav>li>a {
                color:#888888 !important;
            }

            .table-bordered th, .table-bordered td{
                /* border: 2px solid #888 !important;*/
            }

            .well {
                background-color: #F9F9F9 !important;
                min-height: 100px;
                text-align: center;
            }

            .grayText{
                color:#999999;
            }
        }

        .arrow_box {
            position: relative;
            background: #fff;
            border: 4px solid #fff;
        }
        .arrow_box:after, .arrow_box:before {
            top: 100%;
            left: 50%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
        }

        .arrow_box:after {
            border-color: rgba(255, 255, 255, 0);
            border-top-color: #fff;
            border-width: 20px;
            margin-left: -20px;
        }
        .arrow_box:before {
            border-color: rgba(255, 255, 255, 0);
            border-top-color: #fff;
            border-width: 26px;
            margin-left: -26px;
        }

        .col-center {
            margin: 0 auto;
            float: none !important;
        }
        .carousel {
            margin: 50px auto;
            /*padding: 0 70px;*/
            padding: 0px;
        }
        .carousel .item {
            color: #999;
            font-size: 14px;
            text-align: center;
            overflow: hidden;
            min-height: 290px;
        }



        .carousel .item .img-box {
            width: 135px;
            height: 135px;
            margin: 0 auto;
            padding: 5px;
            border-radius: 50%;
        }
        .carousel .img-box img {
            width: 100%;
            height: 100%;
            display: block;
            border-radius: 50%;
        }
        .carousel .testimonial {
            padding: 30px 0 10px;
            color:#4b4b4b;
        }
        .carousel .overview {
            font-style: italic;
        }
        .carousel .overview b {
            text-transform: uppercase;
            color: #7AA641;
        }
        .carousel .carousel-control {
            width: 40px;
            height: 40px;
            margin-top: -20px;
            top: 50%;
            background: none;
        }
        .carousel-control i {
            font-size: 68px;
            line-height: 42px;
            position: absolute;
            display: inline-block;
            color: rgba(0, 0, 0, 0.8);
            text-shadow: 0 3px 3px #e6e6e6, 0 0 0 #000;
        }

        .carousel-indicators li, .carousel-indicators li.active {
            width: 10px;
            height: 10px;
            margin: 1px 3px;
            border-radius: 50%;
        }
        .carousel-indicators li {
            background: #999;
            border-color: transparent;
            box-shadow: inset 0 2px 1px rgba(0,0,0,0.2);
        }
        .carousel-indicators li.active {
            /*background: #555;       */
            box-shadow: inset 0 2px 1px rgba(0,0,0,0.2);
        }

        .carousel-indicators li.active {
            background: #08B99F;
            box-shadow: inset 0 2px 1px rgba(0,0,0,0.2);
            /* font-size: 42px; */
            height: 15px;
            width: 15px;
            margin: -1px 3px;
        }

        .triangle {
            position: relative;
            /*margin: 3em;*/
            padding: 1em;
            box-sizing: border-box;
            background: #fff;
            border-radius: 15px;
            box-shadow: 8px 7px 12px 3px #ccc;
        }
        .triangle::after{
            content: "";
            position: absolute;
            width: 0;
            height: 0;
            margin-left: -0.5em;
            bottom: -2em;
            left: 50%;
            box-sizing: border-box;

            border: 1em solid black;
            border-color: transparent transparent #fff #fff;

            transform-origin: 0 0;
            transform: rotate(-45deg);

            box-shadow: -3px 7px 3px 0 #ccc;
        }

        .triangle--problem {
            position: relative;
            margin: 5em;
            padding: 1em;
            box-sizing: border-box;

            background: #bada55;

            box-shadow: 0px 3px 3px 0 rgba(0, 0, 0, 0.4);

        }
        .triangle--problem::after {
            content: "";
            position: absolute;
            width: 0;
            height: 0;
            margin-left: -0.75em;
            bottom: -3em;
            left: 50%;
            box-sizing: border-box;

            border: 1.5em solid black;
            border-color: #bada55 transparent transparent transparent;

            box-shadow: 0px 3px 3px 0 rgba(0, 0, 0, 0.4);
        }

        .navbar {
            min-height: 90px;
            padding-top:20px;
        }

        .item {
            padding: 0px;
            /*border-bottom: 1px solid #eeeeee;*/
        }

        .jumbotron {
            color: #F9F9F9;
            background-image: url("images/index_image/main_header.jpg");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            /*height: 85vh;*/
            height: 50em;
            padding-bottom: 0px;
            margin-bottom: 0px;
        }

        .headings{
            font-weight: normal;
            color:#4b4b4b;
        }

        footer {
            color: #ffffff;
            padding: 20px 0;
            /*background-color: #079FD0;*/
            background-image:url('images/backgrounds/footer.png') !important;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        .bannerPanel{
            border-radius: 15px;padding: 0px;
        }

        @media only screen and (max-width: 992px){
            #banner{
                height: 0vh;
            }
        }

        .fa-check,.fa-times{
            font-size: 20px !important
        }
    </Style>

    <body>
        <div class="main-wrapper">
            <!-- Banner -->
            <section id="banner" style="min-height: 70px">
                <!--full page slider-->
                <!--   <div class="animated fadeIn" id="maximage">
                      <img src="images/slides/main.png" alt=""/>
                  </div> -->

                <!-- Navbar-->
                <nav class="navbar navbar-inverse" id="navbar">
                    <div class="container">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-navguest" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand hidden-xs" href="/">
                                <img src="images/netpro_logo.png" alt="Telstra Blog">
                            </a>

                            <a class="navbar-brand visible-xs" href="/">
                                <img src="images/netpro_logo.png" alt="Telstra Blog" style="width:80% !important;">
                            </a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="collapse-navguest">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="/"><span class="navHover">Home</span></a></li>
                                <li><a href="#initial"><span class="navHover">Features</span></a></li>
                                <li><a href="#whyNetpro"><span class="navHover">Why NetPro</span></a></li>
                                <li><a href="#pricing"><span class="navHover">Pricing</span></a></li>
                                <!-- <li><a href="/signup.php"><span class="navHover">Signup</span></a></li> -->
                                <li class="hidden-xs hidden-md"><a data-toggle = 'modal' data-target = '#LoginModal'><span class="btn btn-success  loginBtn">Login</span></a></li>
                                <li class="visible-xs visible-md"><a data-toggle = 'modal' data-target = '#LoginModal'>Login</a></li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>
            </section>

            <div class="jumbotron jumbotron-fluid">
                <div id='page-content'>
                    <div id="bannerText">
                        <h1 class="title text-center headingText">Your online NPS tracking tool</h1><br/><br/>
                        <h2 class="text-center white-text subHeadingText">
                            Easily monitor customer advocacy results across your business.<br/>Drill down to individual staff level, with this<br/>dynamic reporting platform.
                        </h2>
                        <br/><br/>
                        <div class="panel panel-default col-lg-4 col-lg-offset-4 col-sm-offset-4 col-sm-6 col-md-6 col-md-offset-3 col-xs-12 bannerPanel">
                            <div class="panel-heading headingTrialPanel">
                                <h2 class="text-center uppercase trialText">SEE NETPRO IN ACTION</h2></div>
                            <div class="panel-body" style="padding:15px 15px 25px 15px">
                                <h3 class="text-center subheadingParagraph">It's quick to set-up, with no lock-in contract &#8212;<br/> so get NetPro for your team today and<br/> see how easy NPS reporting can be.</h3>
                                <a href="https://netpro.bsbo.com.au/signup.php" class="col-lg-10 col-lg-offset-1"><button type="button" class="btn btn-success trialBtn">Click here to activate</button></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6" style="margin-top:2%">

                    </div>
                </div>
            </div>




        </div>
    </div>


</section>




<!-- Footer -->
<footer class="footer-below text-center" id="footer">
    <!-- <div class="container">
        Copyright © <?php echo date('Y') ?> <strong>NetPro </strong>All rights Reserved.
    </div> -->
    <div class="container text-center" style="padding-top:0px;">
        <h4 class="pull-left white-text footer-text">© Azenko Pty Ltd <?php echo date('Y'); ?></h4><h4 class="pull-right white-text footer-text" style="font-weight: normal;"> <a style="color:#fff" href="#" data-toggle="modal" data-target="#privacy">Privacy Policy</a> | Terms & Conditions</h4>
    </div>
</footer>
</div>
</body>


<!--Privacy policy modal-->
<!-- Modal -->
<div id="privacy" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-body" style="height: 700px;overflow-y: scroll;">
                <h3>Privacy Policy</h3>
                <ul class="a">
                    <li>Azenko Pty Ltd A.C.N. 626 107 739 (Azenko) provides a wide range of IT products and services for small businesses specialising in a live reporting tools covering sales, stock and financials directly from a customer’s point of sale (POS) system, or that display a net promoter score.<br/>
                        By using, applying or signing up for any products or services with Azenko, including through mobile devices, you accept the terms of this Privacy Policy and consent to our collection, use, disclosure and retention of your information as described herein (including to contact you) and for all other purposes permitted under applicable personal information privacy statutes, credit bureau reporting rules, anti-spam legislation and consumer protection laws.</li>
                    <li>
                        <strong>1.  Introduction</span></strong>
                    </li>
                    <li>
                        <span class="listNumber">1.1</span>  We are committed to safeguarding the privacy of our website visitors and service users.
                    </li>
                    <li>
                        <span class="listNumber">1.2</span>  This policy applies where we are acting as a data controller with respect to the personal data of our website visitors and service users; in other words, where we determine the purposes and means of the processing of that personal data.
                    </li>
                    <li>
                        <span class="listNumber">1.3</span>  We use cookies on our website. Insofar as those cookies are not strictly necessary for the provision of our website and services, we will ask you to consent to our use of cookies when you first visit our website.
                    </li>
                    <li>
                        <span class="listNumber">1.4</span>  Our website incorporates privacy controls which affect how we will process your personal data. By using the privacy controls, you can specify whether you would like to receive direct marketing communications and limit the publication of your information. You can access the privacy controls via azenko.com.au.
                    </li>
                    <li>
                        <span class="listNumber">1.5</span>  In this policy, "we", "us" and "our" refer to Azenko Pty Ltd A.C.N. 626 107 739.
                    </li>
                    <li><strong>2.  How we collect and use your personal data</strong></li>
                    <li>
                        <span class="listNumber">2.1</span>  In this Section we have set out:
                        <ul>
                            <li>(a) the general categories of personal data that we may process;</li>
                            <li>(b) in the case of personal data that we did not obtain directly from you, the source and specific categories of that data;</li>
                            <li>(c) the purposes for which we may process personal data; and</li>
                            <li>(d) the legal bases of the processing.</li>
                        </ul>
                    </li>
                    <li><span class="listNumber">2.2</span>  We may process data about your use of our website and services ("usage data"). The usage data may include your IP address, geographical location, browser type and version, operating system, referral source, length of visit, page views and website navigation paths, as well as information about the timing, frequency and pattern of your service use. The source of the usage data is our analytics tracking system. This usage data may be processed for the purposes of analysing the use of the website and services. The legal basis for this processing is our legitimate interests, namely monitoring and improving our website and services.</li>
                    <li><span class="listNumber">2.3</span>  We may process your account data ("account data"). The account data may include your name and email address. The source of the account data is you. The account data may be processed for the purposes of operating our website, providing our services, ensuring the security of our website and services, maintaining back-ups of our databases and communicating with you. The legal basis for this processing is our legitimate interests, namely the proper administration of our website and business and the performance of a contract between you and us and/or taking steps, at your request, to enter into such a contract.</li>
                    <li><span class="listNumber">2.4</span>  We may process your information included in your personal profile on our website ("profile data"). The profile data may include your name, address, telephone number, email address, profile pictures, gender, date of birth, relationship status, interests and hobbies, educational details and employment details. The profile data may be processed for the purposes of enabling and monitoring your use of our website and services. The legal basis for this processing is our legitimate interests, namely the proper administration of our website and business and the performance of a contract between you and us and/or taking steps, at you request, to enter into such a contract.</li>
                    <li><span class="listNumber">2.5</span>  We may process information that you post for publication on our website or through our services ("publication data"). The publication data may be processed for the purposes of enabling such publication and administering our website and services. The legal basis for this processing is our legitimate interests, namely the proper administration of our website and business and the performance of a contract between you and us and/or taking steps, at your request, to enter into such a contract.</li>
                    <li><span class="listNumber">2.6</span>  We may process information contained in any enquiry you submit to us regarding goods and/or services ("enquiry data"). The enquiry data may be processed for the purposes of offering, marketing and selling relevant goods and/or services to you. The legal basis for this processing is consent.</li>
                    <li><span class="listNumber">2.7</span>  We may process information relating to our customer relationships, including customer contact information ("customer relationship data"). The customer relationship data may include your name, your employer, your job title or role, your contact details, and information contained in communications between us and you or your employer. The source of the customer relationship data is you or your employer. The customer relationship data may be processed for the purposes of managing our relationships with customers, communicating with customers, keeping records of those communications and promoting our products and services to customers. The legal basis for this processing is our legitimate interests, namely the proper management of our customer relationships.</li>
                    <li><span class="listNumber">2.8</span>  We may process information relating to transactions, including purchases of goods and services, that you enter into with us and/or through our website ("transaction data"). The transaction data may include your contact details, your card details and the transaction details. The transaction data may be processed for the purpose of supplying the purchased goods and services and keeping proper records of those transactions. The legal basis for this processing is the performance of a contract between you and us and/or taking steps, at your request, to enter into such a contract and our legitimate interests, namely the proper administration of our website and business.</li>
                    <li><span class="listNumber">2.9</span>  We may process information that you provide to us for the purpose of subscribing to our email notifications and/or newsletters ("notification data"). The notification data may be processed for the purposes of sending you the relevant notifications and/or newsletters. The legal basis for this processing is the performance of a contract between you and us and/or taking steps, at your request, to enter into such a contract.</li>
                    <li><span class="listNumber">2.10</span>    We may process information contained in or relating to any communication that you send to us ("correspondence data"). The correspondence data may include the communication content and metadata associated with the communication. Our website will generate the metadata associated with communications made using the website contact forms.  The correspondence data may be processed for the purposes of communicating with you and record-keeping. The legal basis for this processing is our legitimate interests, namely the proper administration of our website and business and communications with users.</li>
                    <li><span class="listNumber">2.11</span>    We may process any of your personal data identified in this policy where necessary for the establishment, exercise or defence of legal claims, whether in court proceedings or in an administrative or out-of-court procedure. The legal basis for this processing is our legitimate interests, namely the protection and assertion of our legal rights, your legal rights and the legal rights of others.</li>
                    <li><span class="listNumber">2.12</span>    We may process any of your personal data identified in this policy where necessary for the purposes of obtaining or maintaining insurance coverage, managing risks, or obtaining professional advice. The legal basis for this processing is our legitimate interests, namely the proper protection of our business against risks.</li>
                    <li><span class="listNumber">2.13</span>    In addition to the specific purposes for which we may process your personal data set out in this Section 3, we may also process any of your personal data where such processing is necessary for compliance with a legal obligation to which we are subject, or in order to protect your vital interests or the vital interests of another natural person.</li>
                    <li>
                        <span class="listNumber">2.14</span>    Please do not supply any other person's personal data to us, unless we prompt you to do so.
                    </li>
                    <li><span class="listNumber">2.15</span>    We collect information you provide when you download our app, apply or sign up for an account and when you provide information about yourself and individuals associated with your business as part of our identity verification process. We may collect, without limitation, the following information about you and individuals associated with your business:
                        <ul>
                            <li>•   Identification information, such as your name, email address, mailing address, phone number, photograph, birthdate and passport, driver’s licence, Medicare or other government-issued identification number];</li>
                            <li>•   Financial information, including bank account and payment card numbers;</li>
                            <li>•   Tax information, including withholding allowances and tax filing status; and</li>
                            <li>•   Other historical, contact and demographic information.</li>
                        </ul>
                    <li>We also collect information you upload to or send through our Services, including:</li>
                    <ul style="margin:0 30px;">
                        <li>•   Information about products and services you may sell (including inventory, pricing and other data);</li>
                        <li>•   Information you may provide about you or your business (including appointment, staffing, employee, payroll and third party contact data); and</li>
                        <li>•   Information you may provide to a seller using our Services (including hours worked and other timecard data).</li>
                    </ul>
                    <li>
                        Some of the information we collect is collected pursuant to applicable laws and regulations, including anti-money laundering laws (e.g., the Anti-Money Laundering and Counter-Terrorism Financing Act).
                    </li>
                    <li>We collect information you provide when you participate in contests or promotions offered by us or our partners, respond to our surveys or otherwise communicate with us.</li>
                    </li>
                    <li><strong>3.  Providing your personal data to others</strong></li>
                    <li><span class="listNumber">3.1</span>  We may disclose your personal data to any member of our group of companies (this means our subsidiaries, our ultimate holding company and all its subsidiaries) insofar as reasonably necessary for the purposes, and on the legal bases, set out in this policy.</li>
                    <li><span class="listNumber">3.2</span>  We may disclose your personal data to our insurers and/or professional advisers insofar as reasonably necessary for the purposes of obtaining or maintaining insurance coverage, managing risks, obtaining professional advice, or the establishment, exercise or defence of legal claims, whether in court proceedings or in an administrative or out-of-court procedure.</li>
                    <li><span class="listNumber">3.3</span>  We may disclose personal data to our suppliers or subcontractors insofar as reasonably necessary for us to provide our products and services to you.</li>
                    <li><span class="listNumber">3.4</span>  Financial transactions relating to our website and services are or may be handled by our payment services providers, Xero and Stripe. We will share transaction data with our payment services providers only to the extent necessary for the purposes of processing your payments, refunding such payments and dealing with complaints and queries relating to such payments and refunds. You can find information about the payment services providers' privacy policies and practices at https://www.xero.com/au/about/security/ and https://stripe.com/au/privacy</li>
                    <li><span class="listNumber">3.5</span>  We may disclose your enquiry data to one or more of those selected third party suppliers of goods and services identified on our website for the purpose of enabling them to contact you so that they can offer, market and sell to you relevant goods and/or services. Each such third party will act as a data controller in relation to the enquiry data that we supply to it; and upon contacting you, each such third party will supply to you a copy of its own privacy policy, which will govern that third party's use of your personal data.</li>
                    <li><span class="listNumber">3.6</span>  In addition to the specific disclosures of personal data set out in this Section 4, we may disclose your personal data where such disclosure is necessary for compliance with a legal obligation to which we are subject, or in order to protect your vital interests or the vital interests of another natural person. We may also disclose your personal data where such disclosure is necessary for the establishment, exercise or defence of legal claims, whether in court proceedings or in an administrative or out-of-court procedure.</li>
                    <li><strong>4.  International transfers of your personal data</strong></li>
                    <li><span class="listNumber">4.1</span>  In this section, we provide information about the circumstances in which your personal data may be transferred to countries outside of Australia.</li>
                    <li><span class="listNumber">4.2</span>  We have offices and facilities in Australia and Philippines. Transfers to each of these countries will be protected by appropriate safeguards, namely the use of standard data protection clauses.</li>
                    <li><span class="listNumber">4.3</span>  The hosting facilities for our website are situated in Australia only. </li>
                    <li><span class="listNumber">4.4</span>  You acknowledge that personal data that you submit for publication through our website or services may be available, via the internet, around the world. We cannot prevent the use (or misuse) of such personal data by others.</li>
                    <li><strong>5.  Retaining and deleting personal data</strong></li>
                    <li><span class="listNumber">5.1</span>  This section sets out our data retention policies and procedure, which are designed to help ensure that we comply with our legal obligations in relation to the retention and deletion of personal data.</li>
                    <li><span class="listNumber">5.2</span>  Personal data that we process for any purpose or purposes shall not be kept for longer than is necessary for that purpose or those purposes.</li>
                    <li><span class="listNumber">5.3</span>  We will retain your personal data for a minimum of 7 years for tax purposes.</li>
                    <li><span class="listNumber">5.4</span>  In some cases it is not possible for us to specify in advance the periods for which your personal data will be retained. </li>
                    <li><span class="listNumber">5.5</span>  Notwithstanding the other provisions of this Section 5, we may retain your personal data where such retention is necessary for compliance with a legal obligation to which we are subject, or in order to protect your vital interests or the vital interests of another natural person.</li>
                    <li><strong>6.  Amendments</strong></li>
                    <li><span class="listNumber">6.1</span>  We may update this policy from time to time by publishing a new version on our website.</li>
                    <li><span class="listNumber">6.2</span>  You should check this page occasionally to ensure you are happy with any changes to this policy.</li>
                    <li><span class="listNumber">6.3</span>  We may notify you of changes to this policy by email or through the private messaging system on our website.</li>
                    <li><strong>7.  Your rights</strong></li>
                    <li><span class="listNumber">7.1</span>  In this section, we have summarised the rights that you have under data protection law. Some of the rights are complex, and not all of the details have been included in our summaries. Accordingly, you should read the relevant laws and guidance from the regulatory authorities for a full explanation of these rights.</li>
                    <li>
                        <span class="listNumber">7.2</span>  Your principal rights under data protection law are:
                        <ul>
                            <li>(a) the right to access;</li>
                            <li>(b) the right to rectification;</li>
                            <li>(c) the right to erasure;</li>
                            <li>(d) the right to restrict processing;</li>
                            <li>(e) the right to object to processing;</li>
                            <li>(f) the right to data portability;</li>
                            <li>(g) the right to complain to a supervisory authority; and</li>
                            <li>(h) the right to withdraw consent.</li>
                        </ul>
                    </li>
                    <li><span class="listNumber">7.3</span>  You have the right to confirmation as to whether or not we process your personal data and, where we do, access to the personal data, together with certain additional information. That additional information includes details of the purposes of the processing, the categories of personal data concerned and the recipients of the personal data. Providing the rights and freedoms of others are not affected, we will supply to you a copy of your personal data. The first copy will be provided free of charge, but additional copies may be subject to a reasonable fee. You can request a copy of your personal data by emailing contactus@azenko.com.au.</li>
                    <li><span class="listNumber">7.4</span>  You have the right to have any inaccurate personal data about you rectified and, taking into account the purposes of the processing, to have any incomplete personal data about you completed.</li>
                    <li><span class="listNumber">7.5</span>  In some circumstances you have the right to the erasure of your personal data without undue delay. Those circumstances include: the personal data are no longer necessary in relation to the purposes for which they were collected or otherwise processed; you withdraw consent to consent-based processing; you object to the processing under certain rules of applicable data protection law; the processing is for direct marketing purposes; and the personal data have been unlawfully processed. However, there are exclusions of the right to erasure. The general exclusions include where processing is necessary: for exercising the right of freedom of expression and information; for compliance with a legal obligation; or for the establishment, exercise or defence of legal claims.</li>
                    <li><span class="listNumber">7.6</span>  In some circumstances you have the right to restrict the processing of your personal data. Those circumstances are: you contest the accuracy of the personal data; processing is unlawful but you oppose erasure; we no longer need the personal data for the purposes of our processing, but you require personal data for the establishment, exercise or defence of legal claims; and you have objected to processing, pending the verification of that objection. Where processing has been restricted on this basis, we may continue to store your personal data. However, we will only otherwise process it: with your consent; for the establishment, exercise or defence of legal claims; for the protection of the rights of another natural or legal person; or for reasons of important public interest.</li>
                    <li><span class="listNumber">7.7</span>  You have the right to object to our processing of your personal data on grounds relating to your particular situation, but only to the extent that the legal basis for the processing is that the processing is necessary for: the performance of a task carried out in the public interest or in the exercise of any official authority vested in us; or the purposes of the legitimate interests pursued by us or by a third party. If you make such an objection, we will cease to process the personal information unless we can demonstrate compelling legitimate grounds for the processing which override your interests, rights and freedoms, or the processing is for the establishment, exercise or defence of legal claims.</li>
                    <li><span class="listNumber">7.8</span>  You have the right to object to our processing of your personal data for direct marketing purposes (including profiling for direct marketing purposes). If you make such an objection, we will cease to process your personal data for this purpose.</li>
                    <li><span class="listNumber">7.9</span>  You have the right to object to our processing of your personal data for scientific or historical research purposes or statistical purposes on grounds relating to your particular situation, unless the processing is necessary for the performance of a task carried out for reasons of public interest.</li>
                    <li>
                        <span class="listNumber">7.10</span>    To the extent that the legal basis for our processing of your personal data is:
                        <ul>
                            <li>(a) consent; or</li>
                            <li>(b) that the processing is necessary for the performance of a contract to which you are party or in order to take steps at your request prior to entering into a contract,
                                and such processing is carried out by automated means, you have the right to receive your personal data from us in a structured, commonly used and machine-readable format. However, this right does not apply where it would adversely affect the rights and freedoms of others.</li>
                        </ul>
                    </li>
                    <li><span class="listNumber">7.11</span> If you consider that our processing of your personal information infringes data protection laws, you have a legal right to lodge a complaint with a supervisory authority responsible for data protection. </li>
                    <li><span class="listNumber">7.12</span> To the extent that the legal basis for our processing of your personal information is consent, you have the right to withdraw that consent at any time. Withdrawal will not affect the lawfulness of processing before the withdrawal.</li>
                    <li><span class="listNumber">7.13</span> You may exercise any of your rights in relation to your personal data by written notice to us at contactus@azenko.com.au, in addition to the other methods specified in this Section 8.</li>
                    <li><strong>8. About cookies</strong></li>
                    <li><span class="listNumber">8.1</span> A cookie is a file containing an identifier (a string of letters and numbers) that is sent by a web server to a web browser and is stored by the browser. The identifier is then sent back to the server each time the browser requests a page from the server.</li>
                    <li><span class="listNumber">8.2</span> Cookies may be either "persistent" cookies or "session" cookies: a persistent cookie will be stored by a web browser and will remain valid until its set expiry date, unless deleted by the user before the expiry date; a session cookie, on the other hand, will expire at the end of the user session, when the web browser is closed.</li>
                    <li><span class="listNumber">8.3</span> Cookies do not typically contain any information that personally identifies a user, but personal information that we store about you may be linked to the information stored in and obtained from cookies.</li>
                    <li><strong>9.  Cookies that we use</strong></li>
                    <li>
                        <span class="listNumber">9.1</span> We use cookies for the following purposes:
                        <ul>
                            <li>(a) authentication - we use cookies to identify you when you visit our website and as you navigate our website;</li>
                            <li>(b) status - we use cookies to help us to determine if you are logged into our website;</li>
                            <li>(c) personalisation - we use cookies to store information about your preferences and to personalise the website for you;</li>
                            <li>(d) security - we use cookies as an element of the security measures used to protect user accounts, including preventing fraudulent use of login credentials, and to protect our website and services generally;</li>
                            <li>(e) advertising - we use cookies to help us to display advertisements that will be relevant to you;</li>
                            <li>(f) analysis - we use cookies to help us to analyse the use and performance of our website and services;</li>
                            <li>(g) cookie consent - we use cookies to store your preferences in relation to the use of cookies more generally.</li>
                        </ul>
                    </li>
                    <li><strong>10. Cookies used by our service providers</strong></li>
                    <li><span class="listNumber">10.1</span> Our service providers use cookies and those cookies may be stored on your computer when you visit our website.</li>
                    <li><span class="listNumber">10.2</span> We use Google Analytics to analyse the use of our website. Google Analytics gathers information about website use by means of cookies. The information gathered relating to our website is used to create reports about the use of our website. Google's privacy policy is available at: https://www.google.com/policies/privacy/.</li>
                    <li><span class="listNumber">10.3</span> We publish Google AdSense interest-based advertisements on our website. These are tailored by Google to reflect your interests. To determine your interests, Google will track your behaviour on our website and on other websites across the web using cookies. We may also publish Google AdSense advertisements on our website. To determine your interests, Google will track your behaviour on our website and on other websites across the web using cookies. This behaviour tracking allows Google to tailor the advertisements that you see on other websites to reflect your interests (but we do not publish interest-based advertisements on our website). You can view, delete or add interest categories associated with your browser by visiting: https://adssettings.google.com. You can also opt out of the AdSense partner network cookie using those settings or using the Network Advertising Initiative's multi-cookie opt-out mechanism at: http://optout.networkadvertising.org. However, these opt-out mechanisms themselves use cookies, and if you clear the cookies from your browser your opt-out will not be maintained. To ensure that an opt-out is maintained in respect of a particular browser, you may wish to consider using the Google browser plug-ins available at: https://support.google.com/ads/answer/7395996.</li>
                    <li><strong>11. Managing cookies</strong></li>
                    <li>
                        <span class="listNumber">11.1</span>    Most browsers allow you to refuse to accept cookies and to delete cookies. The methods for doing so vary from browser to browser, and from version to version. You can however obtain up-to-date information about blocking and deleting cookies via these links:
                        <ul>
                            <li>(a) https://support.google.com/chrome/answer/95647?hl=en (Chrome);</li>
                            <li>(b) https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences (Firefox);</li>
                            <li>(c) http://www.opera.com/help/tutorials/security/cookies/ (Opera);</li>
                            <li>(d) https://support.microsoft.com/en-gb/help/17442/windows-internet-explorer-delete- manage-cookies (Internet Explorer);</li>
                            <li>(e) https://support.apple.com/kb/PH21411 (Safari); and</li>
                            <li>(f) https://privacy.microsoft.com/en-us/windows-10-microsoft-edge-and-privacy (Edge).</li>
                        </ul>
                    </li>
                    <li><span class="listNumber">11.2</span>    Blocking all cookies will have a negative impact upon the usability of many websites.</li>
                    <li><span class="listNumber">11.3</span>    If you block cookies, you will not be able to use all the features on our website or app.</li>
                    <li><strong>12. Our details</strong></li>
                    <li><span class="listNumber">12.1</span>    This website and any associated application is owned and operated by Azenko.</li>
                    <li><span class="listNumber">12.2</span>    We are registered in Australia under registration number 626 107 739, and our registered office is at YK Partners, Level 2, 545 King Street, West Melbourne VIC 3003.</li>
                    <li><span class="listNumber">12.3</span>    Our principal place of business is at Level 1, 157 Given Terrace, Paddington QLD 4064.</li>
                    <li><span class="listNumber">12.4</span>    You can contact us: contactus@azenko.com.au
                        <ul>
                            <li>(a) by post, to the postal address given above;</li>
                            <li>(b)     by telephone, on the contact number published on our website from time to time; or</li>
                            <li>(c) by email, using the email address supplied above and published on our website from time to time.</li>
                        </ul>
                    </li>
                    <li><strong>13. Promotional Communications</strong></li>
                    <li><span class="listNumber">13.1</span>    You may opt out of receiving promotional messages by following the instructions in those messages or by making a request to us using the contact details below. If you decide to opt out, we may still send you non-promotional communications, such as digital receipts you request.</li>
                    <li><strong>14  Data protection officer</strong></li>
                    <li><span class="listNumber">14.1</span>    Our data protection officer's contact details are:  rachel@azenko.com.au.</li>

                </ul>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


<form role="form" id="ChangePassForm"><!-- /modal-header -->
    <!-- Modal transparent -->
    <div class="modal modal-transparent fade" id="ForgotPassModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa fa-shield"></i> NetPro Reset Password</h4>
                </div>
                <div class="modal-body">
                    <blockquote id="greetings">
                        This a one time reset password token only.
                    </blockquote>

                    <div class="well">

                        <div class="form-group">
                            <input type="hidden" name="email" value="<?php echo $_GET['emailaddress']; ?>" />
                            <label class="control-label" for="Password">New Password:</label>
                            <input type="password" class="form-control" id="newPassword" name="newPassword" placeholder="Enter New Password" required minlength="8">
                        </div> <!-- /new password -->

                        <div class="form-group">
                            <label class="control-label" for="Password">Confirm Password:</label>
                            <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" placeholder="Confirm New Password" required minlength="8">
                        </div><!-- /confirm new password -->
                    </div>

                    <strong class="text-center text-success"><i class="fa fa fa-gears"></i> REQUIREMENTS </strong>

                    <ul id="pswd_info"  class="list-group">
                        <li class="list-group-item">
                            Confirm password must <strong class="text-success">match new</strong>
                            <span class="marker" id="match"></span>
                        </li>
                        <li class="list-group-item">
                            With at least <strong class="text-success">one letter</strong>
                            <span class="marker"  id="letter"></span>
                        </li>
                        <li class="list-group-item">
                            With at least <strong class="text-success">one capital letter</strong>
                            <span class="marker"  id="capital"></span>
                        </li>
                        <li class="list-group-item">
                            With at least <strong class="text-success">one number</strong>
                            <span class="marker"  id="number"></span>
                        </li>
                        <li class="list-group-item">
                            Must be at least <strong class="text-success">8 characters</strong>
                            <span class="marker"  id="length"></span>
                        </li>
                    </ul>

                    <div id="ajaxmsgs"></div>  <!-- ajaxmsg div -->
                </div>

                <div class="modal-footer">
                    <input type="submit" style="visibility: hidden"/>
                    <button type="button" class="btn btn-sm btn-info" data-dismiss="modal" id="btnClose" style="display:none">Continue</button>
                    <button type="button" class="btn btn-sm btn-warning"  id="btnCancel"  >Cancel</button>
                    <button type="button" class="btn btn-sm btn-success"  id="btnSubmit"  >Submit</button>
                </div>
            </div>
        </div>
    </div>

</form>
<?php
include('db/connection.php');
$userEmail = $_GET['emailaddress'];
$token = $_GET['token'];
$checkExpired = "SELECT DATEDIFF(NOW(),f_forgot_token_date) as DateDiff ,f_forgot_token from t_userlogin where f_UserName = '{$userEmail}' AND f_forgot_token_date IS NOT NULL ";
$result = mysql_query($checkExpired, $connection);
$data = mysql_fetch_array($result);
//var_dump($data); // . '$here';
if (($dateDiff > 0 || is_null($dateDiff)) && $data['f_forgot_token'] != $token) {
    echo "<input type=\"hidden\" value=\"tokenexpired\" id=\"checkExpired\" >";
//exit;
}
?>

<!-- Modal transparent -->
<div class="modal modal-transparent fade" id="ExpiredPassModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa fa-shield"></i> NetPro Reset Password</h4>
            </div>

            <div class="modal-body">
                <blockquote id="greetings">
                    Token already expired or something went wrong with your request.
                    <br> Please contact us at <a href="mailto:support@azenko.com.au?subject=Change Password Assistance&body=Messsage">support@azenko.com.au</a>
                    <?php //print_r($_GET); ?>
                </blockquote>


                <div class="modal-footer">
                    <input type="submit" style="visibility: hidden"/>
                    <button type="button" class="btn btn-sm btn-warning"  id="btnCancelExpired"  >Close</button>
                </div>
            </div>
        </div>
    </div>
</div>




<!--Jquery-->
<script  src="includes/js/jquery1.11.1.min.js" type="text/javascript"></script>
<!--Full Slider-->
<script src="includes/js/jquery.cycle.all.js" type="text/javascript"></script>
<script src="includes/js/jquery.maximage.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="includes/js/bootstrap.min.js" type="text/javascript"></script>


<script type="text/javascript">
    $(document).ready(function() {
        if ($("#checkExpired").length != 0 && $("#checkExpired").attr('value') == 'tokenexpired') {
            $('#ExpiredPassModal').modal({
                backdrop: 'static',
                keyboard: false
            }, 'show');
            //  $('#ForgotPassModal').hide();

        } else {
            $('#ForgotPassModal').modal({
                backdrop: 'static',
                keyboard: false
            }, 'show');
        }

        // $('#pswd_info').hide();


        // .modal-backdrop classes

        $(".modal-transparent").on('show.bs.modal', function() {
            setTimeout(function() {
                $(".modal-backdrop").addClass("modal-backdrop-transparent");
            }, 0);
        });
        $(".modal-transparent").on('hidden.bs.modal', function() {
            $(".modal-backdrop").addClass("modal-backdrop-transparent");
        });


        var concheck = false;
        //  var curcheck = false;
        var verification = false;
        var curError = '';
        var conError = '';
        var button = $('#btnSubmit');


        var conPass = {old: null, match: null, length: null, capital: null, letter: null, number: null};

        $('#confirmPassword,#newPassword').keyup(function(e) {
            var pass1 = $('#newPassword').val();
            var pass2 = $('#confirmPassword').val();
            var div = $('#passwordStrength');
            //var params = 'log_password_n=' + pass1;



            // if (pass1.lenght > 0 || pass2.lenght > 0) {
            if (pass1 != pass2) {
                $('#match').removeClass('fa fa-check').addClass('fa fa-times');
                conPass.match = false;
            } else if (pass1 == pass2) {
                $('#match').removeClass('fa fa-times').addClass('fa fa-check');
                conPass.match = true;
            }


            //validate the length
            if (pass1.length < 8) {
                $('#length').removeClass('fa fa-check').addClass('fa fa-times');
                conPass.length = false;
            } else {
                $('#length').removeClass('fa fa-times').addClass('fa fa-check');
                conPass.length = true;

            }
            //validate letter
            if (pass1.match(/[A-z]/)) {
                $('#letter').removeClass('fa fa-times').addClass('fa fa-check');
                conPass.letter = true;
            } else {
                $('#letter').removeClass('fa fa-check').addClass('fa fa-times');
                conPass.letter = false;
            }

            //validate capital letter
            if (pass1.match(/[A-Z]/)) {
                $('#capital').removeClass('fa fa-times').addClass('fa fa-check');
                conPass.capital = true;
            } else {
                $('#capital').removeClass('fa fa-check').addClass('fa fa-times');
                conPass.capital = false;
            }

            //validate number
            if (pass1.match(/\d/)) {
                $('#number').removeClass('fa fa-times').addClass('fa fa-check');
                conPass.number = true;
            } else {
                $('#number').removeClass('fa fa-check').addClass('fa fa-times');
                conPass.number = false;
            }
            e.preventDefault();
        });

        $("input").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
                $("#btnSubmit").click();
            }
        });

        $('#confirmPassword,#newPassword').keydown(function(e) { // Dont allow users to enter spaces for their username and passwords
            if (e.which == 32) {
                return false;
            }
        });

        $('#btnSubmit').click(function(event) {
            if ($("#ChangePassForm")[0].checkValidity()) {
                var url = 'admin/function/SubFunctionCaller.php?module=ChangePassFromForget&';
                var mydata = $("#ChangePassForm").serialize();

                if (validateCheckPass(conPass)) {
                    $.ajax({
                        url: url,
                        type: 'get',
                        data: mydata,
                        dataType: 'json',
                        success: function(result) {
                            document.getElementById('ajaxmsgs').style.display = 'block';
                            if (result.type == 'Success') {
                                $('#ajaxmsgs').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Awesome!</strong> " + result.message + "</div>");
                                $('#btnClose').show();
                                $('#btnSubmit,#btnCancel').css("display", "none");

                            } else {
                                $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Snap!</strong> " + result.message + "</div>");
                            }

                        },
                        error: function(result) {

                            document.getElementById('ajaxmsgs').style.display = 'block';
                            $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");

                        }

                    });
                } else {
                }

            } else {
                $("#ChangePassForm").find(':submit').click();
            }
            event.preventDefault();

        }); // ajax button form submit

        $("#btnCancelExpired").click(function(event) {
            window.location.href = "index.php";
            // console.log('called');
        });

        $("#btnClose").click(function(event) {
            var urlTogo = 'index.php';
            window.location.href = urlTogo;
        });

        function validateCheckPass(obj) {
            var returnValue = new Array();
            var thisValue = '';
            $.each(obj, function(key, value) {
                if (value == false) {
                    returnValue.push(false);
                } else {
                    returnValue.push(true);
                }
            });
            if (jQuery.inArray(false, returnValue) !== -1) {
                thisValue = false;
            } else {
                thisValue = true;
            }

            return thisValue;
        }


    });
</script>
</html>


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120818313-3"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-120818313-3');
</script>









