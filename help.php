<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; }
    .embed-container iframe,
    .embed-container object,
    .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }
</style>

<div class="container" id="HelpPage">
    <h4 class="page-header"><i class="fa fa-question-circle fa-fw"></i>NetPro Help</h4>
    <br>
    <div class='main-content'>
        <table id="tutorial" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
            <thead>
                <tr>
                    <th style="width:20%; text-align: center;">Tutorial</th>
                    <th>Videos</th>
                </tr>
            </thead>
<!--            <tfoot>
                <tr>
                    <th style="width:20%;  text-align: center;">Tutorial</th>
                    <th>Link</th>
                </tr>
            </tfoot>-->
            <tbody>
                <tr>
                    <td> Add new Employee Tutorial </td>
                    <td>
                        <div class='embed-container'>
                            <iframe width="1280" height="720" src="https://www.youtube.com/embed/-sz6vVI7WQY?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td> Extract Medallia Files & Upload to NetPro Tutorial </td>
                    <td>
                        <div class='embed-container'>
                            <iframe width="1280" height="720" src="https://www.youtube.com/embed/xPdpcHYm6Hg?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td> Update Survey Tutorial </td>
                    <td>
                        <div class='embed-container'>
                            <iframe width="1280" height="720" src="https://www.youtube.com/embed/ZS818TYtpBA?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td> Update Survey Tutorial </td>
                    <td>
                        <div class='embed-container'>
                            <iframe width="1280" height="720" src="https://www.youtube.com/embed/ZS818TYtpBA?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td> Summary Page Intro</td>
                    <td>
                        <div class='embed-container'>
                            <iframe width="1280" height="720"   src="https://www.youtube.com/embed/G9i1MNgUHKE?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td> TLS Store MTD Target</td>
                    <td>
                        <div class='embed-container'>
                            <iframe width="1280" height="720"  src="https://www.youtube.com/embed/uinsFnjycLk?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td> TLS Store MTD Tiers</td>
                    <td>
                        <div class='embed-container'>
                            <iframe width="1280" height="720"  src="https://www.youtube.com/embed/1UuAUE5HU6I?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
</div>



<script type="text/javascript">
    $(document).ready(function() {
        $('#tutorial').DataTable({
            "paging": false,
            "ordering": false,
            "info": false,
            bFilter: false, bInfo: false
        });
    });
</script>

