<?php
sleep(2);
error_reporting(0);

include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/nps_sso/function/NPSFunction.php');
session_start();
$shared = $_GET['shared'];
$empID = $_GET['empID'];
$profileID = $_SESSION['UserProfileID'];
$role = $_SESSION['UserRoleID'];
$to = date("Y-m-d", strtotime($_GET['to']));
$from = date("Y-m-d", strtotime($_GET['from']));
//$storeOwnerID = $_GET['storeOwnerID'];
$storeID = $_GET['storeID'];
$surveyTypeID = $_GET['surveyTypeID'];
$subsurveyTypeID = $_GET['subsurveyTypeID'];

$storeType = $_GET['storeType'];
//
//switch ($role) {
//    case '1':
//        $sqlShared = " ";
//        break;
//    case '2':
//    case '3':
//        $storeOwnerID = $_SESSION['UserProfileID'];
//        break;
//    case '4':
//    case '5':
//        $query = $_SESSION['EmpProfileID'];
////    $type = "StoreLeader";
//        $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
//        left join t_empnumber b on a.f_EmpID = b.f_EmpID
//        left join t_storelist c on b.f_StoreID = c.f_StoreID
//        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
//        $result = mysql_query($sqlGetOwnerID, $connection);
//        while ($row = mysql_fetch_assoc($result)) {
//            $data = $row['f_StoreOwnerUserID'];
//        }
//        $storeOwnerID = $data;
//        break;
//}

  switch ($role) {
        case '1':
            $sqlShared = " ";
            break;
        case '2':
        case '3':
        case '4':
        case '5':           
            $storeOwnerID = $_SESSION['ClientOwnerID'];          
            break;     
    }

$empid = $_SESSION['EmpProfileID'];
$InteractionData = getStoreSurveyNps('no', NULL, $empid, $to, $from, $role, $storeType, $surveyTypeID, $subsurveyTypeID, $storeID);
?>

<script type="text/javascript">
    $(document).ready(function() {
        // prepare modal for new data
        $('a.feedback').click(function(ev) {
            ev.preventDefault();
            var target = $(this).attr('href');

            $("#feedbackmodal .modal-content").load(target, function() {
                $("#feedbackmodal").modal("show");
            });
        });
    });
</script>



<div class="modal fade" id="feedbackmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div> <!-- Feedback Modal -->

<table id="tblSurveyInteractionNps" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
    <thead>
        <tr>
            <th style="width:20px; text-align: center">Rank</th>
            <th>Store</th>
            <th>Survey Name</th>
            <th>Sub Survey Name</th>
            <th>AD</th>
            <th>PA</th>
            <th>D</th>
            <th>Total</th>
            <th>NPS</th>
            <th>Impact</th>


            <th>No Score</th>
            <th>All Surveys</th>

        </tr>
    </thead>
    <tfoot>
        <tr>
            <th style="width:20px; text-align: center">Rank</th>
            <th>Store</th>
            <th>Survey Name</th>
            <th>Sub Survey Name</th>
            <th>AD</th>
            <th>PA</th>
            <th>D</th>
            <th>Total</th>
            <th>NPS</th>
            <th>Impact</th>


            <th>No Score</th>
            <th>All Surveys</th>

        </tr>
    </tfoot>
    <tbody>
        <?php
        foreach ($InteractionData['Interaction'] as $value) {
            //echo "<pre>";
            //print_r($value);
            // exit;
            ?>
            <tr>
                <td style="width:20px; text-align: center"></td>
                <td><?php
                    if ($storeID == "Merged") {
                        echo "Merged";
                        $value['f_StoreID'] = "Merged";
                    } else {
                        echo $value['StoreName'];
                    }
                    ?>
                </td>
                <td><?php echo $value['SurveyName']; ?></td>
                <td><?php echo $value['SubSurveyName']; ?></td>
                <td>

                    <a data-toggle="modal"  class="feedback" href="nps_sso/function/FeedbackModalSurvey.php?rate=advo&StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=interaction&modulator=exlcude&storeOwnerID=<?php echo $storeOwnerID; ?>&surveyTypeID=<?php echo $value['f_SurveyListID']; ?>&subSurveyTypeID=<?php echo $value['f_SubSurveyID']; ?>" data-target="#feedbackmodal">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['InteractionTotalAdvocate']; ?>
                    </a>
                </td>
                <td>
                    <a data-toggle="modal"  class="feedback" href="nps_sso/function/FeedbackModalSurvey.php?rate=pass&StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=interaction&modulator=exlcude&storeOwnerID=<?php echo $storeOwnerID; ?>&surveyTypeID=<?php echo $value['f_SurveyListID']; ?>&subSurveyTypeID=<?php echo $value['f_SubSurveyID']; ?>" data-target="#feedbackmodal">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['InteractionTotalPassive']; ?>
                    </a>
                </td>
                <td>
                    <a data-toggle="modal"  class="feedback" href="nps_sso/function/FeedbackModalSurvey.php?rate=detract&StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=interaction&modulator=exlcude&storeOwnerID=<?php echo $storeOwnerID; ?>&surveyTypeID=<?php echo $value['f_SurveyListID']; ?>&subSurveyTypeID=<?php echo $value['f_SubSurveyID']; ?>" data-target="#feedbackmodal">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['InteractionTotalDetractors']; ?>
                    </a>
                </td>
                <td>
                    <a data-toggle="modal"  class="feedback" href="nps_sso/function/FeedbackModalSurvey.php?StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=interaction&modulator=exlcude&storeOwnerID=<?php echo $storeOwnerID; ?>&surveyTypeID=<?php echo $value['f_SurveyListID']; ?>&subSurveyTypeID=<?php echo $value['f_SubSurveyID']; ?>" data-target="#feedbackmodal">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['InteractionTotalSurvey']; ?>
                    </a>
                </td>
                <td><?php echo $value['InteractionNpsScore']; ?></td>
                <td><?php echo number_format($value['IntImpact'], 2) ?></td>

                <th>
                    <a data-toggle="modal"  class="feedback" href="nps_sso/function/FeedbackModalSurvey.php?rate=noScore&StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=noScoreInteraction&storeOwnerID=<?php echo $storeOwnerID; ?>&surveyTypeID=<?php echo $value['f_SurveyListID']; ?>&subSurveyTypeID=<?php echo $value['f_SubSurveyID']; ?>" data-target="#feedbackmodal">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['InteractionNoScore']; ?>
                    </a>
                </th>
                <th>
                    <a data-toggle="modal"  class="feedback" href="nps_sso/function/FeedbackModalSurvey.php?StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=rawInteraction&storeOwnerID=<?php echo $storeOwnerID; ?>&surveyTypeID=<?php echo $value['f_SurveyListID']; ?>&subSurveyTypeID=<?php echo $value['f_SubSurveyID']; ?>" data-target="#feedbackmodal">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['RawTotal']; ?>
                    </a>
                </th>
    <!--                <th>
                    <a data-toggle="modal"  class="feedback" href="nps_sso/function/FeedbackModalSurvey.php?StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=interaction&action5=blank&storeOwnerID=<?php echo $storeOwnerID; ?>&surveyTypeID=<?php echo $value['f_SurveyListID']; ?>&subSurveyTypeID=<?php echo $value['f_SubSurveyID']; ?>" data-target="#feedbackmodal">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['UnAssigned']; ?>
                    </a>
                </th>-->
            </tr>
        <?php } ?>
    </tbody>
</table>


<script type="text/javascript">
    $(document).ready(function() {
        var t = $('#tblSurveyInteractionNps').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            "order": [[9, "asc"]],
            "aaSorting": [],
            "iDisplayLength": 15,
            "aLengthMenu": [[15, 50, 100, -1], [15, 50, 100, "All"]],
            columnDefs: [
                {sortable: false, targets: -1},
                {"width": "8%", "targets": -1}
            ],
            "oColVis": {
                "buttonText": "Hide"
            },
            initComplete: function() {
                var api = this.api();

                api.columns().indexes().flatten().each(function(i) {
                    var column = api.column(i);
                    var select = $('<select><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function() {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );

                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });

                    column.data().unique().sort().each(function(d, j) {
                        select.append('<option value="' + stripHTML(d).trim() + '">' + stripHTML(d).trim() + '</option>');
                    });
                });
            },
            "autoWidth": false,
            "asort": false,
            "sScrollY": "550px",
//            "sScrollX": true,
            "bScrollCollapse": true,
            "bPaginate": true,
            //"bJQueryUI": true,
            "aoColumnDefs": [
                {"sWidth": "10%", "aTargets": [-1]}
            ],
            responsive: true
        });

        t.on('order.dt search.dt', function() {
            t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        //Remove Heading Sorting
        // $("th").unbind();
        //  $("th").removeClass("sorting");
        //  $("th").removeClass("sorting_asc");
    });


    function stripHTML(dirtyString) {
        var container = document.createElement('div');
        container.innerHTML = dirtyString;
        return container.textContent;
    }
</script>