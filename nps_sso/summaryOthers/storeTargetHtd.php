<?php
session_start();

include($_SERVER['DOCUMENT_ROOT'] . '/nps_sso/function/NPSFunction.php');

if (isset($_GET['reconOwnerID']) && !is_null($_GET['reconOwnerID'])) {
    $profileID = $_GET['reconOwnerID'];
} else {
    $profileID = $_SESSION['UserProfileID'];
}

$to = $_GET['to'];
$from = $_GET['from'];

if (isset($_GET['weekly']) && $_GET['weekly'] == "on") {
    $to = date("Y-m-d");
    // $from = date("Y-m-d", strtotime('4 weeks ago'));
    $from = date("Y-m-d", strtotime('-27 days'));
    $addonStoreName = "wkly";
    // $tableID = "tblInteractionNpsWeekly";
} elseif (isset($_GET['3months']) && $_GET['3months'] == "on") {
    $to = $_GET['to'];
    $from = date("Y-m-01", strtotime('-3 Months'));
    $tableID = "3Months";
} elseif (isset($_GET['htd']) && $_GET['htd'] == "on") {
    $currentMonth = date('m');

    if ($_GET['quarter'] == '2') {
        $from = date("Y-10-01", strtotime("-1 year"));
        $to = date("Y-12-31", strtotime("-1 year"));
    } elseif ($_GET['quarter'] == '3') {
        $from = date("Y-01-01");
        $to = date("Y-03-31");
    } elseif ($_GET['quarter'] == '4') {
        $from = date("Y-04-01");
        $to = date("Y-06-30");
    } elseif ($_GET['quarter'] == '1') {
        $from = date("Y-07-01");
        $to = date("Y-09-30");
    }



    $tableID = "htd";
} elseif (isset($_GET['3mma']) && $_GET['3mma'] == "on") {
    $to = $_GET['to'];
    $from = date("Y-m-01", strtotime('first day of -2 Month', strtotime($_GET['to'])));
    $tableID = "3MMA";
} else {
    // $tableID = "tblInteractionNps";
    $addonStoreName = "";
    if (isset($_GET['from']) && isset($_GET['to'])) {
        $to = $_GET['to'];
        $from = $_GET['from'];
    } else {
        $from = NULL;
        $to = NULL;
    }
}

$shared = $_GET['shared'];
$role = $_GET['role'];
$empID = $_SESSION['EmpProfileID'];
$storeType = $_GET['storeType'];
//Get Employee Store Score Data
$StoreNps = array();

//echo $to . " " . $from . " <br>$tableID to" . $_GET['to'] . " from" . $_GET['from'];
//exit;

$storeNpsData = getStoreNpsOthers($shared, $ownerReconID, $empID, $to, $from, $role, $storeType, $rangeType, "Fixed", "3MMA", "OldQ2");
$storeNpsDataOld = getStoreNpsOthersOld($shared, $ownerReconID, $empID, $to, $from, $role, $storeType, $rangeType, "Fixed", "3MMA", "Q1");


foreach ($storeNpsDataOld['Merged'] as $storeData) {
    $new[$storeData['StoreName']] = array(
        //episode
        'Q1MobileScore' => number_format($storeData['MobileNPS'], npsDecimal()),
        'Q1MobileTier' => $storeData['MobileTier'],
        'Q1MobileTotalSurvey' => $storeData['MobileTotalSurvey'],
        'Q1MobileAdvocates' => $storeData['MobileAdvocates'],
        'Q1MobilePasives' => $storeData['MobilePasives'],
        'Q1MobileDetractors' => $storeData['MobileDetractors'],
        'Q1MobilePoints' => $storeData['MobileTotalPoints'],
        //interaction
//        'InteractionScore' => number_format($storeData['InteractionNPS'], 0),
//        'InteractionTier' => $storeData['InteractionTier'],
//        'InteractionTotalSurvey' => $storeData['InteractionTotalSurvey'],
//        'InteractionAdvocates' => $storeData['InteractionAdvocates'],
//        'InteractionPasives' => $storeData['InteractionPasives'],
//        'InteractionDetractors' => $storeData['InteractionDetractors'],
        //fixed
        'Q1FixedScore' => number_format($storeData['FixedNPS'], npsDecimal()),
        'Q1FixedTier' => $storeData['FixedTier'],
        'Q1FixedTotalSurvey' => $storeData['FixedTotalSurvey'],
        'Q1FixedAdvocates' => $storeData['FixedAdvocates'],
        'Q1FixedPasives' => $storeData['FixedPasives'],
        'Q1FixedDetractors' => $storeData['FixedDetractors'],
        'Q1FixedPoints' => $storeData['FixedTotalPoints'],
        //episode
        'Q1EpisodeScore' => number_format($storeData['EpisodeNPS'], npsDecimal()),
        'Q1EpisodeTier' => $storeData['EpisodeTier'],
        'Q1EpisodeTotalSurvey' => $storeData['EpisodeTotalSurvey'],
        'Q1EpisodeAdvocates' => $storeData['EpisodeAdvocates'],
        'Q1EpisodePasives' => $storeData['EpisodePasives'],
        'Q1EpisodeDetractors' => $storeData['EpisodeDetractors'],
        'Q1PointsTotal' => $storeData['PointsTotal']
    );
}

foreach ($storeNpsData['Merged'] as $storeData) {
    $old[$storeData['StoreName']] = array(
        //episode
        'MobileScore' => number_format($storeData['MobileNPS'], npsDecimal()),
        'MobileTier' => $storeData['MobileTier'],
        'MobileTotalSurvey' => $storeData['MobileTotalSurvey'],
        'MobileAdvocates' => $storeData['MobileAdvocates'],
        'MobilePasives' => $storeData['MobilePasives'],
        'MobileDetractors' => $storeData['MobileDetractors'],
        'MobilePoints' => $storeData['MobileTotalPoints'],
        //interaction
//        'InteractionScore' => number_format($storeData['InteractionNPS'], 0),
//        'InteractionTier' => $storeData['InteractionTier'],
//        'InteractionTotalSurvey' => $storeData['InteractionTotalSurvey'],
//        'InteractionAdvocates' => $storeData['InteractionAdvocates'],
//        'InteractionPasives' => $storeData['InteractionPasives'],
//        'InteractionDetractors' => $storeData['InteractionDetractors'],
        //fixed
        'FixedScore' => number_format($storeData['FixedNPS'], npsDecimal()),
        'FixedTier' => $storeData['FixedTier'],
        'FixedTotalSurvey' => $storeData['FixedTotalSurvey'],
        'FixedAdvocates' => $storeData['FixedAdvocates'],
        'FixedPasives' => $storeData['FixedPasives'],
        'FixedDetractors' => $storeData['FixedDetractors'],
        'FixedPoints' => $storeData['FixedTotalPoints'],
        //episode
        'EpisodeScore' => number_format($storeData['EpisodeNPS'], npsDecimal()),
        'EpisodeTier' => $storeData['EpisodeTier'],
        'EpisodeTotalSurvey' => $storeData['EpisodeTotalSurvey'],
        'EpisodeAdvocates' => $storeData['EpisodeAdvocates'],
        'EpisodePasives' => $storeData['EpisodePasives'],
        'EpisodeDetractors' => $storeData['EpisodeDetractors'],
        'PointsTotal' => $storeData['PointsTotal']
    );
}
$StoreNps = array_merge_recursive($old, $new);

foreach ($StoreNps as $storeKey => $value) {

    $pointsTotalQ1 = 0;
    $pointsPercQ1 = 0;
    $StoreNps[$storeKey]['Q1PointsTotal'] = intval($value['Q1FixedPoints']) + intval($value['Q1MobilePoints']);
    $StoreNps[$storeKey]['Q1PointsPerc'] = $StoreNps[$storeKey]['Q1PointsTotal'] * .5;


    $pointsTotal = 0;
    $pointsPerc = 0;
    $StoreNps[$storeKey]['PointsTotal'] = intval($value['FixedPoints']) + intval($value['MobilePoints']);
    $StoreNps[$storeKey]['PointsPerc'] = $StoreNps[$storeKey]['PointsTotal'] * .5;

    $StoreNps[$storeKey]['BappsScore'] = $StoreNps[$storeKey]['PointsPerc'] + $StoreNps[$storeKey]['Q1PointsPerc'];
    $sortBapps[] = $StoreNps[$storeKey]['PointsPerc'] + $StoreNps[$storeKey]['Q1PointsPerc'];
}

array_multisort($sortBapps, SORT_DESC, $StoreNps);

function clean($string) {
    $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
?>
<center><h5><i>Report Range <bR> Q1: 2019-07-01 - 2019-09-30 <br>
            Q2:  2019-10-01 - 2019-12-31</i></h5></center>
<table class="table-bordered table-hover HtdTarget" style="margin:auto;" width="50%">
    <thead>
        <tr>
            <th></th>
            <th>Metric</th>
            <th>T1</th>
            <th>T2</th>
            <th>T3</th>
            <th>T4</th>
            <th>T5</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th rowspan="2">Q2</th>
            <th>Fixed Home</th>
            <td>&ge; 59</td>
            <td>&ge; 52 and &lt; 59</td>
            <td>&ge; 46 and &lt; 52</td>
            <td>&ge; 40 and &lt; 46</td>
            <td>&lt; 40</td>
        </tr>
        <tr>
            <th>Mobile (on the go)</th>
            <td>&ge; 73</td>
            <td>&ge; 69 and &lt; 73</td>
            <td>&ge; 65 and &lt; 69</td>
            <td>&ge; 60 and &lt; 65</td>
            <td>&lt; 60</td>
        </tr>
        <tr> <th rowspan="2">Q1</th>
            <th>Fixed Home</th>
            <td>&ge; 58</td>
            <td>&ge; 51 and &lt; 58</td>
            <td>&ge; 45 and &lt; 51</td>
            <td>&ge; 39 and &lt; 45</td>
            <td>&lt; 39</td>
        </tr>
        <tr>
            <th>Mobile (on the go)</th>
            <td>&ge; 72</td>
            <td>&ge; 68 and &lt; 72</td>
            <td>&ge; 64 and &lt; 68</td>
            <td>&ge; 59 and &lt; 64</td>
            <td>&lt; 59</td>
        </tr>
    </tbody>
<!--    <tr><td><b><h5>Fixed (Home)</h5></b></td><td>T1 &lt; 39, T2 &ge; 39 and &lt; 45, T3 &ge; 45 and &lt; 51, T4 &ge; 51 and &lt; 58, T5  &ge; 58</td></tr>
    <tr><td><b><h5>Mobile (on the go)<h5></b></td><td>T1 &lt; 59, T2 &ge; 59 and &lt; 64, T3 &ge; 64 and &lt; 68, T4 &ge; 68 and &lt; 72, T5 &ge; 72</td></tr>-->
</table>

<?php if (count($StoreNps) > 0) { ?>
    <div class="row" style="padding-bottom: 15px;">
        <?php
        foreach ($StoreNps as $storeKey => $value) {

            //print_r($value);
//            if (is_null($value['InteractionTotalSurvey'])) {
//                $value['InteractionTotalSurvey'] = 0;
//                $value['InteractionAdvocates'] = 0;
//                $value['InteractionDetractors'] = 0;
//            }
            if (is_null($value['MobileTotalSurvey'])) {

                $value['MobileTotalSurvey'] = 0;
                $value['MobileAdvocates'] = 0;
                $value['MobileDetractors'] = 0;
            }
            if (is_null($value['FixedTotalSurvey'])) {

                $value['FixedTotalSurvey'] = 0;
                $value['FixedAdvocates'] = 0;
                $value['FixedDetractors'] = 0;
            }
            if (is_null($value['EpisodeTotalSurvey'])) {

                $value['EpisodeTotalSurvey'] = 0;
                $value['EpisodeAdvocates'] = 0;
                $value['EpisodeDetractors'] = 0;
            }
            ?>
            <div class='col-lg-6 col-md-4 col-sm-6 col-xs-6' style="text-align: center;margin-top: 32px;">

                <div class="row">

                    <div class="col-lg-12">
                        <table border="3" class="col-lg-12 table-bordered">
                            <tr>
                                <td colspan="3"><h4 class="pull-left col-lg-12 col-md-12"><i class="fa fa-Home fa-fw"></i><?php
                                        /**   $pointsTotalQ1 = 0;
                                          $pointsPercQ1 = 0;
                                          $pointsTotalQ1 = intval($value['Q1FixedPoints']) + intval($value['Q1MobilePoints']);
                                          $pointsPercQ1 = $pointsTotalQ1 * .5;


                                          $pointsTotal = 0;
                                          $pointsPerc = 0;
                                          $pointsTotal = intval($value['FixedPoints']) + intval($value['MobilePoints']);
                                          $pointsPerc = $pointsTotal * .5;

                                          $bappsScore = $pointsPerc + $pointsPercQ1; */
                                        echo $storeKey;
                                        if (isset($_GET['htd']) && $_GET['htd'] == "on") {
                                            echo " (<i>{$value['BappsScore']} Points</i>)";
                                        }
                                        $storeName = clean($storeKey);
                                        ?>
                                    </h4>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><h3>Q1</h3></td>
                                <td><h3>Q2</h3></td>
                            </tr>
                            <tr>
                                <td rowspan="2" style="width:10%">
                                    <SPAN STYLE="writing-mode: vertical-lr;
                                          -ms-writing-mode: tb-rl;
                                          transform: rotate(180deg);">Fixed (Home)
                                    </SPAN>
                                </td>
                                <td style="width:40%">
                                    <h3> <?php echo $value['Q1FixedScore']; ?> </h3>
                                    <p> <?php echo $value['Q1FixedTier']; ?> </p>
                                    <?php if (isset($_GET['htd']) && $_GET['htd'] == "on") { ?>  <p><i><?php echo $value['Q1FixedPoints']; ?> Points </i></p> <?php } ?>
                                </td>
                                <td style="width:40%">
                                    <h3> <?php echo $value['FixedScore']; ?> </h3>
                                    <p> <?php echo $value['FixedTier']; ?> </p>
                                    <?php if (isset($_GET['htd']) && $_GET['htd'] == "on") { ?>  <p><i><?php echo $value['FixedPoints']; ?> Points </i></p> <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td> Survey: <?php echo $value['Q1FixedTotalSurvey']; ?></td>
                                <td> Survey: <?php echo $value['FixedTotalSurvey']; ?></td>
                            </tr>

                        </table>
                        <table border="3" class="col-lg-12 table-bordered" >
                            <tr>
                                <td rowspan="2" style="width:10%">
                                    <SPAN STYLE="writing-mode: vertical-lr;
                                          -ms-writing-mode: tb-rl;
                                          transform: rotate(180deg);">Mobile (OTG)
                                    </SPAN>
                                </td>
                                <td style="width:40%">
                                    <h3> <?php echo $value['Q1MobileScore']; ?> </h3>
                                    <p><?php echo $value['Q1MobileTier']; ?> </p>
                                    <?php if (isset($_GET['htd']) && $_GET['htd'] == "on") { ?>  <p><i><?php echo $value['Q1MobilePoints']; ?> Points </i></p> <?php } ?>
                                </td>
                                <td style="width:40%">
                                    <h3> <?php echo $value['MobileScore']; ?> </h3>
                                    <p><?php echo $value['MobileTier']; ?> </p>
                                    <?php if (isset($_GET['htd']) && $_GET['htd'] == "on") { ?>  <p><i><?php echo $value['MobilePoints']; ?> Points </i></p> <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td> Survey: <?php echo $value['Q1MobileTotalSurvey']; ?></td>
                                <td> Survey: <?php echo $value['MobileTotalSurvey']; ?></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td> Home: <?php echo $value['Q1FixedPoints']; ?> Points
                                    <br> OTG: <?php echo $value['Q1MobilePoints']; ?>  Points

                                    <br> Total: <?php echo $value['Q1PointsTotal']; ?>
                                    <br> x 50 %
                                    <br> = <?php echo $value['Q1PointsPerc']; ?>
                                </td>
                                <td> Home: <?php echo $value['FixedPoints']; ?> Points
                                    <br> OTG: <?php echo $value['MobilePoints']; ?>  Points
                                    <br> Total: <?php echo $value['PointsTotal']; ?>
                                    <br> x 50 %
                                    <br> = <?php echo $value['PointsPerc']; ?>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="3">Bapps Score: <?php echo $value['BappsScore']; ?> </th>
                            </tr>
                        </table >
                        <script type="text/javascript">
                            $(document).ready(function() {


                                Number.prototype.between = function(min, max) {
                                    return this >= min && this <= max;
                                };

                                var NewTotalSurvey2<?php echo $storeName; ?> = <?php echo $value['MobileTotalSurvey']; ?>;
                                var newInterAdvoTotal2<?php echo $storeName; ?> = <?php echo $value['MobileAdvocates']; ?>;
                                var InteractionTotalDetractorTotal2<?php echo $storeName; ?> = <?php echo $value['MobileDetractors']; ?>;
                                var counter2<?php echo $storeName; ?> = 0;
                                var newDetPercentage2<?php echo $storeName; ?> = 0;
                                var newAdvoPercentage2<?php echo $storeName; ?> = 0;
                                var newIntercationNPS2<?php echo $storeName; ?> = 0;
                                var roundedstoreNPS = 0;
                                while (newIntercationNPS2<?php echo $storeName; ?> < 73) {
                                    counter2<?php echo $storeName; ?> += 1;
                                    newAdvoPercentage2<?php echo $storeName; ?> = (newInterAdvoTotal2<?php echo $storeName; ?> / NewTotalSurvey2<?php echo $storeName; ?>) * 100;
                                    newDetPercentage2<?php echo $storeName; ?> = (InteractionTotalDetractorTotal2<?php echo $storeName; ?> / NewTotalSurvey2<?php echo $storeName; ?>) * 100;
                                    newIntercationNPS2<?php echo $storeName; ?> = newAdvoPercentage2<?php echo $storeName; ?> - newDetPercentage2<?php echo $storeName; ?>;
                                    roundedstoreNPS = Math.round(newIntercationNPS2<?php echo $storeName; ?>);
                                    if (roundedstoreNPS < 60) {
                                        $("#Q2MobileT4<?php echo $storeName; ?>").empty();
                                        $("#Q2MobileT4<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                        //        console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
                                    } else if ((roundedstoreNPS).between(60, 64)) {
                                        $("#Q2MobileT3<?php echo $storeName; ?>").empty();
                                        $("#Q2MobileT3<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                        //        console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
                                    } else if ((roundedstoreNPS).between(65, 68)) {
                                        $("#Q2MobileT2<?php echo $storeName; ?>").empty();
                                        $("#Q2MobileT2<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                        ;
                                        //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 2");
                                    } else if ((roundedstoreNPS).between(69, 72)) {
                                        $("#Q2MobileT1<?php echo $storeName; ?>").empty();
                                        $("#Q2MobileT1<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                        //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 1");
                                    }
                                    newInterAdvoTotal2<?php echo $storeName; ?> += 1;
                                    NewTotalSurvey2<?php echo $storeName; ?> += 1;
                                }

                                var NewTotalSurveyFix<?php echo $storeName; ?> = <?php echo $value['FixedTotalSurvey']; ?>;
                                var newInterAdvoTotalFix<?php echo $storeName; ?> = <?php echo $value['FixedAdvocates']; ?>;
                                var FixedTotalDetractorTotalFix<?php echo $storeName; ?> = <?php echo $value['FixedDetractors']; ?>;
                                var counterFix<?php echo $storeName; ?> = 0;
                                var newDetPercentageFix<?php echo $storeName; ?> = 0;
                                var newAdvoPercentageFix<?php echo $storeName; ?> = 0;
                                var newIntercationNPSFix<?php echo $storeName; ?> = 0;
                                var roundedstoreNPS = 0;
                                var ft4 = true;
                                var ft3 = true;
                                var ft2 = true;
                                var ft1 = true;
                                newAdvoPercentageFix<?php echo $storeName; ?> = (newInterAdvoTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                                newDetPercentageFix<?php echo $storeName; ?> = (FixedTotalDetractorTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                                newIntercationNPSFix<?php echo $storeName; ?> = newAdvoPercentageFix<?php echo $storeName; ?> - newDetPercentageFix<?php echo $storeName; ?>;
                                //console.log(newIntercationNPSFix<?php echo $storeName; ?> + 'current');
                                while (newIntercationNPSFix<?php echo $storeName; ?> < 59) {

                                    counterFix<?php echo $storeName; ?> += 1;

                                    newAdvoPercentageFix<?php echo $storeName; ?> = (newInterAdvoTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                                    newDetPercentageFix<?php echo $storeName; ?> = (FixedTotalDetractorTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                                    newIntercationNPSFix<?php echo $storeName; ?> = newAdvoPercentageFix<?php echo $storeName; ?> - newDetPercentageFix<?php echo $storeName; ?>;
                                    roundedstoreNPS = Math.round(newIntercationNPSFix<?php echo $storeName; ?>);
                                    // console.log('Store Name: <?php echo $storeName; ?> '+counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier Int")
                                    if (roundedstoreNPS < 40) {
                                        $("#Q2FixedT4<?php echo $storeName; ?>").empty();
                                        $("#Q2FixedT4<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                        ft4 = false;
                                        //  console.log(counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier 4");
                                    } else if (roundedstoreNPS >= 40 && roundedstoreNPS <= 45) {
                                        $("#Q2FixedT3<?php echo $storeName; ?>").empty();
                                        $("#Q2FixedT3<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                        ft3 = false;
                                        //console.log(counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier 3");
                                    } else if (roundedstoreNPS >= 46 && roundedstoreNPS <= 51) {
                                        $("#Q2FixedT2<?php echo $storeName; ?>").empty();
                                        $("#Q2FixedT2<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                        ft2 = false;
                                        //   console.log(counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier Int");
                                    } else if (roundedstoreNPS >= 52 && roundedstoreNPS <= 58) {
                                        $("#Q2FixedT1<?php echo $storeName; ?>").empty();
                                        $("#Q2FixedT1<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                        ft1 = false;
                                        // console.log(counterFix<?php echo $storeName; ?> + " NPS: " + newIntercationNPSFix<?php echo $storeName; ?> + " for tier 1");
                                    }

                                    newInterAdvoTotalFix<?php echo $storeName; ?> += 1;
                                    NewTotalSurveyFix<?php echo $storeName; ?> += 1;
                                }
                            });
                        </script>
                        <table class="col-lg-12 table-bordered TierTargetHTD" style="padding-bottom: 20px;">
                            <thead>
                                <tr>
                                    <th>Needed</th>
                                    <th>T1</th>
                                    <th>T2</th>
                                    <th>T3</th>
                                    <th>T4</th>
                                    <th>T5</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Fixed (Home)</td>
                                    <td><div id="Q2FixedT1<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="Q2FixedT2<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="Q2FixedT3<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="Q2FixedT4<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="Q2FixedT5<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                </tr>
                                <tr>
                                    <td>Mobility (On the go)</td>
                                    <td><div id="Q2MobileT1<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="Q2MobileT2<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="Q2MobileT3<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="Q2MobileT4<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                    <td><div id="Q2MobileT5<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

<?php } else { ?>
    No Records yet.
<?php } ?>
