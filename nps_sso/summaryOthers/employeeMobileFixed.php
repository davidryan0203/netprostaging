<?php
error_reporting(0);
session_start();
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/dashboard_sso/function/getChamp.php');
if (isset($_GET['weekly']) && $_GET['weekly'] == "on") {
    $to = date("Y-m-d");
    // $from = date("Y-m-d", strtotime('4 weeks ago'));
    $from = date("Y-m-d", strtotime('-27 days'));
    $tableID = "4WeekEmployeeCombinedFixed";
    $reportRange = "4Week";
} elseif (isset($_GET['3mma']) && $_GET['3mma'] == "on") {
    $to = $_GET['to'];
    $from = date("Y-m-01", strtotime('first day of -2 Month', strtotime($_GET['to'])));
    $tableID = "3MMAEmployeeCombinedFixed";
    $reportRange = "3MMA";
} else {
    $reportRange = "MTD";
    $tableID = "MTDEmployeeCombinedFixed";
    if (isset($_GET['from']) && isset($_GET['to'])) {
        $from = date("Y-m-d", strtotime($_GET['from']));
        $to = date("Y-m-d", strtotime($_GET['to']));
    } else {
        $from = NULL;
        $to = NULL;
    }
}
//$StoreChamp = getChampOthers('Fixed','AllEmployees','no',NULL,Null,'RemoveNull',$storeType);

$storeID = $_GET['storeID'];
$reportRange = $_GET['rangeType'];

$StoreChamp = getCombinedOthers($from, $to, $reportRange);

//echo "<pre>";
//var_dump($StoreChamp);
?>

<script type="text/javascript">

    function sortNumbersIgnoreText(a, b, high) {
        var reg = /[+-]?((\d+(\.\d*)?)|\.\d+)([eE][+-]?[0-9]+)?/;
        a = a.match(reg);
        a = a !== null ? parseFloat(a[0]) : high;
        b = b.match(reg);
        b = b !== null ? parseFloat(b[0]) : high;
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    }

    function stripHTML(dirtyString) {
        var container = document.createElement('div');
        container.innerHTML = dirtyString;
        return container.textContent;
    }

    jQuery.extend(jQuery.fn.dataTableExt.oSort, {
        "sort-numbers-ignore-text-asc": function(a, b) {
            return sortNumbersIgnoreText(a, b, Number.POSITIVE_INFINITY);
        },
        "sort-numbers-ignore-text-desc": function(a, b) {
            return sortNumbersIgnoreText(a, b, Number.NEGATIVE_INFINITY) * -1;
        }
    });

    $(document).ready(function() {
        var t = $("#<?php echo $tableID; ?>").DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            "iDisplayLength": 15,
            "aLengthMenu": [[15, 50, 100, -1], [15, 50, 100, "All"]],
            columnDefs: [
//                {sortable: false, targets: -1},
//                {"width": "8%", "targets": -1}

            ],
            "oColVis": {
                "buttonText": "Hide Columns"
            },
            //Footer Sorting
            initComplete: function() {
                var api = this.api();

                api.columns().indexes().flatten().each(function(i) {
                    var column = api.column(i);
                    var select = $('<select><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function() {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );

                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });

                    column.data().unique().sort().each(function(d, j) {
                        select.append('<option value="' + stripHTML(d).trim() + '">' + stripHTML(d).trim() + '</option>');
                    });
                });
            },
            "autoWidth": true,
            "sScrollY": "600px",
            "bScrollCollapse": true,
            "bPaginate": true
        });

        //Remove Heading Sorting
        $("th").unbind();
        $("th").removeClass("sorting");
        $("th").removeClass("sorting_asc");

    });
</script>

<?php if (!is_null($StoreChamp)) { ?>
    <table class="table table-bordered table-responsive" id="<?php echo $tableID; ?>">
        <thead>
            <tr>
                <!--<th style="width:30px; text-align: center;">Rank</th>-->
                <th >Store</th>
                <th >Name</th>
                <th >OTG</th>
                <th>Home</th>
                <th>OTG Volume</th>
                <th>Home Volume</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <!--<th style="width:30px; text-align: center;">Rank</th>-->
                <th >Store</th>
                <th >Name</th>
                <th >OTG</th>
                <th>Home</th>
                <th>OTG Volume</th>
                <th>Home Volume</th>
            </tr>
        </tfoot>
        <?php
        foreach ($StoreChamp as $value) {
//            echo "<pre>";
//            var_dump($value);
//            exit;
            ?>
            <tr>
        <!--                <td style="width:20px; text-align: center;"></td>-->
                <td><?php echo $value['f_StoreListName']; ?></td>
                <td><?php echo $value['EmpName']; ?></td>
                <td><?php echo number_format(is_null($value['MobEmpNPS']) ? ' ' : $value['MobEmpNPS'], npsDecimal()); ?></td>
                <td><?php echo number_format(is_null($value['FixEmpNPS']) ? ' ' : $value['FixEmpNPS'], npsDecimal()); ?></td>
                <td><?php echo $value['MobEmpTotalSurvey']; ?></td>
                <td><?php echo $value['FixEmpTotalSurvey']; ?></td>
        <!--                <td><?php echo number_format($value['EmpEpisode'], npsDecimal()); ?></td>
                <td><?php echo number_format($value['EmpImpact'], 2); ?></td>-->
            </tr>

            <?php
        }
        ?>
    </table>
<?php } else { ?>
    No data to display
<?php } ?>


