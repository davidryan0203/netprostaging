<div class="modal-header feedback">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

    <div class="col-lg-6">
        <h2 id="myModalLabel"><i class="fa fa-paper-plane-o right5px"></i>TLS NPS Changes</h2>
    </div>

</div>

<div class='modal-body' style="overflow-y: scroll; max-height: 72%">
    <center><h4><i>New TLS NPS Gate Q4 (effective April 1, 2020)</i></h4></center>
    <table id="npSnewTBL" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
        <thead>
            <tr>
                <th colspan="2">
                    &ldquo;HOME&rdquo; (Fixed)
                </th>
                <td>&nbsp;</td>
                <th colspan="2">
                    &ldquo;ON THE GO&rdquo; (Mobility)
                </th>
            </tr>
            <tr>
                <th>
                    Applicable 3MMA  <br> Episode NPS
                </th>
                <th bgcolor="#BDE6E7">Percentage</th>

                <td>&nbsp;</td>
                <th>
                    Applicable 3MMA  <br> Episode NPS
                </th>
                <th bgcolor="#BDE6E7">Percentage</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    &lt; 23
                </td>
                <td bgcolor="#BDE6E7">
                    0%
                </td>
                <td>&nbsp;</td>
                <td>
                    &lt; 51
                </td>
                <td bgcolor="#BDE6E7">
                    0%
                </td>
            </tr>
            <tr>
                <td>
                    &ge; 23 and &lt; 41
                </td>
                <td bgcolor="#BDE6E7">
                    60%
                </td>
                <td>&nbsp;</td>
                <td>
                    &ge; 51 and &lt; 61
                </td>
                <td bgcolor="#BDE6E7">
                    60%
                </td>
            </tr>
            <tr>
                <td>
                    &ge; 41 and &lt; 47
                </td>
                <td bgcolor="#BDE6E7">
                    100%
                </td>
                <td>&nbsp;</td>
                <td>
                    &ge; 61 and &lt; 66
                </td>
                <td bgcolor="#BDE6E7">
                    100%
                </td>
            </tr>
            <tr>
                <td>
                    &ge; 47 and &lt; 53
                </td>
                <td bgcolor="#BDE6E7">
                    110%
                </td>
                <td>&nbsp;</td>
                <td>
                    &ge; 66 and &lt; 70
                </td>
                <td bgcolor="#BDE6E7">
                    110%
                </td>
            </tr>
            <tr>
                <td>
                    &ge; 53 and &lt; 60
                </td>
                <td bgcolor="#BDE6E7">
                    120%
                </td>
                <td>&nbsp;</td>
                <td>
                    &ge; 70 and &lt; 74
                </td>
                <td bgcolor="#BDE6E7">
                    120%
                </td>
            </tr>
            <tr>
                <td>
                    &ge; 60
                </td>
                <td bgcolor="#BDE6E7">
                    140%
                </td>
                <td>&nbsp;</td>
                <td>
                    &ge; 74
                </td>
                <td bgcolor="#BDE6E7">
                    140%
                </td>
            </tr>
        </tbody>
    </table><br>
    <center><h4><i>New TLS NPS Gate Q3 (effective January 1, 2020)</i></h4></center>
    <table id="npSnewTBL" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
        <thead>
            <tr>
                <th colspan="2">
                    &ldquo;HOME&rdquo; (Fixed)
                </th>
                <td>&nbsp;</td>
                <th colspan="2">
                    &ldquo;ON THE GO&rdquo; (Mobility)
                </th>
            </tr>
            <tr>
                <th>
                    Applicable 3MMA  <br> Episode NPS
                </th>
                <th bgcolor="#BDE6E7">Points</th>

                <td>&nbsp;</td>
                <th>
                    Applicable 3MMA  <br> Episode NPS
                </th>
                <th bgcolor="#BDE6E7">Points</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    &lt; 23
                </td>
                <td bgcolor="#BDE6E7">
                    5
                </td>
                <td>&nbsp;</td>
                <td>
                    &lt; 51
                </td>
                <td bgcolor="#BDE6E7">
                    10
                </td>
            </tr>
            <tr>
                <td>
                    &ge; 23 and &lt; 41
                </td>
                <td bgcolor="#BDE6E7">
                    10
                </td>
                <td>&nbsp;</td>
                <td>
                    &ge; 51 and &lt; 61
                </td>
                <td bgcolor="#BDE6E7">
                    15
                </td>
            </tr>
            <tr>
                <td>
                    &ge; 41 and &lt; 47
                </td>
                <td bgcolor="#BDE6E7">
                    15
                </td>
                <td>&nbsp;</td>
                <td>
                    &ge; 61 and &lt; 66
                </td>
                <td bgcolor="#BDE6E7">
                    30
                </td>
            </tr>
            <tr>
                <td>
                    &ge; 47 and &lt; 53
                </td>
                <td bgcolor="#BDE6E7">
                    20
                </td>
                <td>&nbsp;</td>
                <td>
                    &ge; 66 and &lt; 70
                </td>
                <td bgcolor="#BDE6E7">
                    40
                </td>
            </tr>
            <tr>
                <td>
                    &ge; 53 and &lt; 60
                </td>
                <td bgcolor="#BDE6E7">
                    25
                </td>
                <td>&nbsp;</td>
                <td>
                    &ge; 70 and &lt; 74
                </td>
                <td bgcolor="#BDE6E7">
                    45
                </td>
            </tr>
            <tr>
                <td>
                    &ge; 60
                </td>
                <td bgcolor="#BDE6E7">
                    30
                </td>
                <td>&nbsp;</td>
                <td>
                    &ge; 74
                </td>
                <td bgcolor="#BDE6E7">
                    50
                </td>
            </tr>
        </tbody>
    </table><br>
    <center><h4><i>TLS NPS Gate Q2 (effective October 1, 2019)</i></h4></center>
    <table id="npSnewTBL" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
        <thead>
            <tr>
                <th colspan="2">
                    &ldquo;HOME&rdquo; (Fixed)
                </th>
                <td>&nbsp;</td>
                <th colspan="2">
                    &ldquo;ON THE GO&rdquo; (Mobility)
                </th>
            </tr>
            <tr>
                <th>
                    Applicable 3MMA  <br> Episode NPS
                </th>
                <th bgcolor="#BDE6E7">Points</th>

                <td>&nbsp;</td>
                <th>
                    Applicable 3MMA  <br> Episode NPS
                </th>
                <th bgcolor="#BDE6E7">Points</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    &lt; 22
                </td>
                <td bgcolor="#BDE6E7">
                    10
                </td>
                <td>&nbsp;</td>
                <td>
                    &lt; 50
                </td>
                <td bgcolor="#BDE6E7">
                    10
                </td>
            </tr>
            <tr>
                <td>
                    &ge; 22 and &lt; 40
                </td>
                <td bgcolor="#BDE6E7">
                    15
                </td>
                <td>&nbsp;</td>
                <td>
                    &ge; 50 and &lt; 60
                </td>
                <td bgcolor="#BDE6E7">
                    15
                </td>
            </tr>
            <tr>
                <td>
                    &ge; 40 and &lt; 46
                </td>
                <td bgcolor="#BDE6E7">
                    30
                </td>
                <td>&nbsp;</td>
                <td>
                    &ge; 60 and &lt; 65
                </td>
                <td bgcolor="#BDE6E7">
                    30
                </td>
            </tr>
            <tr>
                <td>
                    &ge; 46 and &lt; 52
                </td>
                <td bgcolor="#BDE6E7">
                    40
                </td>
                <td>&nbsp;</td>
                <td>
                    &ge; 65 and &lt; 69
                </td>
                <td bgcolor="#BDE6E7">
                    40
                </td>
            </tr>
            <tr>
                <td>
                    &ge; 52 and &lt; 59
                </td>
                <td bgcolor="#BDE6E7">
                    45
                </td>
                <td>&nbsp;</td>
                <td>
                    &ge; 69 and &lt; 73
                </td>
                <td bgcolor="#BDE6E7">
                    45
                </td>
            </tr>
            <tr>
                <td>
                    &ge; 59
                </td>
                <td bgcolor="#BDE6E7">
                    50
                </td>
                <td>&nbsp;</td>
                <td>
                    &ge; 73
                </td>
                <td bgcolor="#BDE6E7">
                    50
                </td>
            </tr>
        </tbody>
    </table><br>
<!--    <center><h4><i>TLS NPS Gate (effective July 1, 2019)</i></h4></center>
    <table id="npSnewTBL" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
        <thead>
            <tr>
                <th colspan="2">
                    &ldquo;HOME&rdquo; (Fixed)
                </th>
                <td>&nbsp;</td>
                <th colspan="2">
                    &ldquo;ON THE GO&rdquo; (Mobility)
                </th>
            </tr>
            <tr>
                <th>
                    Applicable 3MMA  <br> Episode NPS
                </th>
                <th bgcolor="#BDE6E7">Non-Sales  <br>Performance <br>Payment Rate</th>

                <td>&nbsp;</td>
                <th>
                    Applicable 3MMA  <br> Episode NPS
                </th>
                <th bgcolor="#BDE6E7">Non-Sales  <br>Performance <br> Payment Rate</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    &lt; 39
                </td>
                <td bgcolor="#BDE6E7">
                    60%
                </td>
                <td>&nbsp;</td>
                <td>
                    &lt; 59
                </td>
                <td bgcolor="#BDE6E7">
                    60%
                </td>
            </tr>
            <tr>
                <td>
                    &ge; 39 and &lt; 45
                </td>
                <td bgcolor="#BDE6E7">
                    100%
                </td>
                <td>&nbsp;</td>
                <td>
                    &ge; 59 and &lt; 64
                </td>
                <td bgcolor="#BDE6E7">
                    100%
                </td>
            </tr>
            <tr>
                <td>
                    &ge; 45 and &lt; 51
                </td>
                <td bgcolor="#BDE6E7">
                    110%
                </td>
                <td>&nbsp;</td>
                <td>
                    &ge; 64 and &lt; 68
                </td>
                <td bgcolor="#BDE6E7">
                    110%
                </td>
            </tr>
            <tr>
                <td>
                    &ge; 51 and &lt; 58
                </td>
                <td bgcolor="#BDE6E7">
                    120%
                </td>
                <td>&nbsp;</td>
                <td>
                    &ge; 68 and &lt; 72
                </td>
                <td bgcolor="#BDE6E7">
                    120%
                </td>
            </tr>
            <tr>
                <td>
                    &ge; 58
                </td>
                <td bgcolor="#BDE6E7">
                    140%
                </td>
                <td>&nbsp;</td>
                <td>
                    &ge; 72
                </td>
                <td bgcolor="#BDE6E7">
                    140%
                </td>
            </tr>
        </tbody>
    </table><br>
    <center><h4><i>TLS NPS Gate</i></h4></center>
    <table id="npSnewTBL" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
        <thead>
            <tr>
                <th colspan="2">
                    EPISODE NPS GATES&ldquo;HOME&rdquo; (Fixed)
                </th>
                <th bgcolor="#2B7AF3">Points</th>
                <td>&nbsp;</td>
                <th colspan="2">
                    EPISODE NPS GATES<br /> &ldquo;ON THE GO&rdquo; (Mobility)
                </th>
                <th bgcolor="#2B7AF3">Points</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    &lt; 16
                </td>
                <td>
                    60%
                </td>
                <td bgcolor="#BDE6E7">
                    0
                </td>
                <td>&nbsp;</td>
                <td>
                    &lt; 44
                </td>
                <td>
                    60%
                </td>
                <td bgcolor="#BDE6E7">
                    0
                </td>
            </tr>
            <tr>
                <td>
                    &ge; 16 and &lt; 31
                </td>
                <td>
                    80%
                </td>
                <td bgcolor="#BDE6E7">
                    10
                </td>
                <td>&nbsp;</td>
                <td>
                    &ge; 44 and &lt; 53
                </td>
                <td>
                    80%
                </td>
                <td bgcolor="#BDE6E7">
                    10
                </td>
            </tr>
            <tr>
                <td>
                    &ge; 31 and &lt; 41
                </td>
                <td>
                    100%
                </td>
                <td bgcolor="#BDE6E7">
                    20
                </td>
                <td>&nbsp;</td>
                <td>
                    &ge; 53 and &lt; 62
                </td>
                <td>
                    100%
                </td>
                <td bgcolor="#BDE6E7">
                    20
                </td>
            </tr>
            <tr>
                <td>
                    &ge; 41 and &lt; 46
                </td>
                <td>
                    120%
                </td>
                <td bgcolor="#BDE6E7">
                    30
                </td>
                <td>&nbsp;</td>
                <td>
                    &ge; 62 and &lt; 67
                </td>
                <td>
                    120%
                </td>
                <td bgcolor="#BDE6E7">
                    30
                </td>
            </tr>
            <tr>
                <td>
                    &ge; 46
                </td>
                <td>
                    140%
                </td>
                <td bgcolor="#BDE6E7">
                    40
                </td>
                <td>&nbsp;</td>
                <td>
                    &ge; 67
                </td>
                <td>
                    140%
                </td>
                <td bgcolor="#BDE6E7">
                    40
                </td>
            </tr>
        </tbody>
    </table><br>-->
    <center><h4><i>Current and Future Episode NPS Model </i></h4></center>
    <table id="npSnewTBL" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
        <thead>
            <tr>
                <th>
                    Survey Type
                </th>
                <th>
                    TCW New Episode NPS - Construct
                    <br> (From October 1)
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Activation</td>

                <td align="center">&#x2714;</td>
            </tr>
            <tr>
                <td>Billing</td>

                <td align="center">&#x2714;</td>
            </tr>
            <tr>
                <td>Modify</td>

                <td align="center">&#x2714;</td>

            </tr>
            <tr>
                <td>Move</td>

                <td align="center">&#x2714;</td>
            </tr>
            <tr>
                <td>Billing</td>

                <td align="center">&#x2714;<br>(First Bill Only)</td>
            </tr>
            <tr>
                <td>Complaints</td>

                <td align="center">&#x2718;</td>
            </tr>
            <tr>
                <td>Billing Follow Up</td>

                <td align="center">&#x2718;</td>
            </tr>
            <tr>
                <td>Follow Up (prepaid/assurance)</td>

                <td align="center">&#x2718;</td>
            </tr>
            <tr>
                <td>Use</td>

                <td align="center">&#x2718;</td>
            </tr>
        </tbody>
    </table><br>
    <center><h4><i>Product and Service – Category Breakdown and Inclusions </i></h4></center>
    <table id="npSnewTBL" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
        <thead>
            <tr>
                <th>
                    Products / Services Included
                </th>
                <th>
                    Home
                </th>
                <th>
                    On The Go
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>ADSL</td>
                <td align="center">&#x2714;</td>
                <td align="center"></td>
            </tr>
            <tr>
                <td>Bundle</td>
                <td align="center">&#x2714;</td>
                <td align="center"></td>
            </tr>
            <tr>
                <td>Cable</td>
                <td align="center">&#x2714;</td>
                <td align="center"></td>
            </tr>
            <tr>
                <td>Dial-Up</td>
                <td align="center">&#x2714;</td>
                <td align="center"></td>
            </tr>
            <tr>
                <td>Fibre Internet</td>
                <td align="center">&#x2714;</td>
                <td align="center"></td>
            </tr>
            <tr>
                <td>Fixed Line</td>
                <td align="center">&#x2714;</td>
                <td align="center"></td>
            </tr>
            <tr>
                <td>Foxtel</td>
                <td align="center">&#x2714;</td>
                <td align="center"></td>
            </tr>
            <tr>
                <td>Foxtel from Telstra</td>
                <td align="center">&#x2714;</td>
                <td align="center"></td>
            </tr>
            <tr>
                <td>Internet</td>
                <td align="center">&#x2714;</td>
                <td align="center"></td>
            </tr>
            <tr>
                <td>NBN</td>
                <td align="center">&#x2714;</td>
                <td align="center"></td>
            </tr>
            <tr>
                <td>TBB</td>
                <td align="center">&#x2714;</td>
                <td align="center"></td>
            </tr>
            <tr>
                <td>T-Box</td>
                <td align="center">&#x2714;</td>
                <td align="center"></td>
            </tr>
            <tr>
                <td>TBS</td>
                <td align="center">&#x2714;</td>
                <td align="center"></td>
            </tr>
            <tr>
                <td>Telstra Platinum</td>
                <td align="center">&#x2714;</td>
                <td align="center"></td>
            </tr>
            <tr>
                <td>Mobile</td>
                <td align="center"></td>
                <td align="center">&#x2714;</td>
            </tr>
            <tr>
                <td>Other (Un-tagged Mobility Surveys)</td>
                <td align="center"></td>
                <td align="center">&#x2714;</td>
            </tr>
            <tr>
                <td>Telstra Air</td>
                <td align="center"></td>
                <td align="center">&#x2714;</td>
            </tr>
            <tr>
                <td>Wireless Broadband</td>
                <td align="center"></td>
                <td align="center">&#x2714;</td>
            </tr>
            <tr>
                <td>Tech Bar</td>
                <td align="center"></td>
                <td align="center">&#x2714;</td>
            </tr>
            <tr>
                <td>Billing (no product tagged)</td>
                <td align="center" colspan="2">Not Included</td>
            </tr>
            <tr>
                <td>Telstra TV</td>
                <td align="center">&#x2714;</td>
                <td align="center"></td>
            </tr>
            <tr>
                <td>Xbox All Access</td>
                <td align="center">&#x2714;</td>
                <td align="center"></td>
            </tr>


        </tbody>
    </table>
</div>

<script>

    $(document).ready(function() {
        $('#npSnewTBL').DataTable({
//            dom: 'lBfrtip',
//            buttons: [
//                'copyHtml5',
//                'excelHtml5',
//                'csvHtml5',
//                'pdfHtml5'
//            ],
            // iDisplayLength: 2,
            "bPaginate": false,
            bFilter: false,
            bInfo: false,
            bSort: false,
            bLengthChange: false
        });
    });</script>