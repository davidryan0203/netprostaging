<?php

ini_set('memory_limit', '2048M');

require $_SERVER["DOCUMENT_ROOT"] . '/db/connection.php';
require $_SERVER["DOCUMENT_ROOT"] . '/nps_sso/function/UploaderFunction.php';
require $_SERVER["DOCUMENT_ROOT"] . '/php-excel/Classes/PHPExcel.php';
require $_SERVER["DOCUMENT_ROOT"] . '/php-excel/Classes/PHPExcel/IOFactory.php';
require $_SERVER["DOCUMENT_ROOT"] . '/functions/FunctionLog.php';

/* ==================================================== 
  Check if a file is uploaded
  =================================================== */
if (isset($_FILES['spreadsheet']) && !is_null($_FILES['spreadsheet'])) {

    //  Check if a file is uploaded
    if ($_FILES['spreadsheet']['name'] != '') {

        $file = "tmp/" . basename($_FILES['spreadsheet']['name']);
        $extension = strtoupper(pathinfo($file, PATHINFO_EXTENSION));

        //  Check file extension
        if ($extension == 'XLSX') {

            //  Move uploaded file to temp folder
            move_uploaded_file($_FILES['spreadsheet']['tmp_name'], $file);

            //  Read spreadsheeet workbook
            try {
                $file_type = PHPExcel_IOFactory::identify($file);
                $objReader = PHPExcel_IOFactory::createReader($file_type);
                $objPHPExcel = $objReader->load($file);
            } catch (Exception $e) {
                die($e->getMessage());
            }

            //  Get worksheet dimensions
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
            $firstHeader = $objPHPExcel->getActiveSheet()->getCell('A3')->getValue();
            $totalRecords = $highestRow - 3;

            //  Check if valid Medallia file by detecting first header and Last Column
            if ($firstHeader === 'Invitation Id' && $highestColumn == 'CH') {

                //  Loop through each row of the worksheet in turn
                $excelarray = array();
                $row = 4;
                while ($row <= $highestRow) {
                    //  Read a row of data into an array
                    $excelarray[$row] = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                    $row++;
                }
            } else {
                $data['type'] = 'failed';
                $data['class'] = "alert alert-danger";
                $data['message'] = "Invalid Medallia excel file! Expected headers didn't matched.";
                unlink($file);
            }
        } else {
            $data['type'] = 'failed';
            $data['class'] = "alert alert-danger";
            $data['message'] = "Invalid Format! Please upload medallia excel file only.";
        }
    } else {
        $data['type'] = 'failed';
        $data['class'] = "alert alert-danger";
        $data['message'] = 'No selected file. Please upload medallia excel file.';
    }
} else {
    $data['type'] = 'success';
    $data['class'] = "alert alert-success";
    $data['message'] = 'Select Medallia file to upload.';
}


/* ========================================================= 
  If All validation passed, check if excel file is not empty
  ======================================================= */
if (!is_null($excelarray)) {

    //  Prepare insert query for t_surveyPD 
    $sqlSurveyPD = "INSERT INTO t_surveypd (
                        f_InvitationID, 
                        f_CustomerDetailsID, 
                        f_SurveyID, 
                        f_TelstraSurveyProgramID, 
                        f_SubSurveyID, 
                        f_EpisodeReferenceNo, 
                        f_EpisodeSourceID, 
                        f_EpisodeStartDate,
                        f_EpisodeEndDate,
                        f_SurveyMethodID,
                        f_AskFieldQuestion, 
                        f_AskRetailQuestion,
                        f_CustomerBillingAccountID, 
                        f_CustomerBusinessUnitID,
                        f_CustomerMBMSegment, 
                        f_Product1, 
                        f_Product2,
                        f_Product3,
                        f_Product4, 
                        f_Product5, 
                        f_Product6,
                        f_Product7, 
                        f_Product8, 
                        f_Product9, 
                        f_Product10,
                        f_ProductID
                        ) 
                    VALUES ";

    //  Prepare insert query for t_surveySD 
    $sqlSurveySDmain = "INSERT INTO t_surveysd (
                                f_SurveyPDID,
                                f_UserID1, 
                                f_UserID2, 
                                f_UserID3, 
                                f_UserID4, 
                                f_UserID5, 
                                f_UserID6, 
                                f_UserID7, 
                                f_UserID8, 
                                f_UserID9, 
                                f_UserID10, 
                                f_UserID11, 
                                f_UserID12, 
                                f_DealerCode,
                                f_StoreID, 
                                f_OriginationChannelID,
                                f_ActivationExtra4, 
                                f_ActivationExtra5, 
                                f_BillFormat, 
                                f_BillMethod, 
                                f_AssuranceLoggingSystem,
                                f_AssuranceArea, 
                                f_AssuranceSubArea,
                                f_AssuranceSymptom,
                                f_ComplaintTIOPriorityLevel, 
                                f_ComplaintArea, 
                                f_ComplaintReason, 
                                f_ComplaintSubReason, 
                                f_ComplaintSource,
                                f_DisconnectionStatus, 
                                f_InteractionLog, 
                                f_SurveyID, 
                                f_DateTimeCreatedMelbourne, 
                                f_DateTimeResponseMelbourne, 
                                f_IssueResolved, f_RecommendRate, 
                                f_CustomerReason, 
                                f_ResolutionPreference, 
                                f_FollowUp, 
                                f_CallbackNumberCombined,
                                f_IssueDescription, 
                                f_LikelihoodToRecommendField, 
                                f_LikelihoodToRecommendRetail, 
                                f_IndustryCode1, f_IndustryCode2, 
                                f_SalesPortfolioCode, 
                                f_SalesGroup, 
                                f_BusinessUltimateCIDN, 
                                f_BusinessMetroRegional, 
                                f_BusinessManagedStatus, 
                                f_LikelihoodToRecommendAE, 
                                f_ReportingFlagDashboards, 
                                f_TimeWaiting , 
                                f_LikelihoodToRecommendFollowUp
                            ) 
                        VALUES ";

    /* ========================================================= 
      Extract Excel Rows 1 by 1
      ======================================================= */
    $pd_values = array();
    foreach ($excelarray as $value) {
        /* ========================================================= 
          Function that prepares headings for the Uploaded Excel File,
          also checks if cusomter details already exist and add customer records
          ======================================================= */
        $returnData = PrepareExcelArray($value);

        // Customer Insertion Feedback
        $customer_data = $returnData['CustomerDetails'];

        $existing_counter = 0;
        
        if ($returnData['exisiting'] == 'yes') {
            $existing_counter++;
            continue;
        }

        // Values for t_SurveyPD
        $f_InvitationID = $returnData['f_InvitationID'];
        $f_CustomerDetailsID = $returnData['f_CustomerDetailsID'];
        $f_SurveyListID = $returnData['f_SurveyListID'];
        $f_TelstraSurveyProgramID = $returnData['f_TelstraSurveyProgramID'];
        $f_SubSurveyID = $returnData['f_SubSurveyID'];
        $f_EpisodeSourceID = $returnData['f_EpisodeSourceID'];
        $f_EpisodeReferenceNo = $returnData['f_EpisodeReferenceNo'];
        $f_EpisodeStartDate = $returnData['f_EpisodeStartDate'];
        $f_EpisodeEndDate = $returnData['f_EpisodeEndDate'];
        $f_SurveyMethodID = $returnData['f_SurveyMethodID'];
        $f_AskFieldQuestion = $returnData['f_AskFieldQuestion'];
        $f_AskRetialQuestion = $returnData['f_AskRetialQuestion'];
        $f_CustomerBillingAccountID = $returnData['f_CustomerBillingAccountID'];
        $f_CustomerBusinessUnitID = $returnData['f_CustomerBusinessUnitID'];
        $f_CustomerMBMSegment = $returnData['f_CustomerMBMSegment'];
        $f_Product1 = $returnData['f_Product1'];
        $f_Product2 = $returnData['f_Product2'];
        $f_Product3 = $returnData['f_Product3'];
        $f_Product4 = $returnData['f_Product4'];
        $f_Product5 = $returnData['f_Product5'];
        $f_Product6 = $returnData['f_Product6'];
        $f_Product7 = $returnData['f_Product7'];
        $f_Product8 = $returnData['f_Product8'];
        $f_Product9 = $returnData['f_Product9'];
        $f_Product10 = $returnData['f_Product10'];
        $f_ProductID = $returnData['f_ProductID'];

        // Add Each row as Values on PD insert Query
        $sqlSurveyPD .= "(
            '$f_InvitationID', 
            '$f_CustomerDetailsID', 
            '$f_SurveyListID', 
            '$f_TelstraSurveyProgramID',
            '$f_SubSurveyID', 
            '$f_EpisodeSourceID', 
            '$f_EpisodeReferenceNo', 
            '$f_EpisodeStartDate',
            '$f_EpisodeEndDate', 
            '$f_SurveyMethodID',
            '$f_AskFieldQuestion',
            '$f_AskRetialQuestion',
            '$f_CustomerBillingAccountID', 
            '$f_CustomerBusinessUnitID', 
            '$f_CustomerMBMSegment', 
            '$f_Product1',
            '$f_Product2', 
            '$f_Product3', 
            '$f_Product4', 
            '$f_Product5', 
            '$f_Product6',
            '$f_Product7', 
            '$f_Product8', 
            '$f_Product9', 
            '$f_Product10', 
            '$f_ProductID'),";

        // Values for t_SurveySD
        $pd_values[$returnData['f_InvitationID']] = array(
            $returnData['f_UserID1'],
            $returnData['f_UserID2'],
            $returnData['f_UserID3'],
            $returnData['f_UserID4'],
            $returnData['f_UserID5'],
            $returnData['f_UserID6'],
            $returnData['f_UserID7'],
            $returnData['f_UserID8'],
            $returnData['f_UserID9'],
            $returnData['f_UserID10'],
            $returnData['f_UserID11'],
            $returnData['f_UserID12'],
            $returnData['f_DealerCode'],
            $returnData['f_StoreID'],
            $returnData['f_OriginationChannelID'],
            $returnData['f_ActivationExtra4'],
            $returnData['f_ActivationExtra5'],
            $returnData['f_BillFormat'],
            $returnData['f_BillMethod'],
            $returnData['f_AssuranceLoggingSystem'],
            $returnData['f_AssuranceArea'],
            $returnData['f_AssuranceSubArea'],
            $returnData['f_AssuranceSymptom'],
            $returnData['f_ComplaintTIOPriorityLevel'],
            $returnData['f_ComplaintArea'],
            $returnData['f_ComplaintReason'],
            $returnData['f_ComplaintSubReason'],
            $returnData['f_ComplaintSource'],
            $returnData['f_DisconnectionStatus'],
            $returnData['f_InteractionLog'],
            $returnData['f_SurveyID'],
            $returnData['f_DateTimeCreatedMelbourne'],
            $returnData['f_DateTimeResponseMelbourne'],
            $returnData['f_IssueResolved'],
            $returnData['f_RecommendRate'],
            $returnData['f_CustomerReason'],
            $returnData['f_ResolutionPreference'],
            $returnData['f_FollowUp'],
            $returnData['f_CallbackNumberCombined'],
            $returnData['f_IssueDescription'],
            $returnData['f_LikelihoodToRecommendField'],
            $returnData['f_LikelihoodToRecommendRetail'],
            $returnData['f_IndustryCode1'],
            $returnData['f_IndustryCode2'],
            $returnData['f_SalesPortfolioCode'],
            $returnData['f_SalesGroup'],
            $returnData['f_BusinessUltimateCIDN'],
            $returnData['f_BusinessMetroRegional'],
            $returnData['f_BusinessManagedStatus'],
            $returnData['f_LikelihoodToRecommendAE'],
            $returnData['f_ReportingFlagDashboards'],
            $returnData['f_TimeWaiting'],
            $returnData['f_LikelihoodToRecommendFollowUp']);
    }

    /* ========================================================= 
      Check if surveySD values not empty and ready to insert records
      ======================================================= */
    if (!is_null($pd_values)) {
        /* ========================================================= 
          Prepare t_surveyPD records insert
          ======================================================= */

        //  Remove comma on surveyPD query
        $sqlSurveyPD = trim($sqlSurveyPD, ",");

        //  Insert Records to SurveyPD
        $pd_data = InsertSurveyPD($sqlSurveyPD);

        //  Get SurveyID base on Invitation ID
        $getSurveypdID = getSurveypdID($pd_values);

        //  Change Index to PDID
        foreach ($pd_values as $key => $checkvalue) {
            array_unshift($pd_values[$key], $getSurveypdID[$key]);
        }

        /* ========================================================= 
          Prepare t_surveySD records insert
          ======================================================= */

        $pd_array_value = "";

        //  Prepare Data for surveySD data
        foreach ($pd_values as $toStringValue) {
            $pd_array_value .= "(";
            $pd_array_value .= "'" . implode("','", $toStringValue) . "'";
            $pd_array_value .= "),";
        }

        //  Remove last comma from multiple values
        $pd_array_value = rtrim($pd_array_value, ",");

        //  Query for insert SD records
        $surveySDquery = $sqlSurveySDmain . $pd_array_value;

        //  Insert Records to SurveyPD
        $sd_data = InsertSurveySD($surveySDquery);

        //echo "<pre>";
        //echo $surveySDquery . "<br>";
        //var_dump($data);
        // Delete file on Temp Folder after all db updates were made
        unlink($file);
    } else {
        $sd_data['type'] = 'failed';
        $sd_data['message'] = 'Records already exist!';
        $sd_data['count'] = 0;
        unlink($file);
    }
}
