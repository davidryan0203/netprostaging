<?php
//sleep(2);
error_reporting(E_ALL);
session_start();
date_default_timezone_set('Australia/Melbourne');
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
//$when = $_GET[''];
$empID = $_GET['empID'];
$rate = $_GET['rate'];
$action5 = $_GET['action5'];
$storeID = $_GET['StoreID'];
$type = $_GET['type'];
$modulator = $_GET['modulator'];
$storeOwnerID = $_GET['storeOwnerID'];
$ismerge = $_GET['merge'];
//echo "hre";
//if ((!is_null($_GET['to']) && !is_null($_GET['from'])) && ($_GET['to'] != '' && $_GET['from'] != '' )) {
$to = date("Y-m-d", strtotime($_GET['to']));
$from = date("Y-m-d", strtotime($_GET['from']));
//
//    $sqlWhen = " AND b.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
//} else {
//    $sqlWhen = " AND MONTH(b.f_DateTimeResponseMelbourne) = MONTH(CURDATE())
//AND YEAR(b.f_DateTimeResponseMelbourne) = YEAR(CURDATE())";
//}
//echo $sqlWhen;


$rangeType = $_GET['rangeType'];

if ($rangeType == 'mtd') {
    $lastDate = $_GET['to'];
    $firstday = " '{$_GET['from']}' ";
    $mobileday = " '{$_GET['from']}' ";
    $fixedday = " '{$_GET['from']}' ";
    $episode = " '{$_GET['from']}' ";
} else {

    // $lastDate = date('Y-m-t', strtotime($to));

    $lastDate = date('Y-m-t', strtotime($to));
    $firstday = "CONCAT(DATE_FORMAT(DATE_SUB('{$lastDate}', INTERVAL 2 MONTH) ,'%Y-%m-01'),' ','00:00:00') ";
    $mobileday = "CONCAT(DATE_FORMAT(DATE_SUB('{$lastDate}', INTERVAL 2 MONTH) ,'%Y-%m-01'),' ','00:00:00') ";
    $episodeday = "CONCAT(DATE_FORMAT(DATE_SUB('{$lastDate}', INTERVAL 2 MONTH) ,'%Y-%m-01'),' ','00:00:00') ";
    $fixedday = "CONCAT(DATE_FORMAT(DATE_SUB('{$lastDate}', INTERVAL 5 MONTH) ,'%Y-%m-01'),' ','00:00:00') ";
}


if ($type == 'interaction') {
    $sqlReportType = " ";
    if ($rate == 'advo') {
        $sqlRate = "AND trim(b.f_LikelihoodToRecommendRetail) BETWEEN 9 and 10";
    } elseif ($rate == 'pass') {
        $sqlRate = "AND trim(b.f_LikelihoodToRecommendRetail) BETWEEN 7 and 8";
    } elseif ($rate == 'detract') {
        $sqlRate = "AND trim(b.f_LikelihoodToRecommendRetail) BETWEEN 0 and 6";
    } elseif ($rate == 'withScore') {
        $sqlRate = "AND trim(b.f_LikelihoodToRecommendRetail) BETWEEN 0 and 10";
    } elseif ($rate == 'noScore') {
        $sqlRate = "AND ( trim(b.f_LikelihoodToRecommendRetail) is NULL )  AND b.f_ActivationExtra5 != 'P000000'";
    } else {
        $sqlRate = " ";
    }

    $sqlWhen = " AND b.f_DateTimeResponseMelbourne between $firstday AND '{$lastDate} 23:59:59'";
} elseif ($type == 'mobile') {
    $sqlReportType = " AND a.f_ProductID in ('1','2')";

    if ($rate == 'advo') {
        $sqlRate = "AND trim(b.f_RecommendRate) BETWEEN 9 and 10";
    } elseif ($rate == 'pass') {
        $sqlRate = "AND trim(b.f_RecommendRate) BETWEEN 7 and 8";
    } elseif ($rate == 'detract') {
        $sqlRate = "AND trim(b.f_RecommendRate) BETWEEN 0 and 6";
    } elseif ($rate == 'withScore') {
        $sqlRate = "AND trim(b.f_RecommendRate) BETWEEN 0 and 10";
    } elseif ($rate == 'noScore') {
        $sqlRate = "AND ( trim(b.f_RecommendRate) is NULL)  AND b.f_ActivationExtra5 != 'P000000'";
    } else {
        $sqlRate = " ";
    }

    $sqlWhen = " AND b.f_DateTimeResponseMelbourne between $mobileday AND '{$lastDate} 23:59:59'";
} elseif ($type == 'fixed') {
    $sqlReportType = " AND a.f_ProductID in ('5','6','7','8','9','10','11','12','13','16','17','18','19','20')";

    if ($rate == 'advo') {
        $sqlRate = "AND  trim(b.f_RecommendRate) BETWEEN 9 and 10 ";
        // $sqlRate2 = " ";
    } elseif ($rate == 'pass') {
        $sqlRate = "AND trim(b.f_RecommendRate) BETWEEN 7 and 8 ";
        //  $sqlRate2 = " ";
    } elseif ($rate == 'detract') {
        $sqlRate = "AND  trim(b.f_RecommendRate) BETWEEN 0 and 6";
        //   $sqlRate2 = " ";
    } elseif ($rate == 'withScore') {
        $sqlRate = "AND trim(b.f_RecommendRate) BETWEEN 0 and 10 ";
        //   $sqlRate2 = " ";
    } elseif ($rate == 'noScore') {
        $sqlRate = "AND TRIM(b.f_RecommendRate) IS NULL  AND b.f_ActivationExtra5 != 'P000000'";
        //  $sqlRate2 = "AND ( (TRIM(b.f_RecommendRate) IS NULL)  OR ( TRIM(b.f_LikelihoodToRecommendRetail) IS NULL  ) )  AND b.f_ActivationExtra5 != 'P000000'";
    } elseif ($rate == 'int') {
        $sqlRate = "AND trim(b.f_LikelihoodToRecommendRetail) BETWEEN 0 and 10 AND  b.f_ActivationExtra5 != 'P000000'";
    } elseif ($rate == 'epi') {
        $sqlRate = "AND trim(b.f_RecommendRate) BETWEEN 0 and 10 ";
    } else {
        $sqlRate1 = " ";
        $sqlRate2 = " ";
    }

    $sqlWhen = " AND b.f_DateTimeResponseMelbourne between $fixedday AND '{$lastDate} 23:59:59'";
} elseif ($type == 'episode') {
    $sqlReportType = " ";

    if ($rate == 'advo') {
        $sqlRate = "AND trim(b.f_RecommendRate) BETWEEN 9 and 10";
    } elseif ($rate == 'pass') {
        $sqlRate = "AND trim(b.f_RecommendRate) BETWEEN 7 and 8";
    } elseif ($rate == 'detract') {
        $sqlRate = "AND trim(b.f_RecommendRate) BETWEEN 0 and 6";
    } elseif ($rate == 'withScore') {
        $sqlRate = "AND trim(b.f_RecommendRate) BETWEEN 0 and 10";
    } elseif ($rate == 'noScore') {
        $sqlRate = "AND ( trim(b.f_RecommendRate) is NULL)  AND b.f_ActivationExtra5 != 'P000000'";
    } else {
        $sqlRate = " ";
    }

    $sqlWhen = " AND b.f_DateTimeResponseMelbourne between $episodeday AND '{$lastDate} 23:59:59'";
}

if (!is_null($storeID) && !empty($storeID)) {
    if ($ismerge != 'yes') {
        $sqlStoreID = " AND d.f_StoreID = '{$storeID}'";
    } else {
        $sqlStoreID = "";
    }
} else {
    $sqlStoreID = "";
}


if (!is_null($action5) && !empty($action5)) {

    if ($action5 == 'blank' || $action5 == 'empty') {
        $action5 = "";
    } elseif ($action5 == "DUMMY" && !empty($storeOwnerID)) {
        $addTosql = " and d.f_StoreOwnerUserID = '{$storeOwnerID}'";
    }
    $sqlAction5 = " and b.f_ActivationExtra5 = '{$action5}' $addTosql";
} else {

    if (!empty($storeOwnerID)) {
        $sqlAction5 = " and d.f_StoreOwnerUserID = '{$storeOwnerID}' ";
    } else {
        $sqlAction5 = "";
    }
}
//echo $sqlEmp;
if (!is_null($empID) && !empty($empID)) {
    $sqlEmp = " AND e.f_EmpID = '{$empID}' ";
} else {
    $sqlEmp = "";
}

if (!is_null($modulator) && !empty($modulator)) {

    if ($modulator == 'exlcude') {
        $sqlModulator = " and b.f_ActivationExtra5 != 'P000000' and  b.f_LikelihoodToRecommendRetail is not NULL";
    } elseif ($modulator == 'episode') {
        $sqlModulator = " ";
    } else {
        $sqlModulator = "";
    }
}
//echo $sqlrate1;
//echo "<br>" . $sqlWhen . "is here";

if (!is_null($empID) && !empty($empID)) {
    $sqlfeedback = "SELECT concat(c.f_CustomerFirstName, ' ', a.f_CustomerBillingAccountID) as CustomerName, g.f_SurveyName as SurveyType, h.f_SubSurveyName as SubType,
        b.f_CustomerReason as Reason, b.f_LikelihoodToRecommendRetail as InteractionScore, b.f_RecommendRate as EpisodeScore,
        b.f_DateTimeResponseMelbourne as Date, e.f_EmpID, b.f_ActivationExtra5, d.f_StoreListName as StoreName, concat(f.f_EmpFirstName, ' ', f.f_EmpLastName) as EmpName
        FROM t_surveypd a
        LEFT JOIN t_surveysd b on a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_customerdetails c on a.f_CustomerDetailsID = c.f_CustomerDetailsID
        LEFT JOIN t_storelist d on b.f_StoreID = d.f_StoreID
        LEFT JOIN t_empnumber e on e.f_EmpPNumber = b.f_ActivationExtra5 and e.f_StoreID = b.f_StoreID
        LEFT JOIN t_emplist f on e.f_EmpID = f.f_EmpID
        LEFT JOIN t_surveylist g on g.f_SurveyListID = a.f_SurveyID
        LEFT JOIN t_subsurveylist h on h.f_SubSurveyID = a.f_SubSurveyID
        WHERE 1=1 and d.f_StoreTypeID = 2 AND a.f_CustomerBusinessUnitID = 2 {$sqlEmp} {$sqlRate} {$sqlWhen} {$sqlModulator} {$sqlStoreID} {$sqlReportType} order by b.f_DateTimeResponseMelbourne desc ";
} else {
    mysql_query("SET SQL_BIG_SELECTS=1");
    $sqlfeedback = "SELECT concat(c.f_CustomerFirstName, ' ', a.f_CustomerBillingAccountID) as CustomerName, g.f_SurveyName as SurveyType, h.f_SubSurveyName as SubType,
        b.f_CustomerReason as Reason,  b.f_ActivationExtra5 , b.f_LikelihoodToRecommendRetail as InteractionScore, b.f_RecommendRate as EpisodeScore, b.f_DateTimeResponseMelbourne as Date,
        d.f_StoreListName as StoreName ,CASE WHEN e.f_EmpID IS NULL THEN b.f_ActivationExtra5
        ELSE concat(f.f_EmpFirstName, ' ', f.f_EmpLastName) END AS EmpName
        FROM t_surveypd a
        LEFT JOIN t_surveysd b on a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_customerdetails c on a.f_CustomerDetailsID = c.f_CustomerDetailsID
        LEFT JOIN t_storelist d on b.f_StoreID = d.f_StoreID
        LEFT JOIN t_empnumber e on e.f_EmpPNumber = b.f_ActivationExtra5 and e.f_StoreID = b.f_StoreID
        LEFT JOIN t_emplist f on e.f_EmpID = f.f_EmpID
        LEFT JOIN t_surveylist g on g.f_SurveyListID = a.f_SurveyID
        LEFT JOIN t_subsurveylist h on h.f_SubSurveyID = a.f_SubSurveyID
        WHERE 1=1 and d.f_StoreTypeID = 2 AND a.f_CustomerBusinessUnitID = 2 {$sqlAction5} {$sqlRate} {$sqlWhen} {$sqlStoreID} {$sqlModulator} {$sqlReportType}  order by b.f_DateTimeResponseMelbourne desc ";
}
$resultfeedback = mysql_query($sqlfeedback, $connection);

while ($rowfeedback = mysql_fetch_assoc($resultfeedback)) {
    $ArrayData[] = $rowfeedback;
}
//echo $sqlfeedback;
?>

<script type="text/javascript">
    $(document).ready(function() {

        var table2 = $('#Feedback2<?php echo $type; ?>').DataTable({
            //"sDom": '<"top">RCTsf<"bottom"lrtip>',
            "iDisplayLength": 10, bFilter: false, bInfo: false,
            "bSort": false,
            "bLengthChange": false
                    // "aLengthMenu": [[15, 50, 100, -1], [15, 50, 100, "All"]],

        });
    });
</script>
<script type="text/javascript">
    (function($) {
        $.fn.shorten = function(settings) {

            var config = {
                showChars: 255,
                minHideChars: 10,
                ellipsesText: "...",
                moreText: "more",
                lessText: "less"
            };

            if (settings) {
                $.extend(config, settings);
            }

            $(document).off("click", '.morelink');

            $(document).on({click: function() {

                    var $this = $(this);
                    if ($this.hasClass('less')) {
                        $this.removeClass('less');
                        $this.html(config.moreText);
                    } else {
                        $this.addClass('less');
                        $this.html(config.lessText);
                    }
                    $this.parent().prev().toggle();
                    $this.prev().toggle();
                    return false;
                }
            }, '.morelink');

            return this.each(function() {
                var $this = $(this);
                if ($this.hasClass("shortened"))
                    return;

                $this.addClass("shortened");
                var content = $this.html();
                if (content.length > config.showChars + config.minHideChars) {
                    var c = content.substr(0, config.showChars);
                    var h = content.substr(config.showChars, content.length - config.showChars);
                    var html = c + '<span class="moreellipses">' + config.ellipsesText + ' </span><span class="morecontent"><span>' + h + '</span> <a href="#" class="morelink">' + config.moreText + '</a></span>';
                    $this.html(html);
                    $(".morecontent span").hide();
                }
            });

        };

    })(jQuery);
    $(".more").shorten({
        "showChars": 255,
        "minHideChars": 50,
        "moreText": "See More",
        "lessText": "Less",
    });</script>
<style>
    #tableHead{
        display: none;
    }
    #Feedback {
        border: none;
    }
    .feedback{
        padding: 15px 15px 15px 15px;
    }
    .feedbackHead div.DTTT_container{
        margin-top: -26px;
        padding-right: 15px;
    }


</style>
<div class="modal-header feedback">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

    <div class="col-lg-6">
        <h4 class="modal-title" id="myModalLabel">Customer Feedback</h4>
    </div>
    <?php
    $checkData2 = count($ArrayData);
    if ($checkData2 > 0) {
        ?>
        <table id="Feedback<?php echo $type; ?>" border="0">
            <thead id="tableHead">
                <tr>
                    <th>Customer Name</th>
                    <th>Interaction Score</th>
                    <th>Episode Score</th>
                    <th>Survey Type</th>
                    <th>Reason</th>
                    <th>Store Name</th>
                    <th>Assigned To</th>
                    <th>Response Date</th>
                </tr>
            </thead>
            <tbody style="display:none">

                <?php foreach ($ArrayData as $all) { ?>
                    <tr>
                        <td><?php echo $all["CustomerName"]; ?></td>
                        <td><?php echo $all["InteractionScore"]; ?></td>
                        <td><?php echo $all["EpisodeScore"]; ?></td>
                        <td><?php echo $all["SurveyType"] . "-" . $all['SubType']; ?></td>
                        <td><?php echo $all["Reason"]; ?></td>
                        <td><?php echo $all['StoreName']; ?></td>
                        <td><?php echo $all['f_ActivationExtra5'] . ' - ' . $all["EmpName"]; ?></td>
                        <td><?php echo date("d-M-y", strtotime($all["Date"])); ?></td>

                    </tr>
                <?php } ?>
            </tbody>
        </table> <?php } ?>
</div>
<div class='modal-body' style="overflow-y: scroll; max-height: 50%">

    <?php
    $i = 1;
    $checkData = count($ArrayData);
    if ($checkData > 0) {
        ?>
        <table id="Feedback2<?php echo $type; ?>" >
            <thead id="tableHead">
                <tr>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($ArrayData as $all) {
                    ?>
                    <tr><td>
                            <div class = "chat-body clearfix">
                                <strong class = "primary-font"> <?php echo $i . ". " . $all["CustomerName"]; ?>
                                    <span class = "chat-img fa-stack" style="margin-top: -10px">
                                        <i class = "fa fa-comment  fa-stack-2x" style="color:gold; text-shadow: 1px 1px 1px #b7b7b7; "></i>
                                        <strong class = "fa-stack-1x fa-stack-text fa-inverse small " style = "color:white">
                                            <?php echo $all["InteractionScore"]; ?>
                                        </strong>
                                    </span>
                                    <span class = "chat-img fa-stack" style="margin-top: -10px">
                                        <i class = "fa fa-comment  fa-stack-2x" style="color:orange; text-shadow: 1px 1px 1px #b7b7b7; "></i>
                                        <strong class = "fa-stack-1x fa-stack-text fa-inverse small " style = "color:white">
                                            <?php echo $all["EpisodeScore"]; ?>
                                        </strong>
                                    </span>
                                    <span class="pull-right  text-muted" style="font-size: 12px;">
                                        <i class="fa fa-bell"></i> <small> <?php echo $all['SurveyType'] . "-" . $all['SubType']; ?></small>
                                    </span>
                                </strong>
                                <p class="more">
                                    <?php echo $all["Reason"]; ?>
                                </p>
                                <footer>
                                    <small class = "label label-primary text-muted pull-left">
                                        <i class = "fa fa-user fa-fw"></i>  <?php echo $all['f_ActivationExtra5'] . ' - ' . $all["EmpName"]; ?>
                                    </small>

                                    <small class = "text-muted pull-right">
                                        <i class = "fa fa-clock-o fa-fw"></i>  <?php echo $all["StoreName"] . ", " . date("d-M-y", strtotime($all["Date"])); ?>
                                    </small>
                                </footer>
                            </div><hr></td></tr>

                    <?php
                    $i++;
                }
                ?> </tbody></table> <hr><?php
    } else {
        ?>
        <div class = "chat-body clearfix">
            <strong class = "primary-font">No Data</strong>
        </div>
    <?php } ?>
</div>





<script type="text/javascript">
    $(document).ready(function() {

        var table = $('#Feedback<?php echo $type; ?>').DataTable({
            //"sDom": '<"top">RCTsf<"bottom"lrtip>',
            bFilter: false, bInfo: false,
            "bSort": false,
            "bLengthChange": false,
            paging: false,
            "sDom": '<"top">RTf<"bottom">',
            "oTableTools": {
                "sSwfPath": "datatable/media/swf/copy_csv_xls_pdf.swf",
                "aButtons": [
                    {
                        "sExtends": "csv",
                        "sButtonText": "Export to Excel",
                        "oSelectorOpts": {page: 'current'},
                        "mColumns": "visible",
                        "bFooter": false
                    }
                ]
            },
            // "aLengthMenu": [[15, 50, 100, -1], [15, 50, 100, "All"]],

        });

        $('#Feedback_wrapper').addClass('feedbackHead');

    });

</script>