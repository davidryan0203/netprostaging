<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
ini_set('max_execution_time', 300);
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
require($_SERVER['DOCUMENT_ROOT'] . '/admin_sso/function/SubFunction.php');
$getStoreList = SelectStoreList('Own');
$getReferenceList = getSurveyTop();

function getSurveyTop() {
    global $connection;
    $role = $_SESSION['UserRoleID'];
    switch ($role) {
        case '1':
            $sqlOwnerID .= " ";
            break;
        case '2':
        case '3':
            $sqlOwnerID = " AND f.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
            break;
        case '4':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $data = $row['f_StoreOwnerUserID'];
            }
            $sqlOwnerID = " AND f.f_StoreOwnerUserID = '{$data}' ";
            break;
        case '5':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $OwnerID = $row['f_StoreOwnerUserID'];
            }
            $sqlOwnerID = " AND f.f_StoreOwnerUserID = '{$OwnerID}' ";
            break;
    }


    $getReference = "SELECT
    f.f_StoreListName as StoreName,
    concat( f_CustomerFirstName ,' ',f_CustomerBillingAccountID) as CustomerName,
    f_CustomerReason as CustomerReason,
    CONCAT(f_EmpFirstName, ' ', f_EmpLastName) as EmpName,
    f_ActivationExtra5,
     b.f_DateTimeResponseMelbourne AS Date_Time,
    f_ResolutionPreference as Reference
FROM
    t_surveypd a
        LEFT JOIN
    t_surveysd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN
    t_customerdetails c ON a.f_CustomerDetailsID = c.f_CustomerDetailsID
        LEFT JOIN
    t_empnumber d ON b.f_ActivationExtra5 = d.f_EmpPNumber
        LEFT JOIN
    t_emplist e ON d.f_EmpID = e.f_EmpID
        LEFT JOIN
    t_storelist f ON b.f_StoreID = f.f_StoreID
WHERE
    (f_ActivationExtra5 = '' or f_ActivationExtra5 IS NULL )
        AND MONTH(f_DateTimeResponseMelbourne) = MONTH(NOW())
        AND YEAR(f_DateTimeResponseMelbourne) = 2019
        $sqlOwnerID";
    $resusltReference = mysql_query($getReference, $connection);
//CONCAT(DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 2 MONTH)), INTERVAL 1 DAY) , ' 00:00:00') AND NOW()
    while ($rowReference = mysql_fetch_assoc($resusltReference)) {
        $ArrayData[$rowReference['StoreName']][] = $rowReference;
    }
    //  echo $sqlGetSurvey;
    return $ArrayData;
}
?>
<style>
    #tableHead{
        display: none;
    }
    #Feedback {
        border: none;
    }
    .feedback{
        padding: 10px !important;
    }
    .feedbackHead div.DTTT_container{
        margin-top: -26px;
        margin-right: 15px;
    }


</style>

<div id="modal-body" style="padding: 20px;">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

    <h4 class='sub-header text-center'> Needs to be resolve </h4>

    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
        <li><a class="firstHref" href="#<?php echo str_replace(' ', '', $getStoreList[0]['f_StoreListName']); ?>Tab" data-toggle="tab"><?php echo $getStoreList[0]['f_StoreListName']; ?></a></li>
        <?php foreach (array_slice($getStoreList, 1) as $value) { ?>
            <li><a href="#<?php echo str_replace(' ', '', $value['f_StoreListName']); ?>Tab" data-toggle="tab"><?php echo $value['f_StoreListName']; ?></a></li>
        <?php } ?>
    </ul>


    <div id="my-tab-content" class="tab-content">
        <?php
        foreach ($getStoreList as $storeValue) {
            $key = $storeValue['f_StoreListName'];
            $i = 1;
            if (count($getReferenceList[$key]) > 0 || !empty($getReferenceList[$key])) {
                ?>


                <div class="tab-pane" id="<?php echo str_replace(' ', '', $key); ?>Tab">
                    <div class='modal-body' style="overflow-y: scroll; max-height: 72%">
                        <table id="SurveyTabs<?php echo str_replace(' ', '', $key); ?>" style="width:100%">
                            <thead id="tableHead">
                                <tr>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($getReferenceList[$key] as $value) {
                                    ?>
                                    <tr>
                                        <td>
                                            <div class = "chat-body clearfix">
                                                <strong class = "primary-font"> <?php echo $i . ". " . $value["CustomerName"]; ?>
                                                    <span class="pull-right  text-muted" style="font-size: 12px;">
                                                        <i class="fa fa-bell"></i> <small> <?php echo $value['Reference']; ?></small>
                                                    </span>
                                                </strong>
                                                <p class="more">
                                                    <?php echo $value["CustomerReason"]; ?>
                                                </p>
                                                <footer>
                                                    <small class = "label label-primary text-muted pull-left">
                                                        <i class = "fa fa-user fa-fw"></i>  <?php echo $value['f_ActivationExtra5'] . ' - ' . $value["EmpName"]; ?>
                                                    </small>

                                                    <small class = "text-muted pull-right">
                                                        <i class = "fa fa-clock-o fa-fw"></i>  <?php echo date("d-M-y h:i A", strtotime($value["Date_Time"])); ?>
                                                    </small>
                                                </footer>
                                            </div>
                                            <hr>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <?php
            } else {
                ?>  <div class="tab-pane" id="<?php echo str_replace(' ', '', $key); ?>Tab">
                    <div class='modal-body' style="overflow-y: scroll; max-height: 72%"><center>No Recent Inserted Surveys</center>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
    <?php
    foreach ($storeSurvey as $key => $value) {
        ?>


        <script type="text/javascript">
            $(document).ready(function() {

                $('#SurveyTabs<?php echo str_replace(' ', '', $key); ?>').DataTable({
                    "iDisplayLength": 10, bFilter: false, bInfo: false,
                    "bSort": false,
                    "bLengthChange": false,
                    "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'

                            //  "aaSorting": [[0, 'desc']]
                });
            });
        </script>
    <?php } ?>
</div>
<style>

    #tableHead{
        display: none;
    }

</style>

<script type="text/javascript">
    (function($) {
        $.fn.shorten = function(settings) {

            var config = {
                showChars: 255,
                minHideChars: 10,
                ellipsesText: "...",
                moreText: "more",
                lessText: "less"
            };

            if (settings) {
                $.extend(config, settings);
            }

            $(document).off("click", '.morelink');

            $(document).on({click: function() {

                    var $this = $(this);
                    if ($this.hasClass('less')) {
                        $this.removeClass('less');
                        $this.html(config.moreText);
                    } else {
                        $this.addClass('less');
                        $this.html(config.lessText);
                    }
                    $this.parent().prev().toggle();
                    $this.prev().toggle();
                    return false;
                }
            }, '.morelink');

            return this.each(function() {
                var $this = $(this);
                if ($this.hasClass("shortened"))
                    return;

                $this.addClass("shortened");
                var content = $this.html();
                if (content.length > config.showChars + config.minHideChars) {
                    var c = content.substr(0, config.showChars);
                    var h = content.substr(config.showChars, content.length - config.showChars);
                    var html = c + '<span class="moreellipses">' + config.ellipsesText + ' </span><span class="morecontent"><span>' + h + '</span> <a href="#" class="morelink">' + config.moreText + '</a></span>';
                    $this.html(html);
                    $(".morecontent span").hide();
                }
            });

        };

    })(jQuery);
    $(".more").shorten({
        "showChars": 255,
        "minHideChars": 50,
        "moreText": "See More",
        "lessText": "Less",
    });

    jQuery(document).ready(function($) {
        $('#tabs').tab();
        $(".firstHref").trigger('click');
    });
</script>