<?php
sleep(2);
error_reporting(0);
session_start();
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/nps_sso/function/NPSFunction.php');

$shared = $_GET['shared'];
$empID = $_GET['empID'];
$profileID = $_SESSION['UserProfileID'];
$role = $_GET['role'];
$to = date("Y-m-d", strtotime($_GET['toTBC']));
$from = date("Y-m-d", strtotime($_GET['from']));
$storeType = $_GET['storeType'];
$empid = $_SESSION['EmpProfileID'];

if (isset($_GET['reconOwnerID']) && !is_null($_GET['reconOwnerID'])) {
    $ownerReconID = $_GET['reconOwnerID'];
} else {
    $ownerReconID = NULL;
}

if ($rangeType == 'mtd') {
    $to = mysql_real_escape_string($_GET['mtdTo']);
    $from = mysql_real_escape_string($_GET['mtdFrom']);
    $tableID = "episodeNPSMTD";
} else {
    $to = $_GET['toTBC'];
    $format = 'M-y';
    $date = DateTime::createFromFormat($format, $to);
    $to = $date->format('Y-m-d');
    $from = date("Y-m-d", strtotime($_GET['from']));
    $tableID = "episodeNPSDef";
}

$EpisodeData = getStoreNpsTBC($shared, $ownerReconID, $empid, $to, $from, $role, $storeType, $rangeType, 'Episode');
//echo "<pre>";
//var_dump($EpisodeData);
//exit;

if (is_null($EpisodeData['Episode'][0]['f_StoreID'])) {
    unset($EpisodeData);
    $EpisodeData == array();
    //echo 'was here';
}
//echo "<pre>";
//print_r($EpisodeData);
//exit;
?>
<script type="text/javascript">
    $(document).ready(function() {
        // prepare modal for new data
        $('a.feedback<?php echo $tableID; ?>').click(function(ev) {
            ev.preventDefault();
            var target = $(this).attr('href');

            $("#feedbackmodal<?php echo $tableID; ?> .modal-content<?php echo $tableID; ?>").load(target, function() {
                $("#feedbackmodal<?php echo $tableID; ?>").modal("show");
            });
        });
<?php if ($shared != 'no') { ?>
            $("#<?php echo $tableID; ?> a").removeAttr("href data-target data-toggle class").css("cursor", "pointer");
<?php } ?>
    });

    $("#feedbackmodal<?php echo $tableID; ?>").on("hidden.bs.modal", function() {
        $(".modal-content<?php echo $tableID; ?>").html("");
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {

        var t = $('#<?php echo $tableID; ?>').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            "iDisplayLength": 15,
            "aLengthMenu": [[15, 50, 100, -1], [15, 50, 100, "All"]],
            columnDefs: [
                {sortable: false, targets: -1},
                {"width": "8%", "targets": -1}
            ],
            "oColVis": {
                "buttonText": "Hide"
            },
            initComplete: function() {
                var api = this.api();

                api.columns().indexes().flatten().each(function(i) {
                    var column = api.column(i);
                    var select = $('<select><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function() {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );

                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });

                    column.data().unique().sort().each(function(d, j) {
                        select.append('<option value="' + stripHTML(d).trim() + '">' + stripHTML(d).trim() + '</option>');
                    });
                });
            },
            "autoWidth": false,
            "scrollXInner": true,
            // "sScrollY": "519px",
//            "sScrollX": true,
            "bScrollCollapse": true,
            "bPaginate": true,
            //"bJQueryUI": true,
            "aoColumnDefs": [
                {"sWidth": "10%", "aTargets": [-1]}
            ],
            responsive: true
        });

        t.on('order.dt search.dt', function() {
            t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        //Remove Heading Sorting
        $("th").unbind();
        $("th").removeClass("sorting");
        $("th").removeClass("sorting_asc");
    });


    function stripHTML(dirtyString) {
        var container = document.createElement('div');
        container.innerHTML = dirtyString;
        return container.textContent;
    }
</script>

<div class="modal fade" id="feedbackmodal<?php echo $tableID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content<?php echo $tableID; ?>"></div>
    </div>
</div> <!-- /Feedback Modal -->

<table id="<?php echo $tableID; ?>" class="table table-bordered table-hover table-responsive"  cellspacing="0"  >
    <thead>
        <tr>
            <th style="width:20px; text-align: center">Rank</th>
            <th>Store</th>
            <th>AD</th>
            <th>PA</th>
            <th>D</th>
            <th>Total</th>
            <th>NPS</th>
            <th>Tier</th>
            <th>No Score</th>
            <th>To Assign</th>
            <th>Raw Surveys</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th style="width:20px; text-align: center">Rank</th>
            <th>Store</th>
            <th>AD</th>
            <th>PA</th>
            <th>D</th>
            <th>Total</th>
            <th>NPS</th>
            <th>Tier</th>
            <th>No Score</th>
            <th>To Assign</th>
            <th>Raw Surveys</th>

        </tr>
    </tfoot>

    <tbody>
        <?php
        foreach ($EpisodeData['Episode'] as $value) {
            //echo "<pre>";
            //print_r($value);
            //exit;
            ?>
            <tr>
                <td style="width:20px; text-align: center"></td>
                <td><?php echo $value['StoreName']; ?></td>
                <td>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps_sso/function/FeedbackModalTBC.php?rate=advo&StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=episode&rangeType=<?php echo $rangeType; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['EpisodeTotalAdvocate']; ?>
                    </a>
                <td>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps_sso/function/FeedbackModalTBC.php?rate=pass&StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=episode&rangeType=<?php echo $rangeType; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['EpisodeTotalPassive']; ?>
                    </a>
                </td>
                <td>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps_sso/function/FeedbackModalTBC.php?rate=detract&StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=episode&rangeType=<?php echo $rangeType; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['EpisodeTotalDetractors']; ?>
                    </a>
                </td>
                <td>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps_sso/function/FeedbackModalTBC.php?StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=episode&modulator=episode&rangeType=<?php echo $rangeType; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['EpisodeTotalSurvey']; ?>
                    </a>
                </td>
                <td><?php echo $value['EpisodeNpsScore']; ?></td>
                <td><?php echo $value['EpisodeTierLevel']; ?></td>
                <td>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps_sso/function/FeedbackModalTBC.php?rate=noScore&StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=noScoreEpisode&rangeType=<?php echo $rangeType; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['EpisodeNoScore']; ?>
                    </a>
                </td>
                <th>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps_sso/function/FeedbackModalTBC.php?StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=episode&action5=blank&rangeType=<?php echo $rangeType; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['EpisodeUnAssigned']; ?>
                    </a>
                </th>
                <td>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps_sso/function/FeedbackModalTBC.php?StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=episode&rangeType=<?php echo $rangeType; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['EpisodeRawTotal']; ?>
                    </a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>












