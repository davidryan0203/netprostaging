<?php
sleep(2);
error_reporting(0);
session_start();
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/nps_sso/function/NPSFunction.php');

$shared = $_GET['shared'];
$empID = $_GET['empID'];
$profileID = $_SESSION['UserProfileID'];
$role = $_GET['role'];
//$to = date("Y-m-d", strtotime($_GET['to']));
//$from = date("Y-m-d", strtotime($_GET['from']));
$tierRange = NULL;
if (isset($_GET['weekly']) && $_GET['weekly'] == "on") {
    $to = date("Y-m-d");
    //  $from = date("Y-m-d", strtotime('4 weeks ago'));
    $from = date("Y-m-d", strtotime('-27 days'));
    $tableID = "tblMobileNpsWeekly";
} elseif (isset($_GET['3months']) && $_GET['3months'] == "on") {
    $to = date("Y-m-d");
    $from = date("Y-m-d", strtotime('-3 Months'));
    $tableID = "tblMobileNps3Months";
} elseif (isset($_GET['htd']) && $_GET['htd'] == "on") {
    $currentMonth = date('m');
    if (($currentMonth >= 1) && ($currentMonth <= 6)) {
        $from = date("Y-01-01");
        $to = date("Y-06-30");
    } else {
        $from = date("Y-07-01");
        $to = date("Y-12-31");
    }

    $tableID = "tblMobileNpsHtd";
} elseif (isset($_GET['3mma']) && $_GET['3mma'] == "on") {
    $to = $_GET['to'];
    $from = date("Y-m-01", strtotime('first day of -2 Month', strtotime($_GET['to'])));
    $tableID = "tblMobileNps3mma";
    $tierRange = "3MMA";
} else {
    $tableID = "tblMobileNps";
    if (isset($_GET['from']) && isset($_GET['to'])) {
        $from = date("Y-m-d", strtotime($_GET['from']));
        $to = date("Y-m-d", strtotime($_GET['to']));
    } else {
        $from = NULL;
        $to = NULL;
    }
}
$storeType = $_GET['storeType'];

// getStoreNpsTLS($shared = NULL, $profileID = NULL, $empID = NULL, $to = NULL, $from = NULL, $role = NULL, $storeType = 1, $rangeType = NULL, $tierSort = 'Episode', $tierReport = NULL) {

$MobileData = getStoreNpsTLS($shared, $ownerReconID, $empid, $to, $from, $role, $storeType, NULL, 'Mobile', $tierRange);
//echo $to . " " . $from;
//echo "<pre>";
//print_r($EpisodeData);
//exit;
?>
<script type="text/javascript">
    $(document).ready(function() {
        $('a.feedback<?php echo $tableID; ?>').click(function(ev) { // prepare modal for new data
            ev.preventDefault();
            var target = $(this).attr('href');
            $("#feedbackmodal<?php echo $tableID; ?> .modal-content<?php echo $tableID; ?>").load(target, function() {
                $("#feedbackmodal<?php echo $tableID; ?>").modal("show");
            });
        });

<?php if ($shared != 'no') { ?>
            $("#tblEpisodeNps a").removeAttr("href data-target data-toggle class").css("cursor", "pointer");
<?php } ?>
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {

        var t = $('#<?php echo $tableID; ?>').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            "iDisplayLength": 15,
            "aLengthMenu": [[15, 50, 100, -1], [15, 50, 100, "All"]],
            columnDefs: [
                {sortable: false, targets: -1},
                {"width": "8%", "targets": -1}
            ],
            "oColVis": {
                "buttonText": "Hide"
            },
            initComplete: function() {
                var api = this.api();

                api.columns().indexes().flatten().each(function(i) {
                    var column = api.column(i);
                    var select = $('<select><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function() {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );

                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });

                    column.data().unique().sort().each(function(d, j) {
                        select.append('<option value="' + stripHTML(d).trim() + '">' + stripHTML(d).trim() + '</option>');
                    });
                });
            },
            "autoWidth": false,
            "sScrollY": "519px",
//            "sScrollX": true,
            "bScrollCollapse": true,
            "bPaginate": true,
            //"bJQueryUI": true,
            "aoColumnDefs": [
                {"sWidth": "10%", "aTargets": [-1]}
            ],
            responsive: true
        });

        t.on('order.dt search.dt', function() {
            t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        //Remove Heading Sorting
//        $("th").unbind();
//        $("th").removeClass("sorting");
//        $("th").removeClass("sorting_asc");

        $(".feedbackmodal<?php echo $tableID; ?>").on("hidden.bs.modal", function() {
            $(".modal-content<?php echo $tableID; ?>").remove();
        });

    });


    function stripHTML(dirtyString) {
        var container = document.createElement('div');
        container.innerHTML = dirtyString;
        return container.textContent;
    }
</script>

<div class="modal fade" id="feedbackmodal<?php echo $tableID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content<?php echo $tableID; ?>"></div>
    </div>
</div> <!-- /Feedback Modal -->

<table id="<?php echo $tableID; ?>" class="table table-bordered table-hover table-responsive"  cellspacing="0"  >
    <thead>
        <tr>
            <th style="width:20px; text-align: center">Rank</th>
            <th>Store</th>
            <th>AD</th>
            <th>PA</th>
            <th>D</th>
            <th>Total</th>
            <th>NPS</th>
            <th>Tier</th>
            <th>No Score</th>
            <th>All Surveys</th>
            <th>Unassigned</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th style="width:20px; text-align: center">Rank</th>
            <th>Store</th>
            <th>AD</th>
            <th>PA</th>
            <th>D</th>
            <th>Total</th>
            <th>NPS</th>
            <th>Tier</th>
            <th>No Score</th>
            <th>All Surveys</th>
            <th>Unassigned</th>
        </tr>
    </tfoot>

    <tbody>
        <?php
        foreach ($MobileData['Mobile'] as $value) {
//            echo "<pre>";
//            print_r($value);
//            exit;
            ?>
            <tr>
                <td style="width:20px; text-align: center"></td>
                <td><?php echo $value['StoreName']; ?></td>
                <td>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps_sso/function/FeedbackModalTLS.php?rate=advo&StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=mobile&frompage=mob<?php echo $tableID; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['MobileTotalAdvocate']; ?>
                    </a>
                <td>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps_sso/function/FeedbackModalTLS.php?rate=pass&StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=mobile&frompage=mob<?php echo $tableID; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['MobileTotalPassive']; ?>
                    </a>
                </td>
                <td>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps_sso/function/FeedbackModalTLS.php?rate=detract&StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=mobile&frompage=mob<?php echo $tableID; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['MobileTotalDetractors']; ?>
                    </a>
                </td>
                <td>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps_sso/function/FeedbackModalTLS.php?StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=mobile&modulator=mobile&frompage=mob<?php echo $tableID; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['MobileTotalSurvey']; ?>
                    </a>
                </td>
                <td><?php echo $value['MobileNpsScore']; ?></td>
                <td><?php echo $value['MobileTierLevel']; ?></td>
                <th>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps_sso/function/FeedbackModalTLS.php?rate=noScore&StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=mobile&frompage=mob<?php echo $tableID; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['MobileNoScore']; ?>
                    </a>
                </th>
                <th>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps_sso/function/FeedbackModalTLS.php?StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=mobile&frompage=mob<?php echo $tableID; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['MobileRawTotal']; ?>
                    </a>
                </th>
                <th>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps_sso/function/FeedbackModalTLS.php?StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=mobile&action5=blank&frompage=mob<?php echo $tableID; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['MobileUnAssigned']; ?>
                    </a>
                </th>


            </tr>
        <?php } ?>
    </tbody>
</table>