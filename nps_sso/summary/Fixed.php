<?php
sleep(2);
error_reporting(0);
session_start();
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/nps_sso/function/NPSFunction.php');

$shared = $_GET['shared'];
$empID = $_GET['empID'];
$profileID = $_SESSION['UserProfileID'];
$role = $_GET['role'];
//$to = date("Y-m-d", strtotime($_GET['to']));
//$from = date("Y-m-d", strtotime($_GET['from']));
$tierRange = NULL;

if (isset($_GET['weekly']) && $_GET['weekly'] == "on") {
    $to = date("Y-m-d");
    // $from = date("Y-m-d", strtotime('4 weeks ago'));
    $from = date("Y-m-d", strtotime('-27 days'));
    $tableID = "tblFixedNpsWeekly";
} elseif (isset($_GET['3months']) && $_GET['3months'] == "on") {
    $to = date("Y-m-d");
    $from = date("Y-m-d", strtotime('-3 Months'));
    $tableID = "tblFixedNps3Months";
} elseif (isset($_GET['htd']) && $_GET['htd'] == "on") {
    $currentMonth = date('m');
    if (($currentMonth >= 1) && ($currentMonth <= 6)) {
        $from = date("Y-01-01");
        $to = date("Y-06-30");
    } else {
        $from = date("Y-07-01");
        $to = date("Y-12-31");
    }

    $tableID = "tblFixedNpsHtd";
} elseif (isset($_GET['3mma']) && $_GET['3mma'] == "on") {
    $to = $_GET['to'];
    $from = date("Y-m-01", strtotime('first day of -2 Month', strtotime($_GET['to'])));
    $tableID = "tblFixedNps3mma";
    $tierRange = "3MMA";
} else {
    $tableID = "tblFixedNps";
    if (isset($_GET['from']) && isset($_GET['to'])) {
        $from = date("Y-m-d", strtotime($_GET['from']));
        $to = date("Y-m-d", strtotime($_GET['to']));
    } else {
        $from = NULL;
        $to = NULL;
    }
}

$storeType = $_GET['storeType'];
switch ($role) {
    case '1':
        $sqlShared = " ";
        break;
    case '2':
    case '3':
        if ($shared == "no") {
            $sqlShared = " AND c.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
        } else {
            $sqlShared = " ";
        }
        break;
    case '4':
    case '5':
        $query = $_SESSION['EmpProfileID'];
//    $type = "StoreLeader";
        $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
        $result = mysql_query($sqlGetOwnerID, $connection);
        while ($row = mysql_fetch_assoc($result)) {
            $data = $row['f_StoreOwnerUserID'];
        }
        $sqlShared = " AND c.f_StoreOwnerUserID = '{$data}' ";
        break;
}
$empid = $_SESSION['EmpProfileID'];
if (isset($_GET['reconOwnerID']) && !is_null($_GET['reconOwnerID'])) {
    $ownerReconID = $_GET['reconOwnerID'];
} else {
    $ownerReconID = NULL;
}   
            
$FixedData = getStoreNpsTLS($shared, $ownerReconID, $empid, $to, $from, $role, $storeType, NULL, 'Fixed', $tierRange);
//$FixedData = getStoreNpsTLS($shared , $to , $from , 'Fixed' , NULL);
//echo $to . " " . $from;
//echo "<pre>";
//print_r($InteractionData);
//exit;
?>

<script type="text/javascript">
    $(document).ready(function() {
        // prepare modal for new data
        $('a.feedback<?php echo $tableID; ?>').click(function(ev) { // prepare modal for new data
            ev.preventDefault();
            var target = $(this).attr('href');
            $("#feedbackmodal<?php echo $tableID; ?> .modal-content<?php echo $tableID; ?>").load(target, function() {
                $("#feedbackmodal<?php echo $tableID; ?>").modal("show");
            });
        });

<?php if ($shared != 'no') { ?>
            $("#tblInteractionNps a").removeAttr("href data-target data-toggle class").css("cursor", "pofix");
<?php } ?>
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        var t = $('#<?php echo $tableID; ?>').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            "iDisplayLength": 15,
            "aLengthMenu": [[15, 50, 100, -1], [15, 50, 100, "All"]],
            columnDefs: [
                {sortable: false, targets: -1},
                {"width": "8%", "targets": -1}
            ],
            "oColVis": {
                "buttonText": "Hide"
            },
            initComplete: function() {
                var api = this.api();

                api.columns().indexes().flatten().each(function(i) {
                    var column = api.column(i);
                    var select = $('<select><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function() {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );

                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });

                    column.data().unique().sort().each(function(d, j) {
                        select.append('<option value="' + stripHTML(d).trim() + '">' + stripHTML(d).trim() + '</option>');
                    });
                });
            },
            "autoWidth": false,
            "sScrollY": "550px",
//            "sScrollX": true,
            "bScrollCollapse": true,
            "bPaginate": true,
            //"bJQueryUI": true,
            "aoColumnDefs": [
                {"sWidth": "10%", "aTargets": [-1]}
            ],
            responsive: true
        });

        t.on('order.dt search.dt', function() {
            t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        //Remove Heading Sorting
//        $("th").unbind();
//        $("th").removeClass("sorting");
//        $("th").removeClass("sorting_asc");

        $(".feedbackmodal<?php echo $tableID; ?>").on("hidden.bs.modal", function() {
            $(".modal-content<?php echo $tableID; ?>").html("");
        });
    });


    function stripHTML(dirtyString) {
        var container = document.createElement('div');
        container.innerHTML = dirtyString;
        return container.textContent;
    }
</script>


<div class="modal fade" id="feedbackmodal<?php echo $tableID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content<?php echo $tableID; ?>"></div>
    </div>
</div> <!-- /Feedback Modal -->

<table id="<?php echo $tableID; ?>" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
    <thead>
        <tr>
            <th style="width:20px; text-align: center">Rank</th>
            <th>Store</th>
            <th>AD</th>
            <th>PA</th>
            <th>D</th>
            <th>Total</th>
            <th>NPS</th>
            <th>Tier</th>
            <th>No Score</th>
            <th>All Surveys</th>
            <th>Unassigned</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th style="width:20px; text-align: center">Rank</th>
            <th>Store</th>
            <th>AD</th>
            <th>PA</th>
            <th>D</th>
            <th>Total</th>
            <th>NPS</th>
            <th>Tier</th>
            <th>No Score</th>
            <th>All Surveys</th>
            <th>Unassigned</th>
        </tr>
    </tfoot>
    <tbody>
        <?php
        foreach ($FixedData['Fixed'] as $value) {
//            echo "<pre>";
//            print_r($value);
//            exit;
            ?>
            <tr>
                <td style="width:20px; text-align: center"></td>
                <td><?php echo $value['StoreName']; ?></td>
                <td>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps_sso/function/FeedbackModalTLS.php?rate=advo&StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=fixed&frompage=fix<?php echo $tableID; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['FixedTotalAdvocate']; ?>
                    </a>
                </td>
                <td>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps_sso/function/FeedbackModalTLS.php?rate=pass&StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=fixed&frompage=fix<?php echo $tableID; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['FixedTotalPassive']; ?>
                    </a>
                </td>
                <td>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps_sso/function/FeedbackModalTLS.php?rate=detract&StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=fixed&frompage=fix<?php echo $tableID; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['FixedTotalDetractors']; ?>
                    </a>
                </td>
                <td>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps_sso/function/FeedbackModalTLS.php?StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=fixed&frompage=fix<?php echo $tableID; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['FixedTotalSurvey']; ?>
                    </a>
                </td>
                <td><?php echo $value['FixedNpsScore']; ?></td>
                <td><?php echo $value['FixedTierLevel']; ?></td>

                <th>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps_sso/function/FeedbackModalTLS.php?rate=noScore&StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=fixed&frompage=fix<?php echo $tableID; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['FixedNoScore']; ?>
                    </a>
                </th>
                <th>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps_sso/function/FeedbackModalTLS.php?StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=fixed&frompage=fix<?php echo $tableID; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['FixedRawTotal']; ?>
                    </a>
                </th>
                <th>
                    <a data-toggle="modal"  class="feedback<?php echo $tableID; ?>" href="nps_sso/function/FeedbackModalTLS.php?StoreID=<?php echo $value['f_StoreID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=fixed&action5=blank&frompage=fix<?php echo $tableID; ?>" data-target="#feedbackmodal<?php echo $tableID; ?>">
                        <i class="fa fa-comments-o fa-fw"></i><?php echo $value['FixedUnAssigned']; ?>
                    </a>
                </th>

            </tr>
        <?php } ?>
    </tbody>
</table>