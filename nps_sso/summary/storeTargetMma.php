<?php
session_start();

include($_SERVER['DOCUMENT_ROOT'] . '/nps_sso/function/NPSFunction.php');


if (isset($_GET['reconOwnerID']) && !is_null($_GET['reconOwnerID'])) {
    $profileID = $_GET['reconOwnerID'];
} else {
    $profileID = $_SESSION['UserProfileID'];
}

$to = $_GET['to'];
$from = $_GET['from'];


if (isset($_GET['weekly']) && $_GET['weekly'] == "on") {
    $to = date("Y-m-d");
    // $from = date("Y-m-d", strtotime('4 weeks ago'));
    $from = date("Y-m-d", strtotime('-27 days'));
    $addonStoreName = "wkly";
    // $tableID = "tblInteractionNpsWeekly";
} elseif (isset($_GET['3months']) && $_GET['3months'] == "on") {
    $to = $_GET['to'];
    $from = date("Y-m-01", strtotime('-3 Months'));
    $tableID = "3Months";
} elseif (isset($_GET['htd']) && $_GET['htd'] == "on") {
    $currentMonth = date('m');

    if (($currentMonth >= 1) && ($currentMonth <= 6)) {

        $to = date("Y-06-30");
        $from = date("Y-01-01");
        // $from = date("Y-m-d", strtotime('-3 Months'));
    } else {
        $from = date("Y-07-01");
        $to = date("Y-12-31");
    }
    $tableID = "htd";
} elseif (isset($_GET['3mma']) && $_GET['3mma'] == "on") {
    $to = $_GET['to'];
    $from = date("Y-m-01", strtotime('first day of -2 Month', strtotime($_GET['to'])));
    $tableID = "3MMA";
} elseif (isset($_GET['ftd']) && $_GET['ftd'] == "on") {
    $to = date("Y-m-d", strtotime('this week monday'));
    $from = date("Y-m-d", strtotime('-13 days', strtotime($to)));
    $tableID = "FTD";
} else {
    // $tableID = "tblInteractionNps";
    $addonStoreName = "";
    if (isset($_GET['from']) && isset($_GET['to'])) {
        $to = $_GET['to'];
        $from = $_GET['from'];
    } else {
        $from = NULL;
        $to = NULL;
    }
}

$shared = $_GET['shared'];
$role = $_GET['role'];
$empID = $_SESSION['EmpProfileID'];
$storeType = $_GET['storeType'];
//Get Employee Store Score Data
$StoreNps = array();

//echo $to . " " . $from . " <br>$tableID to" . $_GET['to'] . " from" . $_GET['from'];
//exit;

$storeNpsData = getStoreNpsTLS($shared, $ownerReconID, $empID, $to, $from, $role, $storeType, $rangeType, "Mobile", "3MMA");
// = getStoreNpsTLS($shared , $to , $from , 'Mobile' , NULL);


foreach ($storeNpsData['Merged'] as $storeData) {
    $StoreNps[] = array(
        'StoreName' => $storeData['StoreName'],
        //episode
        'MobileScore' => number_format($storeData['MobileNPS'], npsDecimal()),
        'MobileTier' => $storeData['MobileTier'],
        'MobileTotalSurvey' => $storeData['MobileTotalSurvey'],
        'MobileAdvocates' => $storeData['MobileAdvocates'],
        'MobilePasives' => $storeData['MobilePasives'],
        'MobileDetractors' => $storeData['MobileDetractors'],
        'MobilePoints' => $storeData['MobileTotalPoints'],
        //interaction
//        'InteractionScore' => number_format($storeData['InteractionNPS'], 0),
//        'InteractionTier' => $storeData['InteractionTier'],
//        'InteractionTotalSurvey' => $storeData['InteractionTotalSurvey'],
//        'InteractionAdvocates' => $storeData['InteractionAdvocates'],
//        'InteractionPasives' => $storeData['InteractionPasives'],
//        'InteractionDetractors' => $storeData['InteractionDetractors'],
        //fixed
        'FixedScore' => number_format($storeData['FixedNPS'], npsDecimal()),
        'FixedTier' => $storeData['FixedTier'],
        'FixedTotalSurvey' => $storeData['FixedTotalSurvey'],
        'FixedAdvocates' => $storeData['FixedAdvocates'],
        'FixedPasives' => $storeData['FixedPasives'],
        'FixedDetractors' => $storeData['FixedDetractors'],
        'FixedPoints' => $storeData['FixedTotalPoints'],
        //episode
        'EpisodeScore' => number_format($storeData['EpisodeNPS'], npsDecimal()),
        'EpisodeTier' => $storeData['EpisodeTier'],
        'EpisodeTotalSurvey' => $storeData['EpisodeTotalSurvey'],
        'EpisodeAdvocates' => $storeData['EpisodeAdvocates'],
        'EpisodePasives' => $storeData['EpisodePasives'],
        'EpisodeDetractors' => $storeData['EpisodeDetractors'],
        'PointsTotal' => $storeData['PointsTotal']
    );
}

function clean($string) {
    $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
?>
<center><h4><i>Report Range from <?php echo $from; ?> to <?php echo $to; ?></i></h4></center>
<table class="table-bordered table-hover" style="margin:auto;" width="50%">
    <thead>
        <tr>
            <th>Metric</th>
            <th>T1</th>
            <th>T2</th>
            <th>T3</th>
            <th>T4</th>
            <th>T5</th>
            <th>T6</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th>Fixed Home</th>
            <td>&ge; 60</td>
            <td>&ge; 53 and &lt; 60</td>
            <td>&ge; 47 and &lt; 53</td>
            <td>&ge; 41 and &lt; 47</td>
            <td>&ge; 23 and &lt; 41</td>
            <td>&lt; 23</td>
        </tr>
        <tr>
            <th>Mobile (on the go)</th>
            <td>&ge; 74</td>
            <td>&ge; 70 and &lt; 74</td>
            <td>&ge; 66 and &lt; 70</td>
            <td>&ge; 61 and &lt; 66</td>
            <td>&ge; 51 and &lt; 61</td>
            <td>&lt; 51</td>
        </tr>
<!--        <tr>
            <th>Fixed Home</th>
            <td>&ge; 58</td>
            <td>&ge; 51 and &lt; 58</td>
            <td>&ge; 45 and &lt; 51</td>
            <td>&ge; 39 and &lt; 45</td>
            <td>&lt; 39</td>
        </tr>
        <tr>
            <th>Mobile (on the go)</th>
            <td>&ge; 72</td>
            <td>&ge; 68 and &lt; 72</td>
            <td>&ge; 64 and &lt; 68</td>
            <td>&ge; 59 and &lt; 64</td>
            <td>&lt; 59</td>
        </tr>-->
    </tbody>
<!--    <tr><td><b><h5>Fixed (Home)</h5></b></td><td>T1 &lt; 39, T2 &ge; 39 and &lt; 45, T3 &ge; 45 and &lt; 51, T4 &ge; 51 and &lt; 58, T5  &ge; 58</td></tr>
    <tr><td><b><h5>Mobile (on the go)<h5></b></td><td>T1 &lt; 59, T2 &ge; 59 and &lt; 64, T3 &ge; 64 and &lt; 68, T4 &ge; 68 and &lt; 72, T5 &ge; 72</td></tr>-->
</table>

<script>
    var divList = $(".listing-item-3MMA");

    function sortMobile() {
        divList.sort(function(a, b) {
            return $(b).data("mobile") - $(a).data("mobile")
        });
        $("#sortNPS3MMA").html(divList);
        $("#sortFixed").removeClass('active');
        $("#sortMobile").addClass('active');
    }

    function sortFixed() {
        divList.sort(function(a, b) {
            return $(b).data("fixed") - $(a).data("fixed")
        });
        $("#sortNPS3MMA").html(divList);
        $("#sortMobile").removeClass('active');
        $("#sortFixed").addClass('active');
    }

</script>
<center>
    <button class="btn btn-xs btn-warning active" id="sortMobile" onclick="sortMobile()">Sort by Mobile</button>
    <button class="btn btn-xs btn-info" id="sortFixed" onclick="sortFixed()">Sort by Fixed</button>
</center>

<?php if (count($StoreNps) > 0) { ?>
    <div class="row"  id="sortNPS3MMA" style="padding-bottom: 15px;">
        <?php
        foreach ($StoreNps as $value) {

//            if (is_null($value['InteractionTotalSurvey'])) {
//                $value['InteractionTotalSurvey'] = 0;
//                $value['InteractionAdvocates'] = 0;
//                $value['InteractionDetractors'] = 0;
//            }
            if (is_null($value['MobileTotalSurvey']) || empty($value['MobileTotalSurvey']) || $value['MobileTotalSurvey'] == 'nan' || $value['MobileTotalSurvey'] == '') {

                $value['MobileTotalSurvey'] = 0;
                $value['MobileAdvocates'] = 0;
                $value['MobileDetractors'] = 0;
            }
            if (is_null($value['FixedTotalSurvey']) || empty($value['FixedTotalSurvey']) || $value['FixedTotalSurvey'] == 'nan' || $value['FixedTotalSurvey'] == '') {

                $value['FixedTotalSurvey'] = 0;
                $value['FixedAdvocates'] = 0;
                $value['FixedDetractors'] = 0;
            }
            if (is_null($value['EpisodeTotalSurvey'])) {

                $value['EpisodeTotalSurvey'] = 0;
                $value['EpisodeAdvocates'] = 0;
                $value['EpisodeDetractors'] = 0;
            }
            ?>
            <div class='col-lg-6 col-md-4 col-sm-6 col-xs-6 listing-item-3MMA'  data-fixed="<?php echo $value['FixedScore']; ?>" data-mobile="<?php echo $value['MobileScore']; ?>" style="text-align: center;">
                <div class="row">
                    <h4 class="pull-left col-lg-12 col-md-12"><i class="fa fa-Home fa-fw"></i><?php
                        echo $value['StoreName'];
                        if (isset($_GET['htd']) && $_GET['htd'] == "on") {
                            echo " (<i>{$value['PointsTotal']} Points</i>)";
                        }
                        // $storeName = str_replace(' ', '', $value['StoreName']);
                        $storeName = clean($value['StoreName']);
                        // echo $storeName;
                        //  $storeName = str_replace('(', '', $storeName);
                        // $storeName = str_replace(')', '', $storeName);
                        ?>
                    </h4>
                </div>
                <div class="row">
                    <div class="col-12-lg">
                        <!--                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 nps-summary-target"  style="text-align: center;">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            Interaction
                                                        </div>
                                                        <div class="panel-body panel-small">
                                                            <h3> <?php echo $value['InteractionScore']; ?> </h3>
                                                            <p><i class="fa fa-trophy"></i> <?php echo $value['InteractionTier']; ?> </p>
                                                        </div>
                                                        <div class="panel-footer">
                                                            Surveys: <?php echo $value['InteractionTotalSurvey']; ?>
                                                        </div>
                                                    </div>
                                                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4  nps-summary-target"  style="text-align: center;">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            Episode
                                                        </div>
                                                        <div class="panel-body panel-small">
                                                            <h3> <?php echo $value['EpisodeScore']; ?> </h3>
                                                            <p><i class="fa fa-trophy"></i> <?php echo $value['EpisodeTier']; ?> </p>
                                                        </div>
                                                        <div class="panel-footer">
                                                            Surveys: <?php echo $value['EpisodeTotalSurvey']; ?>
                                                        </div>
                                                    </div>
                                                </div>-->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6   nps-summary-target"  style="text-align: center;">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Fixed (Home)
                                </div>
                                <div class="panel-body panel-small">
                                    <h3> <?php echo $value['FixedScore']; ?> </h3>
                                    <p><i class="fa fa-trophy"></i> <?php echo $value['FixedTier']; ?> </p>
                                    <?php if (isset($_GET['htd']) && $_GET['htd'] == "on") { ?>  <p><i><?php echo $value['FixedPoints']; ?> Points </i></p> <?php } ?>
                                </div>
                                <div class="panel-footer">
                                    Surveys: <?php echo $value['FixedTotalSurvey']; ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6   nps-summary-target"  style="text-align: center;">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Mobility (On the go)
                                </div>
                                <div class="panel-body panel-small">
                                    <h3> <?php echo $value['MobileScore']; ?> </h3>
                                    <p><i class="fa fa-trophy"></i> <?php echo $value['MobileTier']; ?> </p>
                                    <?php if (isset($_GET['htd']) && $_GET['htd'] == "on") { ?> <p><i><?php echo $value['MobilePoints']; ?> </i>Points</p> <?php } ?>
                                </div>
                                <div class="panel-footer">
                                    Surveys: <?php echo $value['MobileTotalSurvey']; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function() {


                            Number.prototype.between = function(min, max) {
                                return this >= min && this <= max;
                            };

                        });
                    </script>
                    <script type="text/javascript">
                        $(document).ready(function() {

                            var NewTotalSurvey2<?php echo $storeName; ?> = <?php echo $value['MobileTotalSurvey']; ?>;
                            var newInterAdvoTotal2<?php echo $storeName; ?> = <?php echo $value['MobileAdvocates']; ?>;
                            var InteractionTotalDetractorTotal2<?php echo $storeName; ?> = <?php echo $value['MobileDetractors']; ?>;
                            var counter2<?php echo $storeName; ?> = 0;
                            var newDetPercentage2<?php echo $storeName; ?> = 0;
                            var newAdvoPercentage2<?php echo $storeName; ?> = 0;
                            var newIntercationNPS2<?php echo $storeName; ?> = 0;
                            var roundedstoreNPS = 0;
                            while (newIntercationNPS2<?php echo $storeName; ?> < 74) {
                                counter2<?php echo $storeName; ?> += 1;
                                newAdvoPercentage2<?php echo $storeName; ?> = (newInterAdvoTotal2<?php echo $storeName; ?> / NewTotalSurvey2<?php echo $storeName; ?>) * 100;
                                newDetPercentage2<?php echo $storeName; ?> = (InteractionTotalDetractorTotal2<?php echo $storeName; ?> / NewTotalSurvey2<?php echo $storeName; ?>) * 100;
                                newIntercationNPS2<?php echo $storeName; ?> = newAdvoPercentage2<?php echo $storeName; ?> - newDetPercentage2<?php echo $storeName; ?>;
                                roundedstoreNPS = newIntercationNPS2<?php echo $storeName; ?>;
                                roundedstoreNPS = roundedstoreNPS.toFixed(1);
                                if (roundedstoreNPS < 51) {
                                    $("#MobileT5<?php echo $storeName; ?>").empty();
                                    $("#MobileT5<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                    //        console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
                                } else if (roundedstoreNPS >= 51 && roundedstoreNPS < 61) {
                                    $("#MobileT4<?php echo $storeName; ?>").empty();
                                    $("#MobileT4<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                    //        console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
                                } else if (roundedstoreNPS >= 61 && roundedstoreNPS < 66) {
                                    $("#MobileT3<?php echo $storeName; ?>").empty();
                                    $("#MobileT3<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                    //        console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
                                } else if (roundedstoreNPS >= 66 && roundedstoreNPS < 70) {
                                    $("#MobileT2<?php echo $storeName; ?>").empty();
                                    $("#MobileT2<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                    ;
                                    //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 2");
                                } else if (roundedstoreNPS => 70 && roundedstoreNPS < 74) {
                                    $("#MobileT1<?php echo $storeName; ?>").empty();
                                    $("#MobileT1<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                    //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 1");
                                }
                                newInterAdvoTotal2<?php echo $storeName; ?> += 1;
                                NewTotalSurvey2<?php echo $storeName; ?> += 1;
                            }
                            //    var needed = counter - newInterAdvoTotal;

                        });
                    </script>

                    <script type="text/javascript">
                        $(document).ready(function() {

                            var NewTotalSurveyFix<?php echo $storeName; ?> = <?php echo $value['FixedTotalSurvey']; ?>;
                            var newInterAdvoTotalFix<?php echo $storeName; ?> = <?php echo $value['FixedAdvocates']; ?>;
                            var FixedTotalDetractorTotalFix<?php echo $storeName; ?> = <?php echo $value['FixedDetractors']; ?>;
                            var counterFix<?php echo $storeName; ?> = 0;
                            var newDetPercentageFix<?php echo $storeName; ?> = 0;
                            var newAdvoPercentageFix<?php echo $storeName; ?> = 0;
                            var newIntercationNPSFix<?php echo $storeName; ?> = 0;
                            var roundedstoreNPS = 0;
                            var ft4 = true;
                            var ft3 = true;
                            var ft2 = true;
                            var ft1 = true;
                            newAdvoPercentageFix<?php echo $storeName; ?> = (newInterAdvoTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                            newDetPercentageFix<?php echo $storeName; ?> = (FixedTotalDetractorTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                            newIntercationNPSFix<?php echo $storeName; ?> = newAdvoPercentageFix<?php echo $storeName; ?> - newDetPercentageFix<?php echo $storeName; ?>;
                            //console.log(newIntercationNPSFix<?php echo $storeName; ?> + 'current');
                            while (newIntercationNPSFix<?php echo $storeName; ?> < 60) {

                                counterFix<?php echo $storeName; ?> += 1;

                                newAdvoPercentageFix<?php echo $storeName; ?> = (newInterAdvoTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                                newDetPercentageFix<?php echo $storeName; ?> = (FixedTotalDetractorTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                                newIntercationNPSFix<?php echo $storeName; ?> = newAdvoPercentageFix<?php echo $storeName; ?> - newDetPercentageFix<?php echo $storeName; ?>;
                                roundedstoreNPS = newIntercationNPSFix<?php echo $storeName; ?>;
                                roundedstoreNPS = roundedstoreNPS.toFixed(1);
                                // console.log('Store Name: <?php echo $storeName; ?> '+counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier Int")
                                if (roundedstoreNPS < 23) {
                                    $("#FixedT5<?php echo $storeName; ?>").empty();
                                    $("#FixedT5<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                    ft4 = false;
                                    //  console.log(counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier 4");
                                } else if (roundedstoreNPS >= 23 && roundedstoreNPS < 41) {
                                    $("#FixedT4<?php echo $storeName; ?>").empty();
                                    $("#FixedT4<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                    ft3 = false;
                                    //console.log(counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier 3");
                                } else if (roundedstoreNPS >= 41 && roundedstoreNPS < 47) {
                                    $("#FixedT3<?php echo $storeName; ?>").empty();
                                    $("#FixedT3<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                    ft3 = false;
                                    //console.log(counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier 3");
                                } else if (roundedstoreNPS >= 47 && roundedstoreNPS < 53) {
                                    $("#FixedT2<?php echo $storeName; ?>").empty();
                                    $("#FixedT2<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                    ft2 = false;
                                    //   console.log(counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier Int");
                                } else if (roundedstoreNPS >= 53 && roundedstoreNPS < 60) {
                                    $("#FixedT1<?php echo $storeName; ?>").empty();
                                    $("#FixedT1<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                    ft1 = false;
                                    console.log(counterFix<?php echo $storeName; ?> + " NPS: " + newIntercationNPSFix<?php echo $storeName; ?> + " for tier 1");
                                }

                                newInterAdvoTotalFix<?php echo $storeName; ?> += 1;
                                NewTotalSurveyFix<?php echo $storeName; ?> += 1;
                            }

                        });
                    </script>

                    <table class="table table-bordered table-hover table-condensed"  cellspacing="0" style="  width: 92% !important; margin: 0 auto; padding-bottom: 20px;" >
                        <thead>
                            <tr>
                                <th>Needed</th>
                                <th>T1</th>
                                <th>T2</th>
                                <th>T3</th>
                                <th>T4</th>
                                <th>T5</th>
                                <th>T6</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Fixed (Home)</td>
                                <td><div id="FixedT1<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="FixedT2<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="FixedT3<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="FixedT4<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="FixedT5<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="FixedT6<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                            </tr>
                            <tr>
                                <td>Mobility (On the go)</td>
                                <td><div id="MobileT1<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="MobileT2<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="MobileT3<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="MobileT4<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="MobileT5<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="MobileT6<?php echo $storeName; ?>"><i class="fa fa-check"></i></div></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php } ?>
    </div>

<?php } else { ?>
    No Records yet.
<?php } ?>
