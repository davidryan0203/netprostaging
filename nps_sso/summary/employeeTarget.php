<?php
session_start();

include($_SERVER['DOCUMENT_ROOT'] . '/nps_sso/function/NPSFunction.php');

if (isset($_GET['reconOwnerID']) && !is_null($_GET['reconOwnerID'])) {
    $profileID = $_GET['reconOwnerID'];
} else {
    $profileID = $_SESSION['UserProfileID'];
}

$to = $_GET['to'];
$from = $_GET['from'];

if (isset($_GET['weekly']) && $_GET['weekly'] == "on") {
    $to = date("Y-m-d");
// $from = date("Y-m-d", strtotime('4 weeks ago'));
    $from = date("Y-m-d", strtotime('-27 days'));
    $addonEmpName = "wkly";
// $tableID = "tblInteractionNpsWeekly";
} elseif (isset($_GET['3months']) && $_GET['3months'] == "on") {
    $to = $_GET['to'];
    $from = date("Y-m-01", strtotime('-3 Months'));
    $tableID = "3Months";
} elseif (isset($_GET['htd']) && $_GET['htd'] == "on") {
    $currentMonth = date('m');

    if (($currentMonth >= 1) && ($currentMonth <= 6)) {

        $to = date("Y-06-30");
        $from = date("Y-01-01");
// $from = date("Y-m-d", strtotime('-3 Months'));
    } else {
        $from = date("Y-07-01");
        $to = date("Y-12-31");
    }
    $tableID = "htd";
} elseif (isset($_GET['3mma']) && $_GET['3mma'] == "on") {
    $to = $_GET['to'];
    $from = date("Y-m-01", strtotime('first day of -2 Month', strtotime($_GET['to'])));
    $tableID = "3MMA";
} elseif (isset($_GET['ftd']) && $_GET['ftd'] == "on") {
    $to = date("Y-m-d", strtotime('this week monday'));
    $from = date("Y-m-d", strtotime('-13 days', strtotime($to)));
    $tableID = "FTD";
} else {
// $tableID = "tblInteractionNps";
    $addonEmpName = "";
    if (isset($_GET['from']) && isset($_GET['to'])) {
        $to = $_GET['to'];
        $from = $_GET['from'];
    } else {
        $from = NULL;
        $to = NULL;
    }
}

$shared = $_GET['shared'];
$role = $_SESSION['UserRoleID'];
$empID = $_SESSION['EmpProfileID'];
$empType = $_GET['empType'];
//Get Employee Emp Score Data
$EmpNps = array();

$sqlGetTiers = "SELECT * From t_storeTier where f_StoreTypeID = 1 and f_Quarter = 'Q3' ";
$tierResult = mysql_query($sqlGetTiers, $connection);
while ($row = mysql_fetch_assoc($tierResult)) {
    $Tiers[$row['f_NpsType']][] = array("max" => $row["f_NpsMax"], "min" => $row["f_NpsMin"],
        "tier" => $row["f_TierLevel"], "bapps" => $row["f_BappsPoints"], "bappsPercent" => $row["f_BappsPercentage"]);
}
//print_r($_SESSION);
//switch ($role) {
//    case '1':
//        $ownerID = " ";
//        break;
//    case '2':
//    case '3':
//        $ownerID = $_SESSION['UserProfileID'];
//        break;
//    case '4':
//        $query = $_SESSION['EmpProfileID'];
//        $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
//        left join t_empnumber b on a.f_EmpID = b.f_EmpID
//        left join t_storelist c on b.f_StoreID = c.f_StoreID
//        where a.f_EmpID ='{$_SESSION['EmpProfileID']}' group by  c.f_StoreOwnerUserID limit 1 ";
//        $result = mysql_query($sqlGetOwnerID, $connection);
//        while ($row = mysql_fetch_assoc($result)) {
//            $data = $row['f_StoreOwnerUserID'];
//        }
//        $ownerID = $data;
//        break;
//    case '5':
//        $query = $_SESSION['EmpProfileID'];
//        $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
//        left join t_empnumber b on a.f_EmpID = b.f_EmpID
//        left join t_storelist c on b.f_StoreID = c.f_StoreID
//        where a.f_EmpID ='{$_SESSION['EmpProfileID']}' group by  c.f_StoreOwnerUserID limit 1 ";
//        $result = mysql_query($sqlGetOwnerID, $connection);
//        while ($row = mysql_fetch_assoc($result)) {
//            $ownerID = $row['f_StoreOwnerUserID'];
//        }
//        break;
//    case '6':
//        $ownerID = $profileID;
//        break;
//}

switch ($role) {
        case '1':
            $ownerID = " ";
            break;
        case '2':
        case '3':
        case '4':
        case '5':           
                $ownerID =$_SESSION['ClientOwnerID'];          
            break;
        case '6':
            $ownerID = $profileID;
            break;
    }

$sqlGetEmpNPS = "CALL `getTLSChampWithImpact` ($ownerID, '$from', '$to', 'mobile')";

//echo $sqlGetEmpNPS;
mysql_set_charset("UTF8");
$resultGetEmpNPS = mysql_query($sqlGetEmpNPS, $connection);
$i = 0;
while ($rowGetEmpNps = mysql_fetch_assoc($resultGetEmpNPS)) {
    $EmpNps[$i] = array(
        'EmpName' => $rowGetEmpNps['EmpName'],
        'Storename' => $rowGetEmpNps['f_StoreListName'],
        'StaffEmp' => $rowGetEmpNps['EmpName'] . ' ' . $rowGetEmpNps['f_StoreListName'],
        'MobileScore' => number_format($rowGetEmpNps['MobEmpNPS'], npsDecimal()),
        'MobileTotalSurvey' => $rowGetEmpNps['MobEmpTotalSurvey'],
        'MobileAdvocates' => $rowGetEmpNps['MobEmpAdvo'],
        'MobilePasives' => $rowGetEmpNps['MobEmpPas'],
        'MobileDetractors' => $rowGetEmpNps['MobEmpDet'],
        'FixedScore' => number_format($rowGetEmpNps['FixEmpNPS'], npsDecimal()),
        'FixedTotalSurvey' => $rowGetEmpNps['FixEmpTotalSurvey'],
        'FixedAdvocates' => $rowGetEmpNps['FixEmpAdvo'],
        'FixedPasives' => $rowGetEmpNps['FixEmpPas'],
        'FixedDetractors' => $rowGetEmpNps['FixEmpDet']);

    $fixCounter = 0;
    foreach ($Tiers["Fixed"] as $TierData) {
        if ($rowGetEmpNps['FixEmpNPS'] == 'nan' || $rowGetEmpNps['FixEmpNPS'] == '' || empty($rowGetEmpNps['FixEmpNPS'])) {
            $EmpNps[$i]['FixedTier'] = "No Nps Score";
            $EmpNps[$i]['FixedPoints'] = 0;
        } else {
            if ($fixCounter == 0) {
                if ($rowGetEmpNps['FixEmpNPS'] >= $TierData['min']) {
                    $EmpNps[$i]['FixedTier'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
                    $EmpNps[$i]['FixedPoints'] = $TierData['bapps'];
                }
            } elseif ($fixCounter == count($TierData)) {
                if ($rowGetEmpNps['FixEmpNPS'] < $TierData['max']) {
                    $EmpNps[$i]['FixedTier'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
                    $EmpNps[$i]['FixedPoints'] = $TierData['bapps'];
                }
            } else {
                if ($rowGetEmpNps['FixEmpNPS'] >= $TierData['min'] AND $rowGetEmpNps['FixEmpNPS'] <= $TierData['max']) {
                    $EmpNps[$i]['FixedTier'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
                    $EmpNps[$i]['FixedPoints'] = $TierData['bapps'];
                }
            }
        }
        $fixCounter = $fixCounter + 1;
    }


    $mobCounter = 0;
    foreach ($Tiers["Mobile"] as $TierData) {
        if ($rowGetEmpNps['MobEmpNPS'] == 'nan' || $rowGetEmpNps['MobEmpNPS'] == '' || empty($rowGetEmpNps['MobEmpNPS'])) {
            $EmpNps[$i]['MobileTier'] = "No Nps Score";
            $EmpNps[$i]['MobilePoints'] = 0;
        } else {
            if ($mobCounter == 0) {
                if ($rowGetEmpNps['MobEmpNPS'] >= $TierData['min']) {
                    $EmpNps[$i]['MobileTier'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
                    $EmpNps[$i]['MobilePoints'] = $TierData['bapps'];
                }
            } elseif ($mobCounter == count($TierData)) {
                if ($rowGetEmpNps['MobEmpNPS'] < $TierData['max']) {
                    $EmpNps[$i]['MobileTier'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
                    $EmpNps[$i]['MobilePoints'] = $TierData['bapps'];
                }
            } else {
                if ($rowGetEmpNps['MobEmpNPS'] >= $TierData['min'] AND $rowGetEmpNps['MobEmpNPS'] <= $TierData['max']) {
                    $EmpNps[$i]['MobileTier'] = "Tier " . $TierData['tier'] . " - " . $TierData['bappsPercent'] . "% ";
                    $EmpNps[$i]['MobilePoints'] = $TierData['bapps'];
                }
            }
        }

        $mobCounter = $mobCounter + 1;
    }
    $i++;
}

//foreach ($empNpsData as $empData) {
//    $EmpNps[] = array(
//         'StoreName' => $empData['StoreName'],
//        //episode
//        'MobileScore' => number_format($empData['MobileNPS'], 0),
//        'MobileTier' => $empData['MobileTier'],
//        'MobileTotalSurvey' => $empData['MobileTotalSurvey'],
//        'MobileAdvocates' => $empData['MobileAdvocates'],
//        'MobilePasives' => $empData['MobilePasives'],
//        'MobileDetractors' => $empData['MobileDetractors'],
//        'MobilePoints' => $empData['MobileTotalPoints'],
//        //interaction
////        'InteractionScore' => number_format($empData['InteractionNPS'], 0),
////        'InteractionTier' => $empData['InteractionTier'],
////        'InteractionTotalSurvey' => $empData['InteractionTotalSurvey'],
////        'InteractionAdvocates' => $empData['InteractionAdvocates'],
////        'InteractionPasives' => $empData['InteractionPasives'],
////        'InteractionDetractors' => $empData['InteractionDetractors'],
//        //fixed
//        'FixedScore' => number_format($empData['FixedNPS'], 0),
//        'FixedTier' => $empData['FixedTier'],
//        'FixedTotalSurvey' => $empData['FixedTotalSurvey'],
//        'FixedAdvocates' => $empData['FixedAdvocates'],
//        'FixedPasives' => $empData['FixedPasives'],
//        'FixedDetractors' => $empData['FixedDetractors'],
//        'FixedPoints' => $empData['FixedTotalPoints'],
//        //episode
//        'EpisodeScore' => number_format($empData['EpisodeNPS'], 0),
//        'EpisodeTier' => $empData['EpisodeTier'],
//        'EpisodeTotalSurvey' => $empData['EpisodeTotalSurvey'],
//        'EpisodeAdvocates' => $empData['EpisodeAdvocates'],
//        'EpisodePasives' => $empData['EpisodePasives'],
//        'EpisodeDetractors' => $empData['EpisodeDetractors'],
//        'PointsTotal' => $empData['PointsTotal']
//    );
//}

function clean($string) {
    $string = str_replace('-', '', $string); // Replaces all spaces with hyphens.
    $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
//$string = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $string); // Removes special chars.
}
?>
<center><h4><i>Report Range from <?php echo $from; ?> to <?php echo $to; ?></i></h4></center>
<table class="table-bordered table-hover" style="margin:auto;" width="50%">
    <thead>
        <tr>
            <th>Metric</th>
            <th>T1</th>
            <th>T2</th>
            <th>T3</th>
            <th>T4</th>
            <th>T5</th>
            <th>T6</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th>Fixed Home</th>
            <td>&ge; 60</td>
            <td>&ge; 53 and &lt; 60</td>
            <td>&ge; 47 and &lt; 53</td>
            <td>&ge; 41 and &lt; 47</td>
            <td>&ge; 23 and &lt; 41</td>
            <td>&lt; 23</td>
        </tr>
        <tr>
            <th>Mobile (on the go)</th>
            <td>&ge; 74</td>
            <td>&ge; 70 and &lt; 74</td>
            <td>&ge; 66 and &lt; 70</td>
            <td>&ge; 61 and &lt; 66</td>
            <td>&ge; 51 and &lt; 61</td>
            <td>&lt; 51</td>
        </tr>
<!--        <tr>
            <th>Fixed Home</th>
            <td>&ge; 58</td>
            <td>&ge; 51 and &lt; 58</td>
            <td>&ge; 45 and &lt; 51</td>
            <td>&ge; 39 and &lt; 45</td>
            <td>&lt; 39</td>
        </tr>
        <tr>
            <th>Mobile (on the go)</th>
            <td>&ge; 72</td>
            <td>&ge; 68 and &lt; 72</td>
            <td>&ge; 64 and &lt; 68</td>
            <td>&ge; 59 and &lt; 64</td>
            <td>&lt; 59</td>
        </tr>-->
    </tbody>
<!--    <tr><td><b><h5>Fixed (Home)</h5></b></td><td>T1 &lt; 39, T2 &ge; 39 and &lt; 45, T3 &ge; 45 and &lt; 51, T4 &ge; 51 and &lt; 58, T5  &ge; 58</td></tr>
    <tr><td><b><h5>Mobile (on the go)<h5></b></td><td>T1 &lt; 59, T2 &ge; 59 and &lt; 64, T3 &ge; 64 and &lt; 68, T4 &ge; 68 and &lt; 72, T5 &ge; 72</td></tr>-->
</table>

<script>
    var divList = $(".emplisting-item-3MMA");

    function sortEmpMobile() {
        divList.sort(function(a, b) {
            return $(b).data("mobile") - $(a).data("mobile")
        });
        $("#sortEmpNPS3MMA").html(divList);
        $("#sortEmpFixed").removeClass('active');
        $("#sortEmpMobile").addClass('active');
    }

    function sortEmpFixed() {
        divList.sort(function(a, b) {
            return $(b).data("fixed") - $(a).data("fixed")
        });
        $("#sortEmpNPS3MMA").html(divList);
        $("#sortEmpMobile").removeClass('active');
        $("#sortEmpFixed").addClass('active');
    }

</script>
<center>
    <button class="btn btn-xs btn-warning active" id="sortEmpMobile" onclick="sortEmpMobile()">Sort by Mobile</button>
    <button class="btn btn-xs btn-info" id="sortEmpFixed" onclick="sortEmpFixed()">Sort by Fixed</button>
</center>

<?php if (count($EmpNps) > 0) { ?>
    <div class="row"  id="sortEmpNPS3MMA" style="padding-bottom: 15px;">
        <?php
        foreach ($EmpNps as $value) {

//            if (is_null($value['InteractionTotalSurvey'])) {
//                $value['InteractionTotalSurvey'] = 0;
//                $value['InteractionAdvocates'] = 0;
//                $value['InteractionDetractors'] = 0;
//            }
            if (is_null($value['MobileTotalSurvey']) || empty($value['MobileTotalSurvey']) || $value['MobileTotalSurvey'] == 'nan' || $value['MobileTotalSurvey'] == '') {

                $value['MobileTotalSurvey'] = 0;
                $value['MobileAdvocates'] = 0;
                $value['MobileDetractors'] = 0;
            }
            if (is_null($value['FixedTotalSurvey']) || empty($value['FixedTotalSurvey']) || $value['FixedTotalSurvey'] == 'nan' || $value['FixedTotalSurvey'] == '') {

                $value['FixedTotalSurvey'] = 0;
                $value['FixedAdvocates'] = 0;
                $value['FixedDetractors'] = 0;
            }
            if (is_null($value['EpisodeTotalSurvey'])) {

                $value['EpisodeTotalSurvey'] = 0;
                $value['EpisodeAdvocates'] = 0;
                $value['EpisodeDetractors'] = 0;
            }
            ?>
            <div class='col-lg-6 col-md-4 col-sm-6 col-xs-6 emplisting-item-3MMA'  data-fixed="<?php echo $value['FixedScore']; ?>" data-mobile="<?php echo $value['MobileScore']; ?>" style="text-align: center;">
                <div class="row">
                    <h4 class="pull-left col-lg-12 col-md-12"><i class="fa fa-Home fa-fw"></i><?php
                        echo $value['StaffEmp'];
                        if (isset($_GET['htd']) && $_GET['htd'] == "on") {
                            echo " (<i>{$value['PointsTotal']} Points</i>)";
                        }
                        // $empName = str_replace(' ', '', $value['EmpName']);
                        $empName = clean($value['StaffEmp']);
                        //  echo $empName;
                        //echo $empName;
                        //  $empName = str_replace('(', '', $empName);
                        // $empName = str_replace(')', '', $empName);
                        ?>
                    </h4>
                </div>
                <div class="row">
                    <div class="col-12-lg">

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6   nps-summary-target"  style="text-align: center;">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Fixed (Home)
                                </div>
                                <div class="panel-body panel-small">
                                    <h3> <?php echo $value['FixedScore']; ?> </h3>
                                    <p><i class="fa fa-trophy"></i> <?php echo $value['FixedTier']; ?> </p>
                                    <?php if (isset($_GET['htd']) && $_GET['htd'] == "on") { ?>  <p><i><?php echo $value['FixedPoints']; ?> Points </i></p> <?php } ?>
                                </div>
                                <div class="panel-footer">
                                    Surveys: <?php echo $value['FixedTotalSurvey']; ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6   nps-summary-target"  style="text-align: center;">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Mobility (On the go)
                                </div>
                                <div class="panel-body panel-small">
                                    <h3> <?php echo $value['MobileScore']; ?> </h3>
                                    <p><i class="fa fa-trophy"></i> <?php echo $value['MobileTier']; ?> </p>
                                    <?php if (isset($_GET['htd']) && $_GET['htd'] == "on") { ?> <p><i><?php echo $value['MobilePoints']; ?> </i>Points</p> <?php } ?>
                                </div>
                                <div class="panel-footer">
                                    Surveys: <?php echo $value['MobileTotalSurvey']; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function() {


                            Number.prototype.between = function(min, max) {
                                return this >= min && this <= max;
                            };

                        });
                    </script>
                    <script type="text/javascript">
                        $(document).ready(function() {

                            var NewTotalSurvey2<?php echo $empName; ?> = <?php echo $value['MobileTotalSurvey']; ?>;
                            var newInterAdvoTotal2<?php echo $empName; ?> = <?php echo $value['MobileAdvocates']; ?>;
                            var InteractionTotalDetractorTotal2<?php echo $empName; ?> = <?php echo $value['MobileDetractors']; ?>;
                            var counter2<?php echo $empName; ?> = 0;
                            var newDetPercentage2<?php echo $empName; ?> = 0;
                            var newAdvoPercentage2<?php echo $empName; ?> = 0;
                            var newIntercationNPS2<?php echo $empName; ?> = 0;
                            var roundedempNPS = 0;
                            while (newIntercationNPS2<?php echo $empName; ?> < 74) {
                                counter2<?php echo $empName; ?> += 1;
                                newAdvoPercentage2<?php echo $empName; ?> = (newInterAdvoTotal2<?php echo $empName; ?> / NewTotalSurvey2<?php echo $empName; ?>) * 100;
                                newDetPercentage2<?php echo $empName; ?> = (InteractionTotalDetractorTotal2<?php echo $empName; ?> / NewTotalSurvey2<?php echo $empName; ?>) * 100;
                                newIntercationNPS2<?php echo $empName; ?> = newAdvoPercentage2<?php echo $empName; ?> - newDetPercentage2<?php echo $empName; ?>;
                                roundedempNPS = Math.round(newIntercationNPS2<?php echo $empName; ?>);
                                if (roundedempNPS < 51) {
                                    $("#MobileEmpT5<?php echo $empName; ?>").empty();
                                    $("#MobileEmpT5<?php echo $empName; ?>").append(counter2<?php echo $empName; ?>);
                                    //        console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
                                } else if ((roundedempNPS).between(51, 60)) {
                                    $("#MobileEmpT4<?php echo $empName; ?>").empty();
                                    $("#MobileEmpT4<?php echo $empName; ?>").append(counter2<?php echo $empName; ?>);
                                    //        console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
                                } else if ((roundedempNPS).between(61, 65)) {
                                    $("#MobileEmpT3<?php echo $empName; ?>").empty();
                                    $("#MobileEmpT3<?php echo $empName; ?>").append(counter2<?php echo $empName; ?>);
                                    //        console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
                                } else if ((roundedempNPS).between(66, 69)) {
                                    $("#MobileEmpT2<?php echo $empName; ?>").empty();
                                    $("#MobileEmpT2<?php echo $empName; ?>").append(counter2<?php echo $empName; ?>);
                                    ;
                                    //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 2");
                                } else if ((roundedempNPS).between(70, 73)) {
                                    $("#MobileEmpT1<?php echo $empName; ?>").empty();
                                    $("#MobileEmpT1<?php echo $empName; ?>").append(counter2<?php echo $empName; ?>);
                                    //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 1");
                                }
                                newInterAdvoTotal2<?php echo $empName; ?> += 1;
                                NewTotalSurvey2<?php echo $empName; ?> += 1;
                            }
                            //    var needed = counter - newInterAdvoTotal;

                        });
                    </script>

                    <script type="text/javascript">
                        $(document).ready(function() {

                            var NewTotalSurveyFix<?php echo $empName; ?> = <?php echo $value['FixedTotalSurvey']; ?>;
                            var newInterAdvoTotalFix<?php echo $empName; ?> = <?php echo $value['FixedAdvocates']; ?>;
                            var FixedTotalDetractorTotalFix<?php echo $empName; ?> = <?php echo $value['FixedDetractors']; ?>;
                            var counterFix<?php echo $empName; ?> = 0;
                            var newDetPercentageFix<?php echo $empName; ?> = 0;
                            var newAdvoPercentageFix<?php echo $empName; ?> = 0;
                            var newIntercationNPSFix<?php echo $empName; ?> = 0;
                            var roundedempNPS = 0;
                            var ft4 = true;
                            var ft3 = true;
                            var ft2 = true;
                            var ft1 = true;
                            newAdvoPercentageFix<?php echo $empName; ?> = (newInterAdvoTotalFix<?php echo $empName; ?> / NewTotalSurveyFix<?php echo $empName; ?>) * 100;
                            newDetPercentageFix<?php echo $empName; ?> = (FixedTotalDetractorTotalFix<?php echo $empName; ?> / NewTotalSurveyFix<?php echo $empName; ?>) * 100;
                            newIntercationNPSFix<?php echo $empName; ?> = newAdvoPercentageFix<?php echo $empName; ?> - newDetPercentageFix<?php echo $empName; ?>;
                            //console.log(newIntercationNPSFix<?php echo $empName; ?> + 'current');
                            while (newIntercationNPSFix<?php echo $empName; ?> < 60) {

                                counterFix<?php echo $empName; ?> += 1;

                                newAdvoPercentageFix<?php echo $empName; ?> = (newInterAdvoTotalFix<?php echo $empName; ?> / NewTotalSurveyFix<?php echo $empName; ?>) * 100;
                                newDetPercentageFix<?php echo $empName; ?> = (FixedTotalDetractorTotalFix<?php echo $empName; ?> / NewTotalSurveyFix<?php echo $empName; ?>) * 100;
                                newIntercationNPSFix<?php echo $empName; ?> = newAdvoPercentageFix<?php echo $empName; ?> - newDetPercentageFix<?php echo $empName; ?>;
                                roundedempNPS = Math.round(newIntercationNPSFix<?php echo $empName; ?>);
                                // console.log('Emp Name: <?php echo $empName; ?> '+counterFix<?php echo $empName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $empName; ?> +" for tier Int")
                                if (roundedempNPS < 23) {
                                    $("#FixedEmpT5<?php echo $empName; ?>").empty();
                                    $("#FixedEmpT5<?php echo $empName; ?>").append(counterFix<?php echo $empName; ?>);
                                    ft4 = false;
                                    //  console.log(counterFix<?php echo $empName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $empName; ?> +" for tier 4");
                                } else if (roundedempNPS >= 23 && roundedempNPS <= 40) {
                                    $("#FixedEmpT4<?php echo $empName; ?>").empty();
                                    $("#FixedEmpT4<?php echo $empName; ?>").append(counterFix<?php echo $empName; ?>);
                                    ft3 = false;
                                    //console.log(counterFix<?php echo $empName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $empName; ?> +" for tier 3");
                                } else if (roundedempNPS >= 41 && roundedempNPS <= 46) {
                                    $("#FixedEmpT3<?php echo $empName; ?>").empty();
                                    $("#FixedEmpT3<?php echo $empName; ?>").append(counterFix<?php echo $empName; ?>);
                                    ft3 = false;
                                    //console.log(counterFix<?php echo $empName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $empName; ?> +" for tier 3");
                                } else if (roundedempNPS >= 47 && roundedempNPS <= 52) {
                                    $("#FixedEmpT2<?php echo $empName; ?>").empty();
                                    $("#FixedEmpT2<?php echo $empName; ?>").append(counterFix<?php echo $empName; ?>);
                                    ft2 = false;
                                    //   console.log(counterFix<?php echo $empName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $empName; ?> +" for tier Int");
                                } else if (roundedempNPS >= 53 && roundedempNPS <= 59) {
                                    $("#FixedEmpT1<?php echo $empName; ?>").empty();
                                    $("#FixedEmpT1<?php echo $empName; ?>").append(counterFix<?php echo $empName; ?>);
                                    ft1 = false;
                                    console.log(counterFix<?php echo $empName; ?> + " NPS: " + newIntercationNPSFix<?php echo $empName; ?> + " for tier 1");
                                }

                                newInterAdvoTotalFix<?php echo $empName; ?> += 1;
                                NewTotalSurveyFix<?php echo $empName; ?> += 1;
                            }

                        });
                    </script>

                    <table class="table table-bordered table-hover table-condensed"  cellspacing="0" style="  width: 92% !important; margin: 0 auto; padding-bottom: 20px;" >
                        <thead>
                            <tr>
                                <th>Needed</th>
                                <th>T1</th>
                                <th>T2</th>
                                <th>T3</th>
                                <th>T4</th>
                                <th>T5</th>
                                <th>T6</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Fixed (Home)</td>
                                <td><div id="FixedEmpT1<?php echo $empName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="FixedEmpT2<?php echo $empName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="FixedEmpT3<?php echo $empName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="FixedEmpT4<?php echo $empName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="FixedEmpT5<?php echo $empName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="FixedEmpT6<?php echo $empName; ?>"><i class="fa fa-check"></i></div></td>
                            </tr>
                            <tr>
                                <td>Mobility (On the go)</td>
                                <td><div id="MobileEmpT1<?php echo $empName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="MobileEmpT2<?php echo $empName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="MobileEmpT3<?php echo $empName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="MobileEmpT4<?php echo $empName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="MobileEmpT5<?php echo $empName; ?>"><i class="fa fa-check"></i></div></td>
                                <td><div id="MobileEmpT6<?php echo $empName; ?>"><i class="fa fa-check"></i></div></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php } ?>
    </div>

<?php } else { ?>
    No Records yet.
<?php } ?>
