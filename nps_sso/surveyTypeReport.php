<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
session_start();
include('db/connection.php');
include('admin/function/SubFunction.php');
include('nps_sso/function/NPSFunction.php');
/* $sqlgetSubList = "SELECT
  b.f_StoreID, c.f_SurveyName as 'SurveyName' , d.f_SubSurveyName as 'SubSurveyName' , a.f_SubSurveyID
  FROM
  t_surveypd a
  LEFT JOIN
  t_surveysd b ON a.f_SurveyPDID = b.f_SurveyPDID
  LEFT JOIN
  t_surveylist c ON a.f_SurveyID = c.f_SurveyListID
  LEFT JOIN
  t_subsurveylist d ON a.f_SubSurveyID = d.f_SubSurveyID
  LEFT JOIN
  t_storelist e ON b.f_StoreID = e.f_StoreID
  WHERE
  e.f_StoreOwnerUserID = 1
  GROUP BY a.f_SurveyID , a.f_SubSurveyID";

  $result = mysql_query($sqlgetSubList,$connection);
  while($data = mysql_fetch_assoc($result)){
  $rowdata[$data['SurveyName']][] =  array('subname' => $data['SubSurveyName'] , 'id' => $data['f_SubSurveyID'] );
  }

  echo "<pre>";
  print_r($rowdata); */

ini_set('max_execution_time', 300);


if (date("Y-m-01") == date("Y-m-d")) {
    $first_day_this_month = date('01/m/Y', strtotime("-1 month")); // hard-coded '01' for first day
} else {
    $first_day_this_month = date('01/m/Y');
}

//echo date("Y-m-01") . " " . date("Y-m-d");
$last_day_this_month = date('t/m/Y');
error_reporting(E_ALL);

$role = $_SESSION['UserRoleID'];
$UserID = $_SESSION['UserProfileID'];

$surveyList = getSurveyList();

//var_dump($surveyList);


$param = 'All';
if ($role != 1) {
    $param = 'Own';
}
$storeList = SelectStoreList($param);

//var_dump($storeList);
?>

<!--Survey Type Reports-->
<section class='survey-type'>
    <div class='row'>
        <h4 class="page-header"><i class="fa fa-newspaper-o fa-fw"></i>Summary by Survey Type</h4>

        <div class="filters">
            <select name="StoreID" id="storeID" class="form-control">
                <option value="AllStores">Store</option>
                <option value="Merged">Merged</option>
                <?php
                foreach ($storeList as $value) {
                    echo '<option value="' . $value['f_StoreID'] . '">' . $value['f_StoreListName'] . '</option>';
                }
                ?>
            </select>

            <select name="survey" id="surveyTypeID" onChange="getSubSurvey(this.value);" class="form-control">
                <option value="AllType">Survey Type</option>
                <option value="AllType">All Survey</option>
                <?php
                foreach ($surveyList as $country) {
                    ?>
                    <option value="<?php echo $country["f_SurveyListID"]; ?>"><?php echo $country["f_SurveyName"]; ?></option>
                    <?php
                }
                ?>
            </select>

            <select name="subsurvey" id="subsurveylistid" class="form-control">
                <option value="AllSub">Sub Type</option>
                <option value="AllSub">All Sub Survey</option>
            </select>

            <input type="text" id="from" name="from" class="form-control" value="<?php echo $first_day_this_month; ?>" readonly="true"/>
            <input type="text" id="to" name="to"  class="form-control" value="<?php echo $last_day_this_month; ?>" readonly="true"/>

            <button type="submit" class="btn btn-warning btn-circle" id="refresh" data-tooltip="tooltip" data-placement="bottom" title="Reload">
                <i class="fa fa-refresh"></i>
            </button>
        </div>
    </div>

    <div class='row'>
        <h4 class="latest-record">
            <?php
            echo $latestRec['latest_record'];
            ?>
        </h4>
    </div>

    <div  id="page-contents" class="row page-contents" ></div>
</section>

<script type="text/javascript">

    $(document).ready(function() {
        $('#page-contents', function() {
            var addonParams = setLoad();
            var urlToLoad = "nps/surveyTypeSummary.php?" + addonParams;
            var divToHide = "#LogLoader";
            var divToShow = "#page-contents";
            reload(urlToLoad, divToHide, divToShow);
        });


        $("#from").datepicker({
            showOn: "button",
            buttonImage: "https://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
            buttonImageOnly: true, dateFormat: 'dd/mm/yy',
            numberOfMonths: 2,
            onSelect: function(selected) {
                $("#to").datepicker("option", "minDate", selected)
            }
        });

        $("#to").datepicker({
            showOn: "button",
            buttonImage: "https://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
            buttonImageOnly: true, dateFormat: 'dd/mm/yy',
            numberOfMonths: 2,
            onSelect: function(selected) {
                $("#from").datepicker("option", "maxDate", selected)
            }
        });

        $('#refresh').click(function() {
            var toLoad = setLoad();
            var addonParams = toLoad;
            var urlToLoad = "nps/surveyTypeSummary.php?" + addonParams;
            var divToHide = "#LogLoader";
            var divToShow = "#page-contents";
            //console.log(urlToLoad);
            $(divToShow).fadeOut('fast');
            $(divToHide).fadeIn('slow');
            reload(urlToLoad, divToHide, divToShow);
        });

    });

    function reload(urlToLoad, divToHide, divToShow) {
        var urldata = urlToLoad;
        var page = divToShow;
        $.get(urldata).success(
                function(response, status, jqXhr) {
                    $(divToHide).hide();
                    $(page).fadeIn('slow');
                    $(page).empty().append(response);
                }).error(function(response, status, jqXhr) {
            $(divToHide).show();
            reload(urldata, divToHide, divToShow);
        }).complete(function(response, status, jqXhr) {
        });
    }

    function setLoad() {
        var from = $("#from").val().split("/").reverse().join("-");
        var to = $("#to").val().split("/").reverse().join("-");
        //var storeOwnerID = $("#storeOwnerID").val();
        var surveyTypeID = $("#surveyTypeID").val();
        var subSurveyTypeID = $("#subsurveylistid").val();
        var storeID = $("#storeID").val();
        var toLoad = 'to=' + to + '&from=' + from + '&storeID=' + storeID + '&surveyTypeID=' + surveyTypeID + '&subsurveyTypeID=' + subSurveyTypeID;

        return toLoad;
    }

    function getSubSurvey(val) {
        $.ajax({
            type: "POST",
            url: "nps_sso/function/get_subsurvey.php",
            data: 'survey_id=' + val,
            success: function(data) {
                $("#subsurveylistid").html(data);
            }
        });
    }

    $(function() {
        $('[data-tooltip="tooltip"]').tooltip();
    })
</script>




