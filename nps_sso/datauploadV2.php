<?php
ini_set('memory_limit', '256M');
ini_set('upload_max_filesize', '12M');
ini_set('post_max_size', '16M');
//phpinfo();
//exit;
set_time_limit(0);
include ('db/connection.php');
include ('nps_sso/function/UploaderFunction.php');
include ('php-excel/Classes/PHPExcel.php');
include ('php-excel/Classes/PHPExcel/IOFactory.php');
include ('functions/FunctionLog.php');
//$checkLockFile = CheckLockFile();
//Try to read uploaded file
if (isset($_FILES['spreadsheet']) && !is_null($_FILES['spreadsheet']) && !$checkLockFile['exists']) {
    //  CreateLockFile();
    //  Check if a file is uploaded
    if ($_FILES['spreadsheet']['name'] != '') {
        $target_path = "tmp/";
        $target_path = $target_path . basename($_FILES['spreadsheet']['name']);

        if (move_uploaded_file($_FILES['spreadsheet']['tmp_name'], $target_path)) {

        }


        if ($_FILES['spreadsheet']['tmp_name']) {
            if (!$_FILES['spreadsheet']['error']) {

                $inputFile = $target_path;

                $extension = strtoupper(pathinfo($target_path, PATHINFO_EXTENSION));

                if ($extension == 'XLSX' || $extension == 'TMP') {

                    //Read spreadsheeet workbook
                    try {
                        $inputFileType = PHPExcel_IOFactory::identify($inputFile);
                        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                        $objPHPExcel = $objReader->load($inputFile);
                    } catch (Exception $e) {
                        die($e->getMessage());
                    }

                    //Get worksheet dimensions
                    $sheet = $objPHPExcel->getSheet(0);
                    $highestRow = $sheet->getHighestRow();
                    $highestColumn = $sheet->getHighestColumn();
                    // $highestColumn = "CH";
                    $lastColName = $sheet->getcell(CH . 3)->getValue();


                    if ($highestColumn != "CH" || $lastColName != "Time Waiting") {
                        $fileError['message'] = "Invalid Medallia file! Expected Headers didn't matched. <br> Please remove Column after CH (Time Waiting)";
                        unlink($inputFile);
                    } else {

                        //Loop through each row of the worksheet in turn
                        $excelarray = array();
                        $row = 4;

                        // Check if date today is not greater than 90 days of the upload data
                        $firstDate = strtotime(date("Y-m-d"));
                        $lastDate = strtotime($sheet->getcell(BN . $row)->getValue());
                        $dateDifference = ($lastDate - $firstDate) / 60 / 60 / 24;

                        // echo $dateDifference;
                        //  exit;
                        /*                      $firstDate = strtotime($sheet->getcell(BN . $highestRow)->getValue());
                          echo $firstDate;
                          echo "<br>" . strtotime(date("Y-m-d"));
                          exit;
                          echo date("Y-m-d", strtotime($sheet->getcell(BN . $row)->getValue())) . "<br>";
                          echo date("Y-m-d", strtotime($sheet->getcell(BN . $highestRow)->getValue()));



                          echo $dateDifference;
                          echo $firstDate . " " . $lastDate;
                          if ($dateDifference < 90) {
                          echo 'Data supported';

                          DeleteLockFile();
                          unlink($inputFile);
                          exit;
                          } else {
                          echo 'data over 90 days';
                          DeleteLockFile();
                          unlink($inputFile);
                          exit;
                          } */
                        if ($dateDifference < 90) {


                            while ($row <= $highestRow) {
                                //  Read a row of data into an array
                                $key = $sheet->getcell(A . $row)->getValue();
                                // Get invitationID to remove already existing data from upload process
                                $excelarray[$key] = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                                $dealerCodes[$sheet->getcell(AU . $row)->getValue()] = $sheet->getcell(AU . $row)->getValue();
                                $row++;
                            }
                            // Check invitationID to remove already existing data from upload process
                            $excelKeys = join("','", array_keys($excelarray));
                            $getExisting = "SELECT f_InvitationID AS inserted FROM t_surveypd WHERE f_InvitationID IN ('{$excelKeys}')  ";
                            $resultCheckExist = mysql_query($getExisting, $connection);
                            while ($data = mysql_fetch_assoc($resultCheckExist)) {
                                $inserted[$data['inserted']][] = $data['inserted'];
                            }

                            if (count($inserted) != 0) {
                                $excelarrayNew = array_diff_key($excelarray, $inserted);
                            } else {
                                $excelarrayNew = $excelarray;
                            }


                            $dealerCodeKeys = join("','", array_keys($dealerCodes));
                            $selectCodes = "select f_CompanyCode as 'StoreName' from t_storelist where f_CompanyCode in ('{$dealerCodeKeys}') "
                                    . " UNION "
                                    . "select f_MergeToCompanyCode as 'StoreName' from t_mergereport where f_MergeToCompanyCode in ('{$dealerCodeKeys}')";
                            $resultCodes = mysql_query($selectCodes, $connection);

                            while ($codes = mysql_fetch_assoc($resultCodes)) {
                                $storeCode[$codes['StoreName']][] = $codes['StoreName'];
                            }
                            $notOnNetPro = array_diff_key($dealerCodes, $storeCode);
                        } else {
                            $fileError['message'] = "Uploading surveys is restricted to the pass 90 days. <br> Please remove old survey and try again.";

                            // echo $fileError['message'];
                            // DeleteLockFile();
                            unlink($inputFile);
                        }
                    }
                } else {
                    $fileError['message'] = "Invalid Format! Please upload Medallia Excel File only";
                    // DeleteLockFile();
                    unlink($inputFile);
                }
            } else {
                $fileError['message'] = $_FILES['spreadsheet']['error'];
                //  DeleteLockFile();
                unlink($inputFile);
            }
        }
    } else {
        $fileError['message'] = 'No selected file. Please upload medallia excel file.';
    }
} else {
    $fileError['message'] = 'Select Medallia file to upload.';
}



if (!is_null($excelarrayNew) && empty($fileError)) {

//Prepare  insert for T_SurveyPD
    $sqlSurveyPD = "INSERT INTO t_surveypd (f_InvitationID, f_CustomerDetailsID, f_SurveyID, f_TelstraSurveyProgramID, f_SubSurveyID, f_EpisodeReferenceNo, f_EpisodeSourceID, f_EpisodeStartDate, "
            . "f_EpisodeEndDate, f_SurveyMethodID, f_AskFieldQuestion, f_AskRetailQuestion, f_CustomerBillingAccountID, f_CustomerBusinessUnitID,f_CustomerMBMSegment, f_Product1, f_Product2, "
            . "f_Product3, f_Product4, f_Product5, f_Product6, f_Product7, f_Product8, f_Product9, f_Product10, f_ProductID) VALUES ";

//Prepare insert for T_SurveySD
    $sqlSurveySDmain = "INSERT INTO t_surveysd (f_SurveyPDID,f_UserID1,  f_UserID7, f_UserID8, f_UserID9, f_UserID10, f_UserID11, f_UserID12, f_DealerCode, "
            . "f_StoreID, f_OriginationChannelID, f_ActivationExtra4, f_ActivationExtra5, f_BillFormat, f_BillMethod, f_AssuranceLoggingSystem, f_AssuranceArea, f_AssuranceSubArea, f_AssuranceSymptom, "
            . "f_ComplaintTIOPriorityLevel, f_ComplaintArea, f_ComplaintReason, f_ComplaintSubReason, f_ComplaintSource, f_DisconnectionStatus, f_InteractionLog, f_SurveyID, "
            . "f_DateTimeCreatedMelbourne, f_DateTimeResponseMelbourne, f_IssueResolved, f_RecommendRate, f_CustomerReason, f_ResolutionPreference, f_FollowUp, f_CallbackNumberCombined, "
            . "f_IssueDescription, f_LikelihoodToRecommendField, f_LikelihoodToRecommendRetail, f_IndustryCode1, f_IndustryCode2, f_SalesPortfolioCode, f_SalesGroup, "
            . "f_BusinessUltimateCIDN, f_BusinessMetroRegional, f_BusinessManagedStatus, f_LikelihoodToRecommendAE, f_ReportingFlagDashboards, f_TimeWaiting , f_LikelihoodToRecommendFollowUp) VALUES ";

//Prepare insert for t_customerDetails
    $sqlCustomerDetails = "INSERT INTO t_customerdetails (f_CustomerID, f_CustomerSalutationTitle, f_CustomerFirstName, f_CustomerLastname, f_CustomerMobile, f_CustomerEmail, f_CustomerPhoneNumber, "
            . "f_CustomerState, f_CustomerPostCode) VALUES ";

//Extract Excel Row 1 by 1
    $returnData = array();
    foreach ($excelarrayNew as $value) {


        $returnData = PrepareExcelArray($value);

        if ($returnData['exisiting'] == 'yes') {
            //   echo "was here";
            //  echo $returnData['f_InvitationID']."part1<br>";
            continue;
        } else {
            //  echo "was not";
        }

        if (is_null($returnData['f_StoreID'])) {
            $returnData['StoreNotCreated'][] = $returnData['f_DealerCode'];
            continue;
        } else {

        }

//Excel Values for t_SurveyPD
        $sqlSurveyPD .= "('" . $returnData['f_InvitationID'] . "', '" . $returnData['f_CustomerDetailsID'] . "', '" . $returnData['f_SurveyListID'] . "', '" . $returnData['f_TelstraSurveyProgramID'] . "',"
                . "'" . $returnData['f_SubSurveyID'] . "', '" . $returnData['f_EpisodeReferenceNo'] . "', '" . $returnData['f_EpisodeSourceID'] . "', '" . $returnData['f_EpisodeStartDate'] . "',"
                . "'" . $returnData['f_EpisodeEndDate'] . "', '" . $returnData['f_SurveyMethodID'] . "', '" . $returnData['f_AskFieldQuestion'] . "', '" . $returnData['f_AskRetialQuestion'] . "',"
                . "'" . $returnData['f_CustomerBillingAccountID'] . "', '" . $returnData['f_CustomerBusinessUnitID'] . "', '" . $returnData['f_CustomerMBMSegment'] . "', '" . $returnData['f_Product1'] . "',"
                . "'" . $returnData['f_Product2'] . "', '" . $returnData['f_Product3'] . "', '" . $returnData['f_Product4'] . "', '" . $returnData['f_Product5'] . "', '" . $returnData['f_Product6'] . "',"
                . "'" . $returnData['f_Product7'] . "', '" . $returnData['f_Product8'] . "', '" . $returnData['f_Product9'] . "', '" . $returnData['f_Product10'] . "', '" . $returnData['f_ProductID'] . "'),";

//Excel Values for t_SurveySD
        $array[$returnData['f_InvitationID']] = array($returnData['f_UserID1'], $returnData['f_UserID7'], $returnData['f_UserID8'], $returnData['f_UserID9'],
            $returnData['f_UserID10'], $returnData['f_UserID11'], $returnData['f_UserID12'], $returnData['f_DealerCode'], $returnData['f_StoreID'], $returnData['f_OriginationChannelID'],
            $returnData['f_ActivationExtra4'], $returnData['f_ActivationExtra5'], $returnData['f_BillFormat'], $returnData['f_BillMethod'],
            $returnData['f_AssuranceLoggingSystem'], $returnData['f_AssuranceArea'], $returnData['f_AssuranceSubArea'], $returnData['f_AssuranceSymptom'], $returnData['f_ComplaintTIOPriorityLevel'],
            $returnData['f_ComplaintArea'], $returnData['f_ComplaintReason'], $returnData['f_ComplaintSubReason'], $returnData['f_ComplaintSource'],
            $returnData['f_DisconnectionStatus'], $returnData['f_InteractionLog'], $returnData['f_SurveyID'], $returnData['f_DateTimeCreatedMelbourne'],
            $returnData['f_DateTimeResponseMelbourne'], $returnData['f_IssueResolved'], $returnData['f_RecommendRate'], $returnData['f_CustomerReason'],
            $returnData['f_ResolutionPreference'], $returnData['f_FollowUp'], $returnData['f_CallbackNumberCombined'], $returnData['f_IssueDescription'],
            $returnData['f_LikelihoodToRecommendField'], $returnData['f_LikelihoodToRecommendRetail'], $returnData['f_IndustryCode1'],
            $returnData['f_IndustryCode2'], $returnData['f_SalesPortfolioCode'], $returnData['f_SalesGroup'], $returnData['f_BusinessUltimateCIDN'],
            $returnData['f_BusinessMetroRegional'], $returnData['f_BusinessManagedStatus'], $returnData['f_LikelihoodToRecommendAE'],
            $returnData['f_ReportingFlagDashboards'], $returnData['f_TimeWaiting'], $returnData['f_LikelihoodToRecommendFollowUp']);
    }

//Remove , on last row
    if (count($array) >= 1) {
        $sqlSurveyPD = trim($sqlSurveyPD, ",");

//Insert records to tables
        $resultsurveypd = InsertSurveyPD($sqlSurveyPD);

        $getSurveypdID = getSurveypdID($array);

        foreach ($array as $key => $checkvalue) {
            array_unshift($array[$key], $getSurveypdID[$key]);
        }

        $sqlSurveySD = "";
        foreach ($array as $toStringValue) {
            $sqlSurveySD .= "(";
            foreach ($toStringValue as $key => $toQuote) {

                //echo $key. "qoutes " . $toQuote  ."<br>";
                $noQoutes = array('30', '36', '37', '48');
                if (in_array($key, $noQoutes)) {
                    if (is_null($toQuote)) {
                        $sqlSurveySD .= "NULL,";
                    } else {
                        $sqlSurveySD .= "" . $toQuote . ",";
                    }
                } else {

                    $sqlSurveySD .= "'" . mysql_real_escape_string($toQuote) . "',";
                }
            }
            $sqlSurveySD = trim($sqlSurveySD, ",");
            $sqlSurveySD .= "),";
        }
        $sqlSurveySD = trim($sqlSurveySD, ",");

        //  var_dump($array);
        $surveySDquery = $sqlSurveySDmain . $sqlSurveySD;
        $resultsurveySd = InsertSurveySD($surveySDquery);
        // DeleteLockFile();
        unlink($inputFile);
    } else {
        $resultsurveySd['message'] = 'Records already exist!';
        //  DeleteLockFile();
        unlink($inputFile);
    }
}

if (isset($_POST['deleteBtn']) && $_POST['deleteBtn'] == 'Delete Lock File') {
    // DeleteLockFile();
    unlink($inputFile);
    header("Refresh:0");
}
?>

<div class="row">
    <div class="col-lg-12 col-md-offset-3">
        <h3 class="page-header text-center"  style="margin-bottom: 20px;"> Medallia Excel File Import</h3>
    </div><!--End page-header-->

    <div class="col-lg-7 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel panel-default">
                <div class="panel-heading"><strong>Excel file Uploader</strong></div>
                <div class="panel-body">
                    <!-- Progress Bar -->
                    <div class="progress" style="height: 29px">
                        <div class="progress-bar progress-bar-striped" id='progress-bar' role="progressbar"
                             aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
                            <h4 style="padding-top: 3px;" id='status'>Status</h4>
                        </div>
                    </div>

                    <!-- Standar Form -->
                    <form method="post" enctype="multipart/form-data">
                        <div class="form-inline">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group upload">
                                        <input  type="file" name="spreadsheet"/>
                                    </div>
                                </div>
                                <?php
//                                $setButton = "";
//                                if ($checkLockFile['exists']) {
//                                    $setButton = "disabled";
//                                    if ($_SESSION['UserRoleID'] == 1) {
//                                        echo "<input type=\"submit\" name=\"deleteBtn\" id=\"deleteBtn\" value=\"Delete Lock File\"/> ";
//                                    }
//                                }
                                ?>
                                <div class="col-md-4">
                                    <input class="btn btn-sm btn-success" id="upload" type='submit' name="submit" value="Upload Data" <?php echo $setButton; ?> />
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
            <!--End uploader-->
        </div>

        <?php
        if ($resultsurveySd['message'] != 'Records already exist!') {
            $class = "alert alert-success ";
            $msg = "Success!";
            $message = $resultsurveypd['message'] . "<br>" . $resultsurveySd['message'];
        } else {
            $class = "alert alert-danger ";
            $msg = "Failed!";
            $message = $resultsurveySd['message'];
        }
        ?>
        <?php if (!is_null($excelarrayNew)) { ?>
            <br>
            <div class="<?php echo $class; ?>">
                <i class="fa fa-exclamation-circle fa-fw"></i><strong><?php echo $msg; ?></strong>
                <br>
                <?php
                $InsertedCol = $highestRow - 3;
                $IntertedColumn = $message . " <br> Total Survey(s): " . $InsertedCol;
                echo $IntertedColumn;
                ?>
            </div>
            <?php
            if ($msg == 'Success!') {
                $logRemarks = json_encode(array('RawCount' => $InsertedCol, 'PD' => $resultsurveypd, 'SD' => $resultsurveySd));
                InsertLog('2', '4', $logRemarks, $_SESSION['LoginID'], 'Success', '6');
            } elseif ($msg == 'Failed!') {
                $logRemarks = json_encode(array('RawCount' => $InsertedCol));
                InsertLog('2', '4', $logRemarks, $_SESSION['LoginID'], 'Failed', '6');
            }
            ?>
            <?php
            if (count($notOnNetPro) >= 1) {
                //  $storeNotCreated = array_unique($returnData['StoreNotCreated']);
                $class = "alert alert-danger fadin";
                $message = $resultsurveySd['message'];
                ?>
                <br>
                <div class="<?php echo $class; ?>">
                    <i class="fa fa-exclamation-circle fa-fw"></i><strong>Store/s not existing in our list <?php echo implode(', ', $notOnNetPro); ?> ! <br> Please contact the Administrator</strong>
                </div>

            <?php } ?>
            <?php
        }

        if (count($inserted) > 0) {
            ?>

            <div class=alert alert-warning ">
                 <i class="fa fa-exclamation-circle fa-fw"></i><strong><?php echo count($inserted); ?> surveys already exits VS . <?php echo $InsertedCol; ?> lines from file</strong>
            </div>
            <?php
        }

        if (!is_null($fileError)) {
            ?>
            <div class="alert alert-danger ">
                <i class="fa fa-exclamation-circle fa-fw"></i><strong><?php echo $fileError['message']; ?></strong>
            </div>
            <?php
        }

        if ($checkLockFile['exists']) {
            $lockfileBy = $checkLockFile['details']['f_LockFileBy'];
            ?>
            <br>
            <div class="alert alert-danger">
                <i class="fa fa-exclamation-circle fa-fw"></i><strong><?php echo $lockfileBy; ?> is currently uploading a file please, try again later.</strong>
            </div>
        <?php } ?>

    </div> <!-- end col-5-->
</div><!-- end row-->


<script type="text/javascript">
    $(document).ready(function() {
        window.setTimeout(function() {
            $(".alert").show().fadeTo(1500, 0).slideUp(500, function() {
                $(this).remove();
            });
        }, 10000);


        $('#upload').click(function() {
            //$('#upload').prop( "disabled", true );
            $('#progress-bar').addClass('progress-bar-info active');
            $('#status').text('Uploading...').addClass('animated pulse infinite');
        });
    });
</script>




