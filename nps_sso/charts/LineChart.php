<?php
//session_start();
error_reporting(E_ALL);
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/nps_sso/function/NPSFunction.php');
date_default_timezone_set('America/Denver');
$to = date("Y-m-d", strtotime($_GET['to']));
$from = date("Y-m-d", strtotime($_GET['from']));
$role = $_SESSION['UserRoleID'];
$range = date("d/m/Y", strtotime($_GET['from'])) . " to " . date("d/m/Y", strtotime($_GET['to']));
$storeType = $_GET['storeType'];
$storeID = $_GET['storeID'];
$surveyType = $_GET['surveyTypeID'];
$subsurveyType = $_GET['subsurveyTypeID'];

if (!is_null($to) && !is_null($from)) {
    $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
} else {
    $sqlWhen = " AND MONTH(a.f_DateTimeResponseMelbourne) = MONTH(CURDATE())
AND YEAR(a.f_DateTimeResponseMelbourne) = YEAR(CURDATE())";
}
$groupby = array();
if ($storeID == 'AllStores') {
    $storeIDsql = " ";
    $groupby[] = "c.f_StoreID";
} elseif ($storeID == 'Merged') {
    $storeIDsql = " ";
} else {
    $storeIDsql = " AND a.f_StoreID = '{$storeID}'";
}
$surveyTypesql = " ";
$subsurveyTypesql = " ";
$subSurveyGroup = " ";
if (!is_null($surveyType)) {
    if ($surveyType == 'AllType') {
        $subSurveyGroup = ",b.f_SubSurveyID ";
    } else {
        $surveyTypesql = " AND b.f_SurveyID = '{$surveyType}' ";
    }
}
if (!is_null($subsurveyType)) {
    if ($subsurveyType == 'AllSub') {
        $groupby[] = "b.f_SubSurveyID";
    } else {
        $subsurveyTypesql = " AND b.f_SubSurveyID = '{$subsurveyType}' ";
    }
}
$groupto = implode(", ", $groupby);


 switch ($role) {
        case '1':
            $sqlShared = " ";
            break;
        case '2':
        case '3':
        case '4':
        case '5':
            if ($shared != "no") {
                $sqlShared = " ";
            } else {
                $sqlShared = " AND c.f_StoreOwnerUserID = '{$_SESSION['ClientOwnerID']}' ";
            }

            break;
        case '6':
            $sqlShared = " AND c.f_StoreOwnerUserID = '{$profileID}' ";
            break;
    }


//switch ($role) {
//    case '1':
//        $sqlShared = " ";
//        break;
//    case '2':
//    case '3':
//        $sqlShared = " AND c.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
//        break;
//    case '4':
//    case '5':
//        $query = $_SESSION['EmpProfileID'];
////    $type = "StoreLeader";
//        $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
//        left join t_empnumber b on a.f_EmpID = b.f_EmpID
//        left join t_storelist c on b.f_StoreID = c.f_StoreID
//        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
//        $result = mysql_query($sqlGetOwnerID, $connection);
//        while ($row = mysql_fetch_assoc($result)) {
//            $dataOnwerID = $row['f_StoreOwnerUserID'];
//        }
//        $sqlShared = " AND c.f_StoreOwnerUserID = '{$dataOnwerID}' ";
//        break;
//}

$sql = " SELECT
    d.f_SurveyName AS SurveyName,
    d.f_SurveyListID,
    e.f_SubSurveyName AS SubSurveyName,
    e.f_SubSurveyID,
    c.f_StoreID,
    CASE
        WHEN c.f_StoreListName IS NULL THEN a.f_DealerCode
        ELSE c.f_StoreListName
    END AS StoreName,
    ((SUM(CASE
        WHEN
            (a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                AND a.f_ActivationExtra5 != 'P000000'
        THEN
            1
        ELSE 0
    END) - SUM(CASE
        WHEN
            (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                AND a.f_ActivationExtra5 != 'P000000'
        THEN
            1
        ELSE 0
    END)) * 100 / SUM(CASE
        WHEN
            a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                AND a.f_ActivationExtra5 != 'P000000'
        THEN
            1
        ELSE 0
    END)) AS InteractionNPS,
    ((SUM(CASE
        WHEN (a.f_RecommendRate BETWEEN '9' AND '10') THEN 1
        ELSE 0
    END) - SUM(CASE
        WHEN (a.f_RecommendRate BETWEEN '0' AND '6') THEN 1
        ELSE 0
    END)) * 100 / SUM(CASE
        WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
        ELSE 0
    END)) AS EpisodeNPS,
    SUM(CASE
        WHEN
            a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                AND a.f_ActivationExtra5 != 'P000000'
        THEN
            1
        ELSE 0
    END) AS InteractionTotalSurvey,
    SUM(CASE
        WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
        ELSE 0
    END) AS EpisodeTotalSurvey,
    COUNT(a.f_SurveySDID) AS RawTotal,
    date_format(f_DateTimeResponseMelbourne, '%Y-%u') as Date,
    yearweek(f_DateTimeResponseMelbourne,1) as YearWeek
FROM
    t_surveysd a
        LEFT JOIN
    t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN
    t_storelist c ON a.f_StoreID = c.f_StoreID
        LEFT JOIN
    t_surveylist d ON b.f_SurveyID = d.f_SurveyListID
        LEFT JOIN
    t_subsurveylist e ON b.f_SubSurveyID = e.f_SubSurveyID
WHERE
    1 = 1 AND c.f_Status = 1
       {$sqlShared}
        {$sqlWhen}
            {$storeIDsql} {$surveyTypesql} {$subsurveyTypesql}
GROUP BY $groupto, YearWeek
ORDER BY  YearWeek ASC ";
$results = mysql_query($sql, $connection);

while ($row = mysql_fetch_assoc($results)) {
    $data[] = $row;
    $subSuryName = '';

    if ($row['SurveyName'] = 'Activation' && $row['SubSurveyName'] == '') {
        $subSuryName = 'Activation';
    } else {
        $subSuryName = $row['SubSurveyName'];
    }
    $surveyList[$row['StoreName']][$row['SurveyName']][$subSuryName][$row['Date']] = $row['EpisodeNPS'];

    if (!in_array($row['Date'], $cat)) {
        $cat[] = $row['Date'];
    }
}
//print_r($row);
//exit;
foreach ($surveyList as $storeName => $storeNameData) {

    foreach ($storeNameData as $surveyName => $surveyNameData) {

        foreach ($surveyNameData as $subSurvey => $subSurveyData) {
            $yearWeekData = array();
            foreach ($cat as $yearWeek) {

                $yearWeekData[] = intval($subSurveyData[$yearWeek]);
            }
            if ($storeID == "Merged") {
                $storeName = "Merged";
            }
            $newData[] = array('name' => $storeName . ' ' . $subSurvey, 'data' => $yearWeekData);
        }
    }
}


// 'Store - SubSurveyType' - 'IntScore'
//Series - Name: SubSurvey
// Data: 0 0 0 0 0 0
//echo "<pre>";
//print_r($newData);
//$cat = ($cat);
//
//print_r($cat);
$cat = json_encode($cat);
?>

<script type="text/javascript">
    $(function() {
        // Radialize the colors


        var chart3;
        $(document).ready(function() {

            // console.log(' '? > );
            chart3 = new Highcharts.Chart({
                chart: {
                    renderTo: 'LineChart',
                    //type: 'column',

                    backgroundColor: 'rgba(255, 255, 255, 0.67)'
                },
                title: {
                    text: 'Episode Survey Type'
                },

                xAxis: {
                    categories: <?php echo $cat; ?>,
                    title: {
                        text: 'Year - (Number of Week)'
                    }

                },
                yAxis: {
                    title: {
                        text: 'NPS'
                    }
                },

                series: <?php echo json_encode($newData); ?>,

                responsive: {
                    rules: [{
                            condition: {
                                maxWidth: 800
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',

                },
                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                    }
                },

            });

        });
    });
</script>


<div class="" id="LineChart"></div>



