<?php
session_start();
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/nps_sso/function/NPSFunction.php');
date_default_timezone_set('America/Denver');

$shared = $_GET['shared'];
$role = $_GET['role'];
$rangeType = $_GET['rangeType'];

if ($rangeType == 'mtd') {
    $to = mysql_real_escape_string($_GET['mtdTo']);
    $from = mysql_real_escape_string($_GET['mtdFrom']);
    $range = date("d/m/Y", $from) . " to " . date("d/m/Y", $to);
} else {
    $to = $_GET['toTBC'];
    $format = 'M-y';
    $date = DateTime::createFromFormat($format, $to);
    $to = $date->format('Y-m-d');
    $from = date("Y-m-d", strtotime($_GET['from']));
    $range = date("d/m/Y", $from) . " to " . date("d/m/Y", $to);
}

$storeType = $_GET['storeType'];

$empid = $_SESSION['EmpProfileID'];

if (isset($_GET['reconOwnerID']) && !is_null($_GET['reconOwnerID'])) {
    $ownerReconID = $_GET['reconOwnerID'];
} else {
    $ownerReconID = NULL;
}
$tierSort = $_GET['tiersort'];
$StoreNpsScores = getStoreNpsTBC($shared, $ownerReconID, $empid, $to, $from, $role, $storeType, $rangeType, $tierSort);

foreach ($StoreNpsScores['Merged'] as $value) {
    $storeList[] = $value['StoreName'];
    $MobileEncodeArray[] = array('y' => intval($value['MobileNPS']), 'tierlevel' => $value['MobileTier'], 'color' => "#fe8e20");
    $EpisodeEncodeArray[] = array('y' => intval($value['EpisodeNPS']), 'tierlevel' => $value['EpisodeTier'], 'color' => "#ffd700");
    $FixedEncodeArray[] = array('y' => intval($value['FixedNPS']), 'tierlevel' => $value['FixedTier'], 'color' => "#2090fe");
}

$category = json_encode($storeList);
$episode = json_encode($EpisodeEncodeArray);
$mobile = json_encode($MobileEncodeArray);
$fixed = json_encode($FixedEncodeArray);
//echo $category;
?>

<script type="text/javascript">
    $(function() {
        // Radialize the colors
        Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function(color) {
            return {
                radialGradient: {cx: 0.5, cy: 0.3, r: 0.7},
                stops: [
                    [0, color],
                    [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
                ]
            };
        });

        var chart2;
        $(document).ready(function() {


            chart2 = new Highcharts.Chart({
                credits: {
                    enabled: false
                },
                chart: {
                    renderTo: 'Tier',
                    type: 'column',
                    marginRight: 20,
                    marginBottom: 75,
                    backgroundColor: 'rgba(255, 255, 255, 0.67)'
                },
                title: {
                    style: {
                        color: '#141B50',
                        font: 'bold 17px "Trebuchet MS", Verdana, sans-serif'
                    },
                    text: 'Mobility Episode vs Fixed Episode',
                    x: -20 //center
                },
                subtitle: {
                    text: '',
                    x: -20
                },
                yAxis: [{
                        allowDecimals: false,
                        // "id": "0",
                        //reversed: true,
                        min: -100,
                        max: 100,
                        title: {
                            text: ''
                        }}],
                legend: {
                    shadow: false
                },
                xAxis: {
                    min: 0,
                    max: 5,
                    //"id": "0",
                    categories: <?php echo $category; ?>
                    //reversed: true
                }, scrollbar: {
                    enabled: true
                },
                tooltip: {
                    formatter: function() {
                        return '<b>' + this.series.name + ' : ' + this.y + '</b><br>' +
                                this.x + ': ' + this.point.tierlevel;
                    }

                },
                plotOptions: {
                    series: {
                        fillOpacity: 0.5
                    },
                    column: {
                        grouping: false,
                        shadow: false,
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true, color: 'white',
                            verticalAlign: "top",
                            overflow: true,
                            crop: false,
                            inside: true,
                            // x: -5,
                            style: {
                                fontSize: '12px',
                                fontFamily: 'Verdana, sans-serif',
                                textShadow: '0 0 5px black'
                            }
                        }
                    }
                },
                series: [
                    {
                        name: 'Fixed Episode',
                        color: '#2090fe',
                        data: <?php echo $fixed; ?>,
                        pointPadding: 0.2,
                        pointPlacement: -0.2
                    },
                    {
                        name: 'Episode',
                        color: '#ffd700',
                        data: <?php echo $episode; ?>,
                        pointPadding: 0.25,
                        pointPlacement: -0.2
                    },
                    {
                        name: 'Mobile Episode',
                        color: '#fe8e20',
                        data: <?php echo $mobile; ?>,
                        pointPadding: 0.35,
                        pointPlacement: -0.2
                    }

                ]
            });


        });

    });
</script>

<div class="" id="Tier"></div>



