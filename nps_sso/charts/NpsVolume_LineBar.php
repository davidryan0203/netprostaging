<?php
session_start();
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/nps_sso/function/NPSFunction.php');
date_default_timezone_set('America/Denver');
//$to = date("Y-m-d", strtotime($_GET['to']));
//$from = date("Y-m-d", strtotime($_GET['from']));
$shared = $_GET['shared'];
$role = $_GET['role'];
$range = date("d/m/Y", strtotime($_GET['from'])) . " to " . date("d/m/Y", strtotime($_GET['to']));
$storeID = $_GET['storeID'];
$dateTo = $_GET['to'];
$dateFrom = $_GET['from'];
$reportView = $_GET['reportView'];


if ($storeID == "SelectStore") {
    echo "<div><center><i>Please Select Store and Report View to proceed with the report</i></center></div>";
    exit;
}

/*
  $storeID = 15;

  $sqlCheckStoreType = "SELECT f_StoreTypeID FROM t_storelist where f_StoreID = '{$storeID}' and f_Status=1 LIMIT 1";
  $resultCheckStoreType = mysql_query($sqlCheckStoreType,$connection);
  $dataCheckStoreType = mysql_fetch_assoc($resultCheckStoreType);

  //echo $role . "<br>".$shared;
  switch ($role) {
  case '1':
  $sqlShared = " ";
  break;
  case '2':
  case '3':
  if ($shared == "no") {
  $sqlShared = " AND c.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
  } else {
  $sqlShared = " ";
  }
  break;
  case '4':
  case '5':
  $query = $_SESSION['EmpProfileID'];
  //    $type = "StoreLeader";
  $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
  left join t_empnumber b on a.f_EmpID = b.f_EmpID
  left join t_storelist c on b.f_StoreID = c.f_StoreID
  where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
  $result = mysql_query($sqlGetOwnerID, $connection);
  while ($row = mysql_fetch_assoc($result)) {
  $data = $row['f_StoreOwnerUserID'];
  }
  $sqlShared = " AND c.f_StoreOwnerUserID = '{$data}' ";
  break;
  }
  $empid = $_SESSION['EmpProfileID'];

  $sqlGetTiers = "SELECT * From t_storeTier where f_StoreTypeID = '{$dataCheckStoreType['f_StoreTypeID']}' and f_Quarter = 'Q4' ";
  $tierResult = mysql_query($sqlGetTiers, $connection);
  while ($row = mysql_fetch_assoc($tierResult)) {
  $Tiers[$row['f_NpsType']][] = array("max" => $row["f_NpsMax"], "min" => $row["f_NpsMin"],
  "tier" => $row["f_TierLevel"], "bapps" => $row["f_BappsPoints"], "bappsPercent" => $row["f_BappsPercentage"]);
  }

  $select = "SELECT
  c.f_StoreListName as StoreName,
  DATE_FORMAT(CASE
  WHEN (WEEKDAY(a.f_DateTimeResponseMelbourne) <= 3) THEN DATE(a.f_DateTimeResponseMelbourne + INTERVAL (3 - WEEKDAY(a.f_DateTimeResponseMelbourne)) DAY)
  ELSE DATE(a.f_DateTimeResponseMelbourne + INTERVAL (3 + 7 - WEEKDAY(a.f_DateTimeResponseMelbourne)) DAY)
  END ,'%e-%b') as DateStamp,
  c.f_StoreID AS 'MobileStoreID',
  SUM(CASE
  WHEN (a.f_RecommendRate BETWEEN '9' AND '10') THEN 1
  ELSE 0
  END) AS StoreAdvocate,
  SUM(CASE
  WHEN (a.f_RecommendRate BETWEEN '7' AND '8') THEN 1
  ELSE 0
  END) AS StorePassive,
  SUM(CASE
  WHEN (a.f_RecommendRate BETWEEN '0' AND '6') THEN 1
  ELSE 0
  END) AS StoreDetractor,
  SUM(CASE
  WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
  ELSE 0
  END) AS StoreTotalSurvey,
  ((SUM(CASE
  WHEN (a.f_RecommendRate BETWEEN '9' AND '10') THEN 1
  ELSE 0
  END) - SUM(CASE
  WHEN (a.f_RecommendRate BETWEEN '0' AND '6') THEN 1
  ELSE 0
  END)) * 100 / SUM(CASE
  WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
  ELSE 0
  END)) AS StoreMobile
  FROM
  t_surveysd a
  LEFT JOIN
  t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
  LEFT JOIN
  t_storelist c ON a.f_StoreID = c.f_StoreID
  WHERE
  1 = 1 AND c.f_Status = 1
  AND c.f_StoreTypeID = '1'
  AND c.f_StoreOwnerUserID = 1
  AND c.f_StoreID = 50
  AND a.f_DateTimeResponseMelbourne BETWEEN '2020-03-27 00:00:00' AND '2020-05-21 23:59:59'
  AND b.f_SurveyID IN ('1' , '2', '3', '4', '13', '14', '15')
  AND b.f_ProductID IN ('1' , '2', '3', '14', '39', '40', '36')
  GROUP BY DateStamp
  order by f_DateTimeResponseMelbourne";
  $result = mysql_query($select, $connection);
  $i = 0;
  while ($row = mysql_fetch_assoc($result)) {

  $StoreNpsData[$row['StoreName']][$row['DateStamp']] = intval($row['StoreMobile']);
  $StoreVolumeData[$row['StoreName']][$row['DateStamp']] = intval($row['StoreTotalSurvey']);
  $StoreTierData[$row['StoreName']][$row['DateStamp']]['NPS'] = intval($row['StoreMobile']);
  $StoreTierData[$row['StoreName']][$row['DateStamp']]['Volume'] = intval($row['StoreTotalSurvey']);


  $mobCounter = 0;
  foreach ($Tiers["Mobile"] as $TierData) {
  //  print_r($TierData);
  if ($StoreNpsData[$row['StoreName']][$row['DateStamp']] == 'nan' || $StoreNpsData[$row['StoreName']][$row['DateStamp']] == '' || empty($StoreNpsData[$row['StoreName']][$row['DateStamp']])) {
  $StoreTierData[$row['StoreName']][$row['DateStamp']] = 0;
  $MobileData[$i]['MobilePoints'] = 0;
  } else {
  if ($mobCounter == 0) {
  if ($StoreNpsData[$row['StoreName']][$row['DateStamp']] >= $TierData['min']) {
  $StoreTierData[$row['StoreName']][$row['DateStamp']]['Tier'] = $TierData['tier'];
  }
  } elseif ($mobCounter == count($TierData)) {
  if ($StoreNpsData[$row['StoreName']][$row['DateStamp']] < $TierData['max']) {
  $StoreTierData[$row['StoreName']][$row['DateStamp']]['Tier'] = $TierData['tier'];
  }
  } else {
  if ($StoreNpsData[$row['StoreName']][$row['DateStamp']] >= $TierData['min'] AND $StoreNpsData[$row['StoreName']][$row['DateStamp']] < $TierData['max']) {
  $StoreTierData[$row['StoreName']][$row['DateStamp']]['Tier'] = $TierData['tier'];
  }
  }
  }
  $mobCounter = $mobCounter + 1;
  }

  $category[] = $row['DateStamp'];
  }

  $o = 0;
  foreach ($StoreNpsData as $key => $storeValue) {

  ${'nps' . $o}['name'] = $key;
  foreach ($category as $month) {
  //   ${'row' . $o}['data'][] = intval($value[$getNPSdata]);
  if (is_null($storeValue[$month])) {
  ${'nps' . $o}['data'][] = null;
  } else {
  ${'nps' . $o}['data'][] = $storeValue[$month];
  }
  }
  $nps[] = ${'nps' . $o};
  $o++;
  }

  $w = 0;
  foreach ($StoreVolumeData as $key => $storeVolume) {
  ${'volume' . $w}['name'] = $key;
  foreach ($category as $month) {
  //   ${'row' . $o}['data'][] = intval($value[$getNPSdata]);
  if (is_null($storeVolume[$month])) {
  ${'volume' . $w}['data'][] = null;
  } else {
  ${'volume' . $w}['data'][] = $storeVolume[$month];
  }
  }
  $volume[] = ${'volume' . $w};
  $w++;
  }

  $y = 0;

  foreach ($StoreTierData as $key => $storeVolume) {

  ${'$zone' . $y}['name'] = $key;
  ${'$zoneX' . $y}['name'] = $key;
  ${'$zoneW' . $y}['name'] = $key;
  $x= 1;
  foreach ($category as $month) {
  //   ${'row' . $o}['data'][] = intval($value[$getNPSdata]);
  if (is_null($storeVolume[$month])) {
  ${'$zone' . $y}['data'][] = null;
  } else {
  // ${'tier' . $y}['data'][] = $storeVolume[$month];

  if ($storeVolume[$month]['Tier'] == 1) {
  $color[$x] = "#348939";
  } elseif ($storeVolume[$month]['Tier'] == 2) {
  $color[$x] = "#FDBF02";
  } elseif ($storeVolume[$month]['Tier'] == 3) {
  $color[$x] = "#FDBF02";
  } elseif ($storeVolume[$month]['Tier'] == 4) {
  $color[$x] = "#FE7E03";
  } elseif ($storeVolume[$month]['Tier'] == 5) {
  $color[$x] = "#9B1D1E";
  } else {
  $color[$x] = "#9B1D1E";
  }

  ${'$zone' . $y}['data'][] = array('y' => $storeVolume[$month]['Volume'], 'color' =>  $color[$x]);
  ${'$zoneX' . $y}['data'][] = array('y' => $storeVolume[$month]['NPS'], 'color' =>  $color[$x]);
  ${'$zoneW' . $y}['data'][] = array('value' => $x+.00001, 'color' =>  $color[$x]);
  }
  $x++;
  }
  $zone[] = ${'$zone' . $y};
  $zoneX[] = ${'$zoneX' . $y};
  $zoneW[] = ${'$zoneW' . $y};
  $y++;
  }

 */

$fixed = getNpsVolumeTLS($storeID, 'Fixed', $dateFrom, $dateTo, $quarter = 'Q4');
$mobile = getNpsVolumeTLS($storeID, 'Mobile', $dateFrom, $dateTo, $quarter = 'Q4');

$fixedVolume = json_encode($fixed['volume'][0]['data']);
$fixedNps = json_encode($fixed['nps'][0]['data']);
$fixedCategory = json_encode($fixed['category']);

$mobileVolume = json_encode($mobile['volume'][0]['data']);
$mobileNps = json_encode($mobile['nps'][0]['data']);
$mobiledCategory = json_encode($mobile['category']);
?>

<script type="text/javascript">
<?php if ($reportView == 1) { ?>

        $(document).ready(function () {

            var chart4;
            chart4 = new Highcharts.Chart({
                chart: {
                    renderTo: 'MobileChart',
                    zoomType: 'xy',
                    backgroundColor: 'rgba(255, 255, 255, 0.67)'
                },
                title: {
                    text: 'Mobile Store NPS vs Volume'
                },
                xAxis: [{
                        categories: <?php echo $mobiledCategory; ?>,
                        crosshair: true
                    }],
                yAxis: [{// Primary yAxis
                        labels: {
                            format: '{value}',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        },
                        title: {
                            text: 'Survey Volume',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        }
                    }, {// Secondary yAxis
                        title: {
                            text: 'NPS Score',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        labels: {
                            format: '{value}',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        opposite: true
                    }],
                tooltip: {
                    shared: true
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 0
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true
                        },
                        lineColor: 'orange'
                    }
                },
                series: [{
                        name: 'NPS Score',
                        type: 'column',
                        yAxis: 1,
                        data: <?php echo $mobileNps; ?>,

                    }, {
                        name: 'Survey Volume',
                        type: 'spline',
                        data: <?php echo $mobileVolume; ?>,

                    }
                ]
            });



            var chart5;
            chart5 = new Highcharts.Chart({
                chart: {
                    renderTo: 'FixedChart',
                    zoomType: 'xy',
                    backgroundColor: 'rgba(255, 255, 255, 0.67)'
                },
                title: {
                    text: 'Fixed Store NPS vs Volume'
                },
                xAxis: [{
                        categories: <?php echo $fixedCategory; ?>,
                        crosshair: true
                    }],
                yAxis: [{// Primary yAxis
                        labels: {
                            format: '{value}',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        },
                        title: {
                            text: 'Survey Volume',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        }
                    }, {// Secondary yAxis
                        title: {
                            text: 'NPS Score',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        labels: {
                            format: '{value}',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        opposite: true
                    }],
                tooltip: {
                    shared: true
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 0
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true
                        },
                        lineColor: 'orange'
                    }
                },
                series: [{
                        name: 'NPS Score',
                        type: 'column',
                        yAxis: 1,
                        data: <?php echo $fixedNps; ?>,

                    }, {
                        name: 'Survey Volume',
                        type: 'spline',
                        data: <?php echo $fixedVolume; ?>,

                    }
                ]
            });
        });
<?php } else { ?>

        $(document).ready(function () {

            var chart4;
            chart4 = new Highcharts.Chart({
                chart: {
                    renderTo: 'MobileChart',
                    zoomType: 'xy',
                    backgroundColor: 'rgba(255, 255, 255, 0.67)'
                },
                title: {
                    text: 'Mobile Store Volume vs NPS'
                },
                xAxis: [{
                        categories: <?php echo $mobiledCategory; ?>,
                        crosshair: true
                    }],
                yAxis: [{// Primary yAxis
                        labels: {
                            format: '{value}',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        },
                        title: {
                            text: 'Survey Volume',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        },
                        opposite: true
                    }, {// Secondary yAxis
                        title: {
                            text: 'NPS Score',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        labels: {
                            format: '{value}',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        }
                    }],
                tooltip: {
                    shared: true
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 0
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true
                        },
                        lineColor: '#FFF380'
                    }
                },
                series: [
                    {
                        name: 'Survey Volume',
                        type: 'column',
                        data: <?php echo $mobileVolume; ?>,
                        color: '#488AC7'
                    }, {
                        name: 'NPS Score',
                        type: 'spline',
                        yAxis: 1,
                        data: <?php echo $mobileNps; ?>,

                    }
                ]
            });



            var chart5;
            chart5 = new Highcharts.Chart({
                chart: {
                    renderTo: 'FixedChart',
                    zoomType: 'xy',
                    backgroundColor: 'rgba(255, 255, 255, 0.67)'
                },
                title: {
                    text: 'Fixed Store Volume vs NPS'
                },
                xAxis: [{
                        categories: <?php echo $fixedCategory; ?>,
                        crosshair: true
                    }],
                yAxis: [{// Primary yAxis
                        labels: {
                            format: '{value}',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        },
                        title: {
                            text: 'Survey Volume',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        },
                        opposite: true
                    }, {// Secondary yAxis
                        title: {
                            text: 'NPS Score',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        labels: {
                            format: '{value}',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },

                    }],
                tooltip: {
                    shared: true
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'bottom',
                    x: 0,
                    y: 0
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true
                        },
                        lineColor: '#FFF380'
                    }
                },
                series: [{
                        name: 'Survey Volume',
                        type: 'column',
                        data: <?php echo $fixedVolume; ?>,
                        color: '#488AC7'

                    }, {
                        name: 'NPS Score',
                        type: 'spline',
                        yAxis: 1,
                        data: <?php echo $fixedNps; ?>,

                    },
                ]
            });


        });
<?php } ?>
</script>
<div>
    <center>
        <table border="0"> 
            <span style="background-color:#348939;"> <i>Tier 1</i> </span><span style="background-color:#FDBF02;"> <i>Tier 2 - 3</i> </span><span style="background-color:#FE7E03;"> <i>Tier 4</i> </span><span style="background-color:#9B1D1E;"> <i>Tier 5-6</i> </span>

        </table>
    </center>

</div>

<div class="" id="MobileChart"></div>
<div class="" id="FixedChart"></div>



