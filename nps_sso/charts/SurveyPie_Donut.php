<!--<script src="http://github.highcharts.com/master/modules/drilldown.js"></script>-->

<?php
sleep(2);
error_reporting(0);
session_start();
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/nps_sso/function/NPSFunction.php');

$shared = $_GET['shared'];
$empID = $_GET['empID'];
$profileID = $_SESSION['UserProfileID'];
$role = $_GET['role'];
$to = date("Y-m-d", strtotime($_GET['to']));
$from = date("Y-m-d", strtotime($_GET['from']));
$storeOwnerID = $_GET['storeOwnerID'];
$storeID = $_GET['storeID'];
$surveyTypeID = $_GET['surveyTypeID'];
$subsurveyTypeID = $_GET['subsurveyTypeID'];

$storeType = $_GET['storeType'];
//var_dump($_GET);

$empid = $_SESSION['EmpProfileID'];
$InteractionData = PieChartSurvey('no', NULL, $empid, $to, $from, $role, $storeType, $surveyTypeID, $subsurveyTypeID, $storeID);
//exit;
////echo "<pre>";
//var_dump($InteractionData);

if ($storeID == 'Merged') {
    $totalSurvey = 0;
    foreach ($InteractionData as $dataValue) {
        $storeList[] = $dataValue['StoreName'];
        $surveyList[] = $dataValue['SurveyName'];
        $subsurveyList[] = $dataValue['SubSurveyName'];
        $rawdata[$dataValue['SurveyName']][] = array('name' => $dataValue['SubSurveyName'], 'y' => intval($dataValue['SubSurveyCount']),
            'spare' => 'Total Sub Survey count: <b>' . intval($dataValue['SubSurveyCount']) . '</b><br>NPS Score: <b>' . number_format($dataValue['SubSurveyNPS'], npsDecimal()), 'mainNPS' => number_format($dataValue['MainSurveyNPS'], npsDecimal()), 'subNPS' => number_format($dataValue['SubSurveyNPS'], npsDecimal()));
        $totalSurvey += $dataValue['SubSurveyCount'];
    }
    $yCount = count($rawdata);
    $toJSONseries = json_encode(array('name' => 'Merged', 'y' => $yCount, 'drilldown' => 'merged', 'spare' => 'Main Survey Type Count: <b>' . $yCount));

    $survey = 0;
    foreach ($rawdata as $key => $values) {
        // $toJSONSurveyheader = array('id' => 'merged', 'name' => 'Main Survey');
        $toJSONSurvey[] = array('name' => $key, 'y' => count($values), 'drilldown' => 'mainSurvey' . $survey, 'spare' => 'Total Sub group count: <b>' . count($values) . '</b><br>NPS Score: <b>' . $values[0]['mainNPS'] . '</b>');
        $toJsonSubSurvey[] = array('id' => 'mainSurvey' . $survey, 'name' => $key, 'data' => $rawdata[$key]);
        $survey++;
    }
} elseif ($storeID != 'AllStores' && $storeID != 'Merged') {
    $totalSurvey = 0;
    foreach ($InteractionData as $dataValue) {
        $storeName = $dataValue['StoreName'];
        $storeList[] = $dataValue['StoreName'];
        $surveyList[] = $dataValue['SurveyName'];
        $subsurveyList[] = $dataValue['SubSurveyName'];
        $rawdata[$dataValue['SurveyName']][] = array('name' => $dataValue['SubSurveyName'], 'y' => intval($dataValue['SubSurveyCount']),
            'spare' => 'Total Sub Survey count: <b>' . intval($dataValue['SubSurveyCount']) . '</b><br>NPS Score: <b>' . number_format($dataValue['SubSurveyEpisodeNPS'], npsDecimal()), 'mainNPS' => number_format($dataValue['MainSurveyEpisodeNPS'], npsDecimal()), 'subNPS' => number_format($dataValue['SubSurveyNPS'], npsDecimal()));
        $totalSurvey += $dataValue['SubSurveyCount'];
    }
    $yCount = count($rawdata);
    $toJSONseries = json_encode(array('name' => "" . $storeName . "", 'y' => $yCount, 'drilldown' => 'store1', 'spare' => 'Main Survey Type Count: <b>' . $yCount));

    $survey = 0;
    foreach ($rawdata as $key => $values) {
        // $toJSONSurveyheader = array('id' => "".$storeName."", 'name' => 'Main Survey');
        $toJSONSurvey[] = array('name' => $key, 'y' => count($values), 'drilldown' => 'mainSurvey' . $survey, 'spare' => 'Total Sub group count: <b>' . count($values) . '</b><br>NPS Score: <b>' . number_format($values[0]['mainNPS'], npsDecimal()) . '</b>');
        $toJsonSubSurvey[] = array('id' => 'mainSurvey' . $survey, 'name' => $key, 'data' => $rawdata[$key]);
        $survey++;
    }
} else {
    $store = 0;

    foreach ($InteractionData as $dataValue) {
        $rawData[$dataValue['StoreName']][$dataValue['SurveyName']][] = array('name' => $dataValue['SubSurveyName'], 'y' => intval($dataValue['SubSurveyCount']),
            'spare' => 'Total Sub Survey count: <b>' . intval($dataValue['SubSurveyCount']) . '</b><br>NPS Score: <b>' . number_format($dataValue['SubSurveyNPS'], npsDecimal()), 'mainNPS' => number_format($dataValue['MainSurveyNPS'], npsDecimal()), 'subNPS' => number_format($dataValue['SubSurveyNPS'], npsDecimal()));
        $unique = array_unique($surveyList);
    }


    foreach ($rawData as $key => $storeValues) {
        $surveyCount = count($storeValues);
        $toJSONseries[] = array('name' => "" . $key . "", 'y' => $surveyCount, 'drilldown' => 'store' . $store, 'spare' => 'Main Survey Type Count: <b>' . $surveyCount);

        $surveyCount = 0;
        foreach ($storeValues as $survey => $surveyValues) {

            $toJSONSurvey[] = array('name' => $survey, 'y' => count($surveyValues), 'drilldown' => 'store' . $store . 'mainSurvey' . $surveyCount,
                'spare' => 'Total Sub group count: <b>' . count($surveyValues) . '</b><br>NPS Score: <b>' . number_format($surveyValues[0]['mainNPS'], npsDecimal()) . '</b>');
            $toJsonSubSurvey[] = array('id' => 'store' . $store . 'mainSurvey' . $surveyCount, 'name' => $survey, 'data' => $surveyValues);
            $surveyCount++;
        }
        $toJSONSubSeries[] = array('id' => 'store' . $store, 'name' => 'Main Survey', 'data' => $toJSONSurvey);
        $toJSONSurvey = array();

        $store++;
    }
}
?>
<script type="text/javascript">

    $(function() {

        // Create the chart
        $('#piebar').highcharts({
            chart: {
                type: 'pie',
                backgroundColor: 'rgba(255, 255, 255, 0.67)'
            },
            title: {
                text: 'Basic drilldown'
            },
            subtitle: {
                text: 'Click on chart to view more info'
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.spare}</span><br/>',
                drilldownFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> of total<br/>'

            },
            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}: {point.percentage:.1f}%'
                    }, showInLegend: true
                }
            },
            series: [{
                    name: 'Store List',
                    colorByPoint: true,
                    data: [
<?php
if ($storeID == 'AllStores') {
    $tempSeries = json_encode(array_values($toJSONseries));
    $tempSeries = substr($tempSeries, 1, -1);

    echo $tempSeries;
} else {
    echo $toJSONseries;
}
?>
                    ]
                }],
            drilldown: {
<?php
if ($storeID == 'Merged') {
    $tempSubSurvey = json_encode($toJsonSubSurvey);
    $tempSubSurvey = substr($tempSubSurvey, 1, -1);

    echo "series: [{ id: 'merged', name: 'Main Survey',data:";
    echo json_encode($toJSONSurvey);
    echo "}," . $tempSubSurvey . "]";
} elseif ($storeID != 'AllStores' && $storeID != 'Merged') {
    $tempSubSurvey = json_encode($toJsonSubSurvey);
    $tempSubSurvey = substr($tempSubSurvey, 1, -1);

    echo "series: [{ id: 'store1', name: 'Main Survey',data:";
    echo json_encode($toJSONSurvey);
    echo "}," . $tempSubSurvey . "]";
} else {
    $tempSurvey = json_encode($toJSONSubSeries);
    $tempSurvey = substr($tempSurvey, 1, -1);
    $tempSubSurvey = json_encode($toJsonSubSurvey);
    $tempSubSurvey = substr($tempSubSurvey, 1, -1);
    echo "series: [";
    echo $tempSurvey . ",$tempSubSurvey]";
}
?>


            }
        });
    });

</script>


<div id="piebar"></div>

<!--<script>


    $(function () {

        // Create the chart
        $('#piebar').highcharts({
            chart: {
                type: 'pie'
            },
            title: {
                text: 'Basic drilldown'
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> of total<br/>',
                drilldownFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> of total<br/>'

            },
            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}: {point.percentage:.1f}%'
                    }, showInLegend: true
                }
            },
            series: [{
                    name: 'Store List',
                    colorByPoint: true,
                    data: [
                        {"name": "Carindale Lvl1", "y": 4, "drilldown": "store0"}, {"name": "Carindale GR", "y": 4, "drilldown": "store1"}]
                }],
            drilldown: {
                series: [{"id": "store0", "name": "Main Survey",
                        "data": [{"name": "Modify", "y": 3, "id": "store0mainSurvey0"},
                            {"name": "Activation", "y": 1, "id": "store0mainSurvey1"},
                            {"name": "Billing", "y": 2, "id": "store0mainSurvey2"},
                            {"name": "Assurance", "y": 1, "id": "store0mainSurvey3"}]},
                          {"id": "store1", "name": "Main Survey",
                                "data": [{"name": "Modify", "y": 3, "id": "store1mainSurvey0"},
                                        {"name": "Activation", "y": 1, "id": "store1mainSurvey1"},
                                        {"name": "Billing", "y": 3, "id": "store1mainSurvey2"},
                                        {"name": "Assurance", "y": 3, "id": "store1mainSurvey3"}]},

                        {"id": "store0mainSurvey0", "name": "Modify",
                            "data": [{"name": "Minor Modify", "y": 1},
                                     {"name": "Recontract", "y": 15},
                                     {"name": "Plan Change", "y": 1}]},
                    {"id": "store0mainSurvey1", "name": "Activation", "data": [{"name": "", "y": 27}]},
                    {"id": "store0mainSurvey2", "name": "Billing",
                                "data": [{"name": "First Bill - Recontract", "y": 4},
                                        {"name": "First bill", "y": 2}]},
                    {"id": "store0mainSurvey3", "name": "Assurance",
                                "data": [{"name": "Telstra Platinum", "y": 1}]}


                    , {"id": "store1mainSurvey0", "name": "Modify",
                            "data": [{"name": "Recontract", "y": 27},
                                    {"name": "Plan Change", "y": 2},
                                    {"name": "Minor Modify", "y": 2}]},
                     {"id": "store1mainSurvey1", "name": "Activation", "data": [{"name": "", "y": 59}]},
                     {"id": "store1mainSurvey2", "name": "Billing",
                            "data": [{"name": "First bill", "y": 12},
                             {"name": "First Bill - Plan Change", "y": 2},
                             {"name": "First Bill - Recontract", "y": 13}]},
                     {"id": "store1mainSurvey3", "name": "Assurance",
                            "data": [{"name": "Handset Repair", "y": 2},
                                {"name": "Telstra Platinum", "y": 2},
                                {"name": "Tech Bar", "y": 10}]
                        }
                    ]

            }
        });
    });




</script>-->