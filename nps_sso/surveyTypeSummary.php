<?php
session_start();
$role = $_SESSION['UserRoleID'];
$from = $_GET['from'];
$to = $_GET['to'];
$storeID = $_GET['storeID'];
$surveyTypeID = $_GET['surveyTypeID'];
$subsurveyTypeID = $_GET['subsurveyTypeID'];
?>

<!--Survey Type Reports-->
<section class='survey-type'>

    <div class='filters'>
        <input type="hidden" id="from" name="from" value="<?php echo $from; ?>"/>
        <input type="hidden" id="to" name="to"  value="<?php echo $to; ?>"/>
        <input type="hidden" id="role" name="role" value="<?php echo $role; ?>"/>
        <input type="hidden" id="surveyTypeID" name="surveyTypeID" value="<?php echo $surveyTypeID; ?>"/>
        <input type="hidden" id="subsurveyTypeID" name="subsurveyTypeID" value="<?php echo $subsurveyTypeID; ?>"/>
    </div>

    <div class="main-content">
        <h4 class="sub-header"><i class="fa fa-pie-chart fa-fw"></i>Survey Summary</h4>
        <div class="loader" id="PieSurveyContainerloader"></div>
        <div id="PieSurveyContainer"></div>
    </div>


    <div class="main-content">
        <h4 class="sub-header"><i class="fa fa-line-chart fa-fw"></i>Survey Line Summary</h4>
        <div class="loader" id="LineSurveyContainerloader"></div>
        <div id="LineSurveyContainer"></div>
    </div>

    <div class="main-content">
        <h4 class="sub-header"><i class="fa fa-bar-chart-o fa-fw"></i> Survey Episode Break Down</h4>
        <div class="loader" id="StoreEpisodeSurveyLoader"></div>
        <div class="table table-responsive" id="StoreEpisodeSurvey"></div>
    </div>

    <div class="main-content">
        <h4 class="sub-header"><i class="fa fa-bar-chart-o fa-fw"></i> Survey Interaction Break Down</h4>
        <div class="loader" id="StoreInteractionSurveyLoader"></div>
        <div class="table table-responsive" id="StoreInteractionSurvey"></div>
    </div>

</section>

<script src="../includes/js/custom.js" type="text/javascript"></script>

<script type="text/javascript">

    $(document).ready(function() {

        $('#PieSurveyContainer', function() {
            var addonParams = setLoad();
            var urlToLoad = "nps/charts/SurveyPie_Donut.php?" + addonParams;
            var divToHide = "#PieSurveyContainerloader";
            var divToShow = "#PieSurveyContainer";
            reload(urlToLoad, divToHide, divToShow);
        });

        $('#LineSurveyContainer', function() {
            var addonParams = setLoad();
            var urlToLoad = "nps/charts/LineChart.php?" + addonParams;
            var divToHide = "#LineSurveyContainerloader";
            var divToShow = "#LineSurveyContainer";
            reload(urlToLoad, divToHide, divToShow);
        });

        $('#StoreInteractionSurvey', function() {
            var addonParams = setLoad();
            // console.log(addonParams);
            var urlToLoad = "nps_sso/summary/InteractionSurvey.php?" + addonParams;
            var divToHide = "#StoreInteractionSurveyLoader";
            var divToShow = "#StoreInteractionSurvey";
            reload(urlToLoad, divToHide, divToShow);
        });
        $('#StoreEpisodeSurvey', function() {
            var addonParams = setLoad();
            // console.log(addonParams);
            var urlToLoad = "nps_sso/summary/EpisodeSurvey.php?" + addonParams;
            var divToHide = "#StoreEpisodeSurveyLoader";
            var divToShow = "#StoreEpisodeSurvey";
            reload(urlToLoad, divToHide, divToShow);
        });
    });

    function reload(urlToLoad, divToHide, divToShow) {
        var urldata = urlToLoad;
        var page = divToShow;
        $.get(urldata).success(
                function(response, status, jqXhr) {
                    $(divToHide).hide();
                    $(page).fadeIn('slow');
                    $(page).empty().append(response);
                }).error(function(response, status, jqXhr) {
            $(divToHide).show();
            reload(urldata, divToHide, divToShow);
        }).complete(function(response, status, jqXhr) {
        });
    }

    function setLoad() {
        var from = $("#from").val().split("/").reverse().join("-");
        var to = $("#to").val().split("/").reverse().join("-");
        //var storeOwnerID = $("#storeOwnerID").val();
        var surveyTypeID = $("#surveyTypeID").val();
        var subSurveyTypeID = $("#subsurveylistid").val();
        var storeID = $("#storeID").val();
        var toLoad = 'to=' + to + '&from=' + from + '&storeID=' + storeID + '&surveyTypeID=' + surveyTypeID + '&subsurveyTypeID=' + subSurveyTypeID;
        //console.log(toLoad);
        return toLoad;
    }
</script>


