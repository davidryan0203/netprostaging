<?php
$role = $_SESSION['UserRoleID'];
$UploadAccess = array("1", "6");
?>

<div class="container" id="NpsPage">
    <?php
    $sublink = $_GET['o'];
    switch ($sublink) {
        case 1:
            if (in_array($role, $AdminPages)) {
                include('nps/index.php');
            } else {
                include("404.php");
            }
            break;
        case 2:
            if (in_array($role, $AdminPages)) {
                include('nps/report.php');
            } else {
                include("404.php");
            }
            break;
        case 3:
            if (in_array($role, $UploadAccess) || $_SESSION['LoginID'] == '458') {
                include('nps/dataupload.php');
            } else {
                include("404.php");
            }
            break;
        case 6:
            if (in_array($role, $AdminPages) || $_SESSION['LoginID'] == '458') {
                include('nps/surveyTypeReport.php');
            } else {
                include("404.php");
            }
            break;
        case 7:
            if (in_array($role, $UploadAccess) || $_SESSION['LoginID'] == '5515' || $_SESSION['HasStoreType'] == 'Only Others') {
                include('nps/datauploadV2.php');
            } else {
                include("404.php");
            }
            break;
        case 8:
          if (in_array($role, $AdminPages)) {
                include('nps/npsvolume.php');
            } else {
                include("404.php");
            }
            break;
        default:
            include('nps/index.php');
            break;
    }
    ?>
</div>






