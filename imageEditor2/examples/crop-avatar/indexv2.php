<?php
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
session_start();
$UserLoginID = $_SESSION['LoginID'];
$sql_GetPhoto = "Select f_ProfileImgUrl From t_userlogin  Where f_UserLoginID = '{$UserLoginID}'";
$result = mysql_query($sql_GetPhoto, $connection);
$row = mysql_fetch_assoc($result);
//var_dump($connection);
if (is_null($row['f_ProfileImgUrl'])) {
    $profileImg = "images/user_icon.png";
} else {
    $profileImg = "profilepic/" . $row['f_ProfileImgUrl'];
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="A complete example of Cropper.">
        <meta name="keywords" content="HTML, CSS, JS, JavaScript, jQuery, PHP, image cropping, web development">
        <meta name="author" content="Fengyuan Chen">
        <title>Crop Avatar - Cropper</title>
        <!--<link href="imageEditor2/assets/css/bootstrap.min.css" rel="stylesheet">-->
        <link href="imageEditor2/dist/cropper.min.css" rel="stylesheet">
        <link href="imageEditor2/examples/crop-avatar/css/main.css" rel="stylesheet">
    </head>
    <body>
        <script>
            function goBack() {
                event.preventDefault();
                history.back(1);
            }
        </script>
        <div class="container" id="crop-avatar">

            <!-- Current avatar -->
            <div class="avatar-view" title="Change the avatar"><button onclick="goBack()">Go Back</button>
                <img src="<?php echo $profileImg; ?>" alt="Avatar">
            </div>

            <!-- Cropping modal -->
            <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <form class="avatar-form" action="crop.php" enctype="multipart/form-data" method="post">
                            <div class="modal-header">
                                <button class="close" data-dismiss="modal" type="button">&times;</button>
                                <h4 class="modal-title" id="avatar-modal-label">Change Avatar</h4>
                            </div>
                            <div class="modal-body">
                                <div class="avatar-body">

                                    <!-- Upload image and data -->
                                    <div class="avatar-upload">
                                        <input class="avatar-src" name="avatar_src" type="hidden">
                                        <input class="avatar-data" name="avatar_data" type="hidden">
                                        <label for="avatarInput">Upload Avatar</label>
                                        <input class="avatar-input" id="avatarInput" name="avatar_file" type="file">
                                    </div>

                                    <!-- Crop and preview -->
                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="avatar-wrapper"></div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="avatar-preview preview-lg"></div>
                                            <div class="avatar-preview preview-md"></div>
                                            <div class="avatar-preview preview-sm"></div>
                                        </div>
                                    </div>

                                    <div class="row avatar-btns">
                                        <div class="col-md-9">
                                            <div class="btn-group">
                                                <button class="btn btn-primary" data-method="rotate" data-option="-45" type="button" title="Rotate Left">
                                                    <span class="fa fa-reply"></span> Rotate Left</button>
                                                </button>
                                                <button class="btn btn-primary" data-method="rotate" data-option="45" type="button" title="Rotate Right">
                                                    <span class="fa fa-share"></span> Rotate Right</button>
                                                </button>
                                                <button class="btn btn-success" type="submit" title="Save"><span class="fa fa-floppy-o"></span> Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="modal-footer">
                                  <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                                </div> -->
                        </form>
                    </div>
                </div>
            </div><!-- /.modal -->

            <!-- Loading state -->
            <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
        </div>


        <script src="imageEditor2/dist/cropper.min.js"></script>
        <script src="imageEditor2/examples/crop-avatar/js/main.js"></script>
    </body>
</html>
