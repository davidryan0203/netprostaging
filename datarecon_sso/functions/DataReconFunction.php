<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
session_start();

include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/crypter.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin_sso/function/EmployeeFunction.php');

//include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
//include($_SERVER['DOCUMENT_ROOT'] . '/admin_sso/function/EmployeeFunction.php');

function UpdateBlankPnumber($query) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
    global $connection;

    $SDID = mysql_real_escape_string($query['SDID']);
    $invitationID = mysql_real_escape_string($query['invitationID']);
    $PNumber = mysql_real_escape_string($query['Pnumber']);
    
    $sqlOrginalPnumber = "SELECT f_ActivationExtra5 FROM t_surveysd WHERE f_SurveySDID = '$SDID'";

    $resultOrginalPnumber = mysql_query($sqlOrginalPnumber, $connection);
    $rowExtra5 = mysql_fetch_assoc($resultOrginalPnumber);   


    $sqlUpdateBlankPnumber = "UPDATE t_surveysd SET f_ActivationExtra5 = '$PNumber' WHERE f_SurveySDID = '$SDID'";
    $result = mysql_query($sqlUpdateBlankPnumber, $connection);  //run sql query

    if ($result && mysql_affected_rows() == 1) { //check if success update
        $resultUpdateBlankPnumber['message'] = 'Success Updating PNumber';
        $logMessageArray = json_encode(array('SurveySDID' => $SDID, 'InvitationID' => $invitationID, 'Pnumber' => $PNumber , 'OriginalPNumber' => $rowExtra5['f_ActivationExtra5']));
        InsertLog('3', '2', $logMessageArray, $_SESSION['LoginID'], 'Success', '6');
    } else {
        $resultUpdateBlankPnumber['message'] = 'Failed Updating PNumber';
        $logMessageArray = json_encode(array('Reason' => 'An error occured', 'SurveySDID' => $SDID, 'InvitationID' => $invitationID, 'Pnumber' => $PNumber, 'OriginalPNumber' => $rowExtra5['f_ActivationExtra5']));
        InsertLog('3', '2', $logMessageArray, $_SESSION['LoginID'], 'Failed', '6');
    }

    return $resultUpdateBlankPnumber; //return message and data if any
}

function getCustomerFeedback($query) { //Needed customer ID
    global $connection;

    // $storeID = $query['storeID'];

    $sqlgetCustomerFeedBack = " select a.*,d.f_StoreListName from t_customerdetails a
 left join t_surveypd b on a.f_CustomerDetailsID = b.f_CustomerDetailsID
 left join t_surveysd c on b.f_SurveyPDID = c.f_SurveyPDID
 left join t_storelist d on c.f_StoreID = d.f_StoreID";

    return $resultgetCustomerFeedback;
}

function getCustomerList($query) { //Need Store ID or User Type
    global $connection;

    // echo $connection;

    $from = $query['from'];
    $to = $query['to'];
    $storeOwnerID = $query['storeOwnerID'];

    if (is_null($storeOwnerID) || $storeOwnerID == '') {
        $sqlStoreOwner = "";
    } else {
        $sqlStoreOwner = "d.f_StoreOwnerUserID = '{$storeOwnerID}' AND ";
    }

    $resultgetCustomerList = array();
    $sqlgetCustomerList = "select a.*,d.f_StoreListName from t_customerdetails a
 left join t_surveypd b on a.f_CustomerDetailsID = b.f_CustomerDetailsID
 left join t_surveysd c on b.f_SurveyPDID = c.f_SurveyPDID
 left join t_storelist d on c.f_StoreID = d.f_StoreID
 WHERE 1=1 AND $sqlStoreOwner  c.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' GROUP BY a.f_CustomerDetailsID";

    //$sqlgetCustomerList = "SELECT NOW()";
    //Aecho $sqlgetCustomerList;
    $result = mysql_query($sqlgetCustomerList, $connection);
    while ($row = mysql_fetch_assoc($result)) {
        $resultgetCustomerList[] = $row;
    }
    return $resultgetCustomerList;
}

function getEmpList($storeID) {
    global $connection;
    session_start();

    if (!is_null($storeID) && !empty($storeID) && $storeID != "") {
        $userData = SelectEmployee($storeID, 'specificstore', NULL);
    } else {
        $role = $_SESSION['UserRoleID'];
        if ($role == '1') {
            $userData = SelectEmployee('ALL', NULL, NULL);
        } else {
            $query = $_SESSION['EmpProfileID'];
            if ($role == '5') {
                $query = $_SESSION['EmpProfileID'];
                $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
                                left join t_empnumber b on a.f_EmpID = b.f_EmpID
                                left join t_storelist c on b.f_StoreID = c.f_StoreID
                                where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
                $result = mysql_query($sqlGetOwnerID, $connection);
                while ($row = mysql_fetch_assoc($result)) {
                    $data = $row['f_StoreOwnerUserID'];
                }
            } else {
                $data = $_SESSION['UserProfileID'];
            }
            $keySearch = $data;
            $userData = SelectEmployee($keySearch, 'store', NULL);
        }
    }

    $i = 0;
    if (!is_null($userData)) {
        foreach ($userData as $value) {

            $empList[$i]['value'] = $value['f_EmpFirstName'] . " " . $value['f_EmpLastName'] . " -" . $value['f_EmpPNumber'];
            $empList[$i]['data'] = $value['f_EmpPNumber'];

            $i++;
        }
    } else {
        $empList[0]['value'] = "No Employee available";
        $empList[0]['data'] = NULL;
    }

//echo $query;

    return $empList;
}

function getDataReconUser() {
    global $connection;

    $sqlgetDataReconUser = "SELECT  a.f_UserName, a.f_UserID as ReconUserID , d.f_UserFirstName as ReconFirstName , d.f_UserLastName as ReconLastName ,
                            c.f_UserFirstName as OwnerFirstName , c.f_UserLastName as OwnerLastName , c.f_UserID as OwnerID , b.f_AssignToReconID as ReconID
                            FROM
                                t_userlogin a
                                    LEFT JOIN
                                t_assigntorecon b ON a.f_UserLoginID = b.f_UserLoginID
                                            LEFT JOIN
                                    t_userlist c ON b.f_OwnerUserID = c.f_UserID
                                            LEFT JOIN
                                    t_userlist d ON a.f_UserID = d.f_UserID
                            WHERE
                                a.f_UserRoleID = 6 AND a.f_Status = 1 ";
    $result = mysql_query($sqlgetDataReconUser, $connection);
    while ($row = mysql_fetch_assoc($result)) {
        $data[$row['f_UserName']][] = $row;
        $data[$row['f_UserName']]['OwnerDetails'][] = array('OwnerName' => $row['OwnerFirstName'] . ' ' . $row['OwnerLastName'],
            'OwnerID' => $row['OwnerID'], 'ReconID' => $row['ReconID']);
    }
//    echo $sqlgetDataReconUser
//    $return[] = $data;

    return $data;
}

function getStoreOwner() {
    global $connection;
    $role = $_SESSION['UserRoleID'];

    $sqlGetStoreOwner = "SELECT";

//    switch ($role) {
//        case '1':
//            $ownerID = "ALL";
//            break;
//        case '2':
//        case '3':
//            $ownerID = $_SESSION['UserProfileID'];
//            break;
//        case '4':
//        case '5':
//            $query = $_SESSION['EmpProfileID'];
//            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
//                                left join t_empnumber b on a.f_EmpID = b.f_EmpID
//                                left join t_storelist c on b.f_StoreID = c.f_StoreID
//                                where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
//            $result = mysql_query($sqlGetOwnerID, $connection);
//            while ($row = mysql_fetch_assoc($result)) {
//                $ownerID = $row['f_StoreOwnerUserID'];
//            }
//            break;
//        case '6':
//            $query = $_SESSION['LoginID'];
//            if ($query != '199') {
//                $sqlGetOwnerID = "SELECT f_OwnerUserID FROM t_assigntorecon WHERE f_UserLoginID = '{$query}'";
//                $result = mysql_query($sqlGetOwnerID, $connection);
//                while ($row = mysql_fetch_assoc($result)) {
//                    $ownerID[] = $row['f_OwnerUserID'];
//                }
//            } else {
//                $ownerID = "ALL";
//            }
//
//            break;
//        default:
//            break;
//    }
     switch ($role) {
        case '1':
            $ownerID = "ALL";
            break;
        case '2':
        case '3':
        case '4':
        case '5':
            $ownerID = $_SESSION['ClientOwnerID'];
            break;
        case '6':
            $query = $_SESSION['LoginID'];
            if ($query != '199') {
                $sqlGetOwnerID = "SELECT f_OwnerUserID FROM t_assigntorecon WHERE f_UserLoginID = '{$query}'";
                $result = mysql_query($sqlGetOwnerID, $connection);
                while ($row = mysql_fetch_assoc($result)) {
                    $ownerID[] = $row['f_OwnerUserID'];
                }
            } else {
                $ownerID = "ALL";
            }

            break;
        default:
            break;
    }

    return $ownerID;
}

function CreateDataRecon($query) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
    global $connection;

    $username = mysql_real_escape_string($query['UserName']);
    $cleanpassword = mysql_real_escape_string($query['UserPassword']);

    $password = password_hash($cleanpassword, PASSWORD_BCRYPT);
    $roleID = '6';
    $firstname = mysql_real_escape_string($query['FirstName']);
    $lastname = mysql_real_escape_string($query['LastName']);

    $sql = "INSERT INTO `t_userlist`
            (`f_UserFirstName`,`f_UserLastName`)
            VALUES
            ('$firstname','$lastname')";

    $result = mysql_query($sql, $connection);

    if ($result && mysql_affected_rows() == 1) {
        $returnCreateUser['message'] = "Success Creating Data Recon";
        $insertedID = mysql_insert_id();

        $logMessageArray = json_encode(array('UserID' => $insertedID,
            'Details' => array($firstname, $lastname)));
        InsertLog('2', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '3');
        $sqlCreateUserLogin = "INSERT INTO t_userlogin (`f_UserName`,`f_Password`,`f_UserType`,`f_EmpID`,`f_UserID`,`f_UserRoleID`) VALUES ('{$username}','{$password}','1','0','{$insertedID}','{$roleID}')";
        $resultUserLogin = mysql_query($sqlCreateUserLogin, $connection);
        if ($resultUserLogin && mysql_affected_rows() == 1) {
            $loginid = mysql_insert_id();
            $returnCreateUser['UserLoginID'] = $loginid;
            $logMessageArray = json_encode(array('LoginID' => $loginid, 'Details' => array($username, $password, $roleID, $insertedID)));
            InsertLog('2', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '1');
        } else {
            $logMessageArray = json_encode(array('Reason' => 'An error occured', 'Details' => array($username, $password, $roleID, $insertedID), 'Query' => $sqlCreateUserLogin));
            InsertLog('2', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '1');
        }
    } else {
        $returnCreateUser['message'] = "Failed Creating Data Recon";
        $logMessageArray = json_encode(array('Reason' => 'An error occured', 'Details' => $query));
        InsertLog('2', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '3');
    }

    //mysql_close($connection);
    return $returnCreateUser;
}

function CreateAssignTo($query) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
    global $connection;

    $userLoginID = $query['UserLoginID'];
    $ownerUserID = $query['storeOwnerID'];

    $i = 0;
    foreach ($ownerUserID as $tocreate) {
        $sqlto[$i] = "'{$userLoginID}','{$tocreate}'";
        $i++;
    }
    $sqlCreate = "INSERT INTO t_assigntorecon (`f_UserLoginID`,`f_OwnerUserID`) VALUES ";
    foreach ($sqlto as $data) {
        $sqlCreate .= "(" . $data . "),";
    }
    $sqlCreate = trim($sqlCreate, ",");
    // echo $sqlCreate;
    $result = mysql_query($sqlCreate, $connection);

    if ($result && mysql_affected_rows() >= 1) {
        $returnCreateAssignTo['message'] = "Success Assigning to owner";
        $logMessageArray = json_encode(array('Details' => array($userLoginID, $ownerUserID)));
        InsertLog('2', '2', $logMessageArray, $_SESSION['LoginID'], 'Success', '8');
    } else {
        $returnCreateAssignTo['message'] = "Failed Assigning to owner";
        $logMessageArray = json_encode(array('Reason' => 'An error occured', 'Details' => $query));
        InsertLog('2', '2', $logMessageArray, $_SESSION['LoginID'], 'Failed', '8');
    }

    //mysql_close($connection);
    return $returnCreateAssignTo;
}

function getModifiedSurvey($from, $to, $storeOwnerID) {
    global $connection;

    /**    $sqlGetModifiedSurvey = "SELECT
      max(a.f_LogID), a.f_LogRemark, a.f_UserLoginID, e.f_UserName , a.f_DateTimeCreated
      FROM
      t_log a
      LEFT JOIN
      t_logfrom b USING (f_LogFromID)
      LEFT JOIN
      t_logfromsub c USING (f_LogFromSubID)
      LEFT JOIN
      t_logtype d USING (f_LogTypeID)
      LEFT JOIN
      t_userlogin e USING (f_UserLoginID)
      WHERE
      b.f_LogFromID = 2
      AND c.f_LogFromSubID = 6
      AND d.f_LogTypeID = 3
      AND a.f_LogStatus = 'Success'
      AND (a.f_DateTimeCreated Between '{$from}' and '{$to}')
      GROUP BY a.f_LogRemark
      ORDER BY a.f_DateTimeCreated DESC";
     *
     */
    $toSearch = '"SurveySDID';
    $sqlGetModifiedSurvey = "SELECT
    a.f_LogID,
    a.f_LogRemark,
    a.f_UserLoginID,
    e.f_UserName,
    a.f_DateTimeCreated
FROM
    t_log a
        INNER JOIN
    (SELECT DISTINCT
        (SUBSTRING(a.f_LogRemark, LOCATE('\"SurveySDID\":', a.f_LogRemark) + 14, LOCATE('\",\"', a.f_LogRemark, LOCATE('\"SurveySDID\":', a.f_LogRemark)) - LOCATE('\"SurveySDID\":', a.f_LogRemark) - 14)) AS SDID,
            MAX(a.f_LogID) AS f_LogID
    FROM
        t_log a
    WHERE
        f_LogFromID = 2 AND f_LogFromSubID = 6
            AND f_LogTypeID = 3
            AND f_LogStatus = 'Success'
            AND (a.f_DateTimeCreated Between '{$from}' and '{$to}')
    GROUP BY SDID) table2 ON a.f_LogID = table2.f_LogID
        LEFT JOIN
    t_logfrom b on a.f_LogFromID = b.f_LogFromID
        LEFT JOIN
    t_logfromsub c on a.f_LogFromSubID = c.f_LogFromSubID
        LEFT JOIN
    t_logtype d on a.f_LogTypeID = d.f_LogTypeID
        LEFT JOIN
    t_userlogin e on a.f_UserLoginID = e.f_UserLoginID";
    mysql_query("SELECT 1=1;", $connection);
    $result = mysql_query($sqlGetModifiedSurvey, $connection);
    while ($row = mysql_fetch_assoc($result)) {
        $data[] = array('JSON' => json_decode($row['f_LogRemark'], true), 'User' => $row['f_UserName'], 'ModifiedDate' => $row['f_DateTimeCreated']);
    }
    // echo $sqlGetModifiedSurvey;
    //  exit;
    foreach ($data as $value) {
        $select = "select f_DealerCode , f_ActivationExtra5  , concat(c.f_EmpFirstName , ' ' , c.f_EmpLastName) as EmpName , a.f_SurveySDID , a.f_StoreID
                        from t_surveysd a
                        left join t_empnumber b on a.f_ActivationExtra5 = b.f_EmpPNumber
                        left join t_emplist c on b.f_EmpID = c.f_EmpID
                        left join t_storelist d on a.f_StoreID = d.f_StoreID
                        where a.f_SurveySDID = '{$value['JSON']['SurveySDID']}'
                        AND d.f_StoreOwnerUserID = '{$storeOwnerID}'";

        // echo $select;
        mysql_query("SELECT 1=1;", $connection);
        $result = mysql_query($select, $connection);
        while ($row = mysql_fetch_assoc($result)) {
            $return[] = array('Details' => $row, 'ModifiedBy' => $value['User'], 'ModifiedDate' => $value['ModifiedDate'], 'InivitationID' => $value['JSON']['InvitationID']);
        }

        // $survey[] = $value[0]['SurveySDID'];
    }


    // $return = array($data,$survey);
    return $return;
}

function getSurveyList($from, $to, $storeOwnerID) {
    global $connection;



    /* $select = "select a.f_DealerCode , a.f_ActivationExtra5  , concat(c.f_EmpFirstName , ' ' , c.f_EmpLastName) as EmpName ,
      a.f_SurveySDID , a.f_StoreID , a.f_DateTimeCreatedMelbourne , e.f_InvitationID as InivitationID,
      CONCAT(f.f_CustomerFirstName,' ',f.f_CustomerLastname) AS CustomerName ,a.f_DateTimeResponseMelbourne
      from t_surveysd a
      left join t_empnumber b on a.f_ActivationExtra5 = b.f_EmpPNumber
      left join t_emplist c on b.f_EmpID = c.f_EmpID
      left join t_storelist d on a.f_StoreID = d.f_StoreID
      left join t_surveypd e on a.f_SurveyPDID = e.f_SurveyPDID
      left join t_customerdetails f ON e.f_CustomerDetailsID = f.f_CustomerDetailsID
      where a.f_DateTimeResponseMelbourne between '{$from} 00:00:00' and '{$to} 23:59:59'
      AND d.f_StoreOwnerUserID = '{$storeOwnerID}'";

      // echo $select;
      $result = mysql_query($select, $connection);
      while ($row = mysql_fetch_assoc($result)) {

      $sqlLastModified = "select b.f_UserName,a.f_DateTimeCreated from t_log a
      left join t_userlogin b on a.f_UserLoginID = b.f_UserLoginID
      where a.f_LogRemark like '%{$row['InivitationID']}%' order by a.f_DateTimeCreated DESC LIMIT 1";
      $resultLastModified = mysql_query($sqlLastModified, $connection);
      $rowLastModified = mysql_fetch_assoc($resultLastModified);
      $return[] = array('Details' => $row, 'Modified' => array('By' => $rowLastModified['f_UserName'], 'Last' => $rowLastModified['f_DateTimeCreated']));
      } */
    mysql_query("SELECT  1=1", $connection);
    $select = "SELECT
    a.f_DealerCode,
    a.f_ActivationExtra5,
    CONCAT(c.f_EmpFirstName, ' ', c.f_EmpLastName) AS EmpName,
    a.f_SurveySDID,
    a.f_StoreID,
    a.f_DateTimeCreatedMelbourne,
    e.f_InvitationID AS InivitationID,
    CONCAT(f.f_CustomerFirstName,
            ' ',
            e.f_CustomerBillingAccountID) AS CustomerName,
    a.f_DateTimeResponseMelbourne,
    a.f_SurveyInsertedDate,
    g.f_UserName, g.f_DateTimeCreated , g.f_LogRemark , e.f_EpisodeReferenceNo as EpisodeNum,d.f_StoreOwnerUserID
FROM
    t_surveysd a
        LEFT JOIN
    t_empnumber b ON a.f_ActivationExtra5 = b.f_EmpPNumber
        LEFT JOIN
    t_emplist c ON b.f_EmpID = c.f_EmpID
        LEFT JOIN
    t_storelist d ON a.f_StoreID = d.f_StoreID
        LEFT JOIN
    t_surveypd e ON a.f_SurveyPDID = e.f_SurveyPDID
        LEFT JOIN
    t_customerdetails f ON e.f_CustomerDetailsID = f.f_CustomerDetailsID
        LEFT JOIN
    (SELECT
        b.f_UserName, a.f_DateTimeCreated,a.f_LogRemark
    FROM
        t_log a
    LEFT JOIN t_userlogin b ON a.f_UserLoginID = b.f_UserLoginID
    WHERE
        f_DateTimeCreated BETWEEN  '{$from} 00:00:00' and '{$to} 23:59:59' AND a.f_LogTypeID = 3 AND a.f_LogFromID = 2 AND a.f_LogFromSubID = 6) g ON g.f_LogRemark LIKE CONCAT('%', e.f_InvitationID, '%')
WHERE
    a.f_DateTimeResponseMelbourne BETWEEN  '{$from} 00:00:00' and '{$to} 23:59:59'
        AND d.f_StoreOwnerUserID =  '{$storeOwnerID}'
                      order by f_DateTimeResponseMelbourne DESC";
    $result = mysql_query($select, $connection);
   // echo $select;
    while ($row = mysql_fetch_assoc($result)) {
        
        if (is_null($row['f_LogRemark'])) {
            $return[$row['InivitationID']]['LogRemarks'] = "No modifications";
        } else {
            $LogRemark = json_decode($row['f_LogRemark'], true);
 

            $sqlGetStaffName = "select d.f_UserName from t_storelist a
                                left join t_empnumber b on a.f_StoreID = b.f_StoreID
                                left join t_emplist c on b.f_EmpID = c.f_EmpID
                                left join t_userlogin d on c.f_EmpID = d.f_EmpID
                                WHERE a.f_StoreOwnerUserID = {$storeOwnerID} AND b.f_EmpPnumber = '{$LogRemark['Pnumber']}' ";
            $resultGetStaffName = mysql_query($sqlGetStaffName, $connection);
            $rowGetStaffName = mysql_fetch_assoc($resultGetStaffName);
           // echo "<b>Assigned to: </b><i> " . $row['f_UserName'] . "</i></br>";
           $LogRemarks = "<b>Modified By: </b><i>" . $row['f_UserName'] . "</i><br>";
           $date = new DateTime($row['f_DateTimeCreated']);
           $date->setTimezone(new DateTimeZone('Australia/Melbourne')); // +04          
           $LogRemarks .= "<b>Date: </b><i>" . $date->format('d-M-y (h:i A)') . "</i><br>";
           $LogRemarks .= "<b>Assigned to PNumber: </b><i>" . $LogRemark['Pnumber'] . "</i><br>";
            $LogRemarks .= "<b>Assigned to: </b><i> " . $rowGetStaffName['f_UserName'] . "</i></br>";
            if (isset($LogRemark['OriginalPNumber'])) {
                               $sqlOriginalName = "select d.f_UserName from t_storelist a
                                left join t_empnumber b on a.f_StoreID = b.f_StoreID
                                left join t_emplist c on b.f_EmpID = c.f_EmpID
                                left join t_userlogin d on c.f_EmpID = d.f_EmpID
                                WHERE a.f_StoreOwnerUserID = {$storeOwnerID} AND b.f_EmpPnumber = '{$LogRemark['OriginalPNumber']}' ";
                $resultOriginalName = mysql_query($sqlOriginalName, $connection);
                $rowOriginalName = mysql_fetch_assoc($resultOriginalName);
                $LogRemarks .= "<b>Previous Assigned PNumber: </b><i> " . $LogRemark['OriginalPNumber'] . "</i></br><b>Previosly Assigned to: </b><i> " . $rowOriginalName['f_UserName'] . "</i></br>";
            }
            
           $return[$row['InivitationID']]['LogRemarks'][] = $LogRemarks;
        }
        
        $return[$row['InivitationID']]['Details'] = $row;
    }
    //echo $select;
    // $survey[] = $value[0]['SurveySDID'];
    // $return = array($data,$survey);
    return $return;
}

function getLatestSurveyList() {
    global $connection;

//    $select = "SELECT
//                MAX(b.f_DateTimeResponseMelbourne) as 'LatestResponseTime',
//                a.f_StoreListName as 'StoreName',
//                CONCAT(c.f_UserFirstName, ' ', c.f_UserLastName) AS StoreOwner,
//                MAX(b.f_SurveyInsertedDate) as 'InsertedDate',
//                DATEDIFF(NOW(),MAX(b.f_SurveyInsertedDate)) as 'DiffDate'
//            FROM
//                t_storelist a
//                    LEFT JOIN
//                t_surveysd b ON a.f_StoreID = b.f_StoreID
//                    LEFT JOIN
//                t_userlist c ON a.f_StoreOwnerUserID = c.f_UserID
//            WHERE
//                a.f_Status = 1 AND a.f_StoreID != 15
//            GROUP BY a.f_StoreID
//            ORDER BY StoreOwner";

    $select = "SELECT
    MAX(b.f_DateTimeResponseMelbourne) AS 'LatestResponseTime',
    a.f_StoreListName AS 'StoreName',
    CONCAT(c.f_UserFirstName, ' ', c.f_UserLastName) AS StoreOwner,
    MAX(b.f_SurveyInsertedDate) AS 'InsertedDate',
    DATEDIFF(NOW(), MAX(b.f_SurveyInsertedDate)) AS 'DiffDate',
    (SELECT
            CONCAT(COALESCE(x.f_CompanyCode, ''),
                        '<br>',
                        COALESCE(GROUP_CONCAT(f_MergeToCompanyCode
                                    SEPARATOR '<br>'),
                                ''))
        FROM
            t_storelist x
                LEFT JOIN
            t_mergereport y ON x.f_StoreID = y.f_StoreID
        WHERE
            f_Status = 1  AND x.f_StoreID != 15
                AND x.f_StoreID = a.f_StoreID
        GROUP BY a.f_StoreID) AS DealerCodes
FROM
    t_storelist a
        LEFT JOIN
    t_surveysd b ON a.f_StoreID = b.f_StoreID
        LEFT JOIN
    t_userlist c ON a.f_StoreOwnerUserID = c.f_UserID
WHERE
    a.f_Status = 1 AND a.f_StoreID != 15
GROUP BY a.f_StoreID
ORDER BY StoreOwner";

    // echo $select;
    $result = mysql_query($select, $connection);
    $i = 0;
    while ($row = mysql_fetch_assoc($result)) {
        if ($previousOwner != $row['StoreOwner']) {
            $i = 0;
        }

        $return[$row['StoreOwner']]['StoreName'][$i] = $row['StoreName'];
        $return[$row['StoreOwner']]['ResponseTime'][$i] = $row['LatestResponseTime'];
        $return[$row['StoreOwner']]['DateInserted'][$i] = array('Inserted' => $row['InsertedDate'], 'Diff' => $row['DiffDate']);
        $return[$row['StoreOwner']]['DealerCode'][$i] = $row['DealerCodes'];
        $return[$row['StoreOwner']]['Details'][$i] = array('StoreName' => $row['StoreName'], 'ResponseTime' => $row['LatestResponseTime'], 'Inserted' => $row['InsertedDate'], 'Diff' => $row['DiffDate'], 'DealerCode' => $row['DealerCodes']);
        //$return[$row['StoreOwner']]['DiffDate'][$i] = $row['DiffDate'];
        $i++;
        $previousOwner = $row['StoreOwner'];

//        $return[] = $row;
    }

    return $return;
}


function getSurveyHistory($InvitationID,$dateCreated){
    global  $connection;
    $getInsertedDate = "SELECT
        b.f_UserName as ModifiedBy, a.f_DateTimeCreated as DateLogCreated,a.f_LogRemark as LogRemarks
    FROM
        t_log a
    LEFT JOIN t_userlogin b ON a.f_UserLoginID = b.f_UserLoginID
    WHERE
        f_DateTimeCreated BETWEEN  '{$dateCreated}' and NOW() AND a.f_LogRemark LIKE '%{$InvitationID}%' ";
                        
                        
    $resultInserted = mysql_query($getInsertedDate,$connection);
    while($row = mysql_fetch_assoc($resultInserted)){
        $dataInserted[] = $row;
    }
    
    return $dataInserted;
}