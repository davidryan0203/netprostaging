<?php
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/datarecon/functions/DataReconFunction.php');
ini_set('max_execution_time', 300);
date_default_timezone_set('Australia/Melbourne');

session_start();
$data = getLatestSurveyList();

//echo "<pre>";
//print_r($data);
//exit;
?>

<!--<div class="tab-content" >
    <div class="table-responsive">
        <table id="example" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
            <thead>
                <tr>
                    <th style="min-width:80px;">Store Owner</th>
                    <th>Store Name</th>
                    <th>Response Time Melbourne</th>
                    <th>Date Time Uploaded</th>
                </tr>
            </thead>
            <tbody>
<?php
foreach ($data as $storeOwner => $storeDetails) {
    ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <tr>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <td>
    <?php echo $storeOwner; ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <b>
    <?php
    foreach ($storeDetails['StoreName'] as $storeName) {
        echo $storeName . "<br>";
    }
    ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </b>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <i>
    <?php
    foreach ($storeDetails['ResponseTime'] as $responseTime) {
        $responseTimeDate = new DateTime($responseTime);
        $responseTimeDate->setTimezone(new DateTimeZone('Australia/Melbourne')); // +04
        $response = $responseTimeDate->format('d-M-y (h:i A)');
        if ($response == date('d-M-y (h:i A)')) {
            echo "<tored class=\"tored\">No uploads yet!</tored><br>";
        } else {
            echo $response . "<br>";
        }
    }
    ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </i>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <i>
    <?php
    foreach ($storeDetails['DateInserted'] as $dateInserted) {
        $addThis = "";
        $endThis = "";

        if ($dateInserted['Diff'] >= 2) {
            $addThis = "<tored class=\"tored\" date=\"" . $dateInserted['Diff'] . "\"> ";
            $endThis = " </tored> ";
        }
        $dateInsertedDate = new DateTime($dateInserted['Inserted']);
        // $dateInsertedDate->setTimezone(new DateTimeZone('Australia/Melbourne')); // +04
        $thisDateInserted = $dateInsertedDate->format('d-M-y (h:i A)');
        if ($thisDateInserted == date('d-M-y (h:i A)')) {
            echo "<tored class=\"tored\">No uploads yet!</tored><br>";
        } else {
            echo $addThis . $thisDateInserted . "$endThis<br>";
            ;
        }
    }
    ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </i>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                </tr>
<?php } ?>
            </tbody>
        </table>
    </div>
</div>-->

<div class="tab-content" >
    <div class="table-responsive">
        <table id="example" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
            <thead>
                <tr>
                    <th style="min-width:80px;">Store Owner</th>
                    <th>Store Name</th>
                    <th>Dealer Code</th>
                    <th>Response Time Melbourne</th>
                    <th>Date Time Uploaded</th>

                </tr>
            </thead>
            <tbody>
                <?php
                $storeOwnerAlreadyPrinted = false;

                foreach ($data as $storeOwner => $storeDetails) {
                    $rowSpawnCount = count($storeDetails['Details']);


                    $i = 0;
                    $shown = "false";
                    foreach ($storeDetails['Details'] as $details) {
                        $i++;
                        echo "<tr>";
                        //   print_r($details);
                        if ($shown == "false") {
                            echo "<td rowspan='{$rowSpawnCount}'>{$storeOwner}</td>";
                        } else {
                            echo "<td style='display:none'>{$storeOwner}</td>";
                        }

                        $shown = "true";


                        echo "<td>{$details['StoreName']}</td>";
                        echo "<td>{$details['DealerCode']}</td>";
                        $responseTime = $details['ResponseTime'];
                        $responseTimeDate = new DateTime($responseTime);
                        $responseTimeDate->setTimezone(new DateTimeZone('Australia/Melbourne')); // +04
                        $response = $responseTimeDate->format('d-M-y (h:i A)');
                        if ($response == date('d-M-y (h:i A)')) {
                            echo "<td><tored class=\"tored\">No uploads yet!</tored></td>";
                        } else {
                            echo "<td>{$response}</td>";
                        }

                        $addThis = "";
                        $endThis = "";

                        if ($details['Diff'] >= 2) {
                            $addThis = "<tored class=\"tored\" date=\"" . $details['Diff'] . "\"> ";
                            $endThis = " </tored> ";
                        }
                        $dateInsertedDate = new DateTime($details['Inserted']);
                        // $dateInsertedDate->setTimezone(new DateTimeZone('Australia/Melbourne')); // +04
                        $thisDateInserted = $dateInsertedDate->format('d-M-y (h:i A)');
                        if ($thisDateInserted == date('d-M-y (h:i A)')) {
                            echo "<td><tored class=\"tored\">No uploads yet!</tored></td>";
                        } else {
                            echo "<td>" . $addThis . $thisDateInserted . "$endThis<br></td>";
                        }

                        echo "</tr>";
                    }

                    $storeOwnerAlreadyPrinted = true;
                    ?>


                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<style>
    #exclude{
        table, caption, tbody, tfoot, thead, tr, th, td {
            margin: 0;
            padding: 0;
            border: 0;
            outline: 0;
            font-size: 100%;
            vertical-align: baseline;
            background: transparent;
        }
    }
    .tored {
        background-color: red;
        color: #ffffff;
        opacity: .6;
    }

</style>

<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#example').DataTable({
            dom: 'lBfrtip',
            "iDisplayLength": -1,
            "bSort": false,
            //  "aLengthMenu": [[15, 50, 100, -1], [15, 50, 100, "All"]],
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
        });
    });

    $(function() {
        $('[data-tooltip="tooltip"]').tooltip();
    })
</script>























