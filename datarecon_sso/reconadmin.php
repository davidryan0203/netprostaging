<?php
include('admin_sso/function/UserFunction.php');
include('datarecon/functions/DataReconFunction.php');
ini_set('max_execution_time', 300);

error_reporting(E_ALL);
session_start();
$role = $_SESSION['UserRoleID'];
$getStoreOwnerID = getStoreOwner();

if ($getStoreOwnerID == 'ALL') {
    $userParams = 'Owner';
} else {
    $userParams = $getStoreOwnerID;
}
$storeList = SelectUser($userParams);
//echo "<pre>";
?>

<div class="row">
    <div class="col-lg-12">
        <h4 class="page-header"><i class="fa fa-comments fa-fw"></i>Data Recon</h4>
        <div class="filters" >

            <?php
            $withSearch = array('1', '6');
            if (in_array($role, $withSearch)) {
                ?>
                <select name="storeOwnerID" class="input-sm" id="storeOwnerID">
                    <option value="">Select store director</option>
                    <?php
                    foreach ($storeList as $value) {
                        echo '<option value="' . $value['f_UserID'] . '">' . $value['f_UserFirstName'] . ' ' . $value['f_UserLastName'] . '</option>';
                    }
                    ?>
                </select> 
            <?php } else { ?> 
                <input type="hidden" name="storeOwnerID" id="storeOwnerID" value="<?php echo $getStoreOwnerID; ?>" />
            <?php } ?>
            <button type="submit" class="btn btn-warning btn-circle" id="refresh" data-tooltip="tooltip" data-placement="bottom" title="Reload"> 
                <i class="fa fa-refresh"></i> 
            </button>
        </div>
    </div>   
</div>




<div role="tabpanel">
 
  
    <!-- Tab panes -->
    <div class="tab-content">
        <div class='main-content' style='margin-top:2px !important;' id="reconpanel">
            <div role="tabpanel" class="tab-pane active" id="page-loader"></div>
        </div>
    </div>
</div>

<script>
 

    $(document).ready(function () {
        $('#page-loader').load('admin_sso/employee.php').fadeIn("slow");
        $('#adminTab').tab();
    });
    
       $(document).ready(function () {
        $('#refresh').click(function () {
            var toLoad = setLoad();
            $('#page-loader').fadeOut().load(toLoad).fadeIn();
        });

    });
    
    function setLoad() {

        var storeOwnerID = $("#storeOwnerID").val();
        var active = $(".active").attr('id');
        var specialurl = 'admin_sso/employee.php';
        
        var toLoad = specialurl+'?activeOwnerID='+storeOwnerID;

        return toLoad;
    }

    
</script>


