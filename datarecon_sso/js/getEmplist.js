$(function () {

    var dataStore = (function () {
        var html;
        var url = "datarecon/functions/DataReconFunctionCaller.php?module=GetEmployeeList";
        var storeID = $('#storeID').serialize();
     // console.log(url+'&'+storeID);
        function load() {
            $.ajax({
                async: false,
                type: "GET",
                url: url,
                dataType: "json",
                 data: storeID ,
                success: function (data) {
                    html = data;
                }
            });
        }
        return {
            load: function () {
                if (html)
                    return;
                load();
            },
            getHtml: function () {
                if (!html)
                    load();
                return html;
            }
        }
    })();
    
   

    var currencies = dataStore.getHtml();
    var firstval = currencies[0].value;
    if(firstval == "No Employee available"){
         $('#autocomplete').attr("disabled",true).val(firstval);
         $('#btnSubmit').attr("disabled",true);
    }
    // console.log(currencies[0]);
    $('#autocomplete').autocomplete({
        lookup: currencies,
        onSelect: function (suggestion) {

            var data = suggestion.data;
            $('#Pnumber').val(data);
          
        }
    });

});