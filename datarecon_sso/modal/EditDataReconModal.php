<?php
include($_SERVER['DOCUMENT_ROOT'] . '/admin_sso/function/SubFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin_sso/function/UserFunction.php');
$varID =  $_GET['userid'];

$userData = SelectUser($varID);
$roleList = SelectUserRoleList();

//var_dump($userData);

?>
<script type="text/javascript">

$(function () {
    $("#ajaxmsgs").hide();
}); // default hide divs for modal open
$(document).ready(function () {

    $('#changePass').change(function () {
        if (!this.checked) {
            $('#confirmPassword').fadeOut('slow');
                $('#Password').prop('disabled', true);
                $('#confirmPassword').prop('disabled', false);
            }
            else {
                $('#confirmPassword').fadeIn('slow');
                $('#confirmPassword').prop('disabled', false);
                $('#Password').prop('disabled', false);
                document.getElementById('confirmPassword').style.display = 'block';
            }
        });
        
        
          $('#confirmPassword').blur(function (e) { 
              var pass1 = $('#Password').val();
              var pass2 = $('#confirmPassword').val();
              var div = $('#checkPass');
              
              if(pass1 != pass2){
                  div.html('<span class="erroruser">Password and Confirm Password not match</span>');
              }
              else{
                   div.html('');
              }
              e.preventDefault();
          });
        
        $('#UserName').keyup(function () { // Keyup function for check the user action in input
            var Username = $(this).val(); // Get the username textbox using $(this) or you can use directly $('#username')
            var UsernameAvailResult = $('#username_avail_result'); // Get the ID of the result where we gonna display the results
            if (Username.length > 4) { // check if greater than 5 (minimum 6)
                UsernameAvailResult.html('Loading..'); // Preloader, use can use loading animation here
                var UrlToPass = 'action=username_availability&username=' + Username;
                $.ajax({// Send the username val to another checker.php using Ajax in POST menthod
                    type: 'POST',
                    data: UrlToPass,
                    url: 'admin_sso/function/CheckUserName.php',
                    success: function (responseText) { // Get the result and asign to each cases
                        if (responseText == 0) {
                            UsernameAvailResult.html('<span class="successuser">Username name available</span>');
                        }
                        else if (responseText > 0) {
                            UsernameAvailResult.html('<span class="erroruser">Username already taken</span>');
                        }
                        else {
                            alert('Problem with sql query');
                        }
                    }
                });
            } else {
                UsernameAvailResult.html('Enter atleast 5 characters');
            }
            if (Username.length == 0) {
                UsernameAvailResult.html('');
            }
        });

        $('#UserPassword, #UserName').keydown(function (e) { // Dont allow users to enter spaces for their username and passwords
            if (e.which == 32) {
                return false;
            }
        });

        $('#btnSubmit').on('click', function (event) {
            //alert('here');
            var url = 'admin_sso/function/UserFunctionCaller.php?module=EditUser';
          //  console.log($("#EditUserForm").serialize());
            var mydata = $("#EditUserForm").serialize();
            $.ajax({
                url: url,
                type: 'get',
                data: mydata,
                dataType: 'json',
                success: function (result) {
                    document.getElementById('ajaxmsgs').style.display = 'block';
                    if (result.type == 'Success') {
                        $('#ajaxmsgs').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong><br>" + result.message + "</div>");
                    }
                    else {
                        $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong><br>" + result.message + "</div>");
                    }
                    //alert('success error');
                },
                error: function () {
                    document.getElementById('ajaxmsgs').style.display = 'block';
                    $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
                    //alert('here error');
                }
            });
            event.preventDefault();
        }); // ajax button form submit
    });
</script>
<style type="text/css">  
    .successuser{
        color:#009900;
    }
    .erroruser{
        color:#F33C21;
    }
    .username_avail_result .checkPass{
        display:block;
        width:180px;
    }</style>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Edit User</h4>
</div>	
<form class="form-horizontal" role="form" id="EditUserForm"><!-- /modal-header -->
    <div class="modal-body">
        
        <div class="form-group">
            <label class="control-label col-sm-3" for="UserName">User Name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="UserName" name="UserName" placeholder="Enter User Name" value="<?php echo $userData[0]['f_UserName']; ?>">
                <div class="username_avail_result" id="username_avail_result"></div>
            </div>
        </div> <!-- /div for store name -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="Password">Password:</label>
            <div class="col-sm-6">
              <input type="password" class="form-control" id="Password" name="Password" disabled><input type="checkbox" id="changePass" name="changePass" class="btn-large" >    
                 Change Password? <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" style="display:none" disabled><div class="checkPass" id="checkPass"></div>
            </div>             
        </div> <!-- /div for password -->
       
        <div class="form-group">
            <label class="control-label col-sm-3" for="FirstName">First Name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="FirstName" name="FirstName" placeholder="Enter First Name" value="<?php echo $userData[0]['f_UserFirstName']; ?>">
            </div>
        </div> <!-- /div for first name -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="LastName">Last Name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="LastName" name="LastName" placeholder="Enter Last Name" value="<?php echo $userData[0]['f_UserLastName']; ?>">
            </div>
        </div> <!-- /div for last name -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="UserRoleID">Role:</label>
            <div class="col-sm-6">
                <select name="UserRoleID" class="form-control" >
                    <option value="<?php echo $userData[0]['f_UserRoleID']?>">Please select</option>
                    <?php
                    foreach ($roleList as $value) { ?>
                        <option value="<?php echo $value['f_UserRoleID'];?>" <?php if($value['f_UserRoleID']==$userData[0]['f_UserRoleID']){ echo "selected='selected'";} ?> ><?php echo $value['f_UserRoleName'];?></option>
                   <?php  }
                    ?>
                </select>
            </div>
        </div> <!-- /div for store owner -->    
        <input type="hidden" name="UserID" value="<?php echo $varID; ?>" />
        <div id="ajaxmsgs"></div>  <!-- ajaxmsg div -->
    </div> <!-- /modal-body -->	
    <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-sm btn-primary"  id="btnSubmit" name="btnSubmit">Submit</button>
    </div> <!-- /modal-footer -->	
</form>  <!-- /form --> 

