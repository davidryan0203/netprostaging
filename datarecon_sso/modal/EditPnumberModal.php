<?php
//include($_SERVER['DOCUMENT_ROOT'] . '/admin_sso/function/SubFunction.php');
//include($_SERVER['DOCUMENT_ROOT'] . '/admin_sso/function/UserFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin_sso/function/EmployeeFunction.php');
session_start();
$role = $_SESSION['UserRoleID'];

$storeOwnerID = $_GET['storeOwnderID'];
$invitationID = $_GET['invitationID'];
$SDID = $_GET['SDID'];
$storeID = $_GET['storeID'];
//$storeData = SelectStore($storeID);
//$storeList = SelectStoreList();
//$getEmplist = SelectEmployee($storeOwnerID, 'store');
?>
<link rel="stylesheet" type="text/css" media="all" href="npstracker/style.css">

<script type="text/javascript">

//    $(function () {
//        
//    }); // default hide divs for modal open
    $(document).ready(function () {
        $("#ajaxmsgs").hide();
        var selectedID = $('#storeOwnerID').val();
        $('#OwnerID').val(selectedID);
        
        $('#resetPrompt').click(function () {
            //$('#input1').attr('name', 'other_amount');
            $("#input1").attr("name", "content");
        });
        $('#btnSubmit').on('click', function (event) {
            //alert('here');
            if ($("#EditBlankPnumberForm")[0].checkValidity()) {
                var url = 'datarecon/functions/DataReconFunctionCaller.php?module=UpdateBlankPnumber';
              //  console.log($("#EditBlankPnumberForm").serialize());
                var mydata = $("#EditBlankPnumberForm").serialize();
                //return false;
                $.ajax({
                    url: url,
                    type: 'get',
                    data: mydata,
                    dataType: 'json',
                    success: function (result) {
                        document.getElementById('ajaxmsgs').style.display = 'block';
                        if (result.type == 'Success') {
                            $('#ajaxmsgs').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong><br>" + result.message + "</div>");
                            ("#EditBlankPnumberForm :input").attr("disabled", true);
                            $("#cancel").attr("disabled", false).html('Close');
                       }
                        else {
                            $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong><br>" + result.message + "</div>");
                        }
                        //alert('success error');
                    },
                    error: function () {
                        document.getElementById('ajaxmsgs').style.display = 'block';
                        $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
                        //alert('here error');
                    }
                });
            } else {
                $("#EditBlankPnumberForm").find(':submit').click();

            }
            event.preventDefault();

        }); // ajax button form submit
    });
</script>

<style type="text/css">  
    .successstore{
        color:#009900;
    }
    .errorstore{
        color:#F33C21;
    }
</style>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Edit Blank PNumber</h4>
</div>	
<form class="form-horizontal" role="form" id="EditBlankPnumberForm"><!-- /modal-header -->
    <div class="modal-body">

        <div class="form-group">
            <label class="control-label col-sm-3" for="StoreListName">Invitation ID:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="invitationID" name="invitationID"  value="<?php echo $invitationID; ?>" readonly>                
            </div>
        </div> <!-- /div for store name -->       
        <div class="form-group">
            <label class="control-label col-sm-3" for="EmployeeID">Employee Number: </label>
            <div class="col-sm-6">
                <input type="hidden" name="OwnerID" id="OwnerID" />
                <input type="hidden" name="SDID" value="<?php echo $SDID; ?>" /> 
                <input type="text" name="currency" class="form-control" id="autocomplete" style="display:inline;" required>  
                <!--<button class="btn btn-sm btn-default" id="clear" type="button">Clear</button>-->
                <input type="hidden" name="Pnumber" id="Pnumber" value="" />
                <input type="hidden" name="storeID" id="storeID" value="<?php echo $storeID ?>" />
<!--                <select name="EmpNumber" class="form-control" >
                <option value="">Please select</option>
                <?php foreach ($getEmplist as $value) { ?>
                        <option value="<?php echo $value['f_EmpPNumber']; ?>" > <?php echo $value['f_EmpPNumber'] . ' - ' . $value['f_EmpFirstName'] . ' ' . $value['f_EmpLastName'] ?></option>
                <?php }
                ?>
            </select>-->
            </div>       
        </div> <!-- /div for shared to store owner -->              
        <div id="ajaxmsgs"></div>  <!-- ajaxmsg div -->
    </div> <!-- /modal-body -->	
    <div class="modal-footer">
         <input type="submit" style="visibility: hidden"/>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-sm btn-primary"  id="btnSubmit" name="btnSubmit">Submit</button>
        <button type="reset" class="btn btn-sm btn-danger" >Reset</button>

    </div> <!-- /modal-footer -->	
</form>  <!-- /form --> 

<script type="text/javascript" src="npstracker/js/jquery.autocomplete.min.js"></script>
<script type="text/javascript" src="datarecon/js/getEmplist.js"></script>