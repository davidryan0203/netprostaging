<?php
//require($_SERVER['DOCUMENT_ROOT'] . '/dashboard/function/rawFunction.php');
// $stores = getPartnerStores();
// $users = getUsers();
// $directors = getDirectors();


session_start();
if (isset($_SESSION['UserProfileID']) || isset($_SESSION['EmpProfileID'])) {
    header("location: /public_html.php");
    exit();
}
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <title>NetPro</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Logo-->
        <link rel="shortcut icon" href="images/logoIcon.png">
        <!--Custom Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,400italic,500,500italic,700,700italic,900,900italic,300italic,300" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
        <!--Font Awesome-->
        <link href="includes/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <!--Full slider -->
        <link href="includes/css/jquery.maximage.css" rel="stylesheet" type="text/css"/>
        <link href="includes/css/jquery.screensize.css" rel="stylesheet" type="text/css"/>
        <!--Animate Css -->
        <link href="includes/css/animate.css" rel="stylesheet" type="text/css"/>
        <!--Bootstrap-->
        <link href="includes/css/bootstrap.min.flatly.css" rel="stylesheet" type="text/css"/>
        <!--Custom CSS -->
        <link href="includes/css/stylesV2.css" rel="stylesheet" type="text/css"/>
        <!--Custom Css-->
        <link href="includes/css/homeV2.css" rel="stylesheet">
        <!--Media Queries -->
        <link href="includes/css/media-queries.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">

    </head>
    <Style>
        p,h1,h2,h3,h4,h5{
            color:#4b4b4b;
        }

        h1 {
            font-family: "Open Sans";  font-style: normal; font-variant: normal; font-weight: 700;
        }
        h3 {
            font-family: "Open Sans";  font-style: normal; font-variant: normal; font-weight: 700;
        }
        p {
            font-family: "Open Sans";  font-style: normal; font-variant: normal; font-weight: 400;
        }
        blockquote {
            font-family: "Open Sans"; font-style: normal; font-variant: normal; font-weight: 400;
        }
        pre {
            font-family: "Open Sans"; font-size: 13px; font-style: normal; font-variant: normal; font-weight: 400;
        }

        .loginBtn{
            color: #fff !important;
            float: right;
            margin-top: -5px;
            height: 30px;
            padding-top:3px;
        }

        input[type="text"],[type="email"],[type="password"]{
            font-size: 20px;
        }

        #yourMessage { font-size: 20px; }


        @media (min-width: 992px){

            .white-text{
                color:#fff;
            }

            .modal-lg {
                width: 1200px;
            }

            .uppercase{
                text-transform: uppercase;
                color:#39C7F5;
            }

            #banner{
                /*height: 85vh;*/
                height: 0vh;
            }

            .subheading{
                font-weight: normal;
                color:#9E9E9E;
            }

            .trialBtn{
                min-width: 100%;
                font-size: 26px;
                padding:0px;
                font-weight:600;
            }

            .btn{
                -webkit-box-shadow: 0px 0px 0px rgba(4, 4, 4, 0.3);
                -moz-box-shadow:    0px 0px 0px rgba(4, 4, 4, 0.3);
            }

            .navHover:hover{
                border-top:2px solid #08B99F;
            }

            .navbar-inverse .navbar-nav>li>a {
                color:#888888 !important;
            }

            .table-bordered th, .table-bordered td{
                /* border: 2px solid #888 !important;*/
            }

            .well {
                background-color: #F9F9F9 !important;
                min-height: 100px;
                text-align: center;
            }

            .grayText{
                color:#999999;
            }
        }

        .arrow_box {
            position: relative;
            background: #fff;
            border: 4px solid #fff;
        }
        .arrow_box:after, .arrow_box:before {
            top: 100%;
            left: 50%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
        }

        .arrow_box:after {
            border-color: rgba(255, 255, 255, 0);
            border-top-color: #fff;
            border-width: 20px;
            margin-left: -20px;
        }
        .arrow_box:before {
            border-color: rgba(255, 255, 255, 0);
            border-top-color: #fff;
            border-width: 26px;
            margin-left: -26px;
        }

        .col-center {
            margin: 0 auto;
            float: none !important;
        }
        .carousel {
            margin: 50px auto;
            /*padding: 0 70px;*/
            padding: 0px;
        }
        .carousel .item {
            color: #999;
            font-size: 14px;
            text-align: center;
            overflow: hidden;
            min-height: 290px;
        }



        .carousel .item .img-box {
            width: 135px;
            height: 135px;
            margin: 0 auto;
            padding: 5px;
            border-radius: 50%;
        }
        .carousel .img-box img {
            width: 100%;
            height: 100%;
            display: block;
            border-radius: 50%;
        }
        .carousel .testimonial {
            padding: 30px 0 10px;
            color:#4b4b4b;
        }
        .carousel .overview {
            font-style: italic;
        }
        .carousel .overview b {
            text-transform: uppercase;
            color: #7AA641;
        }
        .carousel .carousel-control {
            width: 40px;
            height: 40px;
            margin-top: -20px;
            top: 50%;
            background: none;
        }
        .carousel-control i {
            font-size: 68px;
            line-height: 42px;
            position: absolute;
            display: inline-block;
            color: rgba(0, 0, 0, 0.8);
            text-shadow: 0 3px 3px #e6e6e6, 0 0 0 #000;
        }

        .carousel-indicators li, .carousel-indicators li.active {
            width: 10px;
            height: 10px;
            margin: 1px 3px;
            border-radius: 50%;
        }
        .carousel-indicators li {
            background: #999;
            border-color: transparent;
            box-shadow: inset 0 2px 1px rgba(0,0,0,0.2);
        }
        .carousel-indicators li.active {
            /*background: #555;       */
            box-shadow: inset 0 2px 1px rgba(0,0,0,0.2);
        }

        .carousel-indicators li.active {
            background: #08B99F;
            box-shadow: inset 0 2px 1px rgba(0,0,0,0.2);
            /* font-size: 42px; */
            height: 15px;
            width: 15px;
            margin: -1px 3px;
        }

        .triangle {
            position: relative;
            /*margin: 3em;*/
            padding: 1em;
            box-sizing: border-box;
            background: #fff;
            border-radius: 15px;
            box-shadow: 8px 7px 12px 3px #ccc;
        }
        .triangle::after{
            content: "";
            position: absolute;
            width: 0;
            height: 0;
            margin-left: -0.5em;
            bottom: -2em;
            left: 50%;
            box-sizing: border-box;

            border: 1em solid black;
            border-color: transparent transparent #fff #fff;

            transform-origin: 0 0;
            transform: rotate(-45deg);

            box-shadow: -3px 7px 3px 0 #ccc;
        }

        .triangle--problem {
            position: relative;
            margin: 5em;
            padding: 1em;
            box-sizing: border-box;

            background: #bada55;

            box-shadow: 0px 3px 3px 0 rgba(0, 0, 0, 0.4);

        }
        .triangle--problem::after {
            content: "";
            position: absolute;
            width: 0;
            height: 0;
            margin-left: -0.75em;
            bottom: -3em;
            left: 50%;
            box-sizing: border-box;

            border: 1.5em solid black;
            border-color: #bada55 transparent transparent transparent;

            box-shadow: 0px 3px 3px 0 rgba(0, 0, 0, 0.4);
        }

        .navbar {
            min-height: 90px;
            padding-top:20px;
        }

        .item {
            padding: 0px;
            /*border-bottom: 1px solid #eeeeee;*/
        }

        .jumbotron {
            color: #F9F9F9;
            background-image: url("images/index_image/main_header.jpg");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            /*height: 85vh;*/
            height: 50em;
            padding-bottom: 0px;
            margin-bottom: 0px;
        }

        .headings{
            font-weight: normal;
            color:#4b4b4b;
        }

        footer {
            color: #ffffff;
            padding: 20px 0;
            /*background-color: #079FD0;*/
            background-image:url('images/backgrounds/footer.png') !important;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        .bannerPanel{
            border-radius: 15px;padding: 0px;
        }

        @media only screen and (max-width: 992px){
            #banner{
                height: 0vh;
            }
        }
    </Style>

    <body>
        <div class="main-wrapper">
            <!-- Banner -->
            <section id="banner" style="min-height: 70px">
                <!--full page slider-->
                <!--   <div class="animated fadeIn" id="maximage">
                      <img src="images/slides/main.png" alt=""/>
                  </div> -->

                <!-- Navbar-->
                <nav class="navbar navbar-inverse" id="navbar">
                    <div class="container">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-navguest" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand hidden-xs" href="/">
                                <img src="images/netpro_logo.png" alt="Telstra Blog">
                            </a>

                            <a class="navbar-brand visible-xs" href="/">
                                <img src="images/netpro_logo.png" alt="Telstra Blog" style="width:80% !important;">
                            </a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="collapse-navguest">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="/"><span class="navHover">Home</span></a></li>
                                <li><a href="#initial"><span class="navHover">Features</span></a></li>
                                <li><a href="#whyNetpro"><span class="navHover">Why NetPro</span></a></li>
                                <li><a href="#pricing"><span class="navHover">Pricing</span></a></li>
                                <!-- <li><a href="/signup.php"><span class="navHover">Signup</span></a></li> -->
                                <li class="hidden-xs hidden-md"><a data-toggle = 'modal' data-target = '#LoginModal'><span class="btn btn-success  loginBtn">Login</span></a></li>
                                <li class="visible-xs visible-md"><a data-toggle = 'modal' data-target = '#LoginModal'>Login</a></li>
                                <!--<li ><a href="login.php"  role="button"><span class="btn btn-info loginBtn"  ">Azenko</span></a></li>-->
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>
            </section>

            <div class="jumbotron jumbotron-fluid">
                <div id='page-content'>
                    <div id="bannerText">
                        <h1 class="title text-center headingText">Your online NPS tracking tool</h1><br/><br/>
                        <h2 class="text-center white-text subHeadingText">
                            Easily monitor customer advocacy results across your business.<br/>Drill down to individual staff level, with this<br/>dynamic reporting platform.
                        </h2>
                        <br/><br/>
                        <div class="panel panel-default col-lg-4 col-lg-offset-4 col-sm-offset-4 col-sm-6 col-md-6 col-md-offset-3 col-xs-12 bannerPanel">
                            <div class="panel-heading headingTrialPanel">
                                <h2 class="text-center uppercase trialText">SEE NETPRO IN ACTION</h2></div>
                            <div class="panel-body" style="padding:15px 15px 25px 15px">
                                <h3 class="text-center subheadingParagraph">It's quick to set-up, with no lock-in contract &#8212;<br/> so get NetPro for your team today and<br/> see how easy NPS reporting can be.</h3>
                                <a href="https://azenko.com.au/azenko-service-activation/" class="col-lg-10 col-lg-offset-1"><button type="button" class="btn btn-success trialBtn">Click here to activate</button></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6" style="margin-top:2%">

                    </div>
                </div>
            </div>


            <section id='initial' style="background:#F9F9F9;padding:40px 0px 0px 0px; clear:both;">
                <div class='container'>
                    <h2 class="text-center headings">Features</h2><br/>

                    <div class='row'>
                        <div class="content col-xs-12 col-md-6 col-lg-4 col-sm-6 col-md-6 section1Content" style="padding:0px">
                            <div class='well'>
                                <img src="images/index_image/time.png" height="100">
                                <br/><br/>
                                <h4 style="font-size:26px;margin-bottom:10px;color:#4b4b4b;" class="text-center">Updates daily</h4>
                                <div class="desc">
                                    <p class="section1Paragraph">
                                        NPS surveys are updated by<br/>9.30am and 5.30pm (AEST)<br/>Mon-Fri and once a day Sat-Sun.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="content col-xs-12 col-md-6 col-lg-4 col-sm-6 col-md-6 section1Content" style="padding:0px">
                            <div class='well'>
                                <img src="images/index_image/staff.png" height="100">
                                <br/><br/>
                                <h4 style="font-size:26px;margin-bottom:10px;color:#4b4b4b;" class="text-center">Individual Staff Profiles</h4>
                                <div class="desc">
                                    <p class="section1Paragraph">
                                        Your whole team can monitor<br/>their NPS performance via<br/>their own login.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="content col-xs-12 col-md-6 col-lg-4 col-sm-6 col-md-6 section1Content" style="padding:0px">
                            <div class='well'>
                                <img src="images/index_image/devices.png" height="100">
                                <br/><br/>
                                <h4 style="font-size:26px;margin-bottom:10px;color:#4b4b4b;" class="text-center">Works across all devices</h4>
                                <div class="desc">
                                    <p class="section1Paragraph">
                                        Easy to use, mobile-friendly<br/>web interface. Optimised for<br/>desktop, phone and tablet.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="content col-xs-12 col-md-6 col-lg-4 col-sm-6 col-md-6 section1Content" style="padding:0px">
                            <div class='well'>
                                <img src="images/index_image/range.png" height="100">
                                <br/><br/>
                                <h4 style="font-size:26px;margin-bottom:10px;color:#4b4b4b;" class="text-center">Dynamic Reporting</h4>
                                <div class="desc">
                                    <p class="section1Paragraph">
                                        Range of automated reports,<br/>complete with date range<br/>capability, graphical trend views<br/>and export functionality.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="content col-xs-12 col-md-6 col-lg-4 col-sm-6 col-md-6 section1Content" style="padding:0px">
                            <div class='well'>
                                <img src="images/index_image/employee.png" height="100">
                                <br/><br/>
                                <h4 style="font-size:26px;margin-bottom:10px;color:#4b4b4b;" class="text-center">Monitor Staff Performance</h4>
                                <div class="desc">
                                    <p class="section1Paragraph">
                                        Managers can easily track team<br/>results plus extract reports to<br/>help with staff performance<br/>reviews.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="content col-xs-12 col-md-6 col-lg-4 col-sm-6 col-md-6 section1Content">
                            <div class='well'>
                                <img src="images/index_image/customer.png" height="100">
                                <br/><br/>
                                <h4 style="font-size:26px;margin-bottom:10px;color:#4b4b4b;" class="text-center">View Customer Verbatim</h4>
                                <div class="desc">
                                    <p class="section1Paragraph">
                                        See the customer feedback at a<br/>glance, filtered by advocates,<br/>passives and detractors,<br/>down to individual staff.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id='whyNetpro' style="background:#fff;padding:40px 0px 0px 0px; clear:both;min-height: 46vh">
                <div class='container'>
                    <h2 class="text-center headings">Why NetPro</h2><br/>
                    <h3 class="text-center whyNetproSubHeading" style="font-weight: normal;color:#00b7f1;font-size:32px;padding-bottom:15px;">All you need for NPS in one place, at your fingertips, for your whole team.</h3><br/>

                    <div class='row' style="padding-bottom:45px;">
                        <div class="content col-xs-12 col-md-6 col-lg-6 col-md-6 col-sm-6 section2Content" style="padding-bottom: 15px;">
                            <div class="col-lg-3 col-sm-12 col-md-12 section2Icon">
                                <img src="images/index_image/magnify.png" style="width: 90px">
                            </div>
                            <div class="col-lg-9 col-xs-12 section2Paragraph">
                                <h3 class="grayText section2TextContent hidden-xs">
                                    Superior visibility of your<br/>NPS performance across<br/>your whole business
                                </h3>

                                <h3 class="grayText section2TextContent visible-xs">
                                    Superior visibility of<br/>your NPS performance<br/>across your whole business
                                </h3>
                            </div>
                        </div>
                        <div class="content col-xs-12 col-md-6 col-lg-6 col-md-6 col-sm-6 section2Content">
                            <div class="col-lg-3 col-sm-12 col-md-12 section2Icon">
                                <img src="images/index_image/bulb.png" style="width: 90px">
                            </div>
                            <div class="col-lg-9 section2Paragraph">
                                <h3 class="grayText section2TextContent ">
                                    Empower your team to improve<br/>using view of customer insights<br/>over time
                                </h3>
                            </div>
                        </div>
                        <div style="clear:both;margin-top:30px;"></div>
                        <div class="content col-xs-12 col-md-6 col-lg-6 col-md-6 col-sm-6 section2Content">
                            <div class="col-lg-3 col-sm-12 col-md-12 section2Icon">
                                <img src="images/index_image/clock.png" style="width: 90px">
                            </div>
                            <div class="col-lg-9 section2Paragraph">
                                <h3 class="grayText section2TextContent">
                                    Spend less time collating data,<br/>and more time analysing<br/>results and taking action
                                </h3>
                            </div>
                        </div>

                        <div class="content col-xs-12 col-md-6 col-lg-6 col-md-6 col-sm-6 section2Content">
                            <div class="col-lg-3 col-sm-12 col-md-12 section2Icon">
                                <img src="images/index_image/hour.png" style="width: 90px">
                            </div>
                            <div class="col-lg-9 section2Paragraph">
                                <h3 class="grayText section2TextContent">
                                    Convenience and flexibility for<br/>your whole team, being able to<br/>access anywhere, anytime
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="testimonial" style="background-image:url('images/backgrounds/testimonial.png');">
                <div class="container">
                    <h2 class="text-center headings">From Our Customers</h2>

                    <div class="row">
                        <div class="col-md-12 col-center m-auto">
                            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                <!-- Carousel indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                    <li data-target="#myCarousel" data-slide-to="1"></li>
                                </ol>
                                <!-- Wrapper for carousel items -->
                                <div class="carousel-inner" style="margin-top:-30px;">
                                    <div class="item carousel-item active" style="margin-bottom: 21px;">
                                        <div  class="testimonial triangle triangleContainer" style="padding: 20px;margin: 20px;">
                                            <p>
                                                <span class="testimonialHeader">NetPro saves us time and effort, making NPS reporting easy.</span><br/>
                                            </p>

                                            <p style="font-size:20px;color:#787878;padding-top:10px;padding-bottom: 10px;">We've used NetPro daily across our business since 2015.</p>
                                            <p style="font-size:20px;color:#787878;">Our sales staff track their own NPS performance, at the same time Managers can view</p>
                                            <p style="font-size:20px;color:#787878;">team results and provide coaching at an individual level as well.</p>
                                            <p style="font-size:20px;color:#787878;padding-top:10px">Other systems can't do what NetPro does, like assign surveys to staff</p>
                                            <p style="font-size:20px;color:#787878;">regardless of product type or business unit.</p>

                                        </div>
                                        <br/><br/>
                                        <div class="col-lg-12 col-xs-12 testimonialAuthor">
                                            <div class="img-box col-lg-2 col-lg-offset-2 col-md-offset-2 col-md-2 col-sm-2 col-sm-offset-2 col-xs-3"><img src="images/clients/1.png" alt=""></div>
                                            <div class="col-lg-8 col-sm-8 col-md-8 col-xs-8"><h2 class="authorName"><b>Jacqaomi Evans</b> <br/><span class="testimonial1Position">Retail Operations Manager<br/>TShopBiz Group</span></h2></div>
                                        </div>
                                    </div>

                                    <div class="item carousel-item">
                                        <div class="testimonial triangle triangleContainer">
                                            <p>
                                                <span class="testimonialHeader">NetPro's helped shaped the way we deliver customer service in store</span><br/>
                                            </p>
                                            <p style="font-size: 17px;color:#787878;padding-top:10px">Ive used Netpro for over 5 years across three stores and it's a critical part of our operations. </p>
                                            <p style="font-size: 17px;color:#787878;padding-top:10px;">You can view current NPS performance and feedback, with an easy link back to the customer's profile so you can </p>
                                            <p style="font-size: 17px;color:#787878;">explore the journey of advocates and detractors, from the beginning of the interaction up until the first bill. </p>
                                            <p style="font-size: 17px;color:#787878;padding-top:10px">Staff coaching is made easy, with automated trend and performance reports </p>
                                            <p style="font-size: 17px;color:#787878;">that help me drill down to each team member's focus areas quickly. </p>
                                            <p style="font-size: 17px;color:#787878;padding-top:10px;">NetPro reinforces to my team the importance of quality follow-up with every customer </p>
                                            <p style="font-size: 17px;color:#787878;">after they've finished their transaction.</p>
                                            </p>
                                        </div>
                                        <br/>

                                        <div class="col-lg-12 col-xs-12 testimonialAuthor">
                                            <div class="img-box col-lg-2 col-lg-offset-2 col-md-offset-2 col-md-2 col-sm-2 col-sm-offset-2 col-xs-3"><img src="images/clients/2.png" alt=""></div>
                                            <div class="col-lg-8 col-sm-8 col-md-8 col-xs-8"><h2 class="authorName"><b>Tyler Luff</b> <br/><span class="testimonial1Position">Store Manager<br/>Telstra Store Toombul</span></h2></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id='pricing' style="background-image:url('images/backgrounds/pricing_bg.png');background-size: cover;padding:40px 0px 40px 0px; clear:both;">
                <div class='container hidden-xs'>
                    <h1 class="text-center white-text headings" style="color:#fff;margin-bottom:20px;">Our Plan</h1><br/>
                    <!-- <h3>
                        <ul class="col-lg-offset-4 col-md-offset-4 col-sm-offset-3 white-text" style="font-weight: 300">
                            <li><span class="fa fa-check"></span> <span class="white-text">No lock-in contracts</span></li>
                            <li><span class="fa fa-check"></span> <span class="white-text">Billed monthly in advance</span></li>
                            <li><span class="fa fa-check"></span> <span class="white-text">Convenience of Direct Debit</span></li>
                        </ul>
                    </h3><br/> -->
                    <div class="col-lg-3 col-lg-offset-2 netproPlan">

                        <div class="text-center trial" style="background-color:#34C6F5 !important;border-top-left-radius: 15px;border-top-right-radius: 15px;padding:10px;border: 3px solid #fff !important;border-right:2px solid #fff;min-height: 50px;height: 60px">
                            <h2 class="white-text" style="font-weight: normal;font-size:30px !important;">NETPRO PLAN</h2>
                        </div>
                        <div class="text-center" style="border-bottom-left-radius: 15px;border-bottom-right-radius: 15px;border-top:0px !important;border-left:3px solid #fff !important;border-right:3px solid #fff !important;border-bottom:3px solid #fff !important;background-color:#08B99F !important;height: 100%;min-height: 80px;padding:10px;border: 3px solid #9E9E9E;border-right:2px solid #9E9E9E;border-bottom:1px solid #9E9E9E;border-top:0">
                            <h3 class="white-text text-center" style="margin-top: 4px;font-size:40px;font-weight: bolder;text-shadow: 2px 2px gray;margin-bottom:5px;">$150 <span style="font-size:24px;">ex/MONTH</span></h3>
                            <h4 class="white-text text-center" style="margin-top: 4px;font-size:32px;font-weight: lighter; font-size: 24px;">PER STORE</h4>
                        </div>

                    </div>
                    <h3 class="col-lg-5">
                        <ul class="white-text" style="font-weight: 300;font-size: 28px;padding-top:6px">
                            <li><span class="fa fa-check"></span> <span class="white-text">One, simple plan</span></li>
                            <li><span class="fa fa-check"></span> <span class="white-text">No lock-in contract</span></li>
                            <li><span class="fa fa-check"></span> <span class="white-text">Billed monthly in advance</span></li>
                            <li><span class="fa fa-check"></span> <span class="white-text">Convenience of Direct Debit</span></li>
                        </ul>
                    </h3><br/>
                    <!--  <div class='row' style="border:0;">
                         <div class="content col-xs-12 col-md-8 col-md-offset-2 col-sm-offset-2 col-sm-8 col-lg-offset-2 col-lg-8 pricingTable" style="padding:0px;border:0;">

                             <div class="text-center trial" style="background-color:#08B99F !important;border-top-left-radius: 15px;border-top-right-radius: 15px;padding:5px;border: 3px solid #9E9E9E !important;border-right:2px solid #9E9E9E;border-bottom:1px solid #9E9E9E;min-height: 60px;height: 60px">
                                 <h2 class="white-text" style="font-weight: normal">Our Plans</h2>
                             </div>
                             <div class="col-lg-6 col-sm-6 col-md-6" style="background-color:#34C6F5 !important;height: 100%;min-height: 80px;padding:10px;padding-top:15px;border: 3px solid #9E9E9E;border-right:2px solid #9E9E9E;border-bottom:1px solid #9E9E9E;border-top:0">
                                 <h3 class="white-text text-center" style="margin-top: 4px;font-size:32px;font-weight: normal">No. of Stores</h3>
                             </div>
                             <div class="col-lg-6 col-sm-6 col-md-6" style="background-color:#34C6F5 !important;height: 100%;min-height: 80px;padding:0px;border: 3px solid #9E9E9E;border-left:1px solid #9E9E9E;border-bottom:1px solid #9E9E9E;border-top:0">
                                 <h3 class="white-text text-center" style="margin-bottom: 0px;padding-top:20px;font-size: 32px;font-weight: normal">Cost per month<p style="margin-bottom:0px;"><i style="font-size: 16px;color:#fff">*GST excl in AUD</p></i></h3>
                             </div>

                             <div class="col-lg-6 col-md-6 col-sm-6" style="background-color:#FAFEFD !important;height: 100%;min-height: 80px;height:80px;padding:25px;border: 3px solid #9E9E9E;border-right:2px solid #9E9E9E;border-bottom:1px solid #9E9E9E">
                                 <h3 class="text-center" style="margin-top: 0px;color:#9E9E9E">1</h3>
                             </div>
                             <div class="col-lg-6 col-md-6 col-sm-6" style="background-color:#FAFEFD !important;height: 100%;min-height: 80px;height:80px;padding:25px;border: 3px solid #9E9E9E;border-left:1px solid #9E9E9E;border-bottom:1px solid #9E9E9E">
                                 <h3 class="text-center" style="margin-top: 0px;color:#9E9E9E">$200<span style="font-weight: normal"> / mth</span></i></h3>
                             </div>

                             <div class="col-lg-6 col-md-6 col-sm-6" style="background-color:#E2EBE8 !important;height: 100%;min-height: 80px;height:80px;padding:20px;;border-bottom-left-radius: 15px;border: 3px solid #9E9E9E;border-right:1px solid #9E9E9E">
                                 <h3 class="text-center" style="margin-top: 0px;color:#9E9E9E">2+</h3>
                             </div>
                             <div class="col-lg-6 col-md-6 col-sm-6" style="background-color:#E2EBE8 !important;height: 100%;min-height: 80px;height:80px;padding:10px;;border-bottom-right-radius: 15px;border: 3px solid #9E9E9E;border-left:2px solid #9E9E9E">
                                 <h3 class="text-center" style="margin-top: 0px;color:#9E9E9E;">$150<span style="font-weight: normal"> / mth</span><br/><span style="font-weight: normal">per Store</span></i></h3>
                             </div>

                         </div>  -->

                    <div class="panel panel-default col-lg-7 col-lg-offset-2 col-sm-offset-2 col-sm-8 col-md-8 col-md-offset-2 col-xs-offset-1 col-xs-10" style="clear:both;margin-top:40px;border-radius:15px;margin-left:21%">
                        <div class="panel-heading headingTrialPanel">
                            <h2 class="text-center uppercase trialText">SEE NETPRO IN ACTION</h2></div>
                        <div class="panel-body">
                            <h3 class="text-center subheadingParagraph">It's quick to set-up, with no lock-in contract &#8212;<br/> so get NetPro for your team today and<br/> see how easy NPS reporting can be.</h3>
                            <a href="https://azenko.com.au/azenko-service-activation/" class="col-lg-10 col-lg-offset-1"><button type="button" class="btn btn-success trialBtn">Click here to activate</button></a>
                        </div>
                    </div>

                </div>
        </div>

        <div class='container visible-xs'>
            <h1 class="text-center white-text headings" style="color:#fff">Pricing</h1><br/>
            <h3>
                <ul class="col-lg-offset-4 col-md-offset-4 col-sm-offset-3 white-text" style="font-weight: 300">
                    <li><span class="fa fa-check"></span> <span class="white-text">No lock-in contracts</span></li>
                    <li><span class="fa fa-check"></span> <span class="white-text">Billed monthly in advance</span></li>
                    <li><span class="fa fa-check"></span> <span class="white-text">Convenience of Direct Debit</span></li>
                </ul>
            </h3><br/>
            <div class='row' style="border:0;">
                <div class="content col-xs-12 col-md-8 col-md-offset-2 col-sm-offset-2 col-sm-8 col-lg-offset-2 col-lg-8 pricingTable" style="padding:0px;border:0;">

                    <div class="text-center trial" style="background-color:#08B99F !important;border-top-left-radius: 15px;border-top-right-radius: 15px;padding:5px;border: 3px solid #9E9E9E !important;border-right:2px solid #9E9E9E;border-bottom:1px solid #9E9E9E;min-height: 60px;height: 60px">
                        <h2 class="white-text" style="font-weight: normal">Our Plans</h2>
                    </div>
                    <div class="col-xs-6" style="background-color:#34C6F5 !important;height: 100%;min-height: 80px;padding:10px;padding-top:15px;border: 3px solid #9E9E9E;border-right:2px solid #9E9E9E;border-bottom:1px solid #9E9E9E;border-top:0">
                        <h3 class="white-text text-center" style="margin-top: 4px;font-size:18px;font-weight: normal">No. of Stores</h3>
                    </div>
                    <div class="col-xs-6" style="background-color:#34C6F5 !important;height: 100%;min-height: 80px;padding:0px;border: 3px solid #9E9E9E;border-left:1px solid #9E9E9E;border-bottom:1px solid #9E9E9E;border-top:0">
                        <h3 class="white-text text-center" style="margin-bottom: 0px;padding-top:20px;font-size: 18px;font-weight: normal">Cost per month<p style="margin-bottom:0px;"><i style="font-size: 14px;color:#fff">*GST excl in AUD</p></i></h3>
                    </div>

                    <div class="col-xs-6" style="background-color:#FAFEFD !important;height: 100%;min-height: 80px;height:80px;padding:25px;border: 3px solid #9E9E9E;border-right:2px solid #9E9E9E;border-bottom:1px solid #9E9E9E">
                        <h3 class="text-center" style="margin-top: 0px;color:#9E9E9E">1</h3>
                    </div>
                    <div class="col-xs-6" style="background-color:#FAFEFD !important;height: 100%;min-height: 80px;height:80px;padding:25px;border: 3px solid #9E9E9E;border-left:1px solid #9E9E9E;border-bottom:1px solid #9E9E9E">
                        <h3 class="text-center" style="margin-top: 0px;color:#9E9E9E">$200<span style="font-weight: normal"> / mth</span></i></h3>
                    </div>

                    <div class="col-xs-6" style="background-color:#E2EBE8 !important;height: 100%;min-height: 80px;height:80px;padding:20px;;border-bottom-left-radius: 15px;border: 3px solid #9E9E9E;border-right:1px solid #9E9E9E">
                        <h3 class="text-center" style="margin-top: 0px;color:#9E9E9E">2+</h3>
                    </div>
                    <div class="col-xs-6" style="background-color:#E2EBE8 !important;height: 100%;min-height: 80px;height:80px;padding:10px;;border-bottom-right-radius: 15px;border: 3px solid #9E9E9E;border-left:2px solid #9E9E9E">
                        <h3 class="text-center" style="margin-top: 0px;color:#9E9E9E;">$150<span style="font-weight: normal"> / mth</span><br/><span style="font-weight: normal">per Store</span></i></h3>
                    </div>

                </div>

                <div class="panel panel-default col-lg-7 col-lg-offset-2 col-sm-offset-2 col-sm-8 col-md-8 col-md-offset-2 col-xs-offset-1 col-xs-10" style="clear:both;margin-top:40px;border-radius:15px;margin-left:21%">
                    <div class="panel-heading headingTrialPanel">
                        <h2 class="text-center uppercase trialText">SEE NETPRO IN ACTION</h2></div>
                    <div class="panel-body">
                        <h3 class="text-center subheadingParagraph">It's quick to set-up, with no lock-in contract &#8212;<br/> so get NetPro for your team today and<br/> see how easy NPS reporting can be.</h3>
                        <a href="https://netpro.azenko.com.au/signup.php" class="col-lg-10 col-lg-offset-1"><button type="button" class="btn btn-success trialBtn">Click here to activate</button></a>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section id='contacts' style="padding:30px 0px 30px 0px; clear:both;background: #fff">
        <div class='container'>
            <h2 class="text-center" style="font-weight: normal;">Contact Us</h2><br/>

            <h3 class="text-center grayText" style="font-weight: normal;color:#787878;clear:both;">
                To arrange a demo of NetPro, fill in your details and we'll be back in touch.
            </h3><div id="contactMsg"  style="margin-top: 5px;clear:both"></div><br/>
            <div class='row'>
                <div class="content col-xs-12 col-md-12 col-lg-12">
                    <div class="panel panel-default col-lg-10 col-lg-offset-1" style="border:0">
                        <!-- <div class="panel-heading text-center">30 Days Trial</div> -->
                        <div class="panel-body contactForm" style="border-radius:15px;background: #EFEFEF; border:2px solid #CDCDCD">
                            <form  class="form-horizontal" role="form" id="form-contact">
                                <input style="font-size:22px;color:#787878" type="text" class="form-control" name="contactName" placeholder="Your Name"><br/>
                                <input style="font-size:22px;color:#787878" type="email" class="form-control" name="contactEmail" placeholder="Your Email"><br/>
                                <textarea style="font-size:22px;color:#787878" class="form-control" rows="5" name="contactContent" placeholder="Your Message" id="yourMessage"></textarea><br/>
                                <input type="submit" value="Submit" id="contactBtn" class="btn btn-success submitBtn no-border col-lg-offset-4">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--//container-->
    </section>

    <a href="#" class="scrollToTop"></a>
    <!-- Footer -->
    <footer class="footer-below text-center" id="footer">
        <!-- <div class="container">
            Copyright Â© <?php echo date('Y') ?> <strong>NetPro </strong>All rights Reserved.
        </div> -->
        <div class="container text-center" style="padding-top:0px;">
            <h4 class="pull-left white-text footer-text">© Azenko Pty Ltd <?php echo date('Y'); ?></h4><h4 class="pull-right white-text footer-text" style="font-weight: normal;"> <a style="color:#fff" href="#" data-toggle="modal" data-target="#privacy">Privacy Policy</a> | Terms & Conditions</h4>
        </div>
    </footer>
</div>
</body>

<div class="modal fade" id="LoginModal" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" role="form" id="form-signin"><!-- /modal-header -->
                <div class="modal-header" style="background-color: #18BC9C">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h2><i class="fa fa-shield right5px white-text"></i><span class="white-text">NetPro Login</span></h4>
                </div>

                <div class="modal-body">
                    <div class="input-group" style="margin-bottom: 5px;">
                        <h4 class="input-group-addon"><i class="fa fa-user"></i> Username</h4>
                        <input type="text" class="form-control" id="log_username_n" name="log_username_n" placeholder="Enter User Name" required value="<?php
                        if (isset($_COOKIE["log_username_n"])) {
                            echo $_COOKIE["log_username_n"];
                        }
                        ?>">
                        <div class="username_avail_result" id="username_avail_result"></div>
                    </div>
                    <div class="input-group">
                        <h4 class="input-group-addon" style="padding-right: 20px;"><i class="fa fa-lock "></i> Password</h4>
                        <input type="password" class="form-control" id="log_password_n" name="log_password_n" placeholder="Enter Password Here" value="<?php
                        if (isset($_COOKIE["log_password_n"])) {
                            echo $_COOKIE["log_password_n"];
                        }
                        ?>" required>
                    </div>

                    <div class="pull-right">
                        <label><input type="checkbox" id="remember_me" name="remember_me" <?php if (isset($_COOKIE["log_username_n"])) { ?> checked <?php } ?>> Remember me</label>

                    </div>
                    <div id="ajaxmsgs"  style="margin-top: 5px;"></div>  <!-- ajaxmsg div -->
                </div> <!-- /modal-body -->
                <div class="modal-footer">
                    <div class="pull-left">

                        <a href="login.php" data-toggle="tooltip" data-placement="top" title="Click here to sign in with your Azenko account"><i class="fa fa-external-link"></i><span> Sign in with your Azenko account </span> </a></bR>

                        <label><a data-toggle="tooltip" data-placement="bottom" title="Click here if you forgot your password" data-toggle = 'modal' data-target = '#ForgotModal' data-dismiss="modal" id='forgotpassword'>Forgot Password?</a></label>
                    </div>
                    <!--<label><li ><a href="login.php"  role="button"><span class="btn btn-info loginBtn"  ">Azenko</span></a></li></label>-->
                    <input type="submit" style="visibility: hidden"/>
                    <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-sm btn-primary btnSubmit"  id="submitBtn" name="submitBtn">Log in</button>
                </div>

            </form>  <!-- /form -->
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal  DeleteUserModal -->

<div class="modal fade" id="ForgotModal" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-contentForgot">
            <form class="form-horizontal" role="form" id="form-forgot"><!-- /modal-header -->
                <div class="modal-header" style="background-color: #18BC9C">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h2><i class="fa fa-shield right5px white-text"></i><span class="white-text">NetPro Forgot Password</span></h4>
                </div>

                <div class="modal-body">
<!--<pre>
Enter the email address linked to your account in the box below.
A link to reset your password will be sent to you by email.
Once you click this link, you will be taken to a page where you can reset your password.</pre>-->
                    <p>Forgot your password? No problem, we will fix it.
                        Just type your email below and we will send you password recovery instruction to your email.</p>
                    <br>
                    <div class="form-group" style="margin-bottom: 5px;">
                        <input type="text" class="form-control" id="emailaddress" name="emailaddress" placeholder="Enter your email here..." required value="<?php
                        if (isset($_COOKIE["log_username_n"])) {
                            echo $_COOKIE["log_username_n"];
                        }
                        ?>">
                        <div class="username_avail_result" id="username_avail_result"></div>
                    </div>
                    <div id="ajaxmsgsforgot"  style="margin-top: 5px;"></div>  <!-- ajaxmsg div -->
                </div> <!-- /modal-body -->
                <div class="modal-footer">
                    <input type="submit" style="visibility: hidden"/>
                    <button type="button"  class="btn btn-sm btn-danger" data-toggle = 'modal' data-target = '#LoginModal' data-dismiss="modal">Back to Login</button>
                    <button type="button" class="btn btn-sm btn-primary" id="submitForgot" name="submitBtn">Submit</button>

                </div>

            </form>  <!-- /form -->
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal  DeleteUserModal -->

<!--Privacy policy modal-->
<!-- Modal -->
<div id="privacy" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-body" style="height: 700px;overflow-y: scroll;">
                <h3>Privacy Policy</h3>
                <ul class="a">
                    <li>Azenko Pty Ltd A.C.N. 626 107 739 (Azenko) provides a wide range of IT products and services for small businesses specialising in a live reporting tools covering sales, stock and financials directly from a customer's point of sale (POS) system, or that display a net promoter score.<br/>
                        By using, applying or signing up for any products or services with Azenko, including through mobile devices, you accept the terms of this Privacy Policy and consent to our collection, use, disclosure and retention of your information as described herein (including to contact you) and for all other purposes permitted under applicable personal information privacy statutes, credit bureau reporting rules, anti-spam legislation and consumer protection laws.</li>
                    <li>
                        <strong>1.  Introduction</span></strong>
                    </li>
                    <li>
                        <span class="listNumber">1.1</span>  We are committed to safeguarding the privacy of our website visitors and service users.
                    </li>
                    <li>
                        <span class="listNumber">1.2</span>  This policy applies where we are acting as a data controller with respect to the personal data of our website visitors and service users; in other words, where we determine the purposes and means of the processing of that personal data.
                    </li>
                    <li>
                        <span class="listNumber">1.3</span>  We use cookies on our website. Insofar as those cookies are not strictly necessary for the provision of our website and services, we will ask you to consent to our use of cookies when you first visit our website.
                    </li>
                    <li>
                        <span class="listNumber">1.4</span>  Our website incorporates privacy controls which affect how we will process your personal data. By using the privacy controls, you can specify whether you would like to receive direct marketing communications and limit the publication of your information. You can access the privacy controls via azenko.com.au.
                    </li>
                    <li>
                        <span class="listNumber">1.5</span>  In this policy, "we", "us" and "our" refer to Azenko Pty Ltd A.C.N. 626 107 739.
                    </li>
                    <li><strong>2.  How we collect and use your personal data</strong></li>
                    <li>
                        <span class="listNumber">2.1</span>  In this Section we have set out:
                        <ul>
                            <li>(a) the general categories of personal data that we may process;</li>
                            <li>(b) in the case of personal data that we did not obtain directly from you, the source and specific categories of that data;</li>
                            <li>(c) the purposes for which we may process personal data; and</li>
                            <li>(d) the legal bases of the processing.</li>
                        </ul>
                    </li>
                    <li><span class="listNumber">2.2</span>  We may process data about your use of our website and services ("usage data"). The usage data may include your IP address, geographical location, browser type and version, operating system, referral source, length of visit, page views and website navigation paths, as well as information about the timing, frequency and pattern of your service use. The source of the usage data is our analytics tracking system. This usage data may be processed for the purposes of analysing the use of the website and services. The legal basis for this processing is our legitimate interests, namely monitoring and improving our website and services.</li>
                    <li><span class="listNumber">2.3</span>  We may process your account data ("account data"). The account data may include your name and email address. The source of the account data is you. The account data may be processed for the purposes of operating our website, providing our services, ensuring the security of our website and services, maintaining back-ups of our databases and communicating with you. The legal basis for this processing is our legitimate interests, namely the proper administration of our website and business and the performance of a contract between you and us and/or taking steps, at your request, to enter into such a contract.</li>
                    <li><span class="listNumber">2.4</span>  We may process your information included in your personal profile on our website ("profile data"). The profile data may include your name, address, telephone number, email address, profile pictures, gender, date of birth, relationship status, interests and hobbies, educational details and employment details. The profile data may be processed for the purposes of enabling and monitoring your use of our website and services. The legal basis for this processing is our legitimate interests, namely the proper administration of our website and business and the performance of a contract between you and us and/or taking steps, at you request, to enter into such a contract.</li>
                    <li><span class="listNumber">2.5</span>  We may process information that you post for publication on our website or through our services ("publication data"). The publication data may be processed for the purposes of enabling such publication and administering our website and services. The legal basis for this processing is our legitimate interests, namely the proper administration of our website and business and the performance of a contract between you and us and/or taking steps, at your request, to enter into such a contract.</li>
                    <li><span class="listNumber">2.6</span>  We may process information contained in any enquiry you submit to us regarding goods and/or services ("enquiry data"). The enquiry data may be processed for the purposes of offering, marketing and selling relevant goods and/or services to you. The legal basis for this processing is consent.</li>
                    <li><span class="listNumber">2.7</span>  We may process information relating to our customer relationships, including customer contact information ("customer relationship data"). The customer relationship data may include your name, your employer, your job title or role, your contact details, and information contained in communications between us and you or your employer. The source of the customer relationship data is you or your employer. The customer relationship data may be processed for the purposes of managing our relationships with customers, communicating with customers, keeping records of those communications and promoting our products and services to customers. The legal basis for this processing is our legitimate interests, namely the proper management of our customer relationships.</li>
                    <li><span class="listNumber">2.8</span>  We may process information relating to transactions, including purchases of goods and services, that you enter into with us and/or through our website ("transaction data"). The transaction data may include your contact details, your card details and the transaction details. The transaction data may be processed for the purpose of supplying the purchased goods and services and keeping proper records of those transactions. The legal basis for this processing is the performance of a contract between you and us and/or taking steps, at your request, to enter into such a contract and our legitimate interests, namely the proper administration of our website and business.</li>
                    <li><span class="listNumber">2.9</span>  We may process information that you provide to us for the purpose of subscribing to our email notifications and/or newsletters ("notification data"). The notification data may be processed for the purposes of sending you the relevant notifications and/or newsletters. The legal basis for this processing is the performance of a contract between you and us and/or taking steps, at your request, to enter into such a contract.</li>
                    <li><span class="listNumber">2.10</span>    We may process information contained in or relating to any communication that you send to us ("correspondence data"). The correspondence data may include the communication content and metadata associated with the communication. Our website will generate the metadata associated with communications made using the website contact forms.  The correspondence data may be processed for the purposes of communicating with you and record-keeping. The legal basis for this processing is our legitimate interests, namely the proper administration of our website and business and communications with users.</li>
                    <li><span class="listNumber">2.11</span>    We may process any of your personal data identified in this policy where necessary for the establishment, exercise or defence of legal claims, whether in court proceedings or in an administrative or out-of-court procedure. The legal basis for this processing is our legitimate interests, namely the protection and assertion of our legal rights, your legal rights and the legal rights of others.</li>
                    <li><span class="listNumber">2.12</span>    We may process any of your personal data identified in this policy where necessary for the purposes of obtaining or maintaining insurance coverage, managing risks, or obtaining professional advice. The legal basis for this processing is our legitimate interests, namely the proper protection of our business against risks.</li>
                    <li><span class="listNumber">2.13</span>    In addition to the specific purposes for which we may process your personal data set out in this Section 3, we may also process any of your personal data where such processing is necessary for compliance with a legal obligation to which we are subject, or in order to protect your vital interests or the vital interests of another natural person.</li>
                    <li>
                        <span class="listNumber">2.14</span>    Please do not supply any other person's personal data to us, unless we prompt you to do so.
                    </li>
                    <li><span class="listNumber">2.15</span>    We collect information you provide when you download our app, apply or sign up for an account and when you provide information about yourself and individuals associated with your business as part of our identity verification process. We may collect, without limitation, the following information about you and individuals associated with your business:
                        <ul>
                            <li>¢   Identification information, such as your name, email address, mailing address, phone number, photograph, birthdate and passport, driver's licence, Medicare or other government-issued identification number];</li>
                            <li>¢   Financial information, including bank account and payment card numbers;</li>
                            <li>¢   Tax information, including withholding allowances and tax filing status; and</li>
                            <li>¢   Other historical, contact and demographic information.</li>
                        </ul>
                    <li>We also collect information you upload to or send through our Services, including:</li>
                    <ul style="margin:0 30px;">
                        <li>¢   Information about products and services you may sell (including inventory, pricing and other data);</li>
                        <li>¢   Information you may provide about you or your business (including appointment, staffing, employee, payroll and third party contact data); and</li>
                        <li>¢   Information you may provide to a seller using our Services (including hours worked and other timecard data).</li>
                    </ul>
                    <li>
                        Some of the information we collect is collected pursuant to applicable laws and regulations, including anti-money laundering laws (e.g., the Anti-Money Laundering and Counter-Terrorism Financing Act).
                    </li>
                    <li>We collect information you provide when you participate in contests or promotions offered by us or our partners, respond to our surveys or otherwise communicate with us.</li>
                    </li>
                    <li><strong>3.  Providing your personal data to others</strong></li>
                    <li><span class="listNumber">3.1</span>  We may disclose your personal data to any member of our group of companies (this means our subsidiaries, our ultimate holding company and all its subsidiaries) insofar as reasonably necessary for the purposes, and on the legal bases, set out in this policy.</li>
                    <li><span class="listNumber">3.2</span>  We may disclose your personal data to our insurers and/or professional advisers insofar as reasonably necessary for the purposes of obtaining or maintaining insurance coverage, managing risks, obtaining professional advice, or the establishment, exercise or defence of legal claims, whether in court proceedings or in an administrative or out-of-court procedure.</li>
                    <li><span class="listNumber">3.3</span>  We may disclose personal data to our suppliers or subcontractors insofar as reasonably necessary for us to provide our products and services to you.</li>
                    <li><span class="listNumber">3.4</span>  Financial transactions relating to our website and services are or may be handled by our payment services providers, Xero and Stripe. We will share transaction data with our payment services providers only to the extent necessary for the purposes of processing your payments, refunding such payments and dealing with complaints and queries relating to such payments and refunds. You can find information about the payment services providers' privacy policies and practices at https://www.xero.com/au/about/security/ and https://stripe.com/au/privacy</li>
                    <li><span class="listNumber">3.5</span>  We may disclose your enquiry data to one or more of those selected third party suppliers of goods and services identified on our website for the purpose of enabling them to contact you so that they can offer, market and sell to you relevant goods and/or services. Each such third party will act as a data controller in relation to the enquiry data that we supply to it; and upon contacting you, each such third party will supply to you a copy of its own privacy policy, which will govern that third party's use of your personal data.</li>
                    <li><span class="listNumber">3.6</span>  In addition to the specific disclosures of personal data set out in this Section 4, we may disclose your personal data where such disclosure is necessary for compliance with a legal obligation to which we are subject, or in order to protect your vital interests or the vital interests of another natural person. We may also disclose your personal data where such disclosure is necessary for the establishment, exercise or defence of legal claims, whether in court proceedings or in an administrative or out-of-court procedure.</li>
                    <li><strong>4.  International transfers of your personal data</strong></li>
                    <li><span class="listNumber">4.1</span>  In this section, we provide information about the circumstances in which your personal data may be transferred to countries outside of Australia.</li>
                    <li><span class="listNumber">4.2</span>  We have offices and facilities in Australia and Philippines. Transfers to each of these countries will be protected by appropriate safeguards, namely the use of standard data protection clauses.</li>
                    <li><span class="listNumber">4.3</span>  The hosting facilities for our website are situated in Australia only. </li>
                    <li><span class="listNumber">4.4</span>  You acknowledge that personal data that you submit for publication through our website or services may be available, via the internet, around the world. We cannot prevent the use (or misuse) of such personal data by others.</li>
                    <li><strong>5.  Retaining and deleting personal data</strong></li>
                    <li><span class="listNumber">5.1</span>  This section sets out our data retention policies and procedure, which are designed to help ensure that we comply with our legal obligations in relation to the retention and deletion of personal data.</li>
                    <li><span class="listNumber">5.2</span>  Personal data that we process for any purpose or purposes shall not be kept for longer than is necessary for that purpose or those purposes.</li>
                    <li><span class="listNumber">5.3</span>  We will retain your personal data for a minimum of 7 years for tax purposes.</li>
                    <li><span class="listNumber">5.4</span>  In some cases it is not possible for us to specify in advance the periods for which your personal data will be retained. </li>
                    <li><span class="listNumber">5.5</span>  Notwithstanding the other provisions of this Section 5, we may retain your personal data where such retention is necessary for compliance with a legal obligation to which we are subject, or in order to protect your vital interests or the vital interests of another natural person.</li>
                    <li><strong>6.  Amendments</strong></li>
                    <li><span class="listNumber">6.1</span>  We may update this policy from time to time by publishing a new version on our website.</li>
                    <li><span class="listNumber">6.2</span>  You should check this page occasionally to ensure you are happy with any changes to this policy.</li>
                    <li><span class="listNumber">6.3</span>  We may notify you of changes to this policy by email or through the private messaging system on our website.</li>
                    <li><strong>7.  Your rights</strong></li>
                    <li><span class="listNumber">7.1</span>  In this section, we have summarised the rights that you have under data protection law. Some of the rights are complex, and not all of the details have been included in our summaries. Accordingly, you should read the relevant laws and guidance from the regulatory authorities for a full explanation of these rights.</li>
                    <li>
                        <span class="listNumber">7.2</span>  Your principal rights under data protection law are:
                        <ul>
                            <li>(a) the right to access;</li>
                            <li>(b) the right to rectification;</li>
                            <li>(c) the right to erasure;</li>
                            <li>(d) the right to restrict processing;</li>
                            <li>(e) the right to object to processing;</li>
                            <li>(f) the right to data portability;</li>
                            <li>(g) the right to complain to a supervisory authority; and</li>
                            <li>(h) the right to withdraw consent.</li>
                        </ul>
                    </li>
                    <li><span class="listNumber">7.3</span>  You have the right to confirmation as to whether or not we process your personal data and, where we do, access to the personal data, together with certain additional information. That additional information includes details of the purposes of the processing, the categories of personal data concerned and the recipients of the personal data. Providing the rights and freedoms of others are not affected, we will supply to you a copy of your personal data. The first copy will be provided free of charge, but additional copies may be subject to a reasonable fee. You can request a copy of your personal data by emailing contactus@azenko.com.au.</li>
                    <li><span class="listNumber">7.4</span>  You have the right to have any inaccurate personal data about you rectified and, taking into account the purposes of the processing, to have any incomplete personal data about you completed.</li>
                    <li><span class="listNumber">7.5</span>  In some circumstances you have the right to the erasure of your personal data without undue delay. Those circumstances include: the personal data are no longer necessary in relation to the purposes for which they were collected or otherwise processed; you withdraw consent to consent-based processing; you object to the processing under certain rules of applicable data protection law; the processing is for direct marketing purposes; and the personal data have been unlawfully processed. However, there are exclusions of the right to erasure. The general exclusions include where processing is necessary: for exercising the right of freedom of expression and information; for compliance with a legal obligation; or for the establishment, exercise or defence of legal claims.</li>
                    <li><span class="listNumber">7.6</span>  In some circumstances you have the right to restrict the processing of your personal data. Those circumstances are: you contest the accuracy of the personal data; processing is unlawful but you oppose erasure; we no longer need the personal data for the purposes of our processing, but you require personal data for the establishment, exercise or defence of legal claims; and you have objected to processing, pending the verification of that objection. Where processing has been restricted on this basis, we may continue to store your personal data. However, we will only otherwise process it: with your consent; for the establishment, exercise or defence of legal claims; for the protection of the rights of another natural or legal person; or for reasons of important public interest.</li>
                    <li><span class="listNumber">7.7</span>  You have the right to object to our processing of your personal data on grounds relating to your particular situation, but only to the extent that the legal basis for the processing is that the processing is necessary for: the performance of a task carried out in the public interest or in the exercise of any official authority vested in us; or the purposes of the legitimate interests pursued by us or by a third party. If you make such an objection, we will cease to process the personal information unless we can demonstrate compelling legitimate grounds for the processing which override your interests, rights and freedoms, or the processing is for the establishment, exercise or defence of legal claims.</li>
                    <li><span class="listNumber">7.8</span>  You have the right to object to our processing of your personal data for direct marketing purposes (including profiling for direct marketing purposes). If you make such an objection, we will cease to process your personal data for this purpose.</li>
                    <li><span class="listNumber">7.9</span>  You have the right to object to our processing of your personal data for scientific or historical research purposes or statistical purposes on grounds relating to your particular situation, unless the processing is necessary for the performance of a task carried out for reasons of public interest.</li>
                    <li>
                        <span class="listNumber">7.10</span>    To the extent that the legal basis for our processing of your personal data is:
                        <ul>
                            <li>(a) consent; or</li>
                            <li>(b) that the processing is necessary for the performance of a contract to which you are party or in order to take steps at your request prior to entering into a contract,
                                and such processing is carried out by automated means, you have the right to receive your personal data from us in a structured, commonly used and machine-readable format. However, this right does not apply where it would adversely affect the rights and freedoms of others.</li>
                        </ul>
                    </li>
                    <li><span class="listNumber">7.11</span> If you consider that our processing of your personal information infringes data protection laws, you have a legal right to lodge a complaint with a supervisory authority responsible for data protection. </li>
                    <li><span class="listNumber">7.12</span> To the extent that the legal basis for our processing of your personal information is consent, you have the right to withdraw that consent at any time. Withdrawal will not affect the lawfulness of processing before the withdrawal.</li>
                    <li><span class="listNumber">7.13</span> You may exercise any of your rights in relation to your personal data by written notice to us at contactus@azenko.com.au, in addition to the other methods specified in this Section 8.</li>
                    <li><strong>8. About cookies</strong></li>
                    <li><span class="listNumber">8.1</span> A cookie is a file containing an identifier (a string of letters and numbers) that is sent by a web server to a web browser and is stored by the browser. The identifier is then sent back to the server each time the browser requests a page from the server.</li>
                    <li><span class="listNumber">8.2</span> Cookies may be either "persistent" cookies or "session" cookies: a persistent cookie will be stored by a web browser and will remain valid until its set expiry date, unless deleted by the user before the expiry date; a session cookie, on the other hand, will expire at the end of the user session, when the web browser is closed.</li>
                    <li><span class="listNumber">8.3</span> Cookies do not typically contain any information that personally identifies a user, but personal information that we store about you may be linked to the information stored in and obtained from cookies.</li>
                    <li><strong>9.  Cookies that we use</strong></li>
                    <li>
                        <span class="listNumber">9.1</span> We use cookies for the following purposes:
                        <ul>
                            <li>(a) authentication - we use cookies to identify you when you visit our website and as you navigate our website;</li>
                            <li>(b) status - we use cookies to help us to determine if you are logged into our website;</li>
                            <li>(c) personalisation - we use cookies to store information about your preferences and to personalise the website for you;</li>
                            <li>(d) security - we use cookies as an element of the security measures used to protect user accounts, including preventing fraudulent use of login credentials, and to protect our website and services generally;</li>
                            <li>(e) advertising - we use cookies to help us to display advertisements that will be relevant to you;</li>
                            <li>(f) analysis - we use cookies to help us to analyse the use and performance of our website and services;</li>
                            <li>(g) cookie consent - we use cookies to store your preferences in relation to the use of cookies more generally.</li>
                        </ul>
                    </li>
                    <li><strong>10. Cookies used by our service providers</strong></li>
                    <li><span class="listNumber">10.1</span> Our service providers use cookies and those cookies may be stored on your computer when you visit our website.</li>
                    <li><span class="listNumber">10.2</span> We use Google Analytics to analyse the use of our website. Google Analytics gathers information about website use by means of cookies. The information gathered relating to our website is used to create reports about the use of our website. Google's privacy policy is available at: https://www.google.com/policies/privacy/.</li>
                    <li><span class="listNumber">10.3</span> We publish Google AdSense interest-based advertisements on our website. These are tailored by Google to reflect your interests. To determine your interests, Google will track your behaviour on our website and on other websites across the web using cookies. We may also publish Google AdSense advertisements on our website. To determine your interests, Google will track your behaviour on our website and on other websites across the web using cookies. This behaviour tracking allows Google to tailor the advertisements that you see on other websites to reflect your interests (but we do not publish interest-based advertisements on our website). You can view, delete or add interest categories associated with your browser by visiting: https://adssettings.google.com. You can also opt out of the AdSense partner network cookie using those settings or using the Network Advertising Initiative's multi-cookie opt-out mechanism at: http://optout.networkadvertising.org. However, these opt-out mechanisms themselves use cookies, and if you clear the cookies from your browser your opt-out will not be maintained. To ensure that an opt-out is maintained in respect of a particular browser, you may wish to consider using the Google browser plug-ins available at: https://support.google.com/ads/answer/7395996.</li>
                    <li><strong>11. Managing cookies</strong></li>
                    <li>
                        <span class="listNumber">11.1</span>    Most browsers allow you to refuse to accept cookies and to delete cookies. The methods for doing so vary from browser to browser, and from version to version. You can however obtain up-to-date information about blocking and deleting cookies via these links:
                        <ul>
                            <li>(a) https://support.google.com/chrome/answer/95647?hl=en (Chrome);</li>
                            <li>(b) https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences (Firefox);</li>
                            <li>(c) http://www.opera.com/help/tutorials/security/cookies/ (Opera);</li>
                            <li>(d) https://support.microsoft.com/en-gb/help/17442/windows-internet-explorer-delete- manage-cookies (Internet Explorer);</li>
                            <li>(e) https://support.apple.com/kb/PH21411 (Safari); and</li>
                            <li>(f) https://privacy.microsoft.com/en-us/windows-10-microsoft-edge-and-privacy (Edge).</li>
                        </ul>
                    </li>
                    <li><span class="listNumber">11.2</span>    Blocking all cookies will have a negative impact upon the usability of many websites.</li>
                    <li><span class="listNumber">11.3</span>    If you block cookies, you will not be able to use all the features on our website or app.</li>
                    <li><strong>12. Our details</strong></li>
                    <li><span class="listNumber">12.1</span>    This website and any associated application is owned and operated by Azenko.</li>
                    <li><span class="listNumber">12.2</span>    We are registered in Australia under registration number 626 107 739, and our registered office is at YK Partners, Level 2, 545 King Street, West Melbourne VIC 3003.</li>
                    <li><span class="listNumber">12.3</span>    Our principal place of business is at Level 1, 157 Given Terrace, Paddington QLD 4064.</li>
                    <li><span class="listNumber">12.4</span>    You can contact us: contactus@azenko.com.au
                        <ul>
                            <li>(a) by post, to the postal address given above;</li>
                            <li>(b)     by telephone, on the contact number published on our website from time to time; or</li>
                            <li>(c) by email, using the email address supplied above and published on our website from time to time.</li>
                        </ul>
                    </li>
                    <li><strong>13. Promotional Communications</strong></li>
                    <li><span class="listNumber">13.1</span>    You may opt out of receiving promotional messages by following the instructions in those messages or by making a request to us using the contact details below. If you decide to opt out, we may still send you non-promotional communications, such as digital receipts you request.</li>
                    <li><strong>14  Data protection officer</strong></li>
                    <li><span class="listNumber">14.1</span>    Our data protection officer's contact details are:  rachel@azenko.com.au.</li>

                </ul>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="SignUpModal" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="form-horizontal" role="form" id="form-signup"><!-- /modal-header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h2><i class="fa fa-user right5px"></i>Signup</h4>
                </div>

                <div class="modal-body">
                    <div id="example-5" class="content">
                        <div class="row">
                            <div class="col-lg-2">
                                <div class="input-group" style="margin-bottom: 5px;">
                                    <input type="text" class="form-control" name="director_name" placeholder="Store Director Name" required>
                                    <div class="username_avail_result" id="username_avail_result"></div>
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <div class="input-group">
                                    <input type="email" class="form-control" name="director_email" placeholder="Email Address" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12"><button type="button" id="btnAdd-5" class="btn btn-primary">Add Store</button></div>
                        </div>
                        <div class="row group">
                            <div class="col-lg-2">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="store_name[]" placeholder="Store Name" required>
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <select class="form-control" name="store_type[]">
                                    <option value="tla">TLA</option>
                                    <option value="tbc">TBC</option>
                                </select>
                            </div>

                            <div class="col-lg-2">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="dealer_code[]" placeholder="Dealer Code" required>
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="line_code[]" placeholder="Fix Line Code" required>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <button type="button" class="btn btn-danger btnRemove">Remove</button>
                            </div>
                        </div>
                    </div>
                    <div id="ajaxmsgs"  style="margin-top: 5px;clear:both"></div>  <!-- ajaxmsg div -->

                    <div id="example-6" class="content">
                        <div class="row">
                            <div class="col-lg-12"><button type="button" id="btnAdd-6" class="btn btn-primary">Add Employee</button></div>
                            <div id="theCount"></div>
                        </div>
                        <div class="row group">
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>P.Number<input class="form-control empPnum" type="text" name="emp[0][p_number]"></label>
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>First Name<input class="form-control empFname" type="text" name="emp[0][f_name]"></label>
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>Last Name<input class="form-control empLname" type="text" name="emp[0][l_name]"></label>
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>Email Address<input class="form-control empEmail" type="email" name="emp[0][email]"></label>
                                </div>
                            </div>

                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>Store Designation<input class="form-control empDesignation" type="text" name="emp[0][designation]"></label>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <label>Netpro Access
                                    <select name="access[]" class="form-control">
                                        <option value="">Please select..</option>
                                        <option value="director">Director</option>
                                        <option value="store_leader">Store Leader</option>
                                        <option value="employee">Employee</option>
                                    </select>
                                </label>
                            </div>

                            <div class="col-md-3">
                                <button type="button" class="btn btn-danger btnRemove">Remove</button>
                            </div>
                        </div>
                    </div>

                </div> <!-- /modal-body -->
                <div class="modal-footer">
                    <input type="submit" style="visibility: hidden"/>
                    <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-sm btn-primary btnSubmit"  id="signUpBtn" name="signUpBtn">Submit</button>
                </div>

            </form>  <!-- /form -->
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div>

<!--Jquery-->
<script  src="includes/js/jquery1.11.1.min.js" type="text/javascript"></script>
<!--Full Slider-->
<script src="includes/js/jquery.cycle.all.js" type="text/javascript"></script>
<script src="includes/js/jquery.maximage.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="includes/js/bootstrap.min.js" type="text/javascript"></script>
</html>


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120818313-3"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-120818313-3');
</script>
<script type="text/javascript" src="includes/js/jquery.multifield.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.carousel').carousel({
            interval: 1000 * 10
        });

        $('#example-5').multifield({
            section: '.group',
            btnAdd: '#btnAdd-5',
            btnRemove: '.btnRemove',
        });

        var counter = 0;

        $('#example-6').multifield({
            section: '.group',
            btnAdd: '#btnAdd-6',
            btnRemove: '.btnRemove',
        });

        $("#btnAdd-6").click(function () {

            setTimeout(function () {

                counter++;
                $('.empPnum').last().attr('name', 'emp[' + counter + '][p_number]')
                $('.empFname').last().attr('name', 'emp[' + counter + '][f_name]')
                $('.empLname').last().attr('name', 'emp[' + counter + '][l_name]')
                $('.empEmail').last().attr('name', 'emp[' + counter + '][email]')
                $('.empDesignation').last().attr('name', 'emp[' + counter + '][designation]')
            }, 100)


        });

        // Select all links with hashes
        $('a[href*="#"]')
// Remove links that don't actually link to anything
                .not('[href="#"]')
                .not('[href="#0"]')
                .click(function (event) {
// On-page links
                    if (
                            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                            &&
                            location.hostname == this.hostname
                            ) {
                        // Figure out element to scroll to
                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        // Does a scroll target exist?
                        if (target.length) {
                            // Only prevent default if animation is actually gonna happen
                            event.preventDefault();
                            $('html, body').animate({
                                scrollTop: target.offset().top
                            }, 1000, function () {
                                // Callback after animation
                                // Must change focus!
                                var $target = $(target);

                                if ($target.is(":focus")) { // Checking if the target was focused
                                    return false;
                                } else {
                                    $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable

                                }
                                ;
                            });
                        }
                    }
                });

        //  Initiate Max image slider
        $('#maximage').maximage();

        //Status Calculations

        //Authenticate User
        $('#submitBtn').on('click', function () {
            if ($("#form-signin")[0].checkValidity()) {
                var url = 'functions/checkUser.php';
                var mydata = $("#form-signin").serialize();
                $.ajax({
                    url: url,
                    type: 'get',
                    data: mydata,
                    dataType: 'json',
                    success: function (result) {
                        document.getElementById('ajaxmsgs').style.display = 'block';
                        if (result.type === 'Success') {
                            var link = $('<a href=' + result.URL + ' rel="lightbox" id="link">Click</a>');
                            $("body").append(link);
                            $("#link")[0].click();
                        } else {
                            $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong><br>" + result.message + "</div>");
                        }
                    },
                    error: function () {
                        document.getElementById('ajaxmsgs').style.display = 'block';
                        $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
                    }
                });
            } else {
                $("#form-signin").find(':submit').click();
            }
            event.preventDefault();
        });

        $('#signUpBtn').on('click', function () {
            if ($("#form-signup")[0].checkValidity()) {
                //var url = 'functions/signupUser.php';
                var url = 'https://team360.app:4443/api/netproSignUp';
                var contactData = $("#form-signup").serialize();
                $.ajax({
                    url: url,
                    type: 'get',
                    data: contactData,
                    dataType: 'jsonp',
                    crossDomain: true,
                    success: function (result) {
                        document.getElementById('ajaxmsgs').style.display = 'block';
                        if (result.type === 'Success') {
                            var link = $('<a href=' + result.URL + ' rel="lightbox" id="link">Click</a>');
                            $("body").append(link);
                            $("#link")[0].click();
                        } else {
                            $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong><br>" + result.message + "</div>");
                        }
                    },
                    error: function () {
                        document.getElementById('ajaxmsgs').style.display = 'block';
                        $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
                    }
                });
            } else {
                $("#form-signin").find(':submit').click();
            }
            event.preventDefault();
        });

        //Check to see if the window is top if not then display button
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('.scrollToTop').fadeIn();
            } else {
                $('.scrollToTop').fadeOut();
            }
        });

//Click event to scroll to top
        $('.scrollToTop').click(function () {
            $('html, body').animate({scrollTop: 0}, 800);
            return false;
        });

        $('#contactBtn').on('click', function () {
            $('#contactBtn').prop('disabled', true);
            $('#contactBtn').val('Sending');
            if ($("#form-contact")[0].checkValidity()) {

                var url = 'https://team360.app:4443/api/netproContactUs';
                var mydata = $("#form-contact").serialize();
                $.ajax({
                    url: url,
                    type: 'get',
                    data: mydata,
                    dataType: 'jsonp',
                    crossDomain: true,
                    success: function (result) {
                        document.getElementById('ajaxmsgs').style.display = 'block';
                        if (result.type === 'Success') {
                            // var link = $('<a href=' + result.URL + ' rel="lightbox" id="link">Click</a>');
                            // $("body").append(link);
                            // $("#link")[0].click();
                            $(':input[type="submit"]').prop('disabled', false);
                            $('#contactBtn').val('Submit');
                            $('#contactMsg').html("<div class='alert alert-success alert-dismissible col-lg-6 col-lg-offset-3 text-center' role='alert' style='margin-bottom:15px;'> <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true' style='margin-right: 20px;'>&times;</span></button>Thank you for contacting us.<br/>Your message has been successfully sent. We will contact you soon!</div>");
                        } else {
                            $(':input[type="submit"]').prop('disabled', false);
                            $('#contactBtn').val('Submit');
                            $('#contactMsg').html("<div class='alert alert-success alert-dismissible col-lg-6 col-lg-offset-3 text-center' role='alert' style='margin-bottom:15px;'> <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true' style='margin-right: 20px;'>&times;</span></button>Thank you for contacting us.<br/>Your message has been successfully sent. We will contact you soon!</div>");
                        }
                    },
                    error: function () {
                        $(':input[type="submit"]').prop('disabled', false);
                        $('#contactBtn').val('Submit');
                        $('#contactMsg').html("<div class='alert alert-success alert-dismissible col-lg-6 col-lg-offset-3 text-center' role='alert' style='margin-bottom:15px;'> <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true' style='margin-right: 20px;'>&times;</span></button>Thank you for contacting us.<br/>Your message has been successfully sent. We will contact you soon!</div>");
                    }
                });
            } else {
                $("#form-signin").find(':submit').click();
            }
            event.preventDefault();
        });

        //If enter is press submit
        $("input").keypress(function (event) {
            if (event.which === 13) {
                event.preventDefault();
                $("#submitBtn").click();
            }
        });

        $('#submitForgot').on('click', function () {
            if ($("#form-forgot")[0].checkValidity()) {
                var url = 'functions/forgotpass.inc.php';
                var mydata = $("#form-forgot").serialize();
                $.ajax({
                    url: url,
                    type: 'get',
                    data: mydata,
                    dataType: 'json',
                    success: function (result) {
                        document.getElementById('ajaxmsgsforgot').style.display = 'block';
                        if (result.type === 'Success') {
                            var TokenUrl = 'https://team360.app:4443/api/netProForgetPassword?';
                            var tokenData = mydata + "&token=" + result.token;
                            $.ajax({
                                url: TokenUrl + tokenData,
                                type: 'get',
                                // data: tokenData,
                                dataType: 'json',
                                success: function (resultEmail) {
                                    // console.log(resultEmail);
                                    $('#ajaxmsgsforgot').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a>Forgot password link has been sent to your email address. </div>");
                                    $('#submitForgot').hide();
                                }
                            });

                        } else {
                            $('#ajaxmsgsforgot').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a>" + result.message + "</div>");
                        }
                    },
                    error: function (result) {
                        console.log(result);
                        document.getElementById('ajaxmsgsforgot').style.display = 'block';
                        $('#ajaxmsgsforgot').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong><br>" + result.message + "</div>");
                    }
                });
            } else {
                $("#form-forgot").find(':submit').click();
            }
            event.preventDefault();
        });


        $("#ForgotModal").on("hidden.bs.modal", function () {
            $('#form-forgot')[0].reset();
            $("#ajaxmsgsforgot").html("");
        });

        $('[data-toggle="tooltip"]').tooltip();

        $('#forgotpassword').click(function () {
            $('#ForgotModal').modal('show');
        });
    });

</script>










