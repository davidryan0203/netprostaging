<?php
session_start();
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
$loginID = $_SESSION['LoginID'];
$_SESSION['ReadMessageBoard'] = true;
?>

<!-- Modal transparent -->
<div class="modal modal-transparent fade" id="modal-transparent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"> <i class="fa fa-paper-plane-o"></i> NetPro Updates...</h4>
            </div>

            <div class="modal-body">
                <pre style="white-space:pre-line">
                        Attention: All NetPRO users!

                        With our compliance to Telstra Customer Privacy we are removing Customer Last Name in NetPro.
                        This would be replaced with Customer Billing Account ID instead.

                        Regards
                        The NetPRO Team!

                </pre>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-info" id="btnCancel">Close</button>
            </div>
        </div>
    </div>
</div>




<script type="text/javascript">
    $(document).ready(function() {
        // $('#pswd_info').hide();
        $('#modal-transparent').modal({
            backdrop: 'static',
            keyboard: false
        }, 'show');

        // .modal-backdrop classes

        $(".modal-transparent").on('show.bs.modal', function() {
            setTimeout(function() {
                $(".modal-backdrop").addClass("modal-backdrop-transparent");
            }, 0);
        });
        $(".modal-transparent").on('hidden.bs.modal', function() {
            $(".modal-backdrop").addClass("modal-backdrop-transparent");
        });



        $("#btnCancel").click(function(event) {
            var UserProfile = <?php echo $_SESSION['UserRoleID']; ?>;
            var urlTogo = '';

            if (UserProfile == 4) {
                urlTogo = 'public_html.php?i=1';
            } else if (UserProfile == 6) {
                urlTogo = 'public_html.php?i=6';
            } else {
                urlTogo = 'public_html.php?i=2&o=1';
            }
            window.location.href = urlTogo;
        });

    });
</script>