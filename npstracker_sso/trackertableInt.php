<?php
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/npstracker/function/NpsTrackerFunction.php');
$query = $_GET;
$data = getEmployeeSummaryAll($query);
?>

<style type="text/css">
    .table>thead>tr>th, .table>thead>tr>td{
        padding: 5px;
        font-size: 13px;
    }
    .table>tbody>tr>th, .table>tbody>tr>td{
        padding: 3px;
        line-height: 1.2;
        vertical-align: top;
        border-top: 1px solid #ddd;
        font-size: 12px;
    }
</style>

<?php
if (!is_null($data)) {
    if ($data['Type'] == 'Store') {
        ?>
        <table class="table table-bordered table-responsive">
            <thead>
                <tr>
                    <th colspan="5">Store Surveys, NPS, and Total Employees</th>
                </tr>
                <tr>
                    <th>Month</th>
                    <th>Store</th>
                    <th>Surveys</th>
                    <th>NPS</th>
                    <th>Employees</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($data['Data'] as $key => $value) {
                    //  print_r($value);
                    ?>
                    <tr>
                        <td><?php echo $key; ?></td>
                        <td>
                            <?php
                            foreach ($value as $store) {
                                echo $store['StoreName'] . "<br>";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            foreach ($value as $survey) {
                                echo $survey['StoreTotalSurvey'] . "<br>";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            foreach ($value as $nps) {
                                if ($query['reportType'] == 'Interaction') {
                                    $getNps = 'StoreNPSInt';
                                } elseif ($query['reportType'] == 'Episode') {
                                    $getNps = 'StoreNPSEpi';
                                } elseif ($query['reportType'] == 'Mobile') {
                                    $getNps = 'StoreNPSMob';
                                } elseif ($query['reportType'] == 'Fixed') {
                                    $getNps = 'StoreNPSFix';
                                }
                                echo number_format($nps[$getNps], 2) . "<br>";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            foreach ($value as $emp) {
                                echo $emp['EmpCount'] . "<br>";
                            }
                            ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>

        <?php
    } elseif ($data['Type'] == 'Emp') {
        // echo "<pre>";
        // print_r($data['Data']);
        ?>
        <table class="table table-bordered table-responsive">
            <thead>
                <tr>
                    <th colspan="5">Total Survey and Impact</th>
                </tr>
                <tr>
                    <th>Month</th>
                    <th>Emp Name</th>
                    <th>Survey</th>
                    <th>Employee </th>
                    <th>Impact</th>

                </tr>
            </thead>
            <tbody>
                <?php foreach ($data['Data'] as $key => $value) { ?>
                    <tr>
                        <td><?php echo $key; ?></td>
                        <td>
                            <?php
                            foreach ($value as $store) {
                                echo $store['EmpName'] . "<br>";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            foreach ($value as $survey) {
                                echo $survey['StoreTotalSurvey'] . "<br>";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            foreach ($value as $emp) {
                                echo $emp['EmpTotalSurvey'] . "<br>";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            foreach ($value as $impact) {
                                echo number_format($impact['Impact'], 2) . "<br>";
                            }
                            ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php } else { ?>
        <table class="table table-bordered table-responsive">
            <thead>
                <tr>
                    <th colspan="5">Total Survey and Impact</th>
                </tr>
                <tr>
                    <th>Month</th>
                    <th>Store</th>
                    <th>Employee</th>
                    <th>Store</th>
                    <th>Impact</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="5"> No data to display</td>
                </tr>
            </tbody>
        </table>
    <?php } ?>
<?php } ?>





