<?php
session_start();
$storeType = $_GET['storeType'];
$role = $_SESSION['UserRoleID'];
?>

<!--main tls dashboard reports-->
<section class='reports'>
    <input type="hidden" id="storeType" name="storeType" value="<?php echo $storeType; ?>" />

<!--    <div class="main-content featuredChamps no-bg">
        <div class="btn-group group1 pull-right" role="group" id="storechamp-group" aria-label="...">
            <button type="button" id="StoreChampionEpisode" data-text="Episode" class="btn btn-xs btn-primary ">Episode</button>
            <button type="button" id="StoreChampionMobile" data-text="Mobile" class="btn btn-xs btn-warning">Mobile</button>
            <button type="button" id="StoreChampionFixed" data-text="Fixed" class="btn btn-xs btn-info">Fixed</button>
        </div>

        <h4 class="sub-header"><i class="fa fa-user fa-fw"></i> Featured Store <Fixed>Episode</champs> Champions</h4>

        <div class="loader" id="StoreChampTlsloader"></div>

        <div id="featuredChampsInteraction">
            <div  id="StoreChampTlsContainer"> </div>
        </div>

    </div>-->

    <div class="main-content  npsTarget no-bg">
        <div class="btn-group group2 pull-right" role="group" id="storerank-group" aria-label="...">
            <!--<button type="button" id="StoreRankingEpisode"  data-text="Episode" class="btn btn-xs btn-primary">Episode</button>-->
            <button type="button" id="StoreRankingMobile" data-text="Mobile" class="btn btn-xs btn-warning">Mobile</button>
            <button type="button" id="StoreRankingFixed" data-text="Fixed" class="btn btn-xs btn-info">Fixed</button>
        </div>

        <h4 class="sub-header"><i class="fa fa-dashboard fa-fw"></i> Store  <rank>Fixed</rank> Ranking </h4>

        <div class="loader" id="StoreRankingGoalsLoader"></div>

        <div id="TargetFade1">
            <div id="StoreRankingGoalsContainer" ></div>
        </div>
    </div>

    <div class="main-content">
        <h4 class="sub-header"><i class="fa fa-building fa-fw"></i> Store Ranking Summary</h4>
        <div class="loader" id="StoreRankingsLoader"></div>

        <div id="storeRanking">
            <div id="StoreRankings"></div>
        </div>
    </div>

    <!--    <div class="main-content">
            <h4 class="sub-header"><i class="fa fa-users fa-fw"></i> Interaction Champions Summary</h4>
            <div class="loader" id="InteractionChampLoader"></div>
            <div id="InteractionChampionRanking"> </div>
        </div>-->

<!--    <div class="main-content">
        <h4 class="sub-header"><i class="fa fa-users fa-fw"></i> Mobility (On the go) Champions Summary</h4>
        <div class="loader" id="MobileChampLoader"></div>
        <div id="MobileChampionRanking"> </div>
    </div>

    <div class="main-content">
        <h4 class="sub-header"><i class="fa fa-users fa-fw"></i> Fixed (Home) Champions Summary</h4>
        <div class="loader" id="FixedChampLoader"></div>
        <div id="FixedChampionRanking"> </div>
    </div>-->

    <?php if ($role == '4') { ?>
        <!--        <div class="main-content">
                    <h4 class="sub-header"><i class="fa fa-users fa-fw"></i>All Staff Interaction Ranking</h4>

                    <div class="loader" id="AllEmployeeInteractionLoader"></div>

                    <div id="AllInteraction">
                        <div id="AllEmployeeInteraction"> </div>
                    </div>
                </div>-->

<!--        <div class="main-content">
            <h4 class="sub-header"><i class="fa fa-users fa-fw"></i>All Staff Mobility (On the go) Ranking</h4>

            <div class="loader" id="AllEmployeeMobileLoader"></div>

            <div id="AllMobile">
                <div id="AllEmployeeMobile"> </div>
            </div>
        </div>

        <div class="main-content">
            <h4 class="sub-header"><i class="fa fa-users fa-fw"></i>All Staff Fixed (Home) Ranking</h4>

            <div class="loader" id="AllEmployeeFixedLoader"></div>

            <div id="AllFixed">
                <div id="AllEmployeeFixed"> </div>
            </div>-->
        </div>
    <?php } ?>
</section>

<script src="../includes/js/custom.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function() {

//        $('#StoreChampTlsContainer', function() {
//            var urlToLoad = "dashboard/summary/FeaturedChampsFixed.php";
//            var divToHide = "#StoreChampTlsloader";
//            var divToShow = "#StoreChampTlsContainer";
//            reload(urlToLoad, divToHide, divToShow);
//        }
//        );
//
//        $('#storechamp-group button').click(function() {
//            var textval = $(this).data("text");
//            var divToHide = "#StoreChampTlsloader";
//            var divToShow = "#StoreChampTlsContainer";
//            var setUrl = '';
//
//            switch (textval) {
//                case 'Episode':
//                    setUrl = "dashboard/summary/FeaturedChampsEpisode.php";
//                    break;
//                case 'Mobile':
//                    setUrl = "dashboard/summary/FeaturedChampsMobile.php";
//                    break;
//                case 'Fixed':
//                    setUrl = "dashboard/summary/FeaturedChampsFixed.php";
//                    break;
//            }
//            $("champs").html(textval);
//            reload(setUrl, divToHide, divToShow);
//        });


        $('#StoreRankingGoalsContainer', function() {
            var urlToLoad = "dashboard/summary/storeFixedGoals.php";
            var divToHide = "#StoreRankingGoalsLoader";
            var divToShow = "#StoreRankingGoalsContainer";
            reload(urlToLoad, divToHide, divToShow);
        }
        );


        $('#storerank-group button').click(function() {
            var textval = $(this).data("text");
            var divToHide = "#StoreRankingGoalsLoader";
            var divToShow = "#StoreRankingGoalsContainer";
            var setUrl = '';

            switch (textval) {
                case 'Episode':
                    setUrl = "dashboard/summary/storeEpisodeGoals.php";
                    break;
                case 'Mobile':
                    setUrl = "dashboard/summary/storeMobileGoals.php";
                    break;
                case 'Fixed':
                    setUrl = "dashboard/summary/storeFixedGoals.php";
                    break;
            }
            $("rank").html(textval);
            reload(setUrl, divToHide, divToShow);
        });


        $('#StoreRankings', function() {
            var urlToLoad = "dashboard/summary/storeRankings.php";
            var divToHide = "#StoreRankingsLoader";
            var divToShow = "#StoreRankings";
            reload(urlToLoad, divToHide, divToShow);
        }
        );

//        $('#InteractionChampionRanking', function() {
//            var urlToLoad = "dashboard/summary/ChampionsEpisodeRank.php";
//            var divToHide = "#InteractionChampLoader";
//            var divToShow = "#InteractionChampionRanking";
//            reload(urlToLoad, divToHide, divToShow);
//        }
//        );
//        $('#MobileChampionRanking', function() {
//            var urlToLoad = "dashboard/summary/ChampionsMobileRank.php";
//            var divToHide = "#MobileChampLoader";
//            var divToShow = "#MobileChampionRanking";
//            reload(urlToLoad, divToHide, divToShow);
//        }
//        );
//        $('#FixedChampionRanking', function() {
//            var urlToLoad = "dashboard/summary/ChampionsFixedRank.php";
//            var divToHide = "#FixedChampLoader";
//            var divToShow = "#FixedChampionRanking";
//            reload(urlToLoad, divToHide, divToShow);
//        }
//        );


            //            $('#AllEmployeeInteraction', function() {
            //                var urlToLoad = "nps/summary/employeeNps.php";
            //                var divToHide = "#AllEmployeeInteractionLoader";
            //                var divToShow = "#AllEmployeeInteraction";
            //                reload(urlToLoad, divToHide, divToShow);
            //            }
            //            );

//            $('#AllEmployeeMobile', function() {
//                var urlToLoad = "nps/summary/employeeNpsMobile.php";
//                var divToHide = "#AllEmployeeMobileLoader";
//                var divToShow = "#AllEmployeeMobile";
//                reload(urlToLoad, divToHide, divToShow);
//            }
//            );
//
//            $('#AllEmployeeFixed', function() {
//                var urlToLoad = "nps/summary/employeeNpsFixed.php";
//                var divToHide = "#AllEmployeeFixedLoader";
//                var divToShow = "#AllEmployeeFixed";
//                reload(urlToLoad, divToHide, divToShow);
//            }
//            );


    });


    function reload(paramurl, loader, content) {
        var urlExtend = "";
        if ($('#StoreType').prop('checked')) {
            urlExtend = '?storeType=2';
        } else {
            urlExtend = '?storeType=1';
        }
        var urldata = paramurl + urlExtend;
        $(content).html('');
        $(loader).show();
        $.ajax({
            url: urldata,
            cache: false,
            success: function(html) {
                $(content).html('');
                $(content).append(html);
            },
            complete: function() {
                $(loader).hide();
            }
        });
    }

</script>