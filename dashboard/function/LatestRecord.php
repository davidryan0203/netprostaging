<?php

function getLatestRercord() {
    date_default_timezone_set('Australia/Melbourne');
    global $connection;
    session_start();
    $role = $_SESSION['UserRoleID'];
    switch ($role) {
        case '1':
            $sqlOwner = " ";
            break;
        case '2':
        case '3':
            $sqlOwner = " AND  b.f_StoreOwnerUserID = {$_SESSION['UserProfileID']} ";
            break;
        case '4':
        case '5':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $data = $row['f_StoreOwnerUserID'];
            }
            $sqlOwner = " AND  b.f_StoreOwnerUserID = {$data} ";
            // $sqlShared = " AND c.f_StoreOwnerUserID = '{$data}' ";
            break;
    }

    $sqlLatestRecord = "SELECT
    (SELECT
            MAX(f_SurveyInsertedDate)
        FROM
            t_surveysd a
                LEFT JOIN
            t_storelist b ON a.f_StoreID = b.f_StoreID
        WHERE
            1 = 1 {$sqlOwner}) AS most_recent_insert,
    (SELECT
            MAX(f_DateTimeResponseMelbourne)
        FROM
            t_surveysd a
                LEFT JOIN
            t_storelist b ON a.f_StoreID = b.f_StoreID
        WHERE
            1 = 1 {$sqlOwner}) AS most_recent_record ";
    $result = mysql_query($sqlLatestRecord, $connection);

    $row = mysql_fetch_assoc($result);

    if ($row['most_recent_record'] != '') {
        $recent_record = new DateTime($row['most_recent_record']);
        $recent_insert = new DateTime($row['most_recent_insert']);
        $date['latest_record'] = 'Latest Record: ' . $recent_record->format('d-M-y h:i A') . '</br> Last Inserted: ' . $recent_insert->format('d-M-y h:i A');
    } else {
        $date['latest_record'] = 'Latest Record: No existing data yet.';
    }

    $cur_month = new DateTime();
    $cur_month->setTimezone(new DateTimeZone('Australia/Melbourne')); // +04
    $date['cur_month'] = $cur_month->format('F');

    return $date;
}
