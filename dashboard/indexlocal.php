<?php
include('nps/function/NPSFunction.php');
require($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');

$SelectStoreTypes = SelectStoreTypes();

$storeTypes = checkStoreTypesLocal();

$date = new DateTime();
$date->setTimezone(new DateTimeZone('Australia/Melbourne')); // +04
?>

<!--Local dashboard container-->
<section class='dashboard'>
    <div class="row">
        <div class="col-lg-12">

            <h4 class="page-header">
                <i class="fa fa-file-word-o fa-fw"></i>Competitive Local Dashboard
            </h4>

            <div class='filters'>
                <?php if ($SelectStoreTypes['StoreTypes'] == 'Both Stores') { ?>
                    <input type="checkbox" id="StoreType" name="StoreType" class="StoreType btn-sm" data-toggle="toggle" data-style="ios"  data-onstyle="info"  
                           data-offstyle="warning" data-on="TBTC" data-off="TLS" />   
                       <?php } else { ?>
                    <input type="checkbox" style="display:none"  id="StoreType" name="StoreType" class="StoreType" />  
                <?php } ?>
            </div>
        </div>
    </div>


    <div  id="page-contents" class="page-contents" ></div>
</section>


<script type="text/javascript">
    $(document).ready(function() {


<?php if ($SelectStoreTypes['StoreTypes'] == 'Only TBC') { ?>
            $("#StoreType").trigger('click');
<?php } ?>


        var storeType = "";
        var url = "";

        function setLoad() {
            if ($('#StoreType').prop('checked')) {
                storeType = 'tbc';
                url = 'dashboard/dashboardextendTBCLocal.php?storeType=' + storeType;
            } else {
                storeType = 'tls';
                url = 'dashboard/dashboardextendLocal.php?storeType=' + storeType;
            }
            //console.log(url);
            return url;
        }

        var url = setLoad();
        $('#page-contents').load(url).fadeIn("slow");

        $('#StoreType').change(function() {
            var url = setLoad();
            $('#page-contents').html('').load(url).fadeIn("slow");
        });
    });
</script>








