<?php
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/nps/function/NPSFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/mynps/function/InteractionMessage.php');
$storeType = $_GET['storeType'];
$StoreEpisode = array();
$storeNpsData = getStoreNps(NULL, NULL, $empID, $to, $from, NULl, $storeType);

function replacer(& $item, $key) {
    if ($item === null) {
        $item = 0;
    }
}

foreach ($storeNpsData['Episode'] as $storeData) {
    array_walk_recursive($storeData, 'replacer');
    $StoreEpisode[] = array(
        'StoreName' => $storeData['StoreName'],
        'StoreTier' => $storeData['EpisodeTierLevel'],
        'Advocate' => $storeData['EpisodeTotalAdvocate'],
        'Passive' => $storeData['EpisodeTotalPassive'],
        'Detractor' => $storeData['EpisodeTotalDetractors'],
        'TotalSurvey' => $storeData['EpisodeTotalSurvey'],
        'NpsScore' => number_format($storeData['EpisodeNpsScore'], npsDecimal())
    );
}

$episodeChunk = array_chunk($StoreEpisode, 3, false);
//echo "<pre>"; var_dump($StoreEpisode);

$rank = 1;
$colorID = 0;
$spanID = 0;
?>

<script type="text/javascript">

    function generateRankStoreEpisode(TotalySurvey, Advocate, Detractor, SpanID) {

        var NewTotalSurvey2 = TotalySurvey;
        var newInterAdvoTotal2 = Advocate;
        var InteractionTotalDetractorTotal2 = Detractor;

        var counter2 = 0;
        var newDetPercentage2 = 0;
        var newAdvoPercentage2 = 0;
        var newIntercationNPS2 = 0;
        var roundedstoreNPS = 0;
        while (newIntercationNPS2 < 50) {
            //console.log(NewTotalSurvey2 + " -" + newInterAdvoTotal2 + "  +" + counter2 + " " + newIntercationNPS2);
            newAdvoPercentage2 = (newInterAdvoTotal2 / NewTotalSurvey2) * 100;
            newDetPercentage2 = (InteractionTotalDetractorTotal2 / NewTotalSurvey2) * 100;
            newIntercationNPS2 = newAdvoPercentage2 - newDetPercentage2;
            //npscore = InteractionTotalDetractorPercentage +  newAdvoPercentage + InteractionPassivePercentage;
            counter2 += 1;
            roundedstoreNPS = Math.round(newIntercationNPS2);
            if (roundedstoreNPS <= 30) {
                $("#EpisodeToGetLvl3" + SpanID).empty();
                $("#EpisodeToGetLvl3" + SpanID).append(counter2);
                //        console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
            } else if (roundedstoreNPS <= 40) {
                $("#EpisodeToGetLvl2" + SpanID).empty();
                $("#EpisodeToGetLvl2" + SpanID).append(counter2);
                //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 2");
            } else if (roundedstoreNPS < 50) {
                $("#EpisodeToGetLvl1" + SpanID).empty();
                $("#EpisodeToGetLvl1" + SpanID).append(counter2);
                //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 1");
            }
            newInterAdvoTotal2 += 1;
            NewTotalSurvey2 += 1;
        }
    }


</script>


<?php if (!is_null($StoreEpisode[0])) { ?>
    <table id="StoreEpisodeTable" border="0" width="100%">
        <thead id="tableHead" style="display: none;">
            <tr>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
    <?php foreach ($episodeChunk as $value) { ?>
                <tr>
                <?php if (!is_null($value[0])) { ?>
                        <td class="col-md-4">
                            <div class="ribbon-wrapper">
                                <div class="ribbon-wrapper-gold">
                                    <div class="ribbon-gold">
                                        <i class="fa fa-trophy"></i> Rank <?php echo $rank ?>
                                    </div>
                                </div>
                                <center>
                                    <div class="ribbon-heading">
                                        <div class="nps-badge-orange">
                                            <h4><?php echo $value[0]['NpsScore']; ?></h4>
                                            <small><?php echo $value[0]['StoreTier']; ?></small>
                                        </div>
                                        <h2 style="margin-top: 0;"><i class="fa fa-shopping-cart"></i> <?php echo $value[0]['StoreName']; ?> </h2>
            <?php if ($value[0]['NpsScore'] > 1 && $value[0]['NpsScore'] < 50) { ?>
                                            <script type="text/javascript">
                                                var TotalySurveyParam = <?php echo $value[0]['TotalSurvey']; ?>;
                                                var AdvocateParam = <?php echo $value[0]['Advocate']; ?>;
                                                var DetractorParam = <?php echo $value[0]['Detractor']; ?>;
                                                var SpanIDParam = <?php echo $spanID; ?>;
                                                generateRankStoreEpisode(TotalySurveyParam, AdvocateParam, DetractorParam, SpanIDParam);
                                            </script>
                <?php
                $idSharetLvl1 = "EpisodeToGetLvl1{$spanID}";
                $idSharetLvl2 = "EpisodeToGetLvl2{$spanID}";
                $idSharetLvl3 = "EpisodeToGetLvl3{$spanID}";
                ?>
                                            <table class="table table-bordered table-hover table-condensed"  style="margin-bottom: 10px;" cellspacing="0" >
                                                <thead>
                                                    <tr>
                                                        <th>T1</th>
                                                        <th>T2</th>
                                                        <th>T3</th>
                                                    </tr>
                                                </thead>
                                                <tbody style="text-align:center;">
                                                    <tr>
                                                        <td><div id="<?php echo $idSharetLvl1; ?>"><i class="fa fa-check"></i></div></td>
                                                        <td><div id="<?php echo $idSharetLvl2; ?>"><i class="fa fa-check"></i></div></td>
                                                        <td><div id="<?php echo $idSharetLvl3; ?>"><i class="fa fa-check"></i></div></td>
                                                    </tr>
                                                </tbody>
                                            </table>
            <?php } elseif ($value[0]['NpsScore'] == 0) { ?>
                                            <?php $getClassStore = getEpisodeClassStore($value[0]['NpsScore']); ?>
                                            <div <?php echo $getClassStore['class3']; ?> id="Dashboard-Alert">
                                                <h5><i class="fa fa-check-square"></i> Store needs at least 1 Advocate</h5>
                                            </div>
            <?php } else { ?>
                                            <?php $getClassStore = getEpisodeClassStore($value[0]['NpsScore']); ?>
                                            <div <?php echo $getClassStore['class3']; ?> id="Dashboard-Alert">
                                                <h5><i class="fa fa-check-square"></i> Goal Accomplished!</h5>
                                            </div>
                <?php
            } $spanID++;
            $rank++;
            ?>
                                    </div>

                                    <h4 class="survey">
                                        <i class="fa fa-exclamation-circle"></i> Survey: <?php echo $value[0]['TotalSurvey']; ?>
                                    </h4>

                                    <h5 class="text-center badges-head">
                                        <span class="badge dashboard-badges" style='background: #00e600;'>Advo: <?php echo number_format($value[0]['Advocate'], 0); ?> </span>
                                        <span class="badge dashboard-badges" style='background: #fec620;'>Pass: <?php echo number_format($value[0]['Passive'], 0); ?> </span>
                                        <span class="badge dashboard-badges" style='background: #ff0000;'>Detra: <?php echo number_format($value[0]['Detractor'], 0); ?> </span>
                                    </h5>
                                </center>
                            </div>
                        </td>
        <?php } else { ?>
                        <td class="col-md-4">   </td>
                    <?php } ?>
                    <!--End first records-->

        <?php if (!is_null($value[1])) { ?>
                        <td class="col-md-4">
                            <div class="ribbon-wrapper">
                                <div class="ribbon-wrapper-gold">
                                    <div class="ribbon-gold">
                                        <i class="fa fa-trophy"></i> Rank <?php echo $rank ?>
                                    </div>
                                </div>
                                <center>
                                    <div class="ribbon-heading">
                                        <div class="nps-badge-orange">
                                            <h4><?php echo $value[1]['NpsScore']; ?></h4>
                                            <small><?php echo $value[1]['StoreTier']; ?></small>
                                        </div>
                                        <h3 style="margin-top: 0;"><i class="fa fa-shopping-cart"></i> <?php echo $value[1]['StoreName']; ?> </h3>
            <?php if ($value[1]['NpsScore'] > 1 && $value['NpsScore'] < 50) { ?>
                                            <script type="text/javascript">
                                                var TotalySurveyParam = <?php echo $value[1]['TotalSurvey']; ?>;
                                                var AdvocateParam = <?php echo $value[1]['Advocate']; ?>;
                                                var DetractorParam = <?php echo $value[1]['Detractor']; ?>;
                                                var SpanIDParam = <?php echo $spanID; ?>;
                                                generateRankStoreEpisode(TotalySurveyParam, AdvocateParam, DetractorParam, SpanIDParam);
                                            </script>
                <?php
                $idSharetLvl1 = "EpisodeToGetLvl1{$spanID}";
                $idSharetLvl2 = "EpisodeToGetLvl2{$spanID}";
                $idSharetLvl3 = "EpisodeToGetLvl3{$spanID}";
                ?>
                                            <table class="table table-bordered table-hover table-condensed"  style="margin-bottom: 10px;" cellspacing="0" >
                                                <thead>
                                                    <tr>
                                                        <th>T1</th>
                                                        <th>T2</th>
                                                        <th>T3</th>
                                                    </tr>
                                                </thead>
                                                <tbody style="text-align:center;">
                                                    <tr>
                                                        <td><div id="<?php echo $idSharetLvl1; ?>"><i class="fa fa-check"></i></div></td>
                                                        <td><div id="<?php echo $idSharetLvl2; ?>"><i class="fa fa-check"></i></div></td>
                                                        <td><div id="<?php echo $idSharetLvl3; ?>"><i class="fa fa-check"></i></div></td>
                                                    </tr>
                                                </tbody>
                                            </table>
            <?php } elseif ($value[1]['NpsScore'] == 0) { ?>
                                            <?php $getClassStore = getEpisodeClassStore($value[1]['NpsScore']); ?>
                                            <div <?php echo $getClassStore['class3']; ?> id="Dashboard-Alert">
                                                <h5><i class="fa fa-check-square"></i> Store needs at least 1 Advocate</h5>
                                            </div>
            <?php } else { ?>
                                            <?php $getClassStore = getEpisodeClassStore($value[1]['NpsScore']); ?>
                                            <div <?php echo $getClassStore['class3']; ?> id="Dashboard-Alert">
                                                <h5><i class="fa fa-check-square"></i> Goal Accomplished!</h5>
                                            </div>
                <?php
            } $spanID++;
            $rank++;
            ?>
                                    </div>

                                    <h4 class="survey">
                                        <i class="fa fa-exclamation-circle"></i> Survey: <?php echo $value[1]['TotalSurvey']; ?>
                                    </h4>

                                    <h5 class="text-center badges-head">
                                        <span class="badge dashboard-badges" style='background: #00e600;'>Advo: <?php echo number_format($value[1]['Advocate'], 0); ?> </span>
                                        <span class="badge dashboard-badges" style='background: #fec620;'>Pass: <?php echo number_format($value[1]['Passive'], 0); ?> </span>
                                        <span class="badge dashboard-badges" style='background: #ff0000;'>Detra: <?php echo number_format($value[1]['Detractor'], 0); ?> </span>
                                    </h5>
                                </center>
                            </div>
                        </td>
        <?php } else { ?>
                        <td class="col-md-4">     </td>
                    <?php } ?>
                    <!--End Second records-->

        <?php if (!is_null($value[2])) { ?>
                        <td class="col-md-4">
                            <div class="ribbon-wrapper">
                                <div class="ribbon-wrapper-gold">
                                    <div class="ribbon-gold">
                                        <i class="fa fa-trophy"></i> Rank <?php echo $rank ?>
                                    </div>
                                </div>
                                <center>
                                    <div class="ribbon-heading">
                                        <div class="nps-badge-orange">
                                            <h4><?php echo $value[2]['NpsScore']; ?></h4>
                                            <small><?php echo $value[2]['StoreTier']; ?></small>
                                        </div>
                                        <h3 style="margin-top: 0;"><i class="fa fa-shopping-cart"></i> <?php echo $value[2]['StoreName']; ?> </h3>
            <?php if ($value[2]['NpsScore'] > 1 && $value[2]['NpsScore'] < 50) { ?>
                                            <script type="text/javascript">
                                                var TotalySurveyParam = <?php echo $value[2]['TotalSurvey']; ?>;
                                                var AdvocateParam = <?php echo $value[2]['Advocate']; ?>;
                                                var DetractorParam = <?php echo $value[2]['Detractor']; ?>;
                                                var SpanIDParam = <?php echo $spanID; ?>;
                                                generateRankStoreEpisode(TotalySurveyParam, AdvocateParam, DetractorParam, SpanIDParam);
                                            </script>
                <?php
                $idSharetLvl1 = "EpisodeToGetLvl1{$spanID}";
                $idSharetLvl2 = "EpisodeToGetLvl2{$spanID}";
                $idSharetLvl3 = "EpisodeToGetLvl3{$spanID}";
                ?>
                                            <table class="table table-bordered table-hover table-condensed"  style="margin-bottom: 10px;" cellspacing="0" >
                                                <thead>
                                                    <tr>
                                                        <th>T1</th>
                                                        <th>T2</th>
                                                        <th>T3</th>
                                                    </tr>
                                                </thead>
                                                <tbody style="text-align:center;">
                                                    <tr>
                                                        <td><div id="<?php echo $idSharetLvl1; ?>"><i class="fa fa-check"></i></div></td>
                                                        <td><div id="<?php echo $idSharetLvl2; ?>"><i class="fa fa-check"></i></div></td>
                                                        <td><div id="<?php echo $idSharetLvl3; ?>"><i class="fa fa-check"></i></div></td>
                                                    </tr>
                                                </tbody>
                                            </table>
            <?php } elseif ($value[2]['NpsScore'] == 0) { ?>
                                            <?php $getClassStore = getEpisodeClassStore($value[2]['NpsScore']); ?>
                                            <div <?php echo $getClassStore['class3']; ?> id="Dashboard-Alert">
                                                <h5><i class="fa fa-check-square"></i> Store needs at least 1 Advocate</h5>
                                            </div>
            <?php } else { ?>
                                            <?php $getClassStore = getEpisodeClassStore($value[2]['NpsScore']); ?>
                                            <div <?php echo $getClassStore['class3']; ?> id="Dashboard-Alert">
                                                <h5><i class="fa fa-check-square"></i> Goal Accomplished!</h5>
                                            </div>
                <?php
            } $spanID++;
            $rank++;
            ?>
                                    </div>

                                    <h4 class="survey">
                                        <i class="fa fa-exclamation-circle"></i> Survey: <?php echo $value[2]['TotalSurvey']; ?>
                                    </h4>

                                    <h5 class="text-center badges-head">
                                        <span class="badge dashboard-badges" style='background: #00e600;'>Advo: <?php echo number_format($value[2]['Advocate'], 0); ?> </span>
                                        <span class="badge dashboard-badges" style='background: #fec620;'>Pass: <?php echo number_format($value[2]['Passive'], 0); ?> </span>
                                        <span class="badge dashboard-badges" style='background: #ff0000;'>Detra: <?php echo number_format($value[2]['Detractor'], 0); ?> </span>
                                    </h5>
                                </center>
                            </div>
                        </td>
        <?php } else { ?>
                        <td class="col-md-4"></td>
                    <?php } ?>
                </tr>
                <?php } ?>
        </tbody>
    </table>
<?php } else { ?>
    No data to display
<?php } ?>

<script>
    $(document).ready(function() {
        $('#StoreEpisodeTable').DataTable({
//            dom: 'lBfrtip',
//            buttons: [
//                'copyHtml5',
//                'excelHtml5',
//                'csvHtml5',
//                'pdfHtml5'
//            ],
            iDisplayLength: 2,
            bFilter: false,
            bInfo: false,
            bSort: false,
            bLengthChange: false
        });
    });
</script>