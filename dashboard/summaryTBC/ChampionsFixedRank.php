<?php
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/mynps/function/getProfileInfo.php');
//include($_SERVER['DOCUMENT_ROOT'] . '/dashboard/function/getChamp.php');
$storeType = $_GET['storeType'];
$StoreChamp = getChampTBC('Fixed', NULL, NULL, NULL, NUll, 'RemoveNull', $storeType);

//echo "<pre>";var_dump($StoreChamp);
?>
<script type="text/javascript">
    $(document).ready(function() {
        var t = $('#ChampFixedRanking').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            "bFilter": false,
            "bPaginate": true,
            "lengthMenu": [[10, -1], [10, "All"]],
            "iDisplayLength": 10
        });

        t.on('order.dt search.dt', function() {
            t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        //Remove Heading Sorting
        $("th").unbind();
        $("th").removeClass("sorting");
        $("th").removeClass("sorting_asc");
    });
</script>

<?php if (!is_null($StoreChamp)) { ?>
    <table class="table table-bordered table-responsive" id="ChampFixedRanking">
        <thead>
            <tr>
                <th style="width:30px; text-align: center;">Rank</th>
                <th style="width:102px">Name</th>
                <th style="width:80px">Store</th>
                <!--<th>AD</th>-->
                <!--<th>PA</th>-->
                <!--<th>D</th>-->
                <th style="width:41px">Survey</th>
                <th style="width:26px">NPS</th>
                <th style="width:43px">Impact</th>
            </tr>
        </thead>
        <?php
        foreach ($StoreChamp as $value) {
            //echo "<pre>";
            //var_dump($value['Data']);
            ?>
            <tr>
                <td style="width:20px; text-align: center;"></td>
                <td><?php echo $value['Data']['EmpName']; ?></td>
                <td><?php echo $value['Data']['Store']; ?></td>
                <!--<td><?php echo $value['Data']['EmpAdvocate']; ?></td>-->
                <!--<td><?php $value['Data']['EmpPassive']; ?></td>-->
                <!--<td><?php echo $value['Data']['EmpDetractor']; ?></td>-->
                <td><?php echo $value['Data']['EmpTotalSurvey']; ?></td>
                <td><?php echo number_format($value['Data']['EmpFixed'], npsDecimal()); ?></td>
                <td><?php echo number_format($value['EmpImpact'], 2); ?></td>
            </tr>

            <?php
        }
        ?>
    </table>
<?php } else { ?>
    No data to display
<?php } ?>


