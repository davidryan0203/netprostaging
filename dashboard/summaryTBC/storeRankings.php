<?php
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/nps/function/NPSFunction.php');
$storeType = $_GET['storeType'];
$StoreInteraction = getStoreNpsTBC(NULL, NULL, NULL, NULL, NULL, NULL, $storeType);
//echo "<pre>";
//var_dump($StoreInteraction['Merged']);
?>
<script type="text/javascript">
    $(document).ready(function() {
        var t = $('#StoreRank').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],"aaSorting": [],
            "bFilter": false,
            "bPaginate": true,
            "lengthMenu": [[10, -1], [10, "All"]],
            "iDisplayLength": 10
        });

        t.on('order.dt search.dt', function() {
            t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        //Remove Heading Sorting
        $("th").unbind();
        $("th").removeClass("sorting");
        $("th").removeClass("sorting_asc");
    });
</script>

<?php if (!is_null($StoreInteraction)) { ?>
    <table class="table table-bordered table-responsive" id="StoreRank">
        <thead>
            <tr>
                <th style="width:20px; text-align: center;">Rank</th>
                <th>Name</th>
                <th>Episode</th>
                <th>Surveys</th>
                <th>Tier</th>
                <th>Mobile Episode</th>
<!--                <th>Tier</th>-->
                <th>Surveys</th>
                <th>Fixed Episode</th>
                <!--<th>Tier</th>-->
                <th>Surveys</th>
            </tr>
        </thead>

        <?php
        $rank = 1;
        foreach ($StoreInteraction['Merged'] as $value) {
            ?>
            <tr>
                <td style="width:20px; text-align: center;">Rank <?php echo $rank; ?></td>
                <td><?php echo $value['StoreName']; ?></td>
                <td><?php echo number_format($value['EpisodeNPS'], npsDecimal()); ?></td>
                <td><?php echo $value['EpisodeTier']; ?></td>
                <td><?php echo $value['EpisodeTotalSurvey']; ?></td>
                <td><?php echo number_format($value['MobileNPS'], npsDecimal()); ?></td>
                <!--<td><?php // echo $value['MobileTier']; ?></td>-->
                <td><?php echo $value['MobileTotalSurvey']; ?></td>
                <td><?php echo number_format($value['FixedNPS'], npsDecimal()); ?></td>
<!--                <td><?php // echo $value['FixedTier']; ?></td>-->
                <td><?php echo $value['FixedTotalSurvey']; ?></td>
            </tr>
            <?php
            $rank++;
        }
        ?>
    </table>
<?php } else { ?>
    No data to display
<?php } ?>

