<?php
include('nps/function/NPSFunction.php');
require($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');

$storeTypes = checkStoreTypes();
$SelectStoreTypes = SelectStoreTypes();
?>

<!--Main dashboard container-->
<section class='dashboard'>
    <div class="row">
        <div class="col-lg-12">     

            <h4 class="page-header">
                <i class="fa fa-file-word-o fa-fw"></i> Competitive NPS Results for <?php echo $latestRec['cur_month']; ?>
            </h4>

            <div class="filters">
                <?php if ($SelectStoreTypes['StoreTypes'] == 'Both Stores') { ?>
                    <input type="checkbox" id="StoreType" name="StoreType" class="StoreType btn-sm" data-toggle="toggle" data-style="ios"  data-onstyle="info"  
                           data-offstyle="warning" data-on="TBTC" data-off="TLS" />   
                       <?php } else { ?>
                    <input type="checkbox" style="display:none"  id="StoreType" name="StoreType" class="StoreType" />  
                <?php } ?>
            </div>
        </div>
    </div>

    <h4 class="latest-record pull-left"> 
        <?php echo $latestRec['latest_record']; ?> 
    </h4>
    <br>

    <div  id="page-contents" class="page-contents"></div>
</section>


<script type="text/javascript">
    $(document).ready(function () {
        
      <?php if ($SelectStoreTypes['StoreTypes'] == 'Only TBC') { ?>
            $("#StoreType").trigger('click');
<?php } ?>  
        
        
        var storeType = "";
        var url = "";

        function setLoad() {
            if ($('#StoreType').prop('checked')) {
                storeType = 'tbc';
                url = 'dashboard/dashboardextendTBC.php?storeType=' + storeType;
            } else {
                storeType = 'tls';
                url = 'dashboard/dashboardextend.php?storeType=' + storeType;
            }
            //  console.log(url);
            return url;
        }

        var url = setLoad();
        $('#page-contents').load(url).fadeIn("slow");

        $('#StoreType').change(function () {
            var url = setLoad();
            $('#page-contents').html('').load(url).fadeIn("slow");
        });
    });
</script>








