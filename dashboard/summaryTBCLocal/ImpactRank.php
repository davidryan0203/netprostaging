<?php
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/dashboard/function/getChamp.php');
$EmpChampData = getImpactRank();
?>

<div class="row">
    <!--<h3 class="blink_me text-center">"Congratulations Episode Champions! Keep up the good work."</h3>-->

    <?php if (!is_null($EmpChampData)) { ?>
        <?php foreach ($EmpChampData as $value) { ?>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                <div class="ribbon-wrapper">
            <!--                <div class="ribbon-wrapper-gold"><div class="ribbon-gold"><?php echo number_format($value['EmpNpsScorePercentage'], 0); ?> %
                        </div>
                    </div>-->
                    <center>
                        <div class="ribbon-heading">
                            <h4 style="margin-top: 0;"><i class="fa fa-user"></i> <?php echo $value['EmpName']; ?> </h4>
                            <h6 style='color: #fec620; font-weight: bold;'><i class="fa fa-trophy"></i> <?php echo $value['Store']; ?> Champion </h6>
                            <!--<img class="img-circle img-responsive ChampPic" src="images/user_icon.png" alt="Profile Picture">-->

                            <div class="nps-badge-orange">
                                <h4><?php echo number_format($value['EmpNpsScorePercentage'], npsDecimal()) ?></h4>
                                <small>EPISODE</small>
                            </div>
                        </div>
                        <h4 class="survey">
                            <i class="fa fa-exclamation-circle"></i>
                            Impact to Store: <?php echo $value['EmpTotalSurvey']; ?><span class="badge dashboard-badges" style="background: #01b1ea;"></span>
                        </h4>

                        <h5 class="text-center badges-head">
                            <span class="badge dashboard-badges" style='background: #00e600;'>AD: <?php echo number_format($value['EmpTotalAdvocate'], 0); ?> </span>
                            <span class="badge dashboard-badges" style='background: #fec620;'>PA: <?php echo number_format($value['EmpTotalPassive'], 0); ?> </span>
                            <span class="badge dashboard-badges" style='background: #ff0000;'>D: <?php echo number_format($value['EmpTotalDetractors'], 0); ?> </span>
                            <span class="badge dashboard-badges" style='background: #01b1ea;'>All: <?php echo number_format($value['EmpTotalSurvey'], 0); ?> </span>
                        </h5>
                    </center>
                </div>
            </div>
        <?php } ?>
    <?php } else { ?>
        No data to display
    <?php } ?>
</div>

<script>
    function blinker() {
        $('.blink_me').fadeOut(75).fadeIn(75);
    }
    setInterval(blinker, 1500); //Runs every second
</script>


