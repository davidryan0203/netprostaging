<?php
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/nps/function/NPSFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/mynps/function/InteractionMessage.php');
$storeType = $_GET['storeType'];
$StoreInteraction = array();
$StoreMobile = array();
$StoreFixed = array();
$storeNpsData = getStoreNpsTBC(NULL, NULL, $empID, $to, $from, NULL, $storeType);

function replacer(& $item, $key) {
    if ($item === null) {
        $item = 0;
    }
}

foreach ($storeNpsData['Merged'] as $storeData) {


    array_walk_recursive($storeData, 'replacer');

    $StoreInteraction[] = array(
        'StoreName' => $storeData['StoreName'],
        'InteractionStoreTier' => $storeData['InteractionTier'],
        'InteractionAdvocate' => $storeData['InteractionAdvocates'],
        'InteractionPassive' => $storeData['InteractionPasives'],
        'InteractionDetractor' => $storeData['InteractionDetractors'],
        'InteractionTotalSurvey' => $storeData['InteractionTotalSurvey'],
        'InteractionNpsScore' => number_format($storeData['InteractionNPS'], npsDecimal())
    );

    $StoreMobile[] = array(
        'StoreName' => $storeData['StoreName'],
        'MobileStoreTier' => $storeData['MobileTier'],
        'MobileAdvocate' => $storeData['MobileAdvocates'],
        'MobilePassive' => $storeData['MobilePasives'],
        'MobileDetractor' => $storeData['MobileDetractors'],
        'MobileTotalSurvey' => $storeData['MobileTotalSurvey'],
        'MobileNpsScore' => number_format($storeData['MobileNPS'], npsDecimal())
    );

    $StoreFixed[] = array(
        'StoreName' => $storeData['StoreName'],
        'FixedStoreTier' => $storeData['FixedTier'],
        'FixedAdvocate' => $storeData['FixedAdvocates'],
        'FixedPassive' => $storeData['FixedPasives'],
        'FixedDetractor' => $storeData['FixedDetractors'],
        'FixedTotalSurvey' => $storeData['FixedTotalSurvey'],
        'FixedNpsScore' => number_format($storeData['FixedNPS'], npsDecimal())
    );
}
//echo "<pre>";
//var_dump($StoreFixed);


foreach ($StoreInteraction as $sort) {
    $toSortNpsScoreInteraction[] = $sort['InteractionNpsScore'];
}

foreach ($StoreMobile as $sort) {
    $toSortNpsScoreMobile[] = $sort['MobileNpsScore'];
}

foreach ($StoreFixed as $sort) {
    $toSortNpsScoreFixed[] = $sort['FixedNpsScore'];
}


array_multisort($toSortNpsScoreInteraction, SORT_DESC, $StoreInteraction);
array_multisort($toSortNpsScoreMobile, SORT_DESC, $StoreMobile);
array_multisort($toSortNpsScoreFixed, SORT_DESC, $StoreFixed);

$interactionChunk = array_chunk($StoreInteraction, 3, false);
$mobileChunk = array_chunk($StoreMobile, 3, false);
$fixedChunk = array_chunk($StoreFixed, 3, false);
?>


<script type="text/javascript">

    $(document).ready(function() {
        Number.prototype.between = function(min, max) {
            return this >= min && this <= max;
        };

    });

    function generateRankStoreMobile(TotalySurvey, Advocate, Detractor, SpanID, SpanIDName) {
        var NewTotalSurvey2 = TotalySurvey;
        var newInterAdvoTotal2 = Advocate;
        var InteractionTotalDetractorTotal2 = Detractor;

        var counter2 = 0;
        var newDetPercentage2 = 0;
        var newAdvoPercentage2 = 0;
        var newIntercationNPS2 = 0;
        var roundedstoreNPS = 0;

        // console.log(newIntercationNPS2);
        roundedstoreNPS = Math.round(newIntercationNPS2);

        while (newIntercationNPS2 < 60) {
            counter2 += 1;
            newAdvoPercentage2 = (newInterAdvoTotal2 / NewTotalSurvey2) * 100;
            newDetPercentage2 = (InteractionTotalDetractorTotal2 / NewTotalSurvey2) * 100;
            newIntercationNPS2 = newAdvoPercentage2 - newDetPercentage2;

            roundedstoreNPS = Math.round(newIntercationNPS2);

            if (roundedstoreNPS < 25) {
                $("#" + SpanIDName + "ToGetLvl5" + SpanID).empty();
                $("#" + SpanIDName + "ToGetLvl5" + SpanID).append(counter2);
                //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 4");
            } else if ((roundedstoreNPS).between(25, 42)) {
                $("#" + SpanIDName + "ToGetLvl4" + SpanID).empty();
                $("#" + SpanIDName + "ToGetLvl4" + SpanID).append(counter2);
                //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 4");
            } else if ((roundedstoreNPS).between(43, 45)) {
                $("#" + SpanIDName + "ToGetLvl3" + SpanID).empty();
                $("#" + SpanIDName + "ToGetLvl3" + SpanID).append(counter2);
                //   console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
            } else if ((roundedstoreNPS).between(46, 49)) {
                $("#" + SpanIDName + "ToGetLvl2" + SpanID).empty();
                $("#" + SpanIDName + "ToGetLvl2" + SpanID).append(counter2);
                //   console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 2");
            } else if ((roundedstoreNPS).between(50, 59)) {
                $("#" + SpanIDName + "ToGetLvl1" + SpanID).empty();
                $("#" + SpanIDName + "ToGetLvl1" + SpanID).append(counter2);
                //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 1");
            }
            newInterAdvoTotal2 += 1;
            NewTotalSurvey2 += 1;
        }
    }

    function generateRankStoreFixed(TotalySurvey, Advocate, Detractor, SpanID, SpanIDName) {
        var NewTotalSurvey2 = TotalySurvey;
        var newInterAdvoTotal2 = Advocate;
        var InteractionTotalDetractorTotal2 = Detractor;

        var counter2 = 0;
        var newDetPercentage2 = 0;
        var newAdvoPercentage2 = 0;
        var newIntercationNPS2 = 0;
        var roundedstoreNPS = 0;
        // console.log(newIntercationNPS2);

        while (newIntercationNPS2 < 15) {
            counter2 += 1;
            newAdvoPercentage2 = (newInterAdvoTotal2 / NewTotalSurvey2) * 100;
            newDetPercentage2 = (InteractionTotalDetractorTotal2 / NewTotalSurvey2) * 100;
            newIntercationNPS2 = newAdvoPercentage2 - newDetPercentage2;

            roundedstoreNPS = Math.round(newIntercationNPS2);

            if (roundedstoreNPS < -35) {
                $("#" + SpanIDName + "ToGetLvl4" + SpanID).empty();
                $("#" + SpanIDName + "ToGetLvl4" + SpanID).append(counter2);
                //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 4");
            } else if ((roundedstoreNPS).between(-35, -12)) {
                $("#" + SpanIDName + "ToGetLvl3" + SpanID).empty();
                $("#" + SpanIDName + "ToGetLvl3" + SpanID).append(counter2);
                //   console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
            } else if ((roundedstoreNPS).between(-13, 1)) {
                $("#" + SpanIDName + "ToGetLvl2" + SpanID).empty();
                $("#" + SpanIDName + "ToGetLvl2" + SpanID).append(counter2);
                //   console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 2");
            } else if ((roundedstoreNPS).between(0, 14)) {
                $("#" + SpanIDName + "ToGetLvl1" + SpanID).empty();
                $("#" + SpanIDName + "ToGetLvl1" + SpanID).append(counter2);
                //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 1");
            }
            newInterAdvoTotal2 += 1;
            NewTotalSurvey2 += 1;
        }
    }

    function generateRankStoreInteraction(TotalySurvey, Advocate, Detractor, SpanID, SpanIDName) {

        var NewTotalSurvey2 = TotalySurvey;
        var newInterAdvoTotal2 = Advocate;
        var InteractionTotalDetractorTotal2 = Detractor;

        var counter2 = 0;
        var newDetPercentage2 = 0;
        var newAdvoPercentage2 = 0;
        var newIntercationNPS2 = 0;
        var roundedstoreNPS = 0;

        while (newIntercationNPS2 < 38) {
            counter2 += 1;
            newAdvoPercentage2 = (newInterAdvoTotal2 / NewTotalSurvey2) * 100;
            newDetPercentage2 = (InteractionTotalDetractorTotal2 / NewTotalSurvey2) * 100;
            newIntercationNPS2 = newAdvoPercentage2 - newDetPercentage2;

            roundedstoreNPS = Math.round(newIntercationNPS2);

            if (roundedstoreNPS < 38) {
                $("#" + SpanIDName + "ToGetLvl1" + SpanID).empty();
                $("#" + SpanIDName + "ToGetLvl1" + SpanID).append(counter2);
                //   console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 1");
            }
            newInterAdvoTotal2 += 1;
            NewTotalSurvey2 += 1;
        }
    }
</script>
<style>
    #tableHead{
        display: none;
        table-layout: fixed;
        width:100%;
    }
</style>
<div id="InteractionDiv">
<?php if (!is_null($StoreInteraction[0])) { ?>


        <table id="tblInteraction" border="0">
            <thead id="tableHead">
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
    <?php
    $rank = 1;
    $colorID = 0;
    $spanID = 0;

    foreach ($interactionChunk as $value) {
        $tablecount = 0;
        ?>

                    <tr> <?php while ($tablecount < 3) { ?>
                        <?php if (!is_null($value[$tablecount])) { ?>
                                <td class="col-md-3">
                                    <div class="ribbon-wrapper">
                                        <div class="ribbon-wrapper-gold">
                                            <div class="ribbon-gold">
                                                <i class="fa fa-trophy"></i> Rank <?php echo $rank ?>
                                            </div>
                                        </div>
                                        <center>
                                            <div class="ribbon-heading">
                                                <!--<h3 class="sub-header" style="margin-top: 0px;">Interaction</h3>-->
                                                <!--<img class="img-circle img-responsive ChampPic" src="images/user_icon.png" alt="Profile Picture">-->
                                                <div class="nps-badge-gold" id="featuredChampsBadge">
                                                    <h4><?php echo $value[$tablecount]['InteractionNpsScore']; ?></h4>
                                                    <small><?php echo $value[$tablecount]['InteractionStoreTier']; ?></small>
                                                </div>
                                                <h2 style="margin-top: 0;"><i class="fa fa-shopping-cart"></i> <?php echo $value[$tablecount]['StoreName']; ?> </h2>

                <?php if ($value[$tablecount]['InteractionNpsScore'] < 38) { ?>
                                                    <script type="text/javascript">
                                                        $(document).ready(function() {

                                                            var TotalySurveyParam = <?php echo $value[$tablecount]['InteractionTotalSurvey']; ?>;
                                                            var AdvocateParam = <?php echo $value[$tablecount]['InteractionAdvocate']; ?>;
                                                            var DetractorParam = <?php echo $value[$tablecount]['InteractionDetractor']; ?>;
                                                            var SpanIDParam = <?php echo $spanID; ?>;
                                                            var SpanIDName = "Interaction";

                                                            generateRankStoreInteraction(TotalySurveyParam, AdvocateParam, DetractorParam, SpanIDParam, SpanIDName);
                                                        });
                                                    </script>
                    <?php $idSharet1 = "InteractionToGetLvl1{$spanID}"; ?>
                    <?php $idSharet2 = "InteractionToGetLvl2{$spanID}"; ?>
                    <?php $idSharet3 = "InteractionToGetLvl3{$spanID}"; ?>
                    <?php $idSharet4 = "InteractionToGetLvl4{$spanID}"; ?>
                    <?php $idSharet5 = "InteractionToGetLvl5{$spanID}"; ?>
                                                    <table class="table table-bordered table-hover table-condensed"  style="margin-bottom: 10px;" cellspacing="0" >
                                                        <thead>
                                                            <tr>
                                                                <th>T1</th>
                                                                <th>T2</th>
                                                                <th>T3</th>
                                                                <th>T4</th>
                                                                <th>T5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody style="text-align:center;">
                                                            <tr>
                                                                <td><div id="<?php echo $idSharet1; ?>"><i class="fa fa-check"></i></div></td>
                                                                <td><div id="<?php echo $idSharet2; ?>"><i class="fa fa-check"></i></div></td>
                                                                <td><div id="<?php echo $idSharet3; ?>"><i class="fa fa-check"></i></div></td>
                                                                <td><div id="<?php echo $idSharet4; ?>"><i class="fa fa-check"></i></div></td>
                                                                <td><div id="<?php echo $idSharet5; ?>"><i class="fa fa-check"></i></div></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>


                <?php } elseif ($value[$tablecount]['InteractionNpsScore'] == 0) { ?>
                    <?php $getClassStore = getClassStoreIntTBC($value[$tablecount]['InteractionNpsScore']); ?>
                                                    <div <?php echo $getClassStore['class2']; ?> id="Dashboard-Alert">
                                                        <h5><i class="fa fa-check-square"></i> Store needs at least 1 Advocate</h5>
                                                    </div>
                <?php } else { ?>
                                                    <?php $getClassStore = getClassStoreIntTBC($value[$tablecount]['InteractionNpsScore']); ?>
                                                    <div <?php echo $getClassStore['class2']; ?> id="Dashboard-Alert">
                                                        <h5><i class="fa fa-check-square"></i> Goals Accomplished!</h5>
                                                    </div>
                    <?php
                } $spanID++;
                $rank++;
                ?>
                                            </div>

                                            <h4 class="survey">
                                                <i class="fa fa-exclamation-circle"></i> Survey: <?php echo $value[$tablecount]['InteractionTotalSurvey']; ?>
                                            </h4>

                                            <h5 class="text-center badges-head">
                                                <span class="badge dashboard-badges" style='background: #00e600;'>Advo: <?php echo number_format($value[$tablecount]['InteractionAdvocate'], 0); ?> </span>
                                                <span class="badge dashboard-badges" style='background: #fec620;'>Pass: <?php echo number_format($value[$tablecount]['InteractionPassive'], 0); ?> </span>
                                                <span class="badge dashboard-badges" style='background: #ff0000;'>Detra: <?php echo number_format($value[$tablecount]['InteractionDetractor'], 0); ?> </span>
                                            </h5>
                                        </center>
                                    </div>
                                </td>
            <?php } else {
                ?>
                                <td class="col-md-3">
                                </td>
            <?php } ?>
            <?php
            $tablecount++;
        }
        ?>
                    </tr>

                    <?php } ?> </tbody></table>
                <?php } else { ?>
        No data to display
                <?php } ?>
</div>
<div id="MobileDiv" style="display:none">
            <?php if (!is_null($StoreMobile[0])) { ?>


        <table id="tblMobile" border="0">
            <thead id="tableHead">
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
    <?php
    $rank = 1;
    $colorID = 0;
    $spanID = 0;

    foreach ($mobileChunk as $value) {
        $tablecount = 0;
        ?>

                    <tr> <?php while ($tablecount < 3) { ?>
                        <?php if (!is_null($value[$tablecount])) { ?>
                                <td class="col-md-3">
                                    <div class="ribbon-wrapper">
                                        <div class="ribbon-wrapper-gold">
                                            <div class="ribbon-gold">
                                                <i class="fa fa-trophy"></i> Rank <?php echo $rank ?>
                                            </div>
                                        </div>
                                        <center>
                                            <div class="ribbon-heading">
                                                <!--<h3 class="sub-header" style="margin-top: 0px;">Interaction</h3>-->
                                                <!--<img class="img-circle img-responsive ChampPic" src="images/user_icon.png" alt="Profile Picture">-->
                                                <div class="nps-badge-orange" id="featuredChampsBadge">
                                                    <h4><?php echo $value[$tablecount]['MobileNpsScore']; ?></h4>
                                                    <small><?php echo $value[$tablecount]['MobileStoreTier']; ?></small>
                                                </div>
                                                <h3 style="margin-top: 0;"><i class="fa fa-shopping-cart"></i> <?php echo $value[$tablecount]['StoreName']; ?> </h3>

                <?php if ($value[$tablecount]['MobileNpsScore'] < 60) { ?>
                                                    <script type="text/javascript">
                                                        $(document).ready(function() {

                                                            var TotalySurveyParam = <?php echo $value[$tablecount]['MobileTotalSurvey']; ?>;
                                                            var AdvocateParam = <?php echo $value[$tablecount]['MobileAdvocate']; ?>;
                                                            var DetractorParam = <?php echo $value[$tablecount]['MobileDetractor']; ?>;
                                                            var SpanIDParam = <?php echo $spanID; ?>;
                                                            var SpanIDName = "Mobile";

                                                            generateRankStoreMobile(TotalySurveyParam, AdvocateParam, DetractorParam, SpanIDParam, SpanIDName);
                                                        });
                                                    </script>
                    <?php $idSharet1 = "MobileToGetLvl1{$spanID}"; ?>
                    <?php $idSharet2 = "MobileToGetLvl2{$spanID}"; ?>
                    <?php $idSharet3 = "MobileToGetLvl3{$spanID}"; ?>
                    <?php $idSharet4 = "MobileToGetLvl4{$spanID}"; ?>
                    <?php $idSharet5 = "MobileToGetLvl5{$spanID}"; ?>
                                                    <table class="table table-bordered table-hover table-condensed"  style="margin-bottom: 10px;" cellspacing="0" >
                                                        <thead>
                                                            <tr>
                                                                <th>T1</th>
                                                                <th>T2</th>
                                                                <th>T3</th>
                                                                <th>T4</th>
                                                                <th>T5</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody style="text-align:center;">
                                                            <tr>
                                                                <td><div id="<?php echo $idSharet1; ?>"><i class="fa fa-check"></i></div></td>
                                                                <td><div id="<?php echo $idSharet2; ?>"><i class="fa fa-check"></i></div></td>
                                                                <td><div id="<?php echo $idSharet3; ?>"><i class="fa fa-check"></i></div></td>
                                                                <td><div id="<?php echo $idSharet4; ?>"><i class="fa fa-check"></i></div></td>
                                                                <td><div id="<?php echo $idSharet5; ?>"><i class="fa fa-check"></i></div></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>


                <?php } elseif ($value[$tablecount]['MobileNpsScore'] == 0) { ?>
                    <?php $getClassStore = getClassStoreMobileTBC($value[$tablecount]['MobileNpsScore']); ?>
                                                    <div <?php echo $getClassStore['class2']; ?> id="Dashboard-Alert">
                                                        <h5><i class="fa fa-check-square"></i> Store needs at least 1 Advocate</h5>
                                                    </div>
                <?php } else { ?>
                                                    <?php $getClassStore = getClassStoreMobileTBC($value[$tablecount]['MobileNpsScore']); ?>
                                                    <div <?php echo $getClassStore['class2']; ?> id="Dashboard-Alert">
                                                        <h5><i class="fa fa-check-square"></i> Goals Accomplished!</h5>
                                                    </div>
                    <?php
                }
                $spanID++;
                $rank++;
                ?>
                                            </div>

                                            <h4 class="survey">
                                                <i class="fa fa-exclamation-circle"></i> Survey: <?php echo $value[$tablecount]['MobileTotalSurvey']; ?>
                                            </h4>

                                            <h5 class="text-center badges-head">
                                                <span class="badge dashboard-badges" style='background: #00e600;'>Advo: <?php echo number_format($value[$tablecount]['MobileAdvocate'], 0); ?> </span>
                                                <span class="badge dashboard-badges" style='background: #fec620;'>Pass: <?php echo number_format($value[$tablecount]['MobilePassive'], 0); ?> </span>
                                                <span class="badge dashboard-badges" style='background: #ff0000;'>Detra: <?php echo number_format($value[$tablecount]['MobileDetractor'], 0); ?> </span>
                                            </h5>
                                        </center>
                                    </div>
                                </td>
            <?php } else {
                ?>
                                <td class="col-md-3">
                                </td>
            <?php } ?>
            <?php
            $tablecount++;
        }
        ?>
                    </tr>

                    <?php } ?> </tbody></table>
                <?php } else { ?>
        No data to display
                <?php } ?>
</div>
<div id="FixedDiv" style="display:none">
            <?php if (!is_null($StoreFixed[0])) { ?>


        <table id="tblFixed" border="0">
            <thead id="tableHead">
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
    <?php
    $rank = 1;
    $colorID = 0;
    $spanID = 0;

    foreach ($fixedChunk as $value) {
        $tablecount = 0;
        ?>

                    <tr> <?php while ($tablecount < 3) { ?>
                        <?php if (!is_null($value[$tablecount])) { ?>
                                <td class="col-md-3">
                                    <div class="ribbon-wrapper">
                                        <div class="ribbon-wrapper-gold">
                                            <div class="ribbon-gold">
                                                <i class="fa fa-trophy"></i> Rank <?php echo $rank ?>
                                            </div>
                                        </div>
                                        <center>
                                            <div class="ribbon-heading">
                                                <!--<h3 class="sub-header" style="margin-top: 0px;">Interaction</h3>-->
                                                <!--<img class="img-circle img-responsive ChampPic" src="images/user_icon.png" alt="Profile Picture">-->
                                                <div class="nps-badge-blue" id="featuredChampsBadge">
                                                    <h4><?php echo $value[$tablecount]['FixedNpsScore']; ?></h4>
                                                    <small><?php echo $value[$tablecount]['FixedStoreTier']; ?></small>
                                                </div>
                                                <h3 style="margin-top: 0;"><i class="fa fa-shopping-cart"></i> <?php echo $value[$tablecount]['StoreName']; ?> </h3>

                <?php if ($value[$tablecount]['FixedNpsScore'] < 15) { ?>
                                                    <script type="text/javascript">
                                                        $(document).ready(function() {

                                                            var TotalySurveyParam = <?php echo $value[$tablecount]['FixedTotalSurvey']; ?>;
                                                            var AdvocateParam = <?php echo $value[$tablecount]['FixedAdvocate']; ?>;
                                                            var DetractorParam = <?php echo $value[$tablecount]['FixedDetractor']; ?>;
                                                            var SpanIDParam = <?php echo $spanID; ?>;
                                                            var SpanIDName = "Fixed";

                                                            generateRankStoreFixed(TotalySurveyParam, AdvocateParam, DetractorParam, SpanIDParam, SpanIDName);
                                                        });
                                                    </script>
                    <?php $idSharet1 = "FixedToGetLvl1{$spanID}"; ?>
                    <?php $idSharet2 = "FixedToGetLvl2{$spanID}"; ?>
                    <?php $idSharet3 = "FixedToGetLvl3{$spanID}"; ?>
                    <?php $idSharet4 = "FixedToGetLvl4{$spanID}"; ?>
                    <?php $idSharet5 = "FixedToGetLvl5{$spanID}"; ?>
                                                    <table class="table table-bordered table-hover table-condensed"  style="margin-bottom: 10px;" cellspacing="0" >
                                                        <thead>
                                                            <tr>
                                                                <th>T1</th>
                                                                <th>T2</th>
                                                                <th>T3</th>
                                                                <th>T4</th>
                                                                <!--<th>T5</th>-->
                                                            </tr>
                                                        </thead>
                                                        <tbody style="text-align:center;">
                                                            <tr>
                                                                <td><div id="<?php echo $idSharet1; ?>"><i class="fa fa-check"></i></div></td>
                                                                <td><div id="<?php echo $idSharet2; ?>"><i class="fa fa-check"></i></div></td>
                                                                <td><div id="<?php echo $idSharet3; ?>"><i class="fa fa-check"></i></div></td>
                                                                <td><div id="<?php echo $idSharet4; ?>"><i class="fa fa-check"></i></div></td>
                                                                <!--<td><div id="<?php echo $idSharet5; ?>"><i class="fa fa-check"></i></div></td>-->
                                                            </tr>
                                                        </tbody>
                                                    </table>


                <?php } elseif ($value[$tablecount]['FixedNpsScore'] == 0) { ?>
                    <?php $getClassStore = getClassStoreFixedTBC($value[$tablecount]['FixedNpsScore']); ?>
                                                    <div <?php echo $getClassStore['class2']; ?> id="Dashboard-Alert">
                                                        <h5><i class="fa fa-check-square"></i> Store needs at least 1 Advocate</h5>
                                                    </div>
                <?php } else { ?>
                                                    <?php $getClassStore = getClassStoreFixedTBC($value[$tablecount]['FixedNpsScore']); ?>
                                                    <div <?php echo $getClassStore['class2']; ?> id="Dashboard-Alert">
                                                        <h5><i class="fa fa-check-square"></i> Goals Accomplished!</h5>
                                                    </div>
                    <?php
                } $spanID++;
                $rank++;
                ?>
                                            </div>

                                            <h4 class="survey">
                                                <i class="fa fa-exclamation-circle"></i> Survey: <?php echo $value[$tablecount]['FixedTotalSurvey']; ?>
                                            </h4>

                                            <h5 class="text-center badges-head">
                                                <span class="badge dashboard-badges" style='background: #00e600;'>Advo: <?php echo number_format($value[$tablecount]['FixedAdvocate'], 0); ?> </span>
                                                <span class="badge dashboard-badges" style='background: #fec620;'>Pass: <?php echo number_format($value[$tablecount]['FixedPassive'], 0); ?> </span>
                                                <span class="badge dashboard-badges" style='background: #ff0000;'>Detra: <?php echo number_format($value[$tablecount]['FixedDetractor'], 0); ?> </span>
                                            </h5>
                                        </center>
                                    </div>
                                </td>
            <?php } else {
                ?>
                                <td class="col-md-3">
                                </td>
            <?php } ?>
            <?php
            $tablecount++;
        }
        ?>
                    </tr>

                    <?php } ?> </tbody></table>
                <?php } else { ?>
        No data to display
                <?php } ?>
</div>

<script type="text/javascript">
    $(document).ready(function() {

        var table = $('#tblInteraction,#tblMobile,#tblFixed').DataTable({

            "iDisplayLength": 2, bFilter: false, bInfo: false,
            "bSort": false,
            "bLengthChange": false
                    // "aLengthMenu": [[15, 50, 100, -1], [15, 50, 100, "All"]],

        });

    });

</script>
