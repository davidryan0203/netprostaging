<?php
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/mynps/function/getProfileInfo.php');
//include($_SERVER['DOCUMENT_ROOT'] . '/dashboard/function/getChamp.php');
$storeType = $_GET['storeType'];
//$StoreChamp = getChampOthers('Fixed','AllEmployees','no',NULL,Null,'RemoveNull',$storeType);

$storeID = $_GET['storeID'];
$reportRange = $_GET['rangeType'];

$StoreChamp = getCombinedOthers($from, $to, $reportRange);

//echo "<pre>";
//var_dump($StoreChamp);
?>
<script type="text/javascript">
    $(document).ready(function() {
        var t = $('#ChampCombinedTblRanking').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            "bFilter": true,
            "bPaginate": true,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "iDisplayLength": 10
        });

        //Remove Heading Sorting
        $("th").unbind();
        $("th").removeClass("sorting");
        $("th").removeClass("sorting_asc");
    });
</script>

<?php if (!is_null($StoreChamp)) { ?>
    <table class="table table-bordered table-responsive" id="ChampCombinedTblRanking">
        <thead>
            <tr>
                <!--<th style="width:30px; text-align: center;">Rank</th>-->
                <th style="width:80px">Store</th>
                <th style="width:102px">Name</th>

                <th style="width:41px">OTG</th>
                <th style="width:26px">Home</th>
                <th style="width:26px">OTG Volume</th>
                <th style="width:43px">Home Volume</th>
            </tr>
        </thead>
        <?php
        foreach ($StoreChamp as $value) {
//            echo "<pre>";
//            var_dump($value);
//            exit;
            ?>
            <tr>
        <!--                <td style="width:20px; text-align: center;"></td>-->
                <td><?php echo $value['f_StoreListName']; ?></td>
                <td><?php echo $value['EmpName']; ?></td>
                <td><?php echo number_format(is_null($value['MobEmpNPS']) ? ' ' : $value['MobEmpNPS'], npsDecimal()); ?></td>
                <td><?php echo number_format(is_null($value['FixEmpNPS']) ? ' ' : $value['FixEmpNPS'], npsDecimal()); ?></td>
                <td><?php echo $value['MobEmpTotalSurvey']; ?></td>
                <td><?php echo $value['FixEmpTotalSurvey']; ?></td>
        <!--                <td><?php echo number_format($value['EmpEpisode'], npsDecimal()); ?></td>
                <td><?php echo number_format($value['EmpImpact'], 2); ?></td>-->
            </tr>

            <?php
        }
        ?>
    </table>
<?php } else { ?>
    No data to display
<?php } ?>


