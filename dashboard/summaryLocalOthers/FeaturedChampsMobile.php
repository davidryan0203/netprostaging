<?php
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/dashboard/function/getChamp.php');

$storeType = $_GET['storeType'];
//$EmpChampData = getChampOthers('Mobile', 'AllEmployees', 'no', NULL, NUlL, 'RemoveNull', $storeType);
$storeID = $_GET['storeID'];
$reportRange = $_GET['rangeType'];

$EmpChampData = getChampOthers('Mobile', 'AllEmployees', 'no', NULL, NUlL, 'RemoveNull', $storeType, $isCombined, $empID, $reconOwnerID, $fromNpsPage, $storeID, $reportRange);
$interactionChunk = $EmpChampData[0];
$rank = 1;
?>


<?php if (!is_null($interactionChunk)) { ?>
    <div class="row">
        <?php
        foreach ($interactionChunk as $value) {
            ?>
            <div class="col-lg-4 col-md-6">
                <div class="emp-results orange">
                    <div class="rank-container">
                        <h3><i class="fa fa-trophy"></i><?php echo $rank ?> </h3>
                    </div>
                    <div class="emp-details">
                        <h5 class="emp-name"><i class="fa fa-user"></i> <?php echo $value['Data']['EmpName']; ?> </h5>
                        <h6>NPS: <?php echo number_format($value['Data']['EmpMobile'], npsDecimal()) ?></h6>
                        <h6>Surveys: <?php echo $value['Data']['EmpTotalSurvey']; ?></h6>
                        <h6>Impact: <?php echo number_format($value['EmpImpact'], 2); ?></h6>
                        <h6 class="badge badge-green">Advo: <?php echo number_format($value['Data']['EmpAdvocate'], 0); ?> </h6>
                        <h6 class="badge badge-orange">Pass: <?php echo number_format($value['Data']['EmpPassive'], 0); ?> </h6>
                        <h6 class="badge badge-red">Detra: <?php echo number_format($value['Data']['EmpDetractor'], 0); ?> </h6>
                    </div>
                    <div class="pic-container">
                        <img class="ca-pics" src="<?php echo $value['Data']['ImgUrl']; ?>" alt=""/>
                    </div>
                </div>
            </div>
            <?php
            $rank++;
        }
        ?>
    </div>
<?php } else { ?>
    No data to display
<?php } ?>

