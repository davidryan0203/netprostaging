<?php
session_start();

include($_SERVER['DOCUMENT_ROOT'] . '/nps/function/NPSFunction.php');


if (isset($_GET['reconOwnerID']) && !is_null($_GET['reconOwnerID'])) {
    $profileID = $_GET['reconOwnerID'];
} else {
    $profileID = $_SESSION['UserProfileID'];
}

$to = $_GET['to'];
$from = $_GET['from'];


if (isset($_GET['weekly']) && $_GET['weekly'] == "on") {
    $to = date("Y-m-d");
    // $from = date("Y-m-d", strtotime('4 weeks ago'));
    $from = date("Y-m-d", strtotime('-27 days'));
    $addonStoreName = "wkly";
    // $tableID = "tblInteractionNpsWeekly";
} elseif (isset($_GET['3months']) && $_GET['3months'] == "on") {
    $to = $_GET['to'];
    $from = date("Y-m-01", strtotime('-3 Months'));
    $tableID = "3Months";
} elseif (isset($_GET['htd']) && $_GET['htd'] == "on") {
    $currentMonth = date('m');

    if (($currentMonth >= 1) && ($currentMonth <= 6)) {

        $to = date("Y-06-30");
        $from = date("Y-01-01");
        // $from = date("Y-m-d", strtotime('-3 Months'));
    } else {
        $from = date("Y-07-01");
        $to = date("Y-12-31");
    }
    $tableID = "htd";
} elseif (isset($_GET['3mma']) && $_GET['3mma'] == "on") {
    $to = $_GET['to'];
    $from = date("Y-m-01", strtotime('first day of -2 Month', strtotime($_GET['to'])));
    $tableID = "3MMA";
} elseif (isset($_GET['ftd']) && $_GET['ftd'] == "on") {
    $to = date("Y-m-d", strtotime('this week monday'));
    $from = date("Y-m-d", strtotime('-13 days', strtotime($to)));
    $tableID = "FTD";
} else {
    // $tableID = "tblInteractionNps";
    $addonStoreName = "";
    if (isset($_GET['from']) && isset($_GET['to'])) {
        $to = $_GET['to'];
        $from = $_GET['from'];
    } else {
        $from = NULL;
        $to = NULL;
    }
}

$shared = $_GET['shared'];
$role = $_GET['role'];
$empID = $_SESSION['EmpProfileID'];
$storeType = $_GET['storeType'];
//Get Employee Store Score Data
$StoreNps = array();

//echo $to . " " . $from . " <br>$tableID to" . $_GET['to'] . " from" . $_GET['from'];
//exit;                     
//

$to = date("Y-m-d");
$from = date("Y-m-01", strtotime('first day of -2 Month', strtotime($to)));

$storeNpsData = getClientNps($shared, $to, $from, 1, "Fixed", NULL);

function setColor($tier){
    
    $tier = explode(" - ", $tier);
    //print_r($tier);
    $tier = trim($tier[0]);
    if($tier == "Tier 1"){
      $color = "#00e600";
    }elseif($tier == "Tier 2"){
      $color = "#ffa500";
    }else{
      $color = "#ff0000";
    }
    return $color;        
}

//echo $to . " " . $from;

foreach ($storeNpsData['Merged'] as $storeData) {
    $StoreNps[$storeData['StoreName']] = array(
        'StoreName' => $storeData['StoreName'],
        'CleanName' => clean($storeData['StoreName']),
        //episode
        'MobileScore' => number_format($storeData['MobileNPS'], npsDecimal()),
        'MobileTier' => $storeData['MobileTier'],
        'MobileTotalSurvey' => $storeData['MobileTotalSurvey'],
        'MobileAdvocates' => $storeData['MobileAdvocates'],
        'MobilePasives' => $storeData['MobilePasives'],
        'MobileDetractors' => $storeData['MobileDetractors'],
        'MobilePoints' => $storeData['MobileTotalPoints'],
        //interaction
//        'InteractionScore' => number_format($storeData['InteractionNPS'], 0),
//        'InteractionTier' => $storeData['InteractionTier'],
//        'InteractionTotalSurvey' => $storeData['InteractionTotalSurvey'],
//        'InteractionAdvocates' => $storeData['InteractionAdvocates'],
//        'InteractionPasives' => $storeData['InteractionPasives'],
//        'InteractionDetractors' => $storeData['InteractionDetractors'],
        //fixed
        'FixedScore' => number_format($storeData['FixedNPS'], npsDecimal()),
        'FixedTier' => $storeData['FixedTier'],
        'FixedTotalSurvey' => $storeData['FixedTotalSurvey'],
        'FixedAdvocates' => $storeData['FixedAdvocates'],
        'FixedPasives' => $storeData['FixedPasives'],
        'FixedDetractors' => $storeData['FixedDetractors'],
        'FixedPoints' => $storeData['FixedTotalPoints'],
        //episode
        'EpisodeScore' => number_format($storeData['EpisodeNPS'], npsDecimal()),
        'EpisodeTier' => $storeData['EpisodeTier'],
        'EpisodeTotalSurvey' => $storeData['EpisodeTotalSurvey'],
        'EpisodeAdvocates' => $storeData['EpisodeAdvocates'],
        'EpisodePasives' => $storeData['EpisodePasives'],
        'EpisodeDetractors' => $storeData['EpisodeDetractors'],
        'PointsTotal' => $storeData['PointsTotal']
    );
}

//print_r($StoreNps);

function clean($string) {
    $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

?>
<center><h4><i>Report Range from <?php echo $from; ?> to <?php echo $to; ?></i></h4></center>

<script>
    var divList = $(".listing-item-3MMA");

    function sortMobile() {
        divList.sort(function (a, b) {
            return $(b).data("mobile") - $(a).data("mobile")
        });
        $("#sortNPS3MMA").html(divList);
        $("#sortFixed").removeClass('active');
        $("#sortMobile").addClass('active');
    }

    function sortFixed() {
        divList.sort(function (a, b) {
            return $(b).data("fixed") - $(a).data("fixed")
        });
        $("#sortNPS3MMA").html(divList);
        $("#sortMobile").removeClass('active');
        $("#sortFixed").addClass('active');
    }

</script>
<!--<center>
    <button class="btn btn-xs btn-warning active" id="sortMobile" onclick="sortMobile()">Sort by Mobile</button>
    <button class="btn btn-xs btn-info" id="sortFixed" onclick="sortFixed()">Sort by Fixed</button>
</center>-->

<?php if (count($StoreNps) > 0) { ?>
    <div class="row"  id="sortNPS3MMA" style="padding-bottom: 15px;">        
        <table class=" table table-bordered"  cellspacing="0" style=" text-align: center; vertical-align: middle; width: 92% !important; margin: 0 auto; padding-bottom: 20px;" >
            <thead>
                <tr>
                    <th>Staff</th>
                    <th width="25%"><h3>Michelle</h3></th>
                    <th width="25%"><h3>Thienna</h3></th>
                    <th width="25%"><h3>Todd</h3></th>
                    <th width="25%"><h3>Will + Day Buddy</h3></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td rowspan="4"><SPAN STYLE="padding-bottom: 200px;writing-mode: vertical-lr !important; vertical-align: middle !important;
                                          -ms-writing-mode: tb-rl !important;
                                          transform: rotate(180deg); text-align:center !important;
                                          white-space:nowrap ;">Store</SPAN>
                    </td>
                    <?php $brooksideColor = setColor($StoreNps['Brookside']['FixedTier']); ?>
                    <td bgcolor="<?php echo $brooksideColor;?>"> 
                        <div class="panel panel-default col-lg-12" style="background-color:<?php echo $brooksideColor;?>">
                            <div>
                                <h2><?php echo $StoreNps['Brookside']['StoreName']; ?></h2>
                              ( <?php echo $StoreNps['Brookside']['FixedTier']; ?> )
                            </div>
                           
                            <div class="panel-heading col-lg-4">
                                <div id="FixedT1<?php echo $StoreNps['Brookside']['CleanName']; ?>"><i class="fa fa-check"></i></div>     
                            </div>
                            <div class="panel-body panel-small col-lg-4">                                
                                <h4> <?php echo $StoreNps['Brookside']['FixedScore']; ?> </h4>                                                             
                            </div>
                            <div class="panel-footer col-lg-4">
                                <div id="FixedT2<?php echo $StoreNps['Brookside']['CleanName']; ?>"><i class="fa fa-check"></i></div>  
                            </div>
                        </div>
                    </td>
                    <?php $ipswichColor = setColor($StoreNps['TLS Ipswich']['FixedTier']); ?>
                    <td bgcolor="<?php echo $ipswichColor;?>"> 
                        <div class="panel panel-default col-lg-12" style="background-color:<?php echo $ipswichColor;?>">
                            <div>
                                <h2> <?php echo $StoreNps['TLS Ipswich']['StoreName']; ?>  </h2>
                              ( <?php echo $StoreNps['TLS Ipswich']['FixedTier']; ?> )
                            </div>                            
                            <div class="panel-heading col-lg-4">
                                <div id="FixedT1<?php echo $StoreNps['TLS Ipswich']['CleanName']; ?>"><i class="fa fa-check"></i></div>     
                            </div>
                            <div class="panel-body panel-small col-lg-4">                                
                                <h4> <?php echo $StoreNps['TLS Ipswich']['FixedScore']; ?> </h4>                                                             
                            </div>
                            <div class="panel-footer col-lg-4">
                                <div id="FixedT2<?php echo $StoreNps['TLS Ipswich']['CleanName']; ?>"><i class="fa fa-check"></i></div>  
                            </div>
                        </div>
                    </td>  
                    <?php $toowongColor = setColor($StoreNps['Toowong']['FixedTier']); ?>
                    <td bgcolor="<?php echo $toowongColor;?>"> 
                        <div class="panel panel-default col-lg-12" style="background-color:<?php echo $toowongColor;?>">
                            <div>
                                 <h2> <?php echo $StoreNps['Toowong']['StoreName']; ?>  </h2>
                             ( <?php echo $StoreNps['Toowong']['FixedTier']; ?> )
                            </div>                           
                            <div class="panel-heading col-lg-4">
                                <div id="FixedT1<?php echo $StoreNps['Toowong']['CleanName']; ?>"><i class="fa fa-check"></i></div>     
                            </div>
                            <div class="panel-body panel-small col-lg-4">                                
                                <h4> <?php echo $StoreNps['Toowong']['FixedScore']; ?> </h4>                                                             
                            </div>
                            <div class="panel-footer col-lg-4">
                                <div id="FixedT2<?php echo $StoreNps['Toowong']['CleanName']; ?>"><i class="fa fa-check"></i></div>  
                            </div>
                        </div>
                    </td> 
                    <?php $sunnybankColor = setColor($StoreNps['TLS Sunnybank']['FixedTier']); ?>
                    <td bgcolor="<?php echo $sunnybankColor;?>"> 
                        <div class="panel panel-default col-lg-12" style="background-color:<?php echo $sunnybankColor;?>">
                            <div>   
                            <h2> <?php echo $StoreNps['TLS Sunnybank']['StoreName']; ?>  </h2>
                              ( <?php echo $StoreNps['TLS Sunnybank']['FixedTier']; ?> )
                            </div>                            
                            <div class="panel-heading col-lg-4">
                                <div id="FixedT1<?php echo $StoreNps['TLS Sunnybank']['CleanName']; ?>"><i class="fa fa-check"></i></div>     
                            </div>
                            <div class="panel-body panel-small col-lg-4">                                
                                <h4> <?php echo $StoreNps['TLS Sunnybank']['FixedScore']; ?> </h4>                                                             
                            </div>
                            <div class="panel-footer col-lg-4">
                                <div id="FixedT2<?php echo $StoreNps['TLS Sunnybank']['CleanName']; ?>"><i class="fa fa-check"></i></div>  
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>                   
                    <?php $toombulColor = setColor($StoreNps['Toombul NEW']['FixedTier']); ?>
                    <td bgcolor="<?php echo $toombulColor;?>"> 
                        <div class="panel panel-default col-lg-12" style="background-color:<?php echo $toombulColor;?>">
                            <div>
                                <h2><?php echo $StoreNps['Toombul NEW']['StoreName']; ?></h2>
                              ( <?php echo $StoreNps['Toombul NEW']['FixedTier']; ?> )
                            </div>
                           
                            <div class="panel-heading col-lg-4">
                                <div id="FixedT1<?php echo $StoreNps['Toombul NEW']['CleanName']; ?>"><i class="fa fa-check"></i></div>     
                            </div>
                            <div class="panel-body panel-small col-lg-4">                                
                                <h4> <?php echo $StoreNps['Toombul NEW']['FixedScore']; ?> </h4>                                                             
                            </div>
                            <div class="panel-footer col-lg-4">
                                <div id="FixedT2<?php echo $StoreNps['Toombul NEW']['CleanName']; ?>"><i class="fa fa-check"></i></div>  
                            </div>
                        </div>
                    </td>
                    <?php $strathpineColor = setColor($StoreNps['TLS Strathpine']['FixedTier']); ?>
                    <td bgcolor="<?php echo $strathpineColor;?>"> 
                        <div class="panel panel-default col-lg-12" style="background-color:<?php echo $strathpineColor;?>">
                            <div>
                                <h2> <?php echo $StoreNps['TLS Strathpine']['StoreName']; ?>  </h2>
                              ( <?php echo $StoreNps['TLS Strathpine']['FixedTier']; ?> )
                            </div>                            
                            <div class="panel-heading col-lg-4">
                                <div id="FixedT1<?php echo $StoreNps['TLS Strathpine']['CleanName']; ?>"><i class="fa fa-check"></i></div>     
                            </div>
                            <div class="panel-body panel-small col-lg-4">                                
                                <h4> <?php echo $StoreNps['TLS Strathpine']['FixedScore']; ?> </h4>                                                             
                            </div>
                            <div class="panel-footer col-lg-4">
                                <div id="FixedT2<?php echo $StoreNps['TLS Strathpine']['CleanName']; ?>"><i class="fa fa-check"></i></div>  
                            </div>
                        </div>
                    </td>  
                    <?php $capalabaColor = setColor($StoreNps['TLS Capalaba']['FixedTier']); ?>
                    <td bgcolor="<?php echo $capalabaColor;?>"> 
                        <div class="panel panel-default col-lg-12" style="background-color:<?php echo $capalabaColor;?>">
                            <div>
                                 <h2> <?php echo $StoreNps['TLS Capalaba']['StoreName']; ?>  </h2>
                             ( <?php echo $StoreNps['TLS Capalaba']['FixedTier']; ?> )
                            </div>                           
                            <div class="panel-heading col-lg-4">
                                <div id="FixedT1<?php echo $StoreNps['TLS Capalaba']['CleanName']; ?>"><i class="fa fa-check"></i></div>     
                            </div>
                            <div class="panel-body panel-small col-lg-4">                                
                                <h4> <?php echo $StoreNps['TLS Capalaba']['FixedScore']; ?> </h4>                                                             
                            </div>
                            <div class="panel-footer col-lg-4">
                                <div id="FixedT2<?php echo $StoreNps['TLS Capalaba']['CleanName']; ?>"><i class="fa fa-check"></i></div>  
                            </div>
                        </div>
                    </td> 
                    <?php $thegapColor = setColor($StoreNps['TLS The Gap']['FixedTier']); ?>
                    <td bgcolor="<?php echo $thegapColor;?>"> 
                        <div class="panel panel-default col-lg-12" style="background-color:<?php echo $thegapColor;?>">
                            <div>   
                            <h2> <?php echo $StoreNps['TLS The Gap']['StoreName']; ?>  </h2>
                              ( <?php echo $StoreNps['TLS The Gap']['FixedTier']; ?> )
                            </div>                            
                            <div class="panel-heading col-lg-4">
                                <div id="FixedT1<?php echo $StoreNps['TLS The Gap']['CleanName']; ?>"><i class="fa fa-check"></i></div>     
                            </div>
                            <div class="panel-body panel-small col-lg-4">                                
                                <h4> <?php echo $StoreNps['TLS The Gap']['FixedScore']; ?> </h4>                                                             
                            </div>
                            <div class="panel-footer col-lg-4">
                                <div id="FixedT2<?php echo $StoreNps['TLS The Gap']['CleanName']; ?>"><i class="fa fa-check"></i></div>  
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>                   
                    <?php $cairnsColor = setColor($StoreNps['Cairns Central']['FixedTier']); ?>
                    <td bgcolor="<?php echo $cairnsColor;?>"> 
                        <div class="panel panel-default col-lg-12" style="background-color:<?php echo $cairnsColor;?>">
                            <div>
                                <h2><?php echo $StoreNps['Cairns Central']['StoreName']; ?></h2>
                              ( <?php echo $StoreNps['Cairns Central']['FixedTier']; ?> )
                            </div>
                           
                            <div class="panel-heading col-lg-4">
                                <div id="FixedT1<?php echo $StoreNps['Cairns Central']['CleanName']; ?>"><i class="fa fa-check"></i></div>     
                            </div>
                            <div class="panel-body panel-small col-lg-4">                                
                                <h4> <?php echo $StoreNps['Cairns Central']['FixedScore']; ?> </h4>                                                             
                            </div>
                            <div class="panel-footer col-lg-4">
                                <div id="FixedT2<?php echo $StoreNps['Cairns Central']['CleanName']; ?>"><i class="fa fa-check"></i></div>  
                            </div>
                        </div>
                    </td>                    
                    <?php $smithfieldColor = setColor($StoreNps['Smithfield']['FixedTier']); ?>
                    <td bgcolor="<?php echo $smithfieldColor;?>"> 
                        <div class="panel panel-default col-lg-12" style="background-color:<?php echo $smithfieldColor;?>">
                            <div>
                                 <h2> <?php echo $StoreNps['Smithfield']['StoreName']; ?>  </h2>
                             ( <?php echo $StoreNps['Smithfield']['FixedTier']; ?> )
                            </div>                           
                            <div class="panel-heading col-lg-4">
                                <div id="FixedT1<?php echo $StoreNps['Smithfield']['CleanName']; ?>"><i class="fa fa-check"></i></div>     
                            </div>
                            <div class="panel-body panel-small col-lg-4">                                
                                <h4> <?php echo $StoreNps['Smithfield']['FixedScore']; ?> </h4>                                                             
                            </div>
                            <div class="panel-footer col-lg-4">
                                <div id="FixedT2<?php echo $StoreNps['Smithfield']['CleanName']; ?>"><i class="fa fa-check"></i></div>  
                            </div>
                        </div>
                    </td> 
                    <?php $victoriaPointColor = setColor($StoreNps['TLS Victoria Point NEW']['FixedTier']); ?>
                    <td bgcolor="<?php echo $victoriaPointColor;?>"> 
                        <div class="panel panel-default col-lg-12" style="background-color:<?php echo $victoriaPointColor;?>">
                            <div>   
                            <h2> <?php echo $StoreNps['TLS Victoria Point NEW']['StoreName']; ?>  </h2>
                              ( <?php echo $StoreNps['TLS Victoria Point NEW']['FixedTier']; ?> )
                            </div>                            
                            <div class="panel-heading col-lg-4">
                                <div id="FixedT1<?php echo $StoreNps['TLS Victoria Point NEW']['CleanName']; ?>"><i class="fa fa-check"></i></div>     
                            </div>
                            <div class="panel-body panel-small col-lg-4">                                
                                <h4> <?php echo $StoreNps['TLS Victoria Point NEW']['FixedScore']; ?> </h4>                                                             
                            </div>
                            <div class="panel-footer col-lg-4">
                                <div id="FixedT2<?php echo $StoreNps['TLS Victoria Point NEW']['CleanName']; ?>"><i class="fa fa-check"></i></div>  
                            </div>
                        </div>
                    </td>
                    <?php $staffordColor = setColor($StoreNps['TLS Stafford Kiosk']['FixedTier']); ?>
                    <td bgcolor="<?php echo $staffordColor;?>"> 
                        <div class="panel panel-default col-lg-12" style="background-color:<?php echo $staffordColor;?>">
                            <div>
                                <h2> <?php echo $StoreNps['TLS Stafford Kiosk']['StoreName']; ?>  </h2>
                              ( <?php echo $StoreNps['TLS Stafford Kiosk']['FixedTier']; ?> )
                            </div>                            
                            <div class="panel-heading col-lg-4">
                                <div id="FixedT1<?php echo $StoreNps['TLS Stafford Kiosk']['CleanName']; ?>"><i class="fa fa-check"></i></div>     
                            </div>
                            <div class="panel-body panel-small col-lg-4">                                
                                <h4> <?php echo $StoreNps['TLS Stafford Kiosk']['FixedScore']; ?> </h4>                                                             
                            </div>
                            <div class="panel-footer col-lg-4">
                                <div id="FixedT2<?php echo $StoreNps['TLS Stafford Kiosk']['CleanName']; ?>"><i class="fa fa-check"></i></div>  
                            </div>
                        </div>
                    </td> 
                </tr>
                <tr>                   
                    <?php $springfieldColor = setColor($StoreNps['TLS Springfield']['FixedTier']); ?>
                    <td bgcolor="<?php echo $springfieldColor;?>"> 
                        <div class="panel panel-default col-lg-12" style="background-color:<?php echo $springfieldColor;?>">
                            <div>
                                <h2><?php echo $StoreNps['TLS Springfield']['StoreName']; ?></h2>
                              ( <?php echo $StoreNps['TLS Springfield']['FixedTier']; ?> )
                            </div>
                           
                            <div class="panel-heading col-lg-4">
                                <div id="FixedT1<?php echo $StoreNps['TLS Springfield']['CleanName']; ?>"><i class="fa fa-check"></i></div>     
                            </div>
                            <div class="panel-body panel-small col-lg-4">                                
                                <h4> <?php echo $StoreNps['TLS Springfield']['FixedScore']; ?> </h4>                                                             
                            </div>
                            <div class="panel-footer col-lg-4">
                                <div id="FixedT2<?php echo $StoreNps['TLS Springfield']['CleanName']; ?>"><i class="fa fa-check"></i></div>  
                            </div>
                        </div>
                    </td>
                    <?php $redbankColor = setColor($StoreNps['TLS Redbank']['FixedTier']); ?>
                    <td bgcolor="<?php echo $redbankColor;?>"> 
                        <div class="panel panel-default col-lg-12" style="background-color:<?php echo $redbankColor;?>">
                            <div>
                                <h2> <?php echo $StoreNps['TLS Redbank']['StoreName']; ?>  </h2>
                              ( <?php echo $StoreNps['TLS Redbank']['FixedTier']; ?> )
                            </div>                            
                            <div class="panel-heading col-lg-4">
                                <div id="FixedT1<?php echo $StoreNps['TLS Redbank']['CleanName']; ?>"><i class="fa fa-check"></i></div>     
                            </div>
                            <div class="panel-body panel-small col-lg-4">                                
                                <h4> <?php echo $StoreNps['TLS Redbank']['FixedScore']; ?> </h4>                                                             
                            </div>
                            <div class="panel-footer col-lg-4">
                                <div id="FixedT2<?php echo $StoreNps['TLS Redbank']['CleanName']; ?>"><i class="fa fa-check"></i></div>  
                            </div>
                        </div>
                    </td>  
                    <?php $jimboombaColor = setColor($StoreNps['TLS Jimboomba']['FixedTier']); ?>
                    <td bgcolor="<?php echo $jimboombaColor;?>"> 
                        <div class="panel panel-default col-lg-12" style="background-color:<?php echo $jimboombaColor;?>">
                            <div>
                                 <h2> <?php echo $StoreNps['TLS Jimboomba']['StoreName']; ?>  </h2>
                             ( <?php echo $StoreNps['TLS Jimboomba']['FixedTier']; ?> )
                            </div>                           
                            <div class="panel-heading col-lg-4">
                                <div id="FixedT1<?php echo $StoreNps['TLS Jimboomba']['CleanName']; ?>"><i class="fa fa-check"></i></div>     
                            </div>
                            <div class="panel-body panel-small col-lg-4">                                
                                <h4> <?php echo $StoreNps['TLS Jimboomba']['FixedScore']; ?> </h4>                                                             
                            </div>
                            <div class="panel-footer col-lg-4">
                                <div id="FixedT2<?php echo $StoreNps['TLS Jimboomba']['CleanName']; ?>"><i class="fa fa-check"></i></div>  
                            </div>
                        </div>
                    </td> 
                    <?php $beenleighColor = setColor($StoreNps['TLS Beenleigh']['FixedTier']); ?>
                    <td bgcolor="<?php echo $beenleighColor;?>"> 
                        <div class="panel panel-default col-lg-12" style="background-color:<?php echo $beenleighColor;?>">
                            <div>   
                            <h2> <?php echo $StoreNps['TLS Beenleigh']['StoreName']; ?>  </h2>
                              ( <?php echo $StoreNps['TLS Beenleigh']['FixedTier']; ?> )
                            </div>                            
                            <div class="panel-heading col-lg-4">
                                <div id="FixedT1<?php echo $StoreNps['TLS Beenleigh']['CleanName']; ?>"><i class="fa fa-check"></i></div>     
                            </div>
                            <div class="panel-body panel-small col-lg-4">                                
                                <h4> <?php echo $StoreNps['TLS Beenleigh']['FixedScore']; ?> </h4>                                                             
                            </div>
                            <div class="panel-footer col-lg-4">
                                <div id="FixedT2<?php echo $StoreNps['TLS Beenleigh']['CleanName']; ?>"><i class="fa fa-check"></i></div>  
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

<?php
        foreach ($StoreNps as $value) {

            if (is_null($value['MobileTotalSurvey']) || empty($value['MobileTotalSurvey']) || $value['MobileTotalSurvey'] == 'nan' || $value['MobileTotalSurvey'] == '') {

                $value['MobileTotalSurvey'] = 0;
                $value['MobileAdvocates'] = 0;
                $value['MobileDetractors'] = 0;
            }
            if (is_null($value['FixedTotalSurvey']) || empty($value['FixedTotalSurvey']) || $value['FixedTotalSurvey'] == 'nan' || $value['FixedTotalSurvey'] == '') {

                $value['FixedTotalSurvey'] = 0;
                $value['FixedAdvocates'] = 0;
                $value['FixedDetractors'] = 0;
            }
            if (is_null($value['EpisodeTotalSurvey'])) {

                $value['EpisodeTotalSurvey'] = 0;
                $value['EpisodeAdvocates'] = 0;
                $value['EpisodeDetractors'] = 0;
            }

              //  echo $value['StoreName'];
                if (isset($_GET['htd']) && $_GET['htd'] == "on") {
                    echo " (<i>{$value['PointsTotal']} Points</i>)";
                }
                // $storeName = str_replace(' ', '', $value['StoreName']);
                $storeName = clean($value['StoreName']);
                // echo $storeName;
                //  $storeName = str_replace('(', '', $storeName);
                // $storeName = str_replace(')', '', $storeName);
                ?>
                           
        <!--                    <script type="text/javascript">
                        $(document).ready(function() {

                            var NewTotalSurvey2<?php echo $storeName; ?> = <?php echo $value['MobileTotalSurvey']; ?>;
                            var newInterAdvoTotal2<?php echo $storeName; ?> = <?php echo $value['MobileAdvocates']; ?>;
                            var InteractionTotalDetractorTotal2<?php echo $storeName; ?> = <?php echo $value['MobileDetractors']; ?>;
                            var counter2<?php echo $storeName; ?> = 0;
                            var newDetPercentage2<?php echo $storeName; ?> = 0;
                            var newAdvoPercentage2<?php echo $storeName; ?> = 0;
                            var newIntercationNPS2<?php echo $storeName; ?> = 0;
                            var roundedstoreNPS = 0;
                            while (newIntercationNPS2<?php echo $storeName; ?> < 74) {
                                counter2<?php echo $storeName; ?> += 1;
                                newAdvoPercentage2<?php echo $storeName; ?> = (newInterAdvoTotal2<?php echo $storeName; ?> / NewTotalSurvey2<?php echo $storeName; ?>) * 100;
                                newDetPercentage2<?php echo $storeName; ?> = (InteractionTotalDetractorTotal2<?php echo $storeName; ?> / NewTotalSurvey2<?php echo $storeName; ?>) * 100;
                                newIntercationNPS2<?php echo $storeName; ?> = newAdvoPercentage2<?php echo $storeName; ?> - newDetPercentage2<?php echo $storeName; ?>;
                                roundedstoreNPS = newIntercationNPS2<?php echo $storeName; ?>;
                                roundedstoreNPS = roundedstoreNPS.toFixed(1);
                                if (roundedstoreNPS < 51) {
                                    $("#MobileT5<?php echo $storeName; ?>").empty();
                                    $("#MobileT5<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                    //        console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
                                } else if (roundedstoreNPS >= 51 && roundedstoreNPS < 61) {
                                    $("#MobileT4<?php echo $storeName; ?>").empty();
                                    $("#MobileT4<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                    //        console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
                                } else if (roundedstoreNPS >= 61 && roundedstoreNPS < 66) {
                                    $("#MobileT3<?php echo $storeName; ?>").empty();
                                    $("#MobileT3<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                    //        console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
                                } else if (roundedstoreNPS >= 66 && roundedstoreNPS < 70) {
                                    $("#MobileT2<?php echo $storeName; ?>").empty();
                                    $("#MobileT2<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                    ;
                                    //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 2");
                                } else if (roundedstoreNPS => 70 && roundedstoreNPS < 74) {
                                    $("#MobileT1<?php echo $storeName; ?>").empty();
                                    $("#MobileT1<?php echo $storeName; ?>").append(counter2<?php echo $storeName; ?>);
                                    //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 1");
                                }
                                newInterAdvoTotal2<?php echo $storeName; ?> += 1;
                                NewTotalSurvey2<?php echo $storeName; ?> += 1;
                            }
                            //    var needed = counter - newInterAdvoTotal;

                        });
                    </script>-->

                    <script type="text/javascript">
                        $(document).ready(function () {

                            var NewTotalSurveyFix<?php echo $storeName; ?> = <?php echo $value['FixedTotalSurvey']; ?>;
                            var newInterAdvoTotalFix<?php echo $storeName; ?> = <?php echo $value['FixedAdvocates']; ?>;
                            var FixedTotalDetractorTotalFix<?php echo $storeName; ?> = <?php echo $value['FixedDetractors']; ?>;
                            var counterFix<?php echo $storeName; ?> = 0;
                            var newDetPercentageFix<?php echo $storeName; ?> = 0;
                            var newAdvoPercentageFix<?php echo $storeName; ?> = 0;
                            var newIntercationNPSFix<?php echo $storeName; ?> = 0;
                            var roundedstoreNPS = 0;
                            var ft4 = true;
                            var ft3 = true;
                            var ft2 = true;
                            var ft1 = true;
                            newAdvoPercentageFix<?php echo $storeName; ?> = (newInterAdvoTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                            newDetPercentageFix<?php echo $storeName; ?> = (FixedTotalDetractorTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                            newIntercationNPSFix<?php echo $storeName; ?> = newAdvoPercentageFix<?php echo $storeName; ?> - newDetPercentageFix<?php echo $storeName; ?>;
                            //console.log(newIntercationNPSFix<?php echo $storeName; ?> + 'current');
                            while (newIntercationNPSFix<?php echo $storeName; ?> < 60) {

                                counterFix<?php echo $storeName; ?> += 1;

                                newAdvoPercentageFix<?php echo $storeName; ?> = (newInterAdvoTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                                newDetPercentageFix<?php echo $storeName; ?> = (FixedTotalDetractorTotalFix<?php echo $storeName; ?> / NewTotalSurveyFix<?php echo $storeName; ?>) * 100;
                                newIntercationNPSFix<?php echo $storeName; ?> = newAdvoPercentageFix<?php echo $storeName; ?> - newDetPercentageFix<?php echo $storeName; ?>;
                                roundedstoreNPS = newIntercationNPSFix<?php echo $storeName; ?>;
                                roundedstoreNPS = roundedstoreNPS.toFixed(1);
                                // console.log('Store Name: <?php echo $storeName; ?> '+counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier Int")
                                if (roundedstoreNPS < 23) {
                                    $("#FixedT5<?php echo $storeName; ?>").empty();
                                    $("#FixedT5<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                    ft4 = false;
                                    //  console.log(counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier 4");
                                } else if (roundedstoreNPS >= 23 && roundedstoreNPS < 41) {
                                    $("#FixedT4<?php echo $storeName; ?>").empty();
                                    $("#FixedT4<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                    ft3 = false;
                                    //console.log(counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier 3");
                                } else if (roundedstoreNPS >= 41 && roundedstoreNPS < 47) {
                                    $("#FixedT3<?php echo $storeName; ?>").empty();
                                    $("#FixedT3<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                    ft3 = false;
                                    //console.log(counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier 3");
                                } else if (roundedstoreNPS >= 47 && roundedstoreNPS < 53) {
                                    $("#FixedT2<?php echo $storeName; ?>").empty();
                                    $("#FixedT2<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                    ft2 = false;
                                    //   console.log(counterFix<?php echo $storeName; ?> + " NPS: "+ newIntercationNPSFix<?php echo $storeName; ?> +" for tier Int");
                                } else if (roundedstoreNPS >= 53 && roundedstoreNPS < 60) {
                                    $("#FixedT1<?php echo $storeName; ?>").empty();
                                    $("#FixedT1<?php echo $storeName; ?>").append(counterFix<?php echo $storeName; ?>);
                                    ft1 = false;
                                    console.log(counterFix<?php echo $storeName; ?> + " NPS: " + newIntercationNPSFix<?php echo $storeName; ?> + " for tier 1");
                                }

                                newInterAdvoTotalFix<?php echo $storeName; ?> += 1;
                                NewTotalSurveyFix<?php echo $storeName; ?> += 1;
                            }

                        });
                    </script>
        <?php } ?>

<?php } else { ?>
    No Records yet.
<?php } ?>
