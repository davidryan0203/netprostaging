<div class="container">
    <?php
    $sublink = $_GET['o'];
   

    if ($_SESSION['HasStoreType'] == 'Only Others') {
        include('dashboard/indexlocalOthers.php');
    } else {
        switch ($sublink) {
            case 1:
                include('dashboard/index.php');
                break;
            case 2:
                include('dashboard/indexlocal.php');
                break;
            case 4:
                include('dashboard/others.php');
                break;
            default:
                if ($_SESSION['HasStoreType'] == 'Only Others') {
                    include('dashboard/indexlocalOthers.php');
                } else {
                    include('dashboard/indexlocal.php');
                }
                break;
        }
    }
    ?>
</div>

