<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include('DataReconFunction.php');
$getData = array();
$getData = $_GET;
$toDo = $getData['module'];
switch ($toDo) {

    case 'UpdateBlankPnumber':
        $updateBlankPnumber = UpdateBlankPnumber($getData); //call function
        if ($updateBlankPnumber['message'] == 'Success Updating PNumber') { //check function return success message
            $result['type'] = 'Success';  //return type message error or success
        } else {
            $result['type'] = 'Error';
        }
        $result['message'] = $updateBlankPnumber['message']; //return message
        break;
        
     case 'GetEmployeeList':
        $storeID = $getData['storeID'];
        $result = getEmpList($storeID);
        break;
    
    case 'CreateDataRecon':
        $createDataRecon = CreateDataRecon($getData);       
        if ($createDataRecon['message'] == 'Success Creating Data Recon') { // check if called create store function is success
            $getData['UserLoginID'] = $createDataRecon['UserLoginID'];
            if (isset($getData['includeAssigned'])) { // now check if get data for shared store has data
                $createSharedStore = CreateAssignTo($getData);  // if shared store has data call create shared store function
                $result['message'] = $createDataRecon['message'] . "<br>" . $createSharedStore['message']; // return result of create shared store function
            } else {
                $result['message'] = $createDataRecon['message'];
            }
            $result['type'] = 'Success';
        } else {
            $result['message'] = $createDataRecon['message'];
            $result['type'] = 'Error';
        }
        break;

    default:
        $result['type'] = 'Error';
        $result['message'] = 'Unknown process';
}

mysql_close($connection); // close database connection
echo json_encode($result);  // return to ajax result json_encode($result)