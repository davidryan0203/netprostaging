<?php

//include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');
//include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/UserFunction.php');
//include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/EmployeeFunction.php');

include($_SERVER['DOCUMENT_ROOT'] . '/datarecon/functions/DataReconFunction.php');
session_start();
$role = $_SESSION['UserRoleID'];
date_default_timezone_set('Australia/Melbourne');

$storeOwnerID = $_GET['ownerID'];
$invitationID = $_GET['invitationID'];
$dateCreated = $_GET['dateCreated'];
$SDID = $_GET['SDID'];
$storeID = $_GET['storeID'];


$history = getSurveyHistory($invitationID, $dateCreated);

if (!is_null($history)) {
    $i = 0;
    foreach ($history as $value) {
        if ($i > 0) {
            $br = "<br>";
        } else {
            $br = "";
        }
        echo "$br<b>Modified By: </b><i>" . $value['ModifiedBy'] . "</i><br>";
        $date = new DateTime($value['DateLogCreated']);
        $date->setTimezone(new DateTimeZone('Australia/Melbourne')); // +04
        $LogRemark = json_decode($value['LogRemarks'], true);
        echo "<b>Date: </b><i>" . $date->format('d-M-y (h:i A)') . "</i><br>";
        echo "<b>Assigned to PNumber: </b><i>" . $LogRemark['Pnumber'] . "</i><br>";
        $sqlGetStaffName = "select d.f_UserName from t_storelist a
                                left join t_empnumber b on a.f_StoreID = b.f_StoreID
                                left join t_emplist c on b.f_EmpID = c.f_EmpID
                                left join t_userlogin d on c.f_EmpID = d.f_EmpID
                                WHERE a.f_StoreOwnerUserID = {$storeOwnerID} AND b.f_EmpPnumber = '{$LogRemark['Pnumber']}' ";
        $result = mysql_query($sqlGetStaffName, $connection);
        $row = mysql_fetch_assoc($result);
        echo "<b>Assigned to: </b><i> " . $row['f_UserName'] . "</i></br>";
        if (isset($LogRemark['OriginalPNumber'])) {
            echo "<b>Previous Assigned PNumber: </b><i> " . $LogRemark['OriginalPNumber'] . "</i></br>";

            $sqlGetStaffName = "select d.f_UserName from t_storelist a
                                left join t_empnumber b on a.f_StoreID = b.f_StoreID
                                left join t_emplist c on b.f_EmpID = c.f_EmpID
                                left join t_userlogin d on c.f_EmpID = d.f_EmpID
                                WHERE a.f_StoreOwnerUserID = {$storeOwnerID} AND b.f_EmpPnumber = '{$LogRemark['OriginalPNumber']}' ";
            $result = mysql_query($sqlGetStaffName, $connection);
            $row = mysql_fetch_assoc($result);
            echo "<b>Previosly Assigned to: </b><i> " . $row['f_UserName'] . "</i></br>";
        }
        $i++;
    }
} else {
    echo "<b><i>No Modifications</i></b>";
}

//print_r($history);
?>
