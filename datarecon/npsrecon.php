<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include('admin/function/UserFunction.php');
include('datarecon/functions/DataReconFunction.php');
include('function/NPSFunction.php');
session_start();
error_reporting(0);
$role = $_SESSION['UserRoleID'];
date_default_timezone_set('America/Denver');
ini_set('max_execution_time', 300);
$first_day_this_month = date('01/m/Y'); // hard-coded '01' for first day
$last_day_this_month = date('t/m/Y');

$date = date('M-y');

error_reporting(E_ALL);
session_start();
$getStoreOwnerID = getStoreOwner();

if ($getStoreOwnerID == 'ALL') {
    $userParams = 'Owner';
} else {
    $userParams = $getStoreOwnerID;
}
$storeList = SelectUser($userParams);
?>
<script type="text/javascript">

    $(document).ready(function () {
        var from = $("#from").val();
        var to = $("#to").val();
        var role = $("#ShareType").attr('role');

        var shared = "";
        var storeType = "";
        var toURL = "";
        $(".toggle:contains('MTD')").attr('id', 'hideMTD');

        function setLoad() {
            var mtdFrom = $("#mtdfrom").val();
            var mtdTo = $("#mtdto").val();
            var mtdSet = '';
            var reconOwnerID = $("#reconOwnerID").val();

            var toTBC = $("#TBC").val();

            shared = 'no';

            if ($('#StoreType').prop('checked')) {

                storeType = 'yes';
                toURL = "nps/summarycloudTBC.php?shared=";
                $('#tlsPicker').hide();
                $('#tbcPicker').show();
                $('#hideMTD').show();

                if ($('#MtdType').prop('checked')) {
                    $('#tbcPicker').hide();
                    $('#mtdPicker').show();
                    mtdSet = '&mtdSet=yes';
                } else {
                    $('#mtdPicker').hide();
                    $('#tbcPicker').show();
                    mtdSet = '';
                }

            } else {

                storeType = 'no';
                toURL = "nps/summarycloud.php?shared=";
                $('#tlsPicker').show();
                $('#mtdPicker').hide();
                $('#tbcPicker').hide();
                $('#hideMTD').hide();

            }


            var toLoad = toURL + shared + '&to=' + to + '&from=' + from + '&role=' + role + '&storeType=' + storeType + '&toTBC=' + toTBC + '&mtdTo=' + mtdTo + '&mtdFrom=' + mtdFrom + mtdSet + '&reconOwnerID=' + reconOwnerID;
            // console.log(toLoad);
            return toLoad;
        }

        $("#toTBC").datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'M-y',
            onClose: function (dateText, inst) {
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('option', 'defaultDate', new Date(year, month, 1));
                $(this).datepicker('setDate', new Date(year, month, 1));
                //  console.log($(this).val());
            }
        });

        $("#from").datepicker({
            showOn: "button",
            buttonImage: "https://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
            buttonImageOnly: true, dateFormat: 'dd/mm/yy',
            numberOfMonths: 2,
            onSelect: function (selected) {
                $("#to").datepicker("option", "minDate", selected);
            }
        });

        $("#to").datepicker({
            showOn: "button",
            buttonImage: "https://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
            buttonImageOnly: true, dateFormat: 'dd/mm/yy',
            numberOfMonths: 2,
            onSelect: function (selected) {
                $("#from").datepicker("option", "maxDate", selected);
            }
        });


        $("#mtdfrom").datepicker({
            showOn: "button",
            buttonImage: "https://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
            buttonImageOnly: true, dateFormat: 'dd/mm/yy',
            numberOfMonths: 2,
            onSelect: function (selected) {
                $("#mtdto").datepicker("option", "minDate", selected);
            }
        });

        $("#mtdto").datepicker({
            showOn: "button",
            buttonImage: "https://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
            buttonImageOnly: true, dateFormat: 'dd/mm/yy',
            numberOfMonths: 2,
            onSelect: function (selected) {
                $("#mtdfrom").datepicker("option", "maxDate", selected);
            }
        });


        $('#refresh').click(function () {
            var toLoad = setLoad();
            $('#myDiv').load(toLoad);
            console.log(toLoad);
        });

        $('#ShareType').change(function () {
            var toLoad = setLoad();
            $('#myDiv').load(toLoad);
        });

        $('#StoreType').change(function () {
            var toLoad = setLoad();
            $('#myDiv').load(toLoad);
        });

        $('#MtdType').change(function () {

            if ($('#MtdType').prop('checked')) {

                $('#mtdPicker').show();
                $('#tbcPicker').hide();
            } else {
                $('#mtdPicker').hide();
                $('#tbcPicker').show();
                $('#MtdType').hide();

            }
        });


        var toLoad = setLoad();
        $('#myDiv').load(toLoad);


    });

    $(function () {
        $('[data-tooltip="tooltip"]').tooltip();
    });
</script>

<div class="row">
    <div class="col-lg-12">
        <h4 class="page-header"><i class="fa fa-comments fa-fw"></i>Data Recon</h4>
        <div class="filters" >

            <?php
            $withSearch = array('1', '6');
            if (in_array($role, $withSearch)) {
                ?>
                <select name="reconOwnerID" class="input-sm" id="reconOwnerID">
                    <option value="">Select store director</option>
                    <?php
                    foreach ($storeList as $value) {
                        echo '<option value="' . $value['f_UserID'] . '">' . $value['f_UserFirstName'] . ' ' . $value['f_UserLastName'] . '</option>';
                    }
                    ?>
                </select> 
            <?php } else { ?> 
                <input type="hidden" name="reconOwnerID" id="reconOwnerID" value="<?php echo $getStoreOwnerID; ?>" />
            <?php } ?>  
            <w id="tlsPicker"> <input type="text" id="from" name="from" class="form-control" value="<?php echo $first_day_this_month; ?>" readonly="true"/>
                <input type="text" id="to" name="to"  class="form-control" value="<?php echo $last_day_this_month; ?>" readonly="true"/> </w> 
            <w id="mtdPicker"> <input type="text" id="mtdfrom" name="mtdfrom" class="form-control" value="<?php echo $first_day_this_month; ?>" readonly="true"/>
                <input type="text" id="mtdto" name="mtdto"  class="form-control" value="<?php echo $last_day_this_month; ?>" readonly="true"/> </w> 
            <w id="tbcPicker"> <input type="text" id="toTBC" name="toTBC"  class="form-control" value="<?php echo $date; ?>" readonly="true"/> </w> 
            <input type="checkbox" id="StoreType" name="StoreType" class="StoreType" data-toggle="toggle" data-style="ios"  data-onstyle="info"  
                   data-offstyle="success" data-on="TBC" data-off="TLS" />   
            <input type="checkbox" id="MtdType" name="MtdType" class="MtdType" data-toggle="toggle" data-style="ios"  data-onstyle="success"  
                   data-offstyle="warning" data-on="MTD" data-off="Default" />
            <button type="submit" class="btn btn-warning btn-circle" id="refresh" data-tooltip="tooltip" data-placement="bottom" title="Reload"> 
                <i class="fa fa-refresh"></i> 
                </br>



        </div>
    </div>   
</div>
<div  id="myDiv" class="row myDiv" ></div>

