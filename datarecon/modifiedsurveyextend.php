<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/datarecon/functions/DataReconFunction.php');
ini_set('max_execution_time', 300);
date_default_timezone_set('Australia/Melbourne');

session_start();
$from = $_GET['from'];
$to = $_GET['to'];
$storeOwnerID = $_GET['storeOwnerID'];

//print_r($_GET);
//exit;
$data = getModifiedSurvey($from, $to, $storeOwnerID);
?>


<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#example').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            columnDefs: [
//                {sortable: false, targets: -1},
                //  {"width": "8%", "targets": -1},
//                {"targets": [5,6,7],"visible": false}
            ],
            colReorder: {
                fixedColumnsRight: 1
            },
            "iDisplayLength": 15,
            "LengthMenu": [[15, 50, 100, -1], [15, 50, 100, "All"]],
            //"bStateSave" : true,
            "oColVis": {
                "buttonText": "Show Columns"
            },
            "bAutoWidth": false,
            stateSave: true
        });
    });

    $(function() {
        $('[data-tooltip="tooltip"]').tooltip();
    })
</script>

<div class="tab-content" >
    <div class="table-responsive">
        <table id="example" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
            <thead>
                <tr>
                    <th></th>
                    <th  style="min-width:80px;">Invitation ID</th>
                    <th>Details</th>
                    <th>Modified</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($data as $value) { ?>
                    <tr>
                        <td>
                            <a class="btn-sm btn-hover btn-warning editPNumber editcls" data-toggle="modal"
                               data-tooltip="tooltip" data-placement="bottom" title="Assign survey"
                               href="datarecon/modal/EditPnumberModal.php?invitationID=<?php echo $value['InivitationID']; ?>&SDID=<?php echo $value['Details']['f_SurveySDID']; ?>&storeID=<?php echo $value['Details']['f_StoreID']; ?>"
                               data-target="#EditPnumberModal" onclick="openEditPnumberModal(this)">
                                <span class="glyphicon glyphicon-cog" ></span>
                            </a>
                        </td>
                        <td>
                            <?php echo $value['InivitationID']; ?>
                        </td>
                        <td>
                            <b> Survey From: </b> <i> <?php echo $value['Details']['f_DealerCode']; ?> </i>
                            <b><br>Assigned To Pnumber: </b> <i> <?php echo $value['Details']['f_ActivationExtra5']; ?> </i>
                            <b> <br>Employee Name: </b> <i> <?php echo $value['Details']['EmpName']; ?> </i>


                        </td>
                        <td><b>By: </b><i><?php
                                echo $value['ModifiedBy'] . "</i><Br> <b>Date:</b> <i>";
                                $date = new DateTime($value['ModifiedDate']);
                                $date->setTimezone(new DateTimeZone('Australia/Melbourne')); // +04
                                echo $date->format('d-M-y (h:i A)');
                                ?></i></td>

                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<!-- Modal EditUserModal-->
<div class="modal fade" id="EditPnumberModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal  EditUserModal -->























