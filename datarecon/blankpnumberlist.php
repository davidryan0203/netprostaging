<?php
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
require($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');

ini_set('max_execution_time', 500);
date_default_timezone_set('Australia/Melbourne');

$sharedType = $_GET['shared'];
$details['to'] = date("Y-m-d", strtotime($_GET['to']));
$details['from'] = date("Y-m-d", strtotime($_GET['from']));
$details['storeOwnerID'] = mysql_real_escape_string($_GET['storeOwnerID']);
$first_day_this_month = date('01/m/Y'); // hard-coded '01' for first day
$last_day_this_month = date('t/m/Y');
//$storeIDsql = " AND d.f_StoreOwnerUserID = '{$storeOwnerID}' ";
//$storeEmpOwnerID = " AND b.f_StoreOwnerUserID = '{$storeOwnerID}' ";


$data = GetBlankPnumberList($details);



if (($details['to'] == $last_day_this_month) && ($details['from'] == $first_day_this_month)) {
    $dateNow = "Current Month";
} else {
    $dateNow = $from . " - " . $to;
}


/* $sqlGetBlankPNumber = "select a.f_SurveyPDID,b.f_SurveySDID, a.f_InvitationID,a.f_EpisodeReferenceNo , b.f_StoreID,
  a.f_CustomerDetailsID, DATE_FORMAT(b.f_DateTimeResponseMelbourne,'%d/%m/%Y') as DateTime ,
  b.f_ActivationExtra5,concat(c.f_CustomerFirstName, ' ' , c.f_CustomerLastName) as CustomerName ,
  c.f_CustomerMobile,c.f_CustomerEmail , d.f_StoreListName ,d.f_StoreOwnerUserID from t_surveypd a
  left join t_surveysd b on a.f_SurveyPDID = b.f_SurveyPDID
  left join t_customerdetails c on a.f_CustomerDetailsID = c.f_CustomerDetailsID
  left join t_storelist d on b.f_StoreID = d.f_StoreID
  WHERE b.f_ActivationExtra5 in (NULL,'','DUMMY','RCUST40','SR000526','TBS_EAS00','TELESALES_SSC','RCUST40','SR00526','TXV01-BUSINESS') AND b.f_DateTimeResponseMelbourne
 * BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' $storeIDsql";
 */

//mysql_query("SELECT 1=1", $connection);
//$sqlGetBlankPNumber = "SELECT
//    a.f_SurveyPDID,
//    b.f_SurveySDID,
//    a.f_InvitationID,
//    a.f_EpisodeReferenceNo,
//    b.f_StoreID,
//    a.f_CustomerDetailsID,
//    DATE_FORMAT(b.f_DateTimeResponseMelbourne,
//            '%d/%m/%Y') AS DateTime,
//    b.f_ActivationExtra5,
//    CONCAT(c.f_CustomerFirstName,
//            ' ',
//            a.f_CustomerBillingAccountID) AS CustomerName,
//       d.f_StoreListName,
//    d.f_StoreOwnerUserID,a.f_EpisodeReferenceNo as EpisodeNum
//FROM
//    t_surveypd a
//        LEFT JOIN
//    t_surveysd b ON a.f_SurveyPDID = b.f_SurveyPDID
//        LEFT JOIN
//    t_customerdetails c ON a.f_CustomerDetailsID = c.f_CustomerDetailsID
//        LEFT JOIN
//    t_storelist d ON b.f_StoreID = d.f_StoreID
//WHERE
//    b.f_ActivationExtra5 NOT IN (SELECT
//            f_EmpPNumber
//        FROM
//            t_empnumber a
//                LEFT JOIN
//            t_storelist b ON a.f_StoreID = b.f_StoreID
//        WHERE
//          1 = 1  $storeEmpOwnerID ) AND b.f_DateTimeResponseMelbourne
//       BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' $storeIDsql ";
//$result = mysql_query($sqlGetBlankPNumber, $connection);
//while ($row = mysql_fetch_assoc($result)) {
//    $data[] = $row;
//}
//echo $sqlGetBlankPNumber;
?>



<script type="text/javascript">
    $(document).ready(function() {


        var table = $('#example').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            columnDefs: [
//                {sortable: false, targets: -1},
                {"width": "8%", "targets": -1},
//                {"targets": [5,6,7],"visible": false}
            ],
            colReorder: {
                fixedColumnsRight: 1
            },
            "iDisplayLength": 15,
            "LengthMenu": [[15, 50, 100, -1], [15, 50, 100, "All"]],
            //"bStateSave" : true,
            "oColVis": {
                "buttonText": "Show Columns"
            },
            "bAutoWidth": false,
            stateSave: true
        });
    });

    $(function() {
        $('[data-tooltip="tooltip"]').tooltip();
    })
</script>



<div class="tab-content" >
    <div class="table-responsive">
        <table id="example" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
            <thead>
                <tr>
                    <th></th>
                    <th>Survey ID</th>
                    <th style="min-width:130px;">Customer</th>
                    <th>Episode Reference #</th>
                    <th>Store</th>
                    <th>Interaction Log </th>
                    <th>Date</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($data as $value) { ?>
                    <tr>
                        <td>
                            <a class="btn-sm btn-hover btn-warning editPNumber editcls" data-toggle="modal"
                               data-tooltip="tooltip" data-placement="bottom" title="Assign survey"
                               href="datarecon/modal/EditPnumberModal.php?invitationID=<?php echo $value['f_InvitationID']; ?>&storeOwnderID=<?php echo $value['f_StoreOwnerUserID']; ?>&SDID=<?php echo $value['f_SurveySDID']; ?>&storeID=<?php echo $value['f_StoreID']; ?>"
                               data-target="#EditPnumberModal" onclick="openEditPnumberModal(this)">
                                <span class="glyphicon glyphicon-cog" ></span>
                            </a>
                        </td>
                        <td>
                            <?php echo $value['f_InvitationID'];
                            ?>
                        </td>
                        <td>
                            <?php echo $value['CustomerName'];
                            ?>
                        </td>
                        <td>
                            <?php echo $value['EpisodeNum'];
                            ?>
                        </td>
                        <td><?php echo $value['f_StoreListName'];
                            ?></td>
                        <td>
                            <?php
                            $Pnumber = '';
                            $toUpdate = '';
                            $staffNames = '';
                            $forceUpdate = '';
                            $InteractionLog = $value['f_InteractionLog'];
                            $start = 'Order Userid : ';
                            $end = ';';

                            $pattern = sprintf(
                                    '/%s(.+?)%s/ims', preg_quote($start, '/'), preg_quote($end, '/')
                            );

                            if (preg_match($pattern, $InteractionLog . $end, $matches)) {
                                list(, $match) = $matches;
                                $fromLog = "<b>Order Userid :" . $match . "</b>";
                            } else {

                                $startStaff = 'Staff_Name:';
                                $endStaff = ',';
                                $patternStaff = sprintf(
                                        '/%s(.+?)%s/ims', preg_quote($startStaff, '/'), preg_quote($endStaff, '/')
                                );

                                if (preg_match($patternStaff, $InteractionLog . $endStaff, $staffNames)) {
                                    list(, $staffname) = $staffNames;
                                    $fromLog = "<b>Staff_Name: " . trim(($staffname)) . "</b>";
                                    $staffNames = strtolower(trim($staffname));

                                    $sqlGetPnumber = "select b.f_EmpPNumber from t_emplist a
                                        left join t_empnumber b on a.f_EmpID = b.f_EmpID
                                        where LOWER(Concat(f_EmpFirstName,' ',f_EmpLastName)) = '{$staffNames}'
                                        and f_StoreID = '{$value['f_StoreID']}'";
                                    $resultGetPnumber = mysql_query($sqlGetPnumber, $connection);
                                    while ($rowPnumber = mysql_fetch_assoc($resultGetPnumber)) {
                                        $Pnumber = $rowPnumber['f_EmpPNumber'];
                                    }
                                    // echo $Pnumber;
//
                                    if ($Pnumber != '' && !is_null($Pnumber)) {
                                        $SDID = mysql_real_escape_string($value['f_SurveySDID']);
                                        $invitationID = mysql_real_escape_string($value['f_InvitationID']);
                                        $PNumber = mysql_real_escape_string($Pnumber);


                                        $sqlUpdateBlankPnumber = "UPDATE t_surveysd SET f_ActivationExtra5 = '{$PNumber}' WHERE f_SurveySDID = '{$SDID}' ";
                                        $result = mysql_query($sqlUpdateBlankPnumber, $connection);  //run sql query

                                        if ($result && mysql_affected_rows() == 1) { //check if success update
                                            $resultUpdateBlankPnumber['message'] = 'Success Updating PNumber';
                                            $logMessageArray = json_encode(array('SurveySDID' => $SDID, 'InvitationID' => $invitationID, 'Pnumber' => $PNumber));
                                            InsertLog('3', '2', $logMessageArray, $_SESSION['LoginID'], 'Success', '6');
                                            $forceUpdate = " <b><i>Staff Name detected: done updating survey to staff PNumber {$PNumber}</i></b>";
                                        }


                                        //print_r($logMessageArray);
                                        // InsertLog('3', '2', $logMessageArray, $_SESSION['LoginID'], 'Success', '6');
                                    }
                                }
                            }
                            echo $InteractionLog . "<br>" . $fromLog . "<br>" . $forceUpdate;
                            ?>
                        </td>

                        <td> <?php echo $value['DateTime']; ?> </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<!-- Modal EditUserModal-->
<div class="modal fade" id="EditPnumberModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal  EditUserModal -->






















