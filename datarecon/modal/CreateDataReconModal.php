<?php
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/UserFunction.php');
session_start();


$storeList = SelectUser('Owner');

?>
<script type="text/javascript">

    $(function () { 
        $("#ajaxmsgs").hide();
  
    
     $('select').on('change', function (e) {
         
            $('.CreateShare').find('option').prop('disabled', false);
            $('.CreateShare').each(function () {
                
                $('.CreateShare').not(this).find('option[value="' + this.value + '"]').prop('disabled', true);
            });
        });
        
       $('select').on('change', function (e) {
            var exclude = ["StoreOwnerID", "StoreTypeID"];
            var optionName = $(this).attr('name');
            var optionid = $(this).attr('id');
           // console.log(optionName + ' ' + optionid);
            if ($.inArray(optionName, exclude) == -1) {
                var ownerID = $('option:selected', this).attr('ownerid');
                var txtboxID = optionid.charAt(optionid.length - 1);
                $('#shareowner' + txtboxID).val(ownerID)
                        .prop("disabled", false);
                // alert('#shareowner' + txtboxID);
            }
            e.preventDefault();
        });   
        

    $('#btnSubmit').on('click', function (event) {      
        if ($("#CreateDataReconForm")[0].checkValidity()) {
        var url = 'datarecon/functions/DataReconFunctionCaller.php?module=CreateDataRecon';
       // console.log($("#CreateDataReconForm").serialize());
        var mydata = $("#CreateDataReconForm").serialize();
      //  console.log(mydata);
        //return false;
        $.ajax({
            url: url,
            type: 'get',
            data: mydata,
            dataType: 'json',
            success: function (result) {
                document.getElementById('ajaxmsgs').style.display = 'block';
                if (result.type == 'Success') {
                    $('#ajaxmsgs').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong><br>" + result.message + "</div>");
                    $("#CreateDataReconForm :input").attr("disabled", true);
                    $("#cancel").attr("disabled", false).html('Close');
               }
                else {
                    $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong><br>" + result.message + "</div>");
                }
                //alert('success error');
            },
            error: function () {
                document.getElementById('ajaxmsgs').style.display = 'block';
                $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
                //alert('here error');
            }
        });
        }else {
            $("#CreateDataReconForm").find(':submit').click();
        }
        event.preventDefault();
    }); // ajax button form submit


   $('#UserName').keyup(function () { // Keyup function for check the user action in input
        var Username = $(this).val(); // Get the username textbox using $(this) or you can use directly $('#username')
        var UsernameAvailResult = $('#username_avail_result'); // Get the ID of the result where we gonna display the results
        if (Username.length > 4) { // check if greater than 5 (minimum 6)
            UsernameAvailResult.html('Loading..'); // Preloader, use can use loading animation here
            var UrlToPass = 'action=username_availability&username=' + Username;
            $.ajax({// Send the username val to another checker.php using Ajax in POST menthod
                type: 'POST',
                data: UrlToPass,
                url: 'admin/function/CheckUserName.php',
                success: function (responseText) { // Get the result and asign to each cases
                    if (responseText == 0) {
                        UsernameAvailResult.html('<span class="successuser">Username name available</span>');
                    }
                    else if (responseText > 0) {
                        UsernameAvailResult.html('<span class="erroruser">Username already taken</span>');
                    }
                    else {
                        alert('Problem with sql query');
                    }
                }
            });
        } else {
            UsernameAvailResult.html('Enter atleast 5 characters');
        }
        if (Username.length == 0) {
            UsernameAvailResult.html('');
        }
    });

        $('#DataReconPassword, #DataReconName').keydown(function (e) { // Dont allow users to enter spaces for their username and passwords
            if (e.which == 32) {
                return false;
            }
        });
        
    
        $('#includeAssigned').change(function (e) {
            if (!this.checked) {
                $('#sharedDiv').fadeOut('slow');
                $('#sharedDiv :input').attr('disabled', true).hide();
            }
            else {
                $('#sharedDiv').fadeIn('slow')
                        .find('#elementsToOperateOn0 :input').prop('disabled', false).show();
                document.getElementById('elementsToOperateOn0').style.display = 'block';
            }
            e.preventDefault();
        });    
     }); // default hide divs for modal open     
    function showDiv(cliked_id) {
        // alert('here');
        var fieldNum = cliked_id.charAt(cliked_id.length - 1);
        var newID = parseInt(fieldNum) + 1;
        var fieldID = "elementsToOperateOn-" + newID;
        var idfield = "#" + fieldID + " :input";
        document.getElementById(fieldID).style.display = 'block';
        $(idfield).prop("disabled", false).fadeIn('slow');
    } // unhide hidden div and enable input fields for shared to user

    function hideDiv(cliked_id) {
        var fieldNum = cliked_id.charAt(cliked_id.length - 1);
        var newID = parseInt(fieldNum);
        var fieldID = "elementsToOperateOn-" + newID;
        var idfield = "#" + fieldID + " :input";
        document.getElementById(fieldID).style.display = 'none';
        $(idfield).prop("disabled", true).fadeOut('slow');
        
    var optionVal = $('#shareTo' + fieldNum).val();
//        var test = $('#'+fieldID).find('select').attr('id');
    $('.CreateShare').find('option[value="' + optionVal + '"]').prop('disabled', false);
    }     
        
        
        
    $(function () {
        $('[data-tooltip="tooltip"]').tooltip();
    });
</script>
<style type="text/css">
    .successuser{
        color:#009900;
    }
    .erroruser{
        color:#F33C21;
    }
    .username_avail_result{
        display:block;
        width:180px;
    }
</style>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Create Data Recon </h4>
</div>	
<form class="form-horizontal" role="form" id="CreateDataReconForm"><!-- /modal-header -->
    <div class="modal-body">
  <input style="display:none" type="text" name="fakeusernameremembered"/>
        <input style="display:none" type="password" name="fakepasswordremembered"/>
        <div class="form-group">
            <label class="control-label col-sm-3" for="UserName">User Name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="UserName" name="UserName" placeholder="Enter User Name" required minlength="5">
                <div class="username_avail_result" id="username_avail_result"></div>
            </div>
        </div> <!-- /div for store name -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="UserPassword">Password:</label>
            <div class="col-sm-6">
                <input type="password" class="form-control" id="UserPassword" name="UserPassword" placeholder="Enter Password Here" required minlength="5">
            </div>
        </div> <!-- /div for password -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="FirstName">First Name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="FirstName" name="FirstName" placeholder="Enter First Name" required>
            </div>
        </div> <!-- /div for first name -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="LastName">Last Name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="LastName" name="LastName" placeholder="Enter Last Name" required>
            </div>
        </div> <!-- /div for last name -->
         <div class="form-group">
            <label class="control-label col-sm-3" for=""></label>
            <div class="col-sm-6">
                Assign to client/s now? <input type="checkbox" id="includeAssigned" name="includeAssigned"  class="btn-large" 
                                                    data-tooltip="tooltip" data-placement="bottom" title="Yes / No">
            </div>
        </div>
         <div id="sharedDiv"  style="display:none">
            <div class="form-group"  id="elementsToOperateOn0" class="col-sm-6">
                <label class="control-label col-sm-3" for="email">Share Store To:</label>
                <div  class="col-sm-6">
                    <select name="storeOwnerID[]" class="form-control CreateShare" id="shareTo0" disabled required>
                        <option value="">Select Store</option>
                        <?php
                        foreach ($storeList as $value) {
                            echo '<option value="' . $value['f_UserID'] . '">' . $value['f_UserFirstName'] . ' ' . $value['f_UsernLastName'] . '</option>';
                        }
                        ?>
                    </select>                
                </div> 
                <button id="bntShow0" class="btn btn-sm btn-info" type="button" onclick="showDiv(this.id)"
                        data-tooltip="tooltip" data-placement="bottom" title="Add more">+</button>
            </div>   <!-- /div for share store owner -->

            <?php for ($loop = 1; $loop < 10; $loop++) { ?> 
                <div class="form-group" id="elementsToOperateOn-<?php echo $loop; ?>" style="display:none" >
                    <label class="control-label col-sm-3" for="email"></label>
                    <div  class="col-sm-6">
                        <select name="storeOwnerID[]" class="form-control CreateShare"  id="shareTo<?php echo $loop; ?>" disabled required>
                            <option value="">Please select</option>
                            <?php
                            foreach ($storeList as $value) {
                                echo '<option value="' . $value['f_UserID'] . '">' . $value['f_UserFirstName'] . ' ' . $value['f_UsernLastName'] . '</option>';
                            }
                            ?>
                        </select>                      
                    </div>  <button id="btnShow<?php echo $loop; ?>" class="btn btn-sm btn-info" type="button" onclick="showDiv(this.id)">+</button>
                    <button id="btnHide<?php echo $loop; ?>" class="btn btn-sm btn-warning" type="button" onclick="hideDiv(this.id)">-</button>
                </div>  <!-- /elementsToOperateOn div loop -->
            <?php } ?> <!-- loop for hidden dropdown -->

        </div> <!-- /modal-body -->
        
<!--        
        <div class="form-group">
            <label class="control-label col-sm-3" for="StoreOwnerID">Assign To:</label>
            <div class="col-sm-6">
                   <select name="storeOwnerID" class="form-control" id="storeOwnerID" required>
                    <option value="">Please select</option>
                    <?php
                    foreach ($storeList as $value) {
                        echo '<option value="' . $value['f_UserID'] . '">' . $value['f_UserFirstName'] . ' ' . $value['f_UsernLastName'] . '</option>';
                    }
                    ?>
                </select> 
            </div>
        </div>   /div for store owner         -->
        <div id="ajaxmsgs"></div>  <!-- ajaxmsg div -->
    </div> <!-- /modal-body -->	
    <div class="modal-footer">
            <input type="submit" style="visibility: hidden"/>
            <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id="cancel">Cancel</button>
            <button type="button" class="btn btn-sm btn-primary"  id="btnSubmit" name="btnSubmit">Submit</button>
            <button type="reset" class="btn btn-sm btn-danger" >Reset</button>
    </div> <!-- /modal-footer -->	
</form>  <!-- /form --> 

