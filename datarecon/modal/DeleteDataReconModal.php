<?php
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/UserFunction.php');
$varID = $_GET['userid'];

$userData = SelectUser($varID);
$roleList = SelectUserRoleList();

//var_dump($userData);
?>
<script type="text/javascript">

    $(function () {
        $("#ajaxmsgs").hide();
    }); // default hide divs for modal open
    $(document).ready(function () {

        $('#btnSubmit').on('click', function (event) {
            //alert('here');
            var url = 'admin/function/UserFunctionCaller.php?module=DeleteUser';
           // console.log($("#DeleteUserForm").serialize());
            var mydata = $("#DeleteUserForm").serialize();
            $.ajax({
                url: url,
                type: 'get',
                data: mydata,
                dataType: 'json',
                success: function (result) {
                    document.getElementById('ajaxmsgs').style.display = 'block';
                    if (result.type == 'Success') {
                        $('#ajaxmsgs').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong><br>" + result.message + "</div>");
                    }
                    else {
                        $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong><br>" + result.message + "</div>");
                    }
                    //alert('success error');
                },
                error: function () {
                    document.getElementById('ajaxmsgs').style.display = 'block';
                    $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
                    //alert('here error');
                }
            });
            event.preventDefault();
        }); // ajax button form submit
    });
</script>
<style type="text/css">  
    .successuser{
        color:#009900;
    }
    .erroruser{
        color:#F33C21;
    }
    .username_avail_result .checkPass{
        display:block;
        width:180px;
    }</style>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Delete User</h4>
</div>	
<form class="form-horizontal" role="form" id="DeleteUserForm"><!-- /modal-header -->
    <div class="modal-body">
        <p>Are you sure you want to delete this User?</p>
        <div class="well"><h4>User details</h4>
            &nbsp;User Name:<b> <?php echo $userData[0]['f_UserName']; ?></b></br>
            &nbsp;Name:<b> <?php echo $userData[0]['f_UserFirstName']. "&nbsp;". $userData[0]['f_UserLastName']; ?> </b></br>
            &nbsp;Role:<b> <?php echo $userData[0]['f_UserRoleName']; ?></b>
                
        </div>
      
        <input type="hidden" name="UserID" id="UserID" value="<?php echo $varID; ?>" /> 
        <div id="ajaxmsgs"></div>  <!-- ajaxmsg div -->
    </div> <!-- /modal-body -->	
    <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-sm btn-danger"  id="btnSubmit" name="btnSubmit">Submit</button>
    </div> <!-- /modal-footer -->	
</form>  <!-- /form --> 

