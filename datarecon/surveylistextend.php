<?php
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/datarecon/functions/DataReconFunction.php');
ini_set('max_execution_time', 300);
date_default_timezone_set('Australia/Melbourne');

session_start();
$from = $_GET['from'];
$to = $_GET['to'];
$storeOwnerID = $_GET['storeOwnerID'];
$data = getSurveyList($from, $to, $storeOwnerID);
?>



<div class="tab-content" >
    <div class="table-responsive">
        <table id="example" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
            <thead>
                <tr>
                    <th></th>
                    <th  style="min-width:80px;width:30%">Survey Details</th>
                    <th style="width:30%">Assigned Details</th>
                    <!--<th>Modified</th>--> 
                    <th style="width:40%">Modified</th> 
                </tr>
            </thead>

            <tbody>
                <?php foreach ($data as $value) { ?>
                    <tr>
                        <td>
                            <a class="btn-sm btn-hover btn-warning editPNumber editcls" data-toggle="modal"
                               data-tooltip="tooltip" data-placement="bottom" title="Assign survey"
                               href="datarecon/modal/EditPnumberModal.php?invitationID=<?php echo $value['Details']['InivitationID']; ?>&SDID=<?php echo $value['Details']['f_SurveySDID']; ?>&storeID=<?php echo $value['Details']['f_StoreID']; ?>"
                               data-target="#EditPnumberModal" onclick="openEditPnumberModal(this)">
                                <span class="glyphicon glyphicon-cog" ></span>
                            </a>
                        </td>
                        <td>
                            <b>Customer Name: </b> <i> <?php echo $value['Details']['CustomerName']; ?> </i>
                            <b><br>Episode Reference #: </b> <i> <?php echo $value['Details']['EpisodeNum']; ?> </i>
                            <b><br> Invitation ID: </b> <i> <?php echo $value['Details']['InivitationID']; ?> </i>
                        </td>
                        <td>
                            <b> Survey From: </b> <i> <?php echo $value['Details']['f_DealerCode']; ?> </i>
                            <b><br>Assigned To Pnumber: </b> <i> <?php echo $value['Details']['f_ActivationExtra5']; ?> </i>
                            <b> <br>Employee Name: </b> <i> <?php echo $value['Details']['EmpName']; ?> </i>
                            <b> <br>Response Date : </b> <i> <?php
                            $date = new DateTime($value['Details']['f_DateTimeResponseMelbourne']);
                            $date->setTimezone(new DateTimeZone('Australia/Melbourne')); // +04
                            echo $date->format('d-M-y (h:i A)');
                    ?> </i> 
                            <b> <br>Inserted Date : </b> <i> <?php
                                    $date2 = new DateTime($value['Details']['f_SurveyInsertedDate']);
                                    $date2->setTimezone(new DateTimeZone('Australia/Melbourne')); // +04
                                    echo $date2->format('d-M-y (h:i A)');
                                    ?> </i>
                        </td>    
                        <td>
                        <?php
                        if (is_array($value['LogRemarks'])) {
                            if (count($value['LogRemarks']) > 0) {
                                // print_r($value['LogRemarks']);
                                $i = 0;
                                foreach ($value['LogRemarks'] as $logValue) {
                                    if ($i > 0) {
                                        $br = "<br>";
                                    } else {
                                        $br = "";
                                    }
                                    echo $br . $logValue;

                                    $i++;
                                }
                            } else {
                                echo $value['LogRemarks'][0];
                            }
                        } else {
                            echo $value['LogRemarks'];
                        }//}
                        ?>


                        </td>
                    </tr>
                        <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<!-- Modal EditUserModal-->
<div class="modal fade" id="EditPnumberModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal  EditUserModal -->


<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#example').DataTable({
            aLengthMenu: [
                [25, 50, 100, 200, -1],
                [25, 50, 100, 200, "All"]
            ],
            iDisplayLength: 25,
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ]
        });
    });

    $(function () {
        $('[data-tooltip="tooltip"]').tooltip();
    })



</script>























