<section class='reports'>
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-header"><i class="fa fa-archive fa-fw"></i> Latest Survey</h4>            
        </div>
    </div>  
    <div class="main-content">
        <h4 class="sub-header"><i class="fa fa-archive fa-fw"></i> Latest Surveys per Store</h4>
        <div class="loader" id="LogLoader"></div>
        <div  id="page-contents" class="page-contents" ></div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function () {
        $('#page-contents', function () {          
            var urlToLoad = "datarecon/viewsurveysextend.php";
            var divToHide = "#LogLoader";
            var divToShow = "#page-contents";
            reload(urlToLoad, divToHide, divToShow);
        });
        
       function reload(urlToLoad, divToHide, divToShow) {
        var urldata = urlToLoad;
        var page = divToShow;
        $.get(urldata).success(
                function (response, status, jqXhr) {
                    $(divToHide).hide();
                    $(page).fadeIn('slow');
                    $(page).empty().append(response);
                }).error(function (response, status, jqXhr) {
            $(divToHide).show();
            reload(urldata, divToHide, divToShow);
        }).complete(function (response, status, jqXhr) {
        });
    }
    ;   
    });
</script>