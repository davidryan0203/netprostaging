<?php
session_start();
ini_set('max_execution_time', 300);

include('admin/function/UserFunction.php');
include('datarecon/functions/DataReconFunction.php');

$getStoreOwnerID = getStoreOwner();

if ($getStoreOwnerID == 'ALL') {
    $userParams = 'Owner';
} else {
    $userParams = $getStoreOwnerID;
}

$directors = SelectUser($userParams);
$first_day_this_month = date('01/m/Y'); // hard-coded '01' for first day
$last_day_this_month = date('t/m/Y');
$role = $_SESSION['UserRoleID'];
?>

<section class='reports'>
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-header"><i class="fa fa-comments fa-fw"></i>Data Recon</h4>

            <div class="filters">
                <input type="text" id="from" name="from" class="form-control" value="<?php echo $first_day_this_month; ?>" readonly="true"/>
                <input type="text" id="to" name="to"  class="form-control" value="<?php echo $last_day_this_month; ?>" readonly="true"/>
                <?php
                $withSearch = array('1', '6');

                if (in_array($role, $withSearch)) {
                    ?>
                    <select name="storeOwnerID" class="input-sm" id="storeOwnerID">
                        <option value="NULL">Director</option>
                        <?php
                        foreach ($directors as $value) {
                            echo '<option value="' . $value['f_UserID'] . '">' . $value['f_UserFirstName'] . ' ' . $value['f_UserLastName'] . '</option>';
                        }
                        ?>
                    </select>
                <?php } else { ?>
                    <input type="hidden" name="storeOwnerID" id="storeOwnerID" value="<?php echo $getStoreOwnerID; ?>" />
                <?php } ?>
                <button type="submit" class="btn btn-warning btn-circle" id="refresh" data-tooltip="tooltip" data-placement="bottom" title="Reload">
                    <i class="fa fa-refresh"></i>
                </button>

                <!--  <?php
                $dontDisplayForThis = array('4', '5', '6');
                if ($role == '1') {
                    $attritubes = "style=\"display:none\" checked=\"checked\" role=\"1\" ";
                } elseif ($role == '2') {
                    $attritubes = " role=\"2\" ";
                } elseif ($role == '3') {
                    $attritubes = "checked=\"checked\" role=\"3\" ";
                } elseif (in_array($role, $dontDisplayForThis)) {
                    $attritubes = "style=\"display:none\" role=\"$role\" ";
                }
                $withShared = array('2', '3');
                if (in_array($role, $withShared)) {
                    ?> <input type="checkbox" id="ShareType" name="ShareType" class="ShareType"
                                                                                                                                                                       data-toggle="toggle" data-style="ios"  data-onstyle="success"
                                                                                                                                                                       data-offstyle="danger" data-on="Shared" data-off="Mine" <?php echo $attritubes; ?>/>
                <?php } else { ?>
                                                                                                                                                                <input type="checkbox" <?php echo $attritubes; ?> class="ShareType" name="ShareType" id="ShareType" />
                <?php } ?> -->
            </div>
        </div>
    </div>

    <p class="latest-record">
        <?php
        echo $latestRec['latest_record'];
        ?>
    </p>

    <div class="main-content">
        <h4 class="sub-header"><i class="fa fa-bookmark fa-fw"></i> Blank PNumber Surveys</h4>
        <div class="loader" id="LogLoader"></div>
        <div  id="page-contents" class="page-contents" > </div>
    </div>
</section>


<script type="text/javascript">

    $(document).ready(function() {
//        $('#page-contents').hide(function() {
//            var urlToLoad = setLoad();
//            var divToHide = "#LogLoader";
//            var divToShow = "#page-contents";
//            reload(urlToLoad, divToHide, divToShow);
//        });


//        function reload(urlToLoad, divToHide, divToShow) {
//            var urldata = urlToLoad;
//            var page = divToShow;
//            $.get(urldata).success(
//                    function (response, status, jqXhr) {
//                        $(divToHide).hide();
//                        $(page).fadeIn('slow');
//                        $(page).empty().append(response);
//                    }).error(function (response, status, jqXhr) {
//                $(divToHide).show();
//                reload(urldata, divToHide, divToShow);
//            }).complete(function (response, status, jqXhr) {
//            });
//        }

//        $('#page-contents', function() {
//            var toLoad = setLoad();
//            var toLoadextended = toLoad;
//            $.get(toLoadextended).success(
//                    function(response, status, jqXhr) {
//                        $('#page-contents').empty().append(response);
//                    }).error(function(response, status, jqXhr) {
//            }).complete(function(response, status, jqXhr) {
//            });
//        });

//         $('#page-contents', function() {
//             var toLoad = setParams();
//            var toLoadextended = toLoad;
//            loadContents(url, hide, show);
//        });

        $('#page-contents', function() {
            var params = setParams();
            var url = "datarecon/blankpnumberlist.php?" + params;
            var hide = "#LogLoader";
            var show = "#page-contents";
            loadContents(url, hide, show);
        });

        $("#from").datepicker({
            showOn: "button",
            buttonImage: "https://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
            buttonImageOnly: true, dateFormat: 'dd/mm/yy',
            numberOfMonths: 2,
            onSelect: function(selected) {
                $("#to").datepicker("option", "minDate", selected)
            }
        });

        $("#to").datepicker({
            showOn: "button",
            buttonImage: "https://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
            buttonImageOnly: true, dateFormat: 'dd/mm/yy',
            numberOfMonths: 2,
            onSelect: function(selected) {
                $("#from").datepicker("option", "maxDate", selected)
            }
        });

        $('#refresh').click(function() {
            var params = setParams();
            var url = "datarecon/blankpnumberlist.php?" + params;
            var hide = "#LogLoader";
            var show = "#page-contents";
            loadContents(url, hide, show);
        });

        $('#ShareType').change(function() {
            var params = setParams();
            var url = "datarecon/blankpnumberlist.php?" + params;
            var hide = "#LogLoader";
            var show = "#page-contents";
            loadContents(url, hide, show);
        });
    });

//    function setLoad() {
//        var from = $("#from").val().split("/").reverse().join("-");
//        var to = $("#to").val().split("/").reverse().join("-");
//        var storeOwnerID = $("#storeOwnerID").val();
//        if ($('#ShareType').prop('checked')) {
//            //alert('ischeked');
//            var toLoad = 'datarecon/blankpnumberlist.php?to=' + to + '&from=' + from + '&storeOwnerID=' + storeOwnerID;
//        } else {
//            var toLoad = 'datarecon/blankpnumberlist.php?to=' + to + '&from=' + from + '&storeOwnerID=' + storeOwnerID;
//        }
//        return toLoad;
//    }

    function setParams() {
        var from = $("#from").val().split("/").reverse().join("-");
        var to = $("#to").val().split("/").reverse().join("-");
        var storeOwnerID = $("#storeOwnerID").val();
        var params = 'to=' + to + '&from=' + from + '&storeOwnerID=' + storeOwnerID;
        return params;
    }

    function openEditPnumberModal(el) {
        var target = $(el).attr('href');
        $("#EditPnumberModal .modal-content").load(target, function() {
            $("#EditPnumberModal").modal("show");
        });

        $('#EditPnumberModal').on('hidden.bs.modal', function(e) {
            e.preventDefault();
            var params = setParams();
            var url = "datarecon/blankpnumberlist.php?" + params;
            var hide = "#LogLoader";
            var show = "#page-contents";
            loadContents(url, hide, show);

        });
    }

    function loadContents(paramurl, loader, content) {
        $(content).html('');
        $(loader).show();
        $.ajax({
            url: paramurl,
            cache: false,
            success: function(html) {
                $(content).append(html);
            },
            complete: function() {
                $(loader).hide();
            }
        });
    }
    $(function() {
        $('[data-tooltip="tooltip"]').tooltip();
    });
</script>





