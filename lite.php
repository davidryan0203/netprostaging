<?php
session_start();

if (!isset($_SESSION['UserRoleID'])) {
    header("Location: http://{$_SERVER['HTTP_HOST']}/index.php");
}
error_reporting(0);
date_default_timezone_set('Australia/Melbourne');
ini_set('max_execution_time', 300);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>NetPro</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Logo-->
        <link rel="shortcut icon" href="images/logoIcon.png">
        <!--Custom Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,400italic,500,500italic,700,700italic,900,900italic,300italic,300" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100" rel="stylesheet" type="text/css">
        <!--Font Awesome-->
        <link href="includes/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <!--Animate Css -->
        <link href="includes/css/animate.css" rel="stylesheet" type="text/css"/>
        <!--Bootstrap-->
        <link href="includes/css/bootstrap.min.flatly.css" rel="stylesheet" type="text/css"/>
        <!--Custom CSS -->
        <link href="includes/css/styles.css" rel="stylesheet" type="text/css"/>
        <!--Media Queries -->
        <link href="includes/css/media-queries.css" rel="stylesheet" type="text/css"/>
        <!--DataTables-->
        <link href="datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
        <!--DataTables Buttons-->
        <link href="https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>
        <!--Jquery UI for date picker-->
        <link href="includes/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <!--toggle bottons -->
        <link href="includes/css/bootstrapToggle.css" rel="stylesheet" type="text/css"/>
        <!--Jquery-->
        <script type="text/javascript" language="javascript"  src="includes/js/jquery1.11.1.min.js"></script>
    </head>

    <body>
        <div class="main-wrapper">
            <div class = "page-content" >
<?php require('navbar/navlite.php'); ?>
                <div class="container" id='LightPage'>

                <?php
                require('light/index.php');
                ?>


                </div>
            </div>
        </div>


        <!-- Bootstrap -->
        <script src="includes/js/bootstrap.min.js" type="text/javascript"></script>
        <!--Full Slider-->
        <script src="includes/js/jquery.cycle.all.js" type="text/javascript"></script>
        <script src="includes/js/jquery.maximage.js" type="text/javascript"></script>
        <!-- Custom js-->
        <script src="includes/js/custom.js" type="text/javascript"></script>
        <!--toggle bottons-->
        <script src="includes/js/bootstrapToggle.js" type="text/javascript"></script>
        <!--DataTables-->
        <script src="datatables/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
        <!--DataTables Exporting Tools-->
        <script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
        <script src="//cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js" type="text/javascript"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js" type="text/javascript"></script>
        <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js" type="text/javascript"></script>
        <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js" type="text/javascript"></script>
        <!--High Charts-->
        <script src="includes/js/highcharts-custom.js" type="text/javascript"></script>
        <script src="includes/js/jquery-ui.js" type="text/javascript"></script>
        <script src="includes/js/jquery.easing.1.3.js" type="text/javascript"></script>
        <!--Password Authentication-->
        <script src="includes/js/md5.js" type="text/javascript"></script>

    </body>
</html>