<?php
sleep(2);
session_start();
error_reporting(0);
include('admin/function/SubFunction.php');
$first_day_this_month = date('M-y', strtotime("-5 months")); // hard-coded '01' for first day
$last_day_this_month = date('M-y');
$empID = $_SESSION['EmpProfileID'];
$role = $_SESSION['UserRoleID'];
$withAdminTracker = array('1', '2', '3', '5');


$param = 'All';
if ($role != 1) {
    $param = 'Own';
}
$storeList = SelectStoreList($param);
?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<style>.ui-datepicker-calendar {
        display: none;
    }
    .select2-search__field,.select2-selection__rendered,.select2-search{ width: 100% !important}
    .select2-results__option[aria-selected=true] {
        display: none;
    }
</style>
<script type="text/javascript">
    $(document).ready(function() {

        $('.employee-multi-select').select2({
            width: 'resolve',
            theme: 'classic',
            placeholder: "Select Store to select 1 or more employees",
            allowClear: true,
            disabled: true
        });


        $('#storeID').on('change', function() {
            var $el = $("#empIDlist");
            $el.empty();
            var storeID = $('#storeID').val();
            var params = "&storeID=" + storeID;
            var empURL = "npstracker/function/NpsTrackerFunctionCaller.php?module=GetEmployeeList"
            var dump = []

            if (storeID == "AllStores") {
                $('.employee-multi-select').select2({
                    width: 'resolve',
                    theme: 'classic',
                    placeholder: "Select Store to select 1 or more employees",
                    allowClear: true,
                    disabled: true
                });
            } else {
                $('.employee-multi-select').select2({
                    width: 'resolve',
                    theme: 'classic',
                    placeholder: "Loading Employee List",
                    allowClear: true,
                    disabled: true
                });
                $.ajax({
                    url: empURL,
                    type: 'GET',
                    data: params,
                    dataType: "json",
                    success: function(data) {
                        var $el = $("#empIDlist");
                        $el.empty();
                        // console.log(data);
                        $.each(data, function(key, item) {
                            if (!item.text.search("DEACTIVATED") != -1) {
                                $el.append($("<option></option>").attr("data-select2-id", item.id).text(item.text).attr('id', item.id));
                            }

                        });
                    }, complete: function(data) {

                        $('.employee-multi-select').select2({
                            width: 'resolve',
                            theme: 'classic',
                            placeholder: "Select 1 or more employees",
                            allowClear: true,
                            disabled: false,
                        });

                    }
                });
            }
        })

        $('#page-contents').hide(function() {
            var urlToLoad = "npstracker/trackernps.php";
            var divToHide = "#LogLoader";
            var divToShow = "#page-contents";
            loadcontent(urlToLoad, divToHide, divToShow);
        });

        $("#from").datepicker({
            defaultDate: '-5m',
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'M-y',
            onClose: function(dateText, inst) {
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('option', 'defaultDate', new Date(year, month, 1));
                $(this).datepicker('setDate', new Date(year, month, 1));
            }
        });

        $("#to").datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'M-y',
            onClose: function(dateText, inst) {
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('option', 'defaultDate', new Date(year, month, 1));
                $(this).datepicker('setDate', new Date(year, month, 1));
            }
        });


    });
    $(function() {
        $('[data-tooltip="tooltip"]').tooltip();
    });
</script>
<script type="text/javascript" src="npstracker/js/npstracker.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="npstracker/style.css">
<input type="hidden" name="empID" id="empID" value="<?php echo $empID; ?>"/>
<input type="hidden" name="role" id="role" value="<?php echo $role; ?>"/>

<div  id="profileWell"  class="profileWell">


    <div class="row">
        <h4 class="page-header"><i class="fa fa-calculator"></i> <?php
            if ($role == 4) {
                echo "My";
            }
            ?> NPS TREND REPORT</h4>
        <div class="filters col-lg-12 col-sm-12 col-md-12">
            <div class="pull-left"><p class="latest-record">
                    <?php
                    echo $latestRec['latest_record'];
                    ?>

                </p><br>
                <!--Note this report covers TLS Retail stores only-->
            </div>
            <div class="filters col-lg-6">

                <div class=" pull-right">
                    <input type="text" id="from" name="from" class="form-control" value="<?php echo $first_day_this_month; ?>" readonly="true"/>
                    <input type="text" id="to" name="to"  class="form-control" value="<?php echo $last_day_this_month; ?>" readonly="true"/>
                    <button type="submit" class="btn btn-warning btn-circle pull-rigth" id="refresh"
                            data-tooltip="tooltip" data-placement="bottom" title="Reload">
                        <i class="fa fa-refresh"></i>
                    </button>
                </div>
            </div>
            <div class="col-lg-6"></div>
            <div class="col-lg-6"></div>
            <div class="col-lg-6 col-lg-offset-6 col-sm-6 col-sm-offset-6 col-md-6 col-md-offset-6 pull-right">
                <div class="row">
                    <?php
                    if (in_array($role, $withAdminTracker)) {
                        $setEmpID = "";
                        if ($role == 5) {
                            $setEmpID = $_SESSION['EmpProfileID'];
                            echo "<input type=\"hidden\" name=\"EmpIDReserved\" id=\"EmpIDReserved\" value=\"{$setEmpID}\"/>";
                        }
                        ?>
                        <select name="StoreID" id="storeID" class="form-control">
                            <option value="AllStores">All Stores</option>
                            <?php
                            foreach ($storeList as $value) {
                                echo '<option value="' . $value['f_StoreID'] . '">' . $value['f_StoreListName'] . '</option>';
                            }
                            ?>
                        </select>
                        <!--<input type="hidden" name="EmpID" id="EmpID" value="3941" empAttr="<?php echo $role; ?>"/>-->
                        <select class="employee-multi-select form-control" name="EmpIDs[]" style="width: 60%" multiple="multiple" id="empIDlist"  placeholder="Search Employee" >

                        </select>
                    <?php } else { ?>
                        <input type="hidden" name="EmpID" id="EmpID" value="<?php echo $_SESSION['EmpProfileID']; ?>" />
                        <input type="hidden" name="EmpType" id="EmpType" value="single" />
                        <input type="hidden" name="storeID" id="storeID" value="AllStores" />
                    <?php } ?>

                </div>

            </div>
        </div>
    </div>
    <div class="main-content" >
        <!--        <div class="loader" id="LogLoader"></div>
                <div id="profilertracker"> </div>-->

        <div class="row">
            <div class="col-lg-4">
                <div class="thumbnail" id="trackerTablesEpi">
                    <div class="loader" id="tableEpiLoader"></div>
                    <div id="loadTableEpi"></div>
                </div>
            </div>

            <div class="col-lg-8">
                <div class="thumbnail" id="trackerGraphEpi">
                    <div class="loader" id="graphEpiLoader"></div>
                    <div id="trackerEpi"></div>
                </div>
            </div>
        </div>

        <!--        <div class="row">
                    <div class="col-lg-4">
                        <div class="thumbnail" id="trackerTablesInt">
                            <div class="loader" id="tableIntLoader"></div>
                            <div id="loadTableInt"></div>
                        </div>
                    </div>

                    <div class="col-lg-8">
                        <div class="thumbnail" id="trackerGraphInt">
                            <div class="loader" id="graphIntLoader"></div>
                            <div id="trackerInt"></div>
                        </div>
                    </div>
                </div>-->

        <div class="row">
            <div class="col-lg-4">
                <div class="thumbnail" id="trackerTablesMob">
                    <div class="loader" id="tableMobLoader"></div>
                    <div id="loadTableMob"></div>
                </div>
            </div>

            <div class="col-lg-8">
                <div class="thumbnail" id="trackerGraphMob">
                    <div class="loader" id="graphMobLoader"></div>
                    <div id="trackerMob"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <div class="thumbnail" id="trackerTablesFix">
                    <div class="loader" id="tableFixLoader"></div>
                    <div id="loadTableFix"></div>
                </div>
            </div>

            <div class="col-lg-8">
                <div class="thumbnail" id="trackerGraphFix">
                    <div class="loader" id="graphFixLoader"></div>
                    <div id="trackerFix"></div>
                </div>
            </div>
        </div>
    </div>
</div>


