<?php

include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');

function getTargetEmployees($query) {
    global $connection;
    session_start();
    $storeID = $query['storeID'];
    $role = $_SESSION['UserRoleID'];
    if ($storeID == 'AllStores') {
        $userData = SelectEmployee('ALL', NULL, 'groupby');
    } else {

        $userData = SelectEmployee($storeID, 'specificstore', 'groupby');
    }
    $i = 0;
    foreach ($userData as $value) {

        $empList[$i]['text'] = $value['f_EmpFirstName'] . " " . $value['f_EmpLastName'];
        $empList[$i]['id'] = $value['f_EmpID'];

        $i++;
    }
    // print_r($empList);
    return $empList;
}

function super_unique($array) {
    $result = array_map("unserialize", array_unique(array_map("serialize", $array)));
    //$i = 0;
    foreach ($result as $value) {
        if (is_array($value)) {
            $result[] = super_unique($value);
        }
        // $i++;
    }

    return $result;
}

function getStoreSummary($query) {
    global $connection;
    session_start();
    $defaultTo = $query['to'];
    $defaultFrom = $query['from'];
    $format = 'M-y';
    $role = $_SESSION['UserRoleID'];

    $toChange = DateTime::createFromFormat($format, $defaultTo);
    $fromChange = DateTime::createFromFormat($format, $defaultFrom);

    $from = $fromChange->format('Y-m-01');
    $to = $toChange->format('Y-m-t');

    $typeReport = $query['reportType'];

    if ($query['storeID'] == "AllStores") {
        $sqlWhereStoreID = " ";
    } else {
        $sqlWhereStoreID = " AND c.f_StoreID = '{$query['storeID']}'";
    }

    $sumCaseInteractionSql = "";
    $NpsInteractionSql = "";
    $sumCaseEpisodeSql = "";
    $NpsEpisodeSql = "";
    $sumCaseMobileSql = "";
    $NpsMobileSql = "";
    $NpsWhereMobile = "";
    $sumCaseFixedSql = "";
    $NpsFixedSql = "";
    $NpsWhereFixed = "";
    // query change if no employee
    if ($typeReport == 'Interaction') {
        $getNPSdata = 'NPSInt';
        $sumCaseInteractionSql = " SUM(CASE
        WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '9' and '10') AND (a.f_ActivationExtra5 != 'P000000') THEN 1
        ELSE 0
        END) AS EmpInteractionTotalAdvocate,
        SUM(CASE
        WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '7' and '8') AND (a.f_ActivationExtra5 != 'P000000') THEN 1
        ELSE 0
        END) AS EmpInteractionTotalPassive,
        SUM(CASE
        WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '0' and '6') AND (a.f_ActivationExtra5 != 'P000000')THEN 1
        ELSE 0
        END) AS EmpInteractionTotalDetractor, ";

        $NpsInteractionSql = " ((SUM(CASE  WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '9' and '10') AND (a.f_ActivationExtra5 != 'P000000') THEN  1 ELSE 0  END) -
		 SUM(CASE  WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '0' and '6') AND (a.f_ActivationExtra5 != 'P000000') THEN  1 ELSE 0 END) ) * 100 /
         SUM(CASE WHEN a.f_LikelihoodToRecommendRetail between 0 and 10 and  a.f_ActivationExtra5 !=  'P000000'  THEN 1  else 0 end)) as NPSInt, ";

        $sumInteractionStoreSql = " SUM(CASE
          WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '9' and '10') AND (a.f_ActivationExtra5 != 'P000000') THEN  1
          ELSE 0
        END) AS InteractionTotalAdvocate,
        SUM(CASE
            WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '7' and '8') AND (a.f_ActivationExtra5 != 'P000000') THEN  1
            ELSE 0
        END) AS InteractionTotalPassive,
        SUM(CASE
            WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '0' and '6') AND (a.f_ActivationExtra5 != 'P000000') THEN  1
            ELSE 0
        END) AS InteractionTotalDetractors, ";
    } elseif ($typeReport == 'Episode') {
        $getNPSdata = 'NPSEpi';
        $sumCaseEpisodeSql = "  SUM(CASE
          WHEN a.f_RecommendRate BETWEEN '9' and '10' THEN  1
          ELSE 0
        END) AS EmpEpisodeTotalAdvocate,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '7' and '8' THEN  1
            ELSE 0
        END) AS EmpEpisodeTotalPassive,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '0' and '6' THEN  1
            ELSE 0
        END) AS EmpEpisodeTotalDetractors, ";

        $sumEpisodeStoreSql = "  SUM(CASE
          WHEN a.f_RecommendRate BETWEEN '9' and '10' THEN  1
          ELSE 0
        END) AS EpisodeTotalAdvocate,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '7' and '8' THEN  1
            ELSE 0
        END) AS EpisodeTotalPassive,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '0' and '6' THEN  1
            ELSE 0
        END) AS EpisodeTotalDetractors, ";
        $NpsEpisodeSql = "((SUM(CASE  WHEN a.f_RecommendRate BETWEEN '9' and '10' THEN  1 ELSE 0 END) - "
                . "SUM(CASE WHEN a.f_RecommendRate BETWEEN '0' and '6' THEN  1 ELSE 0 END) ) * 100 /  COUNT(a.f_RecommendRate)) as NPSEpi,";
    } elseif ($typeReport == 'Mobile') {
        $getNPSdata = 'NPSMob';
        $sumCaseMobileSql = "  SUM(CASE
          WHEN a.f_RecommendRate BETWEEN '9' and '10' THEN  1
          ELSE 0
        END) AS EmpMobileTotalAdvocate,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '7' and '8' THEN  1
            ELSE 0
        END) AS EmpMobileTotalPassive,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '0' and '6' THEN  1
            ELSE 0
        END) AS EmpMobileTotalDetractors, ";

        $sumMobileStoreSql = "  SUM(CASE
          WHEN a.f_RecommendRate BETWEEN '9' and '10' THEN  1
          ELSE 0
        END) AS MobileTotalAdvocate,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '7' and '8' THEN  1
            ELSE 0
        END) AS MobileTotalPassive,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '0' and '6' THEN  1
            ELSE 0
        END) AS MobileTotalDetractors, ";
        $NpsMobileSql = "((SUM(CASE  WHEN a.f_RecommendRate BETWEEN '9' and '10' THEN  1 ELSE 0 END) -"
                . " SUM(CASE WHEN a.f_RecommendRate BETWEEN '0' and '6' THEN  1 ELSE 0 END) ) * 100 /  COUNT(a.f_RecommendRate)) as NPSMob,";
        $NpsWhereMobile = "  AND b.f_SurveyID IN ('1','2','3','4','13','14','15')
                            AND b.f_ProductID IN ('1','2','3','14','39','40','36') ";
    } elseif ($typeReport == 'Fixed') {
        $getNPSdata = 'NPSFix';
        $sumCaseFixedSql = "  SUM(CASE
          WHEN a.f_RecommendRate BETWEEN '9' and '10' THEN  1
          ELSE 0
        END) AS EmpFixedTotalAdvocate,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '7' and '8' THEN  1
            ELSE 0
        END) AS EmpFixedTotalPassive,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '0' and '6' THEN  1
            ELSE 0
        END) AS EmpFixedTotalDetractors, ";

        $sumFixedStoreSql = "  SUM(CASE
          WHEN a.f_RecommendRate BETWEEN '9' and '10' THEN  1
          ELSE 0
        END) AS FixedTotalAdvocate,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '7' and '8' THEN  1
            ELSE 0
        END) AS FixedTotalPassive,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '0' and '6' THEN  1
            ELSE 0
        END) AS FixedTotalDetractors, ";
        $NpsFixedSql = "((SUM(CASE  WHEN a.f_RecommendRate BETWEEN '9' and '10' THEN  1 ELSE 0 END) -"
                . " SUM(CASE WHEN a.f_RecommendRate BETWEEN '0' and '6' THEN  1 ELSE 0 END) ) * 100 /  COUNT(a.f_RecommendRate)) as NPSFix,";
        $NpsWhereFixed = "  AND b.f_SurveyID IN ('1','2','3','4','6','13','14','15')
                        AND b.f_ProductID IN ('4','5','6','7','9','10','11','12','13','15','17','18','22','30','37','38','41','19','42') ";
    }
    /*
     * get Store Summary with Summary of Employee
     * no employee graph
     */
    $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";

    $empID = $query['empID'];

    switch ($role) {
        case '1':
            $storeOwnerID = " ";
            break;
        case '2':
        case '3':
            $storeOwnerID = $_SESSION['UserProfileID'];
            break;
        case '4':
        case '5':
            $empRoleID = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$empRoleID}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $dataOwnerID = $row['f_StoreOwnerUserID'];
            }
            $storeOwnerID = $dataOwnerID;
            break;
    }

//    if ($query['isMultiple'] == 'yes') { //came from tracker
//        if (!is_null($empID) && !empty($empID) && $empID != "") {
//            // echo 'is blank';
//            $sqlStoreOwnerEmp = " AND c.f_EmpID IN ({$empID})";
//            $sqlStoreOwnerStore = " AND c.f_StoreOwnerUserID = '{$storeOwnerID}' ";
//           // $groupby = "EmpSingle";
//        } else {
//            $sqlWhereEmpID = " ";
//            $sqlStoreOwnerEmp = " AND e.f_StoreOwnerUserID = '{$storeOwnerID}'";
//            $sqlStoreOwnerStore = " AND c.f_StoreOwnerUserID = '{$storeOwnerID}'";
//           // $groupby = "All";
//        }
//    } else {
//        if (!is_null($empID) && !empty($empID)) {
//            $sqlStoreOwnerStore = " AND c.f_StoreOwnerUserID = {$dataOwnerID}";
//            $sqlStoreOwnerEmp = " AND c.f_EmPID = '{$empID}' ";
//          //  $groupby = "EmpSingle";
//        } else {
//            $sqlEmp = " ";
//            $sqlStoreOwnerStore = " AND c.f_StoreOwnerUserID = {$_SESSION['UserProfileID']}";
//            $sqlStoreOwnerEmp = " AND e.f_StoreOwnerUserID = {$_SESSION['UserProfileID']}";
//            //$groupby = "All";
//        }
//    }

    if (!is_null($empID) && !empty($empID)) {
        $sqlEmp = "AND c.f_EmpID IN ({$empID}) ";
        $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$empID}' group by  c.f_StoreOwnerUserID limit 1 ";
        $result = mysql_query($sqlGetOwnerID, $connection);
        mysql_query("SET SQL_BIG_SELECTS=1");
        while ($row = mysql_fetch_assoc($result)) {
            $dataOwnerID = $row['f_StoreOwnerUserID'];
        }

        $sqlStoreOwnerStore = " AND c.f_StoreOwnerUserID = {$storeOwnerID}";
        $sqlStoreOwnerEmp = " AND e.f_StoreOwnerUserID = {$storeOwnerID}";
        $groupby = "EmpSingle";

        $sqlEmpNps = "SELECT
                c.f_EmpID as EmpID,
                case
                    WHEN d.f_EmpFirstName IS NULL THEN a.f_ActivationExtra5
                    ELSE concat(d.f_EmpFirstName, ' ', d.f_EmpLastName)
                END AS EmpName,
                d.f_EmpFirstName as Firstname,
                d.f_EmpLastName as Lastname,
                e.f_StoreListName as Store,
                f.f_EmpRoleName as Role,
                 {$sumCaseInteractionSql}
                 {$NpsInteractionSql}
                 {$sumCaseEpisodeSql}
                 {$NpsEpisodeSql}
                 {$sumCaseMobileSql}
                 {$NpsMobileSql}
                 {$sumCaseFixedSql}
                 {$NpsFixedSql}
                DATE_FORMAT(a.f_DateTimeResponseMelbourne,'%b-%y') as MonthChuck, e.f_StoreListName , c.f_EmpPNumber
                from t_surveysd a
                LEFT JOIN t_surveypd b on a.f_SurveyPDID = b.f_SurveyPDID
                LEFT JOIN t_empnumber c on a.f_ActivationExtra5 = c.f_EmpPNumber and a.f_StoreID = c.f_StoreID
                LEFT JOIN t_emplist d on c.f_EmpID = d.f_EmpID
                LEFT JOIN t_storelist e on a.f_StoreID = e.f_StoreID
                LEFT JOIN t_emprole f on d.f_EmpRoleID = f.f_EmpRoleID
                WHERE 1 = 1
                {$sqlEmp}
                {$sqlWhen}
                {$sqlStoreOwnerEmp}
                {$NpsWhereMobile}
                {$NpsWhereFixed}
                {$sqlWhereStoreID}
                GROUP BY e.f_StoreID , MonthChuck , a.f_ActivationExtra5
                ORDER BY a.f_DateTimeResponseMelbourne, e.f_StoreID  ";
        mysql_query("SET SQL_BIG_SELECTS=1");
        $resultEmpNps = mysql_query($sqlEmpNps, $connection);
        $i = 0;
        while ($rowEmpNps = mysql_fetch_assoc($resultEmpNps)) {
            $EmpNpsData[$rowEmpNps['Firstname'] . " " . $rowEmpNps['f_EmpPNumber']][$rowEmpNps['MonthChuck']] = intval($rowEmpNps[$getNPSdata]);
            $monthList[] = $rowEmpNps['MonthChuck']; //
            $i++;
        }
    } else {
        $sqlEmp = " ";
        $sqlStoreOwnerStore = " AND c.f_StoreOwnerUserID = '{$storeOwnerID}'";
        $sqlStoreOwnerEmp = " AND e.f_StoreOwnerUserID = '{$storeOwnerID}'";
        $groupby = "All";
    }
    $sqlStoreNps = "SELECT DATE_FORMAT(a.f_DateTimeCreatedMelbourne, '%m-%d-%Y') as Date , c.f_StoreID,
        CASE
            WHEN c.f_StoreListName IS NULL
            THEN a.f_DealerCode ELSE c.f_StoreListName
        END AS InteractionStoreName,
        {$sumInteractionStoreSql}
        {$sumEpisodeStoreSql}
        {$sumMobileStoreSql}
        {$sumFixedStoreSql}
        {$NpsInteractionSql}
        {$NpsEpisodeSql}
        {$NpsMobileSql}
        {$NpsFixedSql}

        SUM(CASE WHEN a.f_LikelihoodToRecommendRetail between 0 and 10 and  a.f_ActivationExtra5 !=  'P000000'  THEN 1  else 0 end) as InteractionTotalSurvey,
        COUNT(a.f_RecommendRate) as EpisodeTotalSurvey ,
	 DATE_FORMAT(a.f_DateTimeResponseMelbourne,'%b-%y') as MonthChuck
        FROM t_surveysd a
        LEFT JOIN t_surveypd b on a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c on a.f_StoreID = c.f_StoreID
        WHERE 1 = 1 and c.f_Status = 1
        {$sqlWhen}
        {$sqlStoreOwnerStore}
        {$NpsWhereMobile}
        {$NpsWhereFixed}
        {$sqlWhereStoreID}
        GROUP BY  c.f_StoreID , MonthChuck
        ORDER BY a.f_DateTimeResponseMelbourne , c.f_StoreID";
    mysql_query("SET SQL_BIG_SELECTS=1");
    $resultStoreNps = mysql_query($sqlStoreNps, $connection);
    while ($rowStoreNps = mysql_fetch_assoc($resultStoreNps)) {
        $StoreNpsData[$rowStoreNps['InteractionStoreName']][$rowStoreNps['MonthChuck']] = intval($rowStoreNps[$getNPSdata]);
        $monthList2[] = $rowStoreNps['MonthChuck'];
        $monthData[] = $rowStoreNps['MonthChuck'];
    }
    $o = 0;
    $monthData = array_values(array_unique($monthData));

    usort($monthData, function ($a, $b) {
        $a = DateTime::createFromFormat("M-y", $a);
        $b = DateTime::createFromFormat("M-y", $b);
        return ($a == $b) ? 0 : (($a < $b) ? - 1 : 1);
    });
//    echo "<pre>";
//    echo $sqlStoreNps;
//    echo $sqlEmpNps;
//    exit;
    // print_r($StoreNpsData);
    // exit;
    foreach ($StoreNpsData as $key => $storeValue) {
        ${'row' . $o}['name'] = $key;
        foreach ($monthData as $month) {
            //   ${'row' . $o}['data'][] = intval($value[$getNPSdata]);

            if (is_null($storeValue[$month])) {
                ${'row' . $o}['data'][] = null;
            } else {
                ${'row' . $o}['data'][] = $storeValue[$month];
            }
        }
        $merge[] = ${'row' . $o};
        $o++;
    }

    if ($groupby == 'All') {
        $months = array_values(array_unique($monthList2));
        $result = array('series' => $merge, 'cat' => $months);
    } elseif ($groupby == 'EmpSingle') {

        $monthList = array_values(array_unique($monthList));
        $monthList2 = array_values(array_unique($monthList2));
        $mergemonths = array_values(array_unique(array_merge($monthList, $monthList2)));


        usort($mergemonths, function ($a, $b) {
            $a = DateTime::createFromFormat("M-y", $a);
            $b = DateTime::createFromFormat("M-y", $b);
            return ($a == $b) ? 0 : (($a < $b) ? - 1 : 1);
        });

        $t = 0;
        foreach ($EmpNpsData as $key => $empData) {
            ${'emp' . $t}['name'] = $key;
            foreach ($mergemonths as $month) {
                if (is_null($empData[$month])) {
                    ${'emp' . $t}['data'][] = null;
                } else {
                    ${'emp' . $t}['data'][] = $empData[$month];
                }
            }
            $merge2[] = ${'emp' . $t};
            $t++;
        }
        // $months = array_unique($monthList2);
        $mergedata = array_merge($merge, $merge2);
        $result = array('series' => $mergedata, 'cat' => $mergemonths);
    }
    //$sql = array();
    // $result["sql"] = $resultEmpNps;
    // $sql[2] = $sqlEmpNps;
    // $result['groupby'] = $resultEmpNps;
    //echo $sqlStoreNps;
    return $result;
}

function getEmployeeSummaryAll($query) {
    /*
     * get Summary of Store Employees
     *  no IMPACT score
     */

    // get store  NPS
    // get EmpNPs group bys
    // subract empolyee suyvey from store nps

    global $connection;
    session_start();
    $defaultTo = $query['to'];
    $defaultFrom = $query['from'];
    $format = 'M-y';
    $role = $_SESSION['UserRoleID'];

    $toChange = DateTime::createFromFormat($format, $defaultTo);
    $fromChange = DateTime::createFromFormat($format, $defaultFrom);

    $from = $fromChange->format('Y-m-01');
    $to = $toChange->format('Y-m-t');

    $typeReport = $query['reportType'];

    if ($query['storeID'] == "AllStores") {
        $sqlWhereStoreID = " ";
    } else {
        $sqlWhereStoreID = " AND c.f_StoreID = '{$query['storeID']}'";
    }

    $sumCaseInteractionSql = "";
    $NpsInteractionSql = "";
    $sumCaseEpisodeSql = "";
    $NpsEpisodeSql = "";

    $NpsMobileSql = "";
    $sumCaseMobileSql = "";
    $NpsFixedSql = "";
    $sumCaseFixedSql = "";
    $NpsWhereFixed = "";
    $NpsWhereMobile = "";
    $NpsEpisodeStoreSql = "";
    $sumMobileStoreSql = "";
    $sumFixedStoreSql = "";
    $NpsMobileStoreSql = "";
    $NpsFixedStoreSql = "";
    $sumEpisodeStoreSql = "";
    switch ($role) {
        case '1':
            $storeOwnerID = " ";
            break;
        case '2':
        case '3':
            $storeOwnerID = $_SESSION['UserProfileID'];
            break;
        case '4':
        case '5':
            $empRoleID = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$empRoleID}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $dataOwnerID = $row['f_StoreOwnerUserID'];
            }
            $storeOwnerID = $dataOwnerID;
            break;
    }


    // query change if no employee
    if ($typeReport == 'Interaction') {
        $getNPSdata = 'NPSInt';
        $sumCaseInteractionSql = " SUM(CASE
        WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '9' and '10') AND (a.f_ActivationExtra5 != 'P000000') THEN 1
        ELSE 0
        END) AS EmpTotalAdvocate,
        SUM(CASE
        WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '7' and '8') AND (a.f_ActivationExtra5 != 'P000000') THEN 1
        ELSE 0
        END) AS EmpTotalPassive,
        SUM(CASE
        WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '0' and '6') AND (a.f_ActivationExtra5 != 'P000000')THEN 1
        ELSE 0
        END) AS EmpTotalDetractors,
        SUM(CASE WHEN a.f_LikelihoodToRecommendRetail between 0 and 10 and  a.f_ActivationExtra5 !=  'P000000'  THEN 1  else 0 end) as EmpTotalSurvey,";

        $sumInteractionStoreSql = " SUM(CASE
          WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '9' and '10') AND (a.f_ActivationExtra5 != 'P000000') THEN  1
          ELSE 0
        END) AS StoreTotalAdvocate,
        SUM(CASE
            WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '7' and '8') AND (a.f_ActivationExtra5 != 'P000000') THEN  1
            ELSE 0
        END) AS StoreTotalPassive,
        SUM(CASE
            WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '0' and '6') AND (a.f_ActivationExtra5 != 'P000000') THEN  1
            ELSE 0
        END) AS StoreTotalDetractors,
        SUM(CASE WHEN a.f_LikelihoodToRecommendRetail between 0 and 10 and  a.f_ActivationExtra5 !=  'P000000'  THEN 1  else 0 end) as StoreTotalSurvey,";

        $NpsInteractionSql = " ((SUM(CASE  WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '9' and '10') AND (a.f_ActivationExtra5 != 'P000000') THEN  1 ELSE 0  END) -
		 SUM(CASE  WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '0' and '6') AND (a.f_ActivationExtra5 != 'P000000') THEN  1 ELSE 0 END) ) * 100 /
         SUM(CASE WHEN a.f_LikelihoodToRecommendRetail between 0 and 10 and  a.f_ActivationExtra5 !=  'P000000'  THEN 1  else 0 end)) as EmpNPSInt, ";
        $NpsInteractionStoreSql = " ((SUM(CASE  WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '9' and '10') AND (a.f_ActivationExtra5 != 'P000000') THEN  1 ELSE 0  END) -
		 SUM(CASE  WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '0' and '6') AND (a.f_ActivationExtra5 != 'P000000') THEN  1 ELSE 0 END) ) * 100 /
         SUM(CASE WHEN a.f_LikelihoodToRecommendRetail between 0 and 10 and  a.f_ActivationExtra5 !=  'P000000'  THEN 1  else 0 end)) as StoreNPSInt, ";
    } elseif ($typeReport == 'Episode') {
        $getNPSdata = 'NPSEpi';
        $sumCaseEpisodeSql = "  SUM(CASE
          WHEN a.f_RecommendRate BETWEEN '9' and '10' THEN  1
          ELSE 0
        END) AS EmpTotalAdvocate,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '7' and '8' THEN  1
            ELSE 0
        END) AS EmpTotalPassive,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '0' and '6' THEN  1
            ELSE 0
        END) AS EmpTotalDetractors,
        COUNT(a.f_RecommendRate) as EmpTotalSurvey, ";

        $sumEpisodeStoreSql = "  SUM(CASE
          WHEN a.f_RecommendRate BETWEEN '9' and '10' THEN  1
          ELSE 0
        END) AS StoreTotalAdvocate,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '7' and '8' THEN  1
            ELSE 0
        END) AS StoreTotalPassive,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '0' and '6' THEN  1
            ELSE 0
        END) AS StoreTotalDetractors,
        COUNT(a.f_RecommendRate) as StoreTotalSurvey,";
        $NpsEpisodeSql = "((SUM(CASE  WHEN a.f_RecommendRate BETWEEN '9' and '10' THEN  1 ELSE 0 END) - SUM(CASE WHEN a.f_RecommendRate BETWEEN '0' and '6' THEN  1 ELSE 0 END) ) * 100 /  COUNT(a.f_RecommendRate)) as EmpNPSEpi,";
        $NpsEpisodeStoreSql = "((SUM(CASE  WHEN a.f_RecommendRate BETWEEN '9' and '10' THEN  1 ELSE 0 END) - SUM(CASE WHEN a.f_RecommendRate BETWEEN '0' and '6' THEN  1 ELSE 0 END) ) * 100 /  COUNT(a.f_RecommendRate)) as StoreNPSEpi,";
    } elseif ($typeReport == 'Mobile') {
        $getNPSdata = 'NPSMob';
        $sumCaseMobileSql = "  SUM(CASE
          WHEN a.f_RecommendRate BETWEEN '9' and '10' THEN  1
          ELSE 0
        END) AS EmpTotalAdvocate,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '7' and '8' THEN  1
            ELSE 0
        END) AS EmpTotalPassive,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '0' and '6' THEN  1
            ELSE 0
        END) AS EmpTotalDetractors,
        COUNT(a.f_RecommendRate) as EmpTotalSurvey, ";

        $sumMobileStoreSql = "  SUM(CASE
          WHEN a.f_RecommendRate BETWEEN '9' and '10' THEN  1
          ELSE 0
        END) AS StoreTotalAdvocate,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '7' and '8' THEN  1
            ELSE 0
        END) AS StoreTotalPassive,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '0' and '6' THEN  1
            ELSE 0
        END) AS StoreTotalDetractors,
        COUNT(a.f_RecommendRate) as StoreTotalSurvey,";
        $NpsMobileSql = "((SUM(CASE  WHEN a.f_RecommendRate BETWEEN '9' and '10' THEN  1 ELSE 0 END) - SUM(CASE WHEN a.f_RecommendRate BETWEEN '0' and '6' THEN  1 ELSE 0 END) ) * 100 /  COUNT(a.f_RecommendRate)) as EmpNPSMob,";
        $NpsMobileStoreSql = "((SUM(CASE  WHEN a.f_RecommendRate BETWEEN '9' and '10' THEN  1 ELSE 0 END) - SUM(CASE WHEN a.f_RecommendRate BETWEEN '0' and '6' THEN  1 ELSE 0 END) ) * 100 /  COUNT(a.f_RecommendRate)) as StoreNPSMob,";
        $NpsWhereMobile = " AND b.f_SurveyID IN ('1','2','3','4','13','14','15')
                         AND b.f_ProductID IN ('1','2','3','14','39','40','36')  ";
    } elseif ($typeReport == 'Fixed') {
        $getNPSdata = 'NPSFix';
        $sumCaseFixedSql = "  SUM(CASE
          WHEN a.f_RecommendRate BETWEEN '9' and '10' THEN  1
          ELSE 0
        END) AS EmpTotalAdvocate,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '7' and '8' THEN  1
            ELSE 0
        END) AS EmpTotalPassive,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '0' and '6' THEN  1
            ELSE 0
        END) AS EmpTotalDetractors,
        COUNT(a.f_RecommendRate) as EmpTotalSurvey, ";

        $sumFixedStoreSql = "  SUM(CASE
          WHEN a.f_RecommendRate BETWEEN '9' and '10' THEN  1
          ELSE 0
        END) AS StoreTotalAdvocate,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '7' and '8' THEN  1
            ELSE 0
        END) AS StoreTotalPassive,
        SUM(CASE
            WHEN a.f_RecommendRate BETWEEN '0' and '6' THEN  1
            ELSE 0
        END) AS StoreTotalDetractors,
        COUNT(a.f_RecommendRate) as StoreTotalSurvey,";
        $NpsFixedSql = "((SUM(CASE  WHEN a.f_RecommendRate BETWEEN '9' and '10' THEN  1 ELSE 0 END) - SUM(CASE WHEN a.f_RecommendRate BETWEEN '0' and '6' THEN  1 ELSE 0 END) ) * 100 /  COUNT(a.f_RecommendRate)) as EmpNPSFix,";
        $NpsFixedStoreSql = "((SUM(CASE  WHEN a.f_RecommendRate BETWEEN '9' and '10' THEN  1 ELSE 0 END) - SUM(CASE WHEN a.f_RecommendRate BETWEEN '0' and '6' THEN  1 ELSE 0 END) ) * 100 /  COUNT(a.f_RecommendRate)) as StoreNPSFix,";
        $NpsWhereFixed = " AND b.f_SurveyID IN ('1','2','3','4','6','13','14','15')
                        AND b.f_ProductID IN ('4','5','6','7','9','10','11','12','13','15','17','18','22','30','37','38','41','19') ";
    }
    /*
     * get Store Summary with Summary of Employee
     * no employee graph
     */
    $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";

    $empID = $query['empID'];

    if ($query['isMultiple'] == 'yes') { //came from tracker
        if (!is_null($empID) && !empty($empID) && $empID != "") {
            // echo 'is blank';
            $sqlStoreOwnerEmp = " AND c.f_EmpID IN ({$empID})";
            $sqlStoreOwnerStore = " AND c.f_StoreOwnerUserID = '{$storeOwnerID}' ";
            $groupby = "EmpSingle";
        } else {
            $sqlWhereEmpID = " ";
            $sqlStoreOwnerEmp = " AND e.f_StoreOwnerUserID = '{$storeOwnerID}'";
            $sqlStoreOwnerStore = " AND c.f_StoreOwnerUserID = '{$storeOwnerID}'";
            $groupby = "All";
        }
    } else {
        if (!is_null($empID) && !empty($empID)) {
            $sqlStoreOwnerStore = " AND c.f_StoreOwnerUserID = {$dataOwnerID}";
            $sqlStoreOwnerEmp = " AND c.f_EmPID = '{$empID}' ";
            $groupby = "EmpSingle";
        } else {
            $sqlEmp = " ";
            $sqlStoreOwnerStore = " AND c.f_StoreOwnerUserID = {$_SESSION['UserProfileID']}";
            $sqlStoreOwnerEmp = " AND e.f_StoreOwnerUserID = {$_SESSION['UserProfileID']}";
            $groupby = "All";
        }
    }


    if ($groupby == 'All') {

        $sqlStoreNps = "SELECT
        DATE_FORMAT(a.f_DateTimeResponseMelbourne, '%b-%y') AS MonthChunck,
        CASE
            WHEN c.f_StoreListName IS NULL THEN a.f_DealerCode
            ELSE c.f_StoreListName
        END AS StoreName,
        COUNT(DISTINCT d.f_EmpPNumber) as EmpCount,
        {$sumInteractionStoreSql}
        {$sumEpisodeStoreSql}
        {$NpsInteractionStoreSql}
        {$NpsEpisodeStoreSql}
        {$sumMobileStoreSql}
        {$sumFixedStoreSql}
        {$NpsMobileStoreSql}
        {$NpsFixedStoreSql}
        c.f_StoreID
        FROM t_surveysd a
        LEFT JOIN t_surveypd b on a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c on a.f_StoreID = c.f_StoreID
        LEFT JOIN t_empnumber d ON a.f_ActivationExtra5 = d.f_EmpPNumber
        WHERE 1 = 1 and c.f_Status = 1
        {$sqlWhen}
        {$NpsWhereMobile}
        {$NpsWhereFixed}
        {$sqlStoreOwnerStore}
        {$sqlWhereStoreID}
        {$sqlWhereEmpID}
        GROUP BY  c.f_StoreID , MonthChunck
        ORDER BY c.f_StoreID, a.f_DateTimeResponseMelbourne";
        mysql_query("SET SQL_BIG_SELECTS=1");
        $resultStoreNps = mysql_query($sqlStoreNps, $connection);
        $result = array();
        while ($rowStoreNps = mysql_fetch_assoc($resultStoreNps)) {
            // $StoreNpsData['Month'][] = $rowStoreNps['MonthChunck'];
            // $StoreNpsData[] = $rowStoreNps;
            /// [$rowStoreNps['MonthChunck']][$rowStoreNps['StoreName']][] = array('Employee' => $rowStoreNps['EmpCount'], 'NPS' => $rowStoreNps['Store'.$getNPSdata] );
            // $monthList[] = $rowStoreNps['MonthChunck'];

            $result['Data'][$rowStoreNps['MonthChunck']][] = $rowStoreNps;
        }
        $result['Type'] = 'Store';
    } elseif ($groupby == 'EmpSingle') {
        $result = array();
        $sqlEmp = "SELECT
                 concat(d.f_EmpFirstName,' ',d.f_EmpLastName) as EmpName,
                 c.f_EmpID AS EmpID,
                 c.f_EmpPNumber ,
                 e.f_StoreListName AS StoreName,
                 {$sumCaseInteractionSql}
                 {$NpsInteractionSql}
                 {$sumCaseEpisodeSql}
                 {$NpsEpisodeSql}
                 {$sumCaseMobileSql}
                 {$NpsMobileSql}
                 {$sumCaseFixedSql}
                 {$NpsFixedSql}
                 w.*
                from t_surveysd a
                LEFT JOIN t_surveypd b on a.f_SurveyPDID = b.f_SurveyPDID
                LEFT JOIN t_empnumber c on a.f_ActivationExtra5 = c.f_EmpPNumber and a.f_StoreID = c.f_StoreID
                LEFT JOIN t_emplist d on c.f_EmpID = d.f_EmpID
                LEFT JOIN t_storelist e on a.f_StoreID = e.f_StoreID
                LEFT JOIN t_emprole f on d.f_EmpRoleID = f.f_EmpRoleID
                LEFT JOIN
                (
                    SELECT
                    {$sumInteractionStoreSql}
                    {$sumEpisodeStoreSql}
                    {$NpsInteractionStoreSql}
                    {$NpsEpisodeStoreSql}
                    {$sumMobileStoreSql}
                    {$sumFixedStoreSql}
                    {$NpsMobileStoreSql}
                    {$NpsFixedStoreSql}
                    DATE_FORMAT(a.f_DateTimeResponseMelbourne,'%b-%y') as MonthChuck,c.f_StoreID
                    FROM t_surveysd a
                    LEFT JOIN t_surveypd b on a.f_SurveyPDID = b.f_SurveyPDID
                    LEFT JOIN t_storelist c on a.f_StoreID = c.f_StoreID
                    WHERE 1 = 1 and c.f_Status = 1
                    {$sqlWhen}
                    {$sqlStoreOwnerStore}
                         {$NpsWhereMobile}
                         {$NpsWhereFixed}
                    GROUP BY  c.f_StoreID , MonthChuck
                    ORDER BY c.f_StoreID, a.f_DateTimeResponseMelbourne
                ) w on w.MonthChuck = DATE_FORMAT(a.f_DateTimeResponseMelbourne,'%b-%y') and w.f_StoreID = a.f_StoreID
                WHERE 1 = 1

                 {$sqlWhen}
                 {$sqlStoreOwnerEmp}
                     {$NpsWhereMobile}
                     {$NpsWhereFixed}
                     {$sqlWhereStoreID}
                GROUP BY e.f_StoreID , MonthChuck , a.f_ActivationExtra5
                ORDER BY e.f_StoreID , a.f_DateTimeResponseMelbourne";

        mysql_query("SET SQL_BIG_SELECTS=1");
        $resultEmpNps = mysql_query($sqlEmp, $connection);
        $result = array();
        while ($rowEmpNps = mysql_fetch_assoc($resultEmpNps)) {
            // echo "was here";
            $NewStoreAdvocate = intval($rowEmpNps['StoreTotalAdvocate']) - intval($rowEmpNps['EmpTotalAdvocate']);
            $NewStorePassive = intval($rowEmpNps['StoreTotalPassive']) - intval($rowEmpNps['EmpTotalPassive']);
            $NewStoreDetractor = intval($rowEmpNps['StoreTotalDetractors']) - intval($rowEmpNps['EmpTotalDetractors']);
            $NewStoreTotalSurvey = intval($NewStoreAdvocate) + intval($NewStoreDetractor) + intval($NewStorePassive);
            $NewStoreNPS = (($NewStoreAdvocate - $NewStoreDetractor) / $NewStoreTotalSurvey) * 100;
            $StoreNPS = $rowEmpNps['Store' . $getNPSdata];
            $EmpImpact = $StoreNPS - $NewStoreNPS;
            $newArray = array('StoreName' => $rowEmpNps['StoreName'],
                'EmpTotalSurvey' => $rowEmpNps['EmpTotalSurvey'],
                'StoreTotalSurvey' => $rowEmpNps['StoreTotalSurvey'],
                'Impact' => $EmpImpact,
                'EmpName' => $rowEmpNps['EmpName']);

            $result['Type'] = 'Emp';
            $result['SQL'] = $sqlEmp;
            $result['Data'][$rowEmpNps['MonthChuck']][] = $newArray;
        }
    }
    // echo "<pre>";
    // echo $sqlEmp;
    return $result;

    //  var_dump($result);
}
