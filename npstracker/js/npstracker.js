$(function() {


    $('#refresh').click(function() {
        triggerReload();
    });

    $('#loadTableInt', function() {
        var params = setParams();
        var url = "npstracker/trackertableInt.php?reportType=Interaction&" + params;
        var hide = "#tableIntLoader";
        var show = "#loadTableInt";
        loadContents(url, hide, show);
    });

    $('#trackerInt', function() {
        var params = setParams();
        var url = "npstracker/function/NpsTrackerFunctionCaller.php?module=GetStoreSummaryAllEmployee&reportType=Interaction&";
        var hide = "#graphIntLoader";
        var show = "#trackerInt";
        loadContentsGraph(url, hide, show, "trackerInt", params, "Interaction");
    });

    $('#loadTableEpi', function() {
        var params = setParams();
        var url = "npstracker/trackertableEpi.php?reportType=Episode&" + params;
        var hide = "#tableEpiLoader";
        var show = "#loadTableEpi";
        loadContents(url, hide, show);
    });

    $('#trackerEpi', function() {
        var params = setParams();
        var url = "npstracker/function/NpsTrackerFunctionCaller.php?module=GetStoreSummaryAllEmployee&reportType=Episode&";
        var hide = "#graphEpiLoader";
        var show = "#trackerEpi";
        loadContentsGraph(url, hide, show, "trackerEpi", params, "Episode");
    });


    $('#loadTableMob', function() {
        var params = setParams();
        var url = "npstracker/trackertableMob.php?reportType=Mobile&" + params;
        var hide = "#tableMobLoader";
        var show = "#loadTableMob";
        loadContents(url, hide, show);
    });

    $('#trackerMob', function() {
        var params = setParams();
        var url = "npstracker/function/NpsTrackerFunctionCaller.php?module=GetStoreSummaryAllEmployee&reportType=Mobile&";
        var hide = "#graphMobLoader";
        var show = "#trackerMob";
        loadContentsGraph(url, hide, show, "trackerMob", params, "Mobile");
    });

    $('#loadTableFix', function() {
        var params = setParams();
        var url = "npstracker/trackertableFix.php?reportType=Fixed&" + params;
        var hide = "#tableFixLoader";
        var show = "#loadTableFix";
        loadContents(url, hide, show);
    });

    $('#trackerFix', function() {
        var params = setParams();
        var url = "npstracker/function/NpsTrackerFunctionCaller.php?module=GetStoreSummaryAllEmployee&reportType=Fixed&";
        var hide = "#graphFixLoader";
        var show = "#trackerFix";
        loadContentsGraph(url, hide, show, "trackerFix", params, "Fixed");
    });


});


function trackerChart(data, target, reportName) {
    console.log(data);

    var targetTable = "#" + target;
    var cat = data.cat;
    var datablitz = data.series;
    var reportType = reportName;
    var titleof = reportType + ' Employee NPS vs Store NPS';
    $(targetTable).highcharts({
        credits: {
            enabled: false
        },
        chart: {
            type: 'spline'
        },
        title: {
            text: titleof,
            style: {"color": "#333333", "font-size": "15"}
        },
        xAxis: {
            categories: cat
        },
        yAxis: {
            title: {
                text: 'NPS SCORE'
            }
        },
        tooltip: {
            crosshairs: true,
            shared: true
        },
        plotOptions: {
            spline: {
                dataLabels: {
                    enabled: true
                },
                marker: {
                    radius: 4,
                    lineColor: '#666666',
                    lineWidth: 1
                }
            }
        },
        series: datablitz
    });
}
;

function triggerUpdate() {

    triggerReload();
}

//function empIDupdate(searchEmpID) {
//    // console.log(queryString);
//    var dateTo = $('#to').val();
//    var dateFrom = $('#from').val();
//    var empID = $('#EmpID').val();
//    var reportType = $('#reportType').val();
//    var queryString = "to=" + dateTo + "&from=" + dateFrom + "&empID=" + empID + "reportType=" + reportType;
//    $('#loadTable').fadeOut('slow').load('npstracker/trackertable.php?' + queryString).fadeIn("slow");
//    $.ajax({
//        url: 'npstracker/function/NpsTrackerFunctionCaller.php?module=GetMergedStoreEmp',
//        type: 'GET',
//        async: true,
//        data: queryString,
//        dataType: "json",
//        success: function(data) {
//            // console.log(data);
//            trackerChart(data);
//        }
//    });
//}

function triggerReload() {
    $('#loadTableInt', function() {
        var params = setParams();
        var url = "npstracker/trackertableInt.php?reportType=Interaction&" + params;
        var hide = "#tableIntLoader";
        var show = "#loadTableInt";
        loadContents(url, hide, show);
    });

    $('#trackerInt', function() {
        var params = setParams();
        var url = "npstracker/function/NpsTrackerFunctionCaller.php?module=GetStoreSummaryAllEmployee&reportType=Interaction&";
        var hide = "#graphIntLoader";
        var show = "#trackerInt";
        loadContentsGraph(url, hide, show, "trackerInt", params, "Interaction");
    });

    $('#loadTableEpi', function() {
        var params = setParams();
        var url = "npstracker/trackertableEpi.php?reportType=Episode&" + params;
        var hide = "#tableEpiLoader";
        var show = "#loadTableEpi";
        loadContents(url, hide, show);
    });

    $('#trackerEpi', function() {
        var params = setParams();
        var url = "npstracker/function/NpsTrackerFunctionCaller.php?module=GetStoreSummaryAllEmployee&reportType=Episode&";
        var hide = "#graphEpiLoader";
        var show = "#trackerEpi";
        loadContentsGraph(url, hide, show, "trackerEpi", params, "Episode");
    });


    $('#loadTableMob', function() {
        var params = setParams();
        var url = "npstracker/trackertableMob.php?reportType=Mobile&" + params;
        var hide = "#tableMobLoader";
        var show = "#loadTableMob";
        loadContents(url, hide, show);
    });

    $('#trackerMob', function() {
        var params = setParams();
        var url = "npstracker/function/NpsTrackerFunctionCaller.php?module=GetStoreSummaryAllEmployee&reportType=Mobile&";
        var hide = "#graphMobLoader";
        var show = "#trackerMob";
        loadContentsGraph(url, hide, show, "trackerMob", params, "Mobile");
    });

    $('#loadTableFix', function() {
        var params = setParams();
        var url = "npstracker/trackertableFix.php?reportType=Fixed&" + params;
        var hide = "#tableFixLoader";
        var show = "#loadTableFix";
        loadContents(url, hide, show);
    });

    $('#trackerFix', function() {
        var params = setParams();
        var url = "npstracker/function/NpsTrackerFunctionCaller.php?module=GetStoreSummaryAllEmployee&reportType=Fixed&";
        var hide = "#graphFixLoader";
        var show = "#trackerFix";
        loadContentsGraph(url, hide, show, "trackerFix", params, "Fixed");
    });
}


function setParams() {
    var dateTo = $('#to').val();
    var dateFrom = $('#from').val();

    var storeID = $('#storeID').val();
    //var selectedID = [];
    var selectedID = $('#empIDlist').select2('data');

    if ($("#empIDlist").length == 0) {
        var empID = $('#EmpID').val();
        var isMultiple = "no";
    } else {
        var empID = [];
        $.each(selectedID, function(key, value) {
            empID.push(value.element.id);
        });
        var isMultiple = "yes";
    }

    //empID = JSON.stringify(empID);
    var params = "to=" + dateTo + "&from=" + dateFrom + "&storeID=" + storeID + "&empID=" + empID + "&isMultiple=" + isMultiple;
    //console.log(params);
    return params;
}


function loadContents(paramurl, loader, content) {
    $(content).html('');
    $(loader).show();
    $.ajax({
        url: paramurl,
        cache: false,
        success: function(html) {
            $(content).append(html);
        },
        complete: function() {
            $(loader).hide();
        }
    });
}


function loadContentsGraph(paramurl, loader, content, graphName, params, reportName) {
    $(content).html('');
    $(loader).show();
    $.ajax({
        url: paramurl,
        type: 'GET',
        data: params,
        dataType: "json",
        success: function(html) {
            trackerChart(html, graphName, reportName);
            //console.log(html);
        },
        complete: function() {
            $(loader).hide();
        }
    });

}

