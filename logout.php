<?php

include('autoload.php');
session_start();

if ($_SESSION['sso']) {
    session_start();
    session_unset();
    session_destroy();
    $logOut = env('logOut');
    header("Location: {$logOut}");
} else {
    session_start();
    session_unset();
    session_destroy();
    header("location: /");
}



