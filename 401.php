<!DOCTYPE html>
<html lang="en">
    <head>
        <title>NetPro</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Logo-->
        <link rel="shortcut icon" href="images/logoIcon.png">
        <!--Custom Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,400italic,500,500italic,700,700italic,900,900italic,300italic,300" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100" rel="stylesheet" type="text/css">
        <!--Font Awesome-->
        <link href="includes/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <!--Full slider -->
        <link href="includes/css/jquery.maximage.css" rel="stylesheet" type="text/css"/>
        <link href="includes/css/jquery.screensize.css" rel="stylesheet" type="text/css"/>
        <!--Animate Css -->
        <link href="includes/css/animate.css" rel="stylesheet" type="text/css"/>
        <!--Bootstrap-->
        <link href="includes/css/bootstrap.min.flatly.css" rel="stylesheet" type="text/css"/>
        <!--Custom CSS -->
        <link href="includes/css/stylesV2.css" rel="stylesheet" type="text/css"/>
        <!--Custom Css-->
        <link href="includes/css/homeV2.css" rel="stylesheet">
        <!--Media Queries -->
        <link href="includes/css/media-queries.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">


    </head>
    <Style>

        h1 {
            font-family: "Open Sans";  font-style: normal; font-variant: normal; font-weight: 700;
        }
        h2 {
            font-family: "Open Sans";  font-style: normal; font-variant: normal; font-weight: 700;
        }
        h3 {
            font-family: "Open Sans";  font-style: normal; font-variant: normal; font-weight: 700;
        }
        h4 {
            font-family: "Open Sans";  font-style: normal; font-variant: normal; font-weight: 700;
        }
        p {
            font-family: "Open Sans";  font-style: normal; font-variant: normal; font-weight: 400;
        }
        blockquote {
            font-family: "Open Sans"; font-style: normal; font-variant: normal; font-weight: 400;
        }
        pre {
            font-family: "Open Sans"; font-size: 13px; font-style: normal; font-variant: normal; font-weight: 400;
        }
        p, span, h1,h2,h3,h4,h5{
            color:#5F6160;
        }
        .loginBtn{
            height: 30px !important;
            color: #fff !important;
            padding-top: 3px !important;
            margin-top: 3px !important;
        }
        html, body {
            height: 100%;
        }
        body {
            background-color: #00B6CB;
            margin: 0;
            /*background-image:url('images/backgrounds/thank.png');
            background-size: cover;*/
            height: 90vh;
            min-height: 90vh;
        }
        .flex-container {
            height: 100%;
            padding: 0;
            margin: 0;
            display: -webkit-box;
            display: -moz-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .row {
            width: auto;
        }

        .flex-item {
            padding: 5px;
            width: 20px;
            height: 20px;
            margin: 10px;
            line-height: 20px;
            color: white;
            font-weight: bold;
            font-size: 2em;
            text-align: center;
        }
        .navbar {
            height: 90px;
            padding-top:20px;
        }

        .panel{
            -webkit-box-shadow: 4px 1px 17px 0px rgba(0,0,0,0.39);
            -moz-box-shadow: 4px 1px 17px 0px rgba(0,0,0,0.39);
            box-shadow: 4px 1px 17px 0px rgba(0,0,0,0.39);
        }

        .flex-container{
            background-image:url('images/backgrounds/thank.png');
            background-size: cover;
        }
    </Style>

    <body>
        <section id="banner" style="min-height:90px;height: 90px;">

            <!-- Navbar-->
            <nav class="navbar navbar-inverse" id="navbar">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-navguest" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/">
                            <img src="images/netpro_logo.png" alt="Telstra Blog">
                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="collapse-navguest">
                        <ul class="nav navbar-nav">
                        </ul>

                        <ul class="nav navbar-nav navbar-right">
                            <div class="block" style="margin-top:10px;">
                                <label><span style="color:#000 !important;font-weight: normal;padding-right:20px;position:relative;top:5px">Already have an account?</span></label>
                                <a data-toggle = 'modal' data-target = '#LoginModal' class="btn btn-success  loginBtn">Login</a>
                            </div>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </section>
        <div class="flex-container">
            <div class="row panel panel-default" style="border-radius: 15px;">
                <div class="panel-body" style="padding:35px 75px 35px 55px !important;">
                    <h3 style="color:#08B99F;"><strong style="font-size: larger !important;">Access Denied!</strong></h3><br/>
                    <h4>Seems your account has no NetPro Access.</h4>
                    <h4>Please contact <a href="mailto:support@azenko.com.au?subject=Netpro SSO account problem">support@azenko.com.au</a> for assistance.</h4>
                    <br/>
                    <h4>Regards</h4>
                    <br/>
                    <h4>Azenko Support Team</h4>
                    <a href="maito:support@azenko.com.au"><h4 style="color:#138CB1">support@azenko.com.au</h4></a>
                </div>
            </div>
        </div>

        <div class="modal fade" id="LoginModal" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form class="form-horizontal" role="form" id="form-signin"><!-- /modal-header -->
                        <div class="modal-header" style="background-color: #18BC9C">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h2><i class="fa fa-shield right5px white-text"></i><span class="white-text">NetPro Login</span></h4>
                        </div>

                        <div class="modal-body">
                            <div class="input-group" style="margin-bottom: 5px;">
                                <h4 class="input-group-addon"><i class="fa fa-user"></i> Username</h4>
                                <input type="text" class="form-control" id="log_username_n" name="log_username_n" placeholder="Enter User Name" required value="<?php
                                if (isset($_COOKIE["log_username_n"])) {
                                    echo $_COOKIE["log_username_n"];
                                }
                                ?>">
                                <div class="username_avail_result" id="username_avail_result"></div>
                            </div>
                            <div class="input-group">
                                <h4 class="input-group-addon" style="padding-right: 20px;"><i class="fa fa-lock "></i> Password</h4>
                                <input type="password" class="form-control" id="log_password_n" name="log_password_n" placeholder="Enter Password Here" value="<?php
                                if (isset($_COOKIE["log_password_n"])) {
                                    echo $_COOKIE["log_password_n"];
                                }
                                ?>" required>
                            </div>
                            <div class="pull-right">
                                <label><input type="checkbox" id="remember_me" name="remember_me" <?php if (isset($_COOKIE["log_username_n"])) { ?> checked <?php } ?>> Remember me</label><br>

                            </div>
                            <div id="ajaxmsgs"  style="margin-top: 5px;"></div>  <!-- ajaxmsg div -->
                        </div> <!-- /modal-body -->
                        <div class="modal-footer">
                            <a href="login.php" data-toggle="tooltip" data-placement="top" title="Click here to sign in with your Azenko account"><i class="fa fa-external-link"></i><span> Sign in with your Azenko account </span> </a></bR>

                            <input type="submit" style="visibility: hidden"/>
                            <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-sm btn-primary btnSubmit"  id="submitBtn" name="submitBtn">Log in</button>
                        </div>

                    </form>  <!-- /form -->
                </div> <!-- /.modal-content -->
            </div> <!-- /.modal-dialog -->
        </div> <!-- /.modal  DeleteUserModal -->
    </body>

    <!--Jquery-->
    <script  src="includes/js/jquery1.11.1.min.js" type="text/javascript"></script>
    <!--Full Slider-->
    <script src="includes/js/jquery.cycle.all.js" type="text/javascript"></script>
    <script src="includes/js/jquery.maximage.js" type="text/javascript"></script>
    <!-- Bootstrap -->
    <script src="includes/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.min.js"></script>
</html>



<script type="text/javascript" src="includes/js/jquery.multifield.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#maximage').maximage();

        $('#submitBtn').on('click', function() {
            if ($("#form-signin")[0].checkValidity()) {
                var url = 'functions/checkUser.php';
                var mydata = $("#form-signin").serialize();
                $.ajax({
                    url: url,
                    type: 'get',
                    data: mydata,
                    dataType: 'json',
                    success: function(result) {
                        document.getElementById('ajaxmsgs').style.display = 'block';
                        if (result.type === 'Success') {
                            var link = $('<a href=' + result.URL + ' rel="lightbox" id="link">Click</a>');
                            $("body").append(link);
                            $("#link")[0].click();
                        } else {
                            $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong><br>" + result.message + "</div>");
                        }
                    },
                    error: function() {
                        document.getElementById('ajaxmsgs').style.display = 'block';
                        $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
                    }
                });
            } else {
                $("#form-signin").find(':submit').click();
            }
            event.preventDefault();
        });
    });

    var app = new Vue({
        el: '#app',
        data: {
            storeCount: 1
        }
    })
</script>