<section class='reports'>
    <div class="row">
        <div class="col-lg-12">
            <h3 id="admin-head"><i class='fa fa-gears'></i> Admin Tools</h3>
        </div>
    </div><!--end row-->

    <div class="load" id="LogLoader"></div>
    <div id="page-contents"></div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        $('#page-contents').hide(function() {
            var urlToLoad = "admin/toolsextend.php";
            var divToHide = "#LogLoader";
            var divToShow = "#page-contents";
            reload(urlToLoad, divToHide, divToShow);
        });
    });

    function reload(urlToLoad, divToHide, divToShow) {
        var urldata = urlToLoad;
        var page = divToShow;
        $.get(urldata).success(
                function(response, status, jqXhr) {
                    $(divToHide).hide();
                    $(page).fadeIn('slow');
                    $(page).empty().append(response);
                }).error(function(response, status, jqXhr) {
            $(divToHide).show();
            reload(urldata, divToHide, divToShow);
        }).complete(function(response, status, jqXhr) {
        });
    }

    function reLoad() {
        var urlToLoad = "admin/toolsextend.php";
        var divToHide = "#LogLoader";
        var divToShow = "#page-contents";
        reload(urlToLoad, divToHide, divToShow);
    }

    function openUpdateModal(el) {
        var target = $(el).attr('href');
        var reloadthis = $(el).attr('afterclose');
        $("#UpdateModal .modal-content").load(target, function() {
            $("#UpdateModal").modal("show");
        });

        $('#UpdateModal').on('hidden.bs.modal', function(e) {
            e.preventDefault();
            $('#ViewData').load(reloadthis);

        });
    }

    function openActivateModal(el) {
        var target = $(el).attr('href');
        var reloadthis = $(el).attr('afterclose');
        $("#ActivateModal .modal-content").load(target, function() {
            $("#ActivateModal").modal("show");
        });

        $('#ActivateModal').on('hidden.bs.modal', function(e) {
            e.preventDefault();
            $('#ViewData').load(reloadthis);

        });
    }


    function openMergeModal(el) {
        var target = $(el).attr('href');
        var reloadthis = $(el).attr('afterclose');
        $("#MergeModal .modal-content").load(target, function() {
            $("#MergeModal").modal("show");
        });

        $('#MergeModal').on('hidden.bs.modal', function(e) {
            e.preventDefault();
            $('#ViewData').load(reloadthis);

        });
    }

    function openViewData(el) {
        var target = $(el).attr('specialUrl');
        $('#ViewData').html('');
        $('#ViewData').load(target);

    }

    $(function() {
        $('[data-tooltip="tooltip"]').tooltip();
    });
</script>

