<section class='reports'>
    <div class="row">
        <div class="col-lg-12">
            <h3 id="admin-head"><i class='fa fa-child'></i> Data Recon Staff</h3>
            <div class="filters">
                <a class="btn btn-primary btn-xs btn-admin"  data-toggle="modal" href="datarecon/modal/CreateDataReconModal.php" data-target="#CreateDataReconModal" onclick="openCreateDataReconModal(this)" > 
                    <i class="glyphicon glyphicon-plus"></i> New Data Recon
                </a>  
            </div>
        </div>    
    </div><!--end row-->

    <div class="loader" id="LogLoader"></div>
    <div id="page-contents"></div>
</section>

<script src="../includes/js/custom.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#page-contents').hide(function () {
            var urlToLoad = "admin/datareconextend.php";
            var divToHide = "#LogLoader";
            var divToShow = "#page-contents";
            reload(urlToLoad, divToHide, divToShow);
        });
    });

    function reload(urlToLoad, divToHide, divToShow) {
        var urldata = urlToLoad;
        var page = divToShow;
        $.get(urldata).success(
                function (response, status, jqXhr) {
                    $(divToHide).hide();
                    $(page).fadeIn('slow');
                    $(page).empty().append(response);
                }).error(function (response, status, jqXhr) {
            $(divToHide).show();
            reload(urldata, divToHide, divToShow);
        }).complete(function (response, status, jqXhr) {
        });
    }

    function reLoad() {
        var urlToLoad = "admin/datareconextend.php";
        var divToHide = "#LogLoader";
        var divToShow = "#page-contents";
        reload(urlToLoad, divToHide, divToShow);
    }

    function stripHTML(dirtyString) {
        var container = document.createElement('div');
        container.innerHTML = dirtyString;
        return container.textContent;
    }

    function openDeleteDataReconModal(el) {
        var target = $(el).attr('href');
        $("#DeleteDataReconModal .modal-content").load(target, function () {
            $("#DeleteDataReconModal").modal("show");
        });

        $('#DeleteDataReconModal').on('hidden.bs.modal', function (e) {
            e.preventDefault();
            $('#page-contents').hide();
            $('#LogLoader').fadeIn('slow');
            reLoad();
        });
    }

    function openCreateDataReconModal(el) {
        var target = $(el).attr('href');
        $("#CreateDataReconModal .modal-content").load(target, function () {
            $("#CreateDataReconModal").modal("show");
        });

        $('#CreateDataReconModal').on('hidden.bs.modal', function (e) {
            e.preventDefault();
            $('#page-contents').hide();
            $('#LogLoader').fadeIn('slow');
            reLoad();
        });
    }

    function openEditDataReconModal(el) {
        var target = $(el).attr('href');
        $("#EditDataReconModal .modal-content").load(target, function () {
            $("#EditDataReconModal").modal("show");
        });
        $('#EditDataReconModal').on('hidden.bs.modal', function (e) {
            e.preventDefault();
            $('#page-contents').hide();
            $('#LogLoader').fadeIn('slow');
            reLoad();
        });
    }
</script>


