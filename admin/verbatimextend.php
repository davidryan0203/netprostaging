<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');

session_start();
$role = $_SESSION['UserRoleID'];
$to = $_GET['to'];
$from = $_GET['from'];

$rs = mysql_query($sql);


if ($role == '1') {
    $query['storeOwnerID'] = NULL;
} else {
    if ($role == '5') {
        $queryEmpID = $_SESSION['EmpProfileID'];
//    $type = "StoreLeader"; 
        $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$queryEmpID}' group by  c.f_StoreOwnerUserID limit 1 ";
        $result = mysql_query($sqlGetOwnerID, $connection);
        while ($row = mysql_fetch_assoc($result)) {
            $storeOwnerID = $row['f_StoreOwnerUserID'];
        }
    } else {
        $storeOwnerID = $_SESSION['UserProfileID'];
    }
}

$sqlGetCustomer = "CALL `getVerbatim` ($storeOwnerID, '$from', '$to')";
mysql_set_charset("UTF8");
$resultGetCustomer = mysql_query($sqlGetCustomer, $connection);
header('Content-type: text/html; charset=utf-8');
while ($rowGetCustomer = mysql_fetch_assoc($resultGetCustomer)) {
    $verbatimList[] = $rowGetCustomer;
}

//var_dump($customerList);
?>




<div class="table-responsive">
    <table id="CustomerVerbatim" class="table table-bordered table-hover dt-responsive nowrap"  cellspacing="0" style="width:100%" >
        <thead>
            <tr>
                <th>Response Time Melbourne </th>
                <th>Emp Name </th>
                <th>Store Name </th>
                <th style="width: 20px">Customer Name </th>	
                <th>Reason </th>
                <th>Interaction Score </th>
                <th>Episode Score </th>
                <th>Survey Type </th>
                <th>SubType </th>
                <th>Product Type </th>

            </tr>
        </thead>
        <tbody>
            <?php foreach ($verbatimList as $value) { ?>
                <tr>
                    <td> 
                        <i> <?php echo $value['f_DateTimeResponseMelbourne']; ?></i>                                
                    </td>
                    <td> 
                        <?php echo $value['EmpName']; ?>                              
                    </td>
                    <td> 
                        <?php echo $value['StoreName']; ?>                              
                    </td>
                    <td> 
                        <?php echo $value['CustomerName']; ?>                              
                    </td>
                    <td>                        
                        <?php echo addslashes($value['Reason']); ?>

                    </td>
                    <td> 
                        <?php echo $value['InteractionScore']; ?>                              
                    </td>
                    <td> 
                        <?php echo $value['EpisodeScore']; ?>                              
                    </td>
                    <td> 
                        <?php echo $value['SurveyType']; ?>                              
                    </td>
                    <td> 
                        <?php echo $value['SubType']; ?>                              
                    </td>   
                    <td> 
                        <?php echo $value['ProductDescription']; ?>                              
                    </td> 
                    

                </tr>   
            <?php } ?>
        </tbody> 
        <tfoot>
            <tr>
                <th>DateTimeResponseMelbourne </th>
                <th>Emp Name </th>
                <th>Store Name </th>
                <th>Customer Name </th>	
                <th>Reason </th>
                <th>Interaction Score </th>
                <th>Episode Score </th>
                <th>Survey Type </th>
                <th>SubType </th>
                <th>Product Type </th>
            </tr>
        </tfoot>        
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#CustomerVerbatim').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            "iDisplayLength": 15,
            "aLengthMenu": [[15, 50, 100, -1], [15, 50, 100, "All"]],
            "oColVis": {
                "buttonText": "Show Columns"
            },           
             responsive: true,
            //Footer Sorting
            initComplete: function () {
                var api = this.api();

                api.columns([0, 1, 2,  5, 6, 7, 8 ,9]).indexes().flatten().each(function (i) {
                    var column = api.column(i);
                    var select = $('<select><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );

                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + stripHTML(d).trim() + '">' + stripHTML(d).trim() + '</option>');
                    });
                });
            },
           	paging: true,
 		    pageResize: true,
 		
        });
    });
    
     function stripHTML(dirtyString) {
        var container = document.createElement('div');
        container.innerHTML = dirtyString;
        return container.textContent;
    }

    $(function () {
        $('[data-tooltip="tooltip"]').tooltip();
    });
</script>