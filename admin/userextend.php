<script type="text/javascript">
    $(document).ready(function () {

        var table = $('#example').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            columnDefs: [
                {sortable: false, targets: -1},
                {"width": "8%", "targets": -1}
            ],
            colReorder: {
                fixedColumnsRight: 1
            },
            "iDisplayLength": 15,
            "aLengthMenu": [[15, 50, 100, -1], [15, 50, 100, "All"]],
            //"bStateSave" : true,
            "oColVis": {
                "buttonText": "Hide Columns"
            },
//            initComplete: function () {
//                var api = this.api();
//
//                api.columns().indexes().flatten().each(function (i) {
//                    var column = api.column(i);
//                    var select = $('<select><option value=""></option></select>')
//                            .appendTo($(column.footer()).empty())
//                            .on('change', function () {
//                                var val = $.fn.dataTable.util.escapeRegex(
//                                        $(this).val()
//                                        );
//
//                                column
//                                        .search(val ? '^' + val + '$' : '', true, false)
//                                        .draw();
//                            });
//
//                    column.data().unique().sort().each(function (d, j) {
//                        select.append('<option value="' + stripHTML(d).trim() + '">' + stripHTML(d).trim() + '</option>');
//                    });
//                });
//            },
//            "bAutoWidth": false
        });
    });
    function stripHTML(dirtyString) {
        var container = document.createElement('div');
        container.innerHTML = dirtyString;
        return container.textContent;
    }
    $(function () {
        $('[data-tooltip="tooltip"]').tooltip();
    });
</script>

<?php
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/UserFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
session_start();
$role = $_SESSION['UserRoleID'];
$userID = $_SESSION['UserProfileID'];
$userData = SelectUser('Owner');
?>

<div class="table-responsive">
    <table id="example" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
        <thead>
            <tr>
                <th>User Name</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Role</th>
                <th></th>
            </tr>
        </thead>
<!--        <tfoot>
            <tr>
                <th>User Name</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Role</th>
                <th ></th>
            </tr>
        </tfoot>-->
        <tbody>
            <?php foreach ($userData as $value) { ?>
                <tr>
                    <td><?php echo $value['f_UserName']; ?></td>
                    <td><?php echo $value['f_UserFirstName']; ?></td>
                    <td><?php echo $value['f_UserLastName']; ?></td>                  
                    <td><?php echo $value['f_UserRoleName']; ?></td>                  
                    <td><?php if ($role == '1') { ?>
                            <a class="btn-sm btn-hover btn-warning  editmodal"
                               data-tooltip="tooltip" data-placement="left" title="Update"
                               data-toggle="modal" href="admin/modal/EditUserModal.php?userid=<?php echo $value['f_UserID']; ?>" 
                               data-target="#EditUserModal" onclick="openEditUserModal(this)" >
                                <span class="glyphicon glyphicon-edit" ></span>
                            </a>
                            <a class="btn-sm btn-hover btn-danger deletemodal" 
                               data-tooltip="tooltip" data-placement="left" title="Delete"
                               data-toggle="modal" href="admin/modal/DeleteUserModal.php?userid=<?php echo $value['f_UserID']; ?>" 
                               data-target="#DeleteUserModal" onclick="openDeleteUserModal(this)" >
                                <span class="glyphicon glyphicon-trash" ></span>
                            </a>
                        <?php } elseif ($value['f_UserID'] == $userID) { ?> 
                            <a class="btn-sm btn-hover btn-warning  editmodal" 
                               data-tooltip="tooltip" data-placement="left" title="Update"
                               data-toggle="modal" href="admin/modal/EditUserModal.php?userid=<?php echo $value['f_UserID']; ?>" 
                               data-target="#EditUserModal" onclick="openEditUserModal(this)" >
                                <span class="glyphicon glyphicon-edit" ></span>
                            </a>
                        <?php } ?> </td>
                </tr>   
            <?php } ?>
        </tbody> 
    </table>

    <!-- Modal  CreateUserModal-->
    <div class="modal fade" id="CreateUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div> <!-- /.modal-content -->
        </div> <!-- /.modal-dialog -->
    </div> <!-- /.modal  CreateUserModal -->


    <!-- Modal EditUserModal-->
    <div class="modal fade" id="EditUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div> <!-- /.modal-content -->
        </div> <!-- /.modal-dialog -->
    </div> <!-- /.modal  EditUserModal -->

    <!-- Modal DeleteUserModal-->
    <div class="modal fade" id="DeleteUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div> <!-- /.modal-content -->
        </div> <!-- /.modal-dialog -->
    </div> <!-- /.modal  DeleteUserModal -->

</div>