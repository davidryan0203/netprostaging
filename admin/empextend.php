<?php
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/EmployeeFunction.php');

include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
date_default_timezone_set('Australia/Melbourne');
session_start();
$role = $_SESSION['UserRoleID'];

if ($role == '1') {
    $userData = SelectEmployee('ALL');
} else {
    $role = $_SESSION['UserRoleID'];
    if ($role == '5') {
        $query = $_SESSION['EmpProfileID'];
        $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
        $result = mysql_query($sqlGetOwnerID, $connection);
        while ($row = mysql_fetch_assoc($result)) {
            $data = $row['f_StoreOwnerUserID'];
        }
    } elseif ($role == '6') {
        $data = $_GET['reconActiveOwnerID'];
    } else {
        $data = $_SESSION['UserProfileID'];
    }
    $keySearch = $data;
    $userData = SelectEmployee($keySearch, 'store');
}

//print_r($_GET);
foreach ($userData as $newData) {
    $mergeStoreData[$newData['f_EmpID']]['EmpID'] = $newData['f_EmpID'];
    $mergeStoreData[$newData['f_EmpID']]['EmpFirstName'] = $newData['f_EmpFirstName'];
    $mergeStoreData[$newData['f_EmpID']]['EmpLastName'] = $newData['f_EmpLastName'];
    $mergeStoreData[$newData['f_EmpID']]['UserName'] = $newData['f_UserName'];
    $mergeStoreData[$newData['f_EmpID']]['Password'] = $newData['f_Password'];
    $mergeStoreData[$newData['f_EmpID']]['EmpRoleID'] = $newData['f_EmpRoleID'];
    $mergeStoreData[$newData['f_EmpID']]['EmpRoleName'] = $newData['f_EmpRoleName'];
    $mergeStoreData[$newData['f_EmpID']]['EmpPhone'] = $newData['f_EmpPhone'];
    $mergeStoreData[$newData['f_EmpID']]['EmpMobile'] = $newData['f_EmpMobile'];
    $mergeStoreData[$newData['f_EmpID']]['EmpEmail'] = $newData['f_EmpEmail'];
    $mergeStoreData[$newData['f_EmpID']]['LoginCount'] = $newData['LoginCount'];
    $mergeStoreData[$newData['f_EmpID']]['LatestLogin'] = $newData['LatestLogin'];

    $mergeStoreData[$newData['f_EmpID']]['EmpNo'][] = array('StoreListID' => $newData['f_StoreID'],
        'EmpNo' => $newData['f_EmpPNumber'],
        'StoreListName' => $newData['f_StoreListName']
    );
}

//echo "<pre>";var_dump($mergeStoreData);
?>

<script type="text/javascript">
    $(document).ready(function() {

        var table = $('#example').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            columnDefs: [
                {sortable: false, targets: -1},
                {"width": "8%", "targets": -1},
                {"targets": [5, 6, 7], "visible": false}
            ],
            colReorder: {
                fixedColumnsRight: 1
            },
            "iDisplayLength": 15,
            "aLengthMenu": [[15, 50, 100, -1], [15, 50, 100, "All"]],
            //"bStateSave" : true,
            "oColVis": {
                "buttonText": "Show Columns"
            },
            "bAutoWidth": false,
            stateSave: true
        });
    });

    $(function() {
        $('[data-tooltip="tooltip"]').tooltip();
    });
</script>

<div class="table-responsive">
    <table id="example" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
        <thead>
            <tr>
                <th>P#</th>
                <th style="min-width:140px;">Employee No</th>
                <th>Employee Name</th>
                <th>Login Name</th>
                <th>Role</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Mobile</th>
                <th>Log-in Count</th>
                <th>Last Login</th>
                <th>Update</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($mergeStoreData as $value) { ?>
                <tr>
                    <td>
                        <a class="btn-sm btn-hover btn-warning editcls editempmodal"
                           data-toggle="modal"  href="admin/modal/EditEmployeeNumberModal.php?empID=<?php echo $value['EmpID']; ?>"
                           data-tooltip="tooltip" data-placement="right" title="Add / Update P#"
                           data-target="#EditEmployeeNumberModal" onclick="openModalNumberEdit(this)">
                            <span class="glyphicon glyphicon-edit" ></span>
                        </a>
                    </td>
                    <td style="width:130px;"><?php
                foreach ($value['EmpNo'] as $EmpNo) {
                    //var_dump($EmpNo);
                    echo $EmpNo['EmpNo'] . "-" . $EmpNo['StoreListName'] . "<br>";
                }
                ?>
                    <td><?php echo $value['EmpFirstName'] . " " . $value['EmpLastName'] ?></td>
                    <td><?php echo $value['UserName']; ?></td>
                    <td><?php echo $value['EmpRoleName']; ?></td>
                    <td><?php echo $value['EmpEmail']; ?></td>
                    <td><?php echo $value['EmpPhone']; ?></td>
                    <td><?php echo $value['EmpMobile']; ?></td>
                    <td><?php echo $value['LoginCount'] ?></td>
                    <td>
                        <?php
                        if (is_null($value['LatestLogin'])) {
                            echo "No logs";
                        } else {


                            $date = new DateTime($value['LatestLogin']);
                            $date->setTimezone(new DateTimeZone('Australia/Melbourne')); // +04
                            echo $date->format('d-M-y (h:i A)');
                            // echo $lastlogin;
                            // echo date('d-M-y (h:i A)', strtotime($value['LatestLogin']));
                        }
                        ?>
                    </td>
                    <td>
                        <a class="btn-sm btn-hover btn-warning editcls editmodal"
                           data-toggle="modal" href="admin/modal/EditEmployeeModal.php?empID=<?php echo $value['EmpID']; ?>"
                           data-tooltip="tooltip" data-placement="left" title="Update"
                           data-target="#EditEmployeeModal"
                           onclick="openModalEdit(this)"  style="width: 32px; height: 22px;">
                            <span class="glyphicon glyphicon-edit"></span>
                        </a>
                        <a class="btn-sm btn-hover btn-danger editcls deletemodal"
                           data-toggle="modal" href="admin/modal/DeleteEmployeeModal.php?empID=<?php echo $value['EmpID']; ?>"
                           data-tooltip="tooltip" data-placement="left" title="Delete"
                           data-target="#DeleteEmployeeModal"
                           onclick="openDeleteEmployeeModal(this)" style="width: 32px; height: 22px;">
                            <span class="glyphicon glyphicon-trash" ></span>
                        </a>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>


<!-- Modal EditEmployeeModal-->
<div class="modal fade" id="CreateEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal  EditEmployeeModal -->


<!-- Modal EditEmployeeModal-->
<div class="modal fade" id="EditEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal  EditEmployeeModal -->


<!-- Modal EditEmployeeNumberModal-->
<div class="modal fade" id="EditEmployeeNumberModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal  EditEmployeeNumberModal -->

<!-- Modal DeleteEmployeeModal-->
<div class="modal fade" id="DeleteEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal  DeleteEmployeeModal -->



