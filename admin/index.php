<?php
/**
 * Create admin panel function
 */
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');

error_reporting(0);
session_start();
$role = $_SESSION['UserRoleID'];
$noUserAccess = array('3', '5', '2');
$userIsOnClient = checkClientList();
$isVerbatim = $_GET['verbatim'];

//echo $role."hre";
if ($_SESSION['sso']) {
    $isSSO = true;  
    //echo "is sso";
}else {
    $isSSO = false;
    //echo "not";
}

if($userIsOnClient['message']){
    $isClient = true;
    //echo "is client";
} else {
    $isClient = false;
    // echo "not client";
}

if(!$isSSO && !$isClient){
    $empLi = "active";
    $empVerbatim = "";
}else{
    $empLi = "";
    $empVerbatim = "active";
}


?>

<div role="tabpanel">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" id="adminTab" role="tablist">
        <?php if (!$isSSO && !$isClient) { ?>
            <li role="presentation" specialUrl="admin/employee.php" class="<?php echo $empLi; ?> listLink" id="EmpLi" >
                <a href="admin/employee.php" aria-controls="page-loader" role="tab" data-toggle="tab">
                    <i class='fa fa-group'></i> Employees
                </a>
            </li>

            <li role="presentation" specialUrl="admin/store.php" class=" listLink" id="storeLi" >
                <a href="admin/store.php" aria-controls="page-loader" role="tab" data-toggle="tab">
                    <i class='fa fa-shopping-cart'></i> Stores
                </a>
            </li>
        <?php } ?> 
        <?php if (!in_array($role, $noUserAccess)) { ?>
            <li role="presentation" specialUrl="admin/user.php" class="listLink" id="UserLi" >
                <a href="admin/user.php" aria-controls="page-loader" role="tab" data-toggle="tab">
                    <i class='fa fa-users'></i> Privilege Users
                </a>
            </li>
        <?php } ?>

        <li role="presentation" specialUrl="admin/customer.php" class="listLink" id="CustomerLi" >
            <a href="admin/customer.php" aria-controls="page-loader" role="tab" data-toggle="tab">
                <i class='fa fa-tags'></i> Customer
            </a>
        </li>
        <!--        <li role="presentation" specialUrl="admin/trackernps.php" class="listLink" id="AdminTrackerLi" >
                    <a href="admin/trackernps.php" aria-controls="page-loader" role="tab" data-toggle="tab">
                        <i class='fa fa-calculator'></i> NPS Tracker
                    </a>
                </li>-->
        <li role="presentation" specialUrl="admin/verbatim.php" class="<?php echo $empVerbatim; ?> listLink" id="VerbatimLi" >
            <a href="admin/verbatim.php" aria-controls="page-loader" role="tab" data-toggle="tab">
                <i class='fa fa-rss-square'></i> Verbatim Extract
            </a>
        </li>
        <!--        <li role="presentation" specialUrl="admin/tools.php" class="listLink" id="MedalliaLi" >
                    <a href="admin/tools.php" aria-controls="page-loader" role="tab" data-toggle="tab">
                        <i class='fa fa-cog'></i> Tools
                    </a>
                </li>-->
        <?php if ($role == 1) { ?>
            <li role="presentation" specialUrl="admin/datarecon.php" class="listLink" id="DataReconLi" >
                <a href="admin/datarecon.php" aria-controls="page-loader" role="tab" data-toggle="tab">
                    <i class='fa fa-child'></i> Data Recon
                </a>
            </li>
            <li role="presentation" specialUrl="admin/admin.php" class="listLink" id="AdminLi" >
                <a href="admin/admin.php" aria-controls="page-loader" role="tab" data-toggle="tab">
                    <i class='fa fa-gear'></i> Admin
                </a>
            </li>
            <li role="presentation" specialUrl="admin/logs.php" class="listLink" id="LogsLi" >
                <a href="admin/logs.php" aria-controls="page-loader" role="tab" data-toggle="tab">
                    <i class='fa fa-clock-o'></i> Logs
                </a>
            </li>
        <?php } ?>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class='main-content' style='margin-top:2px !important;'>
            <div role="tabpanel" class="tab-pane active" id="page-loader"></div>
        </div>
    </div>
</div>

<script>
    $('.listLink').click(function (e) {
        e.preventDefault();
        $("#page-loader").load($(this).attr('specialUrl'));
        $('.listLink').removeClass('active');
        var toId = $(this).attr('id');
        $('#' + toId).addClass('active');
    });

    $(document).ready(function () {
<?php if (!$isSSO && !$isClient) { ?>
            $('#page-loader').load('admin/employee.php').fadeIn("slow");
            $('#adminTab').tab();
<?php } else { ?>
            $('#page-loader').load('admin/verbatim.php').fadeIn("slow");
            $('#adminTab').tab();
<?php } ?>

    });
</script>


