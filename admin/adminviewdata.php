<?php
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/StoreFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');

session_start();
$role = $_SESSION['UserRoleID'];
$module = $_GET['module'];

$mergeStore = SelectMergeStore();
foreach ($mergeStore as $storeData) {
    $newData[$storeData['f_StoreID']]['Data'] = array('StoreName' => $storeData['f_StoreListName'], 'CompanyCode' => $storeData['f_CompanyCode'], 'StoreID' => $storeData['f_StoreID']);
    $newData[$storeData['f_StoreID']]['MergeCode'][] = array('MergeCode' => $storeData['f_MergeToCompanyCode'], 'MergeID' => $storeData['f_MergeReportID']);
}
//echo "<pre>";print_r($newData);

$StoreType = SelectStoreTypeList();
$UserRole = SelectUserRoleList('UserRole');
$EmpRole = SelectEmpRoleList();
$DeletedStore = SelectStoreInactive();
$DeletedUser = SelectUserInactive();
$DeletedEmployee = SelectEmployeeInactive();
?>

<script type="text/javascript">

    $(document).ready(function() {
        var table = $('#<?php echo $module; ?>Table').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ]
        });

        table.on('order.dt search.dt', function() {
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        $("th").unbind();
        $("th").removeClass("sorting");
        $("th").removeClass("sorting_asc");
    });


    function stripHTML(dirtyString) {
        var container = document.createElement('div');
        container.innerHTML = dirtyString;
        return container.textContent;
    }

    $(function() {
        $('[data-tooltip="tooltip"]').tooltip();
    })
</script>

<div class="table-responsive" style='overflow-x: hidden;'>
    <?php if ($module == 'FixLine') { ?>
        <h4 class='sub-header'>Store Codes</h4>
        <table id="<?php echo $module; ?>Table" class="table table-bordered table-hover"  cellspacing="0" >
            <thead>
                <tr>
                    <th></th>
                    <th>Store Name</th>
                    <th>Dealer Code</th>
                    <th>Fix Line Code</th>
                    <th>Update</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($newData as $key => $value) { ?>
                    <tr>
                        <td></td>
                        <td><?php echo $value['Data']['StoreName']; ?></td>
                        <td><?php echo $value['Data']['CompanyCode']; ?></td>
                        <td><?php
                            foreach ($value['MergeCode'] as $mergedWith) {
                                echo $mergedWith['MergeCode'] . "<br>";
                            }
                            ?></td>
                        <td>
        <?php if ($role == '1') { ?>
                                <a class="btn-sm btn-hover btn-warning editmodal editcls"
                                   data-tooltip="tooltip" data-placement="bottom" title="Edit"
                                   data-toggle="modal" href="admin/modal/MergeModal.php?storeID=<?php echo $key; ?>" afterclose="admin/adminviewdata.php?module=FixLine"
                                   data-target="#EditStoreModal" onclick="openMergeModal(this)">
                                    <span class="glyphicon glyphicon-edit" ></span>
                                </a>
                    <?php } ?>
                        </td>
                    </tr>
        <?php } ?>
            </tbody>
        </table>
<?php }if ($module == 'StoreType') { ?>

        <h4 class='sub-header'>Registered Store Type
            <a class="btn btn-sm btn-success pull-right"
               data-tooltip="tooltip" data-placement="bottom" title="Update"
               data-toggle="modal" href="admin/modal/UpdaterModal.php?type=StoreType" afterclose="admin/adminviewdata.php?module=StoreType"
               data-target="#UpdateModal" onclick="openUpdateModal(this)">
                <span class="glyphicon glyphicon-edit" ></span> Update
            </a>
        </h4>
        <table id="<?php echo $module; ?>Table" class="table table-bordered table-hover" cellspacing="0" >
            <thead>
                <tr>
                    <th></th>
                    <th>Store Type</th>
                </tr>
            </thead>

            <tbody>
    <?php foreach ($StoreType as $value) { ?>
                    <tr>
                        <td></td>
                        <td><?php echo $value['f_StoreTypeName']; ?></td>
                    </tr>
    <?php } ?>
            </tbody>
        </table>

<?php } elseif ($module == 'UserRole') {
    ?>
        <h4 class='sub-header'>Registered User Roles
            <a class="btn btn-sm btn-success pull-right"
               data-tooltip="tooltip" data-placement="bottom" title="Update"
               data-toggle="modal" href="admin/modal/UpdaterModal.php?type=UserRole" afterclose="admin/adminviewdata.php?module=UserRole"
               data-target="#UpdateModal" onclick="openUpdateModal(this)">
                <span class="glyphicon glyphicon-edit" ></span> Update
            </a>
        </h4>
        <table id="<?php echo $module; ?>Table" class="table table-bordered table-hover" cellspacing="0" >
            <thead>
                <tr>
                    <th></th>
                    <th>User Roles</th>
                </tr>
            </thead>

            <tbody>
    <?php foreach ($UserRole as $value) { ?>
                    <tr>
                        <td></td>
                        <td><?php echo $value['f_UserRoleName']; ?></td>
                    </tr>
    <?php } ?>
            </tbody>
        </table>

<?php } elseif ($module == 'EmpRole') { ?>
        <h4 class='sub-header'>Registered Employee Roles
            <a class="btn btn-sm btn-success pull-right"
               data-tooltip="tooltip" data-placement="bottom" title="Update"
               data-toggle="modal" href="admin/modal/UpdaterModal.php?type=EmpRole" afterclose="admin/adminviewdata.php?module=EmpRole"
               data-target="#UpdateModal" onclick="openUpdateModal(this)">
                <span class="glyphicon glyphicon-edit" ></span> Update
            </a>
        </h4>

        <table id="<?php echo $module; ?>Table" class="table table-bordered table-hover" cellspacing="0" >
            <thead>
                <tr>
                    <th></th>
                    <th>Employee Roles</th>
                </tr>
            </thead>

            <tbody>
    <?php foreach ($EmpRole as $value) { ?>
                    <tr>
                        <td></td>
                        <td><?php echo $value['f_EmpRoleName']; ?></td>
                    </tr>
        <?php } ?>
            </tbody>
        </table>
<?php } elseif ($module == 'Store') {
    ?>
        <h4 class='sub-header'>Deactivated Stores
            <a class="btn btn-sm btn-success pull-right"
               data-tooltip="tooltip" data-placement="bottom" title="Update"
               data-toggle="modal" href="admin/modal/ActivatorModal.php?type=Store" afterclose="admin/adminviewdata.php?module=Store"
               data-target="#ActivateModal" onclick="openActivateModal(this)">
                <span class="glyphicon glyphicon-edit" ></span> Update
            </a>
        </h4>

        <table id="<?php echo $module; ?>Table" class="table table-bordered table-hover" cellspacing="0" >
            <thead>
                <tr>
                    <th></th>
                    <th>Director</th>
                    <th>Name</th>
                    <th>Code</th>
                    <th>Type</th>
                </tr>
            </thead>

            <tbody>
    <?php foreach ($DeletedStore as $value) { ?>
                    <tr>
                        <td></td>
                        <td><?php echo $value['']; ?></td>
                        <td><?php echo $value['f_StoreListName']; ?></td>
                        <td><?php echo $value['f_CompanyCode']; ?></td>
                        <td><?php echo $value['']; ?></td>
                    </tr>
        <?php } ?>
            </tbody>
        </table>
<?php } elseif ($module == 'User') { ?>
        <h4 class='sub-header'>Deactivated Users
            <a class="btn btn-sm btn-success pull-right"
               data-tooltip="tooltip" data-placement="bottom" title="Update"
               data-toggle="modal" href="admin/modal/ActivatorModal.php?type=User" afterclose="admin/adminviewdata.php?module=User"
               data-target="#ActivateModal" onclick="openActivateModal(this)">
                <span class="glyphicon glyphicon-edit" ></span> Update
            </a>
        </h4>

        <table id="<?php echo $module; ?>Table" class="table table-bordered table-hover" cellspacing="0" >
            <thead>
                <tr>
                    <th></th>
                    <th>Firstname</th>
                    <th>Surname</th>
                    <th>User Role</th>
                </tr>
            </thead>

            <tbody>
    <?php foreach ($DeletedUser as $value) { ?>
                    <tr>
                        <td></td>
                        <td><?php echo $value['f_UserFirstName']; ?></td>
                        <td><?php echo $value['f_UserLastName']; ?></td>
                        <td><?php echo $value['']; ?></td>
                    </tr>
        <?php } ?>
            </tbody>
        </table>
<?php } elseif ($module == 'Employee') {
    ?>
        <h4 class='sub-header'>Deactivated Employees
            <a class="btn btn-sm btn-success pull-right"
               data-tooltip="tooltip" data-placement="bottom" title="Update"
               data-toggle="modal" href="admin/modal/ActivatorModal.php?type=Employee" afterclose="admin/adminviewdata.php?module=Employee"
               data-target="#ActivateModal" onclick="openActivateModal(this)">
                <span class="glyphicon glyphicon-edit" ></span> Update
            </a>
        </h4>

        <table id="<?php echo $module; ?>Table" class="table table-bordered table-hover" cellspacing="0" >
            <thead>
                <tr>
                    <th></th>
                    <th>Firstname</th>
                    <th>Surname</th>
                    <th>Store</th>
                    <th>Director</th>
                    <th>User Role</th>
                </tr>
            </thead>
            <tbody>
    <?php foreach ($DeletedEmployee as $value) { ?>
                    <tr>
                        <td></td>
                        <td><?php echo $value['f_EmpFirstName']; ?></td>
                        <td><?php echo $value['f_EmpLastName']; ?></td>
                        <td><?php echo $value['StoreName']; ?></td>
                        <td><?php echo $value['CompanyCode']; ?></td>
                        <td><?php echo $value['f_EmpRoleID']; ?></td>
                    </tr>
        <?php } ?>
            </tbody>
        </table>
<?php } ?>
</div>



