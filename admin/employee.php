<?php
session_start();
$role = $_SESSION['UserRoleID'];
if(isset($_GET['activeOwnerID'])){
    $activeOwner = '"&reconActiveOwnerID='.$_GET['activeOwnerID'].'"';
}else{
    $activeOwner = '"&reconActiveOwnerID=0"';
}
?>

<section class='reports'>
    <div class="row">
        <div class="col-lg-12">
            <h3 id="admin-head"><i class='fa fa-group'></i> Employees List</h3>
            <div class="filters">
                <a class="btn btn-primary btn-xs btn-admin"  data-toggle="modal"
                   data-tooltip="tooltip" data-placement="left" title="Add Employee"
                   href="admin/modal/CreateEmployeeModal.php" data-target="#CreateEmployeeModal" onclick="openCreateEmployeeModal(this)">
                    <i class="glyphicon glyphicon-plus"></i> Employee
                </a> 
            </div>
        </div>
    </div><!--end row-->

    <div class="loader" id="LogLoader"></div>

    <div id="page-contents"></div>
</section>

<script src="../includes/js/custom.js" type="text/javascript"></script>

<script type="text/javascript">
     var reconActiveOwnerID = 0;
        <?php if($role == '6'){ ?>
             reconActiveOwnerID = <?php echo $activeOwner ?>;
        <?php } ?>
    $(document).ready(function () {
        $('#page-contents').hide(function () {
            var urlToLoad = "admin/empextend.php?shared=no"+reconActiveOwnerID;
            var divToHide = "#LogLoader";
            var divToShow = "#page-contents";
            reload(urlToLoad, divToHide, divToShow);
        });
    });
    
    function reload(urlToLoad, divToHide, divToShow) {
        var urldata = urlToLoad;
        var page = divToShow;
        $.get(urldata).success(
                function (response, status, jqXhr) {
                    $(divToHide).hide();
                    $(page).fadeIn('slow');
                    $(page).empty().append(response);
                }).error(function (response, status, jqXhr) {
            $(divToHide).show();
            reload(urldata, divToHide, divToShow);
        }).complete(function (response, status, jqXhr) {
        });
    }
    
    function reLoad() {
        var urlToLoad = "admin/empextend.php?shared=no"+reconActiveOwnerID;
        var divToHide = "#LogLoader";
        var divToShow = "#page-contents";
        reload(urlToLoad, divToHide, divToShow);
    }
    function stripHTML(dirtyString) {
        var container = document.createElement('div');
        container.innerHTML = dirtyString;
        return container.textContent;
    }
    
    function openModalEdit(el) {
        var target = $(el).attr('href');
        $("#EditEmployeeModal .modal-content").load(target, function () {
            $("#EditEmployeeModal").modal("show");
        });
        $('#EditEmployeeModal').on('hidden.bs.modal', function (e) {
            e.preventDefault();
            $('#page-contents').hide();
            $('#LogLoader').fadeIn('slow');
            reLoad();
        });
    }

    function openModalNumberEdit(el) {
        var target = $(el).attr('href');
        $("#EditEmployeeNumberModal .modal-content").load(target, function () {
            $("#EditEmployeeNumberModal").modal("show");
        });
        $('#EditEmployeeNumberModal').on('hidden.bs.modal', function (e) {
            e.preventDefault();
            $('#page-contents').hide();
            $('#LogLoader').fadeIn('slow');
            reLoad();
        });
    }

    function openDeleteEmployeeModal(el) {
        var target = $(el).attr('href');
        $("#DeleteEmployeeModal .modal-content").load(target, function () {
            $("#DeleteEmployeeModal").modal("show");
        });
        $('#DeleteEmployeeModal').on('hidden.bs.modal', function (e) {
            e.preventDefault();
            $('#page-contents').hide();
            $('#LogLoader').fadeIn('slow');
            reLoad();
        });
    }

    function openCreateEmployeeModal(el) {
        var target = $(el).attr('href');
        $("#CreateEmployeeModal .modal-content").load(target, function () {
            $("#CreateEmployeeModal").modal("show");
        });
        $('#CreateEmployeeModal').on('hidden.bs.modal', function (e) {
            e.preventDefault();
            $('#page-contents').hide();
            $('#LogLoader').fadeIn('slow');
            reLoad();
        });
    }
    
    $(function () {
        $('[data-tooltip="tooltip"]').tooltip();
    });
</script>

