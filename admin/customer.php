<?php
$first_day_this_month = date('01/m/Y'); // hard-coded '01' for first day
$last_day_this_month = date('t/m/Y');
?>

<section class='reports'>
    <div class="row">
        <div class="col-lg-12">
            <h3 id="admin-head"><i class='fa fa-tags'></i> Customer List  </h3>
            <div class="filters admin-filters">
                <input type="text" id="from" name="from" class="form-control" value="<?php echo $first_day_this_month; ?>" readonly="true"/>
                <input type="text" id="to" name="to"  class="form-control" value="<?php echo $last_day_this_month; ?>" readonly="true"/>                
                <button type="submit" class="btn btn-warning btn-circle" id="refresh" 
                        data-tooltip="tooltip" data-placement="bottom" title="Reload"> 
                    <i class="fa fa-refresh"></i> 
                </button>
            </div>
        </div>
    </div><!--end row-->

    <div class="loader" id="LogLoader"></div>

    <div id="page-contents"></div>
</section>

<script src="../includes/js/custom.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {

        $('#page-contents').hide(function () {
            var addonParams = setLoad();
            var urlToLoad = "admin/customerextend.php?" + addonParams;
            var divToHide = "#LogLoader";
            var divToShow = "#page-contents";
            reload(urlToLoad, divToHide, divToShow);
        });

        $("#from").datepicker({
            showOn: "button",
            buttonImage: "https://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
            buttonImageOnly: true, dateFormat: 'dd/mm/yy',
            numberOfMonths: 2,
            onSelect: function (selected) {
                $("#to").datepicker("option", "minDate", selected);
            }
        });

        $("#to").datepicker({
            showOn: "button",
            buttonImage: "https://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
            buttonImageOnly: true, dateFormat: 'dd/mm/yy',
            numberOfMonths: 2,
            onSelect: function (selected) {
                $("#from").datepicker("option", "maxDate", selected);
            }
        });

        $('#refresh').click(function () {
            var toLoad = setLoad();
            var addonParams = setLoad();
            var urlToLoad = "admin/customerextend.php?" + addonParams;
            var divToHide = "#LogLoader";
            var divToShow = "#page-contents";

            $(divToShow).hide();
            $(divToHide).fadeIn('slow');
            reload(urlToLoad, divToHide, divToShow);
        });
    });

    function reload(urlToLoad, divToHide, divToShow) {
        var urldata = urlToLoad;
        var page = divToShow;
        $.get(urldata).success(
                function (response, status, jqXhr) {
                    $(divToHide).hide();
                    $(page).fadeIn('slow');
                    $(page).empty().append(response);
                }).error(function (response, status, jqXhr) {
            $(divToHide).show();
            reload(urldata, divToHide, divToShow);
        }).complete(function (response, status, jqXhr) {
        });
    }
    ;

    function reLoad() {
        var addonParams = setLoad();
        var urlToLoad = "admin/customerextend.php?" + addonParams;
        var divToHide = "#LogLoader";
        var divToShow = "#page-contents";
        reload(urlToLoad, divToHide, divToShow);
    }

    function openShowCustomerSurvey(el) {
        var target = $(el).attr('href');
        $("#ShowCustomerSurvey .modal-content").load(target, function () {
            $("#howCustomerSurvey").modal("show");
        });

        $('#ShowCustomerSurvey').on('hidden.bs.modal', function (e) {
            e.preventDefault();
            $('#page-contents').hide();
            $('#LogLoader').fadeIn('slow');
            reLoad();
        });
    }

    // openShowCustomerSurvey
    function setLoad() {
        var from = $("#from").val().split("/").reverse().join("-");
        var to = $("#to").val().split("/").reverse().join("-");
        var storeID = $("#storeOwnerID").val();
        if ($('#ShareType').prop('checked')) {
            var toLoad = 'to=' + to + '&from=' + from;
        }
        else {
            var toLoad = 'to=' + to + '&from=' + from;
        }
        return toLoad;
    }

    $(function () {
        $('[data-tooltip="tooltip"]').tooltip();
    });
</script>
