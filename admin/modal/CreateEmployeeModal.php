<?php
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/EmployeeFunction.php');
//include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/StoreFunction.php');
//$varID =  $_GET['userid'];
session_start();
$param = 'All';
if ($_SESSION['UserRoleID'] != 1) {
    $param = 'Own';
}
$storeList = SelectStoreList($param);
$roleList = SelectEmpRoleList();
//echo "<pre>";
//print_r($storeList);
//var_dump($userData);
//var_dump($roleList);
?>
<script type="text/javascript">

    $(function() {
        $("#empDiv,#ajaxmsgs,#elementsToOperateOn0").hide();
        $("#UserName,#UserPassword").val('');
    }); // default hide divs for modal open


    $(document).ready(function() {
        $('#includeEmpNum').change(function() {
            if (!this.checked) {
                $('#empDiv').fadeOut('slow');
                $('#empDiv :input').attr('disabled', true).hide();
            } else {
                $('#empDiv').fadeIn('slow')
                        .find('#elementsToOperateOn0 :input').prop('disabled', false).show();
                document.getElementById('elementsToOperateOn0').style.display = 'block';
            }
        });
    }); // check box toggle for div

    $('#setStoreLeader').change(function() {
        if (!this.checked) {
            $(this).prop('checked', false);
        } else {
            var username = $('#UserName').val();
            var password = $('#UserPassword').val();
            var setLeaderCheck = $('#setLeaderCheck');
            if (username != '' && password != '') {
                $(this).prop('checked', true);
                setLeaderCheck.html('');
            } else {
                $(this).prop('checked', false);
                setLeaderCheck.html('<span class="erroruser">Username & Password needed</span>');
            }
        }
    });

    $('#btnSubmit').on('click', function(event) {
        if ($("#storeFM")[0].checkValidity()) {
            var url = 'admin/function/EmployeeFunctionCaller.php?module=CreateEmployee';
            var mydata = $("#storeFM").serialize();

            $.ajax({
                url: url,
                type: 'get',
                data: mydata,
                dataType: 'json',
                success: function(result) {
                    document.getElementById('ajaxmsgs').style.display = 'block';
                    if (result.type == 'Success') {
                        $('#ajaxmsgs').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong>" + result.message + "</div>");
                        $("#storeFM :input").attr("disabled", true);
                        $("#cancel").attr("disabled", false).html('Close');
                    } else {
                        $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong>" + result.message + "</div>");
                    }
                    //alert('success error');
                },
                error: function() {
                    document.getElementById('ajaxmsgs').style.display = 'block';
                    $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
                    //alert('here error');
                }
            });
        } else {
            $("#storeFM").find(':submit').click();
        }
        event.preventDefault();
    }); // ajax button form submit

    function isEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    $('#UserName').keyup(function() { // Keyup function for check the user action in input
        var Username = $(this).val(); // Get the username textbox using $(this) or you can use directly $('#username')
        var UsernameAvailResult = $('#username_avail_result'); // Get the ID of the result where we gonna display the results
        if (Username.length > 6) { // check if greater than 5 (minimum 6)

            if (isEmail((Username))) {
                UsernameAvailResult.html('Loading..'); // Preloader, use can use loading animation here
                var UrlToPass = 'action=username_availability&username=' + Username;
                $.ajax({// Send the username val to another checker.php using Ajax in POST menthod
                    type: 'POST',
                    data: UrlToPass,
                    url: 'admin/function/CheckUserName.php/',
                    success: function(responseText) { // Get the result and asign to each cases
                        if (responseText == 0) {
                            UsernameAvailResult.html('<span class="successuser">Username name available</span>');
                        } else if (responseText > 0) {
                            UsernameAvailResult.html('<span class="erroruser">Username already taken</span>');
                        } else {
                            alert('Problem with sql query');
                        }
                    }
                });
            } else {
                UsernameAvailResult.html('<span class="erroruser">Email used is invalid!</span>');
            }

        }
        if (Username.length == 0) {
            UsernameAvailResult.html('');
        }
    });

    $('#UserPassword, #UserName').keydown(function(e) { // Dont allow users to enter spaces for their username and passwords
        if (e.which == 32) {
            return false;
        }
    });

    function showDiv(cliked_id) {
        var fieldNum = cliked_id.charAt(cliked_id.length - 1);
        var newID = parseInt(fieldNum) + 1;
        var fieldID = "elementsToOperateOn-" + newID;
        var idfield = "#" + fieldID + " :input";
        document.getElementById(fieldID).style.display = 'block';
        $(idfield).prop("disabled", false).fadeIn('slow');
    } // unhide hidden div and enable input fields for shared to user

    function hideDiv(cliked_id) {
        var fieldNum = cliked_id.charAt(cliked_id.length - 1);
        var newID = parseInt(fieldNum);
        var fieldID = "elementsToOperateOn-" + newID;
        var idfield = "#" + fieldID + " :input";
        document.getElementById(fieldID).style.display = 'none';
        $(idfield).prop("disabled", true).fadeOut('slow');
    } // hide hidden div and disable input field for shared to user

    $(function() {
        $('[data-tooltip="tooltip"]').tooltip();
    });
</script>
<style type="text/css">
    .successuser{
        color:#009900;
    }
    .erroruser{
        color:#F33C21;
    }
    .username_avail_result.setLeaderCheck{
        display:block;
        width:180px;
    }
</style>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Add Employee</h4>
</div>
<form class="form-horizontal" role="form" id="storeFM" autocomplete="off"><!-- /modal-header -->
    <div class="modal-body">
        <input style="display:none" type="text" name="fakeusernameremembered"/>
        <input style="display:none" type="password" name="fakepasswordremembered"/>
        <div class="form-group">
            <label class="control-label col-sm-3" for="">User Name:</label>
            <div class="col-sm-6">
                <input type="email" class="form-control" id="UserName" name="UserName" placeholder="Enter User Name"  required minlength="5">
                <div class="username_avail_result" id="username_avail_result"></div>
            </div>
        </div> <!-- /div for store name -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="">Password:</label>
            <div class="col-sm-6">
                <input type="password" class="form-control" id="UserPassword" name="UserPassword" placeholder="Enter Password Here" minlength="5" required>
            </div>
        </div> <!-- /div for password -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="EmpFirstName">First Name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="EmpFirstName" name="EmpFirstName" placeholder="Enter Employee's First Name" required >
            </div>
        </div> <!-- /div for store name -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="EmpLastName">Last Name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="EmpLastName" name="EmpLastName" placeholder="Enter Employee's Last Name" required>
            </div>
        </div> <!-- /div for store code -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="EmpEmail">Email:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="EmpEmail" name="EmpEmail" placeholder="Enter Employee's Email Address" >
            </div>
        </div> <!-- /div for store code -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="EmpPhone">Phone:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="EmpPhone" name="EmpPhone" placeholder="Enter Employee's Phone No" >
            </div>
        </div> <!-- /div for store code -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="EmpMobile">Mobile:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="EmpMobile" name="EmpMobile" placeholder="Enter Employee's Mobile No" >
            </div>
        </div> <!-- /div for store code -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="EmpRoleID">Employee Role:</label>
            <div class="col-sm-6">
                <select name="EmpRoleID" class="form-control" required>
                    <option value="">Please select</option>
                    <?php
                    foreach ($roleList as $value) {
                        echo '<option value="' . $value['f_EmpRoleID'] . '">' . $value['f_EmpRoleName'] . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div> <!-- /div for store owner -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="StoreLeader"></label>
            <div class="col-sm-6">
                Set as Store Leader? <input type="checkbox" id="setStoreLeader" name="setStoreLeader" class="btn-large"
                                            data-tooltip="tooltip" data-placement="right" title="Grant access to report pages"/>
                <div class="setLeaderCheck" id="setLeaderCheck"></div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="StoreOwnerID"></label>
            <div class="col-sm-6">
                Add Employee number? <input type="checkbox" id="includeEmpNum" name="includeEmpNum" class="btn-large"
                                            data-tooltip="tooltip" data-placement="right" title="Store assignment" required>
            </div>
        </div>
        <div id="empDiv"  style="display:none">
            <div class="form-group"  id="elementsToOperateOn0" class="col-sm-6">
                <label class="control-label col-sm-3" for="email">Employee Store & EmpID To:</label>
                <div  class="col-sm-6">
                    <select name="ToWhatStoreID[]" class="form-control" disabled required>
                        <option value="">Please select</option>
                        <?php
                        foreach ($storeList as $value) {
                            echo '<option value="' . $value['f_StoreID'] . '">' . $value['f_StoreListName'] . '</option>';
                        }
                        ?>
                    </select>
                    <input type="text" class="form-control" name="EmpNum[]" placeholder="Enter Employee Number Here" disabled required/>
                </div>

                <button id="bntShow0" class="btn btn-sm btn-info" type="button" onclick="showDiv(this.id)">+</button>
            </div>   <!-- /div for share store owner -->

            <?php for ($loop = 1; $loop < 10; $loop++) { ?>
                <div class="form-group" id="elementsToOperateOn-<?php echo $loop; ?>" style="display:none" >
                    <label class="control-label col-sm-3" for="email"></label>
                    <div  class="col-sm-6">
                        <select name="ToWhatStoreID[]" class="form-control" disabled required>
                            <option value="">Please select</option>
                            <?php
                            foreach ($storeList as $value) {
                                echo '<option value="' . $value['f_StoreID'] . '">' . $value['f_StoreListName'] . '</option>';
                            }
                            ?>
                        </select>
                        <input type="text" class="form-control" name="EmpNum[]" placeholder="Enter Employee Number Here" disabled required/>
                    </div>  <button id="btnShow<?php echo $loop; ?>" class="btn btn-sm btn-info" type="button" onclick="showDiv(this.id)">+</button>
                    <button id="btnHide<?php echo $loop; ?>" class="btn btn-sm btn-warning" type="button" onclick="hideDiv(this.id)">-</button>
                </div>  <!-- /elementsToOperateOn div loop -->
            <?php } ?> <!-- loop for hidden dropdown -->
        </div> <!-- /share div -->
        <div id="ajaxmsgs"></div>  <!-- ajaxmsg div -->
    </div> <!-- /modal-body -->
    <div class="modal-footer">
        <input type="submit" style="visibility: hidden"/>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id="cancel">Cancel</button>
        <button type="button" class="btn btn-sm btn-primary"  id="btnSubmit" name="btnSubmit">Submit</button>
        <button type="reset" class="btn btn-sm btn-danger" >Reset</button>
    </div> <!-- /modal-footer -->
</form>  <!-- /form -->

