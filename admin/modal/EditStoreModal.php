<?php
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/UserFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/StoreFunction.php');
session_start();
$role = $_SESSION['UserRoleID'];

$storeID = $_GET['storeID'];
$storeOwnerID = $_GET['SharedOwnerUserID'];
$storeData = SelectStore('StoreOwner', $storeOwnerID);
$storeList = SelectStoreList();
$owner = SelectUser('Owner');
//var_dump($storeData);

$sqlGetStoreType = "SELECT * FROM t_storetype";
$resultStoreType = mysql_query($sqlGetStoreType, $connection);
while ($rowStoreType = mysql_fetch_assoc($resultStoreType)) {
    $storeType[] = $rowStoreType;
}

foreach ($storeData as $newData) {
    $mergeStoreData[$newData['SharedStoreID']]['StoreID'] = $newData['SharedStoreID'];
    $mergeStoreData[$newData['SharedStoreID']]['SharedByUserName'] = $newData['SharedByUserName'];
    $mergeStoreData[$newData['SharedStoreID']]['SharedStoreName'] = $newData['SharedStoreName'];
    $mergeStoreData[$newData['SharedStoreID']]['SharedByUserID'] = $newData['SharedByUserID'];
    $mergeStoreData[$newData['SharedStoreID']]['SharedToUserID'] = $newData['SharedToUserID'];
    $mergeStoreData[$newData['SharedStoreID']]['ShowOnReports'] = $newData['f_ShowOnReports'];
    $mergeStoreData[$newData['SharedStoreID']]['StoreType'] = $newData['f_StoreTypeName'];
    $mergeStoreData[$newData['SharedStoreID']]['f_StoreTypeID'] = $newData['f_StoreTypeID'];
    $mergeStoreData[$newData['SharedStoreID']]['SharedCompanyCode'] = $newData['SharedCompanyCode'];
    $mergeStoreData[$newData['SharedStoreID']]['f_Shared'][] = array('SharedStoreID' => $newData['f_SharedStoreID'], 'SharedtoStoreID' => $newData['f_SharedToStoreID'], 'SharedToStoreName' => $newData['SharedToStoreName']);
    //$new = array_merge($newData2,$newData2);
}
?>
<script type="text/javascript">


    $("#ajaxmsgs").hide();

    var button = $('#btnSubmit');
    var orig = [];

    $.fn.getType = function() {
        return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
    }

    $("#EditStoreForm :input").each(function() {
        var type = $(this).getType();
        var tmp = {
            'type': type,
            'value': $(this).val()
        };
        if (type == 'radio') {
            tmp.checked = $(this).is(':checked');
        }
        if (type == 'checkbox') {
            if ($(this).attr('id') == 'showonreports' && $(this).is(':checked')) {
                tmp.value = $(this).val();
            } else {
                tmp.value = '';
            }

        }

        orig[$(this).attr('id')] = tmp;
    });

    $('#EditStoreForm').bind('change keyup', function() {

        var disable = true;
        $("#EditStoreForm :input").each(function() {
            var type = $(this).getType();
            var id = $(this).attr('id');
            if (type == 'text' || type == 'select') {
                disable = (orig[id].value == $(this).val());

            } else if (type == 'radio') {
                disable = (orig[id].checked == $(this).is(':checked'));
            } else if (type == 'checkbox') {
                var chkVal = '';
                if (orig[id].value == 'on') {
                    chkVal = true;
                } else {
                    chkVal = false;
                }
                disable = (chkVal == $(this).is(':checked'));
            }

            if (!disable) {
                return false; // break out of loop
            }
        });

        button.prop('disabled', disable);
    });


    $('#resetPrompt').click(function() {
        //$('#input1').attr('name', 'other_amount');
        button.prop('disabled', true);
    });
    $('#btnSubmit').on('click', function(event) {
        //alert('here');
        if ($("#EditStoreForm")[0].checkValidity()) {
            var url = 'admin/function/StoreFunctionCaller.php?module=EditStore';

            var mydata = $("#EditStoreForm").serialize();
            $.ajax({
                url: url,
                type: 'get',
                data: mydata,
                dataType: 'json',
                success: function(result) {
                    document.getElementById('ajaxmsgs').style.display = 'block';
                    if (result.type == 'Success') {
                        $('#ajaxmsgs').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong><br>" + result.message + "</div>");
                        $("#EditStoreForm :input").attr("disabled", true);
                        $("#cancel").attr("disabled", false).html('Close');
                    } else {
                        $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong><br>" + result.message + "</div>");
                    }
                    //alert('success error');
                },
                error: function(result) {
                    document.getElementById('ajaxmsgs').style.display = 'block';
                    $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!<br> " + result.message + "</strong></div>");
                    //alert('here error');
                }
            });
        } else {
            $("#EditStoreForm").find(':submit').click();
        }
        event.preventDefault();
    }); // ajax button form submit
    //  });

    $(function() {
        $('[data-tooltip="tooltip"]').tooltip();
    });
</script>
<style type="text/css">
    .successstore{
        color:#009900;
    }
    .errorstore{
        color:#F33C21;
    }
    .storename_avail_result .checkPass{
        display:block;
        width:180px;
    }</style>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Edit Store</h4>
</div>
<form class="form-horizontal" role="form" id="EditStoreForm"><!-- /modal-header -->
    <div class="modal-body">

        <div class="form-group">
            <label class="control-label col-sm-3" for="StoreListName">Store Name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="StoreListName" name="StoreListName" placeholder="Enter Store Name" value="<?php echo $mergeStoreData[$storeID]['SharedStoreName']; ?>" required>
                <div class="storename_avail_result" id="storename_avail_result"></div>
            </div>
        </div> <!-- /div for store name -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="StoreCompanyCode">Store Code:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="StoreCompanyCode" name="StoreCode" placeholder="Enter Store Code" value="<?php echo $mergeStoreData[$storeID]['SharedCompanyCode']; ?>" required>
            </div>
        </div> <!-- /div for first name -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="StoreOwnerID">Store Owner:</label>
            <div class="col-sm-6">
                <?php if ($role != '1') { ?> <input type="hidden" name="StoreOwnerID" value="<?php echo $storeOwnerID; ?>" />  <?php } ?>
                <select name="StoreOwnerID" class="form-control" <?php
                if ($role != '1') {
                    echo "disabled";
                }
                ?> required id="StoreOwnerID">
                    <option value="">Please select</option>
                    <?php foreach ($owner as $value) { ?>
                        <option value="<?php echo $value['f_UserID']; ?>" <?php
                        if ($value['f_UserID'] == $storeOwnerID) {
                            echo "selected='selected'";
                        }
                        ?> > <?php echo $value['f_UserFirstName'] . " " . $value['f_UserLastName']; ?></option>
                            <?php } ?>
                </select>
            </div>
        </div> <!-- /div for shared to store owner -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="StoreTypeID">Store Type:</label>
            <div class="col-sm-6">
                <?php if ($role != '1') { ?> <input type="hidden" name="StoreTypeID" value="<?php echo $mergeStoreData[$storeID]['f_StoreTypeID']; ?>" />  <?php } ?>
                <select name="StoreTypeID" class="form-control"  <?php
                if ($role != '1') {
                    echo "disabled";
                }
                ?> required  id="StoreTypeID">
                    <option value="">Please Select</option>
                    <?php foreach ($storeType as $storeTypeValue) { ?>
                        <option value="<?php echo $storeTypeValue['f_StoreTypeID']; ?>"
                        <?php
                        if ($storeTypeValue['f_StoreTypeID'] == $mergeStoreData[$storeID]['f_StoreTypeID']) { {
                                echo "selected='selected'";
                            }
                        }
                        ?> ><?php echo $storeTypeValue['f_StoreTypeName']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div> <!-- /div for store code -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="StoreOwnerID"></label>
            <div class="col-sm-6">
                Show store records on report pages?
                <input type="checkbox" id="showonreports" name="showonreports"  class="btn-large"  data-tooltip="tooltip" data-placement="bottom" title="Yes / No"
                <?php
                if ($mergeStoreData[$storeID]['ShowOnReports'] == 1) {
                    echo "checked='checked'";
                }
                ?> >
            </div>
        </div>


        <input type="hidden" name="StoreListID" value="<?php echo $storeID; ?>" />
        <div id="ajaxmsgs"></div>  <!-- ajaxmsg div -->
    </div> <!-- /modal-body -->
    <div class="modal-footer">
        <input type="submit" style="visibility: hidden"/>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id="cancel">Cancel</button>
        <button type="button" class="btn btn-sm btn-primary"  id="btnSubmit" name="btnSubmit"  disabled="">Submit</button>
        <button type="reset" class="btn btn-sm btn-danger" id="resetPrompt" >Reset</button>

    </div> <!-- /modal-footer -->
</form>  <!-- /form -->

