<?php
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');
//include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/UserFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/StoreFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/EmployeeFunction.php');
$empID = $_GET['empID'];

session_start();
$param = 'All';
if($_SESSION['UserRoleID'] != 1){
    $param = 'Own';
}
$storeList = SelectStoreList($param);
$empData = SelectEmployee($empID);



foreach ($empData as $newData) {

    $mergeEmployeeData['EmpNo'][] = array('StoreListID' => $newData['f_StoreID'],
        'EmpNo' => $newData['f_EmpPNumber'],
        'StoreListName' => $newData['f_StoreListName'],
        'EmpNumberID' => $newData['f_EmpNumberID']
    );
    $beenAdded[] = $newData['f_StoreID'];
}

?>
<script type="text/javascript">

    $(function () {
        $("#ajaxmsgs,#ajaxmsgsedit").hide();
    }); // default hide divs for modal open

    $(function () {
        $('[data-tooltip="tooltip"]').tooltip();
    });
</script>

<script type="text/javascript" language="javascript"  src="admin/js/EditEmployeeNumberModal.js"></script>
<style type="text/css">  
    .successuser{
        color:#009900;
    }
    .erroruser{
        color:#F33C21;
    }
    .empname_avail_result .checkPass{
        display:block;
        width:180px;
    }
    .redBackground{
        background-color:red;
    }
</style>


<div id="modal-body" style="padding: 20px;">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
        <li class="active"><a href="#Add" data-toggle="tab">Add</a></li>
        <li><a href="#Edit" data-toggle="tab" onclick="onLoadEditEmpNumberForm()">Edit</a></li>
        <li><a href="#Delete" data-toggle="tab" onclick="onLoadDeleteEmpNumberForm()">Delete</a></li>
    </ul>

    <div id="my-tab-content" class="tab-content">
        <div class="tab-pane active" id="Add">
            <h4> Add Employee Number</h4>
            <form class="form-horizontal" role="form" id="AddEmpNumberForm"> 
                <div id="empDiv" >
                    <div class="form-group"  id="elementsToOperateOn0" class="col-sm-6">
                        <label class="control-label col-sm-3" for="email">Employee Store & EmpID To:</label>
                        <div  class="col-sm-6">
                            <select name="ToWhatStoreID[]" class="form-control AddShare" required id="empTo0">
                                <option value="">Select Store</option>
                                <?php
                                foreach ($storeList as $value) {
                                    if (in_array($value['f_StoreID'], $beenAdded)) {
                                        continue;
                                    }
                                    echo '<option value="' . $value['f_StoreID'] . '">' . $value['f_StoreListName'] . '</option>';
                                }
                                ?>
                            </select>  
                            <input type="text" class="form-control" name="EmpNum[]" placeholder="Enter Employee Number Here" required/>
                        </div> 

                        <button id="bntShow0" class="btn btn-sm btn-info" type="button" onclick="showDiv(this.id)"
                                data-tooltip="tooltip" data-placement="right" title="Add another">+</button>
                    </div>   <!-- /div for share store owner -->

                    <?php for ($loop = 1; $loop < 10; $loop++) { ?> 
                        <div class="form-group" id="elementsToOperateOn-<?php echo $loop; ?>" style="display:none" >
                            <label class="control-label col-sm-3" for="email"></label>
                            <div  class="col-sm-6">
                                <select name="ToWhatStoreID[]" class="form-control AddShare" disabled required id="empTo<?php echo $loop;?>">
                                    <option value="">Please select</option>
                                    <?php
                                    foreach ($storeList as $value) {
                                        echo '<option value="' . $value['f_StoreID'] . '">' . $value['f_StoreListName'] . '</option>';
                                    }
                                    ?>
                                </select>
                                <input type="text" class="form-control" name="EmpNum[]" placeholder="Enter Employee Number Here" disabled required/>
                            </div>  
                            <button id="btnShow<?php echo $loop; ?>" class="btn btn-sm btn-info" type="button" onclick="showDiv(this.id)"
                                    data-tooltip="tooltip" data-placement="right" title="Add another">+</button>
                            <button id="btnHide<?php echo $loop; ?>" class="btn btn-sm btn-warning" type="button" onclick="hideDiv(this.id)"
                                    data-tooltip="tooltip" data-placement="right" title="Remove">-</button>
                        </div>  <!-- /elementsToOperateOn div loop -->
                    <?php } ?> <!-- loop for hidden dropdown -->
                </div> <!-- /share div -->

                <input type="hidden" name="EmpID" value="<?php echo $empData[0]['f_EmpID']; ?>"/>

                <div id="ajaxmsgsadd"></div>

                <div class="modal-footer">
                       <input type="submit" style="visibility: hidden"/>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id="AddEmpNumberCancel">Cancel</button>
                    <button type="button" class="btn btn-sm btn-primary" id="AddEmpNumberSubmit" name="AddEmpNumberSubmit" disabled="">Submit</button>
                    <button type="reset" class="btn btn-sm btn-danger" id="AddEmpNReset">Reset</button>
                </div> <!-- /modal-footer -->	
            </form>
        </div>
        
        <div class="tab-pane" id="Edit">
            <h4>Edit Employee Number</h4>
            <?php if (!is_null($mergeEmployeeData['EmpNo'][0]['StoreListID'])) { ?>
                <form class="form-horizontal" role="form" id="EditEmpNumberForm"> 
                    <?php  $i = 0;  foreach ($mergeEmployeeData['EmpNo'] as $sharedData) { ?>
                        <div class="row">
                            <label class="control-label col-sm-2" for="StoreOwnerID"></label>
                            <div  class="col-sm-6">
                                <select name="EmpStoreID[]" class="form-control EditShare" id="EmpStore<?php echo $i; ?>" required>
                                    <option value="">Please select</option>
                                    <?php foreach ($storeList as $value) { ?>
                                        <option value="<?php echo $value['f_StoreID']; ?>" <?php
                                        if ($sharedData['StoreListID'] == $value['f_StoreID']) {
                                            echo "selected='selected'";
                                        }
                                        ?> > <?php echo $value['f_StoreListName']; ?></option>
                                            <?php } ?>
                                </select> 
                                <input type="text" class="form-control" name="EmpNo[]" value="<?php echo $sharedData['EmpNo']; ?>" id="empNum<?php echo $i;?>" required>
                            </div>                            
                            <input type="hidden" name="EmpNumberID[]" value="<?php echo $sharedData['EmpNumberID']; ?>"  />
                        </div></br>
                    <?php $i++; } ?>
                    <div id="ajaxmsgsedit"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"  id="EditEmpNumberCancel">Cancel</button>
                        <button type="button" class="btn btn-sm btn-primary"  id="EditEmpNumberSubmit" name="EditEmpNumberSubmit" disabled="">Submit</button>                    
                        <button type="reset" class="btn btn-sm btn-danger" >Reset</button>
                    </div> <!-- /modal-footer -->	
                </form>
            <?php } else { ?>
                <div class="well text-center"> No Employee Number Available</div>    
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                </div>
            <?php } ?>

        </div>
        <div class="tab-pane" id="Delete">
            <h4>Delete Employee Number</h4>
            <?php if (!is_null($mergeEmployeeData['EmpNo'][0]['StoreListID'])) { ?>
                <form class="form-horizontal" role="form" id="DeleteEmpNumberForm"> 
                    <?php foreach ($mergeEmployeeData['EmpNo'] as $sharedData) { ?>
                        <div class="row">
                            <label class="control-label col-sm-2" for=""></label>
                            <div class="col-lg-6" data-toggle="tooltip" data-placement="left" title="Check to include in delete"> 
                                <div class="input-group" >
                                    <span class="input-group-addon">
                                        <input type="checkbox" name="toDelete[]" value="<?php echo $sharedData['EmpNumberID']; ?>"
                                               data-tooltip="tooltip" data-placement="left" title="Yes / No">
                                    </span>
                                    <select name="EmpStoreID[]" class="form-control" disabled>
                                        <option value="">Please select</option>
                                        <?php foreach ($storeList as $value) { ?>
                                            <option value="<?php echo $value['f_StoreID']; ?>" <?php
                                            if ($sharedData['StoreListID'] == $value['f_StoreID']) {
                                                echo "selected='selected'";
                                            }
                                            ?> > <?php echo $value['f_StoreListName']; ?></option>
                                                <?php } ?>
                                    </select> 
                                    <input type="text" class="form-control" name="EmpNo[]"  value="<?php echo $sharedData['EmpNo']; ?>" disabled>
                                </div><!-- /input-group -->
                            </div><!-- /.col-lg-6 -->                           
                        </div></br>
                    <?php } ?>
                    <div id="ajaxmsgsdelete"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"  id="DeleteEmpNumberCancel">Cancel</button>
                        <button type="button" class="btn btn-sm btn-primary"  id="DeleteEmpNumberSubmit" name="DeleteEmpNumberSubmit">Submit</button>                    
                        <button type="reset" class="btn btn-sm btn-danger" >Reset</button>
                    </div> <!-- /modal-footer -->	
                </form> 
            <?php } else { ?>
                <div class="well text-center"> No Employee Number Available</div>    
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                </div>
            <?php } ?>
        </div>

    </div>
</div>


<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#tabs').tab();
    });
</script>    

