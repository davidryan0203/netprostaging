<?php
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/UserFunction.php');
$varID = $_GET['userid'];

$userData = SelectUser($varID);
$roleList = SelectUserRoleList();
session_start();
$role = $_SESSION['UserRoleID'];
//var_dump($userData);
?>
<script type="text/javascript">
    $(document).ready(function () {
        $("#ajaxmsgs").hide();
        var button = $('#btnSubmit');
        var orig = [];

<?php if ($role != 1) { ?>
            $(':input').not('#Password,#confirmPassword,#changePass').attr({readonly: true, onfocus: 'this.blur'});

<?php } ?>

        $.fn.getType = function () {
            return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
        }

        $("#EditUserForm :input").each(function () {
            var type = $(this).getType();
            if ($(this).val().length === 0) {
                var tmp = {
                    'type': type,
                    'value': ''
                };
            } else {
                var tmp = {
                    'type': type,
                    'value': $(this).val()
                };
            }
            if (type == 'radio') {
                tmp.checked = $(this).is(':checked');
            }

            if (type == 'checkbox') {
                tmp.value = $(this).is(':checked');
            }
            orig[$(this).attr('id')] = tmp;
        });

        $('#EditUserForm').bind('change keyup', function () {

            var disable = true;
            $("#EditUserForm :input").each(function () {
                var type = $(this).getType();
                var id = $(this).attr('id');

                if (type == 'text' || type == 'select' || type == 'password') {
                    disable = (orig[id].value == $(this).val());

                } else if (type == 'radio') {
                    disable = (orig[id].checked == $(this).is(':checked'));
                }
                else if (type == 'checkbox' && $(this).attr('id') != 'changePass') {
                    var chkVal = '';
                    if (orig[id].value == 'on') {
                        chkVal = true;
                    }
                    else {
                        chkVal = false;
                    }
                    disable = (chkVal == $(this).is(':checked'));
                }

                if (!disable) {
                    return false; // break out of loop
                }

            });

            button.prop('disabled', disable);
        });

        $('#EditUserReset').click(function () {
            //$('#input1').attr('name', 'other_amount');
            button.prop('disabled', true);
        });

        var conPass = '';

        $('#changePass').change(function () {
            conPass = {match: null, length: null, capital: null, letter: null, number: null};
            $("#pswd_info").find(".fa-times").removeClass('fa fa-times');
            $("#pswd_info").find(".fa-check").removeClass('fa fa-check');
            if (!this.checked) {
                $('#confirmPassword').fadeOut('slow');
                $('#Password').prop('disabled', true);
                $('#confirmPassword').prop('disabled', true);
                $('#checkPass').fadeOut('slow');
                $('#Password').val('');
                $('#confirmPassword').val('');
                $('#passvalidation').hide('slow');
            }
            else {
                $('#confirmPassword').fadeIn('slow');
                $('#confirmPassword').prop('disabled', false);
                $('#Password').prop('disabled', false);
                document.getElementById('confirmPassword').style.display = 'block';
                $('#confirmPassword').val('');
                $('#Password').val('');
                $('#passvalidation').show('slow');
            }

        });

        $('#Password,#confirmPassword').keyup(function (e) {
            var pass1 = $('#Password').val();
            var pass2 = $('#confirmPassword').val();
            var div = $('#checkPass');

            if (pass1 == pass2) {
                conPass.match = true;
                //  div.html('');
                $('#match').removeClass('fa fa-times').addClass('fa fa-check');
            }
            else if (pass1 != pass2) {
                conPass.match = false;
                $('#match').removeClass('fa fa-check').addClass('fa fa-times');
                // div.html('<span class="erroruser">Password and Confirm Password not match</span>');

            }

            //validate the length
            if (pass1.length < 8) {
                $('#length').removeClass('fa fa-check').addClass('fa fa-times');
                conPass.length = false;
            } else {
                $('#length').removeClass('fa fa-times').addClass('fa fa-check');
                conPass.length = true;

            }
            //validate letter
            if (pass1.match(/[A-z]/)) {
                $('#letter').removeClass('fa fa-times').addClass('fa fa-check');
                conPass.letter = true;
            } else {
                $('#letter').removeClass('fa fa-check').addClass('fa fa-times');
                conPass.letter = false;
            }

            //validate capital letter
            if (pass1.match(/[A-Z]/)) {
                $('#capital').removeClass('fa fa-times').addClass('fa fa-check');
                conPass.capital = true;
            } else {
                $('#capital').removeClass('fa fa-check').addClass('fa fa-times');
                conPass.capital = false;
            }

            //validate number
            if (pass1.match(/\d/)) {
                $('#number').removeClass('fa fa-times').addClass('fa fa-check');
                conPass.number = true;
            } else {
                $('#number').removeClass('fa fa-check').addClass('fa fa-times');
                conPass.number = false;
            }

            e.preventDefault();
        });

        $('#UserName').keyup(function () { // Keyup function for check the user action in input
            var Username = $(this).val(); // Get the username textbox using $(this) or you can use directly $('#username')
            var UsernameAvailResult = $('#username_avail_result'); // Get the ID of the result where we gonna display the results
            if (Username.length > 4) { // check if greater than 5 (minimum 6)
                UsernameAvailResult.html('Loading..'); // Preloader, use can use loading animation here
                var UrlToPass = 'action=username_availability&username=' + Username;
                $.ajax({// Send the username val to another checker.php using Ajax in POST menthod
                    type: 'POST',
                    data: UrlToPass,
                    url: 'admin/function/CheckUserName.php',
                    success: function (responseText) { // Get the result and asign to each cases
                        if (responseText == 0) {
                            UsernameAvailResult.html('<span class="successuser">Username name available</span>');
                        }
                        else if (responseText > 0) {
                            button.prop('disabled', true);
                            UsernameAvailResult.html('<span class="erroruser">Username already taken</span>');
                        }
                        else {
                            alert('Problem with sql query');
                        }
                    }
                });
            } else {
                button.prop('disabled', true);
                UsernameAvailResult.html('Enter atleast 5 characters');
            }
            if (Username.length == 0) {
                UsernameAvailResult.html('');
            }
        });

        $('#Password, #UserName').keydown(function (e) { // Dont allow users to enter spaces for their username and passwords
            if (e.which == 32) {
                return false;
            }
        });

        $('#btnSubmit').on('click', function (event) {
            if ($("#EditUserForm")[0].checkValidity()) {
                var url = 'admin/function/UserFunctionCaller.php?module=EditUser';
                var mydata = $("#EditUserForm").serialize();
                if (validateCheckPass(conPass)) {
                    $.ajax({
                        url: url,
                        type: 'get',
                        data: mydata,
                        dataType: 'json',
                        success: function (result) {
                            document.getElementById('ajaxmsgs').style.display = 'block';
                            if (result.type == 'Success') {
                                $('#ajaxmsgs').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong><br>" + result.message + "</div>");
                                $("#btnSubmit :input").attr("disabled", true);
                                $("#cancel").attr("disabled", false).html('Close');
                            }
                            else {
                                $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong><br>" + result.message + "</div>");
                            }
                            //alert('success error');
                        },
                        error: function () {
                            document.getElementById('ajaxmsgs').style.display = 'block';
                            $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
                            //alert('here error');
                        }
                    });
                }

            } else {
                $("#EditUserForm").find(':submit').click();
            }
            event.preventDefault();
        }); // ajax button form submit
    });

    $(function () {
        $('[data-tooltip="tooltip"]').tooltip();
    });


    function validateCheckPass(obj) {
        var returnValue = new Array();
        var thisValue = '';
        $.each(obj, function (key, value) {
            if (value == false) {
                returnValue.push(false);
            } else {
                returnValue.push(true);
            }
        });
        if (jQuery.inArray(false, returnValue) !== -1) {
            thisValue = false;
        } else {
            thisValue = true;
        }

        return thisValue;
    }

    $('#passvalidation').hide();
</script>
<style type="text/css">  
    .successuser{
        color:#009900;
    }
    .erroruser{
        color:#F33C21;
    }
    .username_avail_result .checkPass{
        display:block;
        width:180px;
    }</style>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Edit User</h4>
</div>	
<form class="form-horizontal" role="form" id="EditUserForm"><!-- /modal-header -->
    <div class="modal-body">

        <div class="form-group">
            <label class="control-label col-sm-3" for="UserName">User Name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="UserName" name="UserName" placeholder="Enter User Name" value="<?php echo $userData[0]['f_UserName']; ?>" required minlength="5">
                <div class="username_avail_result" id="username_avail_result"></div>
            </div>
        </div> <!-- /div for store name -->
        
        <div class="form-group">
            <label class="control-label col-sm-3" for="Password">Password:</label>
            <div class="col-sm-6">
                <input type="password" class="form-control" id="Password" name="Password"  placeholder="new password" disabled required minlength="8">
                <input type="checkbox" id="changePass" name="changePass" class="btn-large" data-tooltip="tooltip" data-placement="left" title="Yes / No">    
                Change Password? 
                <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" placeholder="confirm password" style="display:none" disabled required minlength="8">
                <div class="checkPass" id="checkPass"></div>
            </div>             
        </div> <!-- /div for password -->
        
        <div class="well" id="passvalidation">           
            <strong class="text-center text-success"><i class="fa fa fa-gears"></i> PASSWORD REQUIREMENTS </strong>

            <ul id="pswd_info"  class="list-group">
                <li class="list-group-item"> 
                    Confirm password must <strong class="text-success">match new</strong>
                    <span class="marker" id="match"></span>
                </li> 
                <li class="list-group-item"> 
                    With at least <strong class="text-success">one letter</strong>
                    <span class="marker"  id="letter"></span>
                </li>
                <li class="list-group-item"> 
                    With at least <strong class="text-success">one capital letter</strong>
                    <span class="marker"  id="capital"></span>
                </li>
                <li class="list-group-item"> 
                    With at least <strong class="text-success">one number</strong>
                    <span class="marker"  id="number"></span>
                </li>
                <li class="list-group-item"> 
                    Must be at least <strong class="text-success">8 characters</strong>
                    <span class="marker"  id="length"></span>
                </li>
            </ul>
        </div>
        
        <div class="form-group">
            <label class="control-label col-sm-3" for="FirstName">First Name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="FirstName" name="FirstName" placeholder="Enter First Name" value="<?php echo $userData[0]['f_UserFirstName']; ?>" required>
            </div>
        </div> <!-- /div for first name -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="LastName">Last Name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="LastName" name="LastName" placeholder="Enter Last Name" value="<?php echo $userData[0]['f_UserLastName']; ?>" required>
            </div>
        </div> <!-- /div for last name -->
        <?php
        $withRoleEdit = array('1');
        if (in_array($role, $withRoleEdit)) {
            ?>
            <div class="form-group">
                <label class="control-label col-sm-3" for="UserRoleID">Role:</label>
                <div class="col-sm-6">
                    <select name="UserRoleID" class="form-control" id="UserRoleID" required>
                        <option value="">Please select</option>
                        <?php foreach ($roleList as $value) { ?>
                            <option value="<?php echo $value['f_UserRoleID']; ?>" <?php
                            if ($value['f_UserRoleID'] == $userData[0]['f_UserRoleID']) {
                                echo "selected='selected'";
                            }
                            ?> ><?php echo $value['f_UserRoleName']; ?></option>
                                <?php }
                                ?>
                    </select>
                </div>
            </div> <!-- /div for store owner -->   
        <?php } else { ?> <input type="hidden" name="UserRoleID" value="<?php echo $userData[0]['f_UserRoleID']; ?>"/> <?php } ?>
        <input type="hidden" name="UserID" value="<?php echo $varID; ?>" />
        <input type="hidden" name="LoginID" value="<?php echo $userData[0]['f_UserLoginID']; ?>" />
        <div id="ajaxmsgs"></div>  <!-- ajaxmsg div -->
    </div> <!-- /modal-body -->	
    <div class="modal-footer">
        <input type="submit" style="visibility: hidden"/>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id="cancel">Cancel</button>
        <button type="button" class="btn btn-sm btn-primary"  id="btnSubmit" name="btnSubmit"  disabled="">Submit</button>
        <button type="reset" class="btn btn-sm btn-danger" id="EditUserReset" >Reset</button>
    </div> <!-- /modal-footer -->	
</form>  <!-- /form --> 

