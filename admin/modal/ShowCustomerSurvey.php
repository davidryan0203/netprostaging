<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
$customerID = $_GET['customerID'];
$from = $_GET['from'];
$to = $_GET['to'];

if (!is_null($to) && !is_null($from)) {
    $sqlWhen = " AND b.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
}

$sqlCustomerID = " AND a.f_CustomerDetailsID = '{$customerID}'";
mysql_query("SET SQL_BIG_SELECTS=1");
$sqlfeedback = "SELECT 
    CONCAT(c.f_CustomerFirstName,
            ' ',
            c.f_CustomerLastname) AS CustomerName,
    b.f_CustomerReason AS Reason,
    b.f_ActivationExtra5,
    b.f_LikelihoodToRecommendRetail AS InteractionScore,
    b.f_RecommendRate AS EpisodeScore,
    b.f_DateTimeResponseMelbourne AS Date,
    d.f_StoreListName AS StoreName,
    CASE
        WHEN e.f_EmpID IS NULL THEN b.f_ActivationExtra5
        ELSE CONCAT(f.f_EmpFirstName, ' ', f.f_EmpLastName)
    END AS EmpName
FROM
    t_surveypd a
        LEFT JOIN
    t_surveysd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN
    t_customerdetails c ON a.f_CustomerDetailsID = c.f_CustomerDetailsID
        LEFT JOIN
    t_storelist d ON b.f_StoreID = d.f_StoreID
        LEFT JOIN
    t_empnumber e ON e.f_EmpPNumber = b.f_ActivationExtra5
        LEFT JOIN
    t_emplist f ON e.f_EmpID = f.f_EmpID
WHERE 1 = 1 {$sqlWhen} {$sqlCustomerID}";

$resultfeedback = mysql_query($sqlfeedback, $connection);

while ($rowfeedback = mysql_fetch_assoc($resultfeedback)) {
    $ArrayData[] = $rowfeedback;
}
//echo "<pre>" . $sqlfeedback;
?>
<script type="text/javascript">
    (function ($) {
        $.fn.shorten = function (settings) {

            var config = {
                showChars: 255,
                minHideChars: 10,
                ellipsesText: "...",
                moreText: "more",
                lessText: "less"
            };

            if (settings) {
                $.extend(config, settings);
            }

            $(document).off("click", '.morelink');

            $(document).on({click: function () {

                    var $this = $(this);
                    if ($this.hasClass('less')) {
                        $this.removeClass('less');
                        $this.html(config.moreText);
                    } else {
                        $this.addClass('less');
                        $this.html(config.lessText);
                    }
                    $this.parent().prev().toggle();
                    $this.prev().toggle();
                    return false;
                }
            }, '.morelink');

            return this.each(function () {
                var $this = $(this);
                if ($this.hasClass("shortened"))
                    return;

                $this.addClass("shortened");
                var content = $this.html();
                if (content.length > config.showChars + config.minHideChars) {
                    var c = content.substr(0, config.showChars);
                    var h = content.substr(config.showChars, content.length - config.showChars);
                    var html = c + '<span class="moreellipses">' + config.ellipsesText + ' </span><span class="morecontent"><span>' + h + '</span> <a href="#" class="morelink">' + config.moreText + '</a></span>';
                    $this.html(html);
                    $(".morecontent span").hide();
                }
            });

        };

    })(jQuery);
    $(".more").shorten({
        "showChars": 255,
        "minHideChars": 50,
        "moreText": "See More",
        "lessText": "Less",
    });</script>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Customer Feedback</h4>    
</div>

<div class='modal-body' style="overflow-y: scroll; max-height: 50%">
    <?php
    $i = 1;
    $checkData = count($ArrayData);
    if ($checkData > 0) {
        foreach ($ArrayData as $all) {
            ?>           
            <div class = "chat-body clearfix">
                <strong class = "primary-font"> <?php echo $i . ". " . $all["CustomerName"]; ?> 
                    <span class = "chat-img fa-stack" style="margin-top: -10px">
                        <i class = "fa fa-comment  fa-stack-2x" style="color:gold; text-shadow: 1px 1px 1px #b7b7b7; "></i>
                        <strong class = "fa-stack-1x fa-stack-text fa-inverse small " style = "color:white">
                            <?php
                            //  if($type == 'episode'){
//                                        echo $all["EpisodeScore"];
//                                    }else {
                            echo $all["InteractionScore"];
//                                    }  
                            ?>
                        </strong>

                    </span>
                    <span class = "chat-img fa-stack" style="margin-top: -10px">
                        <i class = "fa fa-comment  fa-stack-2x" style="color:orange; text-shadow: 1px 1px 1px #b7b7b7; "></i>
                        <strong class = "fa-stack-1x fa-stack-text fa-inverse small " style = "color:white">
                            <?php
                            //  if($type == 'episode'){
//                                        echo $all["EpisodeScore"];
//                                    }else {
                            echo $all["EpisodeScore"];
//                                    }  
                            ?>
                        </strong>

                    </span>
                </strong>
                <div class="more">
                    <?php echo $all["Reason"]; ?>  
                </div></p>
                <footer>
                    <small class = "label label-primary text-muted pull-left">
                        <i class = "fa fa-user fa-fw"></i>  <?php echo $all['f_ActivationExtra5'] . ' - ' . $all["EmpName"]; ?> 
                    </small>

                    <small class = "text-muted pull-right">
                        <i class = "fa fa-clock-o fa-fw"></i>  <?php echo $all["StoreName"] . ", " . date("d-M-y", strtotime($all["Date"])); ?>
                    </small>
                </footer>
                <hr>
            </div>
            <?php
            $i++;
        }
    } else {
        ?>
        <div class = "chat-body clearfix">
            <strong class = "primary-font">No Data</strong>
        </div>
    <?php } ?>
</div>
