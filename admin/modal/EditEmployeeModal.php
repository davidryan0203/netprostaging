<?php
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');
//include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/UserFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/EmployeeFunction.php');
session_start();
$role = $_SESSION['UserRoleID'];
$empID = $_GET['empID'];
//$empOwnerID = $_GET['SharedOwnerUserID'];
$empData = SelectEmployee($empID);

$roleList = SelectEmpRoleList();
//$empList = SelectEmployeeList();
//$owner = SelectUser('Owner');
//var_dump($empData);

foreach ($empData as $newData) {
    $mergeStoreData['EmpID'] = $newData['f_EmpID'];
    $mergeStoreData['EmpFirstName'] = $newData['f_EmpFirstName'];
    $mergeStoreData['EmpLastName'] = $newData['f_EmpLastName'];
    $mergeStoreData['EmpRoleID'] = $newData['f_EmpRoleID'];
    $mergeStoreData['UserName'] = $newData['f_UserName'];
    $mergeStoreData['Password'] = $newData['f_Password'];
    $mergeStoreData['EmpRoleName'] = $newData['f_EmpRoleName'];
    $mergeStoreData['EmpPhone'] = $newData['f_EmpPhone'];
    $mergeStoreData['EmpMobile'] = $newData['f_EmpMobile'];
    $mergeStoreData['EmpEmail'] = $newData['f_EmpEmail'];
    $mergeStoreData['RoleID'] = $newData['f_UserRoleID'];

    //$mergeStoreData]['StoreListName']  =$newData['f_StoreListName'];
    $mergeStoreData['EmpNo'][] = array('StoreListID' => $newData['f_StoreID'],
        'EmpNo' => $newData['f_EmpPNumber'],
        'StoreListName' => $newData['f_StoreListName']
    );
    //$new = array_merge($newData2,$newData2);
}
//echo "<pre>" . $empID;
//var_dump($mergeEmployeeData);
?>
<script type="text/javascript">
    $(document).ready(function() {

<?php
$roleExlude = array(4);
if (in_array($role, $roleExlude)) {
    ?>
            $(':input').not('#Password,#confirmPassword,#changePass').attr({readonly: true, onfocus: 'this.blur'});
            var empRole = $('#EmployeeRole option:selected').text();
            $("#EmployeeRole").hide();
            $('#tempDiv').append('<input type="text" name="tempRole" class="form-control" id="tempRole" value=' + empRole + ' readonly onfocus="this.blur()">');
<?php } ?>
        $("#ajaxmsgs").hide();
        var button = $('#btnSubmit');
        var orig = [];

        $.fn.getType = function() {
            return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
        }

        $("#EditEmployeeForm :input").each(function() {
            var type = $(this).getType();
            if ($(this).val().length === 0) {
                var tmp = {
                    'type': type,
                    'value': ''
                };
            } else {
                var tmp = {
                    'type': type,
                    'value': $(this).val()
                };
            }

            if (type == 'radio') {
                tmp.checked = $(this).is(':checked');
            }

            if (type == 'checkbox') {
                if ($(this).attr('id') == 'setStoreLeader' && $(this).is(':checked')) {
                    tmp.value = $(this).val();
                } else {
                    tmp.value = '';
                }

            }
            if ($(this).attr('id') == 'setStoreLeader') {

            }
            orig[$(this).attr('id')] = tmp;
        });


        $('#EditEmployeeForm').bind('change keyup', function() {
            var disable = true;
            $("#EditEmployeeForm :input").each(function() {
                var type = $(this).getType();
                var id = $(this).attr('id');

                if (type == 'text' || type == 'select' || type == 'password') {
                    disable = (orig[id].value == $(this).val());
                } else if (type == 'radio') {
                    disable = (orig[id].checked == $(this).is(':checked'));
                } else if (type == 'checkbox' && $(this).attr('id') != 'changePass') {
                    var chkVal = '';
                    if (orig[id].value == 'on') {
                        chkVal = true;
                    } else {
                        chkVal = false;
                    }
                    disable = (chkVal == $(this).is(':checked'));
                }

                if (!disable) {
                    return false; // break out of loop
                }

            });

            button.prop('disabled', disable);
        });


        $('#EditEmpReset').click(function() {
            button.prop('disabled', true);
        });

        var conPass = '';
        $('#changePass').change(function() {
            conPass = {match: null, length: null, capital: null, letter: null, number: null};
            $("#pswd_info").find(".fa-times").removeClass('fa fa-times');
            $("#pswd_info").find(".fa-check").removeClass('fa fa-check');
            if (!this.checked) {
                $('#confirmPassword').val('');
                $('#confirmPassword').fadeOut('slow');
                $('#confirmPassword').prop('disabled', true);
                $('#checkPass').fadeOut('slow');
                $('#Password').prop('disabled', true);
                $('#Password').val('');
                $('#passvalidation').hide('slow');
            } else {
                $('#confirmPassword').fadeIn('slow');
                $('#confirmPassword').prop('disabled', false);
                $('#Password').prop('disabled', false);
                document.getElementById('confirmPassword').style.display = 'block';
                $('#confirmPassword').val('');
                $('#Password').val('');
                $('#passvalidation').show('slow');
            }
        });


        $('#Password,#confirmPassword').keyup(function(e) {
            var pass1 = $('#Password').val();
            var pass2 = $('#confirmPassword').val();
            var div = $('#checkPass');

            if (pass1 == pass2) {
                conPass.match = true;
                //  div.html('');
                $('#match').removeClass('fa fa-times').addClass('fa fa-check');
            } else if (pass1 != pass2) {
                conPass.match = false;
                $('#match').removeClass('fa fa-check').addClass('fa fa-times');
                // div.html('<span class="erroruser">Password and Confirm Password not match</span>');

            }

            //validate the length
            if (pass1.length < 8) {
                $('#length').removeClass('fa fa-check').addClass('fa fa-times');
                conPass.length = false;
            } else {
                $('#length').removeClass('fa fa-times').addClass('fa fa-check');
                conPass.length = true;

            }
            //validate letter
            if (pass1.match(/[A-z]/)) {
                $('#letter').removeClass('fa fa-times').addClass('fa fa-check');
                conPass.letter = true;
            } else {
                $('#letter').removeClass('fa fa-check').addClass('fa fa-times');
                conPass.letter = false;
            }

            //validate capital letter
            if (pass1.match(/[A-Z]/)) {
                $('#capital').removeClass('fa fa-times').addClass('fa fa-check');
                conPass.capital = true;
            } else {
                $('#capital').removeClass('fa fa-check').addClass('fa fa-times');
                conPass.capital = false;
            }

            //validate number
            if (pass1.match(/\d/)) {
                $('#number').removeClass('fa fa-times').addClass('fa fa-check');
                conPass.number = true;
            } else {
                $('#number').removeClass('fa fa-check').addClass('fa fa-times');
                conPass.number = false;
            }

            e.preventDefault();
        });


        $('#btnSubmit').on('click', function(event) {
            if ($("#EditEmployeeForm")[0].checkValidity()) {
                var url = 'admin/function/EmployeeFunctionCaller.php?module=EditEmployee';
                var mydata = $("#EditEmployeeForm").serialize();
                if (validateCheckPass(conPass)) {
                    $.ajax({
                        url: url,
                        type: 'get',
                        data: mydata,
                        dataType: 'json',
                        success: function(result) {
                            document.getElementById('ajaxmsgs').style.display = 'block';
                            if (result.type == 'Success') {
                                $('#ajaxmsgs').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong><br>" + result.message + "</div>");
                                $("#EditEmployeeForm :input").attr("disabled", true);
                                $("#cancel").attr("disabled", false).html('Close');
                            } else {
                                $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong><br>" + result.message + "</div>");
                            }
                            //alert('success error');
                        },
                        error: function() {
                            document.getElementById('ajaxmsgs').style.display = 'block';
                            $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
                            //alert('here error');
                        }
                    });
                }

            } else {
                $("#EditEmployeeForm").find(':submit').click();
            }
            event.preventDefault();
        }); // ajax button form submit

        $('#UserName').keyup(function() { // Keyup function for check the user action in input
            var Username = $(this).val(); // Get the username textbox using $(this) or you can use directly $('#username')
            var UsernameAvailResult = $('#username_avail_result'); // Get the ID of the result where we gonna display the results
            if (Username.length > 4) { // check if greater than 5 (minimum 6)
                UsernameAvailResult.html('Loading..'); // Preloader, use can use loading animation here
                var UrlToPass = 'action=username_availability&username=' + Username;
                $.ajax({// Send the username val to another checker.php using Ajax in POST menthod
                    type: 'POST',
                    data: UrlToPass,
                    url: 'admin/function/CheckUserName.php/',
                    success: function(responseText) { // Get the result and asign to each cases
                        if (responseText == 0) {
                            UsernameAvailResult.html('<span class="successuser">Username name available</span>');
                        } else if (responseText > 0) {
                            UsernameAvailResult.html('<span class="erroruser">Username already taken</span>');
                        } else {
                            alert('Problem with sql query');
                        }
                    }
                });
            } else {
                UsernameAvailResult.html('Enter atleast 5 characters');
            }
            if (Username.length == 0) {
                UsernameAvailResult.html('');
            }
        });

        $('#Password, #UserName').keydown(function(e) { // Dont allow users to enter spaces for their username and passwords
            if (e.which == 32) {
                return false;
            }
        });

    });
    $(function() {
        $('[data-tooltip="tooltip"]').tooltip();
    });


    function validateCheckPass(obj) {
        var returnValue = new Array();
        var thisValue = '';
        $.each(obj, function(key, value) {
            if (value == false) {
                returnValue.push(false);
            } else {
                returnValue.push(true);
            }
        });
        if (jQuery.inArray(false, returnValue) !== -1) {
            thisValue = false;
        } else {
            thisValue = true;
        }

        return thisValue;
    }

    $('#passvalidation').hide();
</script>
<style type="text/css">
    .successuser{
        color:#009900;
    }
    .erroruser{
        color:#F33C21;
    }
    .empname_avail_result .checkPass{
        display:block;
        width:180px;
    }</style>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Edit Employee</h4>
</div>
<form class="form-horizontal" role="form" id="EditEmployeeForm"><!-- /modal-header -->
    <div class="modal-body">

        <div class="form-group">
            <label class="control-label col-sm-3" for="UserName">User Name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="UserName" name="UserName" placeholder="Enter User Name" value="<?php echo $mergeStoreData['UserName']; ?>" required minlength="5"
                <?php
                if ($role != 1) {
                    echo "readonly onfocus='this.blur()'";
                }
                ?>>
                <div class="username_avail_result" id="username_avail_result"></div>
            </div>
        </div> <!-- /div for store name -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="Password">Password:</label>
            <div class="col-sm-6">
                <input type="password" class="form-control" id="Password" name="Password"  placeholder="new password" disabled required  minlength="8">
                <input type="checkbox" id="changePass" name="changePass" class="btn-large" >
                Change Password?
                <input type="password" class="form-control" id="confirmPassword" name="confirmPassword"  placeholder="confirm password" style="display:none" disabled required minlength="8">
                <div class="checkPass" id="checkPass" ></div>
            </div>

        </div> <!-- /div for password -->

        <div class="well" id="passvalidation">
            <strong class="text-center text-success"><i class="fa fa fa-gears"></i> PASSWORD REQUIREMENTS </strong>

            <ul id="pswd_info"  class="list-group">
                <li class="list-group-item">
                    Confirm password must <strong class="text-success">match new</strong>
                    <span class="marker" id="match"></span>
                </li>
                <li class="list-group-item">
                    With at least <strong class="text-success">one letter</strong>
                    <span class="marker"  id="letter"></span>
                </li>
                <li class="list-group-item">
                    With at least <strong class="text-success">one capital letter</strong>
                    <span class="marker"  id="capital"></span>
                </li>
                <li class="list-group-item">
                    With at least <strong class="text-success">one number</strong>
                    <span class="marker"  id="number"></span>
                </li>
                <li class="list-group-item">
                    Must be at least <strong class="text-success">8 characters</strong>
                    <span class="marker"  id="length"></span>
                </li>
            </ul>
        </div>

        <?php
        if ($mergeStoreData['UserName'] == '' || is_null($mergeStoreData['UserName'])) {
            $isNew = "1";
        } else {
            $isNew = "0";
        }
        ?>
        <input type="hidden" name="isNewUser" id="newuser" value="<?php echo $isNew; ?>"/>
        <div class="form-group">
            <label class="control-label col-sm-3" for="EmployeeFirstName">Employee First Name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="EmployeeFirstName" name="EmployeeFirstName" placeholder="Enter First Name" value="<?php echo $mergeStoreData['EmpFirstName']; ?>" required>
                <div class="empname_avail_result" id="empname_avail_result"></div>
            </div>
        </div> <!-- /div for emp name -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="EmployeeLastName">Employee Last Name::</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="EmployeeLastName" name="EmployeeLastName" placeholder="Enter Last Name" value="<?php echo $mergeStoreData['EmpLastName']; ?>" required>
            </div>
        </div> <!-- /div for first name -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="EmployeeEmail">Employee Email:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="EmployeeEmail" name="EmployeeEmail" placeholder="Enter Email Address" value="<?php echo $mergeStoreData['EmpEmail']; ?>">
            </div>
        </div> <!-- /div for first name -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="EmployeePhone">Employee Phone:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="EmployeePhone" name="EmployeePhone" placeholder="Enter Phone Number" value="<?php echo $mergeStoreData['EmpPhone']; ?>">
            </div>
        </div> <!-- /div for first name -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="EmployeeMobile">Employee Mobile:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="EmployeeMobile" name="EmployeeMobile" placeholder="Enter Mobile Number" value="<?php echo $mergeStoreData['EmpMobile']; ?>">
            </div>
        </div> <!-- /div for first name -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="EmpRoleID">Employee Role:</label>
            <div class="col-sm-6">
                <div id="tempDiv"></div>
                <select name="EmployeeRoleID" class="form-control" id="EmployeeRole" required>
                    <option value="">Please select</option>
                    <?php foreach ($roleList as $value) { ?>
                        <option value="<?php echo $value['f_EmpRoleID']; ?>" <?php
                        if ($value['f_EmpRoleID'] == $mergeStoreData['EmpRoleID']) {
                            echo "selected='selected'";
                        }
                        ?> >
                            <?php echo $value['f_EmpRoleName']; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div> <!-- /div for store owner -->
        <?php if ($role != '4') { ?>
            <div class="form-group">
                <label class="control-label col-sm-3" for="StoreLeader"></label>
                <div class="col-sm-6">
                    Set as Store Leader? <input type="checkbox" id="setStoreLeader" name="setStoreLeader" class="btn-large"  <?php
                    if ($mergeStoreData['RoleID'] == '5') {
                        echo "checked=\"checked\"";
                    }
                    ?>data-tooltip="tooltip" data-placement="right" title="Grant access to report pages"/>
                    <div class="setLeaderCheck" id="setLeaderCheck"></div>
                </div>
            </div><?php } ?>
        <input type="hidden" id="EmployeeID" name="EmployeeID" value="<?php echo $empID; ?>" />
        <div id="ajaxmsgs"></div>  <!-- ajaxmsg div -->
    </div> <!-- /modal-body -->
    <div class="modal-footer">
        <input type="submit" style="visibility: hidden"/>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id="cancel">Cancel</button>
        <button type="button" class="btn btn-sm btn-primary"  id="btnSubmit" name="btnSubmit" disabled="">Submit</button>
        <button type="reset" class="btn btn-sm btn-danger" id="EditEmpReset">Reset</button>
    </div> <!-- /modal-footer -->
</form>  <!-- /form -->

