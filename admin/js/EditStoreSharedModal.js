
// <editor-fold defaultstate="collapsed" desc=" ${InitialLoad} ">
$(document).ready(function () {

    $("#ajaxmsgsadd,#ajaxmsgsdelete,#ajaxmsgsedit").hide();

    $('.EditShare').each(function () {
        $('.EditShare').not(this).find('option[value="' + this.value + '"]').prop('disabled', true);
    });

    $('select').on('change', function (e) {
        var exclude = "StoreOwnerID";
        var optionName = $(this).attr('name');
        var optionid = $(this).attr('id');
        var form = $(this).parents("form").attr("id");
        
        if (optionName !== exclude && form !== 'EditStoreForm') {
            var ownerID = $('option:selected', this).attr('ownerid');
            var txtboxID = optionid.charAt(optionid.length - 1);
            $('#shareowner' + txtboxID).val(ownerID)
                    .prop("disabled", false);

            
        }
       
        
        $('.AddShare').find('option').prop('disabled', false);
        $('.AddShare').each(function () {
            $('.AddShare').not(this).find('option[value="' + this.value + '"]').prop('disabled', true);
           // console.log( this.value +' removed');
        });
        $('.EditShare').find('option').prop('disabled', false);
        $('.EditShare').each(function () {
            $('.EditShare').not(this).find('option[value="' + this.value + '"]').prop('disabled', true);
        });        
     
        e.preventDefault();
    });
});

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" ${AddStore} ">
$('#AddStoreSubmit').on('click', function (event) {

    if ($("#AddStoreForm")[0].checkValidity()) {
        var url = 'admin/function/StoreFunctionCaller.php?module=AddSharedStore';   
        var mydata = $("#AddStoreForm").serialize();
        
        $.ajax({
            url: url,
            type: 'get',
            data: mydata,
            dataType: 'json',
            success: function (result) {
                document.getElementById('ajaxmsgsadd').style.display = 'block';
                if (result.type === 'Success') {
                    $('#ajaxmsgsadd').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong>" + result.message + "</div>");
                    $("#AddStoreForm :input").attr("disabled", true);
                    $("#cancel").attr("disabled", false).html('Close');
                }
                else {
                    $('#ajaxmsgsadd').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong>" + result.message + "</div>");
                }
            },
            error: function () {
                document.getElementById('ajaxmsgsadd').style.display = 'block';
                $('#ajaxmsgsadd').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
              
            }
        });
    } else {
        $("#AddStoreForm").find(':submit').click();
    }
    event.preventDefault();
}); // ajax button form submit
var buttonAddStore = $('#AddStoreSubmit');
var origAddStore = [];

$.fn.getTypeAddStore = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("#AddStoreForm :input").each(function () {
    var type = $(this).getTypeAddStore();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
    origAddStore[$(this).attr('id')] = tmp;
});


$('#AddStoreForm').bind('change keyup', function () {

    var disable = true;
    $("#AddStoreForm :input").each(function () {
        var type = $(this).getTypeAddStore();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select') {
            disable = (origAddStore[id].value == $(this).val());

        } else if (type == 'radio') {
            disable = (origAddStore[id].checked == $(this).is(':checked'));
        }
        else if (type == 'checkbox') {
            var chkVal = '';
            if (origAddStore[id].value == 'on') {
                chkVal = true;
            }
            else {
                chkVal = false;
            }
            disable = (chkVal == $(this).is(':checked'));
        }

        if (!disable) {
            return false; // break out of loop
        }
    });

    buttonAddStore.prop('disabled', disable);
  
});
  
     $('#AddStoreReset').click(function () {
        //$('#input1').attr('name', 'other_amount');
        buttonAddStore.prop('disabled',true);
    });
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" ${EditStore} ">

$('#EditStoreSubmit').on('click', function (event) {
    if ($("#EditStoreForm")[0].checkValidity()) {
        var url = 'admin/function/StoreFunctionCaller.php?module=EditSharedStore';      
        var mydata = $("#EditStoreForm").serialize();
       
        $.ajax({
            url: url,
            type: 'get',
            data: mydata,
            dataType: 'json',
            success: function (result) {
                document.getElementById('ajaxmsgsedit').style.display = 'block';
                if (result.type === 'Success') {
                    $('#ajaxmsgsedit').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong>" + result.message + "</div>");
                     $("#EditStoreForm :input").attr("disabled", true);
                    $("#cancel").attr("disabled", false).html('Close');
                }
                else {
                    $('#ajaxmsgsedit').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong>" + result.message + "</div>");
                }                
            },
            error: function () {
                document.getElementById('ajaxmsgsedit').style.display = 'block';
                $('#ajaxmsgsedit').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
            }
        });
    } else {
        $("#EditStoreForm").find(':submit').click();
    }
    event.preventDefault();
}); // ajax button form submit

function onLoadEditStoreForm() {
    var buttonEditStore = $('#EditStoreSubmit');
    var origEditStore = [];

    $.fn.getTypeEditStore = function () {
        return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
    }

    $("#EditStoreForm :input").each(function () {
        var type = $(this).getTypeEditStore();
        var tmp = {
            'type': type,
            'value': $(this).val()
        };
        if (type == 'radio') {
            tmp.checked = $(this).is(':checked');
        }
        origEditStore[$(this).attr('id')] = tmp;
    });

    $('#EditStoreForm').bind('change keyup', function () {

        var disable = true;
        $("#EditStoreForm :input").each(function () {
            var type = $(this).getTypeEditStore();
            var id = $(this).attr('id');
            if (type == 'text' || type == 'select') {
                disable = (origEditStore[id].value == $(this).val());

            } else if (type == 'radio') {
                disable = (origEditStore[id].checked == $(this).is(':checked'));
            }
            else if (type == 'checkbox') {
                var chkVal = '';
                if (origEditStore[id].value == 'on') {
                    chkVal = true;
                }
                else {
                    chkVal = false;
                }
                disable = (chkVal == $(this).is(':checked'));
            }

            if (!disable) {
                return false; // break out of loop
            }
        });

        buttonEditStore.prop('disabled', disable);
    });
    
    $('#EditStoreReset').click(function () {
        //$('#input1').attr('name', 'other_amount');
        buttonEditStore.prop('disabled',true);
    });

}



// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" ${DeleteStore} ">
$('#DeleteStoreSubmit').on('click', function (event) {
    var url = 'admin/function/StoreFunctionCaller.php?module=DeleteSharedStore';   
    var mydata = $("#DeleteStoreForm").serialize();
 
    $.ajax({
        url: url,
        type: 'get',
        data: mydata,
        dataType: 'json',
        success: function (result) {
            document.getElementById('ajaxmsgsdelete').style.display = 'block';
            if (result.type === 'Success') {
                $('#ajaxmsgsdelete').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong>" + result.message + "</div>");
                $("#DeleteStoreSubmit :input").attr("disabled", true);
                $("#cancel").attr("disabled", false).html('Close');
            }
            else {
                $('#ajaxmsgsdelete').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong>" + result.message + "</div>");
            }           
        },
        error: function () {
            document.getElementById('ajaxmsgsdelete').style.display = 'block';
            $('#ajaxmsgsdelete').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
        }
    });
    event.preventDefault();
}); // ajax button form submit

function onLoadDeleteStoreForm() {
    var buttonDeleteStore = $('#DeleteStoreSubmit');
    $('#DeleteStoreForm').bind('change keyup', function () {
        var disable = true;
        if ($("#DeleteStoreForm input:checkbox:checked").length > 0) {
            disable = false;
        } // any one is checked     
        else {
            disable = true;
        } // none is checked
        buttonDeleteStore.prop('disabled', disable);
    });
     
     $('#DeleteStoreReset').click(function () {
        //$('#input1').attr('name', 'other_amount');
        buttonDeleteStore.prop('disabled',true);
        $("input[type='checkbox']").parent().removeClass("redBackground");
    });
    
    
}

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" ${Extra} ">
function showDiv(cliked_id) {

    var fieldNum = cliked_id.charAt(cliked_id.length - 1);
    var newID = parseInt(fieldNum) + 1;
    var fieldID = "elementsToOperateOn-" + newID;
    var idfield = "#" + fieldID + " :input";
    $("#shareTo" + newID).val($("#shareTo" + newID + " option:first").val());

    //  alert(idfield);
    document.getElementById(fieldID).style.display = 'block';
    $(idfield).prop("disabled", false).fadeIn('slow');
    //  cliked_id.preventDefault();
} // unhide hidden div and enable input fields for shared to user
function hideDiv(cliked_id) {
    var fieldNum = cliked_id.charAt(cliked_id.length - 1);
    var newID = parseInt(fieldNum);
    var fieldID = "elementsToOperateOn-" + newID;
    var idfield = "#" + fieldID + " :input";
    document.getElementById(fieldID).style.display = 'none';
    var optionVal = $('#shareTo' + fieldNum).val();
//        var test = $('#'+fieldID).find('select').attr('id');
    $('.AddShare').find('option[value="' + optionVal + '"]').prop('disabled', false);
    //   $('.EditShare').find('option[value="'+ optionVal+'"]').prop('disabled',false);

    $(idfield).prop("disabled", true).fadeOut('slow');



    // cliked_id.preventDefault();
} // hide hidden div and disable input field for shared to user
$("input[type='checkbox']").change(function () {
    if ($(this).is(":checked")) {
        $(this).parent().addClass("redBackground");
    } else {
        $(this).parent().removeClass("redBackground");
    }
});
// </editor-fold>
