<?php
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/StoreFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/functions/toolcrypter.php');

session_start();
$role = $_SESSION['UserRoleID'];
$module = $_GET['module'];

$sqlOwnerID = " ";
switch ($role) {
    case '1':
        $sqlOwnerID = " ";
        break;
    case '2':
    case '3':
        $sqlOwnerID = " AND b.f_UserID  = '{$_SESSION['UserProfileID']}' ";
        break;
    case '4':
        $query = $_SESSION['EmpProfileID'];
        $sqlGetOwnerID = "select b.f_UserID  from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  b.f_UserID  limit 1 ";
        $result = mysql_query($sqlGetOwnerID, $connection);
        while ($row = mysql_fetch_assoc($result)) {
            $data = $row['f_StoreOwnerUserID'];
        }
        $sqlOwnerID = " AND b.f_UserID  = '{$data}' ";
        break;
    case '5':
        $query = $_SESSION['EmpProfileID'];
        $sqlGetOwnerID = "select b.f_UserID  from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  b.f_UserID  limit 1 ";
        $result = mysql_query($sqlGetOwnerID, $connection);
        while ($row = mysql_fetch_assoc($result)) {
            $OwnerID = $row['f_StoreOwnerUserID'];
        }

        if ($shared != "no") {
            $sqlgetPartners = "select a.f_StoreOwnerUserID FROM t_storelist a
            LEFT JOIN t_sharedstore b on a.f_StoreID = b.f_StoreID
            WHERE b.f_ShareToUserID = '{$OwnerID}' group by a.f_StoreOwnerUserID";
            $result = mysql_query($sqlgetPartners, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $storePartners[] = $row['f_StoreOwnerUserID'];
            }
            foreach ($storePartners as $data) {
                $quotedString .= "'$data',";
            }
            $quotedString .= "'{$_SESSION['UserProfileID']}',";
            $quotedString = trim($quotedString, ",");
// var_dump($storePartners);
            $sqlOwnerID = " ";
        } else {
            $sqlOwnerID = " AND b.f_UserID  = '{$OwnerID}' ";
        }
        break;
    case '6':
        $sqlOwnerID = " AND b.f_UserID  = '{$profileID}' ";
        break;
}

//echo $sqlOwnerID;


$sqlGetMedallia = "SELECT
    CONCAT(f_UserFirstName, ' ', f_UserLastName) AS 'OwnerName',
    f_MedalliaUserName AS 'UserName',
    f_MedalliaPassword AS 'Password',
    f_PasswordExpiration AS 'PasswordExpiration',
    f_DateTimeCreated AS 'DateTimeCreated'
FROM
    t_medalliadetails a
        LEFT JOIN
    t_userlist b ON a.f_StoreOwnerUserID = b.f_UserID
    WHERE 1=1 {$sqlOwnerID}";
$results = mysql_query($sqlGetMedallia, $connection);
while ($row = mysql_fetch_assoc($results)) {
    $medalliaDetails[] = $row;
}
?>

<script type="text/javascript">

    $(document).ready(function() {
        var table = $('#<?php echo $module; ?>Table').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ]
        });

        table.on('order.dt search.dt', function() {
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        $("th").unbind();
        $("th").removeClass("sorting");
        $("th").removeClass("sorting_asc");
    });


    function stripHTML(dirtyString) {
        var container = document.createElement('div');
        container.innerHTML = dirtyString;
        return container.textContent;
    }

    $(function() {
        $('[data-tooltip="tooltip"]').tooltip();
    })

    function openModalTools(el) {
        var target = $(el).attr('href');
        $("#CreateMedalliaModal .modal-content").load(target, function() {
            $("#CreateMedalliaModal").modal("show");
        });
        $('#CreateMedalliaModal').on('hidden.bs.modal', function(e) {
            e.preventDefault();
            $('#page-contents').hide();
            $('#LogLoader').fadeIn('slow');
            reLoad();
        });
    }
</script>
<div class="row">
    <div class="filters">
        <a class="btn btn-primary btn-xs btn-admin"  data-toggle="modal"
           data-tooltip="tooltip" data-placement="left" title="Add Employee"
           href="admin/modal/CreateMedalliaDetails.php" data-target="#CreateEmployeeModal" onclick="openModalTools(this)">
            <i class="glyphicon glyphicon-plus"></i> Medallia Details
        </a>
    </div>

    <div class="table-responsive" style='overflow-x: hidden;'>
        <?php if ($module == 'medallia') { ?>
            <h4 class='sub-header'>Medallia Codes</h4>
            <table id="<?php echo $module; ?>Table" class="table table-bordered table-hover"  cellspacing="0" >
                <thead>
                    <tr>
                        <th></th>
                        <th>Licencee / Director</th>
                        <th>UserName</th>
                        <th>Password</th>
                        <th>Password Expiration</th>
                        <th>Created Date</th>
                        <th>Update</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    foreach ($medalliaDetails as $value) {
                        ?>
                        <tr>
                            <td></td>
                            <td><?php echo $value['OwnerName']; ?></td>
                            <td><?php echo $value['UserName']; ?></td>
                            <td><?php echo dencrpyt_medallia($value['Password']); ?></td>
                            <td><?php echo $value['PasswordExpiration']; ?></td>
                            <td><?php echo $value['DateTimeCreated']; ?></td>
                            <td>
                                <?php
                                $roleIn = array('1', '2', '3', '5');
                                if (in_array($role, $roleIn)) {
                                    ?>
                                    <a class="btn-sm btn-hover btn-warning editmodal editcls"
                                       data-tooltip="tooltip" data-placement="bottom" title="Edit"
                                       data-toggle="modal" href="admin/modal/MergeModal.php?storeID=<?php echo $key; ?>" afterclose="admin/adminviewdata.php?module=FixLine"
                                       data-target="#EditStoreModal" onclick="openMergeModal(this)">
                                        <span class="glyphicon glyphicon-edit" ></span>
                                    </a>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } ?>
    </div>
</div>



