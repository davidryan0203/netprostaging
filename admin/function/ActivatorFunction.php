<?php

include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
session_start();
function ActivateStore($StoreIDList) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
    global $connection;
    //echo "<pre>";
    //var_dump($StoreIDList['toActivateStore']);
    //echo "IN ('" . implode("','", $StoreIDList['toActivateStore']) . "')";

    $sql = "UPDATE `t_storelist` SET f_Status = 1
            WHERE f_StoreID IN ('" . implode("','", $StoreIDList['toActivateStore']) . "')";
    //echo $sql;
    
  //  InsertLog($logType,$logFrom,$logRemarks,$logBy,$logStatus,$logFromSub)
    $result = mysql_query($sql, $connection);
    if ($result && mysql_affected_rows() >= 1) {
        $logMessageArray = json_encode(array('Details' => $StoreIDList));
        InsertLog('3', '3', $logMessageArray, $_SESSION['LoginID'], 'Success', '3');
        $returnActivateStore['message'] = "Store Activation Successful";
    } else {
         $logMessageArray = json_encode(array('Details' => $StoreIDList));
        InsertLog('3', '3', $logMessageArray, $_SESSION['LoginID'], 'Failed', '3');
        $returnActivateStore['message'] = "Store Activation Failed";
    }

    return $returnActivateStore;
}

function ActivateUser($UserIDList) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
    global $connection;
    //echo "<pre>";
    //var_dump($UserIDList['toActivateStore']);
    //echo "IN ('" . implode("','", $UserIDList['toActivateUser']) . "')";

    $sql = "UPDATE `t_userlist` SET f_Status = 1
            WHERE f_UserID IN ('" . implode("','", $UserIDList['toActivateUser']) . "')";

    $result = mysql_query($sql, $connection);

    if ($result && mysql_affected_rows() >= 1) {
        $logMessageArray = json_encode(array('Details' => $UserIDList));
        InsertLog('3', '3', $logMessageArray, $_SESSION['LoginID'], 'Success', '3');
        $ActivatedUser = + mysql_affected_rows();
    }
    else{
         $logMessageArray = json_encode(array('Details' => $UserIDList));
        InsertLog('3', '3', $logMessageArray, $_SESSION['LoginID'], 'Failed', '3');
    }

    if ($ActivatedUser >= 1) {
        $sql = "UPDATE `t_userlogin` SET f_Status = 1
            WHERE f_UserID IN ('" . implode("','", $UserIDList['toActivateUser']) . "')";
        $result = mysql_query($sql, $connection);

        if ($result && mysql_affected_rows() >= 1) {
            $logMessageArray = json_encode(array('Details' => $UserIDList));
        InsertLog('3', '3', $logMessageArray, $_SESSION['LoginID'], 'Success', '1');
            $returnActivateUser['message'] = "User Activation Successful";
        } else {
            $logMessageArray = json_encode(array('Details' => $UserIDList));
        InsertLog('3', '3', $logMessageArray, $_SESSION['LoginID'], 'Failed', '1');
            $returnActivateUser['message'] = "User Activation Failed";
        }
    }

    return $returnActivateUser;
}

function ActivateEmployee($EmployeeIDList) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
    global $connection;
//echo "<pre>";
//var_dump($EmployeeIDList['toActivateStore']);
//echo "IN ('" . implode("','", $EmployeeIDList['toActivateUser']) . "')";
    $sql = "UPDATE `t_emplist` SET f_Status = 1
            WHERE f_EmpID IN ('" . implode("','", $EmployeeIDList['toActivateEmployee']) . "')";
//echo $sql;
    $result = mysql_query($sql, $connection);

    if ($result && mysql_affected_rows() >= 1) {
        $ActivatedEmpCount = + mysql_affected_rows();
        $logMessageArray = json_encode(array('Details' => $EmployeeIDList));
        InsertLog('3', '3', $logMessageArray, $_SESSION['LoginID'], 'Success', '2');
    }
    else{
        $logMessageArray = json_encode(array('Details' => $EmployeeIDList));
        InsertLog('3', '3', $logMessageArray, $_SESSION['LoginID'], 'Success', '2');        
    }

    if ($ActivatedEmpCount >= 1) {
        $sql = "UPDATE `t_userlogin` SET f_Status = 1
            WHERE f_EmpID IN ('" . implode("','", $EmployeeIDList['toActivateEmployee']) . "')";
        $result = mysql_query($sql, $connection);

        if ($result && mysql_affected_rows() >= 1) {
             $logMessageArray = json_encode(array('Details' => $EmployeeIDList));
        InsertLog('3', '3', $logMessageArray, $_SESSION['LoginID'], 'Success', '1');
            $returnActivateEmployee['message'] = "Employee Activation Successful";
        } else {
             $logMessageArray = json_encode(array('Details' => $EmployeeIDList));
        InsertLog('3', '3', $logMessageArray, $_SESSION['LoginID'], 'Failed', '1');
            $returnActivateEmployee['message'] = "Employee Activation Failed";
        }
    }

    return $returnActivateEmployee;
}
