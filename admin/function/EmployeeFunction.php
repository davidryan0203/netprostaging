<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/crypter.php');

session_start();


function EmployeeFunction($function, $query) {

    switch ($function) {

        case "CreateEmployee":
            $EmployeeFunctionData = CreateEmployee($query);
            break;
        case "UpdateEmployee":
            $EmployeeFunctionData = UpdateEmployee($query);
            break;
        case "SelectEmployee":
            $EmployeeFunctionData = SelectEmployee($query);
            break;
        case "DeleteEmployee":
            $EmployeeFunctionData = SelectEmployee($query);
            break;

        default:
            $EmployeeFunctionData = "Invalid Protocol";
    }

    return $EmployeeFunctionData;
}

function CreateEmployee($query) {
    global $connection;
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
    //echo "here2";
    // foreach ($query as $data) {

    $empfirstname = trim(ucwords(mysql_real_escape_string($query['EmpFirstName'])));
    $emplastname = trim(ucwords(mysql_real_escape_string($query['EmpLastName'])));
    $emproleID = mysql_real_escape_string($query['EmpRoleID']);
    $empemail = mysql_real_escape_string($query['EmpEmail']);
    $empPhone = mysql_real_escape_string($query['EmpPhone']);
    $empmobile = mysql_real_escape_string($query['EmpMobile']);
    $userName = mysql_real_escape_string($query['UserName']);
    // $userPass = md5(mysql_real_escape_string($query['UserPassword']));

    $userPass = password_hash($query['UserPassword'], PASSWORD_BCRYPT);

    $setAsLeader = mysql_real_escape_string($query['setStoreLeader']);
// }
//var_dump($query);
    $sql = "INSERT INTO `t_emplist` (`f_EmpFirstName`,`f_EmpLastName`,`f_EmpRoleID`,`f_EmpEmail`,`f_EmpPhone`,`f_EmpMobile`)
            VALUES ('$empfirstname','$emplastname','$emproleID','$empemail','$empPhone','$empmobile')";

    $result = mysql_query($sql, $connection);
    // echo $sql;
    if ($result && mysql_affected_rows() == 1) {
        $returnCreateEmployee['message'] = "Success Creating Employee";
        $insertedID = mysql_insert_id();
        $returnCreateEmployee['EmpID'] = $insertedID;

        $logMessageArray = json_encode(array('EmpID' => $insertedID,
            'Details' => array($empfirstname, $emplastname, $emproleID, $empemail, $empPhone, $empmobile)));
        InsertLog('2', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '2');


        if (($userName != "" && !is_null($userName)) && ($userName != "" && !is_null($userPass))) {
            if ($setAsLeader == "on") {
                $empRoleType = '5';
            } else {
                $empRoleType = '4';
            }
            $sqlCreateUserLogin = "INSERT INTO t_userlogin (`f_UserName`,`f_Password`,`f_UserType`,`f_EmpID`,`f_UserID`,`f_UserRoleID`) VALUES ('{$userName}','{$userPass}','2','{$insertedID}','0','{$empRoleType}')";
            $resultCreateUserLogin = mysql_query($sqlCreateUserLogin, $connection);
            if ($resultCreateUserLogin && mysql_affected_rows() == 1) {
                $logMessageArray = json_encode(array('LoginID' => $insertedID,
                    'Details' => array($userName, $userPass, '2', '0', $empRoleType)));
                InsertLog('2', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '1');
            } else {
                $logMessageArray = json_encode(array('Reason' => 'An error occurred',
                    'Details' => array($userName, $userPass, '2', '0', $empRoleType)));
                InsertLog('2', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '1');
            }
        }
    } else {
        $returnCreateEmployee['message'] = "Failed Creating Employee";
        $logMessageArray = json_encode(array('Reason' => 'An error occurred', 'Details' => array($empfirstname, $emplastname, $emproleID, $empemail, $empPhone, $empmobile)));
        InsertLog('2', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '2');
    }

    //mysql_close($connection);
    return $returnCreateEmployee;
}

function UpdateEmployee($query) {
    global $connection;
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
    // foreach ($query as $data) {
    $username = mysql_real_escape_string($query['UserName']);
    if (isset($query['Password'])) {
        //$password = md5(mysql_real_escape_string($query['Password']));

        $password = password_hash($query['Password'], PASSWORD_BCRYPT);
        $cleanpass = mysql_real_escape_string($query['Password']);
        $origpass = encrpyt_pass($cleanpass);
    } else {
        $password = null;
    }
    // var_dump($query);
    $roleID = $query['EmployeeRoleID'];
    $firstname = trim(ucwords(mysql_real_escape_string($query['EmployeeFirstName'])));
    $lastname = trim(ucwords(mysql_real_escape_string($query['EmployeeLastName'])));
    $email = mysql_real_escape_string($query['EmployeeEmail']);
    $phone = mysql_real_escape_string($query['EmployeePhone']);
    $mobile = mysql_real_escape_string($query['EmployeeMobile']);
    $setAsLeader = mysql_real_escape_string($query['setStoreLeader']);
    $userdID = $query['EmployeeID'];

    if ($setAsLeader == "on") {
        $empRoleType = '5';
    } else {
        $empRoleType = '4';
    }
    $sql = "UPDATE `t_emplist`
            SET
            `f_EmpFirstName` = '$firstname',
            `f_EmpLastName` = '$lastname',
            `f_EmpRoleID` = '$roleID',
            `f_EmpEmail` = '$email',
            `f_EmpPhone` = '$phone',
            `f_EmpMobile` = '$mobile'
            WHERE `f_EmpID` = '{$userdID}'";

    if (!is_null($password)) {
        if ($query['isNewUser'] == '1') {
            $sqlCreateUserLogin = "INSERT INTO t_userlogin (`f_UserName`,`f_Password`,`f_UserType`,`f_EmpID`,`f_UserID`,`f_UserRoleID`) VALUES ('{$username}','{$password}','2','{$userdID}','0','{$empRoleType}')";
            $resultcreate = mysql_query($sqlCreateUserLogin, $connection);
            if ($resultcreate && mysql_affected_rows() == 1) {
                $insertedLogin = '1';
                $insertedID = mysql_insert_id();
                $logMessageArray = json_encode(array('LoginID' => $insertedID,
                    'Details' => array($username, $password, '2', '0', $empRoleType)));
                InsertLog('2', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '1');
            } else {
                $logMessageArray = json_encode(array('Reason' => 'An error occurred',
                    'Details' => array($username, $password, '2', '0', $empRoleType)));
                InsertLog('2', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '1');
            }
        } else {

            $sqlUpdateLogin = "UPDATE t_userlogin SET f_UserName = '{$username}', f_Password = '{$password}' , f_UserRoleID  = '{$empRoleType}' WHERE f_EmpID = '{$userdID}' ";
            $resultupdate = mysql_query($sqlUpdateLogin, $connection);

            if ($resultupdate && mysql_affected_rows() == 1) {
                $updateLogin = '1';

                $getUserLogin = "Select f_UserLoginID from t_userlogin where f_EmpID = '{$userdID}' limit 1";
                $resultUserLogin = mysql_query($getUserLogin, $connection);
                while ($rowUserLogin = mysql_fetch_assoc($resultUserLogin)) {
                    $userLoginID = $rowUserLogin['f_UserLoginID'];
                }

                $sqlCheckRepo = "Select f_NewPass as 'CurrentPass',count(*) as 'isInserted' From t_passrepo where f_UserLoginID = '{$userLoginID}' limit 1";
                $resultCheckRepo = mysql_query($sqlCheckRepo, $connection);
                while ($rowCheckRepo = mysql_fetch_assoc($resultCheckRepo)) {
                    $isInserted = $rowCheckRepo['isInserted'];
                    $oldPass = $rowCheckRepo['CurrentPass'];
                }
                if ($isInserted >= 1) {
                    $sqlRepoPass = "UPDATE t_passrepo SET f_NewPass = '{$origpass}' , f_OldPass = '{$oldPass}', f_DateTimeCreated = NOW() where f_UserLoginID = '{$userLoginID}'";
                    mysql_query($sqlRepoPass, $connection);
                } else {
                    $sqlRepoPass = "INSERT INTO `t_passrepo` (`f_UserLoginID`,`f_NewPass`) VALUES ('{$userLoginID}','{$origpass}')";
                    mysql_query($sqlRepoPass, $connection);
                }


                $logMessageArray = json_encode(array('EmpID' => $userdID, 'Details' => array($username, $password, $empRoleType)));
                InsertLog('3', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '1');
            } else {
                $logMessageArray = json_encode(array('Reason' => 'An error occurred', 'Details' => array($username, $password, $empRoleType)));
                InsertLog('3', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '1');
            }
        }
    } else {
        $sqlUpdateLogin2 = "UPDATE t_userlogin SET f_UserName = '{$username}' , f_UserRoleID  = '{$empRoleType}' WHERE f_EmpID = '{$userdID}' ";
        $respdateLogin2 = mysql_query($sqlUpdateLogin2, $connection);
        if ($respdateLogin2 && mysql_affected_rows() == 1) {
            $updateLogin2 = '1';
            $logMessageArray = json_encode(array('EmpID' => $userdID, 'Details' => array($username, $password, $empRoleType)));
            InsertLog('3', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '1');
        } else {
            $logMessageArray = json_encode(array('Reason' => 'An error occurred', 'Details' => array($username, $password, $empRoleType)));
            InsertLog('3', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '1');
        }
    }
    //echo $updateLogin."tus";
    $result = mysql_query($sql, $connection);
    if ($result && mysql_affected_rows() == 1) {
        $UpdateProfile = '1';
        $logMessageArray = json_encode(array('EmpID' => $userdID,
            'Details' => array($firstname, $lastname, $roleID, $email, $phone, $mobile)));
        InsertLog('3', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '2');
    } else {
        $logMessageArray = json_encode(array('Reason' => 'An error occurred',
            'Details' => array($firstname, $lastname, $roleID, $email, $phone, $mobile)));
        InsertLog('3', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '2');
    }

    if ($UpdateProfile == '1' || $updateLogin == '1' || $insertedLogin == '1' || $updateLogin2 == '1') {
        $returnUpdateEmployee['message'] = 'Update Success Employee';
    } else {
        //  echo mysql_affected_rows();
        $returnUpdateEmployee['message'] = 'Update Failed Employee';
    }

    // mysql_close($connection);
    return $returnUpdateEmployee;
}

function SelectEmployee($query = NULL, $type = NULL, $groupby = NULL) {
    global $connection;

    $groupbysql = "";
    if (!is_null($groupby)) {
        $groupbysql = ", a.f_EmpID";
    }
    $countQuery = count($query);

    if ($type == 'store') {
        $sql = "SELECT a.*,b.f_EmpRoleName, b.f_EmpRoleName , d.f_StoreListName , c.f_StoreID, c.f_EmpPNumber,f.f_UserName,f.f_Password,c.f_EmpNumberID , f.f_UserRoleID,
            max(g.f_DateTimeCreated) AS LatestLogin, count(g.f_LogID) as LoginCount, f.f_UserLoginID
            FROM t_emplist a
            LEFT JOIN t_emprole b on a.f_EmpRoleID = b.f_EmpRoleID
            LEFT JOIN t_empnumber c on a.f_EmpID = c.f_EmpID   and c.f_Status = 1
            LEFT JOIN t_storelist d on c.f_StoreID = d.f_StoreID AND d.f_Status = 1
            LEFT JOIN t_userlist e on d.f_StoreOwnerUserID = e.f_UserID
            LEFT JOIN t_userlogin f ON a.f_EmpID = f.f_EmpID
            LEFT JOIN t_log g ON f.f_UserLoginID = g.f_UserLoginID  AND g.f_LogTypeID = '1'
            WHERE 1=1 and a.f_Status = 1 AND d.f_Status = 1 and f.f_Status = 1
            AND e.f_UserID = '{$query}'
            GROUP by  c.f_EmpNumberID $groupbysql
            ORDER BY g.f_DateTimeCreated DESC ";
        mysql_query("SET SQL_BIG_SELECTS=1");
    } elseif ($type == 'specificstore') {
        $sql = "SELECT a.*,b.f_EmpRoleName, b.f_EmpRoleName , d.f_StoreListName , c.f_StoreID, c.f_EmpPNumber,f.f_UserName,f.f_Password,c.f_EmpNumberID , f.f_UserRoleID
            FROM t_emplist a
            LEFT JOIN t_emprole b on a.f_EmpRoleID = b.f_EmpRoleID
            LEFT JOIN t_empnumber c on a.f_EmpID = c.f_EmpID   and c.f_Status = 1
            LEFT JOIN t_storelist d on c.f_StoreID = d.f_StoreID AND d.f_Status = 1
            LEFT JOIN t_userlogin f ON a.f_EmpID = f.f_EmpID
            WHERE 1=1  and a.f_Status = 1 AND d.f_Status = 1 and f.f_Status = 1
            AND d.f_StoreID = '{$query}' ";
        mysql_query("SET SQL_BIG_SELECTS=1");
    } elseif ($type == 'npstracker') {
        $sql = "SELECT a.*,b.f_EmpRoleName, b.f_EmpRoleName , d.f_StoreListName , c.f_StoreID, c.f_EmpPNumber,f.f_UserName,f.f_Password,c.f_EmpNumberID , f.f_UserRoleID,
            max(g.f_DateTimeCreated) AS LatestLogin, count(g.f_LogID) as LoginCount, f.f_UserLoginID
            FROM t_emplist a
            LEFT JOIN t_emprole b on a.f_EmpRoleID = b.f_EmpRoleID
            LEFT JOIN t_empnumber c on a.f_EmpID = c.f_EmpID   and c.f_Status = 1
            LEFT JOIN t_storelist d on c.f_StoreID = d.f_StoreID AND d.f_Status = 1
            LEFT JOIN t_userlist e on d.f_StoreOwnerUserID = e.f_UserID
            LEFT JOIN t_userlogin f ON a.f_EmpID = f.f_EmpID
            LEFT JOIN t_log g ON f.f_UserLoginID = g.f_UserLoginID  AND g.f_LogTypeID = '1'
            WHERE 1=1 and a.f_Status = 1 AND d.f_Status = 1 and f.f_Status = 1
            AND e.f_UserID = '{$query}'
            GROUP by  a.f_EmpID
            ORDER BY g.f_DateTimeCreated DESC ";
        mysql_query("SET SQL_BIG_SELECTS=1");
    } else {
        $sql = "SELECT a.*,b.f_EmpRoleName, b.f_EmpRoleName , d.f_StoreListName , c.f_StoreID, c.f_EmpPNumber,f.f_UserName,f.f_Password,c.f_EmpNumberID , f.f_UserRoleID,
            max(g.f_DateTimeCreated) AS LatestLogin, count(g.f_LogID) as LoginCount, f.f_UserLoginID
            FROM t_emplist a
            LEFT JOIN t_emprole b on a.f_EmpRoleID = b.f_EmpRoleID
            LEFT JOIN t_empnumber c on a.f_EmpID = c.f_EmpID   and c.f_Status = 1
            LEFT JOIN t_storelist d on c.f_StoreID = d.f_StoreID AND d.f_Status = 1
            LEFT JOIN t_userlogin f ON a.f_EmpID = f.f_EmpID
            LEFT JOIN t_log g ON f.f_UserLoginID = g.f_UserLoginID  AND g.f_LogTypeID = '1'
            WHERE 1=1 AND d.f_Status = 1  and a.f_Status = 1 and f.f_Status = 1 ";
        mysql_query("SET SQL_BIG_SELECTS=1");
        if ($countQuery > 1) {
            $quotedString = "";
            foreach ($query as $data) {

                $quotedString .= "'$data',";
            }

            $whereIn = trim($whereIn, ",");

            $sql .= " AND a.f_EmpID in ('{$whereIn}')";
        } else {
            if ($query != "ALL") {

                $sql .= " AND a.f_EmpID = '{$query}'";
            }
        }
        //$sql .= " $groupbysql";
        $sql .= " GROUP by  c.f_EmpNumberID $groupbysql
            ORDER BY g.f_DateTimeCreated DESC ";
    }
//echo $type;
    $result = mysql_query($sql, $connection);
    while ($row = mysql_fetch_assoc($result)) {
        $returnSelectEmployee[] = $row;
    }
    //  echo $sql;
    //mysql_close($connection);
    return $returnSelectEmployee;
}

function DeleteEmployee($query) {
    global $connection;
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
    // var_dump($query);
    $sql = "UPDATE `t_emplist` SET f_Status = 0
            WHERE f_EmpID = '{$query}'";

    // echo $sql;
    $result = mysql_query($sql, $connection);
    //echo 'An error occurred';

    if ($result && mysql_affected_rows() >= 1) {
        $returnDeleteEmployee['message'] = "Deletion Success Employee";

        $logMessageArray = json_encode(array('EmpID' => $query,
            'Remarks' => 'Deactivate Epmloyee'));
        InsertLog('4', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '2');

        $sqlDeactiveLogin = "UPDATE t_userlogin SET f_Status = 0 WHERE f_EmpID = '{$query}'";
        $resultDeactiveLogin = mysql_query($sqlDeactiveLogin, $connection);
        if ($resultDeactiveLogin && mysql_affected_rows() == 1) {
            $logMessageArray = json_encode(array('EmpID' => $query,
                'Remarks' => 'Deactivate Login'));
            InsertLog('4', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '1');
        } else {
            $logMessageArray = json_encode(array('Reason' => 'An error occurred',
                'Details' => $query));
            InsertLog('4', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '1');
        }
        $getPnumber = "SELECT f_EmpPNumber as PNum,f_StoreID FROM t_empnumber WHERE f_EmpID = '{$query}'";
        $resultGetPnumber = mysql_query($getPnumber, $connection);
        while ($row = mysql_fetch_assoc($resultGetPnumber)) {
            $details[] = $row;
        }
        $sqlPNumber = "DELETE FROM t_empnumber WHERE f_EmpID = '{$query}'";
        $resultPnumber = mysql_query($sqlPNumber, $connection);
        if ($resultPnumber && mysql_affected_rows >= 1) {
            $logMessageArray = json_encode(array('EmpID' => $query, 'Details' => $details));
            InsertLog('4', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '5');
        } else {
            $logMessageArray = json_encode(array('Reason' => 'An error occurred', 'Details' => $query));
            InsertLog('4', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '5');
        }
    } else {
        $returnDeleteEmployee['message'] = "Deletion Failed Employee";
    }

    //mysql_close($connection);
    return $returnDeleteEmployee;
}

function CreateEmployeeNo($query) {
    global $connection;
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
//echo "<pre>";
    $queryarray = array_combine($query['ToWhatStoreID'], $query['EmpNum']);
    // var_dump($query);
    // echo "===============";
    // var_dump($queryarray);
    $i = 0;
    foreach ($queryarray as $ToWhatStoreID => $EmpNum) {
        $EmpNumber = trim(strtoupper($EmpNum));
        $test[$i] = "'$EmpNumber',{$query['EmpID']},'$ToWhatStoreID'";
        $i++;
    }
    // var_dump($test);
    $sql = "INSERT INTO t_empnumber (`f_EmpPNumber`,`f_EmpID`,`f_StoreID`) VALUES ";
    foreach ($test as $data) {
        $sql .= "(" . $data . "),";
    }
    $sql = trim($sql, ",");
    //echo $sql;
    //exit;
    $result = mysql_query($sql, $connection);

    if ($result && mysql_affected_rows() >= 1) {
        $logMessageArray = json_encode(array('EmpID' => $query['EmpID'], 'Details' => $queryarray));
        InsertLog('2', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '2');
        $returnCreateSharedStore['message'] = "Success Creating Employee Number";
    } else {
        $logMessageArray = json_encode(array('Reason' => 'An error occurred', 'Details' => $queryarray));
        InsertLog('2', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '2');
        $returnCreateSharedStore['message'] = "Failed  Creating Employee Number";
    }
    // $logType,$logFrom,$logRemarks,$logBy,$logStatus,$logFromSub
    // echo 'An error occurred';
    // mysql_close($connection);
    return $returnCreateSharedStore;
}

function EditEmployeeNumber($query) {
    global $connection;
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
    $updateCount = 0;
    foreach ($query as $toUpdate) {
        $sqlUpdateEmployeeNumber = "UPDATE `t_empnumber` SET `f_EmpPNumber` = '{$toUpdate['NewEmpNo']}' ,`f_StoreID` = '{$toUpdate['NewEmpStoreID']}'
                                     WHERE `f_EmpNumberID` = '{$toUpdate['ByEmpNumberID']}'";
        $result = mysql_query($sqlUpdateEmployeeNumber, $connection);
        if ($result && mysql_affected_rows() >= 1) {
            $updateCount = + mysql_affected_rows();
        }
    }

    if ($updateCount >= 1) {
        $logMessageArray = json_encode(array('Details' => $query));
        InsertLog('3', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '5');
        $returnAddSharedStore['message'] = "Success Updating Employee Number";
    } else {
        $logMessageArray = json_encode(array('Reason' => 'An error occurred', 'Details' => $query));
        InsertLog('3', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '5');
        $returnAddSharedStore['message'] = "Failed Updating Employee Number";
    }
    return $returnAddSharedStore;
}

function DeleteEmployeeNumber($query, $type) {
    global $connection;
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
    if ($type == 'WhereIn') {
        $sqlGetDetails = "SELECT f_EmpPNumber as PNum,f_StoreID FROM t_empnumber WHERE `f_EmpNumberID` IN ({$query})";
        $sql = "DELETE FROM `t_empnumber` WHERE `f_EmpNumberID` IN ({$query})";
    } else if ($type == 'Single') {
        $sqlGetDetails = "SELECT f_EmpPNumber as PNum,f_StoreID FROM t_empnumber WHERE `f_EmpNumberID` = '{$query[0]}'";
        $sql = "DELETE FROM `t_empnumber` WHERE `f_EmpNumberID` = '{$query[0]}'";
    }

    $resultGetPnumber = mysql_query($sqlGetDetails, $connection);
    while ($row = mysql_fetch_assoc($resultGetPnumber)) {
        $details[] = $row;
    }

    $result = mysql_query($sql, $connection);

    if ($result && mysql_affected_rows() >= 1) {
        $returnADeleteEmployeeNumber['message'] = "Success Deleting Employee Number";
        $logMessageArray = json_encode(array('Details' => $details));
        InsertLog('4', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '5');
    } else {
        $returnADeleteEmployeeNumber['message'] = "Failed Deleting Employee Number";
        $logMessageArray = json_encode(array('Reason' => 'An error occurred', 'Details' => $query));
        InsertLog('4', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '5');
    }
    return $returnADeleteEmployeeNumber;
}
