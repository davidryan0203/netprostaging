<?php

include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
session_start();

function AddStoreType($StoreTypeName) {
    require_once '../../functions/FunctionLog.php';
    global $connection;
    //echo "<pre>";
    //var_dump($StoreTypeName['toAddStoreType']);

    $sql = "INSERT INTO t_storetype (f_StoreTypeName) VALUES ('" . implode("'),('", $StoreTypeName['toAddStoreType']) . "')";
    //echo $sql . "<br>";
    $result = mysql_query($sql, $connection);
    if ($result && mysql_affected_rows() >= 1) {
        $returnAddedStoreType['message'] = "Succesfuly Added Store Type.";
        $logRemarks = json_encode(array('Process' => 'Store Type', 'Details' => $StoreTypeName));
        InsertLog('2', '3', $logRemarks, $_SESSION['LoginID'], 'Success', '8');
    } else {
        $returnAddedStoreType['message'] = "Failed Adding Store Type!";
        $logRemarks = json_encode(array('Process' => 'Store Type', 'Details' => $StoreTypeName));
        InsertLog('2', '3', $logRemarks, $_SESSION['LoginID'], 'Failed', '8');
    }

    return $returnAddedStoreType;
}

function AddUserRole($UserRoleName) {
    global $connection;
    //echo "<pre>";
    //var_dump($UserRoleName['toAddUserRole']);

    $sql = "INSERT INTO t_userrole (f_UserRoleName) VALUES  ('" . implode("'),('", $UserRoleName['toAddUserRole']) . "')";
    //echo $sql . "<br>";
    $result = mysql_query($sql, $connection);
    if ($result && mysql_affected_rows() >= 1) {
        $returnAddedUserRole['message'] = "Succesfuly Added User Role.";
        $logRemarks = json_encode(array('Process' => 'User Role', 'Details' => $UserRoleName));
        InsertLog('2', '3', $logRemarks, $_SESSION['LoginID'], 'Success', '8');
    } else {
        $returnAddedUserRole['message'] = "Failed Adding User Role!";
        $logRemarks = json_encode(array('Process' => 'User Role', 'Details' => $StoreTypeName));
        InsertLog('2', '3', $logRemarks, $_SESSION['LoginID'], 'Failed', '8');
    }

    return $returnAddedUserRole;
}

function AddEmpRole($EmpRoleName) {
    global $connection;
    //echo "<pre>";
    //var_dump($EmpRoleName['toAddEmpRole']);

    $sql = "INSERT INTO t_emprole (f_EmpRoleName) VALUES  ('" . implode("'),('", $EmpRoleName['toAddEmpRole']) . "')";
    //echo $sql . "<br>";
    $result = mysql_query($sql, $connection);
    if ($result && mysql_affected_rows() >= 1) {
        $returnAddedEmpRole['message'] = "Succesfuly Added Employee Role.";
        $logRemarks = json_encode(array('Process' => 'Employee Role', 'Details' => $EmpRoleName));
        InsertLog('2', '3', $logRemarks, $_SESSION['LoginID'], 'Success', '8');
    } else {
        $returnAddedEmpRole['message'] = "Failed Adding Employee Role!";
        $logRemarks = json_encode(array('Process' => 'Employee Role', 'Details' => $EmpRoleName));
        InsertLog('2', '3', $logRemarks, $_SESSION['LoginID'], 'Failed', '8');
    }

    return $returnAddedEmpRole;
}

function DeleteStoreType($StoreTypeIDList) {
    global $connection;
    //echo "<pre>";
    //var_dump($StoreTypeIDList['toDeleteStoreTypeID']);

    $sql = "DELETE FROM t_storetype WHERE f_StoreTypeID IN ('" . implode("',  '", $StoreTypeIDList['toDeleteStoreTypeID']) . "' ) ";
    //echo $sql . "<br>";
    $result = mysql_query($sql, $connection);
    if ($result && mysql_affected_rows() >= 1) {
        $returnDeleteStoreType['message'] = "Successfuly Deleted Store Type.";
        $logRemarks = json_encode(array('Process' => 'Store Type', 'Details' => $StoreTypeIDList));
        InsertLog('4', '3', $logRemarks, $_SESSION['LoginID'], 'Success', '8');
    } else {
        $returnDeleteStoreType['message'] = "Failed Deleting Store Type!";
        $logRemarks = json_encode(array('Process' => 'Store Type', 'Details' => $StoreTypeIDList));
        InsertLog('4', '3', $logRemarks, $_SESSION['LoginID'], 'Failed', '8');
    }
    return $returnDeleteStoreType;
}

function DeleteUserRole($UserRoleIDList) {
    global $connection;
    //echo "<pre>";
    //var_dump($UserRoleIDList['toDeleteUserRoleID']);

    $sql = "DELETE FROM t_userrole WHERE f_UserRoleID IN ('" . implode("',  '", $UserRoleIDList['toDeleteUserRoleID']) . "' ) ";
    //echo $sql . "<br>";
    $result = mysql_query($sql, $connection);
    if ($result && mysql_affected_rows() >= 1) {
        $returnDeleteUserRole['message'] = "Successfuly Deleted User Role.";
        $logRemarks = json_encode(array('Process' => 'User Role', 'Details' => $UserRoleIDList));
        InsertLog('4', '3', $logRemarks, $_SESSION['LoginID'], 'Success', '8');
    } else {
        $returnDeleteUserRole['message'] = "Failed Deleting User Role!";
        $logRemarks = json_encode(array('Process' => 'User Role', 'Details' => $UserRoleIDList));
        InsertLog('4', '3', $logRemarks, $_SESSION['LoginID'], 'Failed', '8');
    }
    return $returnDeleteUserRole;
}

function DeleteEmpRole($EmpRoleIDList) {
    global $connection;
    //echo "<pre>";
    //var_dump($EmpRoleIDList['toDeleteEmpRoleID']);

    $sql = "DELETE FROM t_emprole WHERE f_EmpRoleID IN ('" . implode("',  '", $EmpRoleIDList['toDeleteEmpRoleID']) . "' ) ";
    //echo $sql . "<br>";
    $result = mysql_query($sql, $connection);
    if ($result && mysql_affected_rows() >= 1) {
        $returnDeleteEmpRole['message'] = "Successfuly Deleted Employee Role.";
        $logRemarks = json_encode(array('Process' => 'Employee Role', 'Details' => $EmpRoleIDList));
        InsertLog('4', '3', $logRemarks, $_SESSION['LoginID'], 'Success', '8');
    } else {
        $returnDeleteEmpRole['message'] = "Failed Deleting Employee Role!";
        $logRemarks = json_encode(array('Process' => 'Employee Role', 'Details' => $EmpRoleIDList));
        InsertLog('4', '3', $logRemarks, $_SESSION['LoginID'], 'Failed', '8');
    }
    return $returnDeleteEmpRole;
}

function EditStoreType($StoreTypeIDList) {
    global $connection;
    //echo "<pre>";
    //var_dump($StoreTypeIDList);

    foreach ($StoreTypeIDList as $toUpdate) {
        $sql = "UPDATE `t_storetype` SET `f_StoreTypeName` = '{$toUpdate['NewStoreTypeName']}' WHERE `f_StoreTypeID` = '{$toUpdate['ToUpdateStoreTypeID']}'";
        $result = mysql_query($sql, $connection);
    }
    //echo $sql . "<br>";

    if ($result && mysql_affected_rows() >= 1) {
        $returnEditStoreType['message'] = "Succesfuly Edited Store Type.";
        $logRemarks = json_encode(array('Process' => 'Store Type', 'Details' => $StoreTypeIDList));
        InsertLog('3', '3', $logRemarks, $_SESSION['LoginID'], 'Success', '8');
    } else {
        $returnEditStoreType['message'] = "Failed Editting Store Type";
        $logRemarks = json_encode(array('Process' => 'Store Type', 'Details' => $StoreTypeIDList));
        InsertLog('3', '3', $logRemarks, $_SESSION['LoginID'], 'Failed', '8');
    }

    return $returnEditStoreType;
}

function EditUserRole($UserRoleIDList) {
    global $connection;
    //echo "<pre>";
    //print_r($UserRoleIDList['toEditUserRoleID']);

    foreach ($UserRoleIDList as $toUpdate) {
        $sql = "UPDATE `t_userrole` SET `f_UserRoleName` = '{$toUpdate['NewUserRoleName']}' WHERE `f_UserRoleID` = '{$toUpdate['ToUpdateUserRoleID']}'";
        $result = mysql_query($sql, $connection);
    }
    //echo $sql . "<br>";

    if ($result && mysql_affected_rows() >= 1) {
        $returnEditUserRole['message'] = "Succesfuly Edited User Role.";
        $logRemarks = json_encode(array('Process' => 'User Role', 'Details' => $UserRoleIDList));
        InsertLog('3', '3', $logRemarks, $_SESSION['LoginID'], 'Success', '8');
    } else {
        $returnEditUserRole['message'] = "Failed Editting User Role";
        $logRemarks = json_encode(array('Process' => 'User Role', 'Details' => $UserRoleIDList));
        InsertLog('3', '3', $logRemarks, $_SESSION['LoginID'], 'Failed', '8');
    }

    return $returnEditUserRole;
}

function EditEmpRole($EmpRoleIDList) {
    global $connection;
    //echo "<pre>";
    //print_r($EmpRoleIDList['toEditEmpRoleID']);

    foreach ($EmpRoleIDList as $toUpdate) {
        $sql = "UPDATE `t_emprole` SET `f_EmpRoleName` = '{$toUpdate['NewUserEmpName']}' WHERE `f_EmpRoleID` = '{$toUpdate['ToUpdateEmpRoleID']}'";
        $result = mysql_query($sql, $connection);
    }
    //echo $sql . "<br>";

    if ($result && mysql_affected_rows() >= 1) {
        $returnEditEmpRole['message'] = "Succesfuly Edited Employee Role.";
        $logRemarks = json_encode(array('Process' => 'Employee Role', 'Details' => $EmpRoleIDList));
        InsertLog('3', '3', $logRemarks, $_SESSION['LoginID'], 'Success', '8');
    } else {
        $returnEditEmpRole['message'] = "Failed Editting Employee Role";
        $logRemarks = json_encode(array('Process' => 'Employee Role', 'Details' => $EmpRoleIDList));
        InsertLog('3', '3', $logRemarks, $_SESSION['LoginID'], 'Failed', '8');
    }

    return $returnEditEmpRole;
}
