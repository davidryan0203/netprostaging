<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/datarecon/functions/DataReconFunction.php');
session_start();
$role = $_SESSION['UserRoleID'];
$query['to'] = $_GET['to'];
$query['from'] = $_GET['from'];

if ($role == '1') {
    $query['storeOwnerID'] = NULL;
} else {
    if ($role == '5') {
        $queryEmpID = $_SESSION['EmpProfileID'];
//    $type = "StoreLeader"; 
        $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$queryEmpID}' group by  c.f_StoreOwnerUserID limit 1 ";
        $result = mysql_query($sqlGetOwnerID, $connection);
        while ($row = mysql_fetch_assoc($result)) {
            $query['storeOwnerID'] = $row['f_StoreOwnerUserID'];
        }
    } else {
        $query['storeOwnerID'] = $_SESSION['UserProfileID'];
    }
}

$customerList = getCustomerList($query);

//var_dump($customerList);
?>


<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#example').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            columnDefs: [
                {"width": "8%", "targets": -1}
            ],
            colReorder: {
                fixedColumnsRight: 1
            },
            "iDisplayLength": 15,
            "aLengthMenu": [[15, 50, 100, -1], [15, 50, 100, "All"]],
            "oColVis": {
                "buttonText": "Show Columns"
            },
            "bAutoWidth": false,
            stateSave: true
        });
    });

    $(function () {
        $('[data-tooltip="tooltip"]').tooltip();
    });
</script>

<div class="table-responsive">
    <table id="example" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
        <thead>
            <tr>
                <th>Customer Name</th>
                <!--<th>Mobile</th>-->
                <!--<th>Email</th>-->
                <th>Location</th>
                <th>Feedback</th>                
            </tr>
        </thead>
        <tbody>
            <?php foreach ($customerList as $value) { ?>
                <tr>
                    <td> 
                        <i> <?php echo $value['f_CustomerFirstName'] . ' ' . $value['f_CustomerLastname']; ?></i>                                
                    </td>
                    <!--<td><?php echo $value['f_CustomerMobile']; ?></td>-->
                    <!--<td><?php echo $value['f_CustomerEmail']; ?></td>-->
                    <td>
                        <b>State:</b> <?php echo $value['f_CustomerState']; ?>
                    </td>
                    <td>
                        <a class="btn-sm btn-hover btn-success " data-toggle="modal" 
                           data-tooltip="tooltip" data-placement="bottom" title="View feedback"
                           href="admin/modal/ShowCustomerSurvey.php?customerID=<?php echo $value['f_CustomerDetailsID']; ?>&from=<?php echo $query['from']; ?>&to=<?php echo $query['to']; ?>" data-target="#ShowCustomerSurvey" onclick="openShowCustomerSurvey(this)" ><span class="glyphicon glyphicon-list-alt" ></span></a>
                    </td>
                </tr>   
            <?php } ?>
        </tbody> 
    </table>
</div>

<!-- Modal EditEmployeeModal-->
<div class="modal fade" id="ShowCustomerSurvey" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal  EditEmployeveModal -->