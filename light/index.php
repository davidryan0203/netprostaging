<?php
date_default_timezone_set('Australia/Melbourne');
ini_set('max_execution_time', 300);

$role = $_SESSION['UserRoleID'];
$first_day_this_month = date('01/m/Y'); // hard-coded '01' for first day
$last_day_this_month = date('d/m/Y');

$date = date('M-y');




include('function/NPSFunction.php');
require($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');

$SelectStoreTypes = SelectStoreTypes();
$blankPnumber = GetBlankPnumberList();

$countBlank = count($blankPnumber);
?>

<script type="text/javascript">
//test commit
    $(document).ready(function() {


        var from = $("#from").val();
        var to = $("#to").val();
        var role = $("#ShareType").attr('role');

        var shared = "";
        var storeType = "";
        var toURL = "";

        function setLoad() {
            var from = $("#from").val();
            var to = $("#to").val();

            toURL = "light/light.php?";


            var toLoad = toURL + shared + '&to=' + to + '&from=' + from + '&role=' + role + '&storeType=' + storeType;
            // console.log(toLoad);
            return toLoad;
        }

        $('#page-contents', function() {
            var toLoad = setLoad();
            var toLoadextended = toLoad;
            $.get(toLoadextended).success(
                    function(response, status, jqXhr) {
                        window.stop();
                        $('#page-contents').html();
                        $('#page-contents').html(response); // append();
                    }).error(function(response, status, jqXhr) {
            }).complete(function(response, status, jqXhr) {
            });
        });

        $("#from").datepicker({
            showOn: "button",
            buttonImage: "https://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
            buttonImageOnly: true, dateFormat: 'dd/mm/yy',
            numberOfMonths: 2,
            onSelect: function(selected) {
                $("#to").datepicker("option", "minDate", selected);
            }
        });

        $("#to").datepicker({
            showOn: "button",
            buttonImage: "https://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
            buttonImageOnly: true, dateFormat: 'dd/mm/yy',
            numberOfMonths: 2,

        });


        $('#refresh').click(function() {
            var toLoad = setLoad();
            var from = $("#from").val().split("/").reverse().join("-");
            var to = $("#to").val().split("/").reverse().join("-");

            var params = '&to=' + to + '&from=' + from;
            console.log($("#from").val().split("/").reverse().join("-"));

            $('#page-contents').load(toLoad);
        });

    });

    $(function() {
        $('[data-tooltip="tooltip"]').tooltip();
    });

<?php if ($_SESSION['isPrivate']) { ?>
        $("#ShareType").hide();
        console.log('hidden');
<?php } ?>


</script>


<div class="row">
    <h4 class="page-header"><i class="fa fa-shopping-cart fa-fw"></i>NPS Summary </h4>

    <div class="filters">

        <input type="text" id="from" name="from" class="form-control" value="<?php echo $first_day_this_month; ?>" readonly="true"/>
        <input type="text" id="to" name="to"  class="form-control" value="<?php echo $last_day_this_month; ?>" readonly="true"/>

        <button type="submit" class="btn btn-warning btn-circle" id="refresh" data-tooltip="tooltip" data-placement="bottom" title="Reload">
            <i class="fa fa-refresh"></i>
        </button>


    </div>
</div>

<?php
if ($countBlank >= 1) {
    $animated = "animated";
} else {
    $animated = " ";
    $countBlank = "";
}
?>
<div class="row">
    <p class="latest-record">
        <?php echo $latestRec['latest_record']; ?>
    </p>
    <a class='btn btn-success btn-xs infinite rubberBand' href="nps/function/SurveyTabs.php"  data-toggle="modal" data-target="#latestSurveysModal"
       data-tooltip="tooltip" data-placement="bottom" title="Latest Inserted Surveys Today" style='margin-top: -5px;margin-left: 10px;padding: 0 5px;'>
        <i class="fa fa-microphone "style='font-size: 17px; padding: 0px 1px; padding-top: 3px;'></i>
    </a>
    <a class='btn btn-info btn-xs infinite rubberBand animated' href="nps/function/npsNews.php"  data-toggle="modal" data-target="#npsChangesNews"
       data-tooltip="tooltip" data-placement="bottom" title="Last Nps News" style='margin-top: -5px;margin-left: 10px;padding: 0 5px;'>
        <i class="fa fa-paper-plane-o "style='font-size: 17px; padding: 0px 1px; padding-top: 3px;'></i>
    </a>
    <a class='btn btn-danger btn-xs infinite rubberBand <?php echo $animated; ?>' href="public_html.php?i=6"
       data-tooltip="tooltip" data-placement="bottom" title="Unassigned Survey Count" style='margin-top: -5px;margin-left: 10px;padding: 0 5px;'>
        <i class="fa fa-user "style='font-size: 17px; padding: 0px 1px; padding-top: 3px;'> </i><unassigned><?php echo $countBlank; ?> </unassigned>
    </a>
</div>




<div  id="page-contents" class="row page-contents" ></div>


<div class="modal fade" id="latestSurveysModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div> <!-- Feedback Modal -->

<div class="modal fade" id="npsChangesNews" tabindex="-1" role="dialog" aria-labelledby="npsChangesNews" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div>
    </div>
</div> <!-- Feedback Modal -->

<div class="modal fade" id="resolutionModal" tabindex="-1" role="dialog" aria-labelledby="resolutionModal" aria-hidden="true">
    <div class="modal-dialog" style="width: 700px">
        <div class="modal-content" id="resoulutionContent"></div>
    </div>
</div> <!-- Feedback Modal -->



<script type="text/javascript">

    $('a#resolutionFeed').click(function(ev) { // prepare modal for new data
        ev.preventDefault();
        var target = $(this).attr('href');
        console.log(target);
        $("#resoulutionContent").load(target, function() {
            $("#resolutionModal").modal("show");
        });
    });

    $("#resolutionModal").on("hidden.bs.modal", function() {
        $("#resoulutionContent").html("");
    });
</script>