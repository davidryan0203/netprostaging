<?php
ini_set('max_execution_time', 300);
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
date_default_timezone_set("America/Denver");
$sharedType = $_GET['shared'];
$to = mysql_real_escape_string($_GET['to']);
$from = mysql_real_escape_string($_GET['from']);
$role = mysql_real_escape_string($_GET['role']);
$first_day_this_month = date('01/m/Y'); // hard-coded '01' for first day
$last_day_this_month = date('t/m/Y');
$storeType = $_GET['storeType'];

//echo $sharedType."ere".$role;
//echo "<br>".$last_day_this_month."-$last_day_this_month <br>".$dateNow."here$to - $from";
?>

<script type="text/javascript">

    $(document).ready(function() {
        $(".toggle:contains('MTD')").attr('id', 'hideMTD').hide();
    });


</script>

<style> .panel-heading .accordion-toggle:after {
        /* symbol for "opening" panels */
        font-family: 'Glyphicons Halflings';  /* essential for enabling glyphicon */
        content: "\e114";    /* adjust as needed, taken from bootstrap.css */

        color: grey;         /* adjust as needed */
    }
    .panel-heading .accordion-toggle.collapsed:after {
        /* symbol for "collapsed" panels */
        content: "\e080";    /* adjust as needed, taken from bootstrap.css */
    }

    button[data-text="Episode"] {
        background-color: #f3bd12  !important;
        border-color: #f3bd12  !important;
    }
    button[data-text="Episode"]:hover:active {
        background-color: #f1aa22 !important;
        border-color: #f1aa22 !important;
    }

</style>

<!--tls nps reports-->
<section class='reports'>

    <div class='filters'>
        <input type="hidden" id="from" name="from" value="<?php echo $from; ?>"/>
        <input type="hidden" id="to" name="to"  value="<?php echo $to; ?>"/>
        <input type="hidden" id="role" name="role" value="<?php echo $role; ?>"/>
        <input type="hidden" id="storeType" name="storeType" value="<?php echo $storeType; ?>" />
        <input type="checkbox" style="display:none" <?php echo $sharedStatus; ?> id="shareType"/>
    </div>

    <div class="panel-group" role="tablist" id='accordion'>

        <!--Store Targets-->
        <div class="panel panel-default"  id="StoreTargetContent">
            <div class="panel-heading" role="tab" id="headingOne">
                <a role="button" data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="accordion-toggle collapsed" id="store3MmaTargetToggle">
                    <h4 class="panel-title">
                        <i class="fa fa-bullseye fa-fw"></i>Store <storerolling></storerolling> Target
                    </h4>
                </a>

                <div class="btn-group group1 pull-right" id="storetargetrolling-group" role="group" aria-label="...">
                    <button type="button" id="BtnStoreTarget3mma" data-text="3 Months Moving Average"  data-target="ThreeMonthsAverage" class="btn btn-xs btn-danger StoreTargetBtn">3 MMA</button>
                    <button type="button" id="BtnStoreTargetMtd" data-text="MTD" data-target="CurrentMonth" class="btn btn-xs btn-warning StoreTargetBtn">MTD</button>
                    <button type="button" id="BtnStoreTarget4week" data-text="4 Weeks"  data-target="FourWeekRolling" class="btn btn-xs btn-primary StoreTargetBtn">4 Weeks</button>
                    <!--<button type="button" id="StoreEpiNpsWeekly" data-text="3 Months"  data-target="NinetyDayRolling" class="btn btn-xs btn-info">3 Months</button>-->
                    <button type="button" id="BtnStoreTargetHtd" data-text="HTD"  data-target="HalfYearRolling" class="btn btn-xs btn-success StoreTargetBtn">HTD</button>
                    <!--<button type="button" id="BtnStoreTargetFTD" data-text="FTD"  data-target="FortNightRolling" class="btn btn-xs btn-default StoreTargetBtn">FTD</button>-->

                </div>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel">
                <div class="panel-body">
                    <div class="loader" id="StoreTargetloader"></div>
                    <div class="targetcontainer" id="CurrentMonth">
                        <div  id="StoreTargetContainer"></div>
                    </div>
                </div>
            </div>
        </div>


        <div class="panel panel-default"  id="EmployeeTargetContent">
            <div class="panel-heading" role="tab" id="headingTwo">
                <a role="button" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="accordion-toggle collapsed" id="employee3MmaTargetToggle">
                    <h4 class="panel-title">
                        <i class="fa fa-bullseye fa-fw"></i>Employee <emprolling></emprolling> Target
                    </h4>
                </a>

                <div class="btn-group group1 pull-right" id="employeetargetrolling-group" role="group" aria-label="...">
                    <button type="button" id="BtnEmployeeTarget3mma" data-text="3 Months Moving Average"  data-target="ThreeMonthsAverage" class="btn btn-xs btn-danger EmployeeTargetBtn">3 MMA</button>
                    <button type="button" id="BtnEmployeeTargetMtd" data-text="MTD" data-target="CurrentMonth" class="btn btn-xs btn-warning EmployeeTargetBtn">MTD</button>
                    <button type="button" id="BtnEmployeeTarget4week" data-text="4 Weeks"  data-target="FourWeekRolling" class="btn btn-xs btn-primary EmployeeTargetBtn">4 Weeks</button>
                    <!--<button type="button" id="EmployeeEpiNpsWeekly" data-text="3 Months"  data-target="NinetyDayRolling" class="btn btn-xs btn-info">3 Months</button>-->
                    <button type="button" id="BtnEmployeeTargetHtd" data-text="HTD"  data-target="HalfYearRolling" class="btn btn-xs btn-success EmployeeTargetBtn">HTD</button>
                    <!--<button type="button" id="BtnEmployeeTargetFTD" data-text="FTD"  data-target="FortNightRolling" class="btn btn-xs btn-default EmployeeTargetBtn">FTD</button>-->

                </div>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel">
                <div class="panel-body">
                    <div class="loader" id="EmployeeTargetloader"></div>
                    <div class="targetcontainer" id="CurrentMonth">
                        <div  id="EmployeeTargetContainer"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<script src="../includes/js/custom.js" type="text/javascript"></script>

<script type="text/javascript">

    $(document).ready(function() {

        //Parameter Variables
        var from = $("#from").val().split("/").reverse().join("-");
        var to = $("#to").val().split("/").reverse().join("-");
        var role = $("#role").val();

        //Hide loader and load contents function
        function loadContents(paramurl, loader, content) {
            $(content).html('');
            $(loader).show();
            $.ajax({
                url: paramurl,
                cache: false,
                success: function(html) {
                    $(content).html('');
                    $(content).append(html);
                },
                complete: function() {
                    $(loader).hide();
                }
            });
        }

        //Define parameters for each reports function
        function setParams() {
            var from = $("#from").val().split("/").reverse().join("-");
            var to = $("#to").val().split("/").reverse().join("-");
            var shared = 'no';
            var storeType = '1';
            var reconOwnerID = $("#reconOwnerID").val();
            var params = 'shared=' + shared + '&to=' + to + '&from=' + from + '&role=' + role + '&storeType=' + storeType + '&reconOwnerID=' + reconOwnerID;
            return params;
        }



        $('#StoreTargetContainer', function() {
            var isLoaded = $(this).hasClass("loaded");
            if (!isLoaded) {
                $("#BtnStoreTarget3mma").trigger("click");
            }
        });

        $('#storetargetrolling-group button').click(function() {
            var textval = $(this).data("text");
            var loader = "#StoreTargetloader";
            var content = "#StoreTargetContainer";
            var params = setParams();
            var setUrl = "";
            var url = "";

            switch (textval) {
                case 'MTD':
                    setUrl = "nps/summary/storeTargetMma.php?";
                    break;
                case '4 Weeks':
                    setUrl = "nps/summary/storeTargetMma.php?weekly=on&";
                    break;
                case '3 Months':
                    setUrl = "nps/summary/storeTargetMma.php?3months=on&";
                    break;
                case 'HTD':
                    setUrl = "nps/summary/storeTargetMma.php?htd=on&";
                    break;
                case 'FTD':
                    setUrl = "nps/summary/storeTargetMma.php?ftd=on&";
                    break;
                case '3 Months Moving Average':
                    setUrl = "nps/summary/storeTargetMma.php?3mma=on&";
                    break;
            }

            $("#storetargetrolling-group button").removeClass("active");
            $(this).addClass("active");
            var isCollapsed = $("#store3MmaTargetToggle").hasClass("collapsed");
            $("#store3MmaTargetToggle").addClass('loaded');
            if (isCollapsed) {
                $("#store3MmaTargetToggle").click();
            }


            $("storerolling").html(textval);
            url = setUrl + params;
            loadContents(url, loader, content);

        });
        $('#store3MmaTargetToggle').click(function() {

            var isLoaded = $(this).hasClass("loaded");
            if (!isLoaded) {
                $("#BtnStoreTarget3mma").trigger("click");
            }
        });


        $('#employeetargetrolling-group button').click(function() {
            var textval = $(this).data("text");
            var loader = "#EmployeeTargetloader";
            var content = "#EmployeeTargetContainer";
            var params = setParams();
            var setUrl = "";
            var url = "";

            switch (textval) {
                case 'MTD':
                    setUrl = "nps/summary/employeeTarget.php?";
                    break;
                case '4 Weeks':
                    setUrl = "nps/summary/employeeTarget.php?weekly=on&";
                    break;
                case '3 Months':
                    setUrl = "nps/summary/employeeTarget.php?3months=on&";
                    break;
                case 'HTD':
                    setUrl = "nps/summary/employeeTarget.php?htd=on&";
                    break;
                case 'FTD':
                    setUrl = "nps/summary/employeeTarget.php?ftd=on&";
                    break;
                case '3 Months Moving Average':
                    setUrl = "nps/summary/employeeTarget.php?3mma=on&";
                    break;
            }

            $("#employeetargetrolling-group button").removeClass("active");
            $(this).addClass("active");
            var isCollapsed = $("#employee3MmaTargetToggle").hasClass("collapsed");
            $("#employee3MmaTargetToggle").addClass('loaded');
            if (isCollapsed) {
                $("#employee3MmaTargetToggle").click();
            }


            $("emprolling").html(textval);
            url = setUrl + params;
            loadContents(url, loader, content);

        });
        $('#employee3MmaTargetToggle').click(function() {

            var isLoaded = $(this).hasClass("loaded");
            if (!isLoaded) {
                $("#BtnEmployeeTarget3mma").trigger("click");
            }
        });





        /*  $(".btn").on('click', function()
         {
         var active = $(this).hasClass('active');
         console.log(active);
         var panels = localStorage.panels === undefined ? new Array() : JSON.parse(localStorage.panels);
         if ($.inArray(active, panels) == -1) //check that the element is not in the array
         panels.push(active);
         localStorage.panels = JSON.stringify(panels);
         });

         $(".btn").on('click', function()
         {
         var active = $(this).attr('id');
         // console.log(this);
         var panels = localStorage.panels === undefined ? new Array() : JSON.parse(localStorage.panels);
         var elementIndex = $.inArray(active, panels);
         if (elementIndex !== -1) //check the array
         {
         panels.splice(elementIndex, 1); //remove item from array
         }
         localStorage.panels = JSON.stringify(panels); //save array on localStorage
         });

         var panels = localStorage.panels === undefined ? new Array() : JSON.parse(localStorage.panels); //get all panels
         for (var i in panels) { //<-- panel is the name of the cookie
         if ($("#" + panels[i]).hasClass('panel-collapse')) // check if this is a panel
         {
         $("#" + panels[i]).collapse("show");
         }
         } */

        /* $('#StoreNPSTier', function() {
         var params = setParams();
         var url = "nps/charts/TierLevel_Bar.php?tiersort=Fixed&3mma=on&" + params;
         var hide = "#StoreNPSTierLoader";
         var show = "#StoreNPSTier";

         loadContents(url, hide, show);
         });

         $('#StoreMtdNpsContainer', function() {
         var params = setParams();
         var url = "nps/summary/Fixed.php?" + params;
         var hide = "#StoreMtdNpsloader";
         var show = "#StoreMtdNpsContainer";
         loadContents(url, hide, show);
         });

         $('#StoreNpsWeeklyContainer', function() {
         var params = setParams();
         var url = "nps/summary/episode.php?weekly=on&" + params;
         var hide = "#StoreNpsWeeklyloader";
         var show = "#StoreNpsWeeklyContainer";
         loadContents(url, hide, show);
         });

         $('#EmployeeMtdNpsContainer', function() {
         var params = setParams();
         var url = "nps/summary/employeeNpsEpisode.php?" + params;
         var hide = "#EmployeeMtdNpsloader";
         var show = "#EmployeeMtdNpsContainer";
         loadContents(url, hide, show);
         });

         $('#WeeklyEmployeeNpsContainer', function() {
         var params = setParams();
         var url = "nps/summary/employeeNpsEpisode.php?weekly=on&" + params;
         var hide = "#WeeklyEmployeeNpsloader";
         var show = "#WeeklyEmployeeNpsContainer";
         loadContents(url, hide, show);
         });

         $('#weekly-group button').click(function() {
         var textval = $(this).data("text");
         var loader = "#WeeklyEmployeeNpsloader";
         var content = "#WeeklyEmployeeNpsContainer";
         var params = setParams();
         var setUrl = "";
         var url = "";

         switch (textval) {
         case 'Interaction':
         setUrl = "nps/summary/employeeNps.php?weekly=on&";
         break;
         case 'Mobility (OTG)':
         setUrl = "nps/summary/employeeNpsMobile.php?weekly=on&";
         break;
         case 'Fixed (Home)':
         setUrl = "nps/summary/employeeNpsFixed.php?weekly=on&";
         break;
         case 'Episode':
         setUrl = "nps/summary/employeeNpsEpisode.php?weekly=on&";
         break;
         }


         $("#weekly-group button").removeClass("active");
         $(this).addClass("active");


         $("weekly").html(textval);
         url = setUrl + params;
         loadContents(url, loader, content);

         });

         $('#MergedMtdEmployeeNpsloader', function() {
         var params = setParams();
         var url = "nps/summary/employeeMergedNpsEpisode.php?" + params;
         var hide = "#MergedMtdEmployeeNpsloader";
         var show = "#MergedMtdEmployeeNpsContainer";
         loadContents(url, hide, show);
         });

         $('#FourWeekMergedEmployeeNps', function() {
         var params = setParams();
         var url = "nps/summary/employeeMergedNpsEpisode.php?weekly=on&" + params;
         var hide = "#FourWeekMergedEmployeeNpsloader";
         var show = "#FourWeekMergedEmployeeNpsContainer";
         loadContents(url, hide, show);
         });

         $('#StoreNps3MonthContainer', function() {
         var params = setParams();
         var url = "nps/summary/Mobile.php?3months=on&" + params;
         var hide = "#StoreNps3Monthloader";
         var show = "#StoreNps3MonthContainer";
         loadContents(url, hide, show);
         });
         //removed July 09,
         $('#nps3months').click(function() {
         var isLoaded = $(this).hasClass("loaded");
         var textval = $('#3Month-group button').data("text");
         var loader = "#StoreNps3Monthloader";
         var content = "#StoreNps3MonthContainer";
         var params = setParams();
         var setUrl = "";
         var url = "";

         if (!isLoaded) {
         switch (textval) {

         case 'Mobility (OTG)':
         setUrl = "nps/summary/Mobile.php?3months=on&";
         break;
         case 'Fixed (Home)':
         setUrl = "nps/summary/Fixed.php?3months=on&";
         break;
         }

         $("month3").html(textval);
         url = setUrl + params;
         loadContents(url, loader, content);
         $(this).addClass("loaded");
         }
         });

         $('#3Month-group button').click(function() {
         var textval = $(this).data("text");
         var loader = "#StoreNps3Monthloader";
         var content = "#StoreNps3MonthContainer";
         var params = setParams();
         var setUrl = "";
         var url = "";

         switch (textval) {

         case 'Mobility (OTG)':
         setUrl = "nps/summary/Mobile.php?3months=on&";
         break;
         case 'Fixed (Home)':
         setUrl = "nps/summary/Fixed.php?3months=on&";
         break;
         }

         $("#3Month-group button").removeClass("active");
         $(this).addClass("active");

         $("month3").html(textval);
         url = setUrl + params;
         loadContents(url, loader, content);

         });

         $('#StoreNps3MMAContainer', function() {
         var params = setParams();
         var url = "nps/summary/Mobile.php?3mma=on&" + params;
         var hide = "#StoreNps3MMAloader";
         var show = "#StoreNps3MMAContainer";
         loadContents(url, hide, show);
         });

         $('#3MonthsEmployeeNpsContainer', function() {
         var params = setParams();
         var url = "nps/summary/employeeNpsMobile.php?3months=on&" + params;
         var hide = "#3MonthsEmployeeNpsloader";
         var show = "#3MonthsEmployeeNpsContainer";
         loadContents(url, hide, show);
         });

         $('#emp3months').click(function() {
         var isLoaded = $(this).hasClass("loaded");
         var textval = $('#3mnth-group button').data("text");
         var loader = "#3MonthsEmployeeNpsloader";
         var content = "#3MonthsEmployeeNpsContainer";
         var params = setParams();
         var setUrl = "";
         var url = "";

         if (!isLoaded) {
         switch (textval) {
         case 'Interaction':
         setUrl = "nps/summary/employeeNps.php?3months=on&";
         break;
         case 'Mobility (OTG)':
         setUrl = "nps/summary/employeeNpsMobile.php?3months=on&";
         break;
         case 'Fixed (Home)':
         setUrl = "nps/summary/employeeNpsFixed.php?3months=on&";
         break;
         case 'Episode':
         setUrl = "nps/summary/employeeNpsEpisode.php?3months=on&";
         break;
         }

         $("threemonth").html(textval);
         url = setUrl + params;
         loadContents(url, loader, content);
         $(this).addClass("loaded");
         }
         });

         $('#3mnth-group button').click(function() {
         var textval = $(this).data("text");
         var loader = "#3MonthsEmployeeNpsloader";
         var content = "#3MonthsEmployeeNpsContainer";
         var params = setParams();
         var setUrl = "";
         var url = "";

         switch (textval) {
         case 'Interaction':
         setUrl = "nps/summary/employeeNps.php?3months=on&";
         break;
         case 'Mobility (OTG)':
         setUrl = "nps/summary/employeeNpsMobile.php?3months=on&";
         break;
         case 'Fixed (Home)':
         setUrl = "nps/summary/employeeNpsFixed.php?3months=on&";
         break;
         case 'Episode':
         setUrl = "nps/summary/employeeNpsEpisode.php?3months=on&";
         break;
         }

         $("threemonth").html(textval);
         url = setUrl + params;
         loadContents(url, loader, content);

         });

         $('#StoreNpsHTDContainer', function() {
         var params = setParams();
         var url = "nps/summary/episode.php?htd=on&" + params;
         var hide = "#StoreNpsHTDloader";
         var show = "#StoreNpsHTDContainer";
         loadContents(url, hide, show);
         });

         $('#EmployeeHtdNpsContainer', function() {
         var params = setParams();
         var url = "nps/summary/employeeNpsEpisode.php?htd=on&" + params;
         var hide = "#EmployeeHtdNpsloader";
         var show = "#EmployeeHtdNpsContainer";
         loadContents(url, hide, show);
         });

         $('#3MMAEmployeeNpsContainer', function() {
         var params = setParams();
         var url = "nps/summary/employeeNpsFixed.php?3mma=on&" + params;
         var hide = "#3mmaEmployeeNpsloader";
         var show = "#3MMAEmployeeNpsContainer";
         loadContents(url, hide, show);
         });

         $('#3MMAMergeEmployeeNpsContainer', function() {
         var params = setParams();
         var url = "nps/summary/employeeMergedNpsMobile.php?3mma=on&" + params;
         var hide = "#3MMAMergeEmployeeNpsloader";
         var show = "#3MMAMergeEmployeeNpsContainer";
         loadContents(url, hide, show);
         });

         $('#EmployeeCombinedNpsContainer', function() {
         var params = setParams();
         var url = "nps/summary/employeeMobileFixed.php?mtd=on&" + params;
         var hide = "#EmployeeCombinedNpsLoader";
         var show = "#EmployeeCombinedNpsContainer";
         loadContents(url, hide, show);
         }); */


    });
</script>
