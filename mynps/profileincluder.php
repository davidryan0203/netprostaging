<?php
error_reporting(E_ALL);
include ('function/InteractionMessage.php');
include ('function/SetColor.php');
include ('function/getProfileInfo.php');
include($_SERVER['DOCUMENT_ROOT'] . '/nps/function/NPSFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/EmployeeFunction.php');

$profileImg = $_SESSION['ProfileImg'];


$profileID = $_SESSION['UserProfileID'];
$to = $_GET['to'];
$from = $_GET['from'];
$role = $_GET['role'];
$empID = $_SESSION['EmpProfileID'];
$storeType = $_GET['storeType'];

//Get Employee Store Score Data
$StoreNps = array();
$storeNpsData = getStoreNps("no", $profileID, $empID, $to, $from, $role, $storeType);
foreach ($storeNpsData['Merged'] as $storeData) {
    $StoreNps[] = array(
        'StoreName' => $storeData['StoreName'],
        'InteractionScore' => number_format($storeData['InteractionNPS'], npsDecimal()),
        'InteractionTier' => $storeData['InteractionTier'],
        'EpisodeScore' => number_format($storeData['EpisodeNPS'], npsDecimal()),
        'EpisodeTier' => $storeData['EpisodeTier'],
        'Advocate' => $storeData['Advocates'],
        'Passive' => $storeData['Pasives'],
        'Detractor' => $storeData['Detractors'],
        'TotalSurvey' => $storeData['TotalSurvey']
    );
}

//Get profile contents Data
$empProf = SelectEmployee($empID);
$PersonalInteractionStyle = getSetColor($empInteractionNPS, 'interaction');
$PersonalEpisodeStyle = getSetColor($empEpisodeNPS, 'episode');

$EmpInteraction = array();
$empData = getEmployeeData($empID, $from, $to);

foreach ($empData as $value) {
//Employee Profile
    $empName = $value['EmpName'];
    $empFname = $value['Firstname'];
    $empLname = $value['Lastname'];
    $empStore = $value['Store'];
    $empRole = $value['Role'];
}


foreach ($empData as $empTarget) {
    $EmpNpsDetails[] = array(
//Interaction Scores
        'StoreName' => $empTarget['Store'],
        'InteractionAdvocate' => $empTarget['EmpInteractionTotalAdvocate'],
        'InteractionPassive' => $empTarget['EmpInteractionTotalPassive'],
        'InteractionDetractor' => $empTarget['EmpInteractionTotalDetractor'],
        'InteractionTotalSurvey' => $empTarget['EmpTotalInteractionSurvey'],
        'InteractionNpsScore' => number_format($empTarget['EmpInteractionScore'], npsDecimal()),
//Episode Scores
        'EpisodeAdvocate' => $empTarget['EmpEpisodeTotalAdvocate'],
        'EpisodePassive' => $empTarget['EmpEpisodeTotalPassive'],
        'EpisodeDetractor' => $empTarget['EmpEpisodeTotalDetractor'],
        'EpisodeTotalSurvey' => $empTarget['EmpTotalEpisodeSurvey'],
        'EpisodeNpsScore' => number_format($empTarget['EmpEpisodeScore'], npsDecimal()),
    );
}

//echo "<pre>";
//var_dump($EmpNpsDetails);
function generateNpsSummary($EmpNpsDetails) {
//echo "<pre>"; var_dump($EmpNpsDetails);
    $sumAdvo = 0;
    $sumPas = 0;
    $sumDet = 0;
    $sumSurvey = 0;
    foreach ($EmpNpsDetails as $data) {
        $sumAdvoInt += intval($data['InteractionAdvocate']);
        $sumPasInt += intval($data['InteractionPassive']);
        $sumDetInt += intval($data['InteractionDetractor']);
        $sumSurveyInt += intval($data['InteractionTotalSurvey']);

        $sumAdvoEpi += intval($data['EpisodeAdvocate']);
        $sumPasEpi += intval($data['EpisodePassive']);
        $sumDetEpi += intval($data['EpisodeDetractor']);
        $sumSurveyEpi += intval($data['EpisodeTotalSurvey']);
    }
// echo "$sumAvo - $sumDet / $sumSurvey * 100<bR>";
    $NPSInt = ((intval($sumAdvoInt) - intval($sumDetInt)) / intval($sumSurveyInt)) * 100;
    $NPSInt = number_format($NPSInt, npsDecimal());

    $NPSEpi = ((intval($sumAdvoEpi) - intval($sumDetEpi)) / intval($sumSurveyEpi)) * 100;
    $NPSEpi = number_format($NPSEpi, npsDecimal());

    $returnData = array('SummaryNPSInt' => $NPSInt, 'SummaryAdvocateInt' => $sumAdvoInt, 'SummaryDetractorsInt' => $sumDetInt, 'SummaryPassiveInt' => $sumPasInt, 'SummaryTotalSurveyInt' => $sumSurveyInt,
        'SummaryNPSEpi' => $NPSEpi, 'SummaryAdvocateEpi' => $sumAdvoEpi, 'SummaryDetractorsEpi' => $sumDetEpi, 'SummaryPassiveEpi' => $sumPasEpi, 'SummaryTotalSurveyEpi' => $sumSurveyEpi);


    return $returnData;
}

$npsSummaryScore = generateNpsSummary($EmpNpsDetails);


$storeRank = getEmployeeImpactRank('Interaction', 'AllEmployees', 'No', $from, $to);

$weeklyto = date("Y-m-d");
//$weeklyfrom = date("Y-m-d", strtotime('4 weeks ago'));
$weeklyfrom = date("Y-m-d", strtotime('-27 days'));

$intWeek = getWeekly('Interaction', $empID);
$epiWeek = getWeekly('Episode', $empID);
//echo $npsSummaryScore;
//echo "<pre>";
//print_r($intWeek);
?>


<script src="includes/js/plugins.js"></script>
<!--<script src="includes/js/circletype.js"></script>-->
<script type="text/javascript">
    $(document).ready(function() {
        var from = $("#from").val().split("/").reverse().join("-");
        var to = $("#to").val().split("/").reverse().join("-");
        var empID = $("#empid").val();

        $("#toggleFeedack").click(function() {
            $('#CustomerFeedback').fadeOut('slow').load('nps/summary/customerfeedback.php?to=' + to + '&from=' + from + '&empID=' + empID + '&isProfile=Yes').fadeIn("slow");
        });

        $('.showDate').html(" " + from + ' To ' + to);

    });
</script>

<input type="hidden" id="from" name="from" value="<?php echo $from; ?>"/>
<input type="hidden" id="to" name="to"  value="<?php echo $to; ?>"/>
<input type="hidden" id="empid" name="empid"  value="<?php echo $empID; ?>"/>

<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div id="sideprofile">
            <div class="row">
                <div class="col-lg-12">
                    <center>
                        <img class="img-circle" style="border: 2px solid rgba(72, 132, 124, 0.35);" src="<?php echo $profileImg; ?>" id="ProfilePic">
                        <h2 id="user-name">
                            <i class="fa fa-user"></i> <?php echo $empProf[0]['f_EmpFirstName'] . " " . $empProf[0]['f_EmpLastName']; ?>
                        </h2>
                        <button type="button" class="btn btn-sm btn-primary" id="toggleFeedack"><i class="fa fa-comments-o fw"></i> Feedback</button>
                        <button type="button" class="btn btn-sm btn-warning" data-toggle="modal" href="admin/modal/EditEmployeeModal.php?empID=<?php echo $empID; ?>" data-target="#EditEmployeeModal" onclick="openModalEdit(this)"><i class="fa fa-wrench"></i> Profile</button>
                        <button type="button" class="btn btn-sm btn-danger" togolink="imageEditor/index.html?photoUrl=<?php echo $profileImg; ?>" id="ProfilePicture" onclick="LoadProfilePic(this)"><i class="fa fa-plus"></i> Picture</button>
                    </center>
                    <hr>

                    <section class="row" id="userFeedback">
                        <div class="col-lg-12">
                            <h4><i class="fa fa-comments-o fw"  style="margin-bottom: 10px;"></i> Your Customer Feedback:</h4>
                            <div class="col-xs-12 text-center">
                                <div class="col-xs-3 col-sm-3 emphasis">
                                    <h2><strong>
                                            <a data-toggle="modal" class="feedback"  href="nps/function/FeedbackModal.php?empID=<?php echo $empID; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>" data-target="#feedbackmodal" >
                                                <i class="fa fa-comments-o fa-fw"></i><?php echo $npsSummaryScore['SummaryTotalSurveyInt']; ?>
                                            </a>
                                        </strong>
                                    </h2>
                                    <p><small>All</small></p>
                                </div>

                                <div class="col-xs-3 col-sm-3 emphasis">
                                    <h2>
                                        <strong>
                                            <a data-toggle="modal" class="feedback"  href="nps/function/FeedbackModal.php?rate=advo&empID=<?php echo $empID; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>"  data-target="#feedbackmodal">
                                                <i class="fa fa-comments-o fa-fw"></i><?php echo $npsSummaryScore['SummaryAdvocateInt']; ?>
                                            </a>
                                        </strong>
                                    </h2>
                                    <p><small>Advocates</small></p>
                                </div>
                                <div class="col-xs-3 col-sm-3 emphasis">
                                    <h2><strong>
                                            <a data-toggle="modal" class="feedback"  href="nps/function/FeedbackModal.php?rate=pass&empID=<?php echo $empID; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>"  data-target="#feedbackmodal">
                                                <i class="fa fa-comments-o fa-fw"></i><?php echo $npsSummaryScore['SummaryPassiveInt']; ?>
                                            </a>
                                        </strong>
                                    </h2>
                                    <p><small>Passives</small></p>
                                </div>

                                <div class="col-xs-3 col-sm-3 emphasis">
                                    <h2><strong>
                                            <a data-toggle="modal" class="feedback"  href="nps/function/FeedbackModal.php?rate=detract&empID=<?php echo $empID; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>"
                                               data-target="#feedbackmodal">
                                                <i class="fa fa-comments-o fa-fw"></i><?php echo $npsSummaryScore['SummaryDetractorsInt']; ?>
                                            </a>
                                        </strong>
                                    </h2>
                                    <p><small>Detractors</small></p>
                                </div>
                            </div>
                        </div>
                    </section><br>

                    <?php foreach ($empProf as $empValue) { ?>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <ul class="list-group profile-details">
                                    <li class="list-group-item"><i class="glyphicon glyphicon-map-marker"></i> <?php echo $empValue['f_StoreListName']; ?></li>
                                    <li class="list-group-item"><i class="glyphicon glyphicon-lock"></i> <?php echo $empValue['f_EmpRoleName']; ?><br></li>
                                    <li class="list-group-item"><i class="glyphicon glyphicon-user"></i> <?php echo $empValue['f_EmpPNumber']; ?></li>
                                </ul>
                            </div>
                        </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>


    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <div id='profile-content'>

            <section class="row" id="badges">
                <div class="col-lg-12" id='ScoreBadges'>
                    <h4 class="sub-header"><i class="fa fa-comments-o fw"></i> Overall NPS - Current Month to Date</h4>
                    <div class='col-xs-6'>
                        <center>
                            <div class="nps-badge-gold animated pulse infinite">
                                <h4 class="mynps-ribbon-heading-interaction">
                                    <?php echo $npsSummaryScore['SummaryNPSInt']; ?>
                                </h4>
                                <small>Interaction</small>
                            </div>
                        </center>
                    </div>
                    <div class='col-xs-6'>
                        <center>
                            <div class="nps-badge-orange animated pulse infinite" id="mynPS-badge-orange">
                                <h4 class="mynps-ribbon-heading-episode">
                                    <?php echo $npsSummaryScore['SummaryNPSEpi']; ?>
                                </h4>
                                <small>Episode</small>
                            </div>
                        </center>
                    </div>
                </div>
            </section><br>
            <section class="row" id="user-status">
                <?php
                if (!is_null($EmpNpsDetails)) {
                    $spanID2 = 0;
                    foreach ($EmpNpsDetails as $key => $value) {
                        ?>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <h4 class="sub-header"><i class="fa fa-user"></i> NPS Status: <?php echo $value['StoreName']; ?></h4>

                            <?php $getClassEmployee = getClassEmployee($value['InteractionNpsScore']); ?>
                            <div <?php echo $getClassEmployee['class']; ?> id='emp-target' style="margin-bottom: 10px;">
                                <!--<h5><i class="fa fa-certificate"></i> NPS: <?php echo $value['InteractionNpsScore']; ?>%</h5>-->
                                <h5><i class="fa fa-exclamation-circle"></i> <?php echo $getClassEmployee['message']; ?></h5>

                                <?php if ($value['InteractionNpsScore'] > 1 && $value['InteractionNpsScore'] < 75) { ?>
                                    <script type="text/javascript">
                                        $(document).ready(function() {
                                            // var IntercationNPS = <?php echo $value['InteractionNpsScore']; ?>;
                                            var NewTotalSurvey = <?php echo $value['InteractionTotalSurvey']; ?>;
                                            var newInterAdvoTotal = <?php echo $value['InteractionAdvocate']; ?>;
                                            var InteractionTotalDetractorTotal = <?php echo $value['InteractionDetractor']; ?>;
                                            // var InteractionPassivePercentage = <?php echo $value['InteractionPassive']; ?>;
                                            //  var InteractionTotalDetractorPercentage    = <?php echo $value['InteractionDetractor']; ?>;
                                            //alert(newInterAdvoTotal);
                                            var counter = 0;
                                            var newDetPercentage = 0;
                                            var newAdvoPercentage = 0;
                                            var newIntercationNPS = 0;

                                            while (newIntercationNPS < 74) {
                                                newInterAdvoTotal += 1;
                                                NewTotalSurvey += 1;
                                                // console.log(NewTotalSurvey + " -" + newInterAdvoTotal + "  +" + counter + " " + newIntercationNPS);
                                                newAdvoPercentage = (newInterAdvoTotal / NewTotalSurvey) * 100;
                                                newDetPercentage = (InteractionTotalDetractorTotal / NewTotalSurvey) * 100;
                                                newIntercationNPS = newAdvoPercentage - newDetPercentage;
                                                //npscore = InteractionTotalDetractorPercentage +  newAdvoPercentage + InteractionPassivePercentage;
                                                counter += 1;
                                            }
                                            //    var needed = counter - newInterAdvoTotal;
                                            $("#ToGet<?php echo $spanID2; ?>").append(counter + " Advocates needed.");
                                        });
                                    </script>
                                    <?php $idSharet = "ToGet{$spanID2}"; ?>
                                    <h5><i class="fa fa-check-square "></i> <span id="<?php echo $idSharet; ?>"></span> </h5>
                                <?php } elseif ($value['InteractionNpsScore'] == 0) { ?>
                                    <h5><i class="fa fa-check-square"></i> Need at least 1 Advocate</h5>
                                <?php } else { ?>
                                    <h5><i class="fa fa-check-square"></i> Goals Accomplished!</h5>
                                <?php } ?>
                            </div>
                        </div><!--end needed Survey-->
                        <?php
                        $spanID2++;
                    }
                    ?>
                <?php } else { ?>
                    <div class="col-lg-6">
                        <h4 class="sub-header"><i class="fa fa-user"></i> NPS Status </h4>
                        <div class='alert alert-info' style="margin-bottom: 10px;">
                            <p><i class="fa fa-exclamation-circle"></i> No Records yet.</p>
                            <p><i class="fa fa-check-square"></i> Need at least 1 Advocate</p>
                        </div>
                    </div>
                <?php } ?>


                <div id="ranking">
                    <div class="col-lg-12">
                        <h4 class="sub-header" style="margin-top: 20px;"><i class="fa fa-user"></i> Rank and Impact to Store</h4>
                        <?php
                        $i = 1;
                        foreach ($storeRank[0] as $values) {
                            if ($values['Data']['EmpID'] == $empID) {
                                ?>
                                <span class="badge" style="padding: 10px;">
                                    <h5 style="color: #fec620"><i class="fa fa-trophy"></i> <?php echo $values['Data']['Store'] . ":Rank " . $i; ?> </h5>
                                    <h5 style="color: #337ab7"><i class="fa fa-user"></i> <?php echo "Impact: " . number_format($values['EmpImpact'], 2); ?> </h5>
                                </span>
                                <?php
                            }
                            $i++;
                        }
                        ?>
                    </div>
                </div>
            </section><br>

            <section class="row" id="badges">
                <div class="col-lg-12" id='ScoreBadges'>
                    <h4 class="sub-header"><i class="fa fa-comments-o fw"></i> 4 Week Rolling Interaction NPS </h4>
                    <table id="weeklyint" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
                        <thead>
                            <tr>
                                <th>Store</th>
                                <th>AD</th>
                                <th>PA</th>
                                <th>D</th>
                                <th>No Score</th>
                                <th>Total</th>
                                <th>NPS</th>
                                <th>Impact</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($intWeek[0] as $value) {
                                //echo $key;
                                ?>
                                <tr>
                                    <td><?php echo $value['Data']['Store']; ?></td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModal.php?rate=advo&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $weeklyfrom; ?>&to=<?php echo $weeklyto; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpAdvocate']; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModal.php?rate=pass&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $weeklyfrom; ?>&to=<?php echo $weeklyto; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpPassive']; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModal.php?rate=detract&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $weeklyfrom; ?>&to=<?php echo $weeklyto; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpDetractor']; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModal.php?rate=pass&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $weeklyfrom; ?>&to=<?php echo $weeklyto; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpNoScore']; ?></a>
                                    </td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModal.php?empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $weeklyfrom; ?>&to=<?php echo $weeklyto; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&modulator=exlcude" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpTotalSurvey']; ?>
                                        </a>
                                    </td>
                                    <td><?php echo number_format($value['Data']['EmpInteraction'], npsDecimal()); ?></td>
                                    <td><?php echo number_format($value['EmpImpact'], 2); ?></td>
                                </tr>
                            <?php }
                            ?>
                        </tbody>
                    </table>
                </div>
            </section><br>

            <section class="row" id="badges">
                <div class="col-lg-12" id='ScoreBadges'>
                    <h4 class="sub-header"><i class="fa fa-comments-o fw"></i> 4 Week Rolling Episode NPS </h4>
                    <table id="weeklyepi" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
                        <thead>
                            <tr>
                                <th>Store</th>
                                <th>AD</th>
                                <th>PA</th>
                                <th>D</th>
                                <th>No Score</th>
                                <th>Total</th>
                                <th>NPS</th>
                                <th>Impact</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($epiWeek[0] as $value) {
                                //echo $key;
                                ?>
                                <tr>
                                    <td><?php echo $value['Data']['Store']; ?></td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModal.php?rate=advo&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $weeklyfrom; ?>&to=<?php echo $weeklyto; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&type=episode" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpAdvocate']; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModal.php?rate=pass&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $weeklyfrom; ?>&to=<?php echo $weeklyto; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&type=episode" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpPassive']; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModal.php?rate=detract&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $weeklyfrom; ?>&to=<?php echo $weeklyto; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&type=episode" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpDetractor']; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModal.php?rate=pass&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $weeklyfrom; ?>&to=<?php echo $weeklyto; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&type=episode" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpNoScore']; ?></a>
                                    </td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModal.php?empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $weeklyfrom; ?>&to=<?php echo $weeklyto; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&modulator=exlcude&type=episode" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpTotalSurvey']; ?>
                                        </a>
                                    </td>
                                    <td><?php echo number_format($value['Data']['EmpEpisode'], npsDecimal()); ?></td>
                                    <td><?php echo number_format($value['EmpImpact'], 2); ?></td>
                                </tr>
                            <?php }
                            ?>
                        </tbody>
                    </table>
                </div>
            </section><br>

            <section id="UserStoreTarget">
                <div class="row">
                    <?php if (count($StoreNps) > 0) { ?>
                        <?php foreach ($StoreNps as $value) { ?>
                            <div class='col-lg-6  col-md-6 col-sm-12 col-xs-12 nps-target'>
                                <h4 class="sub-header"><i class="fa fa-Home fa-fw"></i>Store NPS: <?php echo $value['StoreName']; ?> </h4>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="text-align: center;">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" >
                                                Interaction
                                            </div>
                                            <div class="panel-body panel-small">
                                                <h3> <?php echo $value['InteractionScore']; ?> </h3>
                                                <p><i class="fa fa-trophy"></i> <?php echo $value['InteractionTier']; ?> </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"  style="text-align: center;">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Episode
                                            </div>
                                            <div class="panel-body panel-small">
                                                <h3> <?php echo $value['EpisodeScore']; ?> </h3>
                                                <p><i class="fa fa-trophy"></i> <?php echo $value['EpisodeTier']; ?> </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <h4 class="sub-header"><i class="fa fa-Home fa-fw"></i>Store NPS: </h4>
                        <div class='alert alert-info'>
                            <p><i class="fa fa-exclamation-circle"></i> No Records yet.</p>
                            <p><i class="fa fa-check-square"></i> You need at least 1 Advocate</p>
                        </div>

                    <?php } ?>
                </div>
            </section>

            <section id="userAllFeedack">
                <div class='col-lg-12'>
                    <div id="CustomerFeedback"></div>
                </div>
            </section>

        </div><!--end profile-content-->
    </div><!--end col-lg-8-->
</div><!--end row-->


<!-- Modal EditEmployeeModal-->
<div class="modal fade" id="EditEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal  EditEmployeeModal -->

<div class="modal fade" id="feedbackmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal  CreateStoreModal -->

<script type="text/javascript">
    $(function()
    {
        $("#toggleFeedack").click(function()
        {
            $(".Allfeedback").slideToggle();
            return false;
        });
    });
</script>

<script>
    $(document).ready(function() {
        //Refresh Modal Content
        $('a.feedback').click(function(ev) {
            ev.preventDefault();
            var target = $(this).attr('href');

            $("#feedbackmodal .modal-content").load(target, function() {
                $("#feedbackmodal").modal("show");
            });
        });
    });

    function sortNumbersIgnoreText(a, b, high) {
        var reg = /[+-]?((\d+(\.\d*)?)|\.\d+)([eE][+-]?[0-9]+)?/;
        a = a.match(reg);
        a = a !== null ? parseFloat(a[0]) : high;
        b = b.match(reg);
        b = b !== null ? parseFloat(b[0]) : high;
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    }

    function stripHTML(dirtyString) {
        var container = document.createElement('div');
        container.innerHTML = dirtyString;
        return container.textContent;
    }

    jQuery.extend(jQuery.fn.dataTableExt.oSort, {
        "sort-numbers-ignore-text-asc": function(a, b) {
            return sortNumbersIgnoreText(a, b, Number.POSITIVE_INFINITY);
        },
        "sort-numbers-ignore-text-desc": function(a, b) {
            return sortNumbersIgnoreText(a, b, Number.NEGATIVE_INFINITY) * -1;
        }
    });

    $(document).ready(function() {
        $('#weeklyint,#weeklyepi').DataTable({
            "paging": false,
            "ordering": false,
            "info": false,
            bFilter: false, bInfo: false
        });
    });
</script>


