<?php

//Custom messages on Employee needed Survey
function getClassEmployee($empInteractionNPS) {
    if ($empInteractionNPS >= 80) {
        $EmpData['message'] = "Excellent work! You're an advocate :)";
        $EmpData['class'] = "class='alert alert-success'";
    } elseif ($empInteractionNPS >= 75 && $empInteractionNPS <= 79) {
        $EmpData['message'] = "Keep it up! Almost an advocate :)";
        $EmpData['class'] = "class='alert alert-warning'";
    } elseif ($empInteractionNPS >= 68 && $empInteractionNPS <= 74) {
        $EmpData['message'] = "Great Job! You're now a passive :)";
        $EmpData['class'] = "class='alert alert-warning'";
    } elseif ($empInteractionNPS >= 58 && $empInteractionNPS <= 67) {
        $EmpData['message'] = "Currently a detractor :(";
        $EmpData['class'] = "class='alert alert-danger'";
    } else {
        $EmpData['message'] = "Currently a  detractor :(";
        $EmpData['class'] = "class='alert alert-danger'";
    }
    return $EmpData;
}

function getClassEmployeeTBC($empInteractionNPS) {
    if ($empInteractionNPS >= 38) {
        $EmpData['message'] = "Excellent work! You're an advocate :)";
        $EmpData['class'] = "class='alert alert-success'";
    } else {
        $EmpData['message'] = "Currently a  detractor :(";
        $EmpData['class'] = "class='alert alert-danger'";
    }
    return $EmpData;
}

//Custom messages on  Store needed Survey for Interaction
function getClassStore($storeInteractionNpsScore) {
    if ($storeInteractionNpsScore >= 80) {
        $data['message2'] = "Excellent work! Store is Tier 1 :)";
        $data['class2'] = "class='alert alert-success'";
    } elseif ($storeInteractionNpsScore >= 75 && $storeInteractionNpsScore <= 79) {
        $data['message2'] = "Great Job! Store is Tier 2";
        $data['class2'] = "class='alert alert-warning'";
    } elseif ($storeInteractionNpsScore >= 68 && $storeInteractionNpsScore <= 74) {
        $data['message2'] = "Keep it up! Store is Tier 3 :)";
        $data['class2'] = "class='alert alert-warning'";
    } elseif ($storeInteractionNpsScore >= 58 && $storeInteractionNpsScore <= 67) {
        $data['message2'] = "More advocates needed, Store is 4 :(";
        $data['class2'] = "class='alert alert-danger'";
    } else {
        $data['message2'] = "More advocates needed, Store is Tier 5 :(";
        $data['class2'] = "class='alert alert-danger'";
    }
    return $data;
}

//Custom messages on  Store needed Survey for Episode
function getEpisodeClassStore($storeEpisodeNpsScore) {
    if ($storeEpisodeNpsScore >= 50) {
        $data['message3'] = "Excellent work! Store is Tier 1 :)";
        $data['class3'] = "class='alert alert-success'";
    } elseif ($storeEpisodeNpsScore >= 40 && $storeEpisodeNpsScore <= 49) {
        $data['message3'] = "Great Job! Store is Tier 2";
        $data['class3'] = "class='alert alert-warning'";
    } elseif ($storeEpisodeNpsScore >= 30 && $storeEpisodeNpsScore <= 39) {
        $data['message3'] = "Keep it up! Store is Tier 3 :)";
        $data['class3'] = "class='alert alert-warning'";
    } elseif ($storeEpisodeNpsScore >= 0 && $storeEpisodeNpsScore <= 29) {
        $data['message3'] = "More advocates needed, Store is Tier 4 :(";
        $data['class3'] = "class='alert alert-danger'";
    } else {
        $data['message3'] = "Poor Performance, Store is Tier 5 :(";
        $data['class3'] = "class='alert alert-danger'";
    }
    return $data;
}

//Custom messages on  Store needed Survey for Interaction
function getClassStoreIntTBC($storeInteractionNpsScore) {
    if ($storeInteractionNpsScore >= 38) {
        $data['message2'] = "Excellent work! Store Qualified  :)";
        $data['class2'] = "class='alert alert-success'";
    } elseif ($storeInteractionNpsScore < 38) {
        $data['message2'] = "Poor Performance, Store Not Qualified :(";
        $data['class2'] = "class='alert alert-warning'";
    }
    return $data;
}

function getClassStoreMobileTBC($storeInteractionNpsScore) {
    if ($storeInteractionNpsScore >= 60) {
        $data['message2'] = "Excellent work! Store is Tier 1 :)";
        $data['class2'] = "class='alert alert-success'";
    } elseif ($storeInteractionNpsScore >= 50 && $storeInteractionNpsScore <= 59) {
        $data['message2'] = "Great Job! Store is Tier 2";
        $data['class2'] = "class='alert alert-warning'";
    } elseif ($storeInteractionNpsScore >= 46 && $storeInteractionNpsScore <= 49) {
        $data['message2'] = "Keep it up! Store is Tier 3 :)";
        $data['class2'] = "class='alert alert-warning'";
    } elseif ($storeInteractionNpsScore >= 43 && $storeInteractionNpsScore <= 45) {
        $data['message2'] = "More advocates needed, Store is Tier 4 :(";
        $data['class2'] = "class='alert alert-danger'";
    } elseif ($storeInteractionNpsScore >= 25 && $storeInteractionNpsScore <= 42) {
        $data['message2'] = "More advocates needed, Store is Tier 5 :(";
        $data['class2'] = "class='alert alert-danger'";
    } else {
        $data['message2'] = "More advocates needed, Store is Tier 6 :(";
        $data['class2'] = "class='alert alert-danger'";
    }
    return $data;
}

function getClassStoreFixedTBC($storeInteractionNpsScore) {
    if ($storeInteractionNpsScore >= 15) {
        $data['message2'] = "Excellent work! Store is Tier 1 :)";
        $data['class2'] = "class='alert alert-success'";
    } elseif ($storeInteractionNpsScore >= 0 && $storeInteractionNpsScore <= 14) {
        $data['message2'] = "Great Job! Store is Tier 2";
        $data['class2'] = "class='alert alert-warning'";
    } elseif ($storeInteractionNpsScore >= -13 && $storeInteractionNpsScore <= 1) {
        $data['message2'] = "Keep it up! Store is Tier 3 :)";
        $data['class2'] = "class='alert alert-warning'";
    } elseif ($storeInteractionNpsScore >= -35 && $storeInteractionNpsScore <= -12) {
        $data['message2'] = "More advocates needed, Store is Tier 4 :(";
        $data['class2'] = "class='alert alert-danger'";
    } else {
        $data['message2'] = "More advocates needed, Store is Tier 5 :(";
        $data['class2'] = "class='alert alert-danger'";
    }
    return $data;
}

function getClassStoreEpisodeTBC($storeInteractionNpsScore) {
    
      if ($storeInteractionNpsScore >= 71) {
        $data['message2'] = "Excellent work! Store is Tier 1 :)";
        $data['class2'] = "class='alert alert-success'";
      } elseif ($storeInteractionNpsScore >= 64 ) {
        $data['message2'] = "Great Job! Store is Tier 2";
        $data['class2'] = "class='alert alert-warning'";
      } elseif ($storeInteractionNpsScore >= 46 ) {
         $data['message2'] = "Keep it up! Store is Tier 3 :)";
        $data['class2'] = "class='alert alert-warning'";
      } elseif ($storeInteractionNpsScore >= 27 ) {
        $data['message2'] = "More advocates needed, Store is Tier 4 :(";
        $data['class2'] = "class='alert alert-danger'";
      } else {
        $data['message2'] = "More advocates needed, Store is Tier 5 :(";
        $data['class2'] = "class='alert alert-danger'";
      }
    return $data;
}

function getEpisodeClassStoreTLS($storeEpisodeNpsScore) {
    if ($storeEpisodeNpsScore >= 50) {
        $data['message3'] = "Excellent work! Store is Tier 1 :)";
        $data['class3'] = "class='alert alert-success'";
    } elseif ($storeEpisodeNpsScore >= 40 && $storeEpisodeNpsScore <= 49) {
        $data['message3'] = "Great Job! Store is Tier 2";
        $data['class3'] = "class='alert alert-warning'";
    } elseif ($storeEpisodeNpsScore >= 30 && $storeEpisodeNpsScore <= 39) {
        $data['message3'] = "Keep it up! Store is Tier 3 :)";
        $data['class3'] = "class='alert alert-warning'";
    } elseif ($storeEpisodeNpsScore >= 0 && $storeEpisodeNpsScore <= 29) {
        $data['message3'] = "More advocates needed, Store is Tier 4 :(";
        $data['class3'] = "class='alert alert-danger'";
    } else {
        $data['message3'] = "Poor Performance, Store is Tier 5 :(";
        $data['class3'] = "class='alert alert-danger'";
    }
    return $data;
}

//Custom messages on  Store needed Survey for Interaction
function getClassStoreIntTLS($storeInteractionNpsScore) {
    if ($storeInteractionNpsScore >= 80) {
        $data['message2'] = "Excellent work! Store Qualified  :)";
        $data['class2'] = "class='alert alert-success'";
    } elseif ($storeInteractionNpsScore >= 75 && $storeInteractionNpsScore <= 79) {
        $data['message3'] = "Great Job! Store is Tier 2";
        $data['class3'] = "class='alert alert-warning'";
    } elseif ($storeInteractionNpsScore >= 68 && $storeInteractionNpsScore <= 74) {
        $data['message3'] = "Keep it up! Store is Tier 3 :)";
        $data['class3'] = "class='alert alert-warning'";
    } elseif ($storeInteractionNpsScore >= 58 && $storeInteractionNpsScore <= 67) {
        $data['message3'] = "More advocates needed, Store is Tier 4 :(";
        $data['class3'] = "class='alert alert-danger'";
    } else {
        $data['message3'] = "Poor Performance, Store is Tier 5 :(";
        $data['class3'] = "class='alert alert-danger'";
    }
    return $data;
}

function getClassStoreMobileTLS($storeInteractionNpsScore) {
    if ($storeInteractionNpsScore >= 74) {
        $data['message2'] = "Excellent work! Store is Tier 1 :)";
        $data['class2'] = "class='alert alert-success'";
    } elseif ($storeInteractionNpsScore >= 70 && $storeInteractionNpsScore <= 73) {
        $data['message2'] = "Great Job! Store is Tier 2";
        $data['class2'] = "class='alert alert-warning'";
    } elseif ($storeInteractionNpsScore >= 66 && $storeInteractionNpsScore <= 69) {
        $data['message2'] = "Keep it up! Store is Tier 3 :)";
        $data['class2'] = "class='alert alert-warning'";
    } elseif ($storeInteractionNpsScore >= 61 && $storeInteractionNpsScore <= 65) {
        $data['message2'] = "More advocates needed, Store is Tier 4 :(";
        $data['class2'] = "class='alert alert-danger'";
    } elseif ($storeInteractionNpsScore >= 51 && $storeInteractionNpsScore <= 60) {
        $data['message2'] = "More advocates needed, Store is Tier 5 :(";
        $data['class2'] = "class='alert alert-danger'";
    } else {
        $data['message2'] = "More advocates needed, Store is Tier 6 :(";
        $data['class2'] = "class='alert alert-danger'";
    }
    return $data;
}

function getClassStoreFixedTLS($storeInteractionNpsScore) {
    if ($storeInteractionNpsScore >= 60) {
        $data['message2'] = "Excellent work! Store is Tier 1 :)";
        $data['class2'] = "class='alert alert-success'";
    } elseif ($storeInteractionNpsScore >= 53 && $storeInteractionNpsScore <= 69) {
        $data['message2'] = "Great Job! Store is Tier 2";
        $data['class2'] = "class='alert alert-warning'";
    } elseif ($storeInteractionNpsScore >= 47 && $storeInteractionNpsScore <= 52) {
        $data['message2'] = "Keep it up! Store is Tier 3 :)";
        $data['class2'] = "class='alert alert-warning'";
    } elseif ($storeInteractionNpsScore >= 41 && $storeInteractionNpsScore <= 46) {
        $data['message2'] = "More advocates needed, Store is Tier 4 :(";
        $data['class2'] = "class='alert alert-danger'";
    } elseif ($storeInteractionNpsScore >= 23 && $storeInteractionNpsScore <= 40) {
        $data['message2'] = "More advocates needed, Store is Tier 5 :(";
        $data['class2'] = "class='alert alert-danger'";
    } else {
        $data['message2'] = "More advocates needed, Store is Tier 6 :(";
        $data['class2'] = "class='alert alert-danger'";
    }
    return $data;
}

//Custom messages on  Employee needed Survey for Interaction
function getClassEmployeeIntTLS($empInteractionNpsScore) {
    if ($empInteractionNpsScore >= 80) {
        $EmpData['message'] = "Excellent work! You're an advocate :)";
        $EmpData['class'] = "class='alert alert-success'";
    } elseif ($empInteractionNpsScore >= 75 && $empInteractionNpsScore <= 79) {
        $EmpData['message'] = "Keep it up! Almost an advocate :)";
        $EmpData['class'] = "class='alert alert-warning'";
    } elseif ($empInteractionNpsScore >= 68 && $empInteractionNpsScore <= 74) {
        $EmpData['message'] = "Great Job! You're now a passive :)";
        $EmpData['class'] = "class='alert alert-warning'";
    } elseif ($empInteractionNpsScore >= 58 && $empInteractionNpsScore <= 67) {
        $EmpData['message'] = "Currently a detractor :(";
        $EmpData['class'] = "class='alert alert-danger'";
    } else {
        $EmpData['message'] = "Currently a  detractor :(";
        $EmpData['class'] = "class='alert alert-danger'";
    }

    return $EmpData;
}

function getClassEmployeeMobileTLS($empInteractionNpsScore) {
    if ($empInteractionNpsScore >= 74) {
        $EmpData['message2'] = "Excellent work! You're an advocate :)";
        $EmpData['class2'] = "class='alert alert-success'";
    } elseif ($empInteractionNpsScore >= 70 && $empInteractionNpsScore <= 73) {
        $EmpData['message2'] = "Keep it up! Almost an advocate :)";
        $EmpData['class2'] = "class='alert alert-warning'";
    } elseif ($empInteractionNpsScore >= 66 && $empInteractionNpsScore <= 69) {
        $EmpData['message2'] = "Great Job! You're now a passive :)";
        $EmpData['class2'] = "class='alert alert-warning'";
    } elseif ($empInteractionNpsScore >= 56 && $empInteractionNpsScore <= 65) {
        $EmpData['message2'] = "Currently a detractor :(";
        $EmpData['class2'] = "class='alert alert-danger'";
    } elseif ($empInteractionNpsScore >= 46 && $empInteractionNpsScore <= 55) {
        $EmpData['message2'] = "Currently a detractor :(";
        $EmpData['class2'] = "class='alert alert-danger'";
    } else {
        $EmpData['message2'] = "Currently a  detractor :(";
        $EmpData['class2'] = "class='alert alert-danger'";
    }
    return $EmpData;
}

function getClassEmployeeFixedTLS($empInteractionNpsScore) {
    if ($empInteractionNpsScore >= 60) {
        $EmpData['message3'] = "Excellent work! You're an advocate :)";
        $EmpData['class3'] = "class='alert alert-success'";
    } elseif ($empInteractionNpsScore >= 53 && $empInteractionNpsScore <= 59) {
        $EmpData['message3'] = "Keep it up! Almost an advocate :)";
        $EmpData['class3'] = "class='alert alert-warning'";
    } elseif ($empInteractionNpsScore >= 47 && $empInteractionNpsScore <= 52) {
        $EmpData['message3'] = "Great Job! You're now a passive :)";
        $EmpData['class3'] = "class='alert alert-warning'";
    } elseif ($empInteractionNpsScore >= 36 && $empInteractionNpsScore <= 46) {
        $EmpData['message3'] = "Currently a detractor :(";
        $EmpData['class3'] = "class='alert alert-danger'";
    } elseif ($empInteractionNpsScore >= 13 && $empInteractionNpsScore <= 35) {
        $EmpData['message3'] = "Currently a detractor :(";
        $EmpData['class3'] = "class='alert alert-danger'";
    } else {
        $EmpData['message3'] = "Currently a  detractor :(";
        $EmpData['class3'] = "class='alert alert-danger'";
    }
    return $EmpData;
}

function getEpisodeClassEmployeeTLS($empEpisodeNpsScore) {
    if ($empEpisodeNpsScore >= 50) {
        $EmpData['message4'] = "Excellent work! You're an advocate :)";
        $EmpData['class4'] = "class='alert alert-success'";
    } elseif ($empEpisodeNpsScore >= 40 && $empEpisodeNpsScore <= 49) {
        $EmpData['message4'] = "Keep it up! Almost an advocate :)";
        $EmpData['class4'] = "class='alert alert-warning'";
    } elseif ($empEpisodeNpsScore >= 30 && $empEpisodeNpsScore <= 39) {
        $EmpData['message4'] = "Great Job! You're now a passive :)";
        $EmpData['class4'] = "class='alert alert-warning'";
    } elseif ($empEpisodeNpsScore >= 0 && $empEpisodeNpsScore <= 29) {
        $EmpData['message4'] = "Currently a detractor :(";
        $EmpData['class4'] = "class='alert alert-danger'";
    } else {
        $EmpData['message4'] = "Currently a  detractor :(";
        $EmpData['class4'] = "class='alert alert-danger'";
    }
    return $EmpData;
}

//Custom messages on  Employee needed Survey for Interaction
function getClassEmployeeIntTBC($empInteractionNpsScore) {
    if ($empInteractionNpsScore >= 38) {
        $EmpData['message'] = "Excellent work! You're an advocate :)";
        $EmpData['class'] = "class='alert alert-success'";
    } else {
        $EmpData['message'] = "Currently a  detractor :(";
        $EmpData['class'] = "class='alert alert-danger'";
    }

    return $EmpData;
}

function getClassEmployeeMobileTBC($empInteractionNpsScore) {
    if ($empInteractionNpsScore >= 59) {
        $EmpData['message2'] = "Excellent work! You're an advocate :)";
        $EmpData['class2'] = "class='alert alert-success'";
    } elseif ($empInteractionNpsScore >= 52 && $empInteractionNpsScore <= 58) {
        $EmpData['message2'] = "Keep it up! Almost an advocate :)";
        $EmpData['class2'] = "class='alert alert-warning'";
    } elseif ($empInteractionNpsScore >= 45 && $empInteractionNpsScore <= 51) {
        $EmpData['message2'] = "Great Job! You're now a passive :)";
        $EmpData['class2'] = "class='alert alert-warning'";
    } elseif ($empInteractionNpsScore >= 35 && $empInteractionNpsScore <= 44) {
        $EmpData['message2'] = "Currently a detractor :(";
        $EmpData['class2'] = "class='alert alert-danger'";
    } else {
        $EmpData['message2'] = "Currently a  detractor :(";
        $EmpData['class2'] = "class='alert alert-danger'";
    }
    return $EmpData;
}

function getClassEmployeeFixedTBC($empInteractionNpsScore) {
    if ($empInteractionNpsScore >= 15) {
        $EmpData['message3'] = "Excellent work! You're an advocate :)";
        $EmpData['class3'] = "class='alert alert-success'";
    } elseif ($empInteractionNpsScore >= 0 && $empInteractionNpsScore <= 14) {
        $EmpData['message3'] = "Keep it up! Almost an advocate :)";
        $EmpData['class3'] = "class='alert alert-warning'";
    } elseif ($empInteractionNpsScore >= -13 && $empInteractionNpsScore <= 1) {
        $EmpData['message3'] = "Great Job! You're now a passive :)";
        $EmpData['class3'] = "class='alert alert-warning'";
    } elseif ($empInteractionNpsScore >= -35 && $empInteractionNpsScore <= -12) {
        $EmpData['message3'] = "Currently a detractor :(";
        $EmpData['class3'] = "class='alert alert-danger'";
    } else {
        $EmpData['message3'] = "Currently a  detractor :(";
        $EmpData['class3'] = "class='alert alert-danger'";
    }
    return $EmpData;
}

function getEpisodeClassEmployeeTBC($empEpisodeNpsScore) {
    if ($empEpisodeNpsScore >= 71) {
        $EmpData['message4'] = "Excellent work! You're an advocate :)";
        $EmpData['class4'] = "class='alert alert-success'";
    } elseif ($empEpisodeNpsScore >= 64 && $empEpisodeNpsScore <= 70) {
        $EmpData['message4'] = "Keep it up! Almost an advocate :)";
        $EmpData['class4'] = "class='alert alert-warning'";
    } elseif ($empEpisodeNpsScore >= 46 && $empEpisodeNpsScore <= 63) {
        $EmpData['message4'] = "Great Job! You're now a passive :)";
        $EmpData['class4'] = "class='alert alert-warning'";
    } elseif ($empEpisodeNpsScore >= 27 && $empEpisodeNpsScore <= 45) {
        $EmpData['message4'] = "Currently a detractor :(";
        $EmpData['class4'] = "class='alert alert-danger'";
    } else {
        $EmpData['message4'] = "Currently a  detractor :(";
        $EmpData['class4'] = "class='alert alert-danger'";
    }
    return $EmpData;
}


//Custom messages on  Employee needed Survey for Interaction
function getClassEmployeeIntOthers($empInteractionNpsScore) {
    if ($empInteractionNpsScore >= 80) {
        $EmpData['message'] = "Excellent work! You're an advocate :)";
        $EmpData['class'] = "class='alert alert-success'";
    } elseif ($empInteractionNpsScore >= 75 && $empInteractionNpsScore <= 79) {
        $EmpData['message'] = "Keep it up! Almost an advocate :)";
        $EmpData['class'] = "class='alert alert-warning'";
    } elseif ($empInteractionNpsScore >= 68 && $empInteractionNpsScore <= 74) {
        $EmpData['message'] = "Great Job! You're now a passive :)";
        $EmpData['class'] = "class='alert alert-warning'";
    } elseif ($empInteractionNpsScore >= 58 && $empInteractionNpsScore <= 67) {
        $EmpData['message'] = "Currently a detractor :(";
        $EmpData['class'] = "class='alert alert-danger'";
    } else {
        $EmpData['message'] = "Currently a  detractor :(";
        $EmpData['class'] = "class='alert alert-danger'";
    }

    return $EmpData;
}

function getClassEmployeeMobileOthers($empInteractionNpsScore) {
    if ($empInteractionNpsScore >= 70) {
        $EmpData['message2'] = "Excellent work! You're an advocate :)";
        $EmpData['class2'] = "class='alert alert-success'";
    } elseif ($empInteractionNpsScore >= 65 && $empInteractionNpsScore <= 69) {
        $EmpData['message2'] = "Keep it up! Almost an advocate :)";
        $EmpData['class2'] = "class='alert alert-warning'";
    } elseif ($empInteractionNpsScore >= 60 && $empInteractionNpsScore <= 64) {
        $EmpData['message2'] = "Great Job! You're now a passive :)";
        $EmpData['class2'] = "class='alert alert-warning'";
    } elseif ($empInteractionNpsScore >= 55 && $empInteractionNpsScore <= 59) {
        $EmpData['message2'] = "Currently a detractor :(";
        $EmpData['class2'] = "class='alert alert-danger'";
    } elseif ($empInteractionNpsScore >= 45 && $empInteractionNpsScore <= 54) {
        $EmpData['message2'] = "Currently a detractor :(";
        $EmpData['class2'] = "class='alert alert-danger'";
    } else {
        $EmpData['message2'] = "Currently a  detractor :(";
        $EmpData['class2'] = "class='alert alert-danger'";
    }
    return $EmpData;
}

function getClassEmployeeFixedOthers($empInteractionNpsScore) {
    if ($empInteractionNpsScore >= 60) {
        $EmpData['message3'] = "Excellent work! You're an advocate :)";
        $EmpData['class3'] = "class='alert alert-success'";
    } elseif ($empInteractionNpsScore >= 53 && $empInteractionNpsScore <= 59) {
        $EmpData['message3'] = "Keep it up! Almost an advocate :)";
        $EmpData['class3'] = "class='alert alert-warning'";
    } elseif ($empInteractionNpsScore >= 47 && $empInteractionNpsScore <= 52) {
        $EmpData['message3'] = "Great Job! You're now a passive :)";
        $EmpData['class3'] = "class='alert alert-warning'";
    } elseif ($empInteractionNpsScore >= 36 && $empInteractionNpsScore <= 46) {
        $EmpData['message3'] = "Currently a detractor :(";
        $EmpData['class3'] = "class='alert alert-danger'";
    } elseif ($empInteractionNpsScore >= 13 && $empInteractionNpsScore <= 35) {
        $EmpData['message3'] = "Currently a detractor :(";
        $EmpData['class3'] = "class='alert alert-danger'";
    } else {
        $EmpData['message3'] = "Currently a  detractor :(";
        $EmpData['class3'] = "class='alert alert-danger'";
    }
    return $EmpData;
}

function getEpisodeClassEmployeeOthers($empEpisodeNpsScore) {
    if ($empEpisodeNpsScore >= 50) {
        $EmpData['message4'] = "Excellent work! You're an advocate :)";
        $EmpData['class4'] = "class='alert alert-success'";
    } elseif ($empEpisodeNpsScore >= 40 && $empEpisodeNpsScore <= 49) {
        $EmpData['message4'] = "Keep it up! Almost an advocate :)";
        $EmpData['class4'] = "class='alert alert-warning'";
    } elseif ($empEpisodeNpsScore >= 30 && $empEpisodeNpsScore <= 39) {
        $EmpData['message4'] = "Great Job! You're now a passive :)";
        $EmpData['class4'] = "class='alert alert-warning'";
    } elseif ($empEpisodeNpsScore >= 0 && $empEpisodeNpsScore <= 29) {
        $EmpData['message4'] = "Currently a detractor :(";
        $EmpData['class4'] = "class='alert alert-danger'";
    } else {
        $EmpData['message4'] = "Currently a  detractor :(";
        $EmpData['class4'] = "class='alert alert-danger'";
    }
    return $EmpData;
}