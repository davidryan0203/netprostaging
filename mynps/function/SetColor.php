<?php

//Set Color for NPS Scores
function getSetColor($valueRange, $type) {
//  echo $valueRange;
    $valueRange = number_format($valueRange, npsDecimal());
    if ($type == 'interaction') {

        if ($valueRange >= 75) {
            $Style = "style='background: #00e600;'";
        } elseif ($valueRange >= 65 && $valueRange <= 74) {
            $Style = "style='background: #fec620;'";
        } elseif ($valueRange >= 55 && $valueRange <= 64) {
            $Style = "style='background: #ffa500;'";
        } elseif ($valueRange >= 45 && $valueRange <= 54) {
            $Style = "style='background: #ff0000;'";
        } else {
            $Style = "style='background: #ff0000;'";
        }
    } elseif ($type == 'episode') {
        if ($valueRange >= 50) {
            $Style = "style='background: #00e600;'";
        } elseif ($valueRange >= 40 && $valueRange <= 49) {
            $Style = "style='background: #fec620;'";
        } elseif ($valueRange >= 30 && $valueRange <= 39) {
            $Style = "style='background: #ffa500;'";
        } elseif ($valueRange >= 0 && $valueRange <= 29) {
            $Style = "style='background: #ff0000;'";
        } else {
            $Style = "style='background: #ff0000;'";
        }
    } else {

    }
    return $Style;
}
