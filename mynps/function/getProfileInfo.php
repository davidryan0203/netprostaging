<?php

include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/dashboard/function/getChamp.php');

function getEmployeeData($empID, $from, $to) {
    global $connection;

    if (!is_null($empID)) {
        $sqlEmp = "AND c.f_EmpID = {$empID} ";
    } else {
        $sqlEmp = " ";
    }

    if (!is_null($to) && !is_null($from)) {
        $to = date("Y-m-d", strtotime($to));
        $from = date("Y-m-d", strtotime($from));


        $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
    } else {
        $sqlWhen = " AND MONTH(a.f_DateTimeResponseMelbourne) = MONTH(CURDATE())
                     AND YEAR(a.f_DateTimeResponseMelbourne) = YEAR(CURDATE())";
    }


    $sqlEmpNps = "SELECT
c.f_EmpID as EmpID,
case
    WHEN d.f_EmpFirstName IS NULL THEN a.f_ActivationExtra5
    ELSE concat(d.f_EmpFirstName, ' ', d.f_EmpLastName)
END AS EmpName,
d.f_EmpFirstName as Firstname,
d.f_EmpLastName as Lastname,
e.f_StoreListName as Store,
f.f_EmpRoleName as Role,
SUM(CASE
        WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '9' and '10') AND (a.f_ActivationExtra5 != 'P000000') THEN 1
        ELSE 0
END) AS EmpInteractionTotalAdvocate,
 SUM(CASE
        WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '7' and '8') AND (a.f_ActivationExtra5 != 'P000000') THEN 1
        ELSE 0
END) AS EmpInteractionTotalPassive,
 SUM(CASE
        WHEN (a.f_LikelihoodToRecommendRetail BETWEEN '0' and '6') AND (a.f_ActivationExtra5 != 'P000000')THEN 1
        ELSE 0
END) AS EmpInteractionTotalDetractor,
SUM(CASE
        WHEN a.f_RecommendRate BETWEEN '9' and '10' THEN 1
        ELSE 0
END) AS EmpEpisodeTotalAdvocate,
 SUM(CASE
        WHEN a.f_RecommendRate BETWEEN '7' and '8' THEN 1
        ELSE 0
END) AS EmpEpisodeTotalPassive,
 SUM(CASE
        WHEN a.f_RecommendRate BETWEEN '0' and '6' THEN 1
        ELSE 0
END) AS EmpEpisodeTotalDetractor,
SUM(CASE WHEN a.f_LikelihoodToRecommendRetail between 0 and 10 and  a.f_ActivationExtra5 != 'P000000'  THEN 1  else 0 end) as EmpTotalInteractionSurvey,
count(a.f_RecommendRate) as EmpTotalEpisodeSurvey
from t_surveysd a
LEFT JOIN t_empnumber c on a.f_ActivationExtra5 = c.f_EmpPNumber and a.f_StoreID = c.f_StoreID
LEFT JOIN t_emplist d on c.f_EmpID = d.f_EmpID
LEFT JOIN t_storelist e on a.f_StoreID = e.f_StoreID
LEFT JOIN t_emprole f on d.f_EmpRoleID = f.f_EmpRoleID
WHERE 1 = 1 {$sqlEmp} {$sqlWhen}
GROUP BY case
WHEN d.f_EmpFirstName IS NULL THEN a.f_ActivationExtra5
ELSE concat(d.f_EmpFirstName, ' ', d.f_EmpLastName)
END, e.f_StoreID";

    //  echo $sqlEmpNps;
//echo "($from - $to)";

    $resultEmpNps = mysql_query($sqlEmpNps, $connection);
    $i = 0;
    while ($rowEmpNps = mysql_fetch_assoc($resultEmpNps)) {
        $EmpNpsData[$i] = $rowEmpNps;

        //Percentage Computations
        $EmpInteractionAdvocatePercentage = $rowEmpNps['EmpInteractionTotalAdvocate'] / $rowEmpNps['EmpTotalInteractionSurvey'] * 100;
        $EmpInteractionPassivePercentage = $rowEmpNps['EmpInteractionTotalPassive'] / $rowEmpNps['EmpTotalInteractionSurvey'] * 100;
        $EmpInteractionDetractorPercentage = $rowEmpNps['EmpInteractionTotalDetractor'] / $rowEmpNps['EmpTotalInteractionSurvey'] * 100;

        $EmpEpisodeAdvocatePercentage = $rowEmpNps['EmpEpisodeTotalAdvocate'] / $rowEmpNps['EmpTotalEpisodeSurvey'] * 100;
        $EmpEpisodePassivePercentage = $rowEmpNps['EmpEpisodeTotalPassive'] / $rowEmpNps['EmpTotalEpisodeSurvey'] * 100;
        $EmpEpisodeDetractorPercentage = $rowEmpNps['EmpEpisodeTotalDetractor'] / $rowEmpNps['EmpTotalEpisodeSurvey'] * 100;

        //Survey Percentage
        $EmpNpsData[$i]['EmpInteractionAdvocate'] = $EmpInteractionAdvocatePercentage;
        $EmpNpsData[$i]['EmpInteractionPassive'] = $EmpInteractionPassivePercentage;
        $EmpNpsData[$i]['EmpInteractionDetractor'] = $EmpInteractionDetractorPercentage;

        $EmpNpsData[$i]['EmpEpisodeAdvocate'] = $EmpEpisodeAdvocatePercentage;
        $EmpNpsData[$i]['EmpEpisodePassive'] = $EmpEpisodePassivePercentage;
        $EmpNpsData[$i]['EmpEpisodeDetractor'] = $EmpEpisodeDetractorPercentage;

        //NPS Scores
        $EmpNpsData[$i]['EmpInteractionScore'] = $EmpInteractionAdvocatePercentage - $EmpInteractionDetractorPercentage;
        $EmpNpsData[$i]['EmpEpisodeScore'] = $EmpEpisodeAdvocatePercentage - $EmpEpisodeDetractorPercentage;
        $i++;
    }

    return $EmpNpsData;
}

function getEmployeeImpactRank($report, $type, $shared, $from, $to, $storeType) {
    global $connection;

    // $type = 'AllEmployee';
    switch ($storeType) {
        case 1:
            $dataImpact = getChampTLS($report, $type, $shared, $from, $to, NULL, NULL, NULL);
            break;
        case 2:
            $dataImpact = getChampTBC($report, $type, $shared, $from, $to, 'RemoveNull', '2', NULL, 'mtd');
            break;
    }



    return $dataImpact;
}

function getWeekly($type, $empID) {
    $weeklyto = date("Y-m-d");
    // $weeklyfrom = date("Y-m-d", strtotime('4 weeks ago'));
    $weeklyfrom = date("Y-m-d", strtotime('-27 days'));
    if ($type == 'Interaction') {
        $result = getChamp('Interaction', 'AllEmployees', 'no', $weeklyfrom, $weeklyto, 'IncludeALL', '1', 'No', $empID);
    } elseif ($type == 'Episode') {
        $result = getChamp('Episode', 'AllEmployees', 'no', $weeklyfrom, $weeklyto, 'RemoveNull', '1', 'No', $empID);
    }


    return $result;
}

function getWeeklyNew($type, $empID, $storeType, $from, $to) {


    switch ($storeType) {
        case 1:
            $weeklyto = date("Y-m-d");
            //$weeklyfrom = date("Y-m-d", strtotime('4 weeks ago'));
            $weeklyfrom = date("Y-m-d", strtotime('-27 days'));
            if ($type == 'Interaction') {
                $result = getChampTLS('Interaction', 'AllEmployees', 'no', $weeklyfrom, $weeklyto, 'IncludeALL', '1', 'No', $empID);
            } elseif ($type == 'Mobility') {
                $result = getChampTLS('Mobile', 'AllEmployees', 'no', $weeklyfrom, $weeklyto, 'RemoveNull', '1', 'No', $empID);
            } elseif ($type == 'Fixed') {
                $result = getChampTLS('Fixed', 'AllEmployees', 'no', $weeklyfrom, $weeklyto, 'RemoveNull', '1', 'No', $empID);
            } elseif ($type == 'Episode') {
                $result = getChampTLS('Episode', 'AllEmployees', 'no', $weeklyfrom, $weeklyto, 'RemoveNull', '1', 'No', $empID);
            }
            break;
        case 2:
            $weeklyto = date("Y-m-d");
            // $weeklyfrom = date("Y-m-d", strtotime('4 weeks ago'));
            $weeklyfrom = date("Y-m-d", strtotime('-27 days'));
            if ($type == 'Interaction') {
                $result = getChampTBC('Interaction', 'AllEmployees', 'no', $weeklyfrom, $weeklyto, 'IncludeALL', '2', 'No', NULL, NULL, NULL, $empID);
            } elseif ($type == 'Mobile') {
                $result = getChampTBC('Mobile', 'AllEmployees', 'no', $weeklyfrom, $weeklyto, 'RemoveNull', '2', 'No', NULL, NULL, NULL, $empID);
            } elseif ($type == 'Fixed') {
                $result = getChampTBC('Fixed', 'AllEmployees', 'no', $weeklyfrom, $weeklyto, 'RemoveNull', '2', 'No', NULL, NULL, NULL, $empID);
            }
            break;
        case 3:
            $weeklyto = date("Y-m-d");
            //$weeklyfrom = date("Y-m-d", strtotime('4 weeks ago'));
            $weeklyfrom = date("Y-m-d", strtotime('-27 days'));
           if ($type == 'Interaction') {
                $result = getChampOthers('Interaction', 'AllEmployees', 'no', $weeklyfrom, $weeklyto, 'IncludeALL', '3', $sharedType, $empID);
            } elseif ($type == 'Mobile') {
                $result = getChampOthers('Mobile', 'AllEmployees', 'no', $weeklyfrom, $weeklyto, 'RemoveNull', '3', $sharedType, $empID);
            } elseif ($type == 'Fixed') {
                $result = getChampOthers('Fixed', 'AllEmployees', 'no', $weeklyfrom, $weeklyto, 'RemoveNull', '3', $sharedType, $empID);
            } elseif ($type == 'Episode') {
                $result = getChampOthers('Episode', 'AllEmployees', 'no', $weeklyfrom, $weeklyto, 'RemoveNull', '3', $sharedType, $empID);
            }
            break;
    }

    return $result;
}

function getMyNps($type, $empID, $storeType, $from, $to, $sharedType = 'No') {


    switch ($storeType) {
        case 1:
            if ($type == 'Interaction') {
                $result = getChampTLS('Interaction', 'AllEmployees', 'no', $from, $to, 'IncludeALL', '1', $sharedType, $empID);
            } elseif ($type == 'Mobile') {
                $result = getChampTLS('Mobile', 'AllEmployees', 'no', $from, $to, 'RemoveNull', '1', $sharedType, $empID);
            } elseif ($type == 'Fixed') {
                $result = getChampTLS('Fixed', 'AllEmployees', 'no', $from, $to, 'RemoveNull', '1', $sharedType, $empID);
            } elseif ($type == 'Episode') {
                $result = getChampTLS('Episode', 'AllEmployees', 'no', $from, $to, 'RemoveNull', '1', $sharedType, $empID);
            }
            break;
        case 2:
            if ($type == 'Interaction') {
                $result = getChampTBC('Interaction', 'AllEmployees', 'no', $from, $to, 'IncludeALL', '2', $sharedType, 'mtd', NULL, NULL, $empID);
            } elseif ($type == 'Mobile') {
                $result = getChampTBC('Mobile', 'AllEmployees', 'no', $from, $to, 'RemoveNull', '2', $sharedType, 'mtd', NULL, NULL, $empID);
            } elseif ($type == 'Fixed') {
                $result = getChampTBC('Fixed', 'AllEmployees', 'no', $from, $to, 'RemoveNull', '2', $sharedType, 'mtd', NULL, NULL, $empID);
            } elseif ($type == 'Episode') {
                $result = getChampTBC('Episode', 'AllEmployees', 'no', $from, $to, 'RemoveNull', '2', $sharedType, 'mtd', NULL, NULL, $empID);
            }
            break;
        case 3:
            if ($type == 'Interaction') {
                $result = getChampOthers('Interaction', 'AllEmployees', 'no', $from, $to, 'IncludeALL', '3', $sharedType, $empID);
            } elseif ($type == 'Mobile') {
                $result = getChampOthers('Mobile', 'AllEmployees', 'no', $from, $to, 'RemoveNull', '3', $sharedType, $empID);
            } elseif ($type == 'Fixed') {
                $result = getChampOthers('Fixed', 'AllEmployees', 'no', $from, $to, 'RemoveNull', '3', $sharedType, $empID);
            } elseif ($type == 'Episode') {
                $result = getChampOthers('Episode', 'AllEmployees', 'no', $from, $to, 'RemoveNull', '3', $sharedType, $empID);
            }
            break;
    }

    return $result;
}

//echo "<pre>";
//print_r($EmpNpsData);

