<?php
session_start();
error_reporting(0);
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');

$empID = $_SESSION['EmpProfileID'];
$role = $_SESSION['UserRoleID'];

require($_SERVER['DOCUMENT_ROOT'] . '/nps/function/NPSFunction.php');
require($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');
$SelectStoreTypes = SelectStoreTypes();

$sqlCounter = "
select b.f_StoreOwnerUserID from t_empnumber a
left join t_storelist b on  a.f_StoreID = b.f_StoreID
 where a.f_EmpID  = '{$empID}' limit 1";
$resultCounter = mysql_query($sqlCounter, $connection);
while ($row = mysql_fetch_assoc($resultCounter)) {
    $storeOwnerID = $row['f_StoreOwnerUserID'];
}

if ($storeOwnerID == 83) { //83
    $first_day_this_month = date('d/m/Y', strtotime("-3 months")); // hard-coded '01' for first day
    $last_day_this_month = date('d/m/Y');
} else {
    // $first_day_this_month = date('d/m/Y', strtotime("-4 week"));
//    $first_day_this_month = date('d/m/Y', strtotime("-27 days"));
    $last_day_this_month = date('d/m/Y');
    $first_day_this_month = date("01/m/Y", strtotime('-2 Months'));
}
?>


<script type="text/javascript">
    $(document).ready(function() {

<?php if ($SelectStoreTypes['StoreTypes'] == 'Only TBC') { ?>
            $("#StoreType").trigger('click');
    <?php
}
if ($storeOwnerID == 83) { //83
    ?>
            $("#ThreeMonthsRolling").addClass("active");
<?php } else {
    ?>
            $("#3MMARolling").addClass("active");
<?php } ?>

        $('#profiler', function() {
            var addonParams = setLoad();
            var urlToLoad = addonParams;
            var divToHide = "#LogLoader";
            var divToShow = "#profiler";
            reload(urlToLoad, divToHide, divToShow);
        });


        $("#from").datepicker({
            showOn: "button",
            buttonImage: "https://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
            buttonImageOnly: true, dateFormat: 'dd/mm/yy',
            numberOfMonths: 2,
            onSelect: function(selected) {
                $("#to").datepicker("option", "minDate", selected)
            }
        });

        $("#to").datepicker({
            showOn: "button",
            buttonImage: "https://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
            buttonImageOnly: true, dateFormat: 'dd/mm/yy',
            numberOfMonths: 2,
            onSelect: function(selected) {
                $("#from").datepicker("option", "maxDate", selected)
            }
        });

        $('#refresh').click(function() {
            var addonParams = setLoad();
            var urlToLoad = addonParams;
            var divToHide = "#LogLoader";
            var divToShow = "#profiler";

            $(divToShow).fadeIn('fast');
            $(divToHide).fadeOut('slow');
            reload(urlToLoad, divToHide, divToShow);
        });


        $('#StoreType').change(function() {
            var addonParams = setLoad();
            var urlToLoad = addonParams;
            var divToHide = "#LogLoader";
            var divToShow = "#profiler";
            reload(urlToLoad, divToHide, divToShow);
        });

        $('#FourWeekRolling').click(function() {
            $("#to").datepicker('setDate', new Date());
            var currentTo = $("#to").datepicker('getDate');
            // console.log(new Date());
            // console.log(currentTo+ ' '+currentTo.getDate()-28);
            currentTo.setDate(currentTo.getDate() - 27);
            $("#from").datepicker('setDate', currentTo);

        });

        $('#ThreeMonthsRolling').click(function() {
            var currentTo = $("#to").datepicker('getDate');
            if ($('#StoreType').prop('checked')) {
                $("#to").datepicker('setDate', new Date(), 0);

                currentTo.setMonth(currentTo.getMonth() - 2, 0);
            } else {
                currentTo.setMonth(currentTo.getMonth() - 3);
            }

            $("#from").datepicker('setDate', currentTo);

        });

        $('#3MMARolling').click(function() {
            var currentTo = $("#to").datepicker('getDate');
            if ($('#StoreType').prop('checked')) {
                $("#to").datepicker('setDate', new Date(), 0);

                currentTo.setMonth(currentTo.getMonth() - 2, 0);
                currentTo.setDate(1);
            } else {
                currentTo.setMonth(currentTo.getMonth() - 2);
                currentTo.setDate(1);
            }
            console.log(currentTo);

            $("#from").datepicker('setDate', currentTo);

        });

        $('#SixMonthsRolling').click(function() {
            var currentTo = $("#to").datepicker('getDate');
            if ($('#StoreType').prop('checked')) {

                $("#to").datepicker('setDate', new Date(), 0);

                currentTo.setMonth(currentTo.getMonth() - 5, 0);
            } else {
                currentTo.setMonth(currentTo.getMonth() - 6);
            }

            $("#from").datepicker('setDate', currentTo);

        });

        $('#MtdRolling').click(function() {
            var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            $("#from").datepicker('setDate', firstDay);
            $("#to").datepicker('setDate', lastDay);
        });

        $('#DefaultRolling').click(function() {
            var currentTo = $("#to").datepicker('getDate');
            currentTo.setDate(currentTo.getDate() - 27);
            $("#from").datepicker('setDate', currentTo);
            $("#to").datepicker('setDate', new Date());
        });

        $('#HTDRolling').click(function() {
            var date = new Date();
            //var output = (moment(date).quarter() <= 2 ? "H1" : "H2") + ' ' + moment(date).format('YYYY');
            // consol.log(output);
            var curMonth = date.getMonth() + 1;
            var curYear = date.getFullYear();
            console.log(curMonth);
            if (curMonth >= 1 && curMonth <= 6) {
                var from = "01/01/" + curYear;
                var to = "06/30/" + curYear;
                $("#from").datepicker('setDate', new Date(from));
                $("#to").datepicker('setDate', new Date(to));
            } else
            {
                var from = "07/01/" + curYear;
                var to = "12/31/" + curYear;
                $("#from").datepicker('setDate', new Date(from));
                $("#to").datepicker('setDate', new Date(to));
            }

        });

        $('#FTDRolling').click(function() {

            var d = new Date();
            var day = d.getDay();
            $("#to").datepicker('setDate', new Date(d.setDate(d.getDate() - day + (day == 0 ? -6 : 1))));
            var currentTo = $("#to").datepicker('getDate');
            // console.log(d.getDate() - day + (day == 0 ? -6 : 1));

            // console.log(currentTo+ ' '+currentTo.getDate()-28);
            currentTo.setDate(currentTo.getDate() - 13);
            $("#from").datepicker('setDate', currentTo);

        });

        $(".btn-group> .btn").click(function() {
            $(this).addClass("active").siblings().removeClass("active");
        });

    });

    function setLoad() {
        var from = $("#from").val().split("/").reverse().join("-");
        var to = $("#to").val().split("/").reverse().join("-");
        var empID = $("#empID").val();
        var role = $("#role").val();
        var toURL = "";

        if ($('#StoreType').prop('checked')) {
            toURL = "mynps/profileincluderTBC.php?storeType=2&";
        } else {

            if ($('#StoreType').data('store-type') == 'others') {
                toURL = "mynps/profileincluderOthers.php?storeType=3&";
            } else {
                toURL = "mynps/profileincluderTLS.php?storeType=1&";
            }

        }

        var toLoad = toURL + 'to=' + to + '&from=' + from + '&empID=' + empID + '&role=' + role;
        //console.log(toLoad);
        return toLoad;
    }

//    function reload(urlToLoad, divToHide, divToShow) {
//        var urldata = urlToLoad;
//        var page = divToShow;
//        $.get(urldata).success(
//                function (response, status, jqXhr) {
//                    $(divToHide).hide();
//                    $(page).fadeIn('slow');
//                    $(page).empty().append(response);
//                }).error(function (response, status, jqXhr) {
//            $(divToHide).show();
//            reload(urldata, divToHide, divToShow);
//        }).complete(function (response, status, jqXhr) {
//        });
//    }

    function reload(paramurl, loader, content) {
        $(content).html('');
        $(loader).show();
        $.ajax({
            url: paramurl,
            cache: false,
            success: function(html) {
                $(content).append(html);
            },
            complete: function() {
                $(loader).hide();
                var setString = "";
                setString = $('.btn-group').find('.active').data('text');
                console.log(setString);
                $("rolling").html(setString);
            }
        });
    }

    function openModalEdit(el) {
        var addonParams = setLoad();
        var from = $("#from").val().split("/").reverse().join("-");
        var to = $("#to").val().split("/").reverse().join("-");
        var empID = $("#empID").val();
        var target = $(el).attr('href');
        $("#EditEmployeeModal .modal-content").load(target, function() {
            $("#EditEmployeeModal").modal("show");
        });
        $('#EditEmployeeModal').on('hidden.bs.modal', function(e) {
            $('#profiler').load(addonParams);
        });
    }

    function LoadProfilePic(el) {
        var data = $('#ProfilePicture').attr('togolink');
        $('.profileWell').hide();
        $('.profileWellEditor').show();
        $('#profileEditor').load('imageEditor2/examples/crop-avatar/indexv2.php');
    }
</script>


<input type="hidden" name="empID" id="empID" value="<?php echo $empID; ?>"/>
<input type="hidden" name="role" id="role" value="<?php echo $role; ?>"/>

<div class="profileWellEditor" style="display:none">
    <div class="row">
        <h4 class="page-header"><i class="fa fa-shopping-cart fa-fw"></i>NPS Profile</h4>
    </div>
    <div id="profileEditor"></div>
</div>

<div class="profileWell">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-header"><i class="fa fa-pagelines fa-fw"></i>NPS Profile</h4>
            <div class="filters" id="npsProfileFilters">
                <?php
                if ($_SESSION['HasStoreType'] == 'Only Others') {
                    ?>

                    <input type="checkbox" style="display:none" <?php echo $attritubes; ?> class="StoreType" name="StoreType" id="StoreType" data-store-type="others" />
                    <?php
                } else {
                    if (in_array($role, $withShared)) {
                        ?> <input type="checkbox" id="ShareType" name="ShareType" class="ShareType" data-toggle="toggle" data-style="ios" data-onstyle="success"
                               data-offstyle="info" data-on="Shared" data-off="Mine" <?php echo $attritubes; ?>/>
                           <?php } else { ?>
                        <input type="checkbox" style="display:none" <?php echo $attritubes; ?> class="ShareType" name="ShareType" id="ShareType" />
                        <?php
                    }
                    $storeTypes = checkStoreTypes();
                    if ($storeTypes) {
                        ?>
                        <input type="checkbox" id="StoreType" name="StoreType" class="StoreType" data-toggle="toggle" data-style="ios"  data-onstyle="info"
                               data-offstyle="success" data-on="TBC" data-off="TLS" />
                           <?php } else { ?>
                        <input type="checkbox" style="display:none"  id="StoreType" name="StoreType" class="StoreType" />
                        <?php
                    }
                }
                ?>

                <input type="text" id="from" name="from" class="form-control" value="<?php echo $first_day_this_month; ?>" readonly="true"/>
                <input type="text" id="to" name="to"  class="form-control" value="<?php echo $last_day_this_month; ?>" readonly="true"/>
                <div class="btn-group group1" id="weekly-group" role="group" aria-label="...">
                    <?php if (!$storeTypes || $_SESSION['HasStoreType'] == 'Only Others') { ?>
                        <button type="button" id="3MMARolling" data-text="3 Month Moving Average"  class="btn btn-xs btn-danger">3 MMA</button>
                    <?php } else { ?>
                        <button type="button" id="ThreeMonthsRolling" data-text="3 Months"  class="btn btn-xs btn-info">3 Months</button>
                        <button type="button" id="SixMonthsRolling" data-text="6 Months"  class="btn btn-xs btn-success">6 Months</button>
                    <?php } ?>
                    <!--<button type="button" id="DefaultRolling" data-text="4 Weeks" class="btn btn-xs btn-primary ">Default</button>-->
                    <button type="button" id="MtdRolling"  data-text="Month to Date" class="btn btn-xs btn-warning">MTD</button>
                    <button type="button" id="FourWeekRolling" data-text="4 Weeks"  class="btn btn-xs btn-primary">4 Weeks</button>
                    <button type="button" id="HTDRolling" data-text="HTD"  class="btn btn-xs btn-success">HTD</button>
                    <button type="button" id="FTDRolling" data-text="FTD"  class="btn btn-xs btn-default">FTD</button>

                </div>
                <button type="submit" class="btn btn-warning btn-circle" id="refresh"> <i class="fa fa-refresh"></i> </button>
            </div>
        </div>
    </div>

    <p class="latest-record">
        <?php
        echo $latestRec['latest_record'];
        ?>
    </p>

    <div class="panel-body">
        <div class="mynpscontainer" id="mynpscontainer">
            <div class="loader" id="LogLoader"></div>
            <div id="profiler">

            </div>
        </div>
    </div>


    <!--    <div id="profiler">
            <div class="loader" id="LogLoader"></div>
        </div>   -->
</div>