<?php
error_reporting(E_ALL);
include($_SERVER['DOCUMENT_ROOT'] . '/mynps/function/InteractionMessage.php');
include($_SERVER['DOCUMENT_ROOT'] . '/mynps/function/SetColor.php');
include($_SERVER['DOCUMENT_ROOT'] . '/mynps/function/getProfileInfo.php');
include($_SERVER['DOCUMENT_ROOT'] . '/nps/function/NPSFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/EmployeeFunction.php');

$profileImg = $_SESSION['ProfileImg'];


$profileID = $_SESSION['UserProfileID'];
$to = $_GET['to'];
$from = $_GET['from'];
$role = $_GET['role'];
$empID = $_SESSION['EmpProfileID'];
$storeType = $_GET['storeType'];
$rangeType = 'mtd';
//Get Employee Store Score Data
$StoreNps = array();
$storeNpsData = getStoreNpsTBC("no", $profileID, $empID, $to, $from, $role, $storeType);
foreach ($storeNpsData['Merged'] as $storeData) {
    $StoreNps[] = array(
        'StoreName' => $storeData['StoreName'],
        'InteractionScore' => number_format($storeData['InteractionNPS'], npsDecimal()),
        'InteractionTier' => $storeData['InteractionTier'],
        'FixedScore' => number_format($storeData['FixedNPS'], npsDecimal()),
        'FixedTier' => $storeData['FixedTier'],
        'MobileScore' => number_format($storeData['MobileNPS'], npsDecimal()),
        'MobileTier' => $storeData['MobileTier'],
        'EpisodeScore' => number_format($storeData['EpisodeNPS'], npsDecimal()),
        'EpisodeTier' => $storeData['EpisodeTier'],
        'Advocate' => $storeData['Advocates'],
        'Passive' => $storeData['Pasives'],
        'Detractor' => $storeData['Detractors'],
        'TotalSurvey' => $storeData['TotalSurvey']
    );
}

$empProf = SelectEmployee($empID);
$EmpInteraction = array();
$empData = getEmployeeData($empID, $from, $to);

$Fixed = getMyNps('Fixed', $empID, $storeType, $from, $to);
$Interaction = getMyNps('Interaction', $empID, $storeType, $from, $to);
$Mobile = getMyNps('Mobile', $empID, $storeType, $from, $to);
$Episode = getMyNps('Episode', $empID, $storeType, $from, $to);

$FixedOverall = getMyNps('Fixed', $empID, $storeType, $from, $to, 'Yes');
$InteractionOverall = getMyNps('Interaction', $empID, $storeType, $from, $to, 'Yes');
$MobileOverall = getMyNps('Mobile', $empID, $storeType, $from, $to, 'Yes');
$EpisodeOverall = getMyNps('Episode', $empID, $storeType, $from, $to, 'Yes');

foreach ($empData as $value) {
//Employee Profile
    $empName = $value['EmpName'];
    $empFname = $value['Firstname'];
    $empLname = $value['Lastname'];
    $empStore = $value['Store'];
    $empRole = $value['Role'];
}

$EmpNpsDetails[] = array(
    'StoreName' => $Interaction[0][0]['Data']['Store'],
    'InteractionAdvocate' => $Interaction[0][0]['Data']['EmpAdvocate'],
    'InteractionPassive' => $Interaction[0][0]['Data']['EmpPassive'],
    'InteractionDetractor' => $Interaction[0][0]['Data']['EmpDetractor'],
    'InteractionTotalSurvey' => $Interaction[0][0]['Data']['EmpTotalSurvey'],
    'InteractionNpsScore' => number_format($Interaction[0][0]['Data']['EmpInteraction'], v),
    'MobileAdvocate' => $Mobile[0][0]['Data']['EmpAdvocate'],
    'MobilePassive' => $Mobile[0][0]['Data']['EmpPassive'],
    'MobileDetractor' => $Mobile[0][0]['Data']['EmpDetractor'],
    'MobileTotalSurvey' => $Mobile[0][0]['Data']['EmpTotalSurvey'],
    'MobileNpsScore' => number_format($Mobile[0][0]['Data']['EmpMobile'], npsDecimal()),
    'FixedAdvocate' => $Fixed[0][0]['Data']['EmpAdvocate'],
    'FixedPassive' => $Fixed[0][0]['Data']['EmpPassive'],
    'FixedDetractor' => $Fixed[0][0]['Data']['EmpDetractor'],
    'FixedTotalSurvey' => $Fixed[0][0]['Data']['EmpTotalSurvey'],
    'FixedNpsScore' => number_format($Fixed[0][0]['Data']['EmpFixed'], npsDecimal()),
    'EpisodeAdvocate' => $Episode[0][0]['Data']['EmpAdvocate'],
    'EpisodePassive' => $Episode[0][0]['Data']['EmpPassive'],
    'EpisodeDetractor' => $Episode[0][0]['Data']['EmpDetractor'],
    'EpisodeTotalSurvey' => $Episode[0][0]['Data']['EmpTotalSurvey'],
    'EpisodeNpsScore' => number_format($Episode[0][0]['Data']['EmpEpisode'], npsDecimal())
);

$sqlRawEmpNPS = "CALL `getRawNpsEmpID` ($empID, '$from', '$to')";
$resultRawEmpNPS = mysql_query($sqlRawEmpNPS, $connection);
while ($rowRawEmpNPS = mysql_fetch_assoc($resultRawEmpNPS)) {
    $RawEmpNPS = $rowRawEmpNPS;
}

//print_r($RawEmpNPS);
//echo "CALL `getRawNpsEmpID` ($empID, '$from', '$to',$storeType)";


function generateNpsSummary($EmpNpsDetails) {
//echo "<pre>"; var_dump($EmpNpsDetails);
    $sumAdvo = 0;
    $sumPas = 0;
    $sumDet = 0;
    $sumSurvey = 0;
    foreach ($EmpNpsDetails as $data) {
        $sumAdvoInt += intval($data['InteractionAdvocate']);
        $sumPasInt += intval($data['InteractionPassive']);
        $sumDetInt += intval($data['InteractionDetractor']);
        $sumSurveyInt += intval($data['InteractionTotalSurvey']);

        $sumAdvoEpi += intval($data['EpisodeAdvocate']);
        $sumPasEpi += intval($data['EpisodePassive']);
        $sumDetEpi += intval($data['EpisodeDetractor']);
        $sumSurveyEpi += intval($data['EpisodeTotalSurvey']);
    }
// echo "$sumAvo - $sumDet / $sumSurvey * 100<bR>";
    $NPSInt = ((intval($sumAdvoInt) - intval($sumDetInt)) / intval($sumSurveyInt)) * 100;
    $NPSInt = number_format($NPSInt, npsDecimal());

    $NPSEpi = ((intval($sumAdvoEpi) - intval($sumDetEpi)) / intval($sumSurveyEpi)) * 100;
    $NPSEpi = number_format($NPSEpi, npsDecimal());

    $returnData = array('SummaryNPSInt' => $NPSInt, 'SummaryAdvocateInt' => $sumAdvoInt, 'SummaryDetractorsInt' => $sumDetInt, 'SummaryPassiveInt' => $sumPasInt, 'SummaryTotalSurveyInt' => $sumSurveyInt,
        'SummaryNPSEpi' => $NPSEpi, 'SummaryAdvocateEpi' => $sumAdvoEpi, 'SummaryDetractorsEpi' => $sumDetEpi, 'SummaryPassiveEpi' => $sumPasEpi, 'SummaryTotalSurveyEpi' => $sumSurveyEpi);


    return $returnData;
}

$npsSummaryScore = generateNpsSummary($EmpNpsDetails);


$storeRank = getEmployeeImpactRank('Interaction', 'AllEmployees', 'No', $from, $to, $storeType);

$storeRankMobile = getEmployeeImpactRank('Mobile', 'AllEmployees', 'No', $from, $to, $storeType);
$storeRankFixed = getEmployeeImpactRank('Fixed', 'AllEmployees', 'No', $from, $to, $storeType);
$storeRankEpisode = getEmployeeImpactRank('Episode', 'AllEmployees', 'No', $from, $to, $storeType);
$weeklyto = date("Y-m-d");
//$weeklyfrom = date("Y-m-d", strtotime('4 weeks ago'));
$weeklyfrom = date("Y-m-d", strtotime('-27 days'));

//$intWeek = getWeekly('Interaction', $empID);
//$epiWeek = getWeekly('Episode', $empID);
//echo $npsSummaryScore;
//echo "<pre>";
//print_r($intWeek);
//echo "this is storetype ".$storeType;
?>


<script src="includes/js/plugins.js"></script>
<!--<script src="includes/js/circletype.js"></script>-->
<script type="text/javascript">
    $(document).ready(function() {
        var from = $("#from").val().split("/").reverse().join("-");
        var to = $("#to").val().split("/").reverse().join("-");
        var empID = $("#empid").val();

        $("#toggleFeedack").click(function() {
            $('#CustomerFeedback').fadeOut('slow').load('nps/summary/customerfeedback.php?to=' + to + '&from=' + from + '&empID=' + empID + '&isProfile=Yes').fadeIn("slow");
        });

        $('.showDate').html(" " + from + ' To ' + to);

    });
</script>

<input type="hidden" id="from" name="from" value="<?php echo $from; ?>"/>
<input type="hidden" id="to" name="to"  value="<?php echo $to; ?>"/>
<input type="hidden" id="empid" name="empid"  value="<?php echo $empID; ?>"/>

<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div id="sideprofile">
            <div class="row">
                <div class="col-lg-12">
                    <center>
                        <img class="img-circle" style="border: 2px solid rgba(72, 132, 124, 0.35);" src="<?php echo $profileImg; ?>" id="ProfilePic">
                        <h2 id="user-name">
                            <i class="fa fa-user"></i> <?php echo $empProf[0]['f_EmpFirstName'] . " " . $empProf[0]['f_EmpLastName']; ?>
                        </h2>
                        <button type="button" class="btn btn-sm btn-warning" data-toggle="modal" href="admin/modal/EditEmployeeModal.php?empID=<?php echo $empID; ?>" data-target="#EditEmployeeModal" onclick="openModalEdit(this)"><i class="fa fa-wrench"></i> Profile</button>
                        <button type="button" class="btn btn-sm btn-danger" togolink="imageEditor/index.html?photoUrl=<?php echo $profileImg; ?>" id="ProfilePicture" onclick="LoadProfilePic(this)"><i class="fa fa-plus"></i> Picture</button>
                    </center>
                    <hr>
                    <center>

                        <section class="row" id="userFeedback">
                            <div class="col-lg-12">
                                <h4><i class="fa fa-comments-o fw"  style="margin-bottom: 10px;"></i> Raw Episode Feedback:</h4>
                                <div class="col-xs-12 text-center">
                                    <div class="col-xs-2 col-sm-2 emphasis">
                                        <h2><strong>
                                                <a data-toggle="modal" class="feedback"  href="nps/function/FeedbackModal.php?empID=<?php echo $empID; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=episode&modulator=exlcude" data-target="#feedbackmodal" >
                                                    <i class="fa fa-comments-o fa-fw"></i><?php echo $RawEmpNPS['EmpEpiTotalSurvey']; ?>
                                                </a>
                                            </strong>
                                        </h2>
                                        <p><small>All</small></p>
                                    </div>

                                    <div class="col-xs-2 col-sm-2 emphasis">
                                        <h2>
                                            <strong>
                                                <a data-toggle="modal" class="feedback"  href="nps/function/FeedbackModal.php?rate=advo&empID=<?php echo $empID; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=episode"  data-target="#feedbackmodal">
                                                    <i class="fa fa-comments-o fa-fw"></i><?php echo $RawEmpNPS['EmpEpiAdvocate']; ?>
                                                </a>
                                            </strong>
                                        </h2>
                                        <p><small>Advo</small></p>
                                    </div>
                                    <div class="col-xs-2 col-sm-2 emphasis">
                                        <h2><strong>
                                                <a data-toggle="modal" class="feedback"  href="nps/function/FeedbackModal.php?rate=pass&empID=<?php echo $empID; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=episode"  data-target="#feedbackmodal">
                                                    <i class="fa fa-comments-o fa-fw"></i><?php echo $RawEmpNPS['EmpEpiPassive']; ?>
                                                </a>
                                            </strong>
                                        </h2>
                                        <p><small>Pass</small></p>
                                    </div>

                                    <div class="col-xs-2 col-sm-2 emphasis">
                                        <h2><strong>
                                                <a data-toggle="modal" class="feedback"  href="nps/function/FeedbackModal.php?rate=detract&empID=<?php echo $empID; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=episode"
                                                   data-target="#feedbackmodal">
                                                    <i class="fa fa-comments-o fa-fw"></i><?php echo $RawEmpNPS['EmpEpiDetractor']; ?>
                                                </a>
                                            </strong>
                                        </h2>
                                        <p><small>Detra</small></p>
                                    </div>

                                    <div class="col-xs-2 col-sm-2 emphasis">
                                        <h2><strong>
                                                <a data-toggle="modal" class="feedback"  href="nps/function/FeedbackModal.php?rate=noScore&empID=<?php echo $empID; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&type=episode"
                                                   data-target="#feedbackmodal">
                                                    <i class="fa fa-comments-o fa-fw"></i><?php echo $RawEmpNPS['EmpEpiNoScore']; ?>
                                                </a>
                                            </strong>
                                        </h2>
                                        <p><small>No Score</small></p>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </center><br>

                    <?php foreach ($empProf as $empValue) { ?>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <ul class="list-group profile-details">
                                    <li class="list-group-item"><i class="glyphicon glyphicon-map-marker"></i> <?php echo $empValue['f_StoreListName']; ?></li>
                                    <li class="list-group-item"><i class="glyphicon glyphicon-lock"></i> <?php echo $empValue['f_EmpRoleName']; ?><br></li>
                                    <li class="list-group-item"><i class="glyphicon glyphicon-user"></i> <?php echo $empValue['f_EmpPNumber']; ?></li>
                                </ul>
                            </div>
                        </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>


    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <div id='profile-content'>

            <section class="row" id="badges">
                <div class="col-lg-12" id='ScoreBadges'>
                    <h4 class="sub-header"><i class="fa fa-comments-o fw"></i> Overall NPS TBC </h4>
                    <!--                    <div class='col-xs-6'>
                                            <center>
                                                <div class="nps-badge-gold animated pulse infinite">
                                                    <h4 class="mynps-ribbon-heading-interaction">
                    <?php echo number_format($InteractionOverall[0][0]['Data']['EmpInteraction'], 0); ?>
                                                    </h4>
                                                    <small>Interaction</small>
                                                </div>
                                            </center>
                                        </div>-->
                    <div class='col-xs-4'>
                        <center>
                            <div class="nps-badge-orange animated pulse infinite" id="mynPS-badge-orange">
                                <h4 class="mynps-ribbon-heading-episode">
                                    <?php echo number_format($MobileOverall[0][0]['Data']['EmpMobile'], npsDecimal()); ?>
                                </h4>
                                <small>Mobile</small>
                            </div>
                        </center>
                    </div>
                    <div class='col-xs-4'>
                        <center>
                            <div class="nps-badge-blue animated pulse infinite" id="mynPS-badge-blue">
                                <h4 class="mynps-ribbon-heading-episode">
                                    <?php echo number_format($FixedOverall[0][0]['Data']['EmpFixed'], npsDecimal()); ?>
                                </h4>
                                <small>Fixed</small>
                            </div>
                        </center>
                    </div>
                    <div class='col-xs-4'>
                        <center>
                            <div class="nps-badge-green animated pulse infinite" id="mynPS-badge-green">
                                <h4 class="mynps-ribbon-heading-episode">
                                    <?php echo number_format($EpisodeOverall[0][0]['Data']['EmpEpisode'], npsDecimal()); ?>
                                </h4>
                                <small>Episode</small>
                            </div>
                        </center>
                    </div>

                </div>
            </section><br>
            <section class="row" id="user-status">
                <?php
                if (!is_null($EmpNpsDetails)) {
                    $spanID2 = 0;
                    foreach ($EmpNpsDetails as $key => $value) {
                        ?>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <h4 class="sub-header"><i class="fa fa-user"></i> NPS Status: <?php echo $value['StoreName']; ?></h4>

                            <?php $getClassEmployee = getClassEmployeeMobileTBC($value['MobileNpsScore']); ?>
                            <div <?php echo $getClassEmployee['class2']; ?> id='emp-target' style="margin-bottom: 10px;">
                                <h6>Mobile</h6>
                                <h5><i class="fa fa-exclamation-circle"></i> <?php echo $getClassEmployee['message2']; ?></h5>
                                <?php if ($value['MobileNpsScore'] > 1 && $value['MobileNpsScore'] < 60) { ?>
                                    <script type="text/javascript">
                                        $(document).ready(function() {
                                            var NewTotalSurvey = <?php echo $value['MobileTotalSurvey']; ?>;
                                            var newInterAdvoTotal = <?php echo $value['MobileAdvocate']; ?>;
                                            var MobileTotalDetractorTotal = <?php echo $value['MobileDetractor']; ?>;

                                            var counter = 0;
                                            var newDetPercentage = 0;
                                            var newAdvoPercentage = 0;
                                            var newIntercationNPS = 0;

                                            while (newIntercationNPS < 60) {
                                                newInterAdvoTotal += 1;
                                                NewTotalSurvey += 1;
                                                newAdvoPercentage = (newInterAdvoTotal / NewTotalSurvey) * 100;
                                                newDetPercentage = (MobileTotalDetractorTotal / NewTotalSurvey) * 100;
                                                newIntercationNPS = newAdvoPercentage - newDetPercentage;
                                                counter += 1;
                                            }
                                            //    var needed = counter - newInterAdvoTotal;
                                            $("#ToGet<?php echo $spanID2; ?>").append(counter + " Advocates needed.");
                                        });
                                    </script>
                                    <?php $idSharet = "ToGet{$spanID2}"; ?>
                                    <h5><i class="fa fa-check-square "></i> <span id="<?php echo $idSharet; ?>"></span> </h5>
                                <?php } elseif ($value['MobileNpsScore'] == 0) { ?>
                                    <h5><i class="fa fa-check-square"></i> Need at least 1 Advocate</h5>
                                <?php } else { ?>
                                    <h5><i class="fa fa-check-square"></i> Goals Accomplished!</h5>
                                <?php } ?>
                            </div>

                            <?php $getClassEmployee = getClassEmployeeFixedTBC($value['FixedNpsScore']); ?>
                            <div <?php echo $getClassEmployee['class3']; ?> id='emp-target' style="margin-bottom: 10px;">
                                <h6>Fixed</h6>
                                <h5><i class="fa fa-exclamation-circle"></i> <?php echo $getClassEmployee['message3']; ?></h5>
                                <?php if ($value['FixedNpsScore'] > 1 && $value['FixedNpsScore'] < 16) { ?>
                                    <script type="text/javascript">
                                        $(document).ready(function() {
                                            var NewTotalSurvey = <?php echo $value['FixedTotalSurvey']; ?>;
                                            var newInterAdvoTotal = <?php echo $value['FixedAdvocate']; ?>;
                                            var FixedTotalDetractorTotal = <?php echo $value['FixedDetractor']; ?>;

                                            var counter = 0;
                                            var newDetPercentage = 0;
                                            var newAdvoPercentage = 0;
                                            var newIntercationNPS = 0;

                                            while (newIntercationNPS < 40) {
                                                newInterAdvoTotal += 1;
                                                NewTotalSurvey += 1;
                                                newAdvoPercentage = (newInterAdvoTotal / NewTotalSurvey) * 100;
                                                newDetPercentage = (FixedTotalDetractorTotal / NewTotalSurvey) * 100;
                                                newIntercationNPS = newAdvoPercentage - newDetPercentage;
                                                counter += 1;
                                            }
                                            //    var needed = counter - newInterAdvoTotal;
                                            $("#ToGet<?php echo $spanID2; ?>").append(counter + " Advocates needed.");
                                        });
                                    </script>
                                    <?php $idSharet = "ToGet{$spanID2}"; ?>
                                    <h5><i class="fa fa-check-square "></i> <span id="<?php echo $idSharet; ?>"></span> </h5>
                                <?php } elseif ($value['FixedNpsScore'] == 0) { ?>
                                    <h5><i class="fa fa-check-square"></i> Need at least 1 Advocate</h5>
                                <?php } else { ?>
                                    <h5><i class="fa fa-check-square"></i> Goals Accomplished!</h5>
                                <?php } ?>
                            </div>
                            <?php $getClassEmployee = getEpisodeClassEmployeeTBC($value['EpisodeNpsScore']); ?>
                            <div <?php echo $getClassEmployee['class4']; ?> id='emp-target' style="margin-bottom: 10px;">
                                <h6>Episode</h6>
                                <h5><i class="fa fa-exclamation-circle"></i> <?php echo $getClassEmployee['message4']; ?></h5>
                                <?php if ($value['EpisodeNpsScore'] > 1 && $value['EpisodeNpsScore'] < 71) { ?>
                                    <script type="text/javascript">
                                        $(document).ready(function() {
                                            var NewTotalSurvey = <?php echo $value['EpisodeTotalSurvey']; ?>;
                                            var newInterAdvoTotal = <?php echo $value['EpisodeAdvocate']; ?>;
                                            var EpisodeTotalDetractorTotal = <?php echo $value['EpisodeDetractor']; ?>;

                                            var counter = 0;
                                            var newDetPercentage = 0;
                                            var newAdvoPercentage = 0;
                                            var newIntercationNPS = 0;

                                            while (newIntercationNPS < 50) {
                                                newInterAdvoTotal += 1;
                                                NewTotalSurvey += 1;
                                                newAdvoPercentage = (newInterAdvoTotal / NewTotalSurvey) * 100;
                                                newDetPercentage = (EpisodeTotalDetractorTotal / NewTotalSurvey) * 100;
                                                newIntercationNPS = newAdvoPercentage - newDetPercentage;
                                                counter += 1;
                                            }
                                            //    var needed = counter - newInterAdvoTotal;
                                            $("#ToGet<?php echo $spanID2; ?>").append(counter + " Advocates needed.");
                                        });
                                    </script>
                                    <?php $idSharet = "ToGet{$spanID2}"; ?>
                                    <h5><i class="fa fa-check-square "></i> <span id="<?php echo $idSharet; ?>"></span> </h5>
                                <?php } elseif ($value['EpisodeNpsScore'] == 0) { ?>
                                    <h5><i class="fa fa-check-square"></i> Need at least 1 Advocate</h5>
                                <?php } else { ?>
                                    <h5><i class="fa fa-check-square"></i> Goals Accomplished!</h5>
                                <?php } ?>
                            </div>
                        </div><!--end needed Survey-->
                        <?php
                        $spanID2++;
                    }
                    ?>
                <?php } else { ?>
                    <div class="col-lg-6">
                        <h4 class="sub-header"><i class="fa fa-user"></i> NPS Status </h4>
                        <div class='alert alert-info' style="margin-bottom: 10px;">
                            <p><i class="fa fa-exclamation-circle"></i> No Records yet.</p>
                            <p><i class="fa fa-check-square"></i> Need at least 1 Advocate</p>
                        </div>
                    </div>
                <?php } ?>
            </section><br>



            <section class="row" id="badges">
                <div class="col-lg-12" id='ScoreBadges'>
                    <h4 class="sub-header"><i class="fa fa-comments-o fw"></i> <rolling>4 Weeks</rolling> Rolling Mobility NPS </h4>
                    <table id="mynpstlsmob" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
                        <thead>
                            <tr>
                                <th>Store</th>
                                <th>AD</th>
                                <th>PA</th>
                                <th>D</th>
                                <th>No Score</th>
                                <th>Total</th>
                                <th>NPS</th>
                                <th>Impact</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($Mobile[0] as $value) {
                                //echo $key;
                                ?>
                                <tr>
                                    <td><?php echo $value['Data']['Store']; ?></td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModalTBC.php?rate=advo&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&type=mobile&storeOwnerID=<?php echo $value['Data']['f_UserID']; ?>&rangeType=<?php echo $rangeType; ?>" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpAdvocate']; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModalTBC.php?rate=pass&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&type=mobile&storeOwnerID=<?php echo $value['Data']['f_UserID']; ?>&rangeType=<?php echo $rangeType; ?>" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpPassive']; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModalTBC.php?rate=detract&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&type=mobile&storeOwnerID=<?php echo $value['Data']['f_UserID']; ?>&rangeType=<?php echo $rangeType; ?>" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpDetractor']; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModalTBC.php?rate=noScore&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&type=mobile&storeOwnerID=<?php echo $value['Data']['f_UserID']; ?>&rangeType=<?php echo $rangeType; ?>" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpNoScore']; ?></a>
                                    </td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModalTBC.php?empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&rate=withScore&type=mobile&storeOwnerID=<?php echo $value['Data']['f_UserID']; ?>&rangeType=<?php echo $rangeType; ?>" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpTotalSurvey']; ?>
                                        </a>
                                    </td>
                                    <td><?php echo number_format($value['Data']['EmpMobile'], npsDecimal()); ?></td>
                                    <td><?php echo number_format($value['EmpImpact'], 2); ?></td>
                                </tr>
                            <?php }
                            ?>
                        </tbody>
                    </table>
                </div>
            </section>
            <section class="row" id="badges">
                <div class="col-lg-12" id='ScoreBadges'>
                    <h4 class="sub-header"><i class="fa fa-comments-o fw"></i> <rolling>4 Weeks</rolling> Rolling Fixed NPS </h4>
                    <table id="mynpstlsfix" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
                        <thead>
                            <tr>
                                <th>Store</th>
                                <th>AD</th>
                                <th>PA</th>
                                <th>D</th>
                                <th>No Score</th>
                                <th>Total</th>
                                <th>NPS</th>
                                <th>Impact</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($Fixed[0] as $value) {
                                //echo $key;
                                ?>
                                <tr>
                                    <td><?php echo $value['Data']['Store']; ?></td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModalTBC.php?rate=advo&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&type=fixed&storeOwnerID=<?php echo $value['Data']['f_UserID']; ?>&rangeType=<?php echo $rangeType; ?>" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpAdvocate']; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModalTBC.php?rate=pass&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&type=fixed&storeOwnerID=<?php echo $value['Data']['f_UserID']; ?>&rangeType=<?php echo $rangeType; ?>" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpPassive']; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModalTBC.php?rate=detract&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&type=fixed&storeOwnerID=<?php echo $value['Data']['f_UserID']; ?>&rangeType=<?php echo $rangeType; ?>" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpDetractor']; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModalTBC.php?rate=noScore&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&type=fixed&storeOwnerID=<?php echo $value['Data']['f_UserID']; ?>&rangeType=<?php echo $rangeType; ?>" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpNoScore']; ?></a>
                                    </td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModalTBC.php?empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&rate=withScore&type=fixed&storeOwnerID=<?php echo $value['Data']['f_UserID']; ?>&rangeType=<?php echo $rangeType; ?>" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpTotalSurvey']; ?>
                                        </a>
                                    </td>
                                    <td><?php echo number_format($value['Data']['EmpFixed'], npsDecimal()); ?></td>
                                    <td><?php echo number_format($value['EmpImpact'], 2); ?></td>
                                </tr>
                            <?php }
                            ?>
                        </tbody>
                    </table>
                </div>
            </section>
            <section class="row" id="badges">
                <div class="col-lg-12" id='ScoreBadges'>
                    <h4 class="sub-header"><i class="fa fa-comments-o fw"></i> <rolling>4 Weeks</rolling> Rolling Episode NPS </h4>
                    <table id="mynpstlsintepi" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
                        <thead>
                            <tr>
                                <th>Store</th>
                                <th>AD</th>
                                <th>PA</th>
                                <th>D</th>
                                <th>No Score</th>
                                <th>Total</th>
                                <th>NPS</th>
                                <th>Impact</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($Episode[0] as $value) {
                                //echo $key;
                                ?>
                                <tr>
                                    <td><?php echo $value['Data']['Store']; ?></td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModalTBC.php?rate=advo&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&type=episode&storeOwnerID=<?php echo $value['Data']['f_UserID']; ?>&rangeType=<?php echo $rangeType; ?>" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpAdvocate']; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModalTBC.php?rate=pass&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&type=episode&storeOwnerID=<?php echo $value['Data']['f_UserID']; ?>&rangeType=<?php echo $rangeType; ?>" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpPassive']; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModalTBC.php?rate=detract&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&type=episode&storeOwnerID=<?php echo $value['Data']['f_UserID']; ?>&rangeType=<?php echo $rangeType; ?>" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpDetractor']; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModalTBC.php?rate=noScore&empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&type=episode&storeOwnerID=<?php echo $value['Data']['f_UserID']; ?>&rangeType=<?php echo $rangeType; ?>" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpNoScore']; ?></a>
                                    </td>
                                    <td>
                                        <a data-toggle="modal"  class="feedback" href="nps/function/FeedbackModalTBC.php?empID=<?php echo $value['Data']['EmpID']; ?>&from=<?php echo $from; ?>&to=<?php echo $to; ?>&action5=<?php echo $value['Data']['f_EmpPNumber']; ?>&StoreID=<?php echo $value['Data']['f_StoreID']; ?>&rate=withScore&type=episode&storeOwnerID=<?php echo $value['Data']['f_UserID']; ?>&rangeType=<?php echo $rangeType; ?>" data-target="#feedbackmodal">
                                            <i class="fa fa-comments-o fa-fw"></i><?php echo $value['Data']['EmpTotalSurvey']; ?>
                                        </a>
                                    </td>
                                    <td><?php echo number_format($value['Data']['EmpEpisode'], npsDecimal()); ?></td>
                                    <td><?php echo number_format($value['EmpImpact'], 2); ?></td>
                                </tr>
                            <?php }
                            ?>
                        </tbody>
                    </table>
                </div>
            </section>

            <br>

            <section id="UserStoreTarget">
                <div class="row">
                    <div class='col-lg-12  col-md-12 col-sm-12 col-xs-12 nps-target'>
                        <?php if (count($StoreNps) > 0) { ?>
                            <?php foreach ($StoreNps as $value) { ?>

                                <h4 class="sub-header"><i class="fa fa-home fa-fw"></i>Store NPS: <?php echo $value['StoreName']; ?> </h4>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"  style="text-align: center;">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Fixed
                                            </div>
                                            <div class="panel-body panel-small">
                                                <h3> <?php echo $value['FixedScore']; ?> </h3>
                                                <p><i class="fa fa-trophy"></i> <?php echo $value['FixedTier']; ?> </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"  style="text-align: center;">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Mobile
                                            </div>
                                            <div class="panel-body panel-small">
                                                <h3> <?php echo $value['MobileScore']; ?> </h3>
                                                <p><i class="fa fa-trophy"></i> <?php echo $value['MobileTier']; ?> </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"  style="text-align: center;">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Episode
                                            </div>
                                            <div class="panel-body panel-small">
                                                <h3> <?php echo $value['EpisodeScore']; ?> </h3>
                                                <p><i class="fa fa-trophy"></i> <?php echo $value['EpisodeTier']; ?> </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>
                        </div>
                    <?php } else { ?>
                        <h4 class="sub-header"><i class="fa fa-Home fa-fw"></i>Store NPS: </h4>
                        <div class='alert alert-info'>
                            <p><i class="fa fa-exclamation-circle"></i> No Records yet.</p>
                            <p><i class="fa fa-check-square"></i> You need at least 1 Advocate</p>
                        </div>

                    <?php } ?>
                </div>
            </section>
        </div>


        <section id="userAllFeedack">
            <div class='col-lg-12'>
                <div id="CustomerFeedback"></div>
            </div>
        </section>

    </div><!--end profile-content-->
</div><!--end col-lg-8-->
</div><!--end row-->


<!-- Modal EditEmployeeModal-->
<div class="modal fade" id="EditEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal  EditEmployeeModal -->

<div class="modal fade" id="feedbackmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal  CreateStoreModal -->

<script type="text/javascript">
    $(function()
    {
        $("#toggleFeedack").click(function()
        {
            $(".Allfeedback").slideToggle();
            return false;
        });
    });
</script>

<script>
    $(document).ready(function() {
        //Refresh Modal Content
        $('a.feedback').click(function(ev) {
            ev.preventDefault();
            var target = $(this).attr('href');

            $("#feedbackmodal .modal-content").load(target, function() {
                $("#feedbackmodal").modal("show");
            });
        });
    });

    function sortNumbersIgnoreText(a, b, high) {
        var reg = /[+-]?((\d+(\.\d*)?)|\.\d+)([eE][+-]?[0-9]+)?/;
        a = a.match(reg);
        a = a !== null ? parseFloat(a[0]) : high;
        b = b.match(reg);
        b = b !== null ? parseFloat(b[0]) : high;
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    }

    function stripHTML(dirtyString) {
        var container = document.createElement('div');
        container.innerHTML = dirtyString;
        return container.textContent;
    }

    jQuery.extend(jQuery.fn.dataTableExt.oSort, {
        "sort-numbers-ignore-text-asc": function(a, b) {
            return sortNumbersIgnoreText(a, b, Number.POSITIVE_INFINITY);
        },
        "sort-numbers-ignore-text-desc": function(a, b) {
            return sortNumbersIgnoreText(a, b, Number.NEGATIVE_INFINITY) * -1;
        }
    });

    $(document).ready(function() {
        $('#mynpstlsint,#mynpstlsmob,#mynpstlsfix,#mynpstlsepi').DataTable({
            "paging": false,
            "ordering": false,
            "info": false,
            bFilter: false, bInfo: false
        });
    });
</script>


