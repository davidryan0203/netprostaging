<?php

$variables = [
    'connectToken' => "https://login.azenko.com.au/connect/token",
#'connectToken' => "https://is.atgsystems.com.au/connect/token",
    'login' => "https://login.azenko.com.au",
    #'login' => "https://is.atgsystems.com.au",
    'logOut' => "https://login.azenko.com.au/account/logout",
    #'logOut' => "https://is.atgsystems.com.au/Account/logout",
    'npsDecimal' => 1
];
foreach ($variables as $key => $value) {
    putenv("$key=$value");
}


if (!function_exists('npsDecimal')) {

    function npsDecimal() {
        return 1;
    }

}
?>