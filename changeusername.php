<?php
session_start();
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
$loginID = $_SESSION['LoginID'];

$sqlGetCurrentPass = "Select f_Password From t_userlogin Where f_UserLoginID = '{$loginID}'";
$result = mysql_query($sqlGetCurrentPass);
while ($row = mysql_fetch_assoc($result)) {
    $currentPass = $row['f_Password'];
}
?>

<form role="form" id="ChangeEmailForm"><!-- /modal-header -->
    <!-- Modal transparent -->
    <div class="modal modal-transparent fade" id="modal-transparent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa fa-shield"></i> NetPro process upgrade</h4>
                </div>

                <div class="modal-body">
                    <blockquote id="greetings">
                        <b>Hi Mate!</b>
                        <br>We are currently upgrading our records.
                        <br>Please enter your current work email address.
                    </blockquote>

                    <div class="well">

                        <div class="form-group">
                            <label class="control-label" for="Password">Email:</label>
                            <input type="email" class="form-control" id="newUserName" name="newUserName" placeholder="Enter Email" size="32" minlength="3" maxlength="64" required >
                        </div> <!-- /new password -->

                        <div class="form-group">
                            <label class="control-label" for="Password">Confirm Email:</label>
                            <input type="email" class="form-control" id="confirmUserName" name="confirmUserName" placeholder="Confirm Email" size="32" minlength="3" maxlength="64" required>
                        </div><!-- /confirm new password -->
                    </div>
                    <div id="emailUsed"></div>
                    <div id="emailUsedCheck"></div>
                    <div id="compareEmail"></div>
                    <div id="ajaxmsgs"></div>  <!-- ajaxmsg div -->
                </div>

                <div class="modal-footer">
                    <input type="submit" style="visibility: hidden"/>
                    <button type="button" class="btn btn-sm btn-info" data-dismiss="modal" id="btnClose" style="display:none">Continue</button>
                    <button type="button" class="btn btn-sm btn-warning"  id="btnCancel"  >Cancel</button>
                    <button type="button" class="btn btn-sm btn-success"  id="btnSubmit"  >Submit</button>
                </div>
            </div>
        </div>
    </div>

</form>


<script type="text/javascript">
    $(document).ready(function() {
        // $('#pswd_info').hide();
        $('#modal-transparent').modal({
            backdrop: 'static',
            keyboard: false
        }, 'show');

        // .modal-backdrop classes

        $(".modal-transparent").on('show.bs.modal', function() {
            setTimeout(function() {
                $(".modal-backdrop").addClass("modal-backdrop-transparent");
            }, 0);
        });
        $(".modal-transparent").on('hidden.bs.modal', function() {
            $(".modal-backdrop").addClass("modal-backdrop-transparent");
        });



        var button = $('#btnSubmit');
        var loginID = '<?php echo $loginID; ?>';
        var match = false;
        var isExist = true;
        var emailUsedCheck = false;

        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }

        $('#confirmUserName').keyup(function(e) {

            var email1 = $('#newUserName').val();
            var email2 = $('#confirmUserName').val();
            if (email1 != email2) {
                $('#compareEmail').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Emails don't match!</strong></div>");
                match = false;
            } else {
                $('#compareEmail').html("");
                match = true;
            }

        });

        $('#newUserName').keyup(function(e) {
            var newUserName = $(this).val();
            var params = 'action=username_availability&username=' + newUserName;

            if (newUserName.length > 6) {

                if (isEmail(newUserName)) {

                    $.ajax({
                        type: "POST",
                        data: params,
                        url: 'admin/function/CheckUserName.php',
                        success: function(responseText) { // Get the result and asign to each cases
                            if (responseText == 0) {
                                $('#emailUsed').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Username name available!</strong></div>");
                                isExist = false;
                            } else if (responseText > 0) {
                                console.log(responseText);
                                $('#emailUsed').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Username already used!</strong></div>");
                                isExist = true;
                            } else {
                                $('#emailUsed').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Something went wrong kindly contact the website administrator!</strong></div>");

                            }
                        }
                    });

                    $('#emailUsedCheck').html("");
                    emailUsedCheck = true;
                } else {
                    $('#emailUsedCheck').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Email used is invalid!</strong></div>");
                    emailUsedCheck = false;
                }
            }
            e.preventDefault();
        });


        $("input").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
                $("#btnSubmit").click();
            }
        });

        $('#newUserName,#confirmUserName').keydown(function(e) { // Dont allow users to enter spaces for their username and passwords
            if (e.which == 32) {
                return false;
            }
        });

        $('#btnSubmit').click(function(event) {
            if ($("#ChangeEmailForm")[0].checkValidity()) {
                var url = 'admin/function/SubFunctionCaller.php?module=ChangeEmail';
                var mydata = $("#ChangeEmailForm").serialize();

                console.log(mydata);
                console.log(match + ' ' + isExist + ' ' + emailUsedCheck);
                if ((match) && !(isExist) && (emailUsedCheck)) {
                    $.ajax({
                        url: url,
                        type: 'get',
                        data: mydata,
                        dataType: 'json',
                        success: function(result) {
                            console.log(result);
                            document.getElementById('ajaxmsgs').style.display = 'block';
                            if (result.type == 'Success') {
                                $('#ajaxmsgs').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times</a><strong>Awesome!</strong> " + result.message + "</div>");
                                $('#btnClose').show();
                                $('#btnSubmit,#btnCancel').css("display", "none");

                            } else {
                                $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Snap!</strong> " + result.message + "</div>");
                            }
                        },
                        error: function() {
                            document.getElementById('ajaxmsgs').style.display = 'block';
                            $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");

                        }
                    });
                }
            } else {
                $("#ChangeEmailForm").find(':submit').click();
            }
            event.preventDefault();

        }); // ajax button form submit

        $("#btnCancel").click(function(event) {
            window.location.href = "logout.php";
        });

        $("#btnClose").click(function(event) {
            var UserProfile = <?php echo $_SESSION['UserRoleID']; ?>;
            var urlTogo = '';
            if (UserProfile == 4) {
                urlTogo = 'public_html.php?i=1';
            } else if (UserProfile == 6) {
                urlTogo = 'public_html.php?i=6';
            } else {
                urlTogo = 'public_html.php?i=2&o=1';
            }
            window.location.href = urlTogo;
        });


    });
</script>