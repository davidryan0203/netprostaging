<section class='reports'>
    <div class="row">
        <div class="col-lg-12">
            <h3 id="admin-head"><i class='fa fa-user'></i> Priviledged Users</h3>
            
            <div class="filters">
                <a class="btn btn-primary btn-xs btn-admin"  data-toggle="modal" 
                   data-tooltip="tooltip" data-placement="left" title="Add User"
                   href="admin/modal/CreateUserModal.php" data-target="#CreateUserModal" onclick="openCreateUserModal(this)" > 
                    <i class="glyphicon glyphicon-plus"></i> User
                </a>    
            </div>
        </div>

    </div><!--end row-->
    <div class="loader" id="LogLoader"></div>
    <div id="page-contents"></div>
</section>

<script src="../includes/js/custom.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {

        $('#page-contents').hide(function () {
            var urlToLoad = "admin/userextend.php";
            var divToHide = "#LogLoader";
            var divToShow = "#page-contents";
            reload(urlToLoad, divToHide, divToShow);
        });
    });

    function reload(urlToLoad, divToHide, divToShow) {
        var urldata = urlToLoad;
        var page = divToShow;
        $.get(urldata).success(
                function (response, status, jqXhr) {
                    $(divToHide).hide();
                    $(page).fadeIn('slow');
                    $(page).empty().append(response);
                }).error(function (response, status, jqXhr) {
            $(divToHide).show();
            reload(urldata, divToHide, divToShow);
        }).complete(function (response, status, jqXhr) {
        });
    }

    function reLoad() {
        var urlToLoad = "admin/userextend.php";
        var divToHide = "#LogLoader";
        var divToShow = "#page-contents";
        reload(urlToLoad, divToHide, divToShow);
    }

    function stripHTML(dirtyString) {
        var container = document.createElement('div');
        container.innerHTML = dirtyString;
        return container.textContent;
    }

    function openCreateUserModal(el) {
        var target = $(el).attr('href');
        $("#CreateUserModal .modal-content").load(target, function () {
            $("#CreateUserModal").modal("show");
        });

        $('#CreateUserModal').on('hidden.bs.modal', function (e) {
            e.preventDefault();
            $('#page-contents').hide();
            $('#LogLoader').fadeIn('slow');
            reLoad();
        });
    }

    function openEditUserModal(el) {
        var target = $(el).attr('href');
        $("#EditUserModal .modal-content").load(target, function () {
            $("#EditUserModal").modal("show");
        });
        $('#EditUserModal').on('hidden.bs.modal', function (e) {
            e.preventDefault();
            $('#page-contents').hide();
            $('#LogLoader').fadeIn('slow');
            reLoad();
        });
    }

    function openDeleteUserModal(el) {
        var target = $(el).attr('href');
        $("#DeleteUserModal .modal-content").load(target, function () {
            $("#DeleteUserModal").modal("show");
        });

        $('#DeleteUserModal').on('hidden.bs.modal', function (e) {
            e.preventDefault();
            $('#page-contents').hide();
            $('#LogLoader').fadeIn('slow');
            reLoad();
        });
    }

    $(function () {
        $('[data-tooltip="tooltip"]').tooltip();
    });
</script>