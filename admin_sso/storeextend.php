<?php
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/StoreFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');


session_start();
$role = $_SESSION['UserRoleID'];
$UserID = $_SESSION['UserProfileID'];
$stack = array('2', '3');
if ($role == '5') {
    $query = $_SESSION['EmpProfileID'];
    $type = "StoreLeader";
    $sqlStoreID = " select b.f_StoreOwnerUserID from t_empnumber a
            left join t_storelist b on a.f_StoreID = b.f_StoreID WHERE  a.f_EmpID = '{$query}'";
    $result = mysql_query($sqlStoreID, $connection);
    while ($row = mysql_fetch_assoc($result)) {
        $UserID = $row['f_StoreOwnerUserID'];
    }
//    echo $sqlStoreID;
//    var_dump($row);
} elseif ($role == '2') {
    $type = "StoreAdmin";
    $query = $UserID;
} elseif ($role == '3') {
    $query = $UserID;
    $type = "StoreOwner";
} elseif ($role == '1') {
    $type = "GlobalAdmin";
    $query = NULL;
}
//$query = null;
//echo $UserID;
$storeData = SelectStore($type, $query);

foreach ($storeData as $newData) {
    $mergeStoreData[$newData['SharedStoreID']]['StoreID'] = $newData['SharedStoreID'];
    $mergeStoreData[$newData['SharedStoreID']]['SharedByUserName'] = $newData['SharedByUserName'];
    $mergeStoreData[$newData['SharedStoreID']]['SharedStoreName'] = $newData['SharedStoreName'];
    $mergeStoreData[$newData['SharedStoreID']]['SharedByUserID'] = $newData['SharedByUserID'];
    $mergeStoreData[$newData['SharedStoreID']]['SharedToUserID'] = $newData['SharedToUserID'];
    $mergeStoreData[$newData['SharedStoreID']]['ShowReports'] = $newData['f_ShowOnReports'];
    $mergeStoreData[$newData['SharedStoreID']]['StoreType'] = $newData['f_StoreTypeName'];
    $mergeStoreData[$newData['SharedStoreID']]['SharedCompanyCode'] = $newData['SharedCompanyCode'];
    $mergeStoreData[$newData['SharedStoreID']]['f_Shared'][] = array('SharedStoreID' => $newData['f_SharedStoreID'], 'SharedtoStoreID' => $newData['f_SharedToStoreID'], 'SharedToStoreName' => $newData['SharedToStoreName']);
}
?>

<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#example').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]],
            columnDefs: [
                {sortable: false, targets: -1},
                {"width": "8%", "targets": -1}
            ],
            "oColVis": {
                "buttonText": "Hide Columns"
            },
            "autoWidth": false
        });
    });

    function stripHTML(dirtyString) {
        var container = document.createElement('div');
        container.innerHTML = dirtyString;
        return container.textContent;
    }

    $(function() {
        $('[data-tooltip="tooltip"]').tooltip();
    })
</script>

<div class="table-responsive">
    <table id="example" class="table table-bordered table-hover"  cellspacing="0" >
        <thead>
            <tr>
                <th>Store</th>
                <th>Code</th>
                <th>Director</th>
                <th>Type</th>
<!--                <th>Reports</th>
                <th>Shared To</th>-->
                <th>Update</th>
            </tr>
        </thead>

        <tbody>
            <?php foreach ($mergeStoreData as $value) { ?>
                <tr>
                    <td><?php echo trim($value['SharedStoreName']); ?></td>
                    <td><?php echo trim($value['SharedCompanyCode']); ?></td>
                    <td><?php echo $value['SharedByUserName']; ?></td>
                    <td><?php echo $value['StoreType']; ?></td>
    <!--                    <td>
                    <?php
//                        if ($value['ShowReports'] == 1) {
//                            echo "Shown";
//                        } else {
//                            echo "Hidden";
//                        }
                    ?>
                    </td>
                    <td>
                    <?php
//                        foreach ($value['f_Shared'] as $sharedValue) {
//                            if (is_null($sharedValue['SharedToStoreName'])) {
//                                echo "Not Shared";
//                            } else {
//                                $shared = $sharedValue['SharedToStoreName'];
//                                echo "<div class='badge badge-green'>" . $shared . "</div>";
//                            }
//                        }
                    ?>
                    <?php if ($value['SharedByUserID'] == $UserID || $role == '1') { ?>
                                <a class="btn-sm btn-hover btn-warning editsharedmodal editcls" data-toggle="modal"
                                   data-tooltip="tooltip" data-placement="bottom" title="Share"
                                   href="admin/modal/EditStoreSharedModal.php?storeID=<?php echo $value['StoreID'] ?>&SharedOwnerUserID=<?php echo $value['SharedByUserID'] ?>"
                                   data-target="#EditStoreSharedModal" onclick="openEditStoreSharedModal(this)">
                                    <span class="glyphicon glyphicon-cog" ></span>
                                </a>
                    <?php } ?>
                    </td>-->
                    <td>
                        <?php if ($role == '1') { ?>
                            <a class="btn-sm btn-hover btn-danger deletemodal editcls"
                               data-tooltip="tooltip" data-placement="bottom" title="Delete"
                               data-toggle="modal" href="admin/modal/DeleteStoreModal.php?storeID=<?php echo $value['StoreID']; ?>"
                               data-target="#DeleteStoreModal" onclick="openDeleteStoreModal(this)">
                                <span class="glyphicon glyphicon-trash" ></span>
                            </a>
                        <?php } ?>
                        <?php if ($value['SharedByUserID'] == $UserID || $role == '1') { ?>
                            <a class="btn-sm btn-hover btn-warning editmodal editcls"
                               data-tooltip="tooltip" data-placement="bottom" title="Update"
                               data-toggle="modal" href="admin/modal/EditStoreModal.php?storeID=<?php echo $value['StoreID'] ?>&SharedOwnerUserID=<?php echo $value['SharedByUserID'] ?>"
                               data-target="#EditStoreModal" onclick="openEditStoreModal(this)">
                                <span class="glyphicon glyphicon-edit" ></span>
                            </a>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>

<!-- Modal  CreateStoreModal-->
<div class="modal fade" id="CreateStoreModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal  CreateStoreModal -->


<!-- Modal EditStoreModal-->
<div class="modal fade" id="EditStoreModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal  EditStoreModal -->

<!-- Modal EditStoreSharedModal-->
<div class="modal fade" id="EditStoreSharedModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal  EditStoreSharedModal -->


<!-- Modal DeleteUserModal-->
<div class="modal fade" id="DeleteStoreModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal  DeleteUserModal -->


