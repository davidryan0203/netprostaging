<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//var_dump($_GET);
include('EmployeeFunction.php');
$getData = array();
$getData = $_GET;
$toDo = $getData['module'];
switch ($toDo) {

    case 'CreateEmployee':
       // echo "was here";
        $createEmployeeRes = CreateEmployee($getData); // call create store function
        if ($createEmployeeRes['message'] == 'Success Creating Employee') { // check if called create store function is success
            $getData['EmpID'] = $createEmployeeRes['EmpID'];
            if (isset($getData['includeEmpNum'])) {
                $createEmployeeNo = CreateEmployeeNo($getData);
                $result['message'] = $createEmployeeRes['message'] . "<br>" . $createEmployeeNo['message']; // return result of create shared store function
            } else {
                $result['message'] = $createEmployeeRes['message'];
            }
            $result['type'] = 'Success';
        } else {
            $result['message'] = $createEmployeeRes['message'];
            $result['type'] = 'Error';
        }

        // $result['message'] = $createEmployeeRes['message'];
        break;
    case 'EditEmployee':
        $editEmployeeRes = UpdateEmployee($getData); // call create store function
        if ($editEmployeeRes['message'] == 'Update Success Employee') { // check if called create store function is success
            $result['type'] = 'Success';
        } else {
            $result['type'] = 'Error';
        }
        $result['message'] = $editEmployeeRes['message'];
        break;
    case 'DeleteEmployee':
        $userID = $getData['EmployeeID'];
        $deleteEmployeeRes = DeleteEmployee($userID); // call delete store function
        if ($deleteEmployeeRes['message'] == 'Deletion Success Employee') { // check if called create store function is success
            $result['type'] = 'Success';
        } else {
            $result['type'] = 'Error';
        }
        $result['message'] = $deleteEmployeeRes['message'];
        break;

    case 'AddEmployeeNumber':

        $createEmployeeNo = CreateEmployeeNo($getData);

        if ($createEmployeeNo['message'] == 'Success Creating Employee Number') {
            $result['type'] = 'Success';
            $result['message'] = $createEmployeeNo['message'];
        } else {
            $result['type'] = 'Error';
        }
        break;

    case 'EditEmployeeNumber':
        $i = 0;
        foreach ($getData['EmpStoreID'] as $dataStoreID) {
            $new_array[] = array('NewEmpStoreID' => $dataStoreID, 'NewEmpNo' => $getData['EmpNo'][$i], 'ByEmpNumberID' => $getData['EmpNumberID'][$i]);
            $i++;
        }

        $editEmployeeNumberRes = EditEmployeeNumber($new_array);

        if ($editEmployeeNumberRes['message'] == 'Success Updating Employee Number') { // check if called update store function is success
            $result['message'] = $editEmployeeNumberRes['message'];
            $result['type'] = 'Success';
        } else {
            $result['type'] = 'Error';
            $result['message'] = $editEmployeeNumberRes['message'];
        }
        break;

    case 'DeleteEmployeeNumber':
      
          if (count($getData['toDelete']) > 1) { // check if multiple delete for ShareStore
            $dataShared = "";
            foreach ($getData['toDelete'] as $data) {
                $dataShared .= "'$data',";
            }
            $dataShared = trim($dataShared, ",");
            $type = "WhereIn";
        } else {
            $dataShared = $getData['toDelete'];
            $type = "Single";
        }

        $deleteSharedStore = DeleteEmployeeNumber($dataShared, $type);

        if ($deleteSharedStore['message'] == 'Success Deleting Employee Number') { // check if called update store function is success
            $result['message'] = "<br>" . $deleteSharedStore['message'];
            $result['type'] = 'Success';
        } else {
            $result['type'] = 'Error';
            $result['message'] = "<br>" . $deleteSharedStore['message'];
        }

        break;
    default:
        $result['type'] = 'Error';
        $result['message'] = 'Unknown process';
}

mysql_close($connection); // close database connection
echo json_encode($result);  // return to ajax result json_encode($result)