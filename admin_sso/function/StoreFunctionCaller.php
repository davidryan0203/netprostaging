<?php

include('StoreFunction.php');

$getData = array();
$getData = $_GET; // get data from ajax
$toDo = $_GET['module'];
//
//echo "<pre>";
//var_dump($getData);
switch ($toDo) {
    case 'CreateStore':
        // $createStoreRes['message'] = 'Success Creating Store';
        $createStoreRes = CreateStore($getData); // call create store function
        // echo $createStoreRes['message'];
        if ($createStoreRes['message'] == 'Success Creating Store') { // check if called create store function is success
            $getData['storeID'] = $createStoreRes['StoreID'];
            if (isset($getData['includeShared'])) { // now check if get data for shared store has data
                $createSharedStore = CreateSharedStore($getData);  // if shared store has data call create shared store function
                $result['message'] = $createStoreRes['message'] . "<br>" . $createSharedStore['message']; // return result of create shared store function
            } else {
                $result['message'] = "Success Creating Store";
            }
            $result['type'] = 'Success';
        } else {
            $result['message'] = $createStoreRes['message'];
            $result['type'] = 'Error';
        }
        break;
    case 'EditStore':
        $updateStoreRes = UpdateStore($getData);
        if ($updateStoreRes['message'] == 'Success Updating Store') { // check if called update store function is success
            $getData['storeID'] = $getData['StoreListID'];

            if (isset($getData['storeShareTo'])) { // now check if get data for shared store has data
                $updateharedStore = UpdateSharedStore($getData);  // if shared store has data call update shared store function  

                if ($updateharedStore['message'] == 'Success Updating Shared Store') {
                    $result['type'] = 'Success';
                    $result['message'] = $updateStoreRes['message'] . "<br>" . $updateharedStore['message']; // return result of update shared store function
                } else {
                    $result['type'] = 'Error';
                    $result['message'] = $updateStoreRes['message'] . "<br>But " . $updateharedStore['message']; // return result of update shared store function
                }
            } else {
                $result['type'] = 'Success';
                $result['message'] = $updateStoreRes['message'];
            }
        } else {
            $result['type'] = 'Error';
        }
        break;
    case 'DeleteStore':
        $storeID = $getData['StoreListID'];
        $deleteStoreRes = DeleteStore($storeID);
        if ($deleteStoreRes['message'] == 'Success Deleting Store') { // check if called update store function is success
            if (isset($getData['ShareStoreID']) && !is_null($getData['ShareStoreID'][0]) && $getData['ShareStoreID'][0] != "") { // now check if get data for shared store has data
                // check if ShareStore will be deleted
                if (count($getData['ShareStoreID']) > 1) { // check if multiple delete for ShareStore
                    $dataShared = "";
                    foreach ($getData['ShareStoreID'] as $data) {
                        $dataShared .= "'$data',";
                    }
                    $dataShared = trim($dataShared, ",");
                    $type = "WhereIn";
                } else {
                    $dataShared = $getData['ShareStoreID'];
                    $type = "Single";
                }
                $deleteharedStore = DeleteSharedStore($dataShared, $type);  // if shared store has data call update shared store function               
                $result['message'] = $deleteStoreRes['message'] . "<br>" . $deleteharedStore['message']; // return result of update shared store function
            } else {
                $result['message'] = "Success Deleting Store";
            }
            $result['type'] = 'Success';
        } else {
            $result['message'] = $deleteStoreRes['message'];
            $result['type'] = 'Error';
        }
        break;
    case 'AddSharedStore':
        $createSharedStore = CreateSharedStore($getData);
        if ($createSharedStore['message'] == 'Success Creating Shared Store') {
            $result['type'] = 'Success';
            $result['message'] = "<br>" . $createSharedStore['message'];
        } else {
            $result['message'] = "Failed Creating Shared Store";
            $result['type'] = 'Error';
        }
        break;
    case 'EditSharedStore':
        //echo "<pre>";
        foreach ($getData['storeShareTo'] as $key => $original) {
            $toAdd[] = array('SharedStoreID' => $getData['SharedStoreID'][$key], 'NewSharedToStoreID' => $original);
        }
        // var_dump($toAdd);
        $editSharedStore = EditSharedStore($toAdd);

        if ($editSharedStore['message'] == 'Success Updating Shared Store') { // check if called update store function is success
            $result['message'] = "<br>" . $editSharedStore['message'];
            $result['type'] = 'Success';
        } else {
            $result['type'] = 'Error';
            $result['message'] = "<br> Failed Updating Shared Store";
        }

        break;
    case 'DeleteSharedStore':
        if (count($getData['toDelete']) > 1) { // check if multiple delete for ShareStore
            $dataShared = "";
            foreach ($getData['toDelete'] as $data) {
                $dataShared .= "'$data',";
            }
            $dataShared = trim($dataShared, ",");
            $type = "WhereIn";
        } else {
            $dataShared = $getData['toDelete'];
            $type = "Single";
        }

        $deleteSharedStore = DeleteSharedStore($dataShared, $type);

        if ($deleteSharedStore['message'] == 'Success Deleting Shared Store') { // check if called update store function is success
            $result['message'] = "<br>" . $deleteSharedStore['message'];
            $result['type'] = 'Success';
        } else {
            $result['type'] = 'Error';
            $result['message'] = "<br> Failed Deleting Shared Store";
        }

        break;
    case 'AddMergeStore':
        $addMergeStore = CreateMergeStore($getData);
        if ($addMergeStore['message'] == 'Success Creating Merge Store') {
            $result['message'] = $addMergeStore['message'];
            $result['type'] = 'Success';
        } else {
            $result['type'] = 'Error';
            $result['message'] = $addMergeStore['message'];
        }

        break;
    case 'EditMergeStore':
        if(count($getData['MergeID']) > 1){
           $mergeQuery = array_combine($getData['MergeID'], $getData['mergeTxb']);
            foreach ($mergeQuery as $toEdit => $newCode) {
                $toAdd[] = array('toEdit' => $toEdit, 'NewCompanyMerge' => $newCode);
            } 
        }else {
           // foreach ($mergeQuery as $toEdit => $newCode) {            
                $toAdd[0] = array('toEdit' => $getData['MergeID'][0], 'NewCompanyMerge' => $getData['mergeTxb'][0]);
           // } 
        }
        
        $editMergeStore = EditMergeStore($toAdd);
        if ($editMergeStore['message'] == 'Success Updating Merge Store') {
            $result['message'] = $editMergeStore['message'];
            $result['type'] = 'Success';
        } else {
            $result['type'] = 'Error';
            $result['message'] = $editMergeStore['message'];
        }

        break;
    case 'DeleteMergeStore':
      
         if (count($getData['toDelete']) > 1) { // check if multiple delete for ShareStore
            $dataShared = "";
            foreach ($getData['toDelete'] as $data) {
                $dataShared .= "'$data',";
            }
            $dataShared = trim($dataShared, ",");
            $type = "WhereIn";
        } else {
            $dataShared = $getData['toDelete'];
            $type = "Single";
        }

        $deleteMergeStore = DeleteMergeStore($dataShared,$type);

        if ($deleteMergeStore['message'] == 'Success Deleting Merge Store') { // check if called update store function is success
            $result['message'] = "<br>" . $deleteMergeStore['message'];
            $result['type'] = 'Success';
        } else {
            $result['type'] = 'Error';
            $result['message'] = "<br> Failed Deleting Merge Store";
        }
        break;

    default:
        $result['type'] = 'Error';
        $result['message'] = 'Unknown Process';
}





mysql_close($connection); // close database connection
echo json_encode($result);  // return to ajax result json_encode($result)
    


