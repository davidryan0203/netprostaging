<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/crypter.php');

session_start();

//InsertLog($logType, $logFrom, $logRemarks, $logBy, $logStatus, $logFromSub);
function UserFunction($function, $query) {

    switch ($function) {

        case "CreateUser":
            $UserFunctionData = CreateUser($query);
            break;
        case "UpdateUser":
            $UserFunctionData = UpdateUser($query);
            break;
        case "SelectUser":
            $UserFunctionData = SelectUser($query);
            break;
        case "DeleteUser":
            $UserFunctionData = SelectUser($query);
            break;

        default:
            $UserFunctionData = "Invalid Protocol";
    }

    return $UserFunctionData;
}

function CreateUser($query) {
    global $connection;
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
    // foreach ($query as $data) {
    $username = mysql_real_escape_string($query['UserName']);
    // $password = md5(mysql_real_escape_string($query['UserPassword']));
    $password = password_hash($query['UserPassword'], PASSWORD_BCRYPT);
    $roleID = mysql_real_escape_string($query['RoleID']);
    $firstname = mysql_real_escape_string($query['FirstName']);
    $lastname = mysql_real_escape_string($query['LastName']);

    $sql = "INSERT INTO `t_userlist`
            (`f_UserFirstName`,`f_UserLastName`)
            VALUES
            ('$firstname','$lastname')";

    $result = mysql_query($sql, $connection);

    if ($result && mysql_affected_rows() == 1) {
        $returnCreateUser['message'] = "Success Creating User";
        $insertedID = mysql_insert_id();

        $logMessageArray = json_encode(array('UserID' => $insertedID,
            'Details' => array($firstname, $lastname)));
        InsertLog('2', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '3');
        $sqlCreateUserLogin = "INSERT INTO t_userlogin (`f_UserName`,`f_Password`,`f_UserType`,`f_EmpID`,`f_UserID`,`f_UserRoleID`) VALUES ('{$username}','{$password}','1','0','{$insertedID}','{$roleID}')";
        $resultUserLogin = mysql_query($sqlCreateUserLogin, $connection);
        if ($resultUserLogin && mysql_affected_rows() == 1) {
            $loginid = mysql_insert_id();
            $logMessageArray = json_encode(array('LoginID' => $loginid, 'Details' => array($username, $password, $roleID)));
            InsertLog('2', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '1');
        } else {
            $logMessageArray = json_encode(array('Reason' => 'An error occured', 'Details' => array($username, $password, $roleID)));
            InsertLog('2', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '1');
        }
    } else {
        $returnCreateUser['message'] = "Failed Creating User";
        $logMessageArray = json_encode(array('Reason' => 'An error occured', 'Details' => $query));
        InsertLog('2', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '3');
    }

    //mysql_close($connection);
    return $returnCreateUser;
}

function UpdateUser($query) {
    global $connection;
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
    // foreach ($query as $data) {
    $username = mysql_real_escape_string($query['UserName']);
    if (isset($query['Password'])) {
//        $password = md5(mysql_real_escape_string($query['Password']));
        $password = password_hash($query['Password'], PASSWORD_BCRYPT);
        $cleanpass = mysql_real_escape_string($query['Password']);
        $origpass = encrpyt_pass($cleanpass);
    } else {
        $password = null;
    }

    $roleID = $query['UserRoleID'];
    $firstname = mysql_real_escape_string($query['FirstName']);
    $lastname = mysql_real_escape_string($query['LastName']);
    $userdID = $query['UserID'];
    $loginID = $query['LoginID'];
    //  }

    if (!is_null($password)) {
        $sqlUpdateLogin = "UPDATE t_userlogin SET f_UserName = '{$username}', f_Password = '{$password}' , `f_UserRoleID` = '$roleID' WHERE f_UserLoginID = '{$loginID}' ";
    } else {

        $sqlUpdateLogin = "UPDATE t_userlogin SET f_UserName = '{$username}' , `f_UserRoleID` = '{$roleID}' WHERE f_UserLoginID = '{$loginID}' ";
    }

    $sqlCheckChange = "SELECT count(*) as existing FROM t_userlist WHERE f_UserFirstName = '{$firstname}' and f_UserLastName = '{$lastname}' AND f_UserID = '{$userdID}' ";
    $resultCheckChange = mysql_query($sqlCheckChange, $connection);
    $isExist = mysql_fetch_assoc($resultCheckChange);

    if ($isExist['existing'] == 0) {
        $sql = "UPDATE `t_userlist` SET `f_UserFirstName` = '$firstname', `f_UserLastName` = '$lastname' WHERE `f_UserID` = '{$userdID}'";
        $result = mysql_query($sql, $connection);
        if ($result && mysql_affected_rows() == 1) {
            $userListUpdated = 1;
            $logMessageArray = json_encode(array('UserID' => $userdID, 'Details' => array($firstname, $lastname)));
            InsertLog('3', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '3');
        } else {
            $logMessageArray = json_encode(array('Reason' => 'An error occured user', 'UserID' => $userdID, 'Details' => array($username, $password, $roleID)));
            InsertLog('3', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '3');
        }
    }

    $result2 = mysql_query($sqlUpdateLogin, $connection);
    if ($result2 && mysql_affected_rows() == 1) {
        $loginUpdate = 1;

        $sqlCheckRepo = "Select f_NewPass as 'CurrentPass',count(*) as 'isInserted' From t_passrepo where f_UserLoginID = '{$loginID}' limit 1";
        $resultCheckRepo = mysql_query($sqlCheckRepo, $connection);
        while ($rowCheckRepo = mysql_fetch_assoc($resultCheckRepo)) {
            $isInserted = $rowCheckRepo['isInserted'];
            $oldPass = $rowCheckRepo['CurrentPass'];
        }
        if ($isInserted >= 1) {
            $sqlRepoPass = "UPDATE t_passrepo SET f_NewPass = '{$origpass}', f_OldPass = '{$oldPass}' , f_DateTimeCreated = NOW() where f_UserLoginID = '{$loginID}'";
            mysql_query($sqlRepoPass, $connection);
        } else {
            $sqlRepoPass = "INSERT INTO `t_passrepo` (`f_UserLoginID`,`f_NewPass`) VALUES ('{$loginID}','{$origpass}')";
            mysql_query($sqlRepoPass, $connection);
        }

        $logMessageArray = json_encode(array('LoginID' => $loginID, 'Details' => array($username, $password, $roleID)));
        InsertLog('3', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '1');
    } else {
        $logMessageArray = json_encode(array('Reason' => $sqlUpdateLogin, 'LoginID' => $loginID, 'Details' => array($username, $password, $roleID)));
        InsertLog('3', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '1');
    }
    if ($loginUpdate == 1 || $userListUpdated == 1) {
        $returnUpdateUser['message'] = 'Update Success User';
    } else {
        $returnUpdateUser['message'] = 'Update Failed User';
    }

    // mysql_close($connection);
    return $returnUpdateUser;
}

function SelectUser($query) {
    global $connection;
    //require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
    $countQuery = count($query);

    $sql = "SELECT * FROM t_userlist a
            LEFT JOIN t_userlogin b on a.f_UserID = b.f_UserID
            LEFT JOIN t_userrole c on b.f_UserRoleID = c.f_UserRoleID
            WHERE 1=1 and a.f_Status = 1  ";
    if ($countQuery > 1) {

        $quotedString = "";
        foreach ($query as $data) {

            $quotedString .= "'$data',";
        }

        $whereIn = trim($quotedString, ",");

        $sql .= " AND a.f_UserID in ({$whereIn})";
    } else {
        if ($query == "Owner") {
            $sql .= " AND b.f_UserRoleID in ('3','2') ";
        } elseif ($query != "Owner" && $query != "ALL") {

            if (is_array($query)) {
                $sql .= " AND a.f_UserID = '{$query[0]}' ";
            } else {
                $sql .= " AND a.f_UserID = '{$query}' ";
            }
        }
    }

    $result = mysql_query($sql, $connection);
    while ($row = mysql_fetch_assoc($result)) {
        $returnSelectUser[] = $row;
    }

    //mysql_close($connection);
    return $returnSelectUser;
}

function DeleteUser($query) {
    global $connection;
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
    //var_dump($query);
    $sql = "UPDATE `t_userlist` set f_Status = 0
            WHERE f_UserID = '{$query}'";

    $result = mysql_query($sql, $connection);




    if ($result && mysql_affected_rows() == 1) {

        $storeList = array();
        $sqlGetDetails = "SELECT * FROM t_storelist WHERE f_StoreOwnerUserID = '{$query}'";
        $resultGetDetails = mysql_query($sqlGetDetails, $connection);
        while ($row = mysql_fetch_assoc($resultGetDetails)) {
            $details[] = $row;
            $storeList[$row['f_StoreID']][] = $row['f_StoreID'];
        }


        $logMessageArray = json_encode(array('Details' => $query));
        InsertLog('4', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '3');

        if (count($details) >= 1) {
            $sqlDeactiveStoreList = "UPDATE `t_storelist` SET f_Status = 0  WHERE f_StoreOwnerUserID = '{$query}'";
            $resutlDeactiveStoreList = mysql_query($sqlDeactiveStoreList, $connection);
            if ($resutlDeactiveStoreList && mysql_affected_rows() >= 1) {
                $logMessageArray = json_encode(array('Details' => $details));
                InsertLog('4', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '4');
            } else {
                $logMessageArray = json_encode(array('Reason' => 'An error occured', 'Details' => $details));
                InsertLog('4', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '4');
            }

            $sqlGetShared = "SELECT * FROM t_sharedstore WHERE f_ShareToUserID = '{$query}' OR IN('" . implode("','", $storeList) . "') OR f_ShearedToStoreID IN('" . implode("','", $storeList) . "')  ";
            $resultShared = mysql_query($sqlGetShared, $connection);
            while ($row = mysql_fetch_assoc($resultShared)) {
                $shareddetails[] = $row;
            }

            if (count($shareddetails) >= 1) {
                $sqlSharedDelete = "DELETE FROM `t_sharedstore` WHERE f_ShareToUserID = '{$query}' OR IN('" . implode("','", $storeList) . "')  OR f_ShearedToStoreID IN('" . implode("','", $storeList) . "') ";
                $resultSharedDelete = mysql_query($sqlSharedDelete, $connection);

                if ($resultSharedDelete >= 1) {
                    $logMessageArray = json_encode(array('Details' => $shareddetails));
                    InsertLog('4', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '7');
                } else {
                    $logMessageArray = json_encode(array('Reason' => 'An error occured', 'Details' => $shareddetails));
                    InsertLog('4', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '7');
                }
            }
        }

        $sqlDeactiveLogin = "UPDATE t_userlogin SET f_Status = 0 WHERE f_UserID = '{$query}'";
        $resultDeactveLogin = mysql_query($sqlDeactiveLogin, $connection);
        if ($resultDeactveLogin && mysql_affected_rows() >= 1) {
            $logMessageArray = json_encode(array('Details' => $query));
            InsertLog('4', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '1');
        } else {
            $logMessageArray = json_encode(array('Reason' => 'An error occured', 'Details' => $query));
            InsertLog('4', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '1');
        }
        //echo $sqlDeactiveStoreList ."<br>" . $sqlDeactiveStoreList;
        // $newSql = "SELECT count(*) from t_storelist Where f_StoreOwnerUserID = '{$query}'";


        $returnDeleteUser['message'] = "Deletion Success User";
    } else {
        $returnDeleteUser['message'] = "Deletion Failed User ";
        $logMessageArray = json_encode(array('Reason' => 'An error occured', 'Details' => $query));
        InsertLog('4', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '3');
    }

    //mysql_close($connection);
    return $returnDeleteUser;
}
