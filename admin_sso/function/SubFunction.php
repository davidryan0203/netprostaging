<?php

include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/crypter.php');
session_start();


if (isset($_GET['count']) && $_GET['count'] == "on") {
    $details['from'] = $_GET['from'];
    $details['to'] = $_GET['to'];
    $count['count'] = count(GetBlankPnumberList($details));
    echo json_encode($count);
}

//Get All Active Store List
function SelectStoreList($type = NULL) {
    global $connection;
    $sql = "SELECT * FROM t_storelist where f_Status = '1' ";

    if ($type == 'Own') {
        $role = $_SESSION['UserRoleID'];
        switch ($role) {
            case '1':
                $sql .= " ";
                break;
            case '2':
            case '3':
                $sql .= " AND f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
                break;
            case '4':
                $query = $_SESSION['EmpProfileID'];
                $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
                $result = mysql_query($sqlGetOwnerID, $connection);
                while ($row = mysql_fetch_assoc($result)) {
                    $data = $row['f_StoreOwnerUserID'];
                }
                $sql .= " AND f_StoreOwnerUserID = '{$data}' ";
                break;
            case '5':
                $query = $_SESSION['EmpProfileID'];
                $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
                $result = mysql_query($sqlGetOwnerID, $connection);
                while ($row = mysql_fetch_assoc($result)) {
                    $OwnerID = $row['f_StoreOwnerUserID'];
                }
                $sql .= " AND f_StoreOwnerUserID = '{$OwnerID}' ";
                break;
        }
    } else {
        $sql .= " ";
    }
    $result = mysql_query($sql, $connection);
    while ($row = mysql_fetch_assoc($result)) {

        $returnSelectStoreList[] = $row;
    }

    //mysql_close($connection);
    return $returnSelectStoreList;
}

//Get Store Type List
function SelectStoreTypeList() {
    global $connection;

    $sql = "SELECT * FROM t_storetype";
    $result = mysql_query($sql, $connection);
    while ($row = mysql_fetch_assoc($result)) {
        $returnSelectStoreList[] = $row;
    }

    //mysql_close($connection);
    return $returnSelectStoreList;
}

//Get User Role List
function SelectUserRoleList($type = NULL) {
    global $connection;

    $sql = "SELECT * FROM t_userrole";
    if ($type == 'ALL') {
        $sql .= " WHERE f_UserRoleID != '4' ";
    } elseif ($type == "UserRole") {
        $sql = $sql;
    } else {
        $sql .= " WHERE f_UserRoleID IN ('2','3') ";
    }

    $result = mysql_query($sql, $connection);
    while ($row = mysql_fetch_assoc($result)) {
        $returnSelectUserRoleList[] = $row;
    }

    //mysql_close($connection);
    return $returnSelectUserRoleList;
}

//Get Employee Role List
function SelectEmpRoleList() {
    global $connection;

    $sql = "SELECT * FROM t_emprole";
    $result = mysql_query($sql, $connection);
    while ($row = mysql_fetch_assoc($result)) {
        $returnSelectEmpRoleList[] = $row;
    }

    //mysql_close($connection);
    return $returnSelectEmpRoleList;
}

function SelectStoreInactive() {
    global $connection;


    $sql = "SELECT * FROM t_storelist where f_Status = '0'";
    $result = mysql_query($sql, $connection);
    while ($row = mysql_fetch_assoc($result)) {
        $returnSelectStoreInactive[] = $row;
    }

    //mysql_close($connection);
    return $returnSelectStoreInactive;
}

function SelectEmployeeInactive() {
    global $connection;


    $sql = "SELECT * FROM t_emplist where f_Status = '0'";
    $result = mysql_query($sql, $connection);
    while ($row = mysql_fetch_assoc($result)) {
        $returnSelectEmployeeInactive[] = $row;
    }

    //mysql_close($connection);
    return $returnSelectEmployeeInactive;
}

function SelectUserInactive() {
    global $connection;


    $sql = "SELECT * FROM t_userlist where f_Status = '0'";
    $result = mysql_query($sql, $connection);
    while ($row = mysql_fetch_assoc($result)) {
        $returnSelectUserInactive[] = $row;
    }

    //mysql_close($connection);
    return $returnSelectUserInactive;
}

function ChangePassword($details) {
    global $connection;
    session_start();
    $userID = $_SESSION['LoginID'];
    $cleannewpassword = mysql_escape_string($details['newPassword']);
    $currentPassword = mysql_escape_string($details['currentPassword']);
    // $hashed = md5($newpassword);
    $hashed = password_hash($cleannewpassword, PASSWORD_BCRYPT);
    $origpass = encrpyt_pass($currentPassword);
    $newpassword = encrpyt_pass($cleannewpassword);

    $sqlUpdatePassword = "UPDATE t_userlogin SET f_Password = '{$hashed}' WHERE f_UserLoginID = '{$userID}'";
    $result = mysql_query($sqlUpdatePassword, $connection);

    if ($result && mysql_affected_rows() == 1) {
        $sqlInsertRepo = "Insert into t_passrepo (f_UserLoginID,f_OldPass,f_NewPass) VALUES ('{$userID}','{$origpass}','{$newpassword}')";
        $resultRepo = mysql_query($sqlInsertRepo, $connection);

        if ($resultRepo && mysql_affected_rows() == 1) {
            $_SESSION['Redirect'] = false;
            $return['type'] = 'Success';
            $return['message'] = 'Successfully updated password :)';
        } else {
            $sqlUpdatePassword = "UPDATE t_userlogin SET f_Password = '{$origpass}' WHERE f_UserLoginID = '{$userID}'";
            $result = mysql_query($sqlUpdatePassword, $connection);
            $return['type'] = 'Failed';
            $return['message'] = 'Update password failed!';
        }
    } else {
        $return['type'] = 'Failed';
        $return['message'] = 'Update password failed ';
    }

    return $return;
}

function SelectStoreTypes() {
    global $connection;

    $role = $_SESSION['UserRoleID'];
    switch ($role) {
        case '1':
            $sql = " ";
            break;
        case '2':
        case '3':
            $sql = " AND f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
            break;
        case '4':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $data = $row['f_StoreOwnerUserID'];
            }
            $sql = " AND f_StoreOwnerUserID = '{$data}' ";
            break;
        case '5':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $OwnerID = $row['f_StoreOwnerUserID'];
            }
            $sql = " AND f_StoreOwnerUserID = '{$OwnerID}' ";
            break;
            
       switch ($role) {
        case '1':
            $sql = " ";
            break;
        case '2':
        case '3':
        case '4':
        case '5':            
                $sql = " AND f_StoreOwnerUserID = '{$_SESSION['ClientOwnerID']}' ";   
            break;
        case '6':
            $sqlShared = " AND f_StoreOwnerUserID = '{$profileID}' ";
            break;
    }        
            
            
            
    }

    $selectStoreTypes = "SELECT  CASE
                            WHEN count(distinct(f_StoreTypeID)) >= 2  THEN 'Both Stores'
                            WHEN f_StoreTypeID = 1 THEN 'Only TLS'
                            WHEN f_StoreTypeID = 2 THEN 'Only TBC'
                            WHEN f_StoreTypeID = 3 THEN 'Only Others'
                        END AS StoreTypes
                        FROM
                            t_userlist a
                                LEFT JOIN
                            t_storelist b ON a.f_UserID = b.f_StoreOwnerUserID
                        WHERE
                          1=1 $sql";

    $result = mysql_query($selectStoreTypes, $connection);
    while ($row = mysql_fetch_assoc($result)) {

        $SelectStoreTypes = $row;
    }

    return $SelectStoreTypes;
}

function ChangeEmail($details) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
    global $connection;
    session_start();
    $userID = $_SESSION['LoginID'];
    $newUserName = mysql_escape_string($details['newUserName']);

    $sqlUpdatePassword = "UPDATE t_userlogin SET f_UserName = '{$newUserName}' WHERE f_UserLoginID = '{$userID}'";
    $result = mysql_query($sqlUpdatePassword, $connection);

    if ($result && mysql_affected_rows() == 1) {
        $_SESSION['ChangeEmail'] = false;
        $return['type'] = 'Success';
        $return['message'] = 'Successfully updated username :)';
        InsertLog('3', '1', $return['message'], $_SESSION['LoginID'], 'Success', '1');
    } else {
        $return['type'] = 'Failed';
        $return['message'] = 'Update username failed ';
        InsertLog('3', '1', $return['message'], $_SESSION['LoginID'], 'Failed', '1');
    }

    return $return;
}

function ChangePassFromForget($details) {
    global $connection;
    //print_r($details);
    // $sqlUpdatePassword = '';
    $cleannewpassword = mysql_escape_string($details['newPassword']);
    $userName = mysql_escape_string($details['email']);

    $hashed = password_hash($cleannewpassword, PASSWORD_BCRYPT);
    $newpassword = encrpyt_pass($cleannewpassword);

    $sqlUpdatePassword = "UPDATE t_userlogin SET f_Password = '{$hashed}' , f_forgot_token = NULL , f_forgot_token_date = NULL WHERE f_UserName = '{$userName}'";
    $result = mysql_query($sqlUpdatePassword, $connection);


    if ($result && mysql_affected_rows() == 1) {

        $getID = "SELECT f_UserLoginID FROM t_userlogin where f_UserName = '{$userName}'";
        $IDresult = mysql_query($getID, $conenction);
        $idData = mysql_fetch_array($IDresult);
        $userID = $idData['f_UserLoginID'];
        // echo $getID;
        // print_r($idData); // $getID;
        $sqlInsertRepo = "Insert into t_passrepo (f_UserLoginID,f_NewPass) VALUES ('{$userID}','{$newpassword}')";
        $resultRepo = mysql_query($sqlInsertRepo, $connection);

        if ($resultRepo && mysql_affected_rows() == 1) {
            // $_SESSION['Redirect'] = false;
            $return['type'] = 'Success';
            $return['message'] = 'Successfully updated password!';
        } else {
            $return['type'] = 'Failed';
            $return['message'] = 'Update password failed!';
        }
    } else {
        $return['type'] = 'Failed';
        $return['message'] = 'Update password failed ';
    }

    return $return;
}

function GetBlankPnumberList($details = NULL) {
    //print_r($details);
    global $connection;

    if (is_null($details['to']) || empty($details['to'])) {
        $to = date('Y-m-t'); // hard-coded '01' for first day
    } else {
        $to = date("Y-m-d", strtotime($details['to']));
    }

    if (is_null($details['from']) || empty($details['from'])) {
        $from = date('Y-m-01'); // hard-coded '01' for first day
    } else {
        $from = date("Y-m-d", strtotime($details['from']));
    }


    if (is_null($details['storeOwnerID']) || empty($details['storeOwnerID'])) {
        $role = $_SESSION['UserRoleID'];
        switch ($role) {
            case '1':
                $sql = " ";
                break;
            case '2':
            case '3':
                $storeOwnerID = $_SESSION['UserProfileID'];
                break;
            case '4':
                $query = $_SESSION['EmpProfileID'];
                $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
                $result = mysql_query($sqlGetOwnerID, $connection);
                while ($row = mysql_fetch_assoc($result)) {
                    $storeOwnerID = $row['f_StoreOwnerUserID'];
                }

                break;
            case '5':
                $query = $_SESSION['EmpProfileID'];
                $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
                $result = mysql_query($sqlGetOwnerID, $connection);
                while ($row = mysql_fetch_assoc($result)) {
                    $storeOwnerID = $row['f_StoreOwnerUserID'];
                }

                break;
        }
    } else {
        $storeOwnerID = mysql_real_escape_string($details['storeOwnerID']);
    }

    // $from = date("Y-m-d", strtotime($details['from']));


    $last_day_this_month = date('t/m/Y');
    $storeIDsql = " AND d.f_StoreOwnerUserID = '{$storeOwnerID}' ";
    $storeEmpOwnerID = " AND b.f_StoreOwnerUserID = '{$storeOwnerID}' ";


    mysql_query("SELECT 1=1", $connection);
    $sqlGetBlankPNumber = "SELECT
    a.f_SurveyPDID,
    b.f_SurveySDID,
    a.f_InvitationID,
    a.f_EpisodeReferenceNo,
    b.f_StoreID,
    a.f_CustomerDetailsID,
    DATE_FORMAT(b.f_DateTimeResponseMelbourne,
            '%d/%m/%Y') AS DateTime,
    b.f_ActivationExtra5,
    CONCAT(c.f_CustomerFirstName,
            ' ',
            a.f_CustomerBillingAccountID) AS CustomerName,
       d.f_StoreListName,
    d.f_StoreOwnerUserID,a.f_EpisodeReferenceNo as EpisodeNum,
    b.f_InteractionLog
FROM
    t_surveypd a
        LEFT JOIN
    t_surveysd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN
    t_customerdetails c ON a.f_CustomerDetailsID = c.f_CustomerDetailsID
        LEFT JOIN
    t_storelist d ON b.f_StoreID = d.f_StoreID
WHERE
    b.f_ActivationExtra5 NOT IN (SELECT
            f_EmpPNumber
        FROM
            t_empnumber a
                LEFT JOIN
            t_storelist b ON a.f_StoreID = b.f_StoreID
        WHERE
          1 = 1  $storeEmpOwnerID  AND b.f_Status = 1 ) AND d.f_Status = 1 AND b.f_DateTimeResponseMelbourne
       BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' $storeIDsql ";
    $result = mysql_query($sqlGetBlankPNumber, $connection);
    while ($row = mysql_fetch_assoc($result)) {
        $data[] = $row;
    }
    //$data['query'] = $sqlGetBlankPNumber;
    return $data;
}

function CreateMedallia($query) {
    global $connection;
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/toolcrypter.php');

    $data = array();

    $userName = mysql_real_escape_string($query['UserName']);
    $password = encrpyt_medallia(mysql_real_escape_string($query['Password']));
    $var = mysql_real_escape_string($query['expire']);
    $date = str_replace('/', '-', $var);
    $expiration = date('Y-m-d', strtotime($date));
    $storeOwnerID = mysql_real_escape_string($query['StoreOwnerID']);

    $sql = "INSERT INTO `t_medalliadetails`
            (`f_StoreOwnerUserID`,`f_MedalliaUserName`,`f_MedalliaPassword`,`f_PasswordExpiration`)
            VALUES
            ('$storeOwnerID','$userName','$password','$expiration')";


    $result = mysql_query($sql, $connection);

    if ($result && mysql_affected_rows() >= 1) {
        $returnCreateStore['message'] = "Success adding Medallia account";
        $returnCreateStore['type'] = "Success";
        $returnCreateStore['StoreID'] = mysql_insert_id();
        $returnCreateStore['sql'] = $sql;
    } else {
        $returnCreateStore['message'] = "Failed adding Medallia account";
    }
    // echo $sql;
    //mysql_close($connection);
    return $returnCreateStore;
}

function checkPrivacyStore() {
    global $connection;

    $role = $_SESSION['UserRoleID'];
    switch ($role) {
        case '1':
            $sql = " ";
            break;
        case '2':
        case '3':
            $sql = " AND f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
            break;
        case '4':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $data = $row['f_StoreOwnerUserID'];
            }
            $sql = " AND f_StoreOwnerUserID = '{$data}' ";
            break;
        case '5':
            $query = $_SESSION['EmpProfileID'];
            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
        left join t_empnumber b on a.f_EmpID = b.f_EmpID
        left join t_storelist c on b.f_StoreID = c.f_StoreID
        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
            $result = mysql_query($sqlGetOwnerID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $OwnerID = $row['f_StoreOwnerUserID'];
            }
            $sql = " AND f_StoreOwnerUserID = '{$OwnerID}' ";
            break;
    }

    $sqlPrivate = "SELECT count(*) as 'WithPrivate' FROM t_privateowner WHERE 1=1 {$sql} ";
    $result = mysql_query($sqlPrivate, $connection);
    while ($row = mysql_fetch_assoc($result)) {
        $isPrivate = $row['WithPrivate'];
    }


    if ($isPrivate > 0) {
        $return['isPrivate'] = true;
    } else {
        $return['isPrivate'] = false;
    }

    return $return;
}


function checkClientList(){
    global $connection;
    session_start();
    
    $userLoginID = $_SESSION['LoginID'];
    $userLoginIDList = array();
    $sqlClientList = "SELECT 
        d.f_UserLoginID
    FROM
        t_clientlist a
            LEFT JOIN
        t_storelist c ON a.f_ClientID = c.f_ClientID
            LEFT JOIN
        t_empnumber d ON c.f_StoreID = d.f_StoreID
    WHERE
        d.f_UserLoginID IS NOT NULL
    GROUP BY d.f_UserLoginID";
    $resultClient = mysql_query($sqlClientList ,$connection);
    while($row = mysql_fetch_assoc($resultClient)){
        $userLoginIDList[] = $row['f_UserLoginID'];
    }
    
    $sqlGetClient = "SELECT 
        a.f_UserLoginID
    FROM
        t_clientlist a";
    $resultGetClient= mysql_query($sqlGetClient ,$connection);
    while($row = mysql_fetch_assoc($resultGetClient)){
        $userLoginIDList[] = $row['f_UserLoginID'];
    }
    
    if(in_array($userLoginID, $userLoginIDList)){
        $msg = true;
    }else{
        $msg = false;
    }
    
    $return = array('message' => $msg , 'data' => $userLoginIDList);
       
    return $return;
}