<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');

session_start();

function CreateStore($query) {
    global $connection;
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');

    $data = array();
    //var_dump($query);
    // foreach ($query as $data) {

    $storelistname = mysql_real_escape_string($query['StoreListName']);
    $storeownerID = mysql_real_escape_string($query['StoreOwnerID']);
    $companyCode = mysql_real_escape_string($query['StoreCode']);
    $storetypeid = mysql_real_escape_string($query['StoreTypeID']);
    if (isset($query['showonreports'])) {
        $showonreports = 1;
    } else {
        $showonreports = 0;
    }

    // }

    $sql = "INSERT INTO `t_storelist`
            (`f_StoreListName`,`f_StoreOwnerUserID` , `f_CompanyCode`,`f_StoreTypeID`,`f_ShowOnReports`)
            VALUES
            ('$storelistname','$storeownerID','$companyCode','$storetypeid',$showonreports)";

    $result = mysql_query($sql, $connection);

    if ($result && mysql_affected_rows() >= 1) {
        $returnCreateStore['message'] = "Success Creating Store";
        $returnCreateStore['StoreID'] = mysql_insert_id();
        $logMessageArray = json_encode(array('StoreID' => $returnCreateStore['StoreID'],
            'Details' => array($storelistname, $storeownerID, $companyCode, $storetypeid, $showonreports)));
        InsertLog('2', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '4');
    } else {
        $returnCreateStore['message'] = "Failed Creating Store";

        $logMessageArray = json_encode(array('Reason' => 'An error occured',
            'Details' => array($storelistname, $storeownerID, $companyCode, $storetypeid, $showonreports)));
        InsertLog('2', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '4');
    }
    // echo $sql;
    //mysql_close($connection);
    return $returnCreateStore;
}

function SelectStore($type = NULL, $query = NULL) {
    global $connection;


    //var_dump($query);
//    $sql = "SELECT a.f_StoreID,a.f_StoreListName, b.f_SharedStoreID, CONCAT(c.f_UserFirstName,' ',c.f_UserLastName) as 'SharedTo',
//                    b.f_SharedOwnerUserID,CONCAT(d.f_UserFirstName,' ',d.f_UserLastName) as 'ShareBy',b.f_ShareToUserID,a.f_CompanyCode,a.f_StoreOwnerUserID
//                    FROM t_storelist a
//                    LEFT JOIN t_sharedstore b on a.f_StoreID = b.f_StoreID
//                    LEFT JOIN t_userlist c on b.f_ShareToUserID = c.f_UserID
//                    LEFT JOIN t_userlist d on a.f_StoreOwnerUserID = d.f_UserID
//                    WHERE 1=1 ";
    // var_dump($query);
    $sql = "SELECT a.f_StoreID as 'SharedStoreID',a.f_StoreListName as 'SharedStoreName',a.f_StoreOwnerUserID,a.f_CompanyCode as 'SharedCompanyCode',
            b.f_SharedStoreID,b.f_SharedToStoreID,b.f_ShareToUserID,
            c.f_StoreListName as 'SharedToStoreName', c.f_CompanyCode as 'SharedToCompanyCode',
            d.f_UserID as 'SharedByUserID',
            e.f_UserID as 'SharedToUserID', e.f_UserName as 'SharedToUserName' ,  a.f_ShowOnReports+0 as f_ShowOnReports , f.f_StoreTypeName , f.f_StoreTypeID,
            CASE
                    WHEN g.f_SharedWithUserID IS NOT NULL
              THEN CONCAT(d.f_UserName, ' / ' , h.f_UserName)
              ELSE d.f_UserName
              END as 'SharedByUserName'
            FROM t_storelist a
            LEFT JOIN t_sharedstore b on a.f_StoreID = b.f_StoreID
            LEFT JOIN t_storelist c on b.f_SharedToStoreID = c.f_StoreID
            LEFT JOIN t_userlogin d on a.f_StoreOwnerUserID = d.f_UserID
            LEFT JOIN t_userlogin e on b.f_ShareToUserID = e.f_UserID
            LEFT JOIN t_storetype f on a.f_StoreTypeID = f.f_StoreTypeID
            LEFT JOIN t_sharedownership g on d.f_UserID = g.f_UserID
            LEFT JOIN t_userlogin h on g.f_SharedWithUserID = h.f_UserID
            WHERE 1=1 AND a.f_Status = 1  ";
    //  d.f_UserName as 'SharedByUserName',
    switch ($type) {
        case "GlobalAdmin":
            $sql .= "";
            break;
        case "StoreOwner":
            // $sql .= " and (e.f_UserID  = '{$query}' or d.f_UserID = '$query')";
            $sql .= " AND a.f_StoreOwnerUserID = '{$query}'";
            break;
        case "StoreAdmin":
            //get partners
            $sqlgetPartners = "select f_UserID from t_userlogin Where f_UserRoleID = '2'";
            $result = mysql_query($sqlgetPartners, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $storePartners[] = $row['f_UserID'];
            }
            foreach ($storePartners as $data) {
                $quotedString .= "'$data',";
            }
            //$quotedString .= "'$query'";
            //  echo $sqlgetPartners."<br>---";
            $quotedString = trim($quotedString, ",");
            // var_dump($storePartners);
            $sql .= " AND a.f_StoreOwnerUserID IN ({$quotedString})";
            break;
        case "StoreLeader":
            $sqlStoreID = "SELECT f_StoreID from t_empnumber WHERE f_EmpID = '{$query}'";
            $result = mysql_query($sqlStoreID, $connection);
            while ($row = mysql_fetch_assoc($result)) {
                $stores[] = $row['f_StoreID'];
            }
            foreach ($stores as $data) {
                $quotedString .= "'$data',";
            }
            $whereIn = trim($quotedString, ",");
            $sql .= " AND a.f_StoreID in ({$whereIn})";
            break;
        case "GetSharedList":
            $sql .= "AND a.f_StoreID = '{$query}'";
            break;
        default;
            $sql = "SELECT a.f_StoreID as 'SharedStoreID',a.f_StoreListName as 'SharedStoreName',a.f_StoreOwnerUserID,a.f_CompanyCode as 'SharedCompanyCode',
            b.f_SharedStoreID,b.f_SharedToStoreID,b.f_ShareToUserID,
            c.f_StoreListName as 'SharedToStoreName', c.f_CompanyCode as 'SharedToCompanyCode',
            d.f_UserID as 'SharedByUserID',
            e.f_UserID as 'SharedToUserID', e.f_UserName as 'SharedToUserName' ,  a.f_ShowOnReports+0 , f.f_StoreTypeName , f.f_StoreTypeID,
              CASE
                    WHEN g.f_SharedWithUserID IS NOT NULL
              THEN CONCAT(d.f_UserName, ' / ' , h.f_UserName)
              ELSE d.f_UserName
              END as 'SharedByUserName'
            FROM t_storelist a
            LEFT JOIN t_sharedstore b on a.f_StoreID = b.f_StoreID
            LEFT JOIN t_storelist c on b.f_SharedToStoreID = c.f_StoreID
            LEFT JOIN t_userlogin d on a.f_StoreOwnerUserID = d.f_UserID
            LEFT JOIN t_userlogin e on b.f_ShareToUserID = e.f_UserID
            LEFT JOIN t_storetype f on a.f_StoreTypeID = f.f_StoreTypeID
            LEFT JOIN t_sharedownership g on d.f_UserID = g.f_UserID
            LEFT JOIN t_userlogin h on g.f_SharedWithUserID = h.f_UserID
            WHERE 1=1 AND a.f_Status = 1";
        //d.f_UserName as 'SharedByUserName',
    }
    mysql_query("SET SQL_BIG_SELECTS=1");
    $result = mysql_query($sql, $connection);
    while ($row = mysql_fetch_assoc($result)) {
        $returnSelectStore[] = $row;
    }
    // echo $sql;
    // var_dump($returnSelectStore);
    //mysql_close($connection);
    return $returnSelectStore;
}

function UpdateStore($query) {
    global $connection;
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');

    // var_dump($query);
    // foreach ($query as $data) {
    $storelistID = mysql_real_escape_string($query['StoreListID']);
    $storelistname = mysql_real_escape_string($query['StoreListName']);
    $storeownerID = mysql_real_escape_string($query['StoreOwnerID']);
    $storecode = mysql_real_escape_string($query['StoreCode']);
    $storetypeid = mysql_real_escape_string($query['StoreTypeID']);
    // } b
    if (isset($query['showonreports'])) {
        $showonreports = 1;
    } else {
        $showonreports = 0;
    }

    $sqlcurret = "SELECT * FROM t_storelist WHERE f_StoreID = '{$storelistID}'";
    $resultcurrent = mysql_query($sqlcurret, $connection);
    $currentData = mysql_fetch_assoc($resultcurrent);

    $sql = "UPDATE `t_storelist` SET `f_StoreListName` = '{$storelistname}', `f_StoreOwnerUserID` = '{$storeownerID}' , `f_CompanyCode` = '{$storecode}' ,`f_StoreTypeID` = '{$storetypeid}', `f_ShowOnReports` = {$showonreports} WHERE `f_StoreID` = '{$storelistID}'";
    // echo $sql;
    $result = mysql_query($sql, $connection);
    if ($result && mysql_affected_rows() >= 1) {

        $logMessageArray = json_encode(array('StoreID' => $storelistID,
            'Details' => array($storelistname, $storeownerID, $storelistname, $storetypeid, $showonreports)));
        InsertLog('3', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '4');

        if ($currentData['f_StoreOwnerUserID'] != $storeownerID) {
            $sqlSharedStore = "UPDATE t_sharedstore SET f_SharedOwnerUserID = '{$storeownerID}' WHERE f_StoreID = '{$storelistID}'";
            $resultSharedStore = mysql_query($sqlSharedStore, $connection);

            if ($resultSharedStore && mysql_affected_rows() >= 1) {
                $logMessageArray = json_encode(array('StoreID' => $storelistID,
                    'Details' => array($storeownerID)));
                InsertLog('3', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '7');
            } else {
                $logMessageArray = json_encode(array('StoreID' => $storelistID,
                    'Details' => array($storeownerID)));
                InsertLog('3', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '7');
            }
        }
        //echo mysql_error();
        $returnUpdateUser['message'] = "Success Updating Store";
    } else {
        $returnUpdateUser['message'] = "Failed Updating Store";
        $logMessageArray = json_encode(array('Reason' => 'An error occured',
            'Details' => array($storelistname, $storeownerID, $storelistname, $storetypeid, $showonreports)));
        InsertLog('3', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '4');
    }

    //mysql_close($connection);
    return $returnUpdateUser;
}

function DeleteStore($query) {
    global $connection;
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
    $sqlGetDetails = "SELECT * FROM t_storelist WHERE f_StoreID = '{$query}'";
    $resultGetDetails = mysql_query($sqlGetDetails, $connection);
    while ($row = mysql_fetch_assoc($resultGetDetails)) {
        $details[] = $row;
    }

    $sql = "UPDATE `t_storelist` SET f_Status = 0
            WHERE f_StoreID = '{$query}'";

    $result = mysql_query($sql, $connection);
    if ($result && mysql_affected_rows() >= 1) {
        $returnDeleteStore['message'] = "Success Deleting Store";
        $logMessageArray = json_encode(array('Details' => $details));
        InsertLog('4', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '4');
    } else {
        $returnDeleteStore['message'] = "Deletion Failed Store";
        $logMessageArray = json_encode(array('Reason' => 'An error occured', 'Details' => $query));
        InsertLog('4', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '4');
    }

    // mysql_close($connection);
    return $returnDeleteStore;
}

function SelectShareStore($query) {
    global $connection;

    $countQuery = count($query);
    if ($countQuery > 1) {
        $whereIn = "";
        foreach ($query as $data) {
            $whereIn .= "'$data',";
        }
        $whereIn = trim($whereIn, ",");
        $sql = "SELECT * FROM t_sharestore WHERE f_SharedStoreID in ('{$whereIn}')";
    } else {
        if ($data == "ALL") {
            $sql = "SELECT * FROM t_sharestore";
        } else {
            $sql = "SELECT * FROM t_sharestore WHERE f_SharedStoreID = '{$data[0]}'";
        }
    }
    $result = mysql_query($sql, $connection);
    while ($row = mysql_fetch_assoc($result)) {
        $returnSelectShareStore[] = $row;
    }

    mysql_close($connection);
    return $returnSelectShareStore;
}

function CreateSharedStore($query) {
    global $connection;
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
    $queryarray = array_combine($query['storeShareTo'], $query['shareStoreOwnerID']);
    //  var_dump($query);
    // echo "===============";
    //  var_dump($queryarray);
    $i = 0;
    foreach ($queryarray as $shareToID => $ownerID) {
        $test[$i] = "'{$query['storeID']}','{$shareToID}','{$ownerID}'";
        $i++;
    }
    $sql = "INSERT INTO t_sharedstore (`f_StoreID`,`f_SharedToStoreID`,`f_ShareToUserID`) VALUES ";
    foreach ($test as $data) {
        $sql .= "(" . $data . "),";
    }
    $sql = trim($sql, ",");
    //  echo $sql;
    //exit;
    $result = mysql_query($sql, $connection);

    if ($result && mysql_affected_rows() >= 1) {
        $logMessageArray = json_encode(array('StoreID' => $query['storeID'], 'Details' => $queryarray));
        InsertLog('2', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '7');
        $returnCreateSharedStore['message'] = "Success Creating Shared Store";
    } else {
        $returnCreateSharedStore['message'] = "Failed Creating Shared Store: ";
        $logMessageArray = json_encode(array('Reason' => 'An error occurred', 'Details' => $queryarray));
        InsertLog('2', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '7');
    }

    // mysql_close($connection);
    return $returnCreateSharedStore;
}

function UpdateSharedStore($query) {
    global $connection;
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
    foreach ($query as $data) {
        $storeID = mysql_real_escape_string($data['StoreID']);
        //   $companycode = mysql_real_escape_string($data['CompanyCode']);
        $storeownerID = mysql_real_escape_string($data['StoreOwnerUserID']);
        $sharedtoID = mysql_real_escape_string($data['SharedToUserID']);
        $sharedstoreID = mysql_real_escape_string($data['SharedStoreID']);
    }

//    $sqlcurret = "SELECT * FROM t_storelist WHERE f_StoreID = '{$storelistID}'";
//    $resultcurrent = mysql_query($sqlcurret, $connection);
//    $currentData = mysql_fetch_assoc($resultcurrent);

    $sql = "UPDATE `t_sharedstore` SET `f_StoreID` = '{$storeID}',`f_ShareToUserID` = '{$sharedtoID}',`f_SharedOwnerUserID` = '{$storeownerID}'
            WHERE `f_SharedStoreID` = '{$sharedstoreID}'";

    $result = mysql_query($sql, $connection);
    if ($result && mysql_affected_rows() >= 1) {
        $returnUpdateUser = "Success Updating Shared Store";
        $logMessageArray = json_encode(array('SharedStoreID' => $sharedstoreID, 'Details' => array($storeID, $sharedtoID, $storeownerID)));
        InsertLog('3', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '7');
    } else {
        $returnUpdateUser = "Failed Updating Shared Store";
        $logMessageArray = json_encode(array('Reason' => 'An error occured', 'Details' => array($storeID, $sharedtoID, $storeownerID)));
        InsertLog('3', '1', $logMessageArray, $_SESSION['LoginID'], 'failed', '7');
    }

    mysql_close($connection);
    return $returnUpdateUser;
}

function DeleteSharedStore($query, $type) {
    global $connection;
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
    if ($type == 'WhereIn') {
        $sql = "Delete FROM `t_sharedstore` WHERE f_SharedStoreID IN ({$query})";
        $sqlGetDetails = "SELECT * FROM t_sharedstore WHERE f_SharedStoreID IN ({$query})";
    } else if ($type == 'Single') {
        $sql = "Delete FROM `t_sharedstore` WHERE f_SharedStoreID = '{$query[0]}'";
        $sqlGetDetails = "SELECT * FROM t_sharedstore WHERE f_SharedStoreID = '{$query[0]}'";
    }

    $resultGetDetails = mysql_query($sqlGetDetails, $connection);
    while ($row = mysql_fetch_assoc($resultGetDetails)) {
        $details[] = $row;
    }

    $result = mysql_query($sql, $connection);
    if ($result && mysql_affected_rows() >= 1) {
        $returnDeleteSharedStore['message'] = "Success Deleting Shared Store";
        $logMessageArray = json_encode(array('Details' => $details));
        InsertLog('4', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '7');
    } else {
        $returnDeleteSharedStore['message'] = "Deletion Failed  Shared Store";
        $logMessageArray = json_encode(array('Reason' => 'An error occured', 'Details' => $query));
        InsertLog('4', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '7');
    }

    // mysql_close($connection);
    return $returnDeleteSharedStore;
}

function EditSharedStore($query) {
    global $connection;
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
    $newSharedStoreID = "(";
    foreach ($query as $toSearch) {
        $newSharedStoreID .= "'" . $toSearch['NewSharedToStoreID'] . "',";
    }
    $newSharedStoreID = trim($newSharedStoreID, ",");
    $newSharedStoreID .= ")";

    $sqlGetOwner = "SELECT f_StoreID,f_StoreOwnerUserID FROM t_storelist WHERE f_StoreID IN $newSharedStoreID";
    $resultGetOwner = mysql_query($sqlGetOwner, $connection);
    while ($rowGetOwner = mysql_fetch_assoc($resultGetOwner)) {
        $dataGetOwner[$rowGetOwner['f_StoreID']] = $rowGetOwner['f_StoreOwnerUserID'];
    }

    $sqlUpdateShared = "";
    $updateCount = 0;
    foreach ($query as $toUpdate) {

        $sqlUpdateShared = "UPDATE t_sharedstore SET f_SharedToStoreID = '{$toUpdate['NewSharedToStoreID']}', f_ShareToUserID = '{$dataGetOwner[$toUpdate['NewSharedToStoreID']]}'"
                . " WHERE f_SharedStoreID = '{$toUpdate['SharedStoreID']}'; ";
        $result = mysql_query($sqlUpdateShared, $connection);
        // echo mysql_affected_rows();
        if ($result && mysql_affected_rows() >= 1) {
            $updateCount = + mysql_affected_rows();
            $logMessageArray = json_encode(array('SharedStoreID' => $toUpdate['SharedStoreID'], 'Details' => array($toUpdate['NewSharedToStoreID'], $dataGetOwner[$toUpdate['NewSharedToStoreID']])));
            InsertLog('3', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '7');
        } else {
            $logMessageArray = json_encode(array('Reason' => 'An error occured', 'SharedStoreID' => $toUpdate['SharedStoreID'], 'Details' => array($toUpdate['NewSharedToStoreID'], $dataGetOwner[$toUpdate['NewSharedToStoreID']])));
            InsertLog('3', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '7');
        }
    }


    if ($updateCount >= 1) {
        $returnAddSharedStore['message'] = "Success Updating Shared Store";
    } else {
        $returnAddSharedStore['message'] = "Failed Updating Shared Store";
    }
    return $returnAddSharedStore;
}

function SelectMergeStore($query = NULL) {
    global $connection;

    $sqlExtend = "";
    if (!is_null($query)) {
        $sqlExtend = " AND a.f_StoreID = '{$query}' ";
    }


    $sqlSelectMergeStore = "SELECT
                a.f_StoreID,a.f_StoreListName, a.f_CompanyCode, b.f_MergeToCompanyCode,b.f_MergeReportID
            FROM
                t_storelist a
                    LEFT JOIN
                t_mergereport b ON a.f_StoreID = b.f_StoreID
            WHERE
                a.f_Status = 1 AND a.f_ShowOnReports = 1 {$sqlExtend} ";

    // echo $sqlSelectMergeStore;
    $result = mysql_query($sqlSelectMergeStore, $connection);
    while ($row = mysql_fetch_assoc($result)) {
        $returnSelectMergeStore[] = $row;
    }

    //mysql_close($connection);
    return $returnSelectMergeStore;
}

function CreateMergeStore($query) {
    global $connection;
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');

    $i = 0;
    foreach ($query['MergeWith'] as $tomerge) {
        $test[$i] = "'{$query['storeID']}','{$query['companycode']}' ,'{$tomerge}'";
        $i++;
    }
    $sql = "INSERT INTO `t_mergereport` (`f_StoreID`,`f_CompanyCode`,`f_MergeToCompanyCode`) VALUES ";
    foreach ($test as $data) {
        $sql .= "(" . $data . "),";
    }
    $sql = trim($sql, ",");

    $result = mysql_query($sql, $connection);
    //InsertLog($logType, $logFrom, $logRemarks, $logBy, $logStatus, $logFromSub);
    if ($result && mysql_affected_rows() >= 1) {
        $logMessageArray = json_encode(array('Details' => $query));
        InsertLog('2', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '8');
        $returnCreateMergeStore['message'] = "Success Creating Merge Store";
    } else {
        $returnCreateMergeStore['message'] = "Failed Creating Merge Store: ";
        $logMessageArray = json_encode(array('Reason' => 'An error occurred', 'Details' => $query));
        InsertLog('2', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '8');
    }
    return $returnCreateMergeStore;
}

function EditMergeStore($query) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
    global $connection;
    // echo "<pre>";print_r($query);
    // exit;
    foreach ($query as $ToUpdate) {
        $sqlEditMergeStore = "UPDATE t_mergereport SET f_MergeToCompanyCode = '{$ToUpdate['NewCompanyMerge']}' WHERE f_MergeReportID = '{$ToUpdate['toEdit']}' ";
        $result = mysql_query($sqlEditMergeStore, $connection);
        // echo $sqlEditMergeStore."<br>";
        if ($result && mysql_affected_rows() >= 1) {
            $updateCount = + mysql_affected_rows();
            $logMessageArray = json_encode(array('Details' => array('NewCompanyMerge' => $ToUpdate['NewCompanyMerge'], 'toEdit' => $ToUpdate['toEdit'])));
            InsertLog('3', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '8');
        } else {
            $logMessageArray = json_encode(array('Reason' => 'An error occured', 'Details' => array('NewCompanyMerge' => $ToUpdate['NewCompanyMerge'], 'toEdit' => $ToUpdate['toEdit'])));
            InsertLog('3', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '8');
        }
    }

    if ($updateCount >= 1) {
        $returnEditMergeStore['message'] = "Success Updating Merge Store";
    } else {
        $returnEditMergeStore['message'] = "Failed Updating Merge Store";
    }


    return $returnEditMergeStore;
}

function DeleteMergeStore($query, $type) {
    global $connection;
    require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');

    if ($type == 'WhereIn') {
        $sql = "Delete FROM `t_mergereport` WHERE f_MergeReportID IN ({$query})";
        $sqlGetDetails = "SELECT * FROM t_sharedstore WHERE f_MergeReportID IN ({$query})";
    } else if ($type == 'Single') {
        $sql = "Delete FROM `t_mergereport` WHERE f_MergeReportID = '{$query[0]}'";
        $sqlGetDetails = "SELECT * FROM t_sharedstore WHERE f_MergeReportID = '{$query[0]}'";
    }

    $resultGetDetails = mysql_query($sqlGetDetails, $connection);
    while ($row = mysql_fetch_assoc($resultGetDetails)) {
        $details[] = $row;
    }

    $result = mysql_query($sql, $connection);
    if ($result && mysql_affected_rows() >= 1) {
        $returnDeleteMergeStore['message'] = "Success Deleting Merge Store";
        $logMessageArray = json_encode(array('Details' => $details));
        InsertLog('4', '1', $logMessageArray, $_SESSION['LoginID'], 'Success', '7');
    } else {
        $returnDeleteMergeStore['message'] = "Deletion Failed  Merge Store";
        $logMessageArray = json_encode(array('Reason' => 'An error occured', 'Details' => $query));
        InsertLog('4', '1', $logMessageArray, $_SESSION['LoginID'], 'Failed', '7');
    }
    return $returnDeleteMergeStore;
}
