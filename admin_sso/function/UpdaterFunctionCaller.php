<?php

include('UpdaterFunction.php');

$getData = array();
$getData = $_GET; // get data from ajax
$toDo = $_GET['module'];

switch ($toDo) {
    case 'AddStoreType':
        $resultAddStoreType = AddStoreType($getData);
        if ($resultAddStoreType['message'] == 'Succesfuly Added Store Type.') {
            $return['type'] = 'Success';
            $return['message'] = $resultAddStoreType['message'];
        } else {
            $return['type'] = 'Failed';
            $return['message'] = $resultAddStoreType['message'];
        }
        break;
    case 'AddUserRole':
        $resultAddUserRole = AddUserRole($getData);
        if ($resultAddUserRole['message'] == 'Succesfuly Added User Role.') {
            $return['type'] = 'Success';
            $return['message'] = $resultAddUserRole['message'];
        } else {
            $return['type'] = 'Failed';
            $return['message'] = $resultAddUserRole['message'];
        }
        break;
    case 'AddEmpRole':
        $resultAddEmpRole = AddEmpRole($getData);
        if ($resultAddEmpRole['message'] == 'Succesfuly Employee  Role.') {
            $return['type'] = 'Success';
            $return['message'] = $resultAddEmpRole['message'];
        } else {
            $return['type'] = 'Failed';
            $return['message'] = $resultAddEmpRole['message'];
        }
        break;
    case 'DeleteStoreType':
        $resultDeleteStoreType = DeleteStoreType($getData);
        if ($resultDeleteStoreType['message'] == 'Successfuly Deleted Store Type.') {
            $return['type'] = 'Success';
            $return['message'] = $resultDeleteStoreType['message'];
        } else {
            $return['type'] = 'Failed';
            $return['message'] = $resultDeleteStoreType['message'];
        }
        break;
    case 'DeleteUserRole':
        $resultDeleteUserRole = DeleteUserRole($getData);
        if ($resultDeleteUserRole['message'] == 'Successfuly Deleted User Role.') {
            $return['type'] = 'Success';
            $return['message'] = $resultDeleteUserRole['message'];
        } else {
            $return['type'] = 'Failed';
            $return['message'] = $resultDeleteUserRole['message'];
        }
        break;
    case 'DeleteEmpRole':
        $resultDeleteEmpRole = DeleteEmpRole($getData);
        if ($resultDeleteEmpRole['message'] == 'Successfuly Deleted Employee Role.') {
            $return['type'] = 'Success';
            $return['message'] = $resultDeleteEmpRole['message'];
        } else {
            $return['type'] = 'Failed';
            $return['message'] = $resultDeleteEmpRole['message'];
        }
        break;
    case 'EditStoreType':
        $i = 0;
        foreach ($getData['toEditStoreTypeID'] as $editStoreType) {
            $new_array[] = array('NewStoreTypeName' => $getData['toEditStoreTypeName'][$i], 'ToUpdateStoreTypeID' => $getData['toEditStoreTypeID'][$i]);
            $i++;
        }
        $resultEditStoreType = EditStoreType($new_array);
        if ($resultEditStoreType['message'] == 'Succesfuly Edited Store Type.') {
            $return['type'] = 'Success';
            $return['message'] = $resultEditStoreType['message'];
        } else {
            $return['type'] = 'Failed';
            $return['message'] = $resultEditStoreType['message'];
        }
        break;
    case 'EditUserRole':
        $i = 0;
        foreach ($getData['toEditUserRoleID'] as $editStoreType) {
            $new_array[] = array('NewUserRoleName' => $getData['toEditUserRoleName'][$i], 'ToUpdateUserRoleID' => $getData['toEditUserRoleID'][$i]);
            $i++;
        }
        $resultEditUserRole = EditUserRole($new_array);
        if ($resultEditUserRole['message'] == 'Succesfuly Edited User Role.') {
            $return['type'] = 'Success';
            $return['message'] = $resultEditUserRole['message'];
        } else {
            $return['type'] = 'Failed';
            $return['message'] = $resultEditUserRole['message'];
        }
        break;
    case 'EditEmpRole':
        $i = 0;
        foreach ($getData['toEditEmpRoleID'] as $editStoreType) {
            $new_array[] = array('NewUserEmpName' => $getData['toEditEmpRoleName'][$i], 'ToUpdateEmpRoleID' => $getData['toEditEmpRoleID'][$i]);
            $i++;
        }
        $resultEditEmpRole = EditEmpRole($new_array);
        if ($resultEditEmpRole['message'] == 'Succesfuly Edited Employee Role.') {
            $return['type'] = 'Success';
            $return['message'] = $resultEditEmpRole['message'];
        } else {
            $return['type'] = 'Failed';
            $return['message'] = $resultEditEmpRole['message'];
        }
        break;
    default:
}

mysql_close($connection); // close database connection
echo json_encode($return);  // return to ajax result json_encode($result)
    


