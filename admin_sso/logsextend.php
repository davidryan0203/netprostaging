<?php
include($_SERVER['DOCUMENT_ROOT'] . '/functions/FunctionLog.php');
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
date_default_timezone_set('Australia/Melbourne');
session_start();
$role = $_SESSION['UserRoleID'];
$UserID = $_SESSION['UserProfileID'];
$from = $_GET['from'];
$to = $_GET['to'];
$UserLogs = SelectUserLogs($from, $to);
//var_dump($UserLogs);
//print_r($mergeStoreData);
?>

<script type="text/javascript">
    $(document).ready(function () {

        var table = $('#example').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]],
            columnDefs: [
                // {sortable: false, targets: -1},
                {"width": "8%", "targets": -1},
                {"width": "2%", "targets": 6}
            ],
            "oColVis": {
                "buttonText": "Hide Columns"
            },
            "autoWidth": false
        });
    });

    $(function () {
        $('[data-tooltip="tooltip"]').tooltip();
    });
</script>

<div class="table-responsive">
    <table id="example" class="table table-bordered table-hover"  cellspacing="0" >
        <thead>
            <tr>
                <th>ID</th>
                <th>Type</th>
                <th>Page</th>
                <th>Feature</th>
                <th>User</th>
                <th>Remarks</th>                
                <th>Status</th>
                <th>Date</th>                
            </tr>
        </thead>

        <tbody>
<?php foreach ($UserLogs as $value) { ?>
                <tr>
                    <td><?php echo $value['f_LogId']; ?></td>
                    <td><?php echo $value['f_LogTypeName']; ?></td>
                    <td><?php echo $value['f_LogFromName']; ?></td>
                    <td><?php echo $value['f_LogFromSubName']; ?></td>
                    <td><?php echo $value['f_UserName']; ?></td>
                    <td><?php echo $value['f_LogRemark']; ?></td>                    
                    <td><?php echo $value['f_LogStatus']; ?></td>
                    <td><?php
            $date = new DateTime($value['LogDate']);
            $date->setTimezone(new DateTimeZone('Australia/Melbourne')); // +04

            $latestRecord = $date->format('d-M-y (h:i A)');
            echo $latestRecord;
    ?></td>                    
                </tr>   
                    <?php } ?>
        </tbody> 
    </table>
</div>