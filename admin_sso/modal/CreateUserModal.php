<?php
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');

session_start();
$role = $_SESSION['UserRoleID'];
if ($role == '1') {
    $type = 'ALL';
} else {
    $type = 'Admin';
}
$roleList = SelectUserRoleList($type);
?>
<script type="text/javascript">

    $(function() {
        $("#ajaxmsgs").hide();
    }); // default hide divs for modal open


    $('#btnSubmit').on('click', function(event) {
        if ($("#CreateUserForm")[0].checkValidity()) {
            var url = 'admin/function/UserFunctionCaller.php?module=CreateUser';
            var mydata = $("#CreateUserForm").serialize();
            $.ajax({
                url: url,
                type: 'get',
                data: mydata,
                dataType: 'json',
                success: function(result) {
                    document.getElementById('ajaxmsgs').style.display = 'block';
                    if (result.type == 'Success') {
                        $('#ajaxmsgs').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong><br>" + result.message + "</div>");
                        $("#btnSubmit :input").attr("disabled", true);
                        $("#cancel").attr("disabled", false).html('Close');
                    } else {
                        $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong><br>" + result.message + "</div>");
                    }
                    //alert('success error');
                },
                error: function() {
                    document.getElementById('ajaxmsgs').style.display = 'block';
                    $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
                    //alert('here error');
                }
            });

        } else {
            $("#CreateUserForm").find(':submit').click();
        }
        event.preventDefault();
    }); // ajax button form submit

    $('#UserName').keyup(function() { // Keyup function for check the user action in input
        var Username = $(this).val(); // Get the username textbox using $(this) or you can use directly $('#username')
        var UsernameAvailResult = $('#username_avail_result'); // Get the ID of the result where we gonna display the results
        if (Username.length > 4) { // check if greater than 5 (minimum 6)
            UsernameAvailResult.html('Loading..'); // Preloader, use can use loading animation here
            var UrlToPass = 'action=username_availability&username=' + Username;
            $.ajax({// Send the username val to another checker.php using Ajax in POST menthod
                type: 'POST',
                data: UrlToPass,
                url: 'admin/function/CheckUserName.php/',
                success: function(responseText) { // Get the result and asign to each cases
                    if (responseText == 0) {
                        UsernameAvailResult.html('<span class="successuser">Username name available</span>');
                    } else if (responseText > 0) {
                        UsernameAvailResult.html('<span class="erroruser">Username already taken</span>');
                    } else {
                        alert('Problem with sql query');
                    }
                }
            });
        } else {
            UsernameAvailResult.html('Enter atleast 5 characters');
        }
        if (Username.length == 0) {
            UsernameAvailResult.html('');
        }
    });

    $('#UserPassword, #UserName').keydown(function(e) { // Dont allow users to enter spaces for their username and passwords
        if (e.which == 32) {
            return false;
        }
    });

</script>
<style type="text/css">
    .successuser{
        color:#009900;
    }
    .erroruser{
        color:#F33C21;
    }
    .username_avail_result{
        display:block;
        width:180px;
    }
</style>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Create User </h4>
</div>
<form class="form-horizontal" role="form" id="CreateUserForm"><!-- /modal-header -->
    <div class="modal-body">
        <input style="display:none" type="text" name="fakeusernameremembered"/>
        <input style="display:none" type="password" name="fakepasswordremembered"/>
        <div class="form-group">
            <label class="control-label col-sm-3" for="UserName">User Name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="UserName" name="UserName" placeholder="Enter User Name" required minlength="5">
                <div class="username_avail_result" id="username_avail_result"></div>
            </div>
        </div> <!-- /div for store name -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="UserPassword">Password:</label>
            <div class="col-sm-6">
                <input type="password" class="form-control" id="UserPassword" name="UserPassword" placeholder="Enter Password Here" required minlength="5">
            </div>
        </div> <!-- /div for password -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="FirstName">First Name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="FirstName" name="FirstName" placeholder="Enter First Name" required>
            </div>
        </div> <!-- /div for first name -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="LastName">Last Name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="LastName" name="LastName" placeholder="Enter Last Name" required>
            </div>
        </div> <!-- /div for last name -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="StoreOwnerID">Role:</label>
            <div class="col-sm-6">
                <select name="RoleID" class="form-control" required>
                    <option value="">Please select</option>
                    <?php
                    foreach ($roleList as $value) {
                        echo '<option value="' . $value['f_UserRoleID'] . '">' . $value['f_UserRoleName'] . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div> <!-- /div for store owner -->
        <div id="ajaxmsgs"></div>  <!-- ajaxmsg div -->
    </div> <!-- /modal-body -->
    <div class="modal-footer">
        <input type="submit" style="visibility: hidden"/>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id="cancel">Cancel</button>
        <button type="button" class="btn btn-sm btn-primary"  id="btnSubmit" name="btnSubmit">Submit</button>
        <button type="reset" class="btn btn-sm btn-danger">Reset</button>
    </div> <!-- /modal-footer -->
</form>  <!-- /form -->

