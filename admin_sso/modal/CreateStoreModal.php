<?php
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/UserFunction.php');


$storeList = SelectStoreList();

session_start();
$role = $_SESSION['UserRoleID'];
$stack = array('2', '3');
if ($role == '5') {
    // can only create his own
    $query = $_SESSION['EmpProfileID'];
//    $type = "StoreLeader";
    $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
                    left join t_empnumber b on a.f_EmpID = b.f_EmpID
                    left join t_storelist c on b.f_StoreID = c.f_StoreID
                    where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1";
    $result = mysql_query($sqlGetOwnerID, $connection);
    while ($row = mysql_fetch_assoc($result)) {
        $ownerID = $row['f_StoreOwnerUserID'];
    }

    //can only create his own
    $ownerID = $_SESSION['UserProfileID'];
    $type = "StoreOwner";
} elseif ($role == '1' || $role = '2') {
    // all
    $type = "GlobalAdmin";
    $owner = SelectUser('Owner');
//    $query = NULL;
}


$sqlGetStoreType = "SELECT * FROM t_storetype";
$resultStoreType = mysql_query($sqlGetStoreType, $connection);
while ($rowStoreType = mysql_fetch_assoc($resultStoreType)) {
    $storeType[] = $rowStoreType;
}
//change to detect if admin else set to default owner
?>
<script type="text/javascript">

    $(function() {
        $("#sharedDiv,#ajaxmsgs,#elementsToOperateOn0").hide();
    }); // default hide divs for modal open


    $(document).ready(function() {

        $('select').on('change', function(e) {
            $('.CreateShare').find('option').prop('disabled', false);
            $('.CreateShare').each(function() {
                $('.CreateShare').not(this).find('option[value="' + this.value + '"]').prop('disabled', true);
            });
        });

        $('select').on('change', function(e) {
            var exclude = ["StoreOwnerID", "StoreTypeID"];
            var optionName = $(this).attr('name');
            var optionid = $(this).attr('id');

            if ($.inArray(optionName, exclude) == -1) {
                var ownerID = $('option:selected', this).attr('ownerid');
                var txtboxID = optionid.charAt(optionid.length - 1);
                $('#shareowner' + txtboxID).val(ownerID)
                        .prop("disabled", false);
                // alert('#shareowner' + txtboxID);
            }
            e.preventDefault();
        });


        $('#includeShared').change(function(e) {
            if (!this.checked) {
                $('#sharedDiv').fadeOut('slow');
                $('#sharedDiv :input').attr('disabled', true).hide();
            } else {
                $('#sharedDiv').fadeIn('slow')
                        .find('#elementsToOperateOn0 :input').prop('disabled', false).show();
                document.getElementById('elementsToOperateOn0').style.display = 'block';
            }
            e.preventDefault();
        });


        $('#StoreCode').keyup(function() { // Keyup function for check the user action in input
            var StoreCode = $(this).val(); // Get the username textbox using $(this) or you can use directly $('#username')
            var StoreCodeAvailResult = $('#storecode_avail_result'); // Get the ID of the result where we gonna display the results
            if (StoreCode.length > 4) { // check if greater than 5 (minimum 6)
                StoreCodeAvailResult.html('Loading..'); // Preloader, use can use loading animation here
                var UrlToPass = 'action=storecode_availability&StoreCode=' + StoreCode;
                $.ajax({// Send the username val to another checker.php using Ajax in POST menthod
                    type: 'POST',
                    data: UrlToPass,
                    url: 'admin/function/CheckUserName.php/',
                    success: function(responseText) { // Get the result and asign to each cases
                        if (responseText == 0) {
                            StoreCodeAvailResult.html('<span class="successuser">Store Code available</span>');
                        } else if (responseText > 0) {
                            StoreCodeAvailResult.html('<span class="erroruser">Store Code already taken</span>');
                        } else {
                            alert('Problem with server process');
                        }
                    }
                });
            } else {
                StoreCodeAvailResult.html('Enter atleast 5 characters');
            }
            if (StoreCode.length == 0) {
                StoreCodeAvailResult.html('');
            }
        });


        $('#btnSubmit').on('click', function(event) {
            if ($("#storeFM")[0].checkValidity()) {
                var url = 'admin/function/StoreFunctionCaller.php?module=CreateStore';
                var mydata = $("#storeFM").serialize();
                // return false;
                $.ajax({
                    url: url,
                    type: 'get',
                    data: mydata,
                    dataType: 'json',
                    success: function(result) {
                        document.getElementById('ajaxmsgs').style.display = 'block';
                        if (result.type == 'Success') {
                            $('#ajaxmsgs').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong><br>" + result.message + "</div>");
                            $("#storeFM :input").attr("disabled", true);
                            $("#cancel").attr("disabled", false).html('Close');
                        } else {
                            $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong><br>" + result.message + "</div>");
                        }
                        //alert('success error');
                    },
                    error: function() {
                        document.getElementById('ajaxmsgs').style.display = 'block';
                        $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
                        //alert('here error');
                    }
                });
            } else {
                $("#storeFM").find(':submit').click();
            }
            event.preventDefault();
        }); // ajax button form submit
    }); // check box toggle for div

    function showDiv(cliked_id) {
        // alert('here');
        var fieldNum = cliked_id.charAt(cliked_id.length - 1);
        var newID = parseInt(fieldNum) + 1;
        var fieldID = "elementsToOperateOn-" + newID;
        var idfield = "#" + fieldID + " :input";
        document.getElementById(fieldID).style.display = 'block';
        $(idfield).prop("disabled", false).fadeIn('slow');
    } // unhide hidden div and enable input fields for shared to user

    function hideDiv(cliked_id) {
        var fieldNum = cliked_id.charAt(cliked_id.length - 1);
        var newID = parseInt(fieldNum);
        var fieldID = "elementsToOperateOn-" + newID;
        var idfield = "#" + fieldID + " :input";
        document.getElementById(fieldID).style.display = 'none';
        $(idfield).prop("disabled", true).fadeOut('slow');
    } // hide hidden div and disable input field for shared to user

    $(function() {
        $('[data-tooltip="tooltip"]').tooltip();
    });
</script>

<style type="text/css">
    .successuser{
        color:#009900;
    }
    .erroruser{
        color:#F33C21;
    }
    .storecode_avail_result{
        display:block;
        width:180px;
    }
</style>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Create Store</h4>
</div>
<form class="form-horizontal" role="form" id="storeFM"><!-- /modal-header -->
    <div class="modal-body">

        <div class="form-group">
            <label class="control-label col-sm-3" for="StoreListName">Store Name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="StoreListName" name="StoreListName" placeholder="Enter store name" required >
            </div>
        </div> <!-- /div for store name -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="StoreCode">Store Code:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="StoreCode" name="StoreCode" placeholder="Enter store code" required >
                <div class="storecode_avail_result" id="storecode_avail_result"></div>
            </div>
        </div> <!-- /div for store code -->
        <?php if ($role == 3 || $role == 5) { ?>
                                    <!--<select name="StoreOwnerID" class="form-control" style="display:none"><option value="<?php echo $ownerID; ?>" selected="selected"></option></select>-->
            <input type="hidden" name="StoreOwnerID" value="<?php echo $ownerID; ?>"/>
        <?php } elseif ($role == 1 || $role == 2) { ?>
            <div class="form-group">
                <label class="control-label col-sm-3" for="StoreOwnerID">Store Owner:</label>
                <div class="col-sm-6">
                    <select name="StoreOwnerID" class="form-control" required >
                        <option value="">Please select</option>
                        <?php
                        foreach ($owner as $value) {
                            echo '<option value="' . $value['f_UserID'] . '">' . $value['f_UserFirstName'] . " " . $value['f_UserLastName'] . " " . '</option>';
                        }
                        ?>
                    </select>
                </div>
            </div> <!-- /div for store owner -->
        <?php } ?>
        <div class="form-group">
            <label class="control-label col-sm-3" for="StoreTypeID">Store Type:</label>
            <div class="col-sm-6">
                <select name="StoreTypeID" class="form-control" required>
                    <option value="">Please Select</option>
                    <?php
                    foreach ($storeType as $storeTypeValue) {
                        echo '<option value="' . $storeTypeValue['f_StoreTypeID'] . '">' . $storeTypeValue['f_StoreTypeName'] . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div> <!-- /div for store code -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="StoreOwnerID"></label>
            <div class="col-sm-6">
                Show store records on report pages? <input type="checkbox" id="showonreports" name="showonreports"  class="btn-large"
                                                           data-tooltip="tooltip" data-placement="bottom" title="Yes / No">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="StoreOwnerID"></label>
            <div class="col-sm-6">
                Share to partner stores now? <input type="checkbox" id="includeShared" name="includeShared"  class="btn-large"
                                                    data-tooltip="tooltip" data-placement="bottom" title="Yes / No">
            </div>
        </div>
        <div id="sharedDiv"  style="display:none">
            <div class="form-group"  id="elementsToOperateOn0" class="col-sm-6">
                <label class="control-label col-sm-3" for="email">Share Store To:</label>
                <div  class="col-sm-6">
                    <select name="storeShareTo[]" class="form-control CreateShare" id="shareTo0" disabled required>
                        <option value="">Select Store</option>
                        <?php foreach ($storeList as $value) { ?>
                            <option value="<?php echo $value['f_StoreID']; ?>" ownerid="<?php echo $value['f_StoreOwnerUserID'] ?>"> <?php echo $value['f_StoreListName']; ?></option>
                        <?php } ?>
                    </select>
                    <input type="hidden" name="shareStoreOwnerID[]" id="shareowner0" value="" disabled/>
                </div>
                <button id="bntShow0" class="btn btn-sm btn-info" type="button" onclick="showDiv(this.id)"
                        data-tooltip="tooltip" data-placement="bottom" title="Add more">+</button>
            </div>   <!-- /div for share store owner -->

            <?php for ($loop = 1; $loop < 10; $loop++) { ?>
                <div class="form-group" id="elementsToOperateOn-<?php echo $loop; ?>" style="display:none" >
                    <label class="control-label col-sm-3" for="email"></label>
                    <div  class="col-sm-6">
                        <select name="storeShareTo[]" class="form-control CreateShare"  id="shareTo<?php echo $loop; ?>" disabled required>
                            <option value="">Please select</option>
                            <?php foreach ($storeList as $value) { ?>
                                <option value="<?php echo $value['f_StoreID']; ?>"  ownerid="<?php echo $value['f_StoreOwnerUserID'] ?>"> <?php echo $value['f_StoreListName']; ?></option>
                            <?php } ?>
                        </select>
                        <input type="hidden" name="shareStoreOwnerID[]"  id="shareowner<?php echo $loop; ?>" value="" disabled />
                    </div>  <button id="btnShow<?php echo $loop; ?>" class="btn btn-sm btn-info" type="button" onclick="showDiv(this.id)">+</button>
                    <button id="btnHide<?php echo $loop; ?>" class="btn btn-sm btn-warning" type="button" onclick="hideDiv(this.id)">-</button>
                </div>  <!-- /elementsToOperateOn div loop -->
            <?php } ?> <!-- loop for hidden dropdown -->

        </div> <!-- /modal-body -->

        <div id="ajaxmsgs"></div>  <!-- ajaxmsg div -->
    </div>
    <div class="modal-footer">
        <input type="submit" style="visibility: hidden"/>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id="cancel">Cancel</button>
        <button type="button" class="btn btn-sm btn-primary"  id="btnSubmit" name="btnSubmit">Submit</button>
        <button type="reset" class="btn btn-sm btn-danger" >Reset</button>
    </div> <!-- /modal-footer -->
</form>  <!-- /form -->

