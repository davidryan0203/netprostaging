<?php
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');
$type = $_GET['type'];

$text = $type . " Activator";

switch ($type) {

    case 'Store':
        $data = SelectStoreInactive();
        break;
    case 'User':
        $data = SelectUserInactive();
        break;
    case 'Employee':
        $data = SelectEmployeeInactive();
        break;
    default;
}
?>

<script type="text/javascript">
    $(document).ready(function () {
        var t = $('.ActivateTable').DataTable({
        });

        //Remove Heading Sorting
        $("th").unbind();
        $("th").removeClass("sorting");
        $("th").removeClass("sorting_asc");
    });
</script>
<script type="text/javascript">
    $('#ActivateStore').bind('change keyup', function (e) {
        var ActivateStoreBtn = $('#ActivateStoreBtn');
        if ($('#ActivateStore input:checkbox:checked').length > 0) {
            ActivateStoreBtn.prop('disabled', false);
        } else {
            //console.log('here');
            ActivateStoreBtn.prop('disabled', true);
        }
        e.preventDefault();
    });

    $('#ActivateUser').bind('change keyup', function (e) {
        var ActivateStoreBtn = $('#ActivateUserBtn');
        if ($('#ActivateUser input:checkbox:checked').length > 0) {
            ActivateStoreBtn.prop('disabled', false);
        } else {
            //console.log('here');
            ActivateStoreBtn.prop('disabled', true);
        }
        e.preventDefault();
    });

    $('#ActivateEmployee').bind('change keyup', function (e) {
        var ActivateStoreBtn = $('#ActivateEmployeeBtn');
        if ($('#ActivateEmployee input:checkbox:checked').length > 0) {
            ActivateStoreBtn.prop('disabled', false);
        } else {
            //console.log('here');
            ActivateStoreBtn.prop('disabled', true);
        }
        e.preventDefault();
    });

    $('#ActivateStoreBtn').on('click', function (event) {
        if ($("#ActivateStore")[0].checkValidity()) {
            var url = 'admin/function/ActivatorFunctionCaller.php?module=ActivateStore';          
            var mydata = $("#ActivateStore").serialize();
            $.ajax({
                url: url,
                type: 'get',
                data: mydata,
                dataType: 'json',
                success: function (result) {
                    document.getElementById('ajaxmsgs').style.display = 'block';
                    if (result.type == 'Success') {
                        $('#ajaxmsgs').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong><br>" + result.message + "</div>");
                        $("#ActivateStore :input").attr("disabled", true);
                        $("#cancel").attr("disabled", false).html('Close');
                    }
                    else {
                        $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong><br>" + result.message + "</div>");
                    }
                    //alert('success error');
                },
                error: function () {
                    document.getElementById('ajaxmsgs').style.display = 'block';
                    $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
                    //alert('here error');
                }
            });
        } else {
            $("#ActivateStore").find(':submit').click();
        }
        event.preventDefault();
    }); // ajax button form submit

    $('#ActivateUserBtn').on('click', function (event) {
        if ($("#ActivateUser")[0].checkValidity()) {
            var url = 'admin/function/ActivatorFunctionCaller.php?module=ActivateUser';       
            var mydata = $("#ActivateUser").serialize();
            $.ajax({
                url: url,
                type: 'get',
                data: mydata,
                dataType: 'json',
                success: function (result) {
                    document.getElementById('ajaxmsgs').style.display = 'block';
                    if (result.type == 'Success') {
                        $('#ajaxmsgs').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong><br>" + result.message + "</div>");
                        $("#ActivateUser :input").attr("disabled", true);
                        $("#cancel").attr("disabled", false).html('Close');
                    }
                    else {
                        $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong><br>" + result.message + "</div>");
                    }
                    //alert('success error');
                },
                error: function () {
                    document.getElementById('ajaxmsgs').style.display = 'block';
                    $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
                    //alert('here error');
                }
            });
        } else {
            $("#ActivateUser").find(':submit').click();
        }
        event.preventDefault();
    }); // ajax button form submit

    $('#ActivateEmployeeBtn').on('click', function (event) {
        if ($("#ActivateEmployee")[0].checkValidity()) {
            var url = 'admin/function/ActivatorFunctionCaller.php?module=ActivateEmployee';         
            var mydata = $("#ActivateEmployee").serialize();
            $.ajax({
                url: url,
                type: 'get',
                data: mydata,
                dataType: 'json',
                success: function (result) {
                    document.getElementById('ajaxmsgs').style.display = 'block';
                    if (result.type == 'Success') {
                        $('#ajaxmsgs').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong><br>" + result.message + "</div>");
                        $("#ActivateEmployee :input").attr("disabled", true);
                        $("#cancel").attr("disabled", false).html('Close');
                    }
                    else {
                        $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong><br>" + result.message + "</div>");
                    }
                    //alert('success error');
                },
                error: function () {
                    document.getElementById('ajaxmsgs').style.display = 'block';
                    $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
                    //alert('here error');
                }
            });
        } else {
            $("#ActivateEmployee").find(':submit').click();
        }
        event.preventDefault();
    }); // ajax button form submit


</script>

<div id="modal-body" style="padding: 20px;">
    <h4><?php echo $text; ?></h4>
    <hr>
    <?php if ($type == 'Store') { ?> 
        <div>    
            <?php
            //echo "<pre>";
            //print_r($data);
            ?>
            <?php if (!is_null($data)) { ?>
                <form class="form-horizontal" role="form" id="ActivateStore">               
                    <div class="row">                           
                        <div class="col-lg-12">
                            <table class="table table-bordered table-responsive ActivateTable">         
                                <thead>
                                    <tr>
                                        <th>Activate</th>
                                        <th>Store</th>
                                        <th>Code</th>
                                    </tr>
                                </thead>

                                <?php
                                $i = 0;
                                foreach ($data as $inactiveStore) {
                                    ?>
                                    <tr>     
                                        <td><input type="checkbox" name="toActivateStore[]" value="<?php echo $inactiveStore['f_StoreID']; ?>" id="toActivateStore[<?php echo $i; ?>]"></td> 
                                        <td><?php echo $inactiveStore['f_StoreListName']; ?></td>       
                                        <td><?php echo $inactiveStore["f_CompanyCode"]; ?></td> 
                                    </tr>
                                    <?php
                                } $i++;
                                ?>
                            </table>                            
                        </div><!-- /.col-lg-6 -->
                    </div>
                    <div id="ajaxmsgs"></div>
                    <div class="modal-footer">
                        <input type="submit" style="visibility: hidden"/>
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-sm btn-primary"  id="ActivateStoreBtn" disabled="">Submit</button>                    
                        <button type="reset" class="btn btn-sm btn-danger" id="StoreResetBtn">Reset</button>
                    </div> <!-- /modal-footer -->	
                </form> 
            <?php } else { ?>
                <div class="well text-center"> No Inactive Store Available</div>    
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                </div>
            <?php } ?>
        </div>
    <?php } elseif ($type == 'User') { ?> 
        <div>   
            <?php
            //echo "<pre>";
            //print_r($data);
            ?>
            <?php if (!is_null($data)) { ?>
                <form class="form-horizontal" role="form" id="ActivateUser">               
                    <div class="row">                           
                        <div class="col-lg-12">
                            <table class="table table-bordered table-responsive ActivateTable">         
                                <thead>
                                    <tr>
                                        <th>Activate</th>
                                        <th>Name</th>
                                    </tr>
                                </thead>

                                <?php
                                $i = 0;
                                foreach ($data as $inactiveUser) {
                                    ?>
                                    <tr>     
                                        <td><input type="checkbox" name="toActivateUser[]" value="<?php echo $inactiveUser['f_UserID']; ?>" id="toActivateUser[<?php echo $i; ?>]"></td> 
                                        <td><?php echo $inactiveUser['f_UserFirstName'] . " " . $inactiveUser['f_UserLastName']; ?></td>       
                                    </tr>
                                    <?php
                                } $i++;
                                ?>
                            </table>                            
                        </div><!-- /.col-lg-6 -->
                    </div>
                    <div id="ajaxmsgs"></div>
                    <div class="modal-footer">
                        <input type="submit" style="visibility: hidden"/>
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-sm btn-primary"  id="ActivateUserBtn" disabled>Submit</button>                    
                        <button type="reset" class="btn btn-sm btn-danger" id="UserResetBtn">Reset</button>
                    </div> <!-- /modal-footer -->	
                </form> 
            <?php } else { ?>
                <div class="well text-center"> No Inactive User Available</div>    
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                </div>
            <?php } ?>
        </div>
    <?php } elseif ($type == 'Employee') { ?> 
        <div>    
            <?php
            //echo "<pre>";
            //print_r($data);
            ?>
            <?php if (!is_null($data)) { ?>
                <form class="form-horizontal" role="form" id="ActivateEmployee">               
                    <div class="row">                           
                        <div class="col-lg-12">
                            <table class="table table-bordered table-responsive ActivateTable">         
                                <thead>
                                    <tr>
                                        <th>Activate</th>
                                        <th>Name</th>
                                    </tr>
                                </thead>

                                <?php
                                $i = 0;
                                foreach ($data as $inactiveEmployee) {
                                    ?>
                                    <tr>     
                                        <td><input type="checkbox" name="toActivateEmployee[]" value="<?php echo $inactiveEmployee['f_EmpID']; ?>" id="toActivateEmployee[<?php echo $i; ?>]"></td> 
                                        <td><?php echo $inactiveEmployee['f_EmpFirstName'] . " " . $inactiveEmployee['f_EmpLastName']; ?></td>       
                                    </tr>
                                    <?php
                                } $i++;
                                ?>
                            </table>                            
                        </div><!-- /.col-lg-6 -->
                    </div>
                    <div id="ajaxmsgs"></div>
                    <div class="modal-footer">
                        <input type="submit" style="visibility: hidden"/>
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-sm btn-primary"  id="ActivateEmployeeBtn" disabled>Submit</button>                    
                        <button type="reset" class="btn btn-sm btn-danger" id="EmployeeResetBtn">Reset</button>
                    </div> <!-- /modal-footer -->	
                </form> 
            <?php } else { ?>
                <div class="well text-center"> No Inactive Employee Available</div>    
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
</div>


