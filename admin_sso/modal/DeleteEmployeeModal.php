<?php
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');
//include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/UserFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/StoreFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/EmployeeFunction.php');
$empID = $_GET['empID'];

$empData = SelectEmployee($empID);
$storeList = SelectStoreList();


foreach ($empData as $newData) {

    $mergeStoreData[$newData['f_EmpID']]['EmpID'] = $newData['f_EmpID'];
    $mergeStoreData[$newData['f_EmpID']]['EmpFirstName'] = $newData['f_EmpFirstName'];
    $mergeStoreData[$newData['f_EmpID']]['EmpLastName'] = $newData['f_EmpLastName'];
    $mergeStoreData[$newData['f_EmpID']]['UserName'] = $newData['f_UserName'];
    $mergeStoreData[$newData['f_EmpID']]['Password'] = $newData['f_Password'];
    $mergeStoreData[$newData['f_EmpID']]['EmpRoleID'] = $newData['f_EmpRoleID'];
    $mergeStoreData[$newData['f_EmpID']]['EmpRoleName'] = $newData['f_EmpRoleName'];
    $mergeStoreData[$newData['f_EmpID']]['EmpPhone'] = $newData['f_EmpPhone'];
    $mergeStoreData[$newData['f_EmpID']]['EmpMobile'] = $newData['f_EmpMobile'];
    $mergeStoreData[$newData['f_EmpID']]['EmpEmail'] = $newData['f_EmpEmail'];

    //$mergeStoreData[$newData['f_EmpID']]['StoreListName']  =$newData['f_StoreListName'];
    $mergeStoreData[$newData['f_EmpID']]['EmpNo'][] = array('StoreListID' => $newData['f_StoreID'],
        'EmpNo' => $newData['f_EmpPNumber'],
        'StoreListName' => $newData['f_StoreListName']
    );
    $beenAdded[] = $newData['f_StoreID'];
}
//echo "<pre>" . $empID;
//var_dump($empData);
//var_dump($mergeEmployeeData);
?>
<script type="text/javascript">

    $(function () {
        $("#ajaxmsgs").hide();
    }); // default hide divs for modal open
   $(document).ready(function () {

        $('#btnSubmit').on('click', function (event) {           
            var url = 'admin/function/EmployeeFunctionCaller.php?module=DeleteEmployee';         
            var mydata = $("#DeleteEmployeeForm").serialize();
            $.ajax({
                url: url,
                type: 'get',
                data: mydata,
                dataType: 'json',
                success: function (result) {
                    document.getElementById('ajaxmsgs').style.display = 'block';
                    if (result.type == 'Success') {
                        $('#ajaxmsgs').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong><br>" + result.message + "</div>");
                          $("#DeleteEmployeeForm :input").attr("disabled", true);
                            $("#cancel").attr("disabled", false).html('Close');
                   }
                    else {
                        $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong><br>" + result.message + "</div>");
                    }                    
                },
                error: function () {
                    document.getElementById('ajaxmsgs').style.display = 'block';
                    $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");                   
                }
            });
            event.preventDefault();
        }); // ajax button form submit
    });
  
</script>
<style type="text/css">  
    .successuser{
        color:#009900;
    }
    .erroruser{
        color:#F33C21;
    }
    .empname_avail_result .checkPass{
        display:block;
        width:180px;
    }
    .redBackground{
    background-color:red;
    }
</style>


<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Delete Employee</h4>
</div>	
<form class="form-horizontal" role="form" id="DeleteEmployeeForm"><!-- /modal-header -->
    <div class="modal-body">
        <p>Are you sure you want to delete this Employee?</p>
        <div class="well"><h4>Employee details</h4>
             <?php foreach ($mergeStoreData as $value) { ?>
                &nbsp;Employee Name:<b> <?php echo $value['EmpFirstName'] . " " . $value['EmpLastName'] ?></b></br>
                &nbsp;Employee User Login:<b> <?php echo $value['UserName']; ?> </b></br>
                &nbsp;Employee Email:<b> <?php echo $value['EmpEmail']; ?></b></br>
                &nbsp;Employee Phone:<b> <?php echo $value['EmpPhone']; ?></b></br>
                &nbsp;Employee Mobile:<b> <?php echo $value['EmpMobile']; ?></b></br>
                &nbsp;Employee Mobile:<b> <?php echo $value['EmpRoleName']; ?></b></br>
                &nbsp;Employee Number:
                    <table><colgroup>
                            <col style="width: 20%">
                              <col style="width: 40%">
                        <col style="width:40%"><tr><th></th><th></th><th></th></tr>
                        <?php foreach ($value['EmpNo'] as $EmpNo) { ?>
                            <input type="hidden" name="ShareStoreID[]" value="<?php echo $sharevalue['SharedStoreID']; ?>"/>
                            <tr><td></td>
                            <td><?php echo $EmpNo['EmpNo']; ?></td>
                            <td><b><?php echo $EmpNo['StoreListName']; ?></b></td>
                            </tr>
                        <?php } ?></table>
                         
                <?php } ?>
        </div>

        <input type="hidden" name="EmployeeID" id="EmployeeID" value="<?php echo $empID; ?>" /> 
        <div id="ajaxmsgs"></div>  <!-- ajaxmsg div -->
    </div> <!-- /modal-body -->	
    <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id="cancel">Cancel</button>
        <button type="button" class="btn btn-sm btn-danger"  id="btnSubmit" name="btnSubmit" >Submit</button>
    </div> <!-- /modal-footer -->	
</form>  <!-- /form --> 

