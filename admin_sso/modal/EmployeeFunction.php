<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/crypter.php');

function EmployeeFunction($function, $query) {

    switch ($function) {

        case "CreateEmployee":
            $EmployeeFunctionData = CreateEmployee($query);
            break;
        case "UpdateEmployee":
            $EmployeeFunctionData = UpdateEmployee($query);
            break;
        case "SelectEmployee":
            $EmployeeFunctionData = SelectEmployee($query);
            break;
        case "DeleteEmployee":
            $EmployeeFunctionData = SelectEmployee($query);
            break;

        default:
            $EmployeeFunctionData = "Invalid Protocol";
    }

    return $EmployeeFunctionData;
}

function CreateEmployee($query) {
    global $connection;

    // foreach ($query as $data) {
    $empfirstname = mysql_real_escape_string($query['EmpFirstName']);
    $emplastname = mysql_real_escape_string($query['EmpLastName']);
    $emproleID = mysql_real_escape_string($query['EmpRoleID']);
    $empemail = mysql_real_escape_string($query['EmpEmail']);
    $empPhone = mysql_real_escape_string($query['EmpPhone']);
    $empmobile = mysql_real_escape_string($query['EmpMobile']);
    // }

    $sql = "INSERT INTO `t_emplist` (`f_EmpFirstName`,`f_EmpLastName`,`f_EmpRoleID`,`f_EmpEmail`,`f_EmpPhone`,`f_EmpMobile`)
            VALUES ('$empfirstname','$emplastname','$emproleID','$empemail','$empPhone','$empmobile')";

    $result = mysql_query($sql, $connection);

    if ($result && mysql_affected_rows() == 1) {
        $returnCreateEmployee['message'] = "Success Creating Employee";
        $returnCreateEmployee['EmpID'] = mysql_insert_id();
    } else {
        $returnCreateEmployee['message'] = "Failed Creating Employee";
    }

    //mysql_close($connection);
    return $returnCreateEmployee;
}

function UpdateEmployee($query) {
    global $connection;

    // foreach ($query as $data) {
    $username = mysql_real_escape_string($query['EmployeeName']);
    if (isset($query['EmployeePassword'])) {
        // $password = md5(mysql_real_escape_string($query['EmployeePassword']));
        $password = password_hash($query['EmployeePassword'], PASSWORD_BCRYPT);
        $cleanpass = mysql_real_escape_string($query['EmployeePassword']);
        $origpass = encrpyt_pass($cleanpass);
    } else {
        $password = null;
    }

    $roleID = $query['EmployeeRoleID'];
    $firstname = mysql_real_escape_string($query['FirstName']);
    $lastname = mysql_real_escape_string($query['LastName']);
    $userdID = $query['EmployeeID'];
    //  }

    if (!is_null($password)) {
        $sql = "UPDATE `t_userlist`
            SET
            `f_EmployeeName` = '$username',
            `f_EmployeePassword` = '$password',
            `f_UesrRoleID` = '$roleID',
            `f_EmployeeFirstName` = '$firstname',
            `f_EmployeeLastName` = '$lastname'
            WHERE `f_EmployeeID` = '{$userdID}'";
    } else {
        $sql = "UPDATE `t_userlist`
            SET
            `f_EmployeeName` = '$username',
            `f_EmployeeRoleID` = '$roleID',
            `f_EmployeeFirstName` = '$firstname',
            `f_EmployeeLastName` = '$lastname'
            WHERE `f_EmployeeID` = '{$userdID}'";
    }
    //echo $sql;
    $result = mysql_query($sql, $connection);

    if ($result && mysql_affected_rows() == 1) {


        $getUserLogin = "Select f_UserLoginID from t_userlogin where f_EmpID = '{$userdID}' limit 1";
        $resultUserLogin = mysql_query($getUserLogin, $connection);
        while ($rowUserLogin = mysql_fetch_assoc($resultUserLogin)) {
            $userLoginID = $rowUserLogin['f_UserLoginID'];
        }

        $sqlCheckRepo = "Select f_NewPass as 'CurrentPass',count(*) as 'isInserted' From t_passrepo where f_UserLoginID = '{$userLoginID}' limit 1";
        $resultCheckRepo = mysql_query($sqlCheckRepo, $connection);
        while ($rowCheckRepo = mysql_fetch_assoc($resultCheckRepo)) {
            $isInserted = $rowCheckRepo['isInserted'];
            $oldPass = $rowCheckRepo['CurrentPass'];
        }
        if ($isInserted >= 1) {
            $sqlRepoPass = "UPDATE t_passrepo SET f_NewPass = '{$origpass}' , f_OldPass = '{$oldPass}', f_DateTimeCreated = NOW() where f_UserLoginID = '{$userLoginID}'";
            mysql_query($sqlRepoPass, $connection);
        } else {
            $sqlRepoPass = "INSERT INTO `t_passrepo` (`f_UserLoginID`,`f_NewPass`) VALUES ('{$userLoginID}','{$origpass}')";
            mysql_query($sqlRepoPass, $connection);
        }


        $returnUpdateEmployee['message'] = 'Update Success Employee';
    } else {
        //  echo mysql_affected_rows();
        $returnUpdateEmployee['message'] = 'Update Failed Employee';
    }

    // mysql_close($connection);
    return $returnUpdateEmployee;
}

function SelectEmployee($query, $type = NULL) {
    global $connection;


    $countQuery = count($query);

    if ($type == 'store') {
        $sql = "SELECT a.*,b.f_EmpRoleName, b.f_EmpRoleName , d.f_StoreListName , c.f_StoreID, c.f_EmpPNumber
            FROM t_emplist a
            LEFT JOIN t_emprole b on a.f_EmpRoleID = b.f_EmpRoleID
            LEFT JOIN t_empnumber c on a.f_EmpID = c.f_EmpID
            LEFT JOIN t_storelist d on c.f_StoreID = d.f_StoreID
            LEFT JOIN t_userlist e on d.f_StoreOwnerUserID = e.f_UserID
            WHERE 1=1 AND e.f_UserID = '{$query}'";
    } else {
        $sql = "SELECT a.*,b.f_EmpRoleName, b.f_EmpRoleName , d.f_StoreListName , c.f_StoreID, c.f_EmpPNumber
            FROM t_emplist a
            LEFT JOIN t_emprole b on a.f_EmpRoleID = b.f_EmpRoleID
            LEFT JOIN t_empnumber c on a.f_EmpID = c.f_EmpID
            LEFT JOIN t_storelist d on c.f_StoreID = d.f_StoreID
            WHERE 1=1 ";
        if ($countQuery > 1) {
            $quotedString = "";
            foreach ($query as $data) {

                $quotedString .= "'$data',";
            }

            $whereIn = trim($whereIn, ",");

            $sql .= " AND a.f_EmpID in ('{$whereIn}')";
        } else {
            if ($query != "ALL") {

                $sql .= " AND a.f_EmpID = '{$query}'";
            }
        }
    }

    $result = mysql_query($sql, $connection);
    while ($row = mysql_fetch_assoc($result)) {
        $returnSelectEmployee[] = $row;
    }
    // echo $sql;
    //mysql_close($connection);
    return $returnSelectEmployee;
}

function DeleteEmployee($query) {
    global $connection;

    //var_dump($query);
    $sql = "DELETE FROM `t_userlist`
            WHERE f_EmployeeID = '{$query[0]}'";

    //  echo $sql;
    $result = mysql_query($sql, $connection);

    if ($result && mysql_affected_rows() == 1) {
        $returnDeleteEmployee['message'] = "Deletion Success Employee";
    } else {
        $returnDeleteEmployee['message'] = "Deletion Failed Employee";
    }

    //mysql_close($connection);
    return $returnDeleteEmployee;
}

function CreateEmployeeNo($query) {
    global $connection;
//echo "<pre>";
    $queryarray = array_combine($query['ToWhatStoreID'], $query['EmpNum']);
    //var_dump($query);
    // echo "===============";
    //var_dump($queryarray);
    $i = 0;
    foreach ($queryarray as $ToWhatStoreID => $EmpNum) {
        $test[$i] = "'$EmpNum',{$query['EmpID']},'$ToWhatStoreID'";
        $i++;
    }
    $sql = "INSERT INTO t_empnumber (`f_EmpPNumber`,`f_EmpID`,`f_StoreID`) VALUES ";
    foreach ($test as $data) {
        $sql .= "(" . $data . "),";
    }
    $sql = trim($sql, ",");
    // echo $sql;
    //exit;
    $result = mysql_query($sql, $connection);

    if ($result && mysql_affected_rows() >= 1) {
        $returnCreateSharedStore['message'] = "Success Creating Shared Store";
    } else {
        $returnCreateSharedStore['message'] = "Failed Creating Shared Store: ";
    }
    // echo mysql_error();
    // mysql_close($connection);
    return $returnCreateSharedStore;
}
