<?php
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');
$type = $_GET['type'];
//echo $type;
switch ($type) {
    case 'StoreType':
        $data = SelectStoreTypeList();
        $text = "Store Type";
        break;
    case 'UserRole':
        $data = SelectUserRoleList($type);
        $text = "User Role";
        break;
    case 'EmpRole':
        $data = SelectEmpRoleList();
        $text = "Employee Role";
        break;
    default;
}
?>

<script type="text/javascript">
    $(document).ready(function () {
        var t = $('.UpdateTable').DataTable({
        });

        //Remove Heading Sorting
        $("th").unbind();
        $("th").removeClass("sorting");
        $("th").removeClass("sorting_asc");
    });
</script>

<script type="text/javascript">

    $(function () {
        $("#ajaxmsgs").hide();
    }); // default hide divs for modal open 

    $('#Edit<?php echo $type; ?>Form').bind('change keyup', function (e) {
        var EditBtn = $('#<?php echo $type; ?>EditBtn');
        if ($('#Edit<?php echo $type; ?>Form input:checkbox:checked').length > 0) {
            EditBtn.prop('disabled', false);
        } else {
            //console.log('here');
            EditBtn.prop('disabled', true);
        }
        e.preventDefault();
    });

    $('#Delete<?php echo $type; ?>Form').bind('change keyup', function (e) {
        var DeleteBtn = $('#<?php echo $type; ?>DeleteBtn');
        if ($('#Delete<?php echo $type; ?>Form input:checkbox:checked').length > 0) {
            DeleteBtn.prop('disabled', false);
        } else {
            //console.log('here');
            DeleteBtn.prop('disabled', true);
        }
        e.preventDefault();
    });

    $('#<?php echo $type; ?>AddBtn').on('click', function (event) {
        if ($("#Add<?php echo $type; ?>Form")[0].checkValidity()) {
            var url = 'admin/function/UpdaterFunctionCaller.php?module=Add<?php echo $type; ?>';
            //  console.log($("#Add<?php echo $type; ?>Form").serialize());
            var mydata = $("#Add<?php echo $type; ?>Form").serialize();

           // console.log(url + '&' + mydata);
            // return false;
            $.ajax({
                url: url,
                type: 'get',
                data: mydata,
                dataType: 'json',
                success: function (result) {
                    document.getElementById('ajaxmsgsAdd').style.display = 'block';
                    if (result.type == 'Success') {
                        $('#ajaxmsgsAdd').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong><br>" + result.message + "</div>");
                        $("#Add<?php echo $type; ?>Form :input").attr("disabled", true);
                        $("#cancel").attr("disabled", false).html('Close');
                    }
                    else {
                        $('#ajaxmsgsAdd').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong><br>" + result.message + "</div>");
                        document.getElementById('ajaxmsgsAdd').style.display = 'block';
                    }
                    //alert('success error');
                },
                error: function () {
                    document.getElementById('ajaxmsgsAdd').style.display = 'block';
                    $('#ajaxmsgsAdd').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error! Unknown Error Occurs</strong></div>");
                    //alert('here error');
                }
            });
        } else {
            $("#Add<?php echo $type; ?>Form").find(':submit').click();
        }
        event.preventDefault();
    }); // ajax button form submit

    $('#<?php echo $type; ?>EditBtn').on('click', function (event) {
        if ($("#Edit<?php echo $type; ?>Form")[0].checkValidity()) {
            var url = 'admin/function/UpdaterFunctionCaller.php?module=Edit<?php echo $type; ?>';
           // console.log($("#Edit<?php echo $type; ?>Form").serialize());
            var mydata = $("#Edit<?php echo $type; ?>Form").serialize();

          //  console.log(url + '&' + mydata);
            // return false;
            $.ajax({
                url: url,
                type: 'get',
                data: mydata,
                dataType: 'json',
                success: function (result) {
                    document.getElementById('ajaxmsgsEdit').style.display = 'block';
                    if (result.type == 'Success') {
                        $('#ajaxmsgsEdit').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong><br>" + result.message + "</div>");
                        $("#Edit<?php echo $type; ?>Form :input").attr("disabled", true);
                        $("#cancel").attr("disabled", false).html('Close');
                    }
                    else {
                        $('#ajaxmsgsEdit').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong><br>" + result.message + "</div>");
                    }
                    //alert('success error');
                },
                error: function () {
                    document.getElementById('ajaxmsgsEdit').style.display = 'block';
                    $('#ajaxmsgsEdit').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error! Unknown Error Occurs</strong></div>");
                    //alert('here error');
                }
            });
        } else {
            $("#Edit<?php echo $type; ?>Form").find(':submit').click();
        }
        event.preventDefault();
    }); // ajax button form submit

    $('#<?php echo $type; ?>DeleteBtn').on('click', function (event) {
        if ($("#Delete<?php echo $type; ?>Form")[0].checkValidity()) {
            var url = 'admin/function/UpdaterFunctionCaller.php?module=Delete<?php echo $type; ?>';
           // console.log($("#Delete<?php echo $type; ?>Form").serialize());
            var mydata = $("#Delete<?php echo $type; ?>Form").serialize();

          //  console.log(url + '&' + mydata);
            // return false;
            $.ajax({
                url: url,
                type: 'get',
                data: mydata,
                dataType: 'json',
                success: function (result) {
                    document.getElementById('ajaxmsgsDelete').style.display = 'block';
                    if (result.type == 'Success') {
                        $('#ajaxmsgsDelete').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong><br>" + result.message + "</div>");
                        $("#Delete<?php echo $type; ?>Form :input").attr("disabled", true);
                        $("#cancel").attr("disabled", false).html('Close');
                    }
                    else {
                        $('#ajaxmsgsDelete').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong><br>" + result.message + "</div>");
                    }
                    //alert('success error');
                },
                error: function () {
                    document.getElementById('ajaxmsgsDelete').style.display = 'block';
                    $('#ajaxmsgsDelete').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error! Unknown Error Occurs</strong></div>");
                    //alert('here error');
                }
            });
        } else {
            $("#Delete<?php echo $type; ?>Form").find(':submit').click();
        }
        event.preventDefault();
    }); // ajax button form submit


    function txtEnabler(this_id) {
        var txtToenable = $('#' + this_id).attr('txtcon');
        var chckID = this_id;
        // console.log(this_id+' '+$('#'+this_id).attr('txtcon'));
        $('#' + txtToenable).prop("disabled", !$('#' + chckID).is(':checked'));
    }


    function showDiv(cliked_id) {

        var fieldNum = cliked_id.charAt(cliked_id.length - 1);
        var newID = parseInt(fieldNum) + 1;
        var fieldID = "elementsToOperateOn-" + newID;
        var idfield = "#" + fieldID + " :input";
        //  alert(idfield);
        document.getElementById(fieldID).style.display = 'block';
        $(idfield).prop("disabled", false).fadeIn('slow');
        // cliked_id.preventDefault();
    } // unhide hidden div and enable input fields for shared to user

    function hideDiv(cliked_id) {
        var fieldNum = cliked_id.charAt(cliked_id.length - 1);
        var newID = parseInt(fieldNum);
        var fieldID = "elementsToOperateOn-" + newID;
        var idfield = "#" + fieldID + " :input";
        document.getElementById(fieldID).style.display = 'none';
        $(idfield).prop("disabled", true).fadeOut('slow');
        // cliked_id.preventDefault();
    } // hide hidden div and disable input field for shared to user     
    $("input[type='checkbox']").change(function () {
        if ($(this).is(":checked")) {
            $(this).parent().addClass("redBackground");
        } else {
            $(this).parent().removeClass("redBackground");
        }
    });

    $(function () {
        $('[data-tooltip="tooltip"]').tooltip();
    });

    jQuery(document).ready(function ($) {
        $('#tabs').tab();
    });
</script>

<style type="text/css">  
    .redBackground{
        background-color:#ea3a01;
    }

    input{
        min-width: 100%;
    }
</style>


<div id="modal-body" style="padding: 20px;">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
        <li class="active"><a href="#Add" data-toggle="tab">Add</a></li>
        <li><a href="#Edit" data-toggle="tab">Edit</a></li>
        <li><a href="#Delete" data-toggle="tab">Delete</a></li>
    </ul>

    <div id="my-tab-content" class="tab-content">
        <?php //echo print_r($data); ?>
        <div class="tab-pane active" id="Add">
            <h4> Add <?php echo $text; ?></h4>
            <hr>
            <form class="form-horizontal" role="form" id="Add<?php echo $type; ?>Form"> 
                <div>
                    <div class="form-group"  id="elementsToOperateOn0" class="col-sm-6">
                        <label class="control-label col-sm-3">Name</label>
                        <div  class="col-sm-6">                            
                            <input type="text" class="form-control" name="toAdd<?php echo $type; ?>[]" placeholder="Enter <?php echo $text; ?> Here" required/>
                        </div> 

                        <button id="bntShow0" class="btn btn-sm btn-info" type="button" onclick="showDiv(this.id)"
                                data-tooltip="tooltip" data-placement="right" title="Add another">+</button>
                    </div>   <!-- /div for share store owner -->

                    <?php for ($loop = 1; $loop < 10; $loop++) { ?> 
                        <div class="form-group" id="elementsToOperateOn-<?php echo $loop; ?>" style="display:none" >
                            <label class="control-label col-sm-3"></label>
                            <div  class="col-sm-6">                                
                                <input type="text" class="form-control" name="toAdd<?php echo $type; ?>[]" placeholder="Enter <?php echo $text; ?> Here" disabled required/>
                            </div>  
                            <button id="btnShow<?php echo $loop; ?>" class="btn btn-sm btn-info" type="button" onclick="showDiv(this.id)"
                                    data-tooltip="tooltip" data-placement="right" title="Add another">+</button>
                            <button id="btnHide<?php echo $loop; ?>" class="btn btn-sm btn-warning" type="button" onclick="hideDiv(this.id)"
                                    data-tooltip="tooltip" data-placement="right" title="Remove">-</button>
                        </div>  <!-- /elementsToOperateOn div loop -->
                    <?php } ?> <!-- loop for hidden dropdown -->
                </div> <!-- /share div -->

                                                <!--<input type="hidden" name="EmpID" value="<?php echo $empData[0]['f_EmpID']; ?>"/>-->

                <div id="ajaxmsgsAdd"></div>

                <div class="modal-footer">
                    <input type="submit" style="visibility: hidden"/>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-sm btn-primary" id="<?php echo $type; ?>AddBtn">Submit</button>
                    <button type="reset" class="btn btn-sm btn-danger" id="<?php echo $type; ?>Reset">Reset</button>
                </div> <!-- /modal-footer -->	
            </form>
        </div>

        <div class="tab-pane" id="Edit">
            <h4>Edit <?php echo $text; ?></h4>
            <hr>
            <?php if (!is_null($data)) { ?>
                <form class="form-horizontal" role="form" id="Edit<?php echo $type; ?>Form">               
                    <div class="row">                           
                        <div class="col-lg-12">
                            <table class="table table-bordered table-responsive UpdateTable"> 
                                <thead>
                                    <tr>
                                        <th style="width: 20px;">Edit</th>
                                        <th><?php echo $text; ?></th>
                                    </tr>
                                </thead>

                                <?php if ($type == 'StoreType') { ?>
                                    <?php
                                    $i = 0;
                                    foreach ($data as $value) {
                                        ?>
                                        <tr>     
                                            <td>
                                                <input type="checkbox" name="toEdit<?php echo $type; ?>ID[]" 
                                                       value="<?php echo $value['f_StoreTypeID']; ?>" txtcon="toEdit<?php echo $type; ?>Name<?php echo $i; ?>"
                                                       id="toEdit<?php echo $type; ?>ID<?php echo $i; ?>" onclick="txtEnabler(this.id)">
                                            </td> 
                                            <td data-search="<?php echo $value['f_StoreTypeName']; ?>">
                                                <input type="text" name="toEdit<?php echo $type; ?>Name[]" 
                                                       value="<?php echo $value['f_StoreTypeName']; ?>" 
                                                       id="toEdit<?php echo $type; ?>Name<?php echo $i; ?>" disabled></td>       
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                    ?>
                                <?php } elseif ($type == 'UserRole') { ?>                                    
                                    <?php
                                    $i = 0;
                                    foreach ($data as $value) {
                                        ?>
                                        <tr>     
                                            <td>
                                                <input type="checkbox" name="toEdit<?php echo $type; ?>ID[]" 
                                                       value="<?php echo $value['f_UserRoleID']; ?>" txtcon="toEdit<?php echo $type; ?>Name<?php echo $i; ?>"
                                                       id="toEdit<?php echo $type; ?>ID<?php echo $i; ?>" onclick="txtEnabler(this.id)">
                                            </td> 
                                            <td data-search="<?php echo $value['f_UserRoleName']; ?>">
                                                <input type="text" name="toEdit<?php echo $type; ?>Name[]" 
                                                       value="<?php echo $value['f_UserRoleName']; ?>" 
                                                       id="toEdit<?php echo $type; ?>Name<?php echo $i; ?>" disabled>
                                            </td>       
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                    ?>
                                <?php } elseif ($type == 'EmpRole') { ?>                                     
                                    <?php
                                    $i = 0;
                                    foreach ($data as $value) {
                                        ?>
                                        <tr>     
                                            <td>
                                                <input type="checkbox" name="toEdit<?php echo $type; ?>ID[]" 
                                                       value="<?php echo $value['f_EmpRoleID']; ?>" txtcon="toEdit<?php echo $type; ?>Name<?php echo $i; ?>"
                                                       id="toEdit<?php echo $type; ?>ID<?php echo $i; ?>" onclick="txtEnabler(this.id)">
                                            </td> 
                                            <td data-search="<?php echo $value['f_EmpRoleName']; ?>">
                                                <input type="text" name="toEdit<?php echo $type; ?>Name[]" 
                                                       value="<?php echo $value['f_EmpRoleName']; ?>" 
                                                       id="toEdit<?php echo $type; ?>Name<?php echo $i; ?>" disabled>
                                            </td>       
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                    ?>
                                <?php } ?>
                            </table>                            
                        </div><!-- /.col-lg-6 -->
                    </div>
                    <div id="ajaxmsgsEdit"></div>
                    <div class="modal-footer">
                        <input type="submit" style="visibility: hidden"/>
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-sm btn-primary"  id="<?php echo $type; ?>EditBtn" disabled>Submit</button>                    
                        <button type="reset" class="btn btn-sm btn-danger" id="<?php echo $type; ?>ResetBtn">Reset</button>
                    </div> <!-- /modal-footer -->	
                </form> 
            <?php } else { ?>
                <div class="well text-center"> No <?php echo $text; ?> Available</div>    
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                </div>
            <?php } ?>
        </div>

        <div class="tab-pane" id="Delete">
            <h4>Delete <?php echo $text; ?></h4> 
            <hr>
            <?php if (!is_null($data)) { ?>
                <form class="form-horizontal" role="form" id="Delete<?php echo $type; ?>Form">               
                    <div class="row">                           
                        <div class="col-lg-12">
                            <table class="table table-bordered table-responsive UpdateTable"> 
                                <thead>
                                    <tr>
                                        <th style="width: 20px;">Delete</th>
                                        <th><?php echo $text; ?></th>
                                    </tr>
                                </thead>

                                <?php if ($type == 'StoreType') { ?>
                                    <?php
                                    $i = 0;
                                    foreach ($data as $value) {
                                        ?>
                                        <tr>     
                                            <td>
                                                <input type="checkbox" name="toDelete<?php echo $type; ?>ID[]" 
                                                       value="<?php echo $value['f_StoreTypeID']; ?>" 
                                                       id="toDelete<?php echo $type; ?>ID[<?php echo $i; ?>]">
                                            </td> 
                                            <td data-search="<?php echo $value['f_StoreTypeName']; ?>">
                                                <input type="text" name="toDelete<?php echo $type; ?>Name[]" 
                                                       value="<?php echo $value['f_StoreTypeName']; ?>" 
                                                       id="toDelete<?php echo $type; ?>Name[<?php echo $i; ?>]" disabled></td>       
                                        </tr>
                                        <?php
                                    } $i++;
                                    ?>
                                <?php } elseif ($type == 'UserRole') { ?>                                    
                                    <?php
                                    $i = 0;
                                    foreach ($data as $value) {
                                        ?>
                                        <tr>     
                                            <td>
                                                <input type="checkbox" name="toDelete<?php echo $type; ?>ID[]" 
                                                       value="<?php echo $value['f_UserRoleID']; ?>" 
                                                       id="toDelete<?php echo $type; ?>ID[<?php echo $i; ?>]">
                                            </td> 
                                            <td data-search="<?php echo $value['f_UserRoleName']; ?>">
                                                <input type="text" name="toDelete<?php echo $type; ?>Name[]" 
                                                       value="<?php echo $value['f_UserRoleName']; ?>" 
                                                       id="toDelete<?php echo $type; ?>Name[<?php echo $i; ?>]" disabled>
                                            </td>       
                                        </tr>
                                        <?php
                                    } $i++;
                                    ?>
                                <?php } elseif ($type == 'EmpRole') { ?>                                     
                                    <?php
                                    $i = 0;
                                    foreach ($data as $value) {
                                        ?>
                                        <tr>     
                                            <td>
                                                <input type="checkbox" name="toDelete<?php echo $type; ?>ID[]" 
                                                       value="<?php echo $value['f_EmpRoleID']; ?>" 
                                                       id="toDelete<?php echo $type; ?>ID[<?php echo $i; ?>]">
                                            </td> 
                                            <td data-search="<?php echo $value['f_EmpRoleName']; ?>">
                                                <input type="text" name="toDelete<?php echo $type; ?>Name[]" 
                                                       value="<?php echo $value['f_EmpRoleName']; ?>" 
                                                       id="toDelete<?php echo $type; ?>Name[<?php echo $i; ?>]" disabled>
                                            </td>       
                                        </tr>
                                        <?php
                                    } $i++;
                                    ?>
                                <?php } ?>
                            </table>            
                        </div><!-- /.col-lg-6 -->
                    </div>
                    <div id="ajaxmsgsDelete"></div>
                    <div class="modal-footer">
                        <input type="submit" style="visibility: hidden"/>
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-sm btn-primary"  id="<?php echo $type; ?>DeleteBtn" disabled>Submit</button>                    
                        <button type="reset" class="btn btn-sm btn-danger" id="<?php echo $type; ?>ResetBtn">Reset</button>
                    </div> <!-- /modal-footer -->	
                </form> 
            <?php } else { ?>
                <div class="well text-center"> No <?php echo $text ?> Available</div>    
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<!--<script>
    $('#CheckBox').change(function () {
        $("#textBox").prop("disabled", !$(this).is(':checked'));
    });
</script>

<input type="checkbox" id="CheckBox" name="isNews"/>
<input id="textBox" name="newsSource" class="input-xlarge" disabled="" type="text" placeholder="news source"/>-->




