<?php
$storeID = $_GET['storeID'];
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/StoreFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');

//$storeData = SelectStore('GetSharedList', $storeID);
$mergeStore = SelectMergeStore($storeID);

foreach($mergeStore as $storeData){
  $newData['Data'] = array('StoreName'=> $storeData['f_StoreListName'] , 'CompanyCode' => $storeData['f_CompanyCode'] , 'StoreID' => $storeData['f_StoreID'] );
   
    $newData['MergeCode'][] = array('MergeCodeName'=> $storeData['f_MergeToCompanyCode'] , 'MergeID' => $storeData['f_MergeReportID'] ); 
}
//echo "<pre>";
//print_r($newData);

?>
<script type="text/javascript" language="javascript"  src="admin/js/MergeModal.js"></script>
<style>.redBackground{
        background-color:red;
    }</style>

<div class="modal-body"> 
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>  
    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
        <li class="active"><a href="#Add" data-toggle="tab">Add</a></li>
        <li><a href="#Edit" data-toggle="tab" onclick="onLoadEditMergeStoreForm()">Edit</a></li>
        <li><a href="#Delete" data-toggle="tab" onclick="onLoadDeleteMergeStoreForm()">Delete</a></li>
      
    </ul>

    <div id="my-tab-content" class="tab-content">
        <div class="tab-pane active" id="Add">
            <h4>Add Merge Store</h4>
            <form class="form-horizontal" role="form" id="AddMergeStoreForm"> 
                <div class="form-group"  id="elementsToOperateOn0" class="col-sm-3">
                    <label class="control-label col-sm-2" for="email"></label>
                    <div  class="col-sm-6">                       
                        <input type="text" name="MergeWith[]" class="form-control" id="shareowner0"  required/>
                    </div> 
                    <button id="bntShow0" class="btn btn-sm btn-info" type="button" onclick="showDiv(this.id)">+</button>
                </div>   <!-- /div for share store owner -->

                <?php for ($loop = 1; $loop < 10; $loop++) { ?> 
                    <div class="form-group" id="elementsToOperateOn-<?php echo $loop; ?>" style="display:none" class="col-sm-3">
                        <label class="control-label col-sm-2" for="email"></label>
                        <div  class="col-sm-6">                            
                            <input type="text" name="MergeWith[]" class="form-control"  id="shareowner<?php echo $loop; ?>" required disabled />
                        </div>  <button id="btnShow<?php echo $loop; ?>" class="btn btn-sm btn-info" type="button" onclick="showDiv(this.id)">+</button>
                        <button id="btnHide<?php echo $loop; ?>" class="btn btn-sm btn-warning" type="button" onclick="hideDiv(this.id)">-</button>
                    </div>  <!-- /elementsToOperateOn div loop -->
                <?php } ?> <!-- loop for hidden dropdown -->
                <input type="hidden" name="storeID" value="<?php echo $newData['Data']['StoreID']; ?>" />
                <input type="hidden" name="companycode" value="<?php echo $newData['Data']['CompanyCode']; ?>"/>
                <div id="ajaxmsgsadd"></div>
                <div class="modal-footer">
                    <input type="submit" style="visibility: hidden"/>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id="cancel">Cancel</button>
                    <button type="button" class="btn btn-sm btn-primary" id="AddMergeStoreSubmit" name="AddMergeStoreSubmit" disabled="">Submit</button>
                    <button type="reset" class="btn btn-sm btn-danger" id="AddMergeStoreReset">Reset</button>
                </div> <!-- /modal-footer -->	
            </form>
        </div>

        <div class="tab-pane" id="Edit">
            <h4 class="tochangeedit">Edit Merged Store</h4>
            <?php if ($newData['MergeCode'][0]['MergeCodeName'] != "") { ?>
                <form class="form-horizontal" role="form" id="EditMergeStoreForm">  
                    <?php $i = 0 ;
                    foreach ($newData['MergeCode'] as $mergeData) { ?>
                        <div class="row">
                            <label class="control-label col-sm-2" for="StoreOwnerID"></label>
                            <div  class="input-group col-sm-6">
                                 <span class="input-group-addon">
                                        <input type="checkbox" name="toEdit" class="enabler" id="enabler" toEnable="toEnable<?php echo $i; ?>" toEnableTxb="mergeTxb<?php echo $i; ?>" >
                                    </span>
                                <input type="text" class="form-control setreadonly" value="<?php echo $mergeData['MergeCodeName'];?>" name="mergeTxb[]" id="mergeTxb<?php echo $i; ?>"  readonly />
                            </div> 
                            <input type="hidden" name="MergeID[] setdisabled" value="<?php echo $mergeData['MergeID']; ?>" id="toEnable<?php echo $i; ?>" disabled />
                            <input type="hidden" name="storeID" value="<?php echo $storeID; ?>" />
                        </div></br>
                    <?php $i++; } ?>
                    <div id="ajaxmsgsedit"></div>
                    <div class="modal-footer">
                        <input type="submit" style="visibility: hidden"/>
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id="EditMergeStoreCancel">Cancel</button>
                        <button type="button" class="btn btn-sm btn-primary"  id="EditMergeStoreSubmit" name="EditMergeStoreSubmit" disabled="">Submit</button>                    
                        <button type="reset" class="btn btn-sm btn-danger" id="EditMergeStoreReset">Reset</button>                
                    </div> <!-- /modal-footer -->	
                </form>
            <?php } else { ?>
                <div class="well text-center"> No Merged Store Available</div>    
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                </div>
            <?php } ?>
        </div>
        <div class="tab-pane" id="Delete">
            <h4>Delete Merged Store</h4>
            <?php if ($newData['MergeCode'][0]['MergeCodeName'] != "") { ?>
                <form class="form-horizontal" role="form" id="DeleteMergeStoreForm"> 
                    <?php $i = 0;
                    foreach ($newData['MergeCode'] as $mergeData) { ?>
                        <div class="row">
                            <label class="control-label col-sm-2" for="StoreOwnerID"></label>

                            <div class="col-lg-6" data-toggle="tooltip" data-placement="left" title="Check to include in delete"> 
                                <div class="input-group" >
                                    <span class="input-group-addon">
                                        <input type="checkbox" name="toDelete[]" value="<?php echo $mergeData['MergeID']; ?>">
                                    </span>
                                    <input type="text" class="form-control" value="<?php echo $mergeData['MergeCodeName'];?>" disabled />
                                </div><!-- /input-group -->
                            </div><!-- /.col-lg-6 -->
                        </div></br>
                    <?php $i++;} ?>
                    <div id="ajaxmsgsdelete"></div>
                    <div class="modal-footer">
                        <input type="submit" style="visibility: hidden"/>
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id="DeleteMergeCancel">Cancel</button>
                        <button type="button" class="btn btn-sm btn-primary"  id="DeleteMergeStoreSubmit" name="DeleteMergeStoreSubmit" disabled="">Submit</button>                    
                        <button type="reset" class="btn btn-sm btn-danger" id="DeleteMergeStoreReset">Reset</button>
                    </div> <!-- /modal-footer -->	
                </form> 
            <?php } else { ?>
                <div class="well text-center"> No Merged Store Available</div>    
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                </div>
            <?php } ?>
        </div>

    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#tabs').tab();
    });
</script>    

