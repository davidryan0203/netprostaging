<?php
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/UserFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/StoreFunction.php');
$storeID = $_GET['storeID'];
//$storeOwnerID = $_GET['SharedOwnerStoreID'];
$storeData = SelectStore('GetSharedList',$storeID);
//$roleList = SelectStoreRoleList();
$owner = SelectUser('Owner');
//var_dump($storeData);

foreach ($storeData as $newData) {
    $mergeStoreData[$newData['SharedStoreID']]['SharedStoreID'] = $newData['SharedStoreID'];
    $mergeStoreData[$newData['SharedStoreID']]['SharedByUserName'] = $newData['SharedByUserName'];
    $mergeStoreData[$newData['SharedStoreID']]['SharedStoreName'] = $newData['SharedStoreName'];
    $mergeStoreData[$newData['SharedStoreID']]['SharedByUserID'] = $newData['SharedByUserID'];
    $mergeStoreData[$newData['SharedStoreID']]['SharedToUserID'] = $newData['SharedToUserID'];

    $mergeStoreData[$newData['SharedStoreID']]['SharedCompanyCode'] = $newData['SharedCompanyCode'];
    $mergeStoreData[$newData['SharedStoreID']]['f_Shared'][] = array('SharedStoreID' => $newData['f_SharedStoreID'], 'SharedtoStoreID' => $newData['f_SharedToStoreID'], 'SharedToStoreName' => $newData['SharedToStoreName']);
    //$new = array_merge($newData2,$newData2);
}

//var_dump($userData);
?>
<script type="text/javascript">

    $(function () {
        $("#ajaxmsgs").hide();
    }); // default hide divs for modal open
    $(document).ready(function () {

        $('#btnSubmit').on('click', function (event) {            
            var url = 'admin/function/StoreFunctionCaller.php?module=DeleteStore';          
            var mydata = $("#DeleteStoreForm").serialize();
            $.ajax({
                url: url,
                type: 'get',
                data: mydata,
                dataType: 'json',
                success: function (result) {
                    document.getElementById('ajaxmsgs').style.display = 'block';
                    if (result.type == 'Success') {
                        $('#ajaxmsgs').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong><br>" + result.message + "</div>");
                        $("#DeleteStoreForm :input").attr("disabled", true);
                            $("#cancel").attr("disabled", false).html('Close');
                   }
                    else {
                        $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong><br>" + result.message + "</div>");
                    }
                    //alert('success error');
                },
                error: function () {
                    document.getElementById('ajaxmsgs').style.display = 'block';
                    $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
                    //alert('here error');
                }
            });
            event.preventDefault();
        }); // ajax button form submit
    });
</script>
<style type="text/css">  
    .successuser{
        color:#009900;
    }
    .erroruser{
        color:#F33C21;
    }
    .username_avail_result .checkPass{
        display:block;
        width:180px;
    }</style>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Delete Store</h4>
</div>	
<form class="form-horizontal" role="form" id="DeleteStoreForm"><!-- /modal-header -->
    <div class="modal-body">
        <p>Are you sure you want to delete this Store?</p>
        <div class="well"><h4>Store details</h4>
            <?php foreach ($mergeStoreData as $value) { ?>

                &nbsp;Store Name:<b> <?php echo $value['SharedStoreName']; ?></b></br>
                &nbsp;Store Code:<b> <?php echo $value['SharedCompanyCode']; ?> </b></br>
                &nbsp;Store Owner:<b> <?php echo $value['SharedByUserName']; ?></b></br>
                &nbsp;Share To:
                <table><colgroup>
                        <col style="width: 20%">
                    <col style="width:80%"><tr><th></th><th></th></tr>
                    <?php foreach ($value['f_Shared'] as $sharevalue) { ?>
                        <input type="hidden" name="ShareStoreID[]" value="<?php echo $sharevalue['SharedStoreID'];?>"/>
                        <tr><td></td><td><b><?php echo $sharevalue['SharedToStoreName']; ?></b></td> </tr>
                    <?php } ?></table>
            <?php }
            ?>
        </div>

        <input type="hidden" name="StoreListID" id="StoreListID" value="<?php echo $storeID; ?>" /> 
        <div id="ajaxmsgs"></div>  <!-- ajaxmsg div -->
    </div> <!-- /modal-body -->	
    <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id="cancel">Cancel</button>
        <button type="button" class="btn btn-sm btn-danger"  id="btnSubmit" name="btnSubmit" >Submit</button>
    </div> <!-- /modal-footer -->	
</form>  <!-- /form --> 

