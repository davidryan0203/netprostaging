<?php
$storeID = $_GET['storeID'];
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/StoreFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');

$storeData = SelectStore('GetSharedList', $storeID);
$storeList = SelectStoreList();

foreach ($storeData as $newData) {
    $mergeStoreData[$newData['SharedStoreID']]['StoreID'] = $newData['SharedStoreID'];
    $mergeStoreData[$newData['SharedStoreID']]['SharedByUserName'] = $newData['SharedByUserName'];
    $mergeStoreData[$newData['SharedStoreID']]['SharedStoreName'] = $newData['SharedStoreName'];
    $mergeStoreData[$newData['SharedStoreID']]['SharedByUserID'] = $newData['SharedByUserID'];
    $mergeStoreData[$newData['SharedStoreID']]['SharedToUserID'] = $newData['SharedToUserID'];

    $mergeStoreData[$newData['SharedStoreID']]['SharedCompanyCode'] = $newData['SharedCompanyCode'];
    $mergeStoreData[$newData['SharedStoreID']]['f_Shared'][] = array('SharedStoreID' => $newData['f_SharedStoreID'], 'SharedtoStoreID' => $newData['f_SharedToStoreID'], 'SharedToStoreName' => $newData['SharedToStoreName']);
    //$new = array_merge($newData2,$newData2);
    $beenShared[] = $newData['f_SharedToStoreID'];
}
?>
<script type="text/javascript" language="javascript"  src="admin/js/EditStoreSharedModal.js"></script>
<style>.redBackground{
        background-color:red;
    }</style>

<div class="modal-body"> 
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>  
    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
        <li class="active"><a href="#Add" data-toggle="tab">Add</a></li>
        <li><a href="#Edit" data-toggle="tab" onclick="onLoadEditStoreForm()">Edit</a></li>
        <li><a href="#Delete" data-toggle="tab" onclick="onLoadDeleteStoreForm()">Delete</a></li>
    </ul>

    <div id="my-tab-content" class="tab-content">
        <div class="tab-pane active" id="Add">
            <h4>Add Shared Store</h4>
            <form class="form-horizontal" role="form" id="AddStoreForm"> 
                <div class="form-group"  id="elementsToOperateOn0" class="col-sm-3">
                    <label class="control-label col-sm-2" for="email"></label>
                    <div  class="col-sm-6">
                        <select name="storeShareTo[]" class="form-control AddShare" id="shareTo0" required>
                            <option value="" >Please select</option>
                            <?php
                            foreach ($storeList as $value) {
                                if (in_array($value['f_StoreID'], $beenShared)) {
                                    continue;
                                }
                                ?>
                                <option value="<?php echo $value['f_StoreID']; ?>" ownerid="<?php echo $value['f_StoreOwnerUserID'] ?>"> <?php echo $value['f_StoreListName']; ?></option>
                            <?php } ?>
                        </select>
                        <input type="hidden" name="shareStoreOwnerID[]" id="shareowner0" value="" disabled/>
                    </div> 
                    <button id="bntShow0" class="btn btn-sm btn-info" type="button" onclick="showDiv(this.id)">+</button>
                </div>   <!-- /div for share store owner -->

                <?php for ($loop = 1; $loop < 10; $loop++) { ?> 
                    <div class="form-group" id="elementsToOperateOn-<?php echo $loop; ?>" style="display:none" class="col-sm-3">
                        <label class="control-label col-sm-2" for="email"></label>
                        <div  class="col-sm-6">
                            <select name="storeShareTo[]" class="form-control AddShare"  id="shareTo<?php echo $loop; ?>" disabled="disabled" required>
                                <option value="">Please select</option>
                                <?php
                                foreach ($storeList as $value) {
                                    if (in_array($value['f_StoreID'], $beenShared)) {
                                        continue;
                                    }
                                    ?>
                                    <option value="<?php echo $value['f_StoreID']; ?>"  ownerid="<?php echo $value['f_StoreOwnerUserID'] ?>"> <?php echo $value['f_StoreListName']; ?></option>
                                <?php } ?>
                            </select> 
                            <input type="hidden" name="shareStoreOwnerID[]"  id="shareowner<?php echo $loop; ?>" value="" disabled />
                        </div>  <button id="btnShow<?php echo $loop; ?>" class="btn btn-sm btn-info" type="button" onclick="showDiv(this.id)">+</button>
                        <button id="btnHide<?php echo $loop; ?>" class="btn btn-sm btn-warning" type="button" onclick="hideDiv(this.id)">-</button>
                    </div>  <!-- /elementsToOperateOn div loop -->
                <?php } ?> <!-- loop for hidden dropdown -->
                <input type="hidden" name="storeID" value="<?php echo $storeID; ?>" />
                <div id="ajaxmsgsadd"></div>
                <div class="modal-footer">
                    <input type="submit" style="visibility: hidden"/>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id="cancel">Cancel</button>
                    <button type="button" class="btn btn-sm btn-primary" id="AddStoreSubmit" name="AddStoreSubmit"  disabled="">Submit</button>
                    <button type="reset" class="btn btn-sm btn-danger" id="AddStoreReset">Reset</button>
                </div> <!-- /modal-footer -->	
            </form>
        </div>

        <div class="tab-pane" id="Edit">
            <h4 class="tochangeedit">Edit Shared Store</h4>
            <?php if (!is_null($mergeStoreData[$storeID]['f_Shared'][0]['SharedStoreID'])) { ?>
                <form class="form-horizontal" role="form" id="EditStoreForm">  
                    <?php $i = 0 ;
                    foreach ($mergeStoreData[$storeID]['f_Shared'] as $sharedData) { ?>
                        <div class="row">
                            <label class="control-label col-sm-2" for="StoreOwnerID"></label>
                            <div  class="col-sm-6">
                                <select name="storeShareTo[]" class="form-control EditShare" id="storeShareTo[<?php echo $i; ?>]">
                                    <option value="">Please select</option>
                                    <?php foreach ($storeList as $value) { ?>
                                        <option value="<?php echo $value['f_StoreID']; ?>" <?php
                                        if ($sharedData['SharedtoStoreID'] == $value['f_StoreID']) {
                                            echo "selected='selected'";
                                        }
                                        ?> > <?php echo $value['f_StoreListName']; ?></option>
                                            <?php } ?>
                                </select>  
                            </div> 
                            <input type="hidden" name="SharedStoreID[]" value="<?php echo $sharedData['SharedStoreID']; ?>" />
                            <input type="hidden" name="storeID" value="<?php echo $storeID; ?>" />
                        </div></br>
                    <?php $i++; } ?>
                    <div id="ajaxmsgsedit"></div>
                    <div class="modal-footer">
                        <input type="submit" style="visibility: hidden"/>
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id="cancel">Cancel</button>
                        <button type="button" class="btn btn-sm btn-primary"  id="EditStoreSubmit" name="EditStoreSubmit" disabled="">Submit</button>                    
                        <button type="reset" class="btn btn-sm btn-danger" id="EditStoreReset">Reset</button>                
                    </div> <!-- /modal-footer -->	
                </form>
            <?php } else { ?>
                <div class="well text-center"> No Shared Store Available</div>    
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                </div>
            <?php } ?>
        </div>
        <div class="tab-pane" id="Delete">
            <h4>Delete Shared Store</h4>
            <?php if (!is_null($mergeStoreData[$storeID]['f_Shared'][0]['SharedStoreID'])) { ?>
                <form class="form-horizontal" role="form" id="DeleteStoreForm"> 
                    <?php $i = 0;
                    foreach ($mergeStoreData[$storeID]['f_Shared'] as $sharedData) { ?>
                        <div class="row">
                            <label class="control-label col-sm-2" for="StoreOwnerID"></label>

                            <div class="col-lg-6" data-toggle="tooltip" data-placement="left" title="Check to include in delete"> 
                                <div class="input-group" >
                                    <span class="input-group-addon">
                                        <input type="checkbox" name="toDelete[]" value="<?php echo $sharedData['SharedStoreID']; ?>" id="toDelete[<?php echo $i; ?>]">
                                    </span>
                                    <select name="storeShareTo[]" class="form-control" disabled  >
                                        <option value="">Please select</option>
                                        <?php foreach ($storeList as $value) { ?>
                                            <option value="<?php echo $value['f_StoreID']; ?>" <?php
                                            if ($sharedData['SharedtoStoreID'] == $value['f_StoreID']) {
                                                echo "selected='selected'";
                                            }
                                            ?> > <?php echo $value['f_StoreListName']; ?></option>
                                                <?php } ?>
                                    </select> 
                                </div><!-- /input-group -->
                            </div><!-- /.col-lg-6 -->
                        </div></br>
                    <?php $i++;} ?>
                    <div id="ajaxmsgsdelete"></div>
                    <div class="modal-footer">
                        <input type="submit" style="visibility: hidden"/>
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id="cancel">Cancel</button>
                        <button type="button" class="btn btn-sm btn-primary"  id="DeleteStoreSubmit" name="DeleteStoreSubmit" disabled="">Submit</button>                    
                        <button type="reset" class="btn btn-sm btn-danger" id="DeleteStoreReset">Reset</button>
                    </div> <!-- /modal-footer -->	
                </form> 
            <?php } else { ?>
                <div class="well text-center"> No Shared Store Available</div>    
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                </div>
            <?php } ?>
        </div>

    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#tabs').tab();
    });
</script>    