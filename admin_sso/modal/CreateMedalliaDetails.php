<?php
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/admin/function/UserFunction.php');


$storeList = SelectStoreList();
$last_day_this_month = date('d/m/Y');

session_start();
$role = $_SESSION['UserRoleID'];
$stack = array('2', '3');
if ($role == '5') {
    // can only create his own
    $query = $_SESSION['EmpProfileID'];
//    $type = "StoreLeader";
    $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
                    left join t_empnumber b on a.f_EmpID = b.f_EmpID
                    left join t_storelist c on b.f_StoreID = c.f_StoreID
                    where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1";
    $result = mysql_query($sqlGetOwnerID, $connection);
    while ($row = mysql_fetch_assoc($result)) {
        $ownerID = $row['f_StoreOwnerUserID'];
    }

    //can only create his own
    $ownerID = $_SESSION['UserProfileID'];
    $type = "StoreOwner";
} elseif ($role == '1' || $role = '2') {
    // all
    $type = "GlobalAdmin";
    $owner = SelectUser('Owner');
//    $query = NULL;
}


$sqlGetStoreType = "SELECT * FROM t_storetype";
$resultStoreType = mysql_query($sqlGetStoreType, $connection);
while ($rowStoreType = mysql_fetch_assoc($resultStoreType)) {
    $storeType[] = $rowStoreType;
}
//change to detect if admin else set to default owner
?>
<script type="text/javascript">

    $(function() {
        $("#sharedDiv,#ajaxmsgs,#elementsToOperateOn0").hide();
    }); // default hide divs for modal open


    $(document).ready(function() {

        $('select').on('change', function(e) {
            $('.CreateShare').find('option').prop('disabled', false);
            $('.CreateShare').each(function() {
                $('.CreateShare').not(this).find('option[value="' + this.value + '"]').prop('disabled', true);
            });
        });

        $('select').on('change', function(e) {
            var exclude = ["StoreOwnerID", "StoreTypeID"];
            var optionName = $(this).attr('name');
            var optionid = $(this).attr('id');

            if ($.inArray(optionName, exclude) == -1) {
                var ownerID = $('option:selected', this).attr('ownerid');
                var txtboxID = optionid.charAt(optionid.length - 1);
                $('#shareowner' + txtboxID).val(ownerID)
                        .prop("disabled", false);
                // alert('#shareowner' + txtboxID);
            }
            e.preventDefault();
        });



        $('#UserName').keyup(function() { // Keyup function for check the user action in input
            var UserName = $(this).val(); // Get the username textbox using $(this) or you can use directly $('#username')
            var StoreCodeAvailResult = $('#storecode_avail_result'); // Get the ID of the result where we gonna display the results
            if (UserName.length > 4) { // check if greater than 5 (minimum 6)
                StoreCodeAvailResult.html('Loading..'); // Preloader, use can use loading animation here
                var UrlToPass = 'action=medallia_availability&UserName=' + UserName;

                $.ajax({// Send the username val to another checker.php using Ajax in POST menthod
                    type: 'GET',
                    data: UrlToPass,
                    url: 'admin/function/CheckUserName.php?',
                    success: function(responseText) { // Get the result and asign to each cases
                        console.log(responseText);
                        if (responseText == 0) {
                            StoreCodeAvailResult.html('<span class="successuser">Medallia account can be added</span>');
                        } else if (responseText > 0) {
                            StoreCodeAvailResult.html('<span class="erroruser">Medallia account is already in our list, please contact Netpro Admin.</span>');
                        } else {
                            alert('Problem with server process');
                        }
                    }
                });
            } else {
                StoreCodeAvailResult.html('Enter atleast 5 characters');
            }
            if (UserName.length == 0) {
                StoreCodeAvailResult.html('');
            }
        });


        $('#btnSubmit').on('click', function(event) {
            if ($("#storeFM")[0].checkValidity()) {
                var url = 'admin/function/SubFunctionCaller.php?module=CreateMedallia';
                var mydata = $("#storeFM").serialize();
                // return false;
                console.log(url + mydata);
                $.ajax({
                    url: url,
                    type: 'get',
                    data: mydata,
                    dataType: 'json',
                    success: function(result) {
                        console.log(result);
                        document.getElementById('ajaxmsgs').style.display = 'block';
                        if (result.type == 'Success') {
                            $('#ajaxmsgs').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong><br>" + result.message + "</div>");
                            $("#storeFM :input").attr("disabled", true);
                            $("#cancel").attr("disabled", false).html('Close');
                        } else {
                            $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong><br>" + result.message + "</div>");
                        }
                        //alert('success error');
                    },
                    error: function(error) {
                        console.log(error)
                        document.getElementById('ajaxmsgs').style.display = 'block';
                        $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
                        //alert('here error');
                    }
                });
            } else {
                $("#storeFM").find(':submit').click();
            }
            event.preventDefault();
        }); // ajax button form submit
    }); // check box toggle for div

    function showDiv(cliked_id) {
        // alert('here');
        var fieldNum = cliked_id.charAt(cliked_id.length - 1);
        var newID = parseInt(fieldNum) + 1;
        var fieldID = "elementsToOperateOn-" + newID;
        var idfield = "#" + fieldID + " :input";
        document.getElementById(fieldID).style.display = 'block';
        $(idfield).prop("disabled", false).fadeIn('slow');
    } // unhide hidden div and enable input fields for shared to user

    function hideDiv(cliked_id) {
        var fieldNum = cliked_id.charAt(cliked_id.length - 1);
        var newID = parseInt(fieldNum);
        var fieldID = "elementsToOperateOn-" + newID;
        var idfield = "#" + fieldID + " :input";
        document.getElementById(fieldID).style.display = 'none';
        $(idfield).prop("disabled", true).fadeOut('slow');
    } // hide hidden div and disable input field for shared to user

    $(function() {
        $('[data-tooltip="tooltip"]').tooltip();
    });

    $("#expire").datepicker({
        //showOn: "button",
        buttonImage: "https://jqueryui.com/resources/demos/datepicker/images/calendar.gif",
        buttonImageOnly: true, dateFormat: 'dd/mm/yy',
        numberOfMonths: 2,
        onSelect: function(selected) {
            $("#expire").datepicker("option", "minDate", selected);
        }
    });
</script>

<style type="text/css">
    .successuser{
        color:#009900;
    }
    .erroruser{
        color:#F33C21;
    }
    .storecode_avail_result{
        display:block;
        width:180px;
    }

    .ui-datepicker-trigger {
        z-index:9999!important;
    }
</style>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Create Store</h4>
</div>
<form class="form-horizontal" role="form" id="storeFM"><!-- /modal-header -->
    <div class="modal-body">

        <div class="form-group">
            <label class="control-label col-sm-3" for="StoreListName">User Name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="UserName" name="UserName" placeholder="Enter username name" required >
                <div class="storecode_avail_result" id="storecode_avail_result"></div>
            </div>
        </div> <!-- /div for store name -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="StoreCode">Password:</label>
            <div class="col-sm-6">
                <input type="password" class="form-control" id="Password" name="Password" placeholder="Enter password" required >
            </div>
        </div> <!-- /div for store code -->
        <div class="form-group">
            <label class="control-label col-sm-3" for="StoreListName">Password Expiration:</label>
            <div class="col-sm-6">
                <input type="text" id="expire" name="expire"  class="form-control" value="<?php echo $last_day_this_month; ?>" />
            </div>
        </div> <!-- /div for store name -->
        <?php if ($role == 3 || $role == 5) { ?>
                                                                                                                                                                                                                <!--<select name="StoreOwnerID" class="form-control" style="display:none"><option value="<?php echo $ownerID; ?>" selected="selected"></option></select>-->
            <input type="hidden" name="
                   " value="<?php echo $ownerID; ?>"/>
               <?php } elseif ($role == 1 || $role == 2) { ?>
            <div class="form-group">
                <label class="control-label col-sm-3" for="StoreOwnerID">Store Owner:</label>
                <div class="col-sm-6">
                    <select name="StoreOwnerID" class="form-control" required >
                        <option value="">Please select</option>
                        <?php
                        foreach ($owner as $value) {
                            echo '<option value="' . $value['f_UserID'] . '">' . $value['f_UserFirstName'] . " " . $value['f_UserLastName'] . " " . '</option>';
                        }
                        ?>
                    </select>
                </div>
            </div> <!-- /div for store owner -->
        <?php } ?>



        <div id="ajaxmsgs"></div>  <!-- ajaxmsg div -->
    </div>
    <div class="modal-footer">
        <input type="submit" style="visibility: hidden"/>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal" id="cancel">Cancel</button>
        <button type="button" class="btn btn-sm btn-primary"  id="btnSubmit" name="btnSubmit">Submit</button>
        <button type="reset" class="btn btn-sm btn-danger" >Reset</button>
    </div> <!-- /modal-footer -->
</form>  <!-- /form -->

