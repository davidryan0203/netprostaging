
// <editor-fold defaultstate="collapsed" desc=" ${InitialLoad} ">
$(document).ready(function () {

    $("#ajaxmsgsadd,#ajaxmsgsdelete,#ajaxmsgsedit").hide();

    $('.EditShare').each(function () {
        $('.EditShare').not(this).find('option[value="' + this.value + '"]').prop('disabled', true);
    });

    $('select').on('change', function (e) {
        var exclude = "StoreOwnerID";
        var optionName = $(this).attr('name');
        var optionid = $(this).attr('id');
        var form = $(this).parents("form").attr("id");      
        if (optionName !== exclude && form !== 'EditStoreForm') {
            var ownerID = $('option:selected', this).attr('ownerid');
            var txtboxID = optionid.charAt(optionid.length - 1);
            $('#shareowner' + txtboxID).val(ownerID)
                    .prop("disabled", false);

          
        }


        $('.AddShare').find('option').prop('disabled', false);
        $('.AddShare').each(function () {
            $('.AddShare').not(this).find('option[value="' + this.value + '"]').prop('disabled', true);
          
        });
        $('.EditShare').find('option').prop('disabled', false);
        $('.EditShare').each(function () {
            $('.EditShare').not(this).find('option[value="' + this.value + '"]').prop('disabled', true);
        });

        e.preventDefault();
    });
});

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" ${AddMergeStore} ">
$('#AddMergeStoreSubmit').on('click', function (event) {

    if ($("#AddMergeStoreForm")[0].checkValidity()) {
        var url = 'admin/function/StoreFunctionCaller.php?module=AddMergeStore';
       
        var mydata = $("#AddMergeStoreForm").serialize();

        $.ajax({
            url: url,
            type: 'get',
            data: mydata,
            dataType: 'json',
            success: function (result) {
                document.getElementById('ajaxmsgsadd').style.display = 'block';
                if (result.type === 'Success') {
                    $('#ajaxmsgsadd').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong>" + result.message + "</div>");
                    $("#AddMergeStoreForm :input").attr("disabled", true);
                    $("#cancel").attr("disabled", false).html('Close');
                }
                else {
                    $('#ajaxmsgsadd').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong>" + result.message + "</div>");
                }
                //alert('success error');
            },
            error: function () {
                document.getElementById('ajaxmsgsadd').style.display = 'block';
                $('#ajaxmsgsadd').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
                //alert('here error');
            }
        });
    } else {
        $("#AddMergeStoreForm").find(':submit').click();
    }
    event.preventDefault();
}); // ajax button form submit
var buttonAddMergeStore = $('#AddMergeStoreSubmit');
var origAddMergeStore = [];

$.fn.getTypeAddMergeStore = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("#AddMergeStoreForm :input").each(function () {
    var type = $(this).getTypeAddMergeStore();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
    origAddMergeStore[$(this).attr('id')] = tmp;
});

// console.log(origAddMergeStore);
$('#AddMergeStoreForm').bind('change keyup', function () {

    var disable = true;
    $("#AddMergeStoreForm :input").each(function () {
        var type = $(this).getTypeAddMergeStore();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select') {
            disable = (origAddMergeStore[id].value == $(this).val());

        } else if (type == 'radio') {
            disable = (origAddMergeStore[id].checked == $(this).is(':checked'));
        }
        else if (type == 'checkbox') {
            var chkVal = '';
            if (origAddMergeStore[id].value == 'on') {
                chkVal = true;
            }
            else {
                chkVal = false;
            }
            disable = (chkVal == $(this).is(':checked'));
        }

        if (!disable) {
            return false; // break out of loop
        }
    });

    buttonAddMergeStore.prop('disabled', disable);

});

$('#AddMergeStoreReset').click(function () {
    //$('#input1').attr('name', 'other_amount');
    buttonAddMergeStore.prop('disabled', true);
});
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" ${EditMergeStore} ">

$('#EditMergeStoreSubmit').on('click', function (event) {
    if ($("#EditMergeStoreForm")[0].checkValidity()) {
        var url = 'admin/function/StoreFunctionCaller.php?module=EditMergeStore';        
        var mydata = $("#EditMergeStoreForm").serialize();    
        $.ajax({
            url: url,
            type: 'get',
            data: mydata,
            dataType: 'json',
            success: function (result) {
                document.getElementById('ajaxmsgsedit').style.display = 'block';
                if (result.type === 'Success') {
                    $('#ajaxmsgsedit').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!<br></strong>" + result.message + "</div>");
                    $("#EditMergeStoreForm :input").attr("disabled", true);
                    $("#EditMergeStoreCancel").attr("disabled", false).html('Close');
                }
                else {
                    $('#ajaxmsgsedit').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!<br></strong>" + result.message + "</div>");
                }
                
            },
            error: function () {
                document.getElementById('ajaxmsgsedit').style.display = 'block';
                $('#ajaxmsgsedit').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
                
            }
        });
    } else {
        $("#EditMergeStoreForm").find(':submit').click();
    }
    event.preventDefault();
}); // ajax button form submit

function onLoadEditMergeStoreForm() {
    var buttonEditMergeStore = $('#EditMergeStoreSubmit');
    var origEditMergeStore = [];

    $.fn.getTypeEditMergeStore = function () {
        return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
    }

    $("#EditMergeStoreForm :input").each(function () {
        var type = $(this).getTypeEditMergeStore();
        var tmp = {
            'type': type,
            'value': $(this).val()
        };
        if (type == 'radio') {
            tmp.checked = $(this).is(':checked');
        }
        origEditMergeStore[$(this).attr('id')] = tmp;
    });

    $('#EditMergeStoreForm').bind('change keyup', function () {

        var disable = true;
        $("#EditMergeStoreForm :input").each(function () {
            var type = $(this).getTypeEditMergeStore();
            var id = $(this).attr('id');

            if (type == 'checkbox') {

            }

            if (type == 'text' || type == 'select') {
                disable = (origEditMergeStore[id].value == $(this).val());

            }
            if (!disable) {
                return false; // break out of loop
            }

        });

        buttonEditMergeStore.prop('disabled', disable);
    });

    $('#EditMergeStoreReset').click(function () {
        $('.setdisabled').prop('disabled', true);
        $('.setreadonly').attr('readonly', true);
        $('.enabler').parent().removeClass("redBackground");
        //$('#input1').attr('name', 'other_amount');
        buttonEditMergeStore.prop('disabled', true);
    });
     $("#EditMergeStoreForm :input.setreadonly").prop("disabled", true);
}

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" ${DeleteMergeStore} ">
$('#DeleteMergeStoreSubmit').on('click', function (event) {
    var url = 'admin/function/StoreFunctionCaller.php?module=DeleteMergeStore';   
    var mydata = $("#DeleteMergeStoreForm").serialize();
   
    $.ajax({
        url: url,
        type: 'get',
        data: mydata,
        dataType: 'json',
        success: function (result) {
            document.getElementById('ajaxmsgsdelete').style.display = 'block';
            if (result.type === 'Success') {
                $('#ajaxmsgsdelete').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong>" + result.message + "</div>");
                $("#DeleteMergeStoreForm :input").attr("disabled", true);
                $("#DeleteMergeCancel").attr("disabled", false).html('Close');
            }
            else {
                $('#ajaxmsgsdelete').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong>" + result.message + "</div>");
            }
            
        },
        error: function () {
            document.getElementById('ajaxmsgsdelete').style.display = 'block';
            $('#ajaxmsgsdelete').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
           
        }
    });
    event.preventDefault();
}); // ajax button form submit

function onLoadDeleteMergeStoreForm() {
    var buttonDeleteMergeStore = $('#DeleteMergeStoreSubmit');
    $('#DeleteMergeStoreForm').bind('change keyup', function () {
        var disable = true;
        if ($("#DeleteMergeStoreForm input:checkbox:checked").length > 0) {
            disable = false;
        } // any one is checked     
        else {
            disable = true;
        } // none is checked
        buttonDeleteMergeStore.prop('disabled', disable);
    });

    $('#DeleteMergeStoreReset').click(function () {
        //$('#input1').attr('name', 'other_amount');
        buttonDeleteMergeStore.prop('disabled', true);
        $("input[type='checkbox']").parent().removeClass("redBackground");
    });


}

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" ${Extra} ">
function showDiv(cliked_id) {

    var fieldNum = cliked_id.charAt(cliked_id.length - 1);
    var newID = parseInt(fieldNum) + 1;
    var fieldID = "elementsToOperateOn-" + newID;
    var idfield = "#" + fieldID + " :input";
    $("#shareTo" + newID).val($("#shareTo" + newID + " option:first").val());

    //  alert(idfield);
    document.getElementById(fieldID).style.display = 'block';
    $(idfield).prop("disabled", false).fadeIn('slow');
    //  cliked_id.preventDefault();
} // unhide hidden div and enable input fields for shared to user
function hideDiv(cliked_id) {
    var fieldNum = cliked_id.charAt(cliked_id.length - 1);
    var newID = parseInt(fieldNum);
    var fieldID = "elementsToOperateOn-" + newID;
    var idfield = "#" + fieldID + " :input";
    document.getElementById(fieldID).style.display = 'none';
    var optionVal = $('#shareTo' + fieldNum).val();
//        var test = $('#'+fieldID).find('select').attr('id');
    $('.AddShare').find('option[value="' + optionVal + '"]').prop('disabled', false);
    //   $('.EditShare').find('option[value="'+ optionVal+'"]').prop('disabled',false);

    $(idfield).prop("disabled", true).fadeOut('slow');



    // cliked_id.preventDefault();
} // hide hidden div and disable input field for shared to user
$("input[type='checkbox']").change(function () {
    if ($(this).is(":checked")) {
        $(this).parent().addClass("redBackground");
    } else {
        $(this).parent().removeClass("redBackground");
    }


    var edithidden = $(this).attr('toEnable');
    var editTxb = $(this).attr('toEnableTxb');
    // console.log(editTxb);
    if ($(this).is(':checked') && $(this).hasClass('enabler')) {
        $('#' + editTxb).attr('readonly', false).prop('disabled', false);

        $('#' + edithidden).prop('disabled', false);

    } else {
        $('#' + editTxb).attr('readonly', true).prop('disabled', true);
        $('#' + edithidden).prop('disabled', true);

    }


});
// </editor-fold>
