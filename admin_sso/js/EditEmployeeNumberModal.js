
// <editor-fold defaultstate="collapsed" desc=" ${InitialLoad} ">
$(document).ready(function () {

    $("#ajaxmsgsadd,#ajaxmsgsdelete,#ajaxmsgsedit").hide();

    $('.EditShare').each(function () {
        $('.EditShare').not(this).find('option[value="' + this.value + '"]').prop('disabled', true);
    });

    $('select').on('change', function (e) {

           
        $('.AddShare').find('option').prop('disabled', false);
        $('.AddShare').each(function () {
            $('.AddShare').not(this).find('option[value="' + this.value + '"]').prop('disabled', true);
           // console.log( this.value +' removed');
        });
        $('.EditShare').find('option').prop('disabled', false);
        $('.EditShare').each(function () {
            $('.EditShare').not(this).find('option[value="' + this.value + '"]').prop('disabled', true);
        });
        $('.AddShare').find('option[value=""]').prop('disabled', false);
              $('.EditShare').find('option[value=""]').prop('disabled', false);
      
        e.preventDefault();
    });
});

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" ${AddStore} ">
$('#AddEmpNumberSubmit').on('click', function (event) {

    if ($("#AddEmpNumberForm")[0].checkValidity()) {
        var url = 'admin/function/EmployeeFunctionCaller.php?module=AddEmployeeNumber';     
        var mydata = $("#AddEmpNumberForm").serialize();     
        $.ajax({
            url: url,
            type: 'get',
            data: mydata,
            dataType: 'json',
            success: function (result) {
                document.getElementById('ajaxmsgsadd').style.display = 'block';
                if (result.type === 'Success') {
                    $('#ajaxmsgsadd').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong>" + result.message + "</div>");
                    $("#AddEmpNumberForm :input").attr("disabled", true);
                    $("#AddEmpNumberCancel").attr("disabled", false).html('Close');
                }
                else {
                    $('#ajaxmsgsadd').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong>" + result.message + "</div>");
                }              
            },
            error: function () {
                document.getElementById('ajaxmsgsadd').style.display = 'block';
                $('#ajaxmsgsadd').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
               
            }
        });
    } else {
        $("#AddEmpNumberForm").find(':submit').click();
    }
    event.preventDefault();
}); // ajax button form submit
var buttonAddEmpNumber = $('#AddEmpNumberSubmit');
var origAddEmpNumber = [];

$.fn.getTypeAddEmpNumber = function () {
    return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
}

$("#AddEmpNumberForm :input").each(function () {
    var type = $(this).getTypeAddEmpNumber();
    var tmp = {
        'type': type,
        'value': $(this).val()
    };
    if (type == 'radio') {
        tmp.checked = $(this).is(':checked');
    }
    origAddEmpNumber[$(this).attr('id')] = tmp;
});

$('#AddEmpNumberForm').bind('change keyup', function () {

    var disable = true;
    $("#AddEmpNumberForm :input").each(function () {
        var type = $(this).getTypeAddEmpNumber();
        var id = $(this).attr('id');
        if (type == 'text' || type == 'select') {
            disable = (origAddEmpNumber[id].value == $(this).val());

        } else if (type == 'radio') {
            disable = (origAddEmpNumber[id].checked == $(this).is(':checked'));
        }
        else if (type == 'checkbox') {
            var chkVal = '';
            if (origAddEmpNumber[id].value == 'on') {
                chkVal = true;
            }
            else {
                chkVal = false;
            }
            disable = (chkVal == $(this).is(':checked'));
        }

        if (!disable) {
            return false; // break out of loop
        }
    });

    buttonAddEmpNumber.prop('disabled', disable);
});


            $('#AddEmpNReset').click(function () {
               //$('#input1').attr('name', 'other_amount');
               buttonAddEmpNumber.prop('disabled',true);
           });

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" ${EditStore} ">

$('#EditEmpNumberSubmit').on('click', function (event) {
    if ($("#EditEmpNumberForm")[0].checkValidity()) {
        var url = 'admin/function/EmployeeFunctionCaller.php?module=EditEmployeeNumber';
       
        var mydata = $("#EditEmpNumberForm").serialize();
      
        $.ajax({
            url: url,
            type: 'get',
            data: mydata,
            dataType: 'json',
            success: function (result) {
                document.getElementById('ajaxmsgsedit').style.display = 'block';
                if (result.type === 'Success') {
                    $('#ajaxmsgsedit').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong>" + result.message + "</div>");
                     $("#EditEmpNumberForm :input").attr("disabled", true);
                    $("#EditEmpNumberCancel").attr("disabled", false).html('Close');
                }
                else {
                    $('#ajaxmsgsedit').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong>" + result.message + "</div>");
                }
                
            },
            error: function () {
                document.getElementById('ajaxmsgsedit').style.display = 'block';
                $('#ajaxmsgsedit').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
               
            }
        });
    } else {
        $("#EditEmpNumberForm").find(':submit').click();
    }
    event.preventDefault();
}); // ajax button form submit

function onLoadEditEmpNumberForm() {   
    var buttonEditEmpNumber = $('#EditEmpNumberSubmit');
    var origEditEmpNumber = [];

    $.fn.getTypeEditEmpNumber = function () {
        return this[0].tagName == "INPUT" ? $(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase();
    }

    $("#EditEmpNumberForm :input").each(function () {
        var type = $(this).getTypeEditEmpNumber();
        var tmp = {
            'type': type,
            'value': $(this).val()
        };
        if (type == 'radio') {
            tmp.checked = $(this).is(':checked');
        }
        origEditEmpNumber[$(this).attr('id')] = tmp;
    });
    //console.log(origEditEmpNumber)
    $('#EditEmpNumberForm').bind('change keyup', function () {

        var disable = true;
        $("#EditEmpNumberForm :input").each(function () {
            var type = $(this).getTypeEditEmpNumber();
            var id = $(this).attr('id');
            if (type == 'text' || type == 'select') {
                disable = (origEditEmpNumber[id].value == $(this).val());

            } else if (type == 'radio') {
                disable = (origEditEmpNumber[id].checked == $(this).is(':checked'));
            }
            else if (type == 'checkbox') {
                var chkVal = '';
                if (origEditEmpNumber[id].value == 'on') {
                    chkVal = true;
                }
                else {
                    chkVal = false;
                }
                disable = (chkVal == $(this).is(':checked'));
            }

            if (!disable) {
                return false; // break out of loop
            }
        });

        buttonEditEmpNumber.prop('disabled', disable);
    });
}



// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" ${DeleteStore} ">
$('#DeleteEmpNumberSubmit').on('click', function (event) {
    var url = 'admin/function/EmployeeFunctionCaller.php?module=DeleteEmployeeNumber';    
    var mydata = $("#DeleteEmpNumberForm").serialize();
    
    $.ajax({
        url: url,
        type: 'get',
        data: mydata,
        dataType: 'json',
        success: function (result) {
            document.getElementById('ajaxmsgsdelete').style.display = 'block';
            if (result.type === 'Success') {
                $('#ajaxmsgsdelete').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Success!</strong>" + result.message + "</div>");
                $("#DeleteEmpNumberSubmit :input").attr("disabled", true);
                $("#DeleteEmpNumberCancel").attr("disabled", false).html('Close');
            }
            else {
                $('#ajaxmsgsdelete').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong>" + result.message + "</div>");
            }            
        },
        error: function () {
            document.getElementById('ajaxmsgsdelete').style.display = 'block';
            $('#ajaxmsgsdelete').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");
          
        }
    });
    event.preventDefault();
}); // ajax button form submit

function onLoadDeleteEmpNumberForm() {
    var buttonDeleteEmpNumber = $('#DeleteEmpNumberSubmit');
    $('#DeleteEmpNumberForm').bind('change keyup', function () {
        var disable = true;
        if ($("#DeleteEmpNumberForm input:checkbox:checked").length > 0) {
            disable = false;
        } // any one is checked     
        else {
            disable = true;
        } // none is checked
        buttonDeleteEmpNumber.prop('disabled', disable);
    });
}

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" ${Extra} ">
function showDiv(cliked_id) {

    var fieldNum = cliked_id.charAt(cliked_id.length - 1);
    var newID = parseInt(fieldNum) + 1;
    var fieldID = "elementsToOperateOn-" + newID;
    var idfield = "#" + fieldID + " :input";
    $("#empTo" + newID).val($("#shareTo" + newID + " option:first").val());

    //  alert(idfield);
    document.getElementById(fieldID).style.display = 'block';
    $(idfield).prop("disabled", false).fadeIn('slow');
    //  cliked_id.preventDefault();
} // unhide hidden div and enable input fields for shared to user
function hideDiv(cliked_id) {
    var fieldNum = cliked_id.charAt(cliked_id.length - 1);
    var newID = parseInt(fieldNum);
    var fieldID = "elementsToOperateOn-" + newID;
    var idfield = "#" + fieldID + " :input";
    document.getElementById(fieldID).style.display = 'none';
    var optionVal = $('#empTo' + fieldNum).val();
//        var test = $('#'+fieldID).find('select').attr('id');
    $('.AddShare').find('option[value="' + optionVal + '"]').prop('disabled', false);
    //   $('.EditShare').find('option[value="'+ optionVal+'"]').prop('disabled',false);

    $(idfield).prop("disabled", true).fadeOut('slow');



    // cliked_id.preventDefault();
} // hide hidden div and disable input field for shared to user
$("input[type='checkbox']").change(function () {
    if ($(this).is(":checked")) {
        $(this).parent().addClass("redBackground");
    } else {
        $(this).parent().removeClass("redBackground");
    }
});
// </editor-fold>
