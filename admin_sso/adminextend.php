<section class='reports'>
    <div class="row">        
        <div class="col-lg-3">
            <div class="list-group">
                <a class="list-group-item active" specialUrl="admin/adminviewdata.php?module=FixLine" onclick="openViewData(this)">
                    <i class="fa fa-search"></i> Store Fix Line
                </a> 

                <a class="list-group-item" specialUrl="admin/adminviewdata.php?module=StoreType" onclick="openViewData(this)">
                    <span class="glyphicon glyphicon-edit" ></span> Update Store Type
                </a> 

                <a class="list-group-item" specialUrl="admin/adminviewdata.php?module=UserRole" onclick="openViewData(this)">
                    <span class="glyphicon glyphicon-edit" ></span> Update User Role
                </a> 

                <a class="list-group-item" specialUrl="admin/adminviewdata.php?module=EmpRole" onclick="openViewData(this)">
                    <span class="glyphicon glyphicon-edit" ></span> Update Employee Role
                </a> 

                <a class="list-group-item" specialUrl="admin/adminviewdata.php?module=Store" onclick="openViewData(this)">
                    <span class="glyphicon glyphicon-edit" ></span> Activate Store
                </a> 

                <a class="list-group-item" specialUrl="admin/adminviewdata.php?module=User" onclick="openViewData(this)">
                    <span class="glyphicon glyphicon-edit" ></span> Activate User
                </a> 

                <a class="list-group-item" specialUrl="admin/adminviewdata.php?module=Employee" onclick="openViewData(this)">
                    <span class="glyphicon glyphicon-edit" ></span> Activate Employee
                </a>
            </div>
        </div>

        <div class="col-lg-9">
            <div class="ViewData" id="ViewData"> </div>
        </div>
    </div>
</section>

<!-- Modal  Update Store Type-->
<div class="modal fade" id="ActivateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal  CreateUserModal -->


<!-- Modal Update User Role-->
<div class="modal fade" id="UpdateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal  EditUserModal -->

<div class="modal fade" id="MergeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal  EditUserModal -->



<script type="text/javascript">
    $(document).ready(function () {
        var t = $('#AdminTracker').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            "bFilter": false,
            "bPaginate": false,
            "bInfo": false,
            "aaSorting": []
        });

        //Remove Heading Sorting
        $("th").unbind();
        $("th").removeClass("sorting");
        $("th").removeClass("sorting_asc");
        $("th").removeClass("dataTables_info");

        $('#ViewData').load('admin/adminviewdata.php?module=FixLine');
    });

    $(function () {
        $('[data-tooltip="tooltip"]').tooltip();
    })
</script>
