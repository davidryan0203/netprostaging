<?php
session_start();
$role = $_SESSION['UserRoleID'];
?>

<section class='reports'>
    <div class="row">
        <div class="col-lg-12">
            <h4 class="" id="admin-head"><i class="fa fa-shopping-cart fa-fw"></i>Store Lists</h4>

            <div class="filters">
                <?php if ($role == 1) { ?>
                    <a class="btn btn-primary btn-xs btn-admin" data-tooltip="tooltip" data-placement="left" title="Add Store"
                       data-toggle="modal" href="admin/modal/CreateStoreModal.php" data-target="#CreateStoreModal" onclick="openCreateStoreModal(this)">
                        <i class="glyphicon glyphicon-plus"></i> Store
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="loader" id="LogLoader"></div>
    <div id="page-contents"></div>
</section>

<script src="../includes/js/custom.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#page-contents').hide(function () {
            var urlToLoad = "admin/storeextend.php";
            var divToHide = "#LogLoader";
            var divToShow = "#page-contents";
            reload(urlToLoad, divToHide, divToShow);
        });
    });

    function reload(urlToLoad, divToHide, divToShow) {
        var urldata = urlToLoad;
        var page = divToShow;
        $.get(urldata).success(
                function (response, status, jqXhr) {
                    $(divToHide).hide();
                    $(page).fadeIn('slow');
                    $(page).empty().append(response);
                }).error(function (response, status, jqXhr) {
            $(divToHide).show();
            reload(urldata, divToHide, divToShow);
        }).complete(function (response, status, jqXhr) {
        });
    }

    function reLoad() {
        var urlToLoad = "admin/storeextend.php";
        var divToHide = "#LogLoader";
        var divToShow = "#page-contents";
        reload(urlToLoad, divToHide, divToShow);
    }

    function stripHTML(dirtyString) {
        var container = document.createElement('div');
        container.innerHTML = dirtyString;
        return container.textContent;
    }

    function openCreateStoreModal(el) {
        var target = $(el).attr('href');
        $("#CreateStoreModal .modal-content").load(target, function () {
            $("#CreateStoreModal").modal("show");
        });

        $('#CreateStoreModal').on('hidden.bs.modal', function (e) {
            e.preventDefault();
            $('#page-contents').hide();
            $('#LogLoader').fadeIn('slow');
            reLoad();
        });
    }

    function openEditStoreSharedModal(el) {
        var target = $(el).attr('href');
        $("#EditStoreSharedModal .modal-content").load(target, function () {
            $("#EditStoreSharedModa").modal("show");
        });

        $('#EditStoreSharedModal').on('hidden.bs.modal', function (e) {
            e.preventDefault();
            $('#page-contents').hide();
            $('#LogLoader').fadeIn('slow');
            reLoad();
        });
    }

    function openEditStoreModal(el) {
        var target = $(el).attr('href');
        $("#EditStoreModal .modal-content").load(target, function () {
            $("#EditStoreModal").modal("show");
        });
        $('#EditStoreModal').on('hidden.bs.modal', function (e) {
            e.preventDefault();
            $('#page-contents').hide();
            $('#LogLoader').fadeIn('slow');
            reLoad();
        });
    }

    function openDeleteStoreModal(el) {
        var target = $(el).attr('href');
        $("#DeleteStoreModal .modal-content").load(target, function () {
            $("#DeleteStoreModal").modal("show");
        });

        $('#DeleteStoreModal').on('hidden.bs.modal', function (e) {
            e.preventDefault();
            $('#page-contents').hide();
            $('#LogLoader').fadeIn('slow');
            reLoad();
        });
    }

    $(function () {
        $('[data-tooltip="tooltip"]').tooltip();
    });
</script>



