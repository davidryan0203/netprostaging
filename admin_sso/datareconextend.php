<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#example').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            columnDefs: [
                {sortable: false, targets: -1},
                {"width": "8%", "targets": -1}
            ],
            colReorder: {
                fixedColumnsRight: 1
            },
            "iDisplayLength": 15,
            "aLengthMenu": [[15, 50, 100, -1], [15, 50, 100, "All"]],
            //"bStateSave" : true,
            "oColVis": {
                "buttonText": "Hide Columns"
            }
        });
    });


    function stripHTML(dirtyString) {
        var container = document.createElement('div');
        container.innerHTML = dirtyString;
        return container.textContent;
    }
</script>

<?php
include($_SERVER['DOCUMENT_ROOT'] . '/datarecon/functions/DataReconFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
session_start();
$role = $_SESSION['UserRoleID'];
$userID = $_SESSION['UserProfileID'];
$dataReconData = getDataReconUser();
$i = 0;
//echo "<pre>";
$mergeData = array();
foreach ($dataReconData as $key => $dataReconValues) {

    //$mergeData[$dataReconValues['ReconID']]['ID'] = $dataReconValues['ReconID'];
    $mergeData[$key]['ReconUserID'] = $dataReconValues[0]['ReconUserID'];
    $mergeData[$key]['ReconFirstName'] = $dataReconValues[0]['ReconFirstName'];
    $mergeData[$key]['ReconLastName'] = $dataReconValues[0]['ReconLastName'];
    $mergeData[$key]['AssignedTo'] = $dataReconValues['OwnerDetails'];

    $i++;
}
//echo "<pre>";
//print_r($mergeData);
?>

<div class="table-responsive">
    <table id="example" class="table table-bordered table-hover table-responsive"  cellspacing="0" >
        <thead>
            <tr>
                <th>User Name</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Assigned To</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 0;
            foreach ($mergeData as $key => $value) {
                ?>
                <tr>
                    <td><?php echo $key; ?></td>
                    <td><?php echo $value['ReconFirstName']; ?></td>
                    <td><?php echo $value['ReconLastName']; ?></td>                  
                    <td><?php
                        foreach ($value['AssignedTo'] as $assignTo) {
                            echo $assignTo['OwnerName'] . ", ";
                            $i++;
                        }
                        ?>
                    </td>                  
                    <td><?php if ($role == '1') { ?>
            <!--                            <a class="btn-sm btn-hover btn-warning  editmodal" data-toggle="modal" href="admin/modal/EditUserModal.php?userid=<?php echo $value['f_UserID']; ?>" data-target="#EditUserModal" onclick="openEditUserModal(this)" ><span class="glyphicon glyphicon-edit" ></span></a>
                                        <a class="btn-sm btn-hover btn-danger deletemodal" data-toggle="modal" href="admin/modal/DeleteUserModal.php?userid=<?php echo $value['f_UserID']; ?>" data-target="#DeleteUserModal" onclick="openDeleteUserModal(this)" ><span class="glyphicon glyphicon-trash" ></span></a>-->
                        <?php } elseif ($value['f_UserID'] == $userID) { ?> 
                            <a class="btn-sm btn-hover btn-warning  editmodal" data-toggle="modal" href="admin/modal/EditUserModal.php?userid=<?php echo $value['f_UserID']; ?>" data-target="#EditUserModal" onclick="openEditUserModal(this)" ><span class="glyphicon glyphicon-edit" ></span></a>
                        <?php } ?> </td>
                </tr>   
            <?php } ?>
        </tbody> 
    </table>

    <!-- Modal  CreateDataReconModal-->
    <div class="modal fade" id="CreateDataReconModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div> <!-- /.modal-content -->
        </div> <!-- /.modal-dialog -->
    </div> <!-- /.modal  CreateDataReconModal -->


    <!-- Modal EditDataReconModal-->
    <div class="modal fade" id="EditDataReconModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div> <!-- /.modal-content -->
        </div> <!-- /.modal-dialog -->
    </div> <!-- /.modal  EditDataReconModal -->

    <!-- Modal DeleteDataReconModal-->
    <div class="modal fade" id="DeleteDataReconModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div> <!-- /.modal-content -->
        </div> <!-- /.modal-dialog -->
    </div> <!-- /.modal  DeleteDataReconModal -->
</div>