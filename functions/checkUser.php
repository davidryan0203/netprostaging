<?php

include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/functions/crypter.php');
require($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');



$username = mysql_escape_string(trim($_GET['log_username_n']));
$password = mysql_escape_string(trim($_GET['log_password_n']));


$updatePassword = password_hash($password, PASSWORD_BCRYPT);

$selectHash = "SELECT f_Password,f_UserLoginID from t_userlogin where f_UserName = '{$username}'";
$result = mysql_query($selectHash, $connection);
$data = mysql_fetch_array($result);
$rows = mysql_num_rows($result);

$hash = $data['f_Password'];

if ($data['f_Password'] == md5($password)) {
    $correctPassword = true;
} else {
    if (password_verify($password, $hash)) {
        $correctPassword = true;
    } else {
        $correctPassword = false;
    }
}



if ($correctPassword) {


    //echo $updatePassword;
    $query = "SELECT
        a.f_UserLoginID,a.f_UserName,
        CASE WHEN a.f_UserID != 0 and a.f_EmpID = 0
        THEN b.f_UserFirstName
        WHEN a.f_UserID = 0 and a.f_EmpID != 0 THEN c.f_EmpFirstName
        END  as 'FirstName',
        CASE WHEN a.f_UserID != 0 and a.f_EmpID = 0   THEN b.f_UserLastName
        WHEN a.f_UserID = 0 and a.f_EmpID != 0 THEN c.f_EmpLastName
        END  as 'LastName',c.f_EmpID,b.f_UserID,d.f_UserRoleID, case when a.f_ProfileImgUrl is null then 'images/user_icon.png' else concat('profilepic/',a.f_ProfileImgUrl) end as ImgUrl,
        (a.f_ReadOnly + 0) as ReadOnly
         FROM t_userlogin a
        LEFT JOIN t_userlist b on a.f_UserID = b.f_UserID
        LEFT JOIN t_emplist c on a.f_EmpID = c.f_EmpID
        LEFT JOIN t_userrole d on a.f_UserRoleID = d.f_UserRoleID
        WHERE a.f_Status = 1 AND a.f_UserName = '$username' LIMIT 1";
//echo $query;
    $result = mysql_query($query, $connection);
    $data = mysql_fetch_assoc($result);
    $num = mysql_num_rows($result);

    if ($num == 1) {

        $loginSuccess = "INSERT INTO `t_log`
                        (`f_LogTypeID`,`f_LogFromID`,`f_LogRemark`,`f_UserLoginID`, `f_DateTimeCreated`,`f_LogStatus`,`f_LogFromSubID`)
                        VALUES
                        ('1','3','Login Success','{$data['f_UserLoginID']}',NOW(),'Success','1')";
        mysql_query($loginSuccess, $connection);


        $sqlSharedOwnerShip = "SELECT f_UserID FROM t_sharedownership WHERE f_SharedWithUserID = '{$data['f_UserID']}'";
        $resultSharedOwnerShip = mysql_query($sqlSharedOwnerShip, $connection);
        $dataSharedOwnerShip = mysql_fetch_assoc($resultSharedOwnerShip);

        session_start();

        if (!empty($_GET["remember_me"])) {
            setcookie("log_username_n", $_GET["log_username_n"], time() + (10 * 365 * 24 * 60 * 60), "/");
            setcookie("log_password_n", $_GET["log_password_n"], time() + (10 * 365 * 24 * 60 * 60), "/");
        } else {

            if (isset($_COOKIE["log_username_n"])) {
                setcookie("log_username_n");
                unset($_COOKIE["log_username_n"]);
                setcookie('log_username_n', null, -1, '/');
            }
            if (isset($_COOKIE["log_password_n"])) {
                setcookie("log_password_n");
                unset($_COOKIE["log_password_n"]);
                setcookie('log_password_n', null, -1, '/');
            }
        }


        if (!is_null($dataSharedOwnerShip['f_UserID'])) {
            $_SESSION['UserProfileID'] = $dataSharedOwnerShip['f_UserID'];
            $_SESSION['OriginalUserID'] = $data['f_UserID'];
            $_SESSION['SharedOwnerShip'] = True;
        } else {
            $_SESSION['UserProfileID'] = $data['f_UserID'];
            $_SESSION['OriginalUserID'] = $data['f_UserID'];
            $_SESSION['SharedOwnerShip'] = False;
        }


        $sqlCheckChangedPass = "SELECT count(*) as 'change' FROM t_passrepo WHERE f_UserLoginID = '{$data['f_UserLoginID']}'";
        $resultCheckChangedPass = mysql_query($sqlCheckChangedPass, $connection);
        $dataCheckChangedPass = mysql_fetch_assoc($resultCheckChangedPass);

        $_SESSION['EmpProfileID'] = $data['f_EmpID'];
        $_SESSION['Firstname'] = $data['FirstName'];
        $_SESSION['Lastname'] = $data['LastName'];
        $_SESSION['UserName'] = $data['f_UserName'];
        $_SESSION['UserRoleID'] = $data['f_UserRoleID'];
        $_SESSION['LoginID'] = $data['f_UserLoginID'];
        $resultdata['message'] = 'Success';
        $resultdata['type'] = 'Success';
        $_SESSION['ProfileImg'] = $data['ImgUrl'];
        $_SESSION['ReadOnly'] = $data['ReadOnly'];
        $SelectStoreTypes = SelectStoreTypes();
        $_SESSION['HasStoreType'] = $SelectStoreTypes['StoreTypes'];
        $checkPrivate = checkPrivacyStore();
        $_SESSION['isPrivate'] = $checkPrivate['isPrivate'];

        $arrayRederict = array('4');
        if (in_array($data['f_UserRoleID'], $arrayRederict)) {
            $resultdata['URL'] = 'public_html.php?i=1';
        } elseif ($data['f_UserRoleID'] == 6) {
            $resultdata['URL'] = 'public_html.php?i=6';
        } elseif ($data['f_UserRoleID'] == 1) {
            $resultdata['URL'] = 'public_html.php?i=4';
        } else {
            $resultdata['URL'] = 'public_html.php?i=2&o=1';
        }


        if ($dataCheckChangedPass['change'] < 1) {
            $_SESSION['Redirect'] = true;
            $resultdata['URL'] = 'public_html.php?i=8';
        } else {
            $_SESSION['Redirect'] = false;
        }

        if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
            $_SESSION['ChangeEmail'] = false;
        } else {
            $_SESSION['ChangeEmail'] = true;
            $resultdata['URL'] = 'public_html.php?i=12';
        }
    } else {
        $resultdata['message'] = 'Invalid username or password!';
        $resultdata['type'] = 'Error';
    }
} else {
    $resultdata['message'] = 'Invalid username or password!';
    $resultdata['type'] = 'Error';
}

mysql_close($connection);
echo json_encode($resultdata);
