<?php

/* array sort helper function */

function randomSort($a, $b) {
    return rand(-1, 1);
}

class PTagCloud {

    var $m_arTagsAdvocate = array();
    var $m_arTagsPassive = array();
    var $m_arTagsDetractors = array();
    //custom parameters
    var $m_displayedElementsCount;
    var $m_searchURL;
    var $m_backgroundImage;
    var $m_backgroundColor;
    var $m_width;
    var $m_arColors;
    var $m_bUTF8;

    function __construct($displayedElementCount, $arSeedWords = false) {
        $this->m_displayedElementsCount = $displayedElementCount;
        $this->m_searchURL = "http://www.google.com/search?hl=en&q=";
        $this->m_bUTF8 = false;
        // $this->m_backgroundColor = "#000";
        $this->m_arAdvoColors[0] = "#008000";
        $this->m_arAdvoColors[1] = "#008000";
        $this->m_arAdvoColors[2] = "#008000";
        $this->m_arAdvoColors[3] = "#008000";
        $this->m_arAdvoColors[4] = "#008000";
        $this->m_arAdvoColors[5] = "#008000";
        $this->m_arAdvoColors[6] = "#008000";
        $this->m_arAdvoColors[7] = "#008000";
        $this->m_arAdvoColors[8] = "#008000";
        $this->m_arAdvoColors[9] = "#008000";
        
        $this->m_arPassColors[0] = "#a59d9d";
        $this->m_arPassColors[1] = "#a59d9d";
        $this->m_arPassColors[2] = "#a59d9d";
        $this->m_arPassColors[3] = "#a59d9d";
        $this->m_arPassColors[4] = "#a59d9d";
        $this->m_arPassColors[5] = "#a59d9d";
        $this->m_arPassColors[6] = "#a59d9d";
        $this->m_arPassColors[7] = "#a59d9d";
        $this->m_arPassColors[8] = "#a59d9d";
        $this->m_arPassColors[9] = "#a59d9d";
        
        $this->m_arDetColors[0] = "#F21105";
        $this->m_arDetColors[1] = "#F21105";
        $this->m_arDetColors[2] = "#F21105";
        $this->m_arDetColors[3] = "#F21105";
        $this->m_arDetColors[4] = "#F21105";
        $this->m_arDetColors[5] = "#F21105";
        $this->m_arDetColors[6] = "#F21105";
        $this->m_arDetColors[7] = "#F21105";
        $this->m_arDetColors[8] = "#F21105";
        $this->m_arDetColors[9] = "#F21105";
        
        

        if ($arSeedWords !== false && is_array($arSeedWords)) {
            foreach ($arSeedWords as $key => $value) {
                $this->addTag($value);
            }
        }
    }

    function PTagCloud($displayedElementCount, $arSeedWords = false) {
        $this->__construct($displayedElementCount, $arSeedWords);
    }

    function setSearchURL($searchURL) {
        $this->m_searchURL = $searchURL;
    }

    function setUTF8($bUTF8) {
        $this->m_bUTF8 = $bUTF8;
    }

    function setWidth($width) {
        $this->m_width = $width;
    }

    function setBackgroundImage($backgroundImage) {
        $this->m_backgroundImage = $backgroundImage;
    }

    function setBackgroundColor($backgroundColor) {
        $this->m_backgroundColor = $backgroundColor;
    }

    function setTextColors($arColors) {
        $this->m_arColors = $arColors;
    }

    /* word replace helper */

    function str_replace_word($needle, $replacement, $haystack) {
        $pattern = "/\b$needle\b/i";
        $haystack = preg_replace($pattern, $replacement, $haystack);
        return $haystack;
    }

    function keywords_extract($text) {
        $text = strtolower($text);
        $text = strip_tags($text);

        /*
         * Handle common words first because they have punctuation and we need to remove them
         * before removing punctuation.
         */
        $commonWords = "'tis,'twas,a,able,about,across,after,ain't,all,almost,also,am,among,an,and,any,are,aren't," .
                "as,at,be,because,been,but,by,can,can't,cannot,could,could've,couldn't,dear,did,didn't,do,does,doesn't," .
                "don't,either,else,ever,every,for,from,get,got,had,has,hasn't,have,he,he'd,he'll,he's,her,hers,him,his," .
                "how,how'd,how'll,how's,however,i,i'd,i'll,i'm,i've,if,in,into,is,isn't,it,it's,its,just,least,let,like," .
                "likely,may,me,might,might've,mightn't,most,must,must've,mustn't,my,neither,no,nor,not,o'clock,of,off," .
                "often,on,only,or,other,our,own,rather,said,say,says,shan't,she,she'd,she'll,she's,should,should've," .
                "shouldn't,since,so,some,than,that,that'll,that's,the,their,them,then,there,there's,these,they,they'd," .
                "they'll,they're,they've,this,tis,to,too,twas,us,wants,was,wasn't,we,we'd,we'll,we're,were,weren't,what," .
                "what'd,what's,when,when,when'd,when'll,when's,where,where'd,where'll,where's,which,while,who,who'd," .
                "who'll,who's,whom,why,why'd,why'll,why's,will,with,won't,would,would've,wouldn't,yet,you,you'd,you'll," .
                "you're,you've,your,b";
        $commonWords = strtolower($commonWords);
        $commonWords = explode(",", $commonWords);
        foreach ($commonWords as $commonWord) {
            $text = $this->str_replace_word($commonWord, "", $text);
        }

        /* remove punctuation and newlines */
        /*
         * Changed to handle international characters
         */
        if ($this->m_bUTF8)
            $text = preg_replace('/[^\p{L}0-9\s]|\n|\r/u', ' ', $text);
        else
            $text = preg_replace('/[^a-zA-Z0-9\s]|\n|\r/', ' ', $text);

        /* remove extra spaces created */
        $text = preg_replace('/ +/', ' ', $text);

        $text = trim($text);
        $words = explode(" ", $text);
        foreach ($words as $value) {
            $temp = trim($value);
            if (is_numeric($temp))
                continue;
            $keywords[] = trim($temp);
        }

        return $keywords;
    }

    function addTagsFromText($SeedAdvocate, $SeedPassive, $SeedDetractors) {

        $wordsAdvocate = $this->keywords_extract($SeedAdvocate);
            foreach ($wordsAdvocate as $key => $value) {
            $this->addTagAdvocate($value);
        }
        $wordsPassive = $this->keywords_extract($SeedPassive);
            foreach ($wordsPassive as $key => $value) {
            $this->addTagPassive($value);
        }
        $wordsDetractors = $this->keywords_extract($SeedDetractors);
            foreach ($wordsDetractors as $key => $value) {
            $this->addTagDetractors($value);
        }
    }

    function addTagAdvocate($tag, $useCount = 1) {
        $tag = strtolower($tag);
        if (array_key_exists($tag, $this->m_arTagsAdvocate)) {
            $this->m_arTagsAdvocate[$tag]['count'] += $useCount;
            $this->m_arTagsAdvocate[$tag]['type'] = 'advo';
        } else {
            $this->m_arTagsAdvocate[$tag]['count'] = $useCount;
            $this->m_arTagsAdvocate[$tag]['type'] = 'advo';
        }
    }

    function addTagPassive($tag, $useCount = 1) {
        $tag = strtolower($tag);
      if(!array_key_exists($tag,$this->m_arTagsAdvocate)){
           if (array_key_exists($tag, $this->m_arTagsPassive)) {
                $this->m_arTagsPassive[$tag]['count'] += $useCount;
                $this->m_arTagsPassive[$tag]['type'] = 'pass';
            } else {
                $this->m_arTagsPassive[$tag]['count'] = $useCount;
                $this->m_arTagsPassive[$tag]['type'] = 'pass';
            }
      }
           
    
    }

    function addTagDetractors($tag, $useCount = 1) {
        $tag = strtolower($tag);
       if(!array_key_exists($tag,$this->m_arTagsPassive) && !array_key_exists($tag,$this->m_arTagsAdvocate) ){
            if (array_key_exists($tag, $this->m_arTagsDetractors)) {
                $this->m_arTagsDetractors[$tag]['count'] += $useCount;
                $this->m_arTagsDetractors[$tag]['type'] = 'det';
            } else {
                $this->m_arTagsDetractors[$tag]['count'] = $useCount;
                $this->m_arTagsDetractors[$tag]['type'] = 'det';
            }
       }
    }

    function gradeFrequency($frequency) {
        $grade = 0;
        if ($frequency >= 90 )
            $grade = 9;
        else if ($frequency >= 70)
            $grade = 9;
        else if ($frequency >= 60)
            $grade = 9;
        else if ($frequency >= 50)
            $grade = 4;
        else if ($frequency >= 40)
            $grade = 4;
        else if ($frequency >= 30)
            $grade = 4;
        else if ($frequency >= 20)
            $grade = 2;
        else if ($frequency >= 10)
            $grade = 2;
        else if ($frequency >= 5)
            $grade = 2;

    return $grade;
    }

    function emitCloud($bHTML = true) {
        // arsort($this->m_arTags);
        
         
        foreach($this->m_arTagsAdvocate as $count){
            $advo[] = $count['count'];
        }
        
         foreach($this->m_arTagsPassive as $count){
            $pass[] = $count['count'];
        }
        
         foreach($this->m_arTagsDetractors as $count){
            $detr[] = $count['count'];
        }
        
     array_multisort($this->m_arTagsAdvocate, SORT_DESC, $advo);
       array_multisort($this->m_arTagsPassive, SORT_DESC, $pass);
        array_multisort($this->m_arTagsDetractors, SORT_DESC, $detr);
    //     arsort($this->m_arTagsAdvocate);
    //      arsort($this->m_arTagsPassive);
     //      arsort($this->m_arTagsDetractors);
        $top10Advo = array_slice($this->m_arTagsAdvocate , 0 ,  10); 
         $top10Pass = array_slice($this->m_arTagsPassive , 0 , 10);
         $top10Detr = array_slice($this->m_arTagsDetractors , 0 , 10);
       //  echo "<pre>";
       //var_dump($this->m_arTagsAdvocate, $this->m_arTagsPassive, $this->m_arTagsDetractors);       
        $newarray = array_merge($top10Advo, $top10Pass,$top10Detr);       
        
        //var_dump($newarray);
        //arsort($newarray);
        $arTopTags = array_slice($newarray, 0, $this->m_displayedElementsCount);

        /* randomize the order of elements */
       uasort($arTopTags, 'randomSort');
       

        $this->maxCountAdvo = max($advo);
        $this->maxCountPass = max($pass);
        $this->maxCountDet = max($detr);
    
        if (is_array($newarray))
		{
			if ($bHTML) 
			    $result = '<div id="id_tag_cloud" style="' . (isset($this->m_width) ? ("width:". $this->m_width. ";") : "") .
			               'line-height:normal"><div style="border-style:solid;border-width:1px;' .
			              (isset($this->m_backgroundImage) ? ("background:url('". $this->m_backgroundImage ."');") : "") .
			              'border-color:#888;margin-top:20px;margin-bottom:10px;padding:5px 5px 20px 5px;background-color:'.$this->m_backgroundColor.';">';
            else			    
                $result = array();
         
			foreach ($arTopTags as $tag => $useCount)
			{
                               $gradeFreq = "";
                                $grade = "";
                                $color = "";
                            if($useCount['type'] == 'advo'){                            
                                $gradeFreq = ($useCount['count'] * 100) / $this->maxCountAdvo;
                                $grade = $this->gradeFrequency($gradeFreq);
                                $color = $this->m_arAdvoColors[$grade];
                            }
                            elseif($useCount['type'] == 'pass'){
                                $gradeFreq = ($useCount['count'] * 100) / $this->maxCountPass;
                                $grade = $this->gradeFrequency($gradeFreq);
                                 $color = $this->m_arPassColors[$grade];
                            }
                            elseif($useCount['type'] == 'det'){
                                $gradeFreq = ($useCount['count'] * 100) / $this->maxCountDet;
                                $grade = $this->gradeFrequency($gradeFreq);
                                $color = $this->m_arDetColors[$grade];
                            }
                            
                            else{}
				//$grade = $this->gradeFrequency($gradeFreq);
				if ($bHTML)
				{
					$result .= ("<a href=\"". $this->m_searchURL.urlencode($tag)."\" title=\"More info on " .
					    $tag."\" style=\"color:".$color.";\">" .
					    "<span style=\"color:".$color."; letter-spacing:3px; ".
					    "padding:4px; font-family:Tahoma; font-weight:900; font-size:" . 
					    (0.45 + 0.1 * $grade) . "em\">".$tag."</span></a> ");
				}
				else
				    $result[$tag] = $grade;
			}
			if ($bHTML)
                $result .= ('</div><div style="position:relative;top:-25px">' .
                           '<div style="float:right;padding-right:5px;height:15px;font-size:10px">' .
                          
                           '</div></div></div><br />');

			return $result;
            }       
        
    }

}
