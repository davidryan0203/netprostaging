<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//include ('db/connection.php');


function InsertLog($logType, $logFrom, $logRemarks, $logBy, $logStatus, $logFromSub) {
    global $connection;

    $sqlLogInsertEmp = "INSERT INTO `t_log` (`f_LogTypeID`,`f_LogFromID`,`f_LogRemark`,`f_UserLoginID`,`f_DateTimeCreated`,`f_LogStatus`,`f_LogFromSubID`) 
            VALUES ($logType,$logFrom,'{$logRemarks}','{$logBy}',NOW(),'{$logStatus}','{$logFromSub}')";
    mysql_query($sqlLogInsertEmp, $connection);
}

function SelectUserLogs($from,$to) {
    global $connection;
   // $daterange = " ";
    if((isset($from) && !is_null($from)) && (isset($to) && !is_null($to)) ){
        $daterange = " AND a.f_DateTimeCreated Between '{$from} 00:00:00' and '{$to} 23:59:59' ";
    } else {
        $daterange = " AND MONTH(f_DateTimeResponseMelbourne) = MONTH(CURDATE())
                     AND YEAR(f_DateTimeResponseMelbourne) = YEAR(CURDATE())";
    }
    
    $sql = "SELECT a.f_LogId, b.f_LogTypeName, c.f_LogFromName, d.f_LogFromSubName, e.f_UserName, a.f_LogRemark, a.f_LogStatus, 
        DATE_FORMAT(a.f_DateTimeCreated,'%d-%b-%Y %k:%i:%S') as LogDate
        FROM t_log a
        LEFT JOIN t_logtype b ON a.f_LogTypeID = b.f_LogTypeID
        LEFT JOIN t_logfrom c ON a.f_LogFromID = c.f_LogFromID
        LEFT JOIN t_logfromsub d ON a.f_LogFromSubID = d.f_LogFromSubID
        LEFT JOIN t_userlogin e ON a.f_UserLoginID = e.f_UserLoginID
        WHERE 1=1 $daterange
        ORDER BY a.f_DateTimeCreated DESC";
   // echo $sql . "<pre>";
    //        WHERE  f_LogFromID = {} and f_LogFromSubID = {} and f_LogTypeID = {}
    $result = mysql_query($sql, $connection);
    while ($row = mysql_fetch_assoc($result)) {
        $returnSelectUserLogs[] = $row;
    }
    //mysql_close($connection);
    return $returnSelectUserLogs;
}
