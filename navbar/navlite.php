<?php
$role = $_SESSION['UserRoleID'];
$UserLoginID = $_SESSION['LoginID'];

if ($_SESSION['SharedOwnerShip']) {
    $userID = $_SESSION['OriginalUserID'];
} else {
    $userID = $_SESSION['UserProfileID'];
}

$profileImg = $_SESSION['ProfileImg'];
?>

<style>
    #navContainer {
        width:1270px !important;
    }
</style>
<nav class="navbar navbar-inverse navbar-fixed-top" id="navbar">
    <div class="container" id="navContainer">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="public_html.php">
                <img src="images/logo.png" alt="Telstra Blog">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="collapse">
            <ul class="nav navbar-nav">
                <!--
                                <li class = 'dropdown'>
                                    <a class = 'dropdown-toggle' data-toggle = 'dropdown'><i class = 'fa fa-list'></i> Dashboard <i class = 'fa fa-caret-down'></i></a>
                                    <ul class = 'dropdown-menu dropdown-user'>
                <?php if ($_SESSION['HasStoreType'] == 'Only Others') { ?>
                                                    <li><a href='public_html.php?i=0&o=3'><i class='fa fa-folder-open'></i>Local Dashboard </a></li>
                <?php } elseif ($_SESSION['isPrivate']) { ?>
                                                    <li><a href='public_html.php?i=0&o=2'><i class='fa fa-folder-open'></i>Local Dashboard </a></li>
                <?php } else { ?>
                                                    <li><a href='public_html.php?i=0'><i class='fa fa-home'></i> Main Dashboard</a></li>
                                                    <li><a href='public_html.php?i=0&o=2'><i class='fa fa-folder-open'></i> Local Dashboard</a></li>
                <?php } ?>
                                    </ul>
                                </li>
                                <li class = 'dropdown'>
                                    <a class = 'dropdown-toggle' data-toggle = 'dropdown'><i class = 'fa fa-pie-chart'></i> MyNPS <i class = 'fa fa-caret-down'></i></a>
                                    <ul class = 'dropdown-menu dropdown-user'>
                                        <li><a href='public_html.php?i=1&o=1'><i class='fa fa-home'></i> MyNPS</a></li>
                                        <li><a href='public_html.php?i=7'><i class='fa fa-folder-open'></i> NPS Trend</a></li>
                                    </ul>
                                </li>
                                <li><a href = 'public_html.php?i=1' class = 'navurl'><i class = 'fa fa-child'></i> MyNPS</a></li>-->

            </ul>


            <ul class="nav navbar-nav navbar-right">
                <li class = 'dropdown'>
                                    <!--<a class = 'dropdown-toggle' data-toggle = 'dropdown'><i class = 'fa fa-user fa-fw'></i> <?php echo $_SESSION['Firstname'] ?> <i class = 'fa fa-caret-down'></i></a>-->
                    <a class="dropdown-toggle " data-toggle ='dropdown' style="padding: 0;">
                        <img  class="navPic" id="navPic" src="<?php echo $profileImg; ?>" >
                    </a>
                    <ul class = 'dropdown-menu dropdown-user'>

                        <li><a href = 'logout.php'><i class = 'fa fa-sign-out fa-fw'></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
            <?php
            if (!isset($_SESSION)) {
                session_start();
            }
            if ($_SESSION['sso']) {
                ?>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="https://atg.azenko.com.au/Login.aspx" target="atg"  ><img src="images/atg.jpg" alt="atg"></a>
                    </li>
                    <li>
                        <a href="https://pro360.com.au:4443/" target="pro360" ><img src="images/pro360.jpg" alt="pro60"></a>
                    </li>
                </ul>
            <?php } ?>


        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<!-- Modal EditUserModal-->
<div class="modal fade" id="EditUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal  EditUserModal -->

<script type="text/javascript">
    $(document).ready(function() {

        $("#navPic").error(function() {
            // alert('qwe');
            $(this).attr('src', 'images/user_icon.png');
        });

    });

    function openEditUserModal(el) {

        var target = $(el).attr('href');
        //alert(target);
        $("#EditUserModal .modal-content").load(target, function() {
            $("#EditUserModal").modal("show");
        });

        $('#EditUserModal').on('hidden.bs.modal', function(e) {
            e.preventDefault();
            location.reload();
        });
    }

</script>

