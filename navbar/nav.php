<?php
$role = $_SESSION['UserRoleID'];
$UserLoginID = $_SESSION['LoginID'];

if ($_SESSION['SharedOwnerShip']) {
    $userID = $_SESSION['OriginalUserID'];
} else {
    $userID = $_SESSION['UserProfileID'];
}

$profileImg = $_SESSION['ProfileImg'];
?>

<style>


    div.AppDrawerTop img {
        margin-left: 15px;
        margin-right: 16px;
        margin-top: 0px;
    }

    div.AppDrawerTop {

        padding-top: 19px;
        height: 61px;
        border-bottom: 1px solid white;
    }

    div.AppDrawerTop hr {
        margin-bottom: 10px;
        margin-top: 12px;
    }

    .sidenav {
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif !important;
        height: 100%;
        width: 0;
        position: fixed;
        z-index: 1;
        top: 0;
        left: 0;
        background-color: #FFFFFF;
        border-right: 2px solid #a0a0a0;
        color: #555555;
        overflow-x: hidden;
        transition: 0.2s;

    }

    .AppDrawerContent img {
        width: 35px;
        height: 35px;
        border: 1px solid #b0b0b0;
        border-radius: 50px;
    }

    div.AppDrawerContent img {
        margin-left: 20px;
        margin-right: 10px;
    }

    .AppDrawerContent a {

        padding: 11px;
        text-decoration: none;
        font-size: 16px;
        color: #818181;
        display: block;
        transition: 0.3s;
    }

    .sidenav a:hover {
        color: #f1f1f1;
    }

    .sidenav .closebtn {
        position: absolute;
        top: 0;      
    }

    .closebtn {       
        padding : 20px 3px 4px 0px !important;
        text-decoration: none;
        font-size: 16px;
        color: #818181;
        display: block;
        transition: 0.3s;
    }


    .sidenav .closebtn {       
        padding: 8px 8px 8px 32px;
        text-decoration: none;
        font-size: 16px;
        color: #818181;
        display: block;
        transition: 0.3s;
    }

    @media screen and (max-height: 450px) {
        .sidenav {padding-top: 15px;}
        .sidenav a {font-size: 18px;}
    }


    #navContainer {
        width:1270px !important;
    }
</style>
<nav>



</nav>
<nav class="navbar navbar-inverse navbar-fixed-top" id="navbar">
    <?php
    if (!isset($_SESSION)) {
        session_start();
    }
    if ($_SESSION['sso']) {

        $userApps = $_SESSION['UserApps'];
        ?>
        <div id="mySidenav" class="sidenav">
            <div class="AppDrawerTop"  style='border-bottom:1px solid #cef3fd;'>
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"> <img src="https://login.azenko.com.au/images/MENU_ICON.png"><span>Azenko apps</span> <img src="https://login.azenko.com.au/images/ARROW.png"></a>
            </div>
            <div class="AppDrawerContent"><?php foreach ($userApps['userApplications'] as $apps) { ?>
                    <a href="<?php echo $apps['url']; ?>"><img src="<?php echo $apps['logo']; ?>" onerror="this.onerror=null; this.src='https://login.azenko.com.au/images/AZENKO_ICON_ONLY.png'"  ></i><span><?php echo $apps['description']; ?> </span></a>                   
                    <?php
                }
                $countOtherApps = count($userApps['otherApplications']);
                if ($countOtherApps != 0) {
                    echo "<div class='AppDrawerTop'><li style='border-bottom:1px solid #DCDCDC'></li><li style=';padding-top: 10px;'><center><span> View more apps </span></center></li></div>";
                    foreach ($userApps['otherApplications'] as $apps) {
                        ?>
                        <a href="<?php echo $apps['url']; ?>"><img src="<?php echo $apps['logo']; ?>" onerror="this.onerror=null; this.src='https://login.azenko.com.au/images/AZENKO_ICON_ONLY.png'" ><span><?php echo $apps['description']; ?> </span></a>                 
                        <?php
                    }
                }
                ?></div>
        </div>
        <ul class="nav navbar-nav">        
            <li class = 'dropdown'>
                <a class = 'dropdown-toggle' onclick="openNav()" style="cursor:pointer"><img src="https://portal.azenko.com.au/images/MENU_ICON_WHITE.png" onerror="this.onerror=null; this.src='https://login.azenko.com.au/images/AZENKO_ICON_ONLY.png'" width="20px" height="20px"></i></a>
            </li>
        </ul>
    <?php } ?>
    <div class="container" id="navContainer">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="public_html.php">
                <img src="images/logo.png" alt="Telstra Blog">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="collapse">
            <ul class="nav navbar-nav">                
                <?php
                switch ($role) {
                    case '1': // global admin
                        ?>
                        <li><a href = 'public_html.php?i=0' class = 'navurl'><i class = 'fa fa-list'></i> Dashboard</a></li>
                        <li class = 'dropdown'>
                            <a class = 'dropdown-toggle' data-toggle = 'dropdown'><i class = 'fa fa-pie-chart'></i> NPS <i class = 'fa fa-caret-down'></i></a>
                            <ul class = 'dropdown-menu dropdown-user'>
                                <li><a href='public_html.php?i=2&o=1'><i class='fa fa-home'></i> Summary</a></li>
                                <!--<li><a href='public_html.php?i=2&o=2'><i class='fa fa-folder-open'></i> Episode Vs Interaction</a></li>-->
                                <li><a href='public_html.php?i=2&o=3'><i class='fa fa-upload'></i> Upload</a></li>
                            </ul>
                        </li>
                        <li class = 'dropdown'>
                            <a class = 'dropdown-toggle' data-toggle = 'dropdown'><i class = 'fa fa-comments'></i> Data Recon <i class = 'fa fa-caret-down'></i></a>
                            <ul class = 'dropdown-menu dropdown-user'>
                                <li><a href = 'public_html.php?i=6&o=1' class = 'navurl'><i class = 'fa fa-comments'></i> Update Blank</a></li>
                                <li><a href='public_html.php?i=6&o=2'><i class='fa fa-folder-open'></i> Modified Surveys</a></li>
                                <li><a href = 'public_html.php?i=6&o=9' class = 'navurl'><i class = 'fa fa-archive'></i> Latest Surveys</a></li>

                            </ul>
                        </li>
                        <li><a href='public_html.php?i=6&o=3'><i class='fa fa-newspaper-o'></i> Surveys List</a></li>
                        <li><a href = 'public_html.php?i=4' class = 'navurl'><i class = 'fa fa-gears'></i> Admin</a></li>
                        <li><a href = 'public_html.php?i=9' class = 'navurl'><i class = 'fa fa-question-circle'></i> Help</a></li>
                        <?php
                        break;
                    case '2': //store admin
                    case '3': //store owner
                        ?>
                        <!--<li><a href = 'public_html.php?i=0' class = 'navurl'><i class = 'fa fa-list'></i> Dashboard</a></li>-->
                        <li class = 'dropdown'>
                            <a class = 'dropdown-toggle' data-toggle = 'dropdown'><i class = 'fa fa-list'></i> Dashboard <i class = 'fa fa-caret-down'></i></a>
                            <ul class = 'dropdown-menu dropdown-user'>
                                <?php if ($_SESSION['HasStoreType'] == 'Only Others') { ?>
                                    <li><a href='public_html.php?i=0&o=3'><i class='fa fa-folder-open'></i>Local Dashboard </a></li>
                                <?php } elseif ($_SESSION['isPrivate']) { ?>
                                    <li><a href='public_html.php?i=0&o=2'><i class='fa fa-folder-open'></i>Local Dashboard </a></li>
                                <?php } else { ?>
                                    <li><a href='public_html.php?i=0&o=1'><i class='fa fa-home'></i> Main Dashboard</a></li>
                                    <li><a href='public_html.php?i=0&o=2'><i class='fa fa-folder-open'></i> Local Dashboard</a></li>
                                <?php } ?>

                            </ul>
                        </li>
                        <li class = 'dropdown'>
                            <a class = 'dropdown-toggle' data-toggle = 'dropdown'><i class = 'fa fa-pie-chart'></i> NPS <i class = 'fa fa-caret-down'></i></a>
                            <ul class = 'dropdown-menu dropdown-user'>
                                <li><a href='public_html.php?i=2&o=1'><i class='fa fa-home'></i> Summary</a></li>
                                <li><a href='public_html.php?i=2&o=6'><i class='fa fa-pie-chart'></i> Summary by Survey Type</a></li>
                                <li><a href='public_html.php?i=2&o=8'><i class='fa fa-line-chart'></i> NPS vs Volume Trend</a></li>
                                <!--<li><a href='public_html.php?i=2&o=2'><i class='fa fa-folder-open'></i> Episode Vs Interaction</a></li>-->
                            </ul>
                        </li>
                        <?php if ($_SESSION['HasStoreType'] == 'Only Others') { ?>
                            <li><a href='public_html.php?i=2&o=7' class = 'navurl'><i class='fa fa-upload'></i> Upload</a></li>
                        <?php } ?>
                        <li><a href = 'public_html.php?i=11' class = 'navurl'><i class = 'fa fa-calculator'></i> NPS Trend</a></li>
                        <li class = 'dropdown'>
                            <a class = 'dropdown-toggle' data-toggle = 'dropdown'><i class = 'fa fa-comments'></i> Data Recon <i class = 'fa fa-caret-down'></i></a>
                            <ul class = 'dropdown-menu dropdown-user'>
                                <li><a href = 'public_html.php?i=6&o=1' class = 'navurl'><i class = 'fa fa-comments'></i> Update Blank</a></li>
                                <li><a href='public_html.php?i=6&o=2'><i class='fa fa-folder-open'></i> Modified Surveys</a></li>  
                            </ul>
                        </li>
                        <li><a href = 'public_html.php?i=6' class = 'navurl'><i class = 'fa fa-comments'></i> Data Recon</a></li>
                        <li><a href='public_html.php?i=6&o=3'><i class='fa fa-newspaper-o'></i> Surveys List</a></li>
                        <li><a href = 'public_html.php?i=4' class = 'navurl'><i class = 'fa fa-gears'></i> Admin</a></li>
                        <li><a href = 'public_html.php?i=9' class = 'navurl'><i class = 'fa fa-question-circle'></i> Help</a></li>
                        <?php
                        break;
                    case '4'; // Employee
                        ?>
                        <li class = 'dropdown'>
                            <a class = 'dropdown-toggle' data-toggle = 'dropdown'><i class = 'fa fa-list'></i> Dashboard <i class = 'fa fa-caret-down'></i></a>
                            <ul class = 'dropdown-menu dropdown-user'>
                                <?php if ($_SESSION['HasStoreType'] == 'Only Others') { ?>
                                    <li><a href='public_html.php?i=0&o=3'><i class='fa fa-folder-open'></i>Local Dashboard </a></li>
                                <?php } elseif ($_SESSION['isPrivate']) { ?>
                                    <li><a href='public_html.php?i=0&o=2'><i class='fa fa-folder-open'></i>Local Dashboard </a></li>
                                <?php } else { ?>
                                    <li><a href='public_html.php?i=0&o=1'><i class='fa fa-home'></i> Main Dashboard</a></li>
                                    <li><a href='public_html.php?i=0&o=2'><i class='fa fa-folder-open'></i> Local Dashboard</a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li class = 'dropdown'>
                            <a class = 'dropdown-toggle' data-toggle = 'dropdown'><i class = 'fa fa-pie-chart'></i> MyNPS <i class = 'fa fa-caret-down'></i></a>
                            <ul class = 'dropdown-menu dropdown-user'>
                                <li><a href='public_html.php?i=1&o=1'><i class='fa fa-home'></i> MyNPS</a></li>
                                <li><a href='public_html.php?i=7'><i class='fa fa-folder-open'></i> NPS Trend</a></li>
                            </ul>
                        </li>
                        <!--<li><a href = 'public_html.php?i=1' class = 'navurl'><i class = 'fa fa-child'></i> MyNPS</a></li>-->
                        <?php
                        break;
                    case '5'; // Store Leader
                        ?>
                        <li class = 'dropdown'>
                            <a class = 'dropdown-toggle' data-toggle = 'dropdown'><i class = 'fa fa-list'></i> Dashboard <i class = 'fa fa-caret-down'></i></a>
                            <ul class = 'dropdown-menu dropdown-user'>
                                <?php if ($_SESSION['HasStoreType'] == 'Only Others') { ?>
                                    <li><a href='public_html.php?i=0&o=3'><i class='fa fa-folder-open'></i>Local Dashboard </a></li>
                                <?php } elseif ($_SESSION['isPrivate']) { ?>
                                    <li><a href='public_html.php?i=0&o=2'><i class='fa fa-folder-open'></i>Local Dashboard </a></li>
                                <?php } else { ?>
                                    <li><a href='public_html.php?i=0&o=1'><i class='fa fa-home'></i> Main Dashboard</a></li>
                                    <li><a href='public_html.php?i=0&o=2'><i class='fa fa-folder-open'></i> Local Dashboard</a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li class = 'dropdown'>
                            <a class = 'dropdown-toggle' data-toggle = 'dropdown'><i class = 'fa fa-pie-chart'></i> MyNPS <i class = 'fa fa-caret-down'></i></a>
                            <ul class = 'dropdown-menu dropdown-user'>
                                <li><a href='public_html.php?i=1&o=1'><i class='fa fa-home'></i> MyNPS</a></li>
                                <!--<li><a href='public_html.php?i=7'><i class='fa fa-calculator'></i> NPS Trend</a></li>-->
                            </ul>
                        </li>
                        <!--<li><a href = 'public_html.php?i=1' class = 'navurl'><i class = 'fa fa-child'></i> MyNPS</a></li>-->
                        <li class = 'dropdown'>
                            <a class = 'dropdown-toggle' data-toggle = 'dropdown'><i class = 'fa fa-pie-chart'></i> NPS <i class = 'fa fa-caret-down'></i></a>
                            <ul class = 'dropdown-menu dropdown-user'>
                                <li><a href='public_html.php?i=2&o=1'><i class='fa fa-home'></i> Summary</a></li>
                                <li><a href='public_html.php?i=2&o=6'><i class='fa fa-pie-chart'></i> Summary by Survey Type</a></li>
                                <li><a href='public_html.php?i=2&o=8'><i class='fa fa-line-chart'></i> NPS vs Volume Trend</a></li>                                                                                                                                                                                                                                                                                                                                                                                                <!--<li><a href='public_html.php?i=2&o=2'><i class='fa fa-folder-open'></i> Episode Vs Interaction</a></li>-->

                                <?php if ($UserLoginID == '458') { ?>
                                    <li><a href='public_html.php?i=2&o=3'><i class='fa fa-upload'></i> Upload</a></li>
                                <?php } ?>

                            </ul>
                        </li>
                        <li><a href = 'public_html.php?i=11' class = 'navurl'><i class = 'fa fa-calculator'></i> NPS Trend</a></li>
                        <?php if ($_SESSION['ReadOnly'] != 1) { ?>
                            <li><a href = 'public_html.php?i=6' class = 'navurl'><i class = 'fa fa-comments'></i> Data Recon</a></li>
                            <li><a href='public_html.php?i=6&o=3'><i class='fa fa-newspaper-o'></i> Surveys List</a></li>
                            <li><a href = 'public_html.php?i=4' class = 'navurl'><i class = 'fa fa-gears'></i> Admin</a></li>
                            <li><a href = 'public_html.php?i=9' class = 'navurl'><i class = 'fa fa-question-circle'></i> Help</a></li>
                        <?php } ?>
                        <?php
                        break;
                    case '6'; // Data Entry
                        ?>
                        <li><a href='public_html.php?i=2&o=3'><i class='fa fa-upload'></i> Upload</a></li>
                        <li class = 'dropdown'>
                            <a class = 'dropdown-toggle' data-toggle = 'dropdown'><i class = 'fa fa-comments'></i> Data Recon <i class = 'fa fa-caret-down'></i></a>
                            <ul class = 'dropdown-menu dropdown-user'>
                                <li><a href = 'public_html.php?i=6&o=1' class = 'navurl'><i class = 'fa fa-comments'></i> Update Blank</a></li>
                                <li><a href='public_html.php?i=6&o=2'><i class='fa fa-folder-open'></i> Modified Surveys</a></li>
                                <li><a href = 'public_html.php?i=6&o=7' class = 'navurl'><i class = 'fa fa-gears'></i> Employee List</a></li>
                                <li><a href = 'public_html.php?i=6&o=8' class = 'navurl'><i class = 'fa fa-pie-chart'></i> NPS Reports</a></li>
                                <li><a href = 'public_html.php?i=6&o=9' class = 'navurl'><i class = 'fa fa-archive'></i> Latest Surveys</a></li>
                            </ul>
                        </li>

                        <?php
                        break;
                    default:
                        ?>
                        <li><a href = 'public_html.php?i=0' class = 'navurl'><i class = 'fa fa-list'></i> Dashboard</a></li>
                <?php } ?>


            </ul>


            <ul class="nav navbar-nav navbar-right">
                <li class = 'dropdown'>
                                    <!--<a class = 'dropdown-toggle' data-toggle = 'dropdown'><i class = 'fa fa-user fa-fw'></i> <?php echo $_SESSION['Firstname'] ?> <i class = 'fa fa-caret-down'></i></a>-->
                    <a class="dropdown-toggle " data-toggle ='dropdown' style="padding: 0;">
                        <img  class="navPic" id="navPic" src="<?php echo $profileImg; ?>" >
                    </a>
                    <ul class = 'dropdown-menu dropdown-user'>
                        <?php
                        switch ($role) {
                            case '1': //global admin
                            case '2': //store admin
                            case '3': //store owner
                                ?>
                                <li>
                                    <a data-toggle="modal" href="admin/modal/EditUserModal.php?userid=<?php echo $userID; ?>" data-target="#EditUserModal" onclick="openEditUserModal(this)" ><i class = 'fa fa-shield fa-fw'> </i> Edit Profile</a></a>
                                    <a href='public_html.php?i=5'><i class='fa fa-camera'></i> Edit Profile Picture</a>
                                </li>
                                     <!--<a data-toggle = 'modal' data-target = '#ChangePass'><i class = 'fa fa-shield fa-fw'> </i> Change Password</a></li>-->
                                <?php
                                //add change password or edit profile
                                break;
                            case '4': //employee
                            case '5':
                                ?>
                                <li>
                                    <a href='public_html.php?i=5'><i class='fa fa-camera'></i> Edit Profile Picture</a>
                                </li>

                                <?php
                                break; //store leader
                                break;
                        }
                        ?>
                        <li><a href = 'logout.php'><i class = 'fa fa-sign-out fa-fw'></i> Logout</a></li>
                    </ul>
                </li>
            </ul>         


        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<!-- Modal EditUserModal-->
<div class="modal fade" id="EditUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content"></div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal  EditUserModal -->

<script type="text/javascript">
    $(document).ready(function () {

        $("#navPic").error(function () {
            // alert('qwe');
            $(this).attr('src', 'images/user_icon.png');
        });

    });

    function openEditUserModal(el) {

        var target = $(el).attr('href');
        //alert(target);
        $("#EditUserModal .modal-content").load(target, function () {
            $("#EditUserModal").modal("show");
        });

        $('#EditUserModal').on('hidden.bs.modal', function (e) {
            e.preventDefault();
            location.reload();
        });
    }

</script>

<script>
    function openNav() {
        document.getElementById("mySidenav").style.width = "200px";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }
</script>