<?php
include('db/connection.php');
require 'dashboard/function/LatestRecord.php';

$latestRec = getLatestRercord();

session_start();

date_default_timezone_set('Australia/Melbourne');
ini_set('max_execution_time', 300);

$role = $_SESSION['UserRoleID'];
$AdminPages = array("1", "2", "3", "5", "6");
$myNpsPage = array("1", "2", "3", "6");
$PersonalTracker = array("4", "5");
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>NetPro</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Logo-->
        <link rel="shortcut icon" href="images/logoIcon.png">
        <!--Custom Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,400italic,500,500italic,700,700italic,900,900italic,300italic,300" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100" rel="stylesheet" type="text/css">
        <!--Font Awesome-->
        <link href="includes/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <!--Animate Css -->
        <link href="includes/css/animate.css" rel="stylesheet" type="text/css"/>
        <!--Bootstrap-->
        <link href="includes/css/bootstrap.min.flatly.css" rel="stylesheet" type="text/css"/>
        <!--Custom CSS -->
        <link href="includes/css/styles.css" rel="stylesheet" type="text/css"/>
        <!--Media Queries -->
        <link href="includes/css/media-queries.css" rel="stylesheet" type="text/css"/>
        <!--DataTables-->
        <link href="datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
        <!--DataTables Buttons-->
        <link href="https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>
        <!--Jquery UI for date picker-->
        <link href="includes/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <!--toggle bottons -->
        <link href="includes/css/bootstrapToggle.css" rel="stylesheet" type="text/css"/>
        <!--Jquery-->
        <script type="text/javascript" language="javascript"  src="includes/js/jquery1.11.1.min.js"></script>
        <!--<script type="text/javascript" language="javascript"  src="includes/js/autologout.js"></script>-->
    </head>

    <body>
        <div class="main-wrapper">
            <div class = "page-content" >
                <?php
                if (!isset($_SESSION['UserProfileID']) && !isset($_SESSION['EmpProfileID'])) {
                    header("location: /index.php");
                }elseif(isset($_SESSION['LoginID']) && $_SESSION['LoginID'] == '6445'){
                    $_GET['o'] = 4;
                     include('dashboard.php');
                } else {

                    require('navbar/nav.php');

                    $link = $_GET['i'];

                    if ($role == 6 && !($_SESSION['Redirect']) & !($_SESSION['ChangeEmail'])) {
                        $onlyhave = array('2', '6', '4', '12');
                        if (isset($_GET['i']) && !in_array($_GET['i'], $onlyhave)) {
                            ?>
                            <script type="text/javascript">
                                var link = $('<a href=public_html.php?i=6 rel="lightbox" id="link">Click</a>');
                                $("body").append(link);
                                $("#link")[0].click();
                            </script>
                            <?php
                            exit();
                        } elseif (isset($_GET['i']) && $_GET['i'] == '2' && $_GET['o'] != '3') {
                            ?>
                            <script type="text/javascript">
                                var link = $('<a href=public_html.php?i=6 rel="lightbox" id="link">Click</a>');
                                $("body").append(link);
                                $("#link")[0].click();

                            </script>
                            <?php
                            exit();
                        }
                    }

//                    $toannounce = array('1', '2', '3', '5', '6');
//                    if (!isset($_SESSION['ReadMessageBoard']) && in_array($_GET['i'], $toannounce)) {
//                        include('messageboard.php');
//                    }

                    switch ($link) {
                        case 0:
                            include('dashboard.php');
                            break;
                        case 1:
                            if (in_array($role, $myNpsPage)) {
                                include("404.php");
                            } else {
                                include('mynps.php');
                            }
                            break;
                        case 2:
                            if (in_array($role, $AdminPages) OR $role == '6') {
                                include('nps.php');
                            } else {
                                include("404.php");
                            }
                            break;
                        case 4:
                            if (in_array($role, $AdminPages)) {
                                include('admin.php');
                            } else {
                                include("404.php");
                            }
                            break;
                        case 5:
                            //  if (in_array($role, $AdminPages) OR $role == '4') {
                            include('imageEditor2/examples/crop-avatar/indexv2.php');
//                            } else {
//                                include("404.php");
//                            }
                            break;
                        case 6:
                            if (in_array($role, $AdminPages) OR $role == '6') {
                                include('datarecon.php');
                            } else {
                                include("404.php");
                            }
                            break;
                        case 7:
                            if (in_array($role, $PersonalTracker)) {
                                include('mynpstracker.php');
                            } else {
                                include("404.php");
                            }
                            break;
                        case 8:
                            include('changepass.php');
                            break;
                        case 9:
                            include('help.php');
                            break;
                        case 11:
                            if (in_array($role, $AdminPages)) {
                                include('npstracker.php');
                            } else {
                                include("404.php");
                            }
                            break;
                        case 12:
                            include('changeusername.php');
                            break;
                        default:
                            include("404.php");
                            break;
                    }
                }
                ?>
            </div>
        </div>
        <!--// Reset Password Modal-->
        <?php if ($_SESSION['Redirect'] && $_SESSION['ChangeEmail'] && $_GET['i'] != 12) { ?>
            <script type="text/javascript">
                var link = $('<a href=public_html.php?i=12 rel="lightbox" id="link">Click</a>');
                $("body").append(link);
                $("#link")[0].click();
            </script>
        <?php } elseif ($_SESSION['Redirect'] && !$_SESSION['ChangeEmail'] && $_GET['i'] != 8) { ?>
            <script type="text/javascript">
                var link = $('<a href=public_html.php?i=8 rel="lightbox" id="link">Click</a>');
                $("body").append(link);
                $("#link")[0].click();
            </script>
        <?php } elseif ($_SESSION['ChangeEmail'] && $_GET['i'] != 12) { ?>
            <script type="text/javascript">
                var link = $('<a href=public_html.php?i=12 rel="lightbox" id="link">Click</a>');
                $("body").append(link);
                $("#link")[0].click();
            </script>
        <?php } ?>

        <!-- Bootstrap -->
        <script src="includes/js/bootstrap.min.js" type="text/javascript"></script>
        <!--Full Slider-->
        <script src="includes/js/jquery.cycle.all.js" type="text/javascript"></script>
        <script src="includes/js/jquery.maximage.js" type="text/javascript"></script>
        <!-- Custom js-->
        <script src="includes/js/custom.js" type="text/javascript"></script>
        <!--toggle bottons-->
        <script src="includes/js/bootstrapToggle.js" type="text/javascript"></script>
        <!--DataTables-->
        <script src="datatables/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
        <!--DataTables Exporting Tools-->
        <script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
        <script src="//cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js" type="text/javascript"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js" type="text/javascript"></script>
        <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js" type="text/javascript"></script>
        <script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js" type="text/javascript"></script>
        <!--High Charts-->
        <script src="includes/js/highcharts-custom.js" type="text/javascript"></script>
        <script src="includes/js/jquery-ui.js" type="text/javascript"></script>
        <script src="includes/js/jquery.easing.1.3.js" type="text/javascript"></script>
        <!--Password Authentication-->
        <script src="includes/js/md5.js" type="text/javascript"></script>

    </body>
</html>

