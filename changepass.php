<?php
session_start();
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
$loginID = $_SESSION['LoginID'];

$sqlGetCurrentPass = "Select f_Password From t_userlogin Where f_UserLoginID = '{$loginID}'";
$result = mysql_query($sqlGetCurrentPass);
while ($row = mysql_fetch_assoc($result)) {
    $currentPass = $row['f_Password'];
}
?>

<form role="form" id="ChangePassForm"><!-- /modal-header -->
    <!-- Modal transparent -->
    <div class="modal modal-transparent fade" id="modal-transparent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa fa-shield"></i> NetPro Security Update</h4>
                </div>

                <div class="modal-body">
                    <blockquote id="greetings">
                        <b>Hi Mate!</b>
                        <br>We've recently upgraded NetPro security to ensure data sovereignty.
                        <br>Please update your password with criteria below.
                    </blockquote>

                    <div class="well">
                        <div class="form-group">
                            <label class="control-label" for="UserName">Current Password:</label>
                            <input type="password" class="form-control" id="currentPassword" name="currentPassword" placeholder="Enter Current Password"  required minlength="5">
                            <div class="checker" id="checker"></div>
                        </div> <!-- /current password -->

                        <div class="form-group">
                            <label class="control-label" for="Password">New Password:</label>
                            <input type="password" class="form-control" id="newPassword" name="newPassword" placeholder="Enter New Password" required minlength="8">
                        </div> <!-- /new password -->

                        <div class="form-group">
                            <label class="control-label" for="Password">Confirm Password:</label>
                            <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" placeholder="Confirm New Password" required minlength="8">
                        </div><!-- /confirm new password -->
                    </div>

                    <strong class="text-center text-success"><i class="fa fa fa-gears"></i> REQUIREMENTS </strong>

                    <ul id="pswd_info"  class="list-group">
                        <li class="list-group-item">
                            Current password must <strong class="text-success"> match  </strong>
                            <span class="marker"  id="current"></span>
                        </li>
                        <li class="list-group-item">
                            New password must <strong  class="text-danger">not match previous </strong>
                            <span class="marker"  id="old"></span>
                        </li>
                        <li class="list-group-item">
                            Confirm password must <strong class="text-success">match new</strong>
                            <span class="marker" id="match"></span>
                        </li>
                        <li class="list-group-item">
                            With at least <strong class="text-success">one letter</strong>
                            <span class="marker"  id="letter"></span>
                        </li>
                        <li class="list-group-item">
                            With at least <strong class="text-success">one capital letter</strong>
                            <span class="marker"  id="capital"></span>
                        </li>
                        <li class="list-group-item">
                            With at least <strong class="text-success">one number</strong>
                            <span class="marker"  id="number"></span>
                        </li>
                        <li class="list-group-item">
                            Must be at least <strong class="text-success">8 characters</strong>
                            <span class="marker"  id="length"></span>
                        </li>
                    </ul>

                    <div id="ajaxmsgs"></div>  <!-- ajaxmsg div -->
                </div>

                <div class="modal-footer">
                    <input type="submit" style="visibility: hidden"/>
                    <button type="button" class="btn btn-sm btn-info" data-dismiss="modal" id="btnClose" style="display:none">Continue</button>
                    <button type="button" class="btn btn-sm btn-warning"  id="btnCancel"  >Cancel</button>
                    <button type="button" class="btn btn-sm btn-success"  id="btnSubmit"  >Submit</button>
                </div>
            </div>
        </div>
    </div>

</form>


<script type="text/javascript">
    $(document).ready(function() {
        // $('#pswd_info').hide();
        $('#modal-transparent').modal({
            backdrop: 'static',
            keyboard: false
        }, 'show');

        // .modal-backdrop classes

        $(".modal-transparent").on('show.bs.modal', function() {
            setTimeout(function() {
                $(".modal-backdrop").addClass("modal-backdrop-transparent");
            }, 0);
        });
        $(".modal-transparent").on('hidden.bs.modal', function() {
            $(".modal-backdrop").addClass("modal-backdrop-transparent");
        });


        var concheck = false;
        var curcheck = false;
        var verification = false;
        var curError = '';
        var conError = '';
        var button = $('#btnSubmit');
        var loginID = '<?php echo $loginID; ?>';

        var conPass = {old: null, match: null, length: null, capital: null, letter: null, number: null};

        $('#confirmPassword,#newPassword').keyup(function(e) {
            var pass1 = $('#newPassword').val();
            var pass2 = $('#confirmPassword').val();
            var div = $('#passwordStrength');
            var params = 'log_password_n=' + pass1 + '&login_id_n=' + loginID;

            $.ajax({
                type: "POST",
                data: params,
                url: 'functions/checkPass.php/',
                dataType: 'json',
                success: function(responseText) { // Get the result and asign to each cases
                    verification = responseText.msg;
                    if (verification == true) {
                        $('#old').removeClass('fa fa-check').addClass('fa fa-times');
                        conPass.old = false;
                    } else {
                        $('#old').removeClass('fa fa-times').addClass('fa fa-check');
                        conPass.old = true;
                        // if (pass1.lenght > 0 || pass2.lenght > 0) {
                        if (pass1 != pass2) {
                            $('#match').removeClass('fa fa-check').addClass('fa fa-times');
                            conPass.match = false;
                        } else if (pass1 == pass2) {
                            $('#match').removeClass('fa fa-times').addClass('fa fa-check');
                            conPass.match = true;
                        }
                    }
                }
            });

            //validate the length
            if (pass1.length < 8) {
                $('#length').removeClass('fa fa-check').addClass('fa fa-times');
                conPass.length = false;
            } else {
                $('#length').removeClass('fa fa-times').addClass('fa fa-check');
                conPass.length = true;

            }
            //validate letter
            if (pass1.match(/[A-z]/)) {
                $('#letter').removeClass('fa fa-times').addClass('fa fa-check');
                conPass.letter = true;
            } else {
                $('#letter').removeClass('fa fa-check').addClass('fa fa-times');
                conPass.letter = false;
            }

            //validate capital letter
            if (pass1.match(/[A-Z]/)) {
                $('#capital').removeClass('fa fa-times').addClass('fa fa-check');
                conPass.capital = true;
            } else {
                $('#capital').removeClass('fa fa-check').addClass('fa fa-times');
                conPass.capital = false;
            }

            //validate number
            if (pass1.match(/\d/)) {
                $('#number').removeClass('fa fa-times').addClass('fa fa-check');
                conPass.number = true;
            } else {
                $('#number').removeClass('fa fa-check').addClass('fa fa-times');
                conPass.number = false;
            }
            e.preventDefault();
        });

        $('#currentPassword').keyup(function(e) {
            var curverif = '';
            var value = $(this).val();
            var inputpass = value;
            var ipass = $('#checker');
            var params = 'log_password_n=' + inputpass + '&login_id_n=' + loginID;
            $.ajax({
                type: "POST",
                data: params,
                url: 'functions/checkPass.php/',
                dataType: 'json',
                success: function(responseText) { // Get the result and asign to each cases

                    curverif = responseText.msg;
                    if (curverif == false) {
                        $('#current').removeClass('fa fa-check').addClass('fa fa-times');
                        curcheck = false;
                    } else if (curverif == true) {
                        $('#current').removeClass('fa fa-times').addClass('fa fa-check');
                        curcheck = true;
                    }
                }
            });
            e.preventDefault();
        });

        $("input").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
                $("#btnSubmit").click();
            }
        });

        $('#confirmPassword,#newPassword').keydown(function(e) { // Dont allow users to enter spaces for their username and passwords
            if (e.which == 32) {
                return false;
            }
        });

        $('#btnSubmit').click(function(event) {
            if ($("#ChangePassForm")[0].checkValidity()) {
                var url = 'admin/function/SubFunctionCaller.php?module=ChangePassword';
                var mydata = $("#ChangePassForm").serialize();
                if ((validateCheckPass(conPass))) {
                    $.ajax({
                        url: url,
                        type: 'get',
                        data: mydata,
                        dataType: 'json',
                        success: function(result) {
                            document.getElementById('ajaxmsgs').style.display = 'block';
                            if (result.type == 'Success') {
                                $('#ajaxmsgs').html("<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Awesome!</strong> " + result.message + "</div>");
                                $('#btnClose').show();
                                $('#btnSubmit,#btnCancel').css("display", "none");

                            } else {
                                $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Snap!</strong> " + result.message + "</div>");
                            }
                        },
                        error: function() {
                            document.getElementById('ajaxmsgs').style.display = 'block';
                            $('#ajaxmsgs').html("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a><strong>Error!</strong></div>");

                        }
                    });
                } else {
                }

            } else {
                $("#ChangePassForm").find(':submit').click();
            }
            event.preventDefault();

        }); // ajax button form submit

        $("#btnCancel").click(function(event) {
            window.location.href = "logout.php";
        });

        $("#btnClose").click(function(event) {
            var UserProfile = <?php echo $_SESSION['UserRoleID']; ?>;
            var urlTogo = '';
            if (UserProfile == 4) {
                urlTogo = 'public_html.php?i=1';
            } else if (UserProfile == 6) {
                urlTogo = 'public_html.php?i=6';
            } else {
                urlTogo = 'public_html.php?i=2&o=1';
            }
            window.location.href = urlTogo;
        });

        function validateCheckPass(obj) {
            var returnValue = new Array();
            var thisValue = '';
            $.each(obj, function(key, value) {
                if (value == false) {
                    returnValue.push(false);
                } else {
                    returnValue.push(true);
                }
            });
            if (jQuery.inArray(false, returnValue) !== -1) {
                thisValue = false;
            } else {
                thisValue = true;
            }

            return thisValue;
        }


    });
</script>