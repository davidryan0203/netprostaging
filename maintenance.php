<!DOCTYPE html>
<html lang="en">
    <head>
        <title>NetPro</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Logo-->
        <link rel="shortcut icon" href="images/logoIcon.png">
        <!--Custom Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,400italic,500,500italic,700,700italic,900,900italic,300italic,300" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100" rel="stylesheet" type="text/css">
        <!--Font Awesome-->
        <link href="includes/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <!--Full slider -->
        <link href="includes/css/jquery.maximage.css" rel="stylesheet" type="text/css"/>
        <link href="includes/css/jquery.screensize.css" rel="stylesheet" type="text/css"/>
        <!--Animate Css -->
        <link href="includes/css/animate.css" rel="stylesheet" type="text/css"/>
        <!--Bootstrap-->
        <link href="includes/css/bootstrap.min.flatly.css" rel="stylesheet" type="text/css"/>
        <!--Custom CSS -->
        <link href="includes/css/stylesV2.css" rel="stylesheet" type="text/css"/>
        <!--Custom Css-->
        <link href="includes/css/homeV2.css" rel="stylesheet">
        <!--Media Queries -->
        <link href="includes/css/media-queries.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">


    </head>
    <Style>

        h1 {
            font-family: "Open Sans";  font-style: normal; font-variant: normal; font-weight: 700;
        }
        h2 {
            font-family: "Open Sans";  font-style: normal; font-variant: normal; font-weight: 700;
        }
        h3 {
            font-family: "Open Sans";  font-style: normal; font-variant: normal; font-weight: 700;
        }
        h4 {
            font-family: "Open Sans";  font-style: normal; font-variant: normal; font-weight: 700;
        }
        p {
            font-family: "Open Sans";  font-style: normal; font-variant: normal; font-weight: 400;
        }
        blockquote {
            font-family: "Open Sans"; font-style: normal; font-variant: normal; font-weight: 400;
        }
        pre {
            font-family: "Open Sans"; font-size: 13px; font-style: normal; font-variant: normal; font-weight: 400;
        }
        p, span, h1,h2,h3,h4,h5{
            color:#5F6160;
        }
        .loginBtn{
            height: 30px !important;
            color: #fff !important;
            padding-top: 3px !important;
            margin-top: 3px !important;
        }
        html, body {
            height: 100%;
        }
        body {
            background-color: #00B6CB;
            margin: 0;
            /*background-image:url('images/backgrounds/thank.png');
            background-size: cover;*/
            height: 90vh;
            min-height: 90vh;
        }
        .flex-container {
            height: 100%;
            padding: 0;
            margin: 0;
            display: -webkit-box;
            display: -moz-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .row {
            width: auto;
        }

        .flex-item {
            padding: 5px;
            width: 20px;
            height: 20px;
            margin: 10px;
            line-height: 20px;
            color: white;
            font-weight: bold;
            font-size: 2em;
            text-align: center;
        }
        .navbar {
            height: 90px;
            padding-top:20px;
        }

        .panel{
            -webkit-box-shadow: 4px 1px 17px 0px rgba(0,0,0,0.39);
            -moz-box-shadow: 4px 1px 17px 0px rgba(0,0,0,0.39);
            box-shadow: 4px 1px 17px 0px rgba(0,0,0,0.39);
        }

        .flex-container{
            background-image:url('images/backgrounds/thank.png');
            background-size: cover;
        }
    </Style>

    <body>
        <section id="banner" style="min-height:90px;height: 90px;">

        </section>
        <div class="flex-container">
            <div class="row panel panel-default" style="border-radius: 15px;">
                <div class="panel-body" style="padding:35px 75px 35px 55px !important;">
                    <h3 style="color:#08B99F;"><strong style="font-size: larger !important;">Netpro Maintenance!</strong></h3><br/>
                    <h4>Hi NetPro is currently under maintenance.</h4>
                    <br><h4>Sorry for the inconvenience.</h4>
                    <h4>Please contact <a href="mailto:support@azenko.com.au?subject=Netpro SSO account problem">support@azenko.com.au</a> for assistance.</h4>
                    <br/>
                    <h4>Regards</h4>
                    <h4>Azenko Support Team</h4>
                    <a href="maito:support@azenko.com.au"><h4 style="color:#138CB1">support@azenko.com.au</h4></a>
                </div>
            </div>
        </div>


        </div> <!-- /.modal  DeleteUserModal -->
    </body>

    <!--Jquery-->
    <script  src="includes/js/jquery1.11.1.min.js" type="text/javascript"></script>
    <!--Full Slider-->
    <script src="includes/js/jquery.cycle.all.js" type="text/javascript"></script>
    <script src="includes/js/jquery.maximage.js" type="text/javascript"></script>
    <!-- Bootstrap -->
    <script src="includes/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.min.js"></script>
</html>

