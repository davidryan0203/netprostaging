<div class="container" id='AdminPage'>
    <?php
    session_start();
    $role = $_SESSION['UserRoleID'];
    if ($role == 6) {
        $sublink = 0;
    } else {
        $sublink = $_GET['o'];
    }


    switch ($sublink) {
        case 0:
            include('admin/index.php');
            break;
        case 1:
            include('admin/store.php');
            break;
        case 2:
            include('admin/user.php');
            break;
        case 3:
            include('admin/employee.php');
            break;
        default:
            include('admin/store.php');
            break;
    }
    ?>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="includes/js/custom.js" type="text/javascript"></script>





