<?php
session_start();
$storeType = $_GET['storeType'];
$role = $_SESSION['UserRoleID'];
//echo $role;
?>

<section class='reports'>
    <input type="hidden" id="storeType" name="storeType" value="<?php echo $storeType; ?>" />

    <div class="main-content no-bg">
        <div class="btn-group group1 pull-right" id="champ-group" role="group" aria-label="...">
            <!--<button type="button" id="StoreChampionInteraction" data-text="Interaction" class="btn btn-xs btn-primary ">Interaction</button>-->
            <button type="button" id="StoreChampionMobile" data-text="Mobile" class="btn btn-xs btn-warning">Mobile</button>
            <button type="button" id="StoreChampionFixed" data-text="Fixed" class="btn btn-xs btn-info">Fixed</button>  </div>
        <h4 class="sub-header"><i class="fa fa-user fa-fw"></i> Featured Store Champions</h4>

        <div id="interactionChamp">
            <div class="loader" id="ChampLocalTbcLoader"></div>
            <div  id="ChampLocalTbcContainer"> </div>
        </div>
    </div>

    <!--    <div class="main-content  champRanking">
            <div  id="champRanking">
                <h4 class="sub-header"><i class="fa fa-users fa-fw"></i> Interaction Champions Summary</h4>
                <div class="loader" id="InteractionChampLoader"></div>
                <div id="InteractionChampionRanking" style="margin-top: 5px;"> </div>
            </div>
        </div>-->

    <div class="main-content champRanking">
        <h4 class="sub-header"><i class="fa fa-users fa-fw"></i> Mobile Episode Champions Summary</h4>
        <div id="champRanking">
            <div class="loader" id="MobileChampLoader"></div>
            <div id="MobileChampionRanking" style="margin-top: 5px;"> </div>
        </div>
    </div>

    <div class="main-content champRanking">
        <h4 class="sub-header"><i class="fa fa-users fa-fw"></i> Fixed Interaction and Episode Champions Summary</h4>
        <div id="champRanking">
            <div class="loader" id="FixedChampLoader"></div>
            <div id="FixedChampionRanking" style="margin-top: 5px;"> </div>
        </div>
    </div>

</section>

<script src="../includes/js/custom.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function() {

        $('#ChampLocalTbcContainer', function() {
            var urlToLoad = "dashboard_sso/summaryTBCLocal/FeaturedChampsMobile.php?";
            var divToHide = "#ChampLocalTbcLoader";
            var divToShow = "#ChampLocalTbcContainer";
            reload(urlToLoad, divToHide, divToShow);
        }
        );

        $('#champ-group button').click(function() {
            var textval = $(this).data("text");
            var divToHide = "#ChampLocalTbcLoader";
            var divToShow = "#ChampLocalTbcContainer";
            var setUrl = '';

            switch (textval) {
                case 'Interaction':
                    setUrl = "dashboard_sso/summaryTBCLocal/FeaturedChampsInteraction.php?";
                    break;
                case 'Mobile':
                    setUrl = "dashboard_sso/summaryTBCLocal/FeaturedChampsMobile.php?";
                    break;
                case 'Fixed':
                    setUrl = "dashboard_sso/summaryTBCLocal/FeaturedChampsFixed.php?";
                    break;
            }

            reload(setUrl, divToHide, divToShow);
        });

//        $('#ChampInteractionTBC').fadeOut('slow',
//                function () {
//                    var urlToLoad = "dashboard_sso/summaryTBCLocal/FeaturedChampsInteraction.php";
//                    var divToHide = "#ChampLoader1";
//                    var divToShow = "#ChampInteractionTBC";
//                    loadDiv(urlToLoad, divToHide, divToShow);
//                }
//        );
//
//        $('#ChampMobile').fadeOut('slow',
//                function () {
//                    var urlToLoad = "dashboard_sso/summaryTBCLocal/FeaturedChampsMobile.php";
//                    var divToHide = "#ChampLoader2";
//                    var divToShow = "#ChampMobile";
//                    loadDiv(urlToLoad, divToHide, divToShow);
//                }
//        );
//
//        $('#ChampFixed').fadeOut('slow',
//                function () {
//                    var urlToLoad = "dashboard_sso/summaryTBCLocal/FeaturedChampsFixed.php";
//                    var divToHide = "#ChampLoader3";
//                    var divToShow = "#ChampFixed";
//                    loadDiv(urlToLoad, divToHide, divToShow);
//                }
//        );

//        $('#InteractionChampionRanking', function() {
//            var urlToLoad = "dashboard_sso/summaryTBCLocal/ChampionsInteractionRank.php?";
//            var divToHide = "#InteractionChampLoader";
//            var divToShow = "#InteractionChampionRanking";
//            reload(urlToLoad, divToHide, divToShow);
//        }
//        );

        $('#MobileChampionRanking', function() {
            var urlToLoad = "dashboard_sso/summaryTBCLocal/ChampionsMobileRank.php?";
            var divToHide = "#MobileChampLoader";
            var divToShow = "#MobileChampionRanking";
            reload(urlToLoad, divToHide, divToShow);
        }
        );

        $('#FixedChampionRanking', function() {
            var urlToLoad = "dashboard_sso/summaryTBCLocal/ChampionsFixedRank.php?";
            var divToHide = "#FixedChampLoader";
            var divToShow = "#FixedChampionRanking";
            reload(urlToLoad, divToHide, divToShow);
        }
        );

        // interactionChamp  MobileChamp FixedChamp
//        $('#StoreChampionInteraction').click(function () {
//            //alert('clicked');
//            $('#MobileChamp').fadeOut('slow');
//            $('#FixedChamp').fadeOut('slow');
//            $('#interactionChamp').fadeIn('slow');
//        });
//        $('#StoreChampionMobile').click(function () {
//            // alert('clicked 1 ');
//            $('#interactionChamp').fadeOut('slow');
//            $('#FixedChamp').fadeOut('slow');
//            $('#MobileChamp').fadeIn('slow');
//        });
//        $('#StoreChampionFixed').click(function () {
//            // alert('clicked 2');
//            $('#MobileChamp').fadeOut('slow');
//            $('#interactionChamp').fadeOut('slow');
//            $('#FixedChamp').fadeIn('slow');
//        });

//        $('#StoreRankingInteraction').click(function () {
//            //alert('clicked');
//            $('#FixedDiv').fadeOut('slow');
//            $('#MobileDiv').fadeOut('slow');
//            $('#InteractionDiv').fadeIn('slow');
//        });
//        $('#StoreRankingMobile').click(function () {
//            // alert('clicked 1 ');
//            $('#InteractionDiv').fadeOut('slow');
//            $('#FixedDiv').fadeOut('slow');
//            $('#MobileDiv').fadeIn('slow');
//        });
//        $('#StoreRankingFixed').click(function () {
//            //alert('clicked 2');
//            $('#MobileDiv').fadeOut('slow');
//            $('#InteractionDiv').fadeOut('slow');
//            $('#FixedDiv').fadeIn('slow');
//        });

    });

    function reload(paramurl, loader, content) {
        var urlExtend = 'storeType=2';
        var urldata = paramurl + urlExtend;
        $(content).html('');
        $(loader).show();
        $.ajax({
            url: urldata,
            cache: false,
            success: function(html) {
                $(content).html('');
                $(content).append(html);
            },
            complete: function() {
                $(loader).hide();
            }
        });
    }
</script>