<?php
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/dashboard_sso/function/getChamp.php');

$storeType = $_GET['storeType'];
$EmpChampData = getChampTBC('Interaction', 'AllEmployees', 'no', NULL, NUlL, 'RemoveNull', $storeType);
$interactionChunk = $EmpChampData[0];

$rank = 1;
?>


<?php if (!empty($EmpChampData)) { ?>
    <div class="row">
        <?php foreach ($interactionChunk as $value) { ?>
            <div class="col-md-4 col-md-6">
                <div class="ribbon-wrapper">
                    <div class="ribbon-wrapper-gold">
                        <div class="ribbon-gold">
                            <i class="fa fa-trophy"></i> Rank <?php echo $rank ?>
                        </div>
                    </div>
                    <center>
                        <div class="ribbon-heading">
                            <h2 style="margin-top: 0;"><i class="fa fa-user"></i> <?php echo $value['Data']['EmpName']; ?> </h2>
                            <h6 style='color: #fec620; font-weight: bold;'><i class="fa fa-trophy"></i> <?php echo $value['Data']['Store']; ?> Champion </h6>

                            <div class="nps-badge-gold" id="featuredChampsBadge">
                                <h4><?php echo number_format($value['Data']['EmpInteraction'], npsDecimal()) ?></h4>
                                <small>Interaction</small>
                            </div>
                        </div>
                        <h4 class="survey">
                            <span style="color: black">Survey: <?php echo $value['Data']['EmpTotalSurvey']; ?></span> |
                            <span style="color: white">Impact: <?php echo number_format($value['EmpImpact'], 2); ?></span>
                        </h4>

                        <h5 class="text-center badges-head">
                            <span class="badge badge-green" style='background: #00e600;'>Advo: <?php echo number_format($value['Data']['EmpAdvocate'], 0); ?> </span>
                            <span class="badge badge-orange" style='background: #fec620;'>Pass: <?php echo number_format($value['Data']['EmpPassive'], 0); ?> </span>
                            <span class="badge badge-red" style='background: #ff0000;'>Detra: <?php echo number_format($value['Data']['EmpDetractor'], 0); ?> </span>
                        </h5>
                    </center>
                </div>
            </div>
            <?php
            $rank++;
        }
        ?>
    </div>
<?php } else { ?>
    No data to display
<?php } ?>

