<?php
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/dashboard_sso/function/getChamp.php');
$storeType = $_GET['storeType'];
$EmpChampData = getChampTLS('Mobile', NULL, NULL, NULL, NUlL, 'RemoveNull', $storeType);
$interactionChunk = array_chunk($EmpChampData, 3, false);
?>

<?php if (!is_null($EmpChampData)) { ?>
    <table id="MobileChampTable" border="0" width="100%">
        <thead id="tableHead" style="display: none;">
            <tr>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 0;

            foreach ($interactionChunk as $value) {
                ?>
                <tr> <?php if (!is_null($value[0])) { ?>
                        <td class="col-md-4">
                            <div class="ribbon-wrapper">
                                <center>
                                    <div class="ribbon-heading">
                                        <h2 style="margin-top: 0;"><i class="fa fa-user"></i> <?php echo $value[0]['Data']['EmpName']; ?> </h2>
                                        <h6 style='color: #fec620; font-weight: bold;'><i class="fa fa-trophy"></i> <?php echo $value[0]['Data']['Store']; ?> Champion </h6>

                                        <div class="nps-badge-orange" id="featuredChampsBadge">
                                            <h4><?php echo number_format($value[0]['Data']['EmpMobile'],  npsDecimal()) ?></h4>
                                            <small>MOBILITY</small>
                                        </div>
                                    </div>
                                    <h4 class="survey">
                                        <span style="color: black">Survey: <?php echo $value[0]['Data']['EmpTotalSurvey']; ?></span> |
                                        <span style="color: white">Impact: <?php echo number_format($value[0]['EmpImpact'], 2); ?></span>
                                    </h4>

                                    <h5 class="text-center badges-head">
                                        <span class="badge dashboard-badges" style='background: #00e600;'>Advo: <?php echo number_format($value[0]['Data']['EmpAdvocate'], 0); ?> </span>
                                        <span class="badge dashboard-badges" style='background: #fec620;'>Pass: <?php echo number_format($value[0]['Data']['EmpPassive'], 0); ?> </span>
                                        <span class="badge dashboard-badges" style='background: #ff0000;'>Detra: <?php echo number_format($value[0]['Data']['EmpDetractor'], 0); ?> </span>
                                    </h5>
                                </center>
                            </div>
                        </td><?php
                    } else {
                        echo "<td class=\"col-md-4\"> </td>";
                    }
                    ?>
                    <?php if (!is_null($value[1])) { ?>
                        <td class="col-md-4">
                            <div class="ribbon-wrapper">
                                <center>
                                    <div class="ribbon-heading">
                                        <h4 style="margin-top: 0;"><i class="fa fa-user"></i> <?php echo $value[1]['Data']['EmpName']; ?> </h4>
                                        <h6 style='color: #fec620; font-weight: bold;'><i class="fa fa-trophy"></i> <?php echo $value[1]['Data']['Store']; ?> Champion </h6>

                                        <div class="nps-badge-orange" id="featuredChampsBadge">
                                            <h4><?php echo number_format($value[1]['Data']['EmpMobile'],  npsDecimal()) ?></h4>
                                            <small>MOBILITY</small>
                                        </div>
                                    </div>
                                    <h4 class="survey">
                                        <span style="color: black">Survey: <?php echo $value[1]['Data']['EmpTotalSurvey']; ?></span> |
                                        <span style="color: white">Impact: <?php echo number_format($value[1]['EmpImpact'], 2); ?></span>
                                    </h4>

                                    <h5 class="text-center badges-head">
                                        <span class="badge dashboard-badges" style='background: #00e600;'>Advo: <?php echo number_format($value[1]['Data']['EmpAdvocate'], 0); ?> </span>
                                        <span class="badge dashboard-badges" style='background: #fec620;'>Pass: <?php echo number_format($value[1]['Data']['EmpPassive'], 0); ?> </span>
                                        <span class="badge dashboard-badges" style='background: #ff0000;'>Detra: <?php echo number_format($value[1]['Data']['EmpDetractor'], 0); ?> </span>
                                    </h5>
                                </center>
                            </div>
                        </td><?php
                    } else {
                        echo "<td class=\"col-md-4\"> </td>";
                    }
                    ?>
                    <?php if (!is_null($value[2])) { ?>
                        <td class="col-md-4">
                            <div class="ribbon-wrapper">
                                <center>
                                    <div class="ribbon-heading">
                                        <h4 style="margin-top: 0;"><i class="fa fa-user"></i> <?php echo $value[2]['Data']['EmpName']; ?> </h4>
                                        <h6 style='color: #fec620; font-weight: bold;'><i class="fa fa-trophy"></i> <?php echo $value[2]['Data']['Store']; ?> Champion </h6>

                                        <div class="nps-badge-orange" id="featuredChampsBadge">
                                            <h4><?php echo number_format($value[2]['Data']['EmpMobile'],  npsDecimal()) ?></h4>
                                            <small>MOBILITY</small>
                                        </div>
                                    </div>
                                    <h4 class="survey">
                                        <span style="color: black">Survey: <?php echo $value[2]['Data']['EmpTotalSurvey']; ?></span> |
                                        <span style="color: white">Impact: <?php echo number_format($value[2]['EmpImpact'], 2); ?></span>
                                    </h4>

                                    <h5 class="text-center badges-head">
                                        <span class="badge dashboard-badges" style='background: #00e600;'>Advo: <?php echo number_format($value[2]['Data']['EmpAdvocate'], 0); ?> </span>
                                        <span class="badge dashboard-badges" style='background: #fec620;'>Pass: <?php echo number_format($value[2]['Data']['EmpPassive'], 0); ?> </span>
                                        <span class="badge dashboard-badges" style='background: #ff0000;'>Detra: <?php echo number_format($value[2]['Data']['EmpDetractor'], 0); ?> </span>
                                    </h5>
                                </center>
                            </div>
                        </td><?php
                    } else {
                        echo "<td class=\"col-md-4\"> </td>";
                    }
                    ?>
                </tr>

                <?php
            }
            ?>
        </tbody></table>
<?php } else { ?>
    No data to display
<?php } ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#MobileChampTable').DataTable({
//            dom: 'lBfrtip',
//            buttons: [
//                'copyHtml5',
//                'excelHtml5',
//                'csvHtml5',
//                'pdfHtml5'
//            ],
            iDisplayLength: 2,
            bFilter: false,
            bInfo: false,
            bSort: false,
            bLengthChange: false
        });
    });
</script>