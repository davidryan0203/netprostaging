<?php
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');
include($_SERVER['DOCUMENT_ROOT'] . '/nps_sso/function/NPSFunction.php');
include($_SERVER['DOCUMENT_ROOT'] . '/mynps_sso/function/InteractionMessage.php');
$storeType = $_GET['storeType'];
$StoreInteraction = array();
$storeNpsData = getStoreNps(NULL, NULL, $empID, $to, $from, NULL, $storeType);
$npsScore = 0;

function replacer(& $item, $key) {
    if ($item === null) {
        $item = 0;
    }
}

foreach ($storeNpsData['Interaction'] as $storeData) {

    array_walk_recursive($storeData, 'replacer');

    $npsScore = number_format($storeData['InteractionNpsScore'], npsDecimal());
    if ($npsScore > 75) {
        $classWrapper = "ribbon-green";
    } elseif ($npsScore > 65) {
        $classWrapper = "ribbon-gold";
    } elseif ($npsScore > 55) {
        $classWrapper = "ribbon-orange";
    } else {
        $classWrapper = "ribbon-red";
    }
    $StoreInteraction[] = array(
        'RibbonWrapper' => $classWrapper,
        'StoreName' => $storeData['StoreName'],
        'StoreTier' => $storeData['InteractionTierLevel'],
        'Advocate' => $storeData['InteractionTotalAdvocate'],
        'Passive' => $storeData['InteractionTotalPassive'],
        'Detractor' => $storeData['InteractionTotalDetractors'],
        'TotalSurvey' => $storeData['InteractionTotalSurvey'],
        'NpsScore' => number_format($storeData['InteractionNpsScore'], npsDecimal())
    );
}

$interactionChunk = array_chunk($StoreInteraction, 3, false);
?>


<?php

function jsCaller($TotalySurveyParam, $AdvocateParam, $DetractorParam, $SpanIDParam) { ?>


<?php }
?>

<script type="text/javascript">


    function generateRankStore(TotalySurvey, Advocate, Detractor, SpanID) {
        var NewTotalSurvey2 = TotalySurvey;
        var newInterAdvoTotal2 = Advocate;
        var InteractionTotalDetractorTotal2 = Detractor;

        var counter2 = 0;
        var newDetPercentage2 = 0;
        var newAdvoPercentage2 = 0;
        var newIntercationNPS2 = 0;
        var roundedstoreNPS = 0;
        while (newIntercationNPS2 < 78) {
            counter2 += 1;
            newAdvoPercentage2 = (newInterAdvoTotal2 / NewTotalSurvey2) * 100;
            newDetPercentage2 = (InteractionTotalDetractorTotal2 / NewTotalSurvey2) * 100;
            newIntercationNPS2 = newAdvoPercentage2 - newDetPercentage2;
            roundedstoreNPS = Math.round(newIntercationNPS2);
            if (roundedstoreNPS <= 48) {
                $("#InteractionToGetLvl4" + SpanID).empty();
                $("#InteractionToGetLvl4" + SpanID).append(counter2);
                //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 4");
            } else if (roundedstoreNPS <= 58) {
                $("#InteractionToGetLvl3" + SpanID).empty();
                $("#InteractionToGetLvl3" + SpanID).append(counter2);
                //   console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 3");
            } else if (roundedstoreNPS <= 68) {
                $("#InteractionToGetLvl2" + SpanID).empty();
                $("#InteractionToGetLvl2" + SpanID).append(counter2);
                //   console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 2");
            } else if (roundedstoreNPS < 78) {
                $("#InteractionToGetLvl1" + SpanID).empty();
                $("#InteractionToGetLvl1" + SpanID).append(counter2);
                //  console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 1");
            }
            newInterAdvoTotal2 += 1;
            NewTotalSurvey2 += 1;
        }
    }

    function generateRankStore2(TotalySurvey, Advocate, Detractor, SpanID) {

        var NewTotalSurvey2 = TotalySurvey;
        var newInterAdvoTotal2 = Advocate;
        var InteractionTotalDetractorTotal2 = Detractor;

        var counter2 = 0;
        var newDetPercentage2 = 0;
        var newAdvoPercentage2 = 0;
        var newIntercationNPS2 = 0;
        var roundedstoreNPS = 0;
        while (newIntercationNPS2 < 38) {
            counter2 += 1;
            newAdvoPercentage2 = (newInterAdvoTotal2 / NewTotalSurvey2) * 100;
            newDetPercentage2 = (InteractionTotalDetractorTotal2 / NewTotalSurvey2) * 100;
            newIntercationNPS2 = newAdvoPercentage2 - newDetPercentage2;
            roundedstoreNPS = Math.round(newIntercationNPS2);
            if (roundedstoreNPS < 38) {
                $("#InteractionToGetLvl1" + SpanID).empty();
                $("#InteractionToGetLvl1" + SpanID).append(counter2);
                //   console.log(counter2 + " NPS: "+ newIntercationNPS2 +" for tier 1");
            }
            newInterAdvoTotal2 += 1;
            NewTotalSurvey2 += 1;
        }
    }
</script>

<?php if (!is_null($StoreInteraction[0])) { ?>

    <style>
        #tableHead{
            display: none;
            table-layout: fixed;
            width:100%;
        }


    </style>
    <table id="example4" border="0">
        <thead id="tableHead">
            <tr>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
    <?php
    $rank = 1;
    $colorID = 0;
    $spanID = 0;
    foreach ($interactionChunk as $value) {
        ?>
                <?php if ($storeType == 1) { ?>
                    <tr> <?php if (!is_null($value[0])) { ?>
                            <td class="col-md-3">
                                <div class="ribbon-wrapper">
                                    <div class="ribbon-wrapper-gold">
                                        <div class="<?php echo $value[0]['RibbonWrapper']; ?>">
                                            <i class="fa fa-trophy"></i> Rank <?php echo $rank ?>
                                        </div>
                                    </div>
                                    <center>
                                        <div class="ribbon-heading">
                                            <!--<h3 class="sub-header" style="margin-top: 0px;">Interaction</h3>-->
                                            <!--<img class="img-circle img-responsive ChampPic" src="images/user_icon.png" alt="Profile Picture">-->
                                            <div class="nps-badge-gold" id="featuredChampsBadge">
                                                <h4><?php echo $value[0]['NpsScore']; ?></h4>
                                                <small><?php echo $value[0]['StoreTier']; ?></small>
                                            </div>
                                            <h2 style="margin-top: 0;"><i class="fa fa-shopping-cart"></i> <?php echo $value[0]['StoreName']; ?> </h2>

                <?php
                if ($value[0]['NpsScore'] < 75) {

                    //jsCaller($value[0]['TotalSurvey'], $value[0]['Advocate'], $value[0]['Detractor'], $spanID);
                    ?>
                                                <script type="text/javascript">

                                                    $(document).ready(function() {

                                                        var TotalySurveyParam = <?php echo $value[0]['TotalSurvey']; ?>;
                                                        var AdvocateParam = <?php echo $value[0]['Advocate']; ?>;
                                                        var DetractorParam = <?php echo $value[0]['Detractor']; ?>;
                                                        var SpanIDParam = <?php echo $spanID; ?>;
                                                        //console.log('was here '+SpanIDParam);

                                                        generateRankStore(TotalySurveyParam, AdvocateParam, DetractorParam, SpanIDParam);
                                                    });
                                                </script>
                    <?php $idSharet1 = "InteractionToGetLvl1{$spanID}"; ?>
                    <?php $idSharet2 = "InteractionToGetLvl2{$spanID}"; ?>
                    <?php $idSharet3 = "InteractionToGetLvl3{$spanID}"; ?>
                    <?php $idSharet4 = "InteractionToGetLvl4{$spanID}"; ?>

                                                <table class="table table-bordered table-hover table-condensed"  style="margin-bottom: 10px;" cellspacing="0" >
                                                    <thead>
                                                        <tr>
                                                            <th>T1</th>
                                                            <th>T2</th>
                                                            <th>T3</th>
                                                            <th>T4</th>

                                                        </tr>
                                                    </thead>
                                                    <tbody style="text-align:center;">
                                                        <tr>
                                                            <td><div id="<?php echo $idSharet1; ?>"><i class="fa fa-check"></i></div></td>
                                                            <td><div id="<?php echo $idSharet2; ?>"><i class="fa fa-check"></i></div></td>
                                                            <td><div id="<?php echo $idSharet3; ?>"><i class="fa fa-check"></i></div></td>
                                                            <td><div id="<?php echo $idSharet4; ?>"><i class="fa fa-check"></i></div></td>
                                                        </tr>
                                                    </tbody>
                                                </table>


                <?php } elseif ($value[0]['NpsScore'] == 0) { ?>
                    <?php $getClassStore = getClassStore($value[0]['NpsScore']); ?>
                                                <div <?php echo $getClassStore['class2']; ?> id="Dashboard-Alert">
                                                    <h5><i class="fa fa-check-square"></i> Store needs at least 1 Advocate</h5>
                                                </div>
                                            <?php } else { ?>
                                                <?php $getClassStore = getClassStore($value[0]['NpsScore']); ?>
                                                <div <?php echo $getClassStore['class2']; ?> id="Dashboard-Alert">
                                                    <h5><i class="fa fa-check-square"></i> Goals Accomplished!</h5>
                                                </div>
                                                <?php
                                            } $spanID++;
                                            $rank++;
                                            ?>
                                        </div>

                                        <h4 class="survey">
                                            <i class="fa fa-exclamation-circle"></i> Survey: <?php echo $value[0]['TotalSurvey']; ?>
                                        </h4>

                                        <h5 class="text-center badges-head">
                                            <span class="badge dashboard-badges" style='background: #00e600;'>Advo: <?php echo number_format($value[0]['Advocate'], 0); ?> </span>
                                            <span class="badge dashboard-badges" style='background: #fec620;'>Pass: <?php echo number_format($value[0]['Passive'], 0); ?> </span>
                                            <span class="badge dashboard-badges" style='background: #ff0000;'>Detra: <?php echo number_format($value[0]['Detractor'], 0); ?> </span>
                                        </h5>
                                    </center>
                                </div>
                            </td><?php } else {
                            ?>
                            <td class="col-md-3">
                            </td>
            <?php } ?>
            <?php if (!is_null($value[1])) { ?>
                            <td class="col-md-3">
                                <div class="ribbon-wrapper">
                                    <div class="ribbon-wrapper-gold">
                                        <div class="<?php echo $value[1]['RibbonWrapper']; ?>">
                                            <i class="fa fa-trophy"></i> Rank <?php echo $rank ?>
                                        </div>
                                    </div>
                                    <center>
                                        <div class="ribbon-heading">
                                            <!--<h3 class="sub-header" style="margin-top: 0px;">Interaction</h3>-->
                                            <!--<img class="img-circle img-responsive ChampPic" src="images/user_icon.png" alt="Profile Picture">-->
                                            <div class="nps-badge-gold" id="featuredChampsBadge">
                                                <h4><?php echo $value[1]['NpsScore']; ?></h4>
                                                <small><?php echo $value[1]['StoreTier']; ?></small>
                                            </div>
                                            <h3 style="margin-top: 0;"><i class="fa fa-shopping-cart"></i> <?php echo $value[1]['StoreName']; ?> </h3>

                <?php
                if ($value[1]['NpsScore'] < 75) {
                    // jsCaller($value[1]['TotalSurvey'], $value[1]['Advocate'], $value[1]['Detractor'], $spanID);
                    ?>
                                                <script type="text/javascript">
                                                    $(document).ready(function() {

                                                        var TotalySurveyParam = <?php echo $value[1]['TotalSurvey']; ?>;
                                                        var AdvocateParam = <?php echo $value[1]['Advocate']; ?>;
                                                        var DetractorParam = <?php echo $value[1]['Detractor']; ?>;
                                                        var SpanIDParam = <?php echo $spanID; ?>;

                                                        generateRankStore(TotalySurveyParam, AdvocateParam, DetractorParam, SpanIDParam);
                                                    });
                                                </script>
                    <?php $idSharet1 = "InteractionToGetLvl1{$spanID}"; ?>
                    <?php $idSharet2 = "InteractionToGetLvl2{$spanID}"; ?>
                    <?php $idSharet3 = "InteractionToGetLvl3{$spanID}"; ?>
                    <?php $idSharet4 = "InteractionToGetLvl4{$spanID}"; ?>

                                                <table class="table table-bordered table-hover table-condensed"  style="margin-bottom: 10px;" cellspacing="0" >
                                                    <thead>
                                                        <tr>
                                                            <th>T1</th>
                                                            <th>T2</th>
                                                            <th>T3</th>
                                                            <th>T4</th>

                                                        </tr>
                                                    </thead>
                                                    <tbody style="text-align:center;">
                                                        <tr>
                                                            <td><div id="<?php echo $idSharet1; ?>"><i class="fa fa-check"></i></div></td>
                                                            <td><div id="<?php echo $idSharet2; ?>"><i class="fa fa-check"></i></div></td>
                                                            <td><div id="<?php echo $idSharet3; ?>"><i class="fa fa-check"></i></div></td>
                                                            <td><div id="<?php echo $idSharet4; ?>"><i class="fa fa-check"></i></div></td>
                                                        </tr>
                                                    </tbody>
                                                </table>


                <?php } elseif ($value[1]['NpsScore'] == 0) { ?>
                    <?php $getClassStore = getClassStore($value[1]['NpsScore']); ?>
                                                <div <?php echo $getClassStore['class2']; ?> id="Dashboard-Alert">
                                                    <h5><i class="fa fa-check-square"></i> Store needs at least 1 Advocate</h5>
                                                </div>
                                            <?php } else { ?>
                                                <?php $getClassStore = getClassStore($value[1]['NpsScore']); ?>
                                                <div <?php echo $getClassStore['class2']; ?> id="Dashboard-Alert">
                                                    <h5><i class="fa fa-check-square"></i> Goals Accomplished!</h5>
                                                </div>
                                                <?php
                                            } $spanID++;
                                            $rank++;
                                            ?>
                                        </div>

                                        <h4 class="survey">
                                            <i class="fa fa-exclamation-circle"></i> Survey: <?php echo $value[1]['TotalSurvey']; ?>
                                        </h4>

                                        <h5 class="text-center badges-head">
                                            <span class="badge dashboard-badges" style='background: #00e600;'>Advo: <?php echo number_format($value[1]['Advocate'], 0); ?> </span>
                                            <span class="badge dashboard-badges" style='background: #fec620;'>Pass: <?php echo number_format($value[1]['Passive'], 0); ?> </span>
                                            <span class="badge dashboard-badges" style='background: #ff0000;'>Detra: <?php echo number_format($value[1]['Detractor'], 0); ?> </span>
                                        </h5>
                                    </center>
                                </div>
                            </td><?php } else {
                            ?>
                            <td class="col-md-3">
                            </td>
            <?php } ?>
            <?php if (!is_null($value[2])) { ?>
                            <td class="col-md-3">
                                <div class="ribbon-wrapper">
                                    <div class="ribbon-wrapper-gold">
                                        <div class="<?php echo $value[2]['RibbonWrapper']; ?>">
                                            <i class="fa fa-trophy"></i> Rank <?php echo $rank ?>
                                        </div>
                                    </div>
                                    <center>
                                        <div class="ribbon-heading">
                                            <!--<h3 class="sub-header" style="margin-top: 0px;">Interaction</h3>-->
                                            <!--<img class="img-circle img-responsive ChampPic" src="images/user_icon.png" alt="Profile Picture">-->
                                            <div class="nps-badge-gold" id="featuredChampsBadge">
                                                <h4><?php echo $value[2]['NpsScore']; ?></h4>
                                                <small><?php echo $value[2]['StoreTier']; ?></small>
                                            </div>
                                            <h3 style="margin-top: 0;"><i class="fa fa-shopping-cart"></i> <?php echo $value[2]['StoreName']; ?> </h3>

                <?php
                if ($value[2]['NpsScore'] < 75) {
                    ?>
                                                <script type="text/javascript">
                                                    $(document).ready(function() {

                                                        var TotalySurveyParam = <?php echo $value[2]['TotalSurvey']; ?>;
                                                        var AdvocateParam = <?php echo $value[2]['Advocate']; ?>;
                                                        var DetractorParam = <?php echo $value[2]['Detractor']; ?>;
                                                        var SpanIDParam = <?php echo $spanID; ?>;

                                                        generateRankStore(TotalySurveyParam, AdvocateParam, DetractorParam, SpanIDParam);
                                                    });
                                                </script>
                    <?php $idSharet1 = "InteractionToGetLvl1{$spanID}"; ?>
                    <?php $idSharet2 = "InteractionToGetLvl2{$spanID}"; ?>
                    <?php $idSharet3 = "InteractionToGetLvl3{$spanID}"; ?>
                    <?php $idSharet4 = "InteractionToGetLvl4{$spanID}"; ?>

                                                <table class="table table-bordered table-hover table-condensed"  style="margin-bottom: 10px;" cellspacing="0" >
                                                    <thead>
                                                        <tr>
                                                            <th>T1</th>
                                                            <th>T2</th>
                                                            <th>T3</th>
                                                            <th>T4</th>

                                                        </tr>
                                                    </thead>
                                                    <tbody style="text-align:center;">
                                                        <tr>
                                                            <td><div id="<?php echo $idSharet1; ?>"><i class="fa fa-check"></i></div></td>
                                                            <td><div id="<?php echo $idSharet2; ?>"><i class="fa fa-check"></i></div></td>
                                                            <td><div id="<?php echo $idSharet3; ?>"><i class="fa fa-check"></i></div></td>
                                                            <td><div id="<?php echo $idSharet4; ?>"><i class="fa fa-check"></i></div></td>
                                                        </tr>
                                                    </tbody>
                                                </table>


                <?php } elseif ($value[2]['NpsScore'] == 0) { ?>
                    <?php $getClassStore = getClassStore($value[2]['NpsScore']); ?>
                                                <div <?php echo $getClassStore['class2']; ?> id="Dashboard-Alert">
                                                    <h5><i class="fa fa-check-square"></i> Store needs at least 1 Advocate</h5>
                                                </div>
                                            <?php } else { ?>
                                                <?php $getClassStore = getClassStore($value[2]['NpsScore']); ?>
                                                <div <?php echo $getClassStore['class2']; ?> id="Dashboard-Alert">
                                                    <h5><i class="fa fa-check-square"></i> Goals Accomplished!</h5>
                                                </div>
                                                <?php
                                            } $spanID++;
                                            $rank++;
                                            ?>
                                        </div>

                                        <h4 class="survey">
                                            <i class="fa fa-exclamation-circle"></i> Survey: <?php echo $value[2]['TotalSurvey']; ?>
                                        </h4>

                                        <h5 class="text-center badges-head">
                                            <span class="badge dashboard-badges" style='background: #00e600;'>Advo: <?php echo number_format($value[2]['Advocate'], 0); ?> </span>
                                            <span class="badge dashboard-badges" style='background: #fec620;'>Pass: <?php echo number_format($value[2]['Passive'], 0); ?> </span>
                                            <span class="badge dashboard-badges" style='background: #ff0000;'>Detra: <?php echo number_format($value[2]['Detractor'], 0); ?> </span>
                                        </h5>
                                    </center>
                                </div>
                            </td>
            <?php } else {
                ?>
                            <td class="col-md-3">
                            </td>
            <?php } ?>
                    </tr>
                    <?php } ?>
                    <?php if ($storeType == 2) { ?>
                    <tr>
                        <?php if (!is_null($value[0])) { ?>
                            <td class="col-md-3">
                                <div class="ribbon-wrapper">
                                    <div class="ribbon-wrapper-gold">
                                        <div class="<?php echo $value[0]['RibbonWrapper']; ?>">
                                            <i class="fa fa-trophy"></i> Rank <?php echo $rank ?>
                                        </div>
                                    </div>
                                    <center>
                                        <div class="ribbon-heading">
                                            <!--<h3 class="sub-header" style="margin-top: 0px;">Interaction</h3>-->
                                            <!--<img class="img-circle img-responsive ChampPic" src="images/user_icon.png" alt="Profile Picture">-->
                                            <div class="nps-badge-gold" id="featuredChampsBadge">
                                                <h4><?php echo $value[0]['NpsScore']; ?></h4>
                                                <small><?php echo $value[0]['StoreTier']; ?></small>
                                            </div>
                                            <h3 style="margin-top: 0;"><i class="fa fa-shopping-cart"></i> <?php echo $value[0]['StoreName']; ?> </h3>

                <?php if ($value[0]['NpsScore'] < 67) { ?>
                                                <script type="text/javascript">
                                                    $(document).ready(function() {

                                                        var TotalySurveyParam = <?php echo $value[0]['TotalSurvey']; ?>;
                                                        var AdvocateParam = <?php echo $value[0]['Advocate']; ?>;
                                                        var DetractorParam = <?php echo $value[0]['Detractor']; ?>;
                                                        var SpanIDParam = <?php echo $spanID; ?>;

                                                        generateRankStore2(TotalySurveyParam, AdvocateParam, DetractorParam, SpanIDParam);
                                                    });
                                                </script>
                    <?php $idSharet1 = "InteractionToGetLvl1{$spanID}"; ?>
                    <?php $idSharet2 = "InteractionToGetLvl2{$spanID}"; ?>
                    <?php $idSharet3 = "InteractionToGetLvl3{$spanID}"; ?>
                    <?php $idSharet4 = "InteractionToGetLvl4{$spanID}"; ?>
                    <?php $idSharet5 = "InteractionToGetLvl5{$spanID}"; ?>
                                                <table class="table table-bordered table-hover table-condensed"  style="margin-bottom: 10px;" cellspacing="0" >
                                                    <thead>
                                                        <tr>
                                                            <th>T1</th>
                                                            <th>T2</th>
                                                            <th>T3</th>
                                                            <th>T4</th>
                                                            <th>T5</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody style="text-align:center;">
                                                        <tr>
                                                            <td><div id="<?php echo $idSharet1; ?>"><i class="fa fa-check"></i></div></td>
                                                            <td><div id="<?php echo $idSharet2; ?>"><i class="fa fa-check"></i></div></td>
                                                            <td><div id="<?php echo $idSharet3; ?>"><i class="fa fa-check"></i></div></td>
                                                            <td><div id="<?php echo $idSharet4; ?>"><i class="fa fa-check"></i></div></td>
                                                            <td><div id="<?php echo $idSharet5; ?>"><i class="fa fa-check"></i></div></td>
                                                        </tr>
                                                    </tbody>
                                                </table>


                <?php } elseif ($value[0]['NpsScore'] == 0) { ?>
                    <?php $getClassStore = getClassStore($value[0]['NpsScore']); ?>
                                                <div <?php echo $getClassStore['class2']; ?> id="Dashboard-Alert">
                                                    <h5><i class="fa fa-check-square"></i> Store needs at least 1 Advocate</h5>
                                                </div>
                                            <?php } else { ?>
                                                <?php $getClassStore = getClassStore($value[0]['NpsScore']); ?>
                                                <div <?php echo $getClassStore['class2']; ?> id="Dashboard-Alert">
                                                    <h5><i class="fa fa-check-square"></i> Goals Accomplished!</h5>
                                                </div>
                                                <?php
                                            } $spanID++;
                                            $rank++;
                                            ?>
                                        </div>

                                        <h4 class="survey">
                                            <i class="fa fa-exclamation-circle"></i> Survey: <?php echo $value[0]['TotalSurvey']; ?>
                                        </h4>

                                        <h5 class="text-center badges-head">
                                            <span class="badge dashboard-badges" style='background: #00e600;'>Advo: <?php echo number_format($value[0]['Advocate'], 0); ?> </span>
                                            <span class="badge dashboard-badges" style='background: #fec620;'>Pass: <?php echo number_format($value[0]['Passive'], 0); ?> </span>
                                            <span class="badge dashboard-badges" style='background: #ff0000;'>Detra: <?php echo number_format($value[0]['Detractor'], 0); ?> </span>
                                        </h5>
                                    </center>
                                </div>
                            </td>
            <?php } else {
                ?>
                            <td class="col-md-3">
                            </td>
            <?php } ?>
                        <?php if (!is_null($value[1])) { ?>
                            <td class="col-md-3">
                                <div class="ribbon-wrapper">
                                    <div class="ribbon-wrapper-gold">
                                        <div class="<?php echo $value[1]['RibbonWrapper']; ?>">
                                            <i class="fa fa-trophy"></i> Rank <?php echo $rank ?>
                                        </div>
                                    </div>
                                    <center>
                                        <div class="ribbon-heading">
                                            <!--<h3 class="sub-header" style="margin-top: 0px;">Interaction</h3>-->
                                            <!--<img class="img-circle img-responsive ChampPic" src="images/user_icon.png" alt="Profile Picture">-->
                                            <div class="nps-badge-gold" id="featuredChampsBadge">
                                                <h4><?php echo $value[1]['NpsScore']; ?></h4>
                                                <small><?php echo $value[1]['StoreTier']; ?></small>
                                            </div>
                                            <h3 style="margin-top: 0;"><i class="fa fa-shopping-cart"></i> <?php echo $value[1]['StoreName']; ?> </h3>

                <?php if ($value[1]['NpsScore'] < 67) { ?>
                                                <script type="text/javascript">
                                                    $(document).ready(function() {

                                                        var TotalySurveyParam = <?php echo $value[1]['TotalSurvey']; ?>;
                                                        var AdvocateParam = <?php echo $value[1]['Advocate']; ?>;
                                                        var DetractorParam = <?php echo $value[1]['Detractor']; ?>;
                                                        var SpanIDParam = <?php echo $spanID; ?>;

                                                        generateRankStore2(TotalySurveyParam, AdvocateParam, DetractorParam, SpanIDParam);
                                                    });
                                                </script>
                    <?php $idSharet1 = "InteractionToGetLvl1{$spanID}"; ?>
                    <?php $idSharet2 = "InteractionToGetLvl2{$spanID}"; ?>
                    <?php $idSharet3 = "InteractionToGetLvl3{$spanID}"; ?>
                    <?php $idSharet4 = "InteractionToGetLvl4{$spanID}"; ?>
                    <?php $idSharet5 = "InteractionToGetLvl5{$spanID}"; ?>
                                                <table class="table table-bordered table-hover table-condensed"  style="margin-bottom: 10px;" cellspacing="0" >
                                                    <thead>
                                                        <tr>
                                                            <th>T1</th>
                                                            <th>T2</th>
                                                            <th>T3</th>
                                                            <th>T4</th>
                                                            <th>T5</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody style="text-align:center;">
                                                        <tr>
                                                            <td><div id="<?php echo $idSharet1; ?>"><i class="fa fa-check"></i></div></td>
                                                            <td><div id="<?php echo $idSharet2; ?>"><i class="fa fa-check"></i></div></td>
                                                            <td><div id="<?php echo $idSharet3; ?>"><i class="fa fa-check"></i></div></td>
                                                            <td><div id="<?php echo $idSharet4; ?>"><i class="fa fa-check"></i></div></td>
                                                            <td><div id="<?php echo $idSharet5; ?>"><i class="fa fa-check"></i></div></td>
                                                        </tr>
                                                    </tbody>
                                                </table>


                <?php } elseif ($value[1]['NpsScore'] == 0) { ?>
                    <?php $getClassStore = getClassStore($value[1]['NpsScore']); ?>
                                                <div <?php echo $getClassStore['class2']; ?> id="Dashboard-Alert">
                                                    <h5><i class="fa fa-check-square"></i> Store needs at least 1 Advocate</h5>
                                                </div>
                                            <?php } else { ?>
                                                <?php $getClassStore = getClassStore($value[1]['NpsScore']); ?>
                                                <div <?php echo $getClassStore['class2']; ?> id="Dashboard-Alert">
                                                    <h5><i class="fa fa-check-square"></i> Goals Accomplished!</h5>
                                                </div>
                                                <?php
                                            } $spanID++;
                                            $rank++;
                                            ?>
                                        </div>

                                        <h4 class="survey">
                                            <i class="fa fa-exclamation-circle"></i> Survey: <?php echo $value[1]['TotalSurvey']; ?>
                                        </h4>

                                        <h5 class="text-center badges-head">
                                            <span class="badge dashboard-badges" style='background: #00e600;'>Advo: <?php echo number_format($value[1]['Advocate'], 0); ?> </span>
                                            <span class="badge dashboard-badges" style='background: #fec620;'>Pass: <?php echo number_format($value[1]['Passive'], 0); ?> </span>
                                            <span class="badge dashboard-badges" style='background: #ff0000;'>Detra: <?php echo number_format($value[1]['Detractor'], 0); ?> </span>
                                        </h5>
                                    </center>
                                </div>
                            </td>
            <?php } else {
                ?>
                            <td class="col-md-3">
                            </td>
            <?php } ?>
                        <?php if (!is_null($value[2])) { ?>
                            <td class="col-md-3">
                                <div class="ribbon-wrapper">
                                    <div class="ribbon-wrapper-gold">
                                        <div class="<?php echo $value[2]['RibbonWrapper']; ?>">
                                            <i class="fa fa-trophy"></i> Rank <?php echo $rank ?>
                                        </div>
                                    </div>
                                    <center>
                                        <div class="ribbon-heading">
                                            <!--<h3 class="sub-header" style="margin-top: 0px;">Interaction</h3>-->
                                            <!--<img class="img-circle img-responsive ChampPic" src="images/user_icon.png" alt="Profile Picture">-->
                                            <div class="nps-badge-gold" id="featuredChampsBadge">
                                                <h4><?php echo $value[2]['NpsScore']; ?></h4>
                                                <small><?php echo $value[2]['StoreTier']; ?></small>
                                            </div>
                                            <h3 style="margin-top: 0;"><i class="fa fa-shopping-cart"></i> <?php echo $value[2]['StoreName']; ?> </h3>

                <?php if ($value[2]['NpsScore'] < 67) { ?>
                                                <script type="text/javascript">
                                                    $(document).ready(function() {

                                                        var TotalySurveyParam = <?php echo $value[2]['TotalSurvey']; ?>;
                                                        var AdvocateParam = <?php echo $value[2]['Advocate']; ?>;
                                                        var DetractorParam = <?php echo $value[2]['Detractor']; ?>;
                                                        var SpanIDParam = <?php echo $spanID; ?>;

                                                        generateRankStore2(TotalySurveyParam, AdvocateParam, DetractorParam, SpanIDParam);
                                                    });
                                                </script>
                    <?php $idSharet1 = "InteractionToGetLvl1{$spanID}"; ?>
                    <?php $idSharet2 = "InteractionToGetLvl2{$spanID}"; ?>
                    <?php $idSharet3 = "InteractionToGetLvl3{$spanID}"; ?>
                    <?php $idSharet4 = "InteractionToGetLvl4{$spanID}"; ?>
                    <?php $idSharet5 = "InteractionToGetLvl5{$spanID}"; ?>
                                                <table class="table table-bordered table-hover table-condensed"  style="margin-bottom: 10px;" cellspacing="0" >
                                                    <thead>
                                                        <tr>
                                                            <th>T1</th>
                                                            <th>T2</th>
                                                            <th>T3</th>
                                                            <th>T4</th>
                                                            <th>T5</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody style="text-align:center;">
                                                        <tr>
                                                            <td><div id="<?php echo $idSharet1; ?>"><i class="fa fa-check"></i></div></td>
                                                            <td><div id="<?php echo $idSharet2; ?>"><i class="fa fa-check"></i></div></td>
                                                            <td><div id="<?php echo $idSharet3; ?>"><i class="fa fa-check"></i></div></td>
                                                            <td><div id="<?php echo $idSharet4; ?>"><i class="fa fa-check"></i></div></td>
                                                            <td><div id="<?php echo $idSharet5; ?>"><i class="fa fa-check"></i></div></td>
                                                        </tr>
                                                    </tbody>
                                                </table>


                <?php } elseif ($value[2]['NpsScore'] == 0) { ?>
                    <?php $getClassStore = getClassStore($value[2]['NpsScore']); ?>
                                                <div <?php echo $getClassStore['class2']; ?> id="Dashboard-Alert">
                                                    <h5><i class="fa fa-check-square"></i> Store needs at least 1 Advocate</h5>
                                                </div>
                                            <?php } else { ?>
                                                <?php $getClassStore = getClassStore($value[2]['NpsScore']); ?>
                                                <div <?php echo $getClassStore['class2']; ?> id="Dashboard-Alert">
                                                    <h5><i class="fa fa-check-square"></i> Goals Accomplished!</h5>
                                                </div>
                                                <?php
                                            } $spanID++;
                                            $rank++;
                                            ?>
                                        </div>

                                        <h4 class="survey">
                                            <i class="fa fa-exclamation-circle"></i> Survey: <?php echo $value[2]['TotalSurvey']; ?>
                                        </h4>

                                        <h5 class="text-center badges-head">
                                            <span class="badge dashboard-badges" style='background: #00e600;'>Advo: <?php echo number_format($value[2]['Advocate'], 0); ?> </span>
                                            <span class="badge dashboard-badges" style='background: #fec620;'>Pass: <?php echo number_format($value[2]['Passive'], 0); ?> </span>
                                            <span class="badge dashboard-badges" style='background: #ff0000;'>Detra: <?php echo number_format($value[2]['Detractor'], 0); ?> </span>
                                        </h5>
                                    </center>
                                </div>
                            </td>
            <?php } else {
                ?>
                            <td class="col-md-3">
                            </td>
            <?php } ?>
                    </tr>
                    <?php } ?>
                <?php } ?> </tbody></table>
<?php } else { ?>
    No data to display
            <?php } ?>


<script type="text/javascript">
    $(document).ready(function() {

        var table = $('#example4').DataTable({
            dom: 'lBfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            "iDisplayLength": 2, bFilter: false, bInfo: false,
            "bSort": false,
            "bLengthChange": false
                    // "aLengthMenu": [[15, 50, 100, -1], [15, 50, 100, "All"]],

        });
    });

    // function testalert(){


    // }
</script>