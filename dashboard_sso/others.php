<?php

if($_SESSION['LoginID'] != '6445'){

  echo "<script>window.location.href='public_html.php';</script>";
   
   exit;
}



include('nps_sso/function/NPSFunction.php');
require($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');

$SelectStoreTypes = SelectStoreTypes();

$storeTypes = checkStoreTypesLocal();

$date = new DateTime();
$date->setTimezone(new DateTimeZone('Australia/Melbourne')); // +04
?>
<a href = 'logout.php'><i class = 'fa fa-sign-out fa-fw'></i> Logout</a>
<!--Local dashboard container-->
<section class='dashboard'>
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-header">
                <i class="fa fa-file-word-o fa-fw"></i>Fixed Target's
            </h4>
        </div>
    </div>
    <div>

        <section class='reports'> 
            <div class="main-content">
                <h4 class="sub-header"><i class="fa fa-building fa-fw"></i> Store Ranking Summary</h4>
                <div class="loader" id="StoreRankingsLoader"></div>

                <div id="storeRanking">
                    <div id="page-contents"></div>
                </div>
            </div>
        </section>      


    </div>
</section>


<script type="text/javascript">
    $(document).ready(function () {
      
   
        function loadContents(paramurl, loader, content) {
            $(content).html('');
            $(loader).show();
            $.ajax({
                url: paramurl,
                cache: false,
                success: function(html) {
                    $(content).html('');
                    $(content).append(html);
                },
                complete: function() {
                    $(loader).hide();
                }
            });
        }

      loadContents('dashboard_sso/summaryLocal/storeTargetsFixed.php','#StoreRankingsLoader','#page-contents');
       

    });
</script>








