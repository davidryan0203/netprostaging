<?php
session_start();
$storeType = $_GET['storeType'];
$role = $_SESSION['UserRoleID'];

$param = 'All';
if ($role != 1) {
    $param = 'Own';
}
$withFilter = array('2', '3', '5');
include($_SERVER['DOCUMENT_ROOT'] . '/dashboard_sso/function/rawFunction.php');
$storeList = getStoreSpecific($param);
$countStore = count($storeList);
?>

<!--local tls dashboard reports-->
<section class='reports'>
    <input type="hidden" id="storeType" name="storeType" value="<?php echo $storeType; ?>" />

    <!--    <div class="main-content">
            <button class="pull-right btn btn-xs btn-info" id="changer">Interaction</button>

            <h4 class="sub-header"><i class="fa fa-user fa-fw"></i> Overall Performance Ranking</h4>
            <div class="loader" id="ChampLoader1"></div>

            <div id="featuredChampsInteraction">
                <div  id="ChampInteraction"></div>
            </div>
        </div>-->
    <?php if (in_array($role, $withFilter)) { ?>
        <div class = "row no-bg" id = "StoreTargetContent">
            <div class = "filters">
                <?php if ($countStore > 1) { ?>
                    <select name = "StoreID" id = "storeID" class = "form-control">
                        <option value="">All Stores</option>
                        <?php
                        foreach ($storeList as $value) {
                            echo '<option value="' . $value['f_StoreID'] . '">' . $value['f_StoreListName'] . '</option>';
                        }
                        ?>
                    </select>
                <?php } else { ?>
                    <input type="hidden" name="StoreID" id="storeID" value=""/>
                <?php } ?>
                <div class="btn-group group1 " id="targetrolling-group" role="group" aria-label="...">
                    <button type="button" id="StoreChamps3MMA" data-text="3MMA"  data-target="ThreeMonthsRolling" class="btn btn-xs btn-danger active">3 MMA</button>
                    <button type="button" id="StoreChampsMTD" data-text="MTD" data-target="CurrentMonth" class="btn btn-xs btn-warning ">MTD</button>
                    <button type="button" id="StoreChamps4Week" data-text="4Week"  data-target="FourWeekRolling" class="btn btn-xs btn-primary">4 Weeks</button>
                    <!--<button type="button" id="StoreChamps3Mnt" data-text="3Months"  data-target="NinetyDayRolling" class="btn btn-xs btn-info">3 Months</button>-->
                    <button type="button" id="StoreChampsHTD" data-text="HTD"  data-target="HalfYearRolling" class="btn btn-xs btn-success">HTD</button>

                </div>
                <button type="submit" class="btn btn-warning btn-circle" id="refresh" data-tooltip="tooltip" data-placement="bottom" title="Reload">
                    <i class="fa fa-refresh"></i>
                </button>
            </div>
        </div>
    <?php } else { ?> <input type="hidden" name="StoreID" id="storeID" value=""/> <?php } ?>

    <div class="main-content featuredChamps no-bg">

        <div class="btn-group group1 pull-right" role="group" id="storechamp-group" aria-label="...">
            <button type="button" id="StoreChampionEpisode" data-text="Episode" class="btn btn-xs btn-primary ">Episode</button>
            <button type="button" id="StoreChampionMobile" data-text="Mobile" class="btn btn-xs btn-warning">Mobile</button>
            <button type="button" id="StoreChampionFixed" data-text="Fixed" class="btn btn-xs btn-info active">Fixed</button>
        </div>

        <h4 class="sub-header"><i class="fa fa-user fa-fw"></i> Overall Performance <overall>Fixed</overall> Ranking</h4>

        <div class="loader" id="StoreChampTlsloader"></div>

        <div id="featuredChampsInteraction">
            <div  id="StoreChampTlsContainer"> </div>
        </div>

    </div>
    <!--
        <div class="main-content">
            <h4 class="sub-header"><i class="fa fa-users fa-fw"></i> Episode Ranking</h4>
            <div class="loader" id="InteractionChampLoader"></div>
            <div id="EpisodeChampionRanking"> </div>
        </div>-->

    <div class="main-content">
        <h4 class="sub-header"><i class="fa fa-users fa-fw"></i> Mobility (On the go) Ranking</h4>
        <div class="loader" id="MobileChampLoader"></div>
        <div id="MobileChampionRanking"> </div>
    </div>

    <div class="main-content">
        <h4 class="sub-header"><i class="fa fa-users fa-fw"></i> Fixed (Home) Ranking</h4>
        <div class="loader" id="FixedChampLoader"></div>
        <div id="FixedChampionRanking"> </div>
    </div>

    <div class="main-content">
        <h4 class="sub-header"><i class="fa fa-users fa-fw"></i> Combined Mobility and Fixed </h4>
        <div class="loader" id="CombinedChampLoader"></div>
        <div id="CombinedChampionRanking"> </div>
    </div>

    <?php if ($role == 4) { ?>

        <div class="main-content " >
            <div class="panel-heading" role="tab" id="headingEight">
                <div class="btn-group group2 pull-right" id="weekmerge-group" role="group" aria-label="...">
                    <!--<button type="button" id="FourWeekMergeInteraction" data-text="Interaction" data-target="fourweekmergeint" class="btn btn-xs btn-primary active">Interaction</button>-->
                    <button type="button" id="FourWeekMergeMobile" data-text="Mobile"  data-target="fourweekmergemob" class="btn btn-xs btn-warning">Mobile</button>
                    <button type="button" id="FourWeekMergeFixed" data-text="Fixed"  data-target="fourweekmergefix" class="btn btn-xs btn-info active">Fixed</button>
                    <!--<button type="button" id="SFourWeekMergeEpisode" data-text="Episode"  data-target="fourweekmergeepi" class="btn btn-xs btn-success">Episode</button>-->
                </div>
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="true" aria-controls="collapseEight">
                    <h4 class="sub-header"><i class="fa fa-newspaper-o fa-fw"></i>Employee with Multiple Stores Merged 4 Weeks <weekmerge>Fixed</weekmerge> NPS</h4>
                </a>
            </div>
            <div id="collapseEight" class="panel-collapse collapse in" role="tabpanel">
                <div class="panel-body">
                    <div class="loader" id="FourWeekMergedEmployeeNpsloader">
                        <?php include($_SERVER['DOCUMENT_ROOT'] . '/loading.php'); ?>
                    </div>
                    <div class="weekmergecontainer" id="fourweekmergeint">
                        <div class="table table-responsive" id="FourWeekMergedEmployeeNps"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="main-content " >
            <div class="panel-heading" role="tab" id="headingEight">
                <div class="btn-group group2 pull-right" id="mtdweekmerge-group" role="group" aria-label="...">
                    <!--<button type="button" id="MTDMergeInteraction" data-text="Interaction" data-target="mtdmergeint" class="btn btn-xs btn-primary ">Interaction</button>-->
                    <button type="button" id="MTDMergeMobile" data-text="Mobile"  data-target="mtdmergemob" class="btn btn-xs btn-warning">Mobile</button>
                    <button type="button" id="MTDMergeFixed" data-text="Fixed"  data-target="mtdmergefix" class="btn btn-xs btn-info active">Fixed</button>
                    <!--<button type="button" id="SMTDMergeEpisode" data-text="Episode"  data-target="mtdmergeepi" class="btn btn-xs btn-success">Episode</button>-->
                </div>
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="true" aria-controls="collapseEight">
                    <h4 class="sub-header"><i class="fa fa-newspaper-o fa-fw"></i>Employee with Multiple Stores Merged MTD <mtdmerge>Fixed</mtdmerge> NPS</h4>
                </a>
            </div>
            <div id="collapseEight" class="panel-collapse collapse in" role="tabpanel">
                <div class="panel-body">
                    <div class="loader" id="MTDMergedEmployeeNpsloader">
                        <?php include($_SERVER['DOCUMENT_ROOT'] . '/loading.php'); ?>
                    </div>
                    <div class="mtdmergecontainer" id="mtdmergeint">
                        <div class="table table-responsive" id="MTDMergedEmployeeNps"></div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

</section>

<script src="../includes/js/custom.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function() {

        function getParams() {
            var storeID = $('#storeID').val();
            var rangeType = "";
            $('#targetrolling-group .active').each(function() {
                rangeType = $(this).data('text');
            });
            var param = "storeID=" + storeID + "&rangeType=" + rangeType;
            return param;
        }

        function reload(paramurl, loader, content) {
            var params = getParams();
            var urlExtend = 'storeType=1&';
            var urldata = paramurl + urlExtend + params;
            console.log(params);
            $(content).html('');
            $(loader).show();
            console.log(urldata);
            $.ajax({
                url: urldata,
                cache: false,
                success: function(html) {
                    $(content).html('');
                    $(content).append(html);
                },
                complete: function() {
                    $(loader).hide();
                }
            });
        }

        $('#StoreChampTlsContainer', function() {
            var urlToLoad = "dashboard_sso/summaryLocal/FeaturedChampsFixed.php?";
            var divToHide = "#StoreChampTlsloader";
            var divToShow = "#StoreChampTlsContainer";
            reload(urlToLoad, divToHide, divToShow);
        });

        $('#storechamp-group button').click(function() {
            var textval = $(this).data("text");
            var divToHide = "#StoreChampTlsloader";
            var divToShow = "#StoreChampTlsContainer";
            var setUrl = '';

            switch (textval) {
                case 'Episode':
                    setUrl = "dashboard_sso/summaryLocal/FeaturedChampsEpisode.php?";
                    break;
                case 'Mobile':
                    setUrl = "dashboard_sso/summaryLocal/FeaturedChampsMobile.php?";
                    break;
                case 'Fixed':
                    setUrl = "dashboard_sso/summaryLocal/FeaturedChampsFixed.php?";
                    break;
            }

            $("#storechamp-group button").removeClass("active");
            $(this).addClass("active");
            $("overall").html(textval);

            reload(setUrl, divToHide, divToShow);
        });

        $('#EpisodeChampionRanking', function() {
            var urlToLoad = "dashboard_sso/summaryLocal/ChampionsEpisodeRank.php?";
            var divToHide = "#InteractionChampLoader";
            var divToShow = "#EpisodeChampionRanking";
            reload(urlToLoad, divToHide, divToShow);
        });

        $('#MobileChampionRanking', function() {
            var urlToLoad = "dashboard_sso/summaryLocal/ChampionsMobileRank.php?";
            var divToHide = "#MobileChampLoader";
            var divToShow = "#MobileChampionRanking";
            reload(urlToLoad, divToHide, divToShow);
        });

        $('#FixedChampionRanking', function() {
            var urlToLoad = "dashboard_sso/summaryLocal/ChampionsFixedRank.php?";
            var divToHide = "#FixedChampLoader";
            var divToShow = "#FixedChampionRanking";
            reload(urlToLoad, divToHide, divToShow);
        });

        $('#CombinedChampionRanking', function() {
            var urlToLoad = "dashboard_sso/summaryLocal/ChampionsCombinedRank.php?";
            var divToHide = "#CombinedChampLoader";
            var divToShow = "#CombinedChampionRanking";
            reload(urlToLoad, divToHide, divToShow);
        });


        $("#targetrolling-group > .btn").click(function() {
            $(this).addClass("active").siblings().removeClass("active");
        });

        $("#refresh").click(function() {

            $('#CombinedChampionRanking', function() {
                var urlToLoad = "dashboard_sso/summaryLocal/ChampionsCombinedRank.php?";
                var divToHide = "#CombinedChampLoader";
                var divToShow = "#CombinedChampionRanking";
                reload(urlToLoad, divToHide, divToShow);
            });

//            $('#StoreChampTlsContainer', function() {
//                var urlToLoad = "dashboard_sso/summaryLocal/FeaturedChampsInteraction.php?";
//                var divToHide = "#StoreChampTlsloader";
//                var divToShow = "#StoreChampTlsContainer";
//                reload(urlToLoad, divToHide, divToShow);
//            });
//            $('#EpisodeChampionRanking', function() {
//                var urlToLoad = "dashboard_sso/summaryLocal/ChampionsEpisodeRank.php?";
//                var divToHide = "#InteractionChampLoader";
//                var divToShow = "#EpisodeChampionRanking";
//                reload(urlToLoad, divToHide, divToShow);
//            });

            $('#MobileChampionRanking', function() {
                var urlToLoad = "dashboard_sso/summaryLocal/ChampionsMobileRank.php?";
                var divToHide = "#MobileChampLoader";
                var divToShow = "#MobileChampionRanking";
                reload(urlToLoad, divToHide, divToShow);
            });

            $('#FixedChampionRanking', function() {
                var urlToLoad = "dashboard_sso/summaryLocal/ChampionsFixedRank.php?";
                var divToHide = "#FixedChampLoader";
                var divToShow = "#FixedChampionRanking";
                reload(urlToLoad, divToHide, divToShow);
            });

            $('#StoreChampTlsContainer', function() {
                var urlToLoad = "dashboard_sso/summaryLocal/FeaturedChampsFixed.php?";
                var divToHide = "#StoreChampTlsloader";
                var divToShow = "#StoreChampTlsContainer";
                reload(urlToLoad, divToHide, divToShow);
            });

        });

<?php if ($role == 4) { ?>

            $('#FourWeekMergedEmployeeNps', function() {
                var urlToLoad = "nps_sso/summary/employeeMergedNpsFixed.php?weekly=on&shared=no&";
                var divToHide = "#FourWeekMergedEmployeeNpsloader";
                var divToShow = "#FourWeekMergedEmployeeNps";
                reload(urlToLoad, divToHide, divToShow);
            });

            $('#weekmerge-group button').click(function() {
                var textval = $(this).data("text");
                var divToHide = "#FourWeekMergedEmployeeNpsloader";
                var divToShow = "#FourWeekMergedEmployeeNps";
                var setUrl = '';

                switch (textval) {
                    case 'Interaction':
                        setUrl = "nps_sso/summary/employeeMergedNps.php?weekly=on&shared=no&";
                        break;
                    case 'Mobile':
                        setUrl = "nps_sso/summary/employeeMergedNpsMobile.php?weekly=on&shared=no&";
                        break;
                    case 'Fixed':
                        setUrl = "nps_sso/summary/employeeMergedNpsFixed.php?weekly=on&shared=no&";
                        break;
                    case 'Episode':
                        setUrl = "nps_sso/summary/employeeMergedNpsEpisode.php?weekly=on&shared=no&";
                        break;
                }

                $("#weekmerge-group button").removeClass("active");
                $(this).addClass("active");

                $("weekmerge").html(textval);
                reload(setUrl, divToHide, divToShow);


            });

            $('#MTDMergedEmployeeNps', function() {
                var urlToLoad = "nps_sso/summary/employeeMergedNpsFixed.php?shared=no&";
                var divToHide = "#MTDMergedEmployeeNpsloader";
                var divToShow = "#MTDMergedEmployeeNps";
                reload(urlToLoad, divToHide, divToShow);
            });

            $('#mtdweekmerge-group button').click(function() {
                var textval = $(this).data("text");
                var divToHide = "#MTDMergedEmployeeNpsloader";
                var divToShow = "#MTDMergedEmployeeNps";
                var setUrl = '';

                switch (textval) {
                    case 'Interaction':
                        setUrl = "nps_sso/summary/employeeMergedNps.php?shared=no&";
                        break;
                    case 'Mobile':
                        setUrl = "nps_sso/summary/employeeMergedNpsMobile.php?shared=no&";
                        break;
                    case 'Fixed':
                        setUrl = "nps_sso/summary/employeeMergedNpsFixed.php?shared=no&";
                        break;
                    case 'Episode':
                        setUrl = "nps_sso/summary/employeeMergedNpsEpisode.php?shared=no&";
                        break;
                }
                $("#mtdweekmerge-group button").removeClass("active");
                $(this).addClass("active");

                $("mtdmerge").html(textval);
                reload(setUrl, divToHide, divToShow);


            });


<?php } ?>

    });



</script>
