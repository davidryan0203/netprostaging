<?php

error_reporting(0);
include($_SERVER['DOCUMENT_ROOT'] . '/db/connection.php');

function getChamp($report, $type, $shared, $from, $to, $remove, $storeType = NULL, $isCombined = NULL, $empID = NULL, $reconOwnerID = NULL, $fromNpsPage = NULL) {
    global $connection;
    session_start();
    $role = $_SESSION['UserRoleID'];
    date_default_timezone_set('Australia/Melbourne');
    if ($remove == 'RemoveNull') {
        $removeSql = "and c.f_Status = 1 and d.f_Status = 1 AND a.f_ActivationExtra5 not in ('P000000','')";
    } elseif ($remove == 'IncludeALL') {
        $removeSql = "AND a.f_ActivationExtra5 not in ('P000000')  AND a.f_LikelihoodToRecommendRetail is not NULL";
    } else {
        $removeSql = " ";
    }


    if ($isCombined == 'Yes') {
        $innerQuery = "GROUP BY d.f_UserID
                        ORDER BY d.f_UserID , a.f_DateTimeResponseMelbourne) w ON w.f_UserID = e.f_StoreOwnerUserID";
        $outerQuery = "GROUP BY EmpName
                        ORDER BY c.f_EmpID , a.f_DateTimeResponseMelbourne";
    } else {
        $innerQuery = " GROUP BY c.f_StoreID
                         ORDER BY c.f_StoreID , a.f_DateTimeResponseMelbourne) w ON w.f_StoreID = e.f_StoreID ";
        $outerQuery = " GROUP BY e.f_StoreID , a.f_ActivationExtra5
                        ORDER BY e.f_StoreID , a.f_DateTimeResponseMelbourne";
    }

    if (!is_null($to) && !is_null($from)) {
        $to = date("Y-m-d", strtotime($to));
        $from = date("Y-m-d", strtotime($from));
        $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
    } else {
        $sqlWhen = " AND MONTH(a.f_DateTimeResponseMelbourne) = MONTH(CURDATE())
                     AND YEAR(a.f_DateTimeResponseMelbourne) = YEAR(CURDATE())";
    }

    $sqlStoreType = " ";
    if (!is_null($storeType) && $storeType != "") {
        $sqlStoreType = " AND e.f_StoreTypeID = '{$storeType}'";
    }
    if (!empty($empID) && !is_null($empID)) {
        $sqlEmpID = " AND d.f_EmpID = '{$empID}'";
    }
//    switch ($role) {
//        case '1':
//            $sqlSharedStore = " ";
//            break;
//        case '2':
//        case '3':
//            if ($shared == "no") {
////                $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
////                $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
//                if ($fromNpsPage == 'Yes') {
//                    $selectCollab = "SELECT f_StoreCollabWithUserID as 'CollabWith' FROM t_storecollab WHERE f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}'";
//                    $resultCollab = mysql_query($selectCollab, $connection);
//                    while ($row = mysql_fetch_assoc($resultCollab)) {
//                        $CollabWith[] = $row['CollabWith'];
//                    }
//                    if (count($CollabWith) >= 1) {
//                        array_push($CollabWith, $_SESSION['UserProfileID']);
//                        $whereInCollab = implode(', ', $CollabWith);
//                        $sqlSharedStore = " AND c.f_StoreOwnerUserID in ({$whereInCollab}) ";
//                        $sqlSharedEmp = " AND e.f_StoreOwnerUserID in ({$whereInCollab}) ";
//                    } else {
//                        $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
//                        $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
//                    }
//                } else {
//                    $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
//                    $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
//                }
//            } else {
//                $sqlSharedStore = " ";
//            }
//            break;
//        case '4':
//            $query = $_SESSION['EmpProfileID'];
//            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
//        left join t_empnumber b on a.f_EmpID = b.f_EmpID
//        left join t_storelist c on b.f_StoreID = c.f_StoreID
//        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
//            $result = mysql_query($sqlGetOwnerID, $connection);
//            while ($row = mysql_fetch_assoc($result)) {
//                $data = $row['f_StoreOwnerUserID'];
//            }
//            $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$data}' ";
//            $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$data}'";
//            break;
//        case '5':
//            $query = $_SESSION['EmpProfileID'];
//            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
//        left join t_empnumber b on a.f_EmpID = b.f_EmpID
//        left join t_storelist c on b.f_StoreID = c.f_StoreID
//        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
//            $result = mysql_query($sqlGetOwnerID, $connection);
//            while ($row = mysql_fetch_assoc($result)) {
//                $OwnerID = $row['f_StoreOwnerUserID'];
//            }
//            if ($shared != "no") {
//                $sqlgetPartners = "select a.f_StoreOwnerUserID FROM t_storelist a
//            LEFT JOIN t_sharedstore b on a.f_StoreID = b.f_StoreID
//            WHERE b.f_ShareToUserID = '{$OwnerID}' group by a.f_StoreOwnerUserID";
//                $result = mysql_query($sqlgetPartners, $connection);
//                while ($row = mysql_fetch_assoc($result)) {
//                    $storePartners[] = $row['f_StoreOwnerUserID'];
//                }
//                foreach ($storePartners as $data) {
//                    $quotedString .= "'$data',";
//                }
//                $quotedString .= "'{$OwnerID}',";
//                $quotedString = trim($quotedString, ",");
//                $sqlSharedStore = " ";
//                $sqlSharedEmp = " ";
////                $sqlSharedStore = " AND c.f_StoreOwnerUserID IN ({$quotedString},'$OwnerID')";
////                $sqlSharedEmp = " AND e.f_StoreOwnerUserID IN ({$quotedString},'$OwnerID')";
//            } else {
//                $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$OwnerID}' ";
//                $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$OwnerID}' ";
//            }
//            break;
//        case '6':
//            $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$reconOwnerID}' ";
//            $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$reconOwnerID}' ";
//            break;
//    }

    switch ($role) {
        case '1':
            $sqlSharedStore = " ";
            break;
        case '2':
        case '3':
        case '4':
        case '5':
            if ($shared != "no") {
                $sqlSharedStore = " ";
            } else {
                $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$_SESSION['ClientOwnerID']}' ";
                $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$_SESSION['ClientOwnerID']}' ";
            }

            break;
        case '6':
            $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$reconOwnerID}' ";
            $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$reconOwnerID}' ";
            break;
    }

    $selectStorelIst = "SELECT f_StoreID,f_StoreListName FROM t_storelist WHERE f_Status = 1";
    $resStoreList = mysql_query($selectStorelIst);
    while ($datastoreList = mysql_fetch_assoc($resStoreList)) {
        $storeList[] = $datastoreList;
    }
    $i = 0;


    if ($report == 'Interaction') {
        $storeNPS = "StoreInteraction";
        $sqlReportEmp = "SUM(CASE
                WHEN(
                    a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS EmpAdvocate,
        SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '7' AND '8')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS EmpPassive,
        SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS EmpDetractor,
        SUM(CASE
                WHEN
                    a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS EmpTotalSurvey,
        SUM(CASE
            WHEN a.f_LikelihoodToRecommendRetail is null
            AND  a.f_ActivationExtra5 != 'P000000' THEN 1  else 0
        END) as EmpNoScore,
       ((SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
            END) - SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
            END)) * 100 / SUM(CASE
                WHEN
                    a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
            END)) AS EmpInteraction, ";

        $sqlReportStore = "SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS StoreAdvocate,
        SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '7' AND '8')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS StorePassive,
        SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS StoreDetractor,
        SUM(CASE
                WHEN
                    a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS StoreInteractionTotalSurvey,
        ((SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) - SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END)) * 100 / SUM(CASE
                WHEN
                    a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END)) AS StoreInteraction ";
    } elseif ($report == 'Episode') {
        $storeNPS = "StoreEpisode";
        $sqlReportEmp = "SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) AS EmpAdvocate,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '7' AND '8')
            THEN 1 ELSE 0
        END) AS EmpPassive,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '0' AND '6')
            THEN 1 ELSE 0
        END) AS EmpDetractor,
        SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END) AS EmpTotalSurvey,
        SUM(CASE
            WHEN a.f_RecommendRate is null
            THEN 1  else 0
        END) as EmpNoScore,
        ((SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '9' AND '10')
                THEN 1 ELSE 0
            END) - SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '0' AND '6')
                THEN 1 ELSE 0
            END)) * 100 / SUM(CASE
                WHEN
                    a.f_RecommendRate BETWEEN 0 AND 10
                THEN 1 ELSE 0
            END)) AS EmpEpisode, ";
        $sqlReportStore = "SUM(CASE
            WHEN (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) AS StoreAdvocate,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '7' AND '8')
            THEN 1 ELSE 0
        END) AS StorePassive,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '0' AND '6')
            THEN 1 ELSE 0
        END) AS StoreDetractor,
        SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END) AS StoreTotalSurvey,
        ((SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) - SUM(CASE

                WHEN
                    (a.f_RecommendRate BETWEEN '0' AND '6')
                THEN 1 ELSE 0
        END)) * 100 / SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END)) AS StoreEpisode";
    }

    // foreach ($storeList as $storeID) {
    $sqlEmpNps = "SELECT c.f_EmpID AS EmpID,
        CASE
        WHEN
             d.f_EmpFirstName  IS NULL
                AND (a.f_ActivationExtra5 IS NULL
                OR a.f_ActivationExtra5 = '')
        THEN
             'Unassigned'
        WHEN  d.f_EmpFirstName  IS NULL THEN a.f_ActivationExtra5
        ELSE CONCAT(d.f_EmpFirstName, ' ', d.f_EmpLastName)
    END AS EmpName,
    CASE
        WHEN
            c.f_EmpPNumber IS NULL
                AND (a.f_ActivationExtra5 IS NULL
                OR a.f_ActivationExtra5 = '')
        THEN
            'empty'
        WHEN f_EmpPNumber IS NULL THEN a.f_ActivationExtra5
        ELSE c.f_EmpPNumber
    END AS f_EmpPNumber,
        e.f_StoreListName AS Store,
        {$sqlReportEmp}

        w.* ,
         CONCAT(x.f_UserFirstName, ' ', x.f_UserLastName) AS StoreOwnerName,
	'images/user_icon.png' as ImgUrl
        FROM t_surveysd a
        LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_empnumber c ON a.f_ActivationExtra5 = c.f_EmpPNumber and a.f_StoreID = c.f_StoreID
        LEFT JOIN t_emplist d ON c.f_EmpID = d.f_EmpID
        LEFT JOIN t_storelist e ON a.f_StoreID = e.f_StoreID  and e.f_Status = 1

        LEFT JOIN (
        SELECT
        {$sqlReportStore}
        , c.f_StoreID , d.f_UserID
        FROM t_surveysd a
        LEFT JOIN t_surveypd b ON a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID  AND c.f_Status = 1
        LEFT JOIN t_userlist d ON c.f_StoreOwnerUserID = d.f_UserID
        WHERE 1 = 1
            {$sqlWhen} {$sqlSharedStore}
            {$innerQuery}

        LEFT JOIN t_userlist x ON e.f_StoreOwnerUserID = x.f_UserID
        WHERE 1 = 1
           {$sqlEmpID} {$removeSql} {$sqlWhen} {$sqlSharedEmp} {$sqlStoreType}
            {$outerQuery} ";
    mysql_query("SET SQL_BIG_SELECTS=1");
    // echo "<pre>";
    // echo $sqlEmpNps;
    //  exit;

    $resultEmpNps = mysql_query($sqlEmpNps, $connection);
    $i = 0;


    while ($rowEmpNps = mysql_fetch_assoc($resultEmpNps)) {


        $storeListnew[] = $rowEmpNps['Store'];



        $EmplInteractionImpactData[$rowEmpNps['Store']][$i]['Data'] = $rowEmpNps;

        //Prepare Data needed for impact computation
        $NewStoreAdvocate = intval($rowEmpNps['StoreAdvocate']) - intval($rowEmpNps['EmpAdvocate']);
        $NewStorePassive = intval($rowEmpNps['StorePassive']) - intval($rowEmpNps['EmpPassive']);
        $NewStoreDetractor = intval($rowEmpNps['StoreDetractor']) - intval($rowEmpNps['EmpDetractor']);
        $NewStoreTotalSurvey = intval($NewStoreAdvocate) + intval($NewStoreDetractor) + intval($NewStorePassive);
        $NewStoreNPS = (($NewStoreAdvocate - $NewStoreDetractor) / $NewStoreTotalSurvey) * 100;
        $StoreNPS = $rowEmpNps[$storeNPS];
        //Employee Impact
        $EmpImpact = $StoreNPS - $NewStoreNPS;

        $EmplInteractionImpactData[$rowEmpNps['Store']][$i]['EmpImpact'] = $EmpImpact;

        if ($type == "AllEmployees") {
            $EmplInteractionImpactData2[$i]['Data'] = $rowEmpNps;
            $EmplInteractionImpactData2[$i]['EmpImpact'] = $EmpImpact;
            $EmplInteractionImpactData2[$i]['EmpNotInStore'] = $NewStoreNPS;
        }

        // $toSortTotalAdvo[] = $sort['IntAdvocate'];
        $toSorImpact[$rowEmpNps['Store']][] = $EmpImpact;
        $toSorImpact2[$i][] = $EmpImpact;
        $i++;
    }

    if ($type != "AllEmployees") {
        foreach ($toSorImpact as $key => $value) {
            // sort here
            array_multisort($toSorImpact[$key], SORT_DESC, $EmplInteractionImpactData[$key]);

            //get first here
            $topEmp[$key] = $EmplInteractionImpactData[$key][0];
            //  }
        }
    } else {
        // foreach ($toSorImpact as $key => $value) {
        // sort here
        array_multisort($toSorImpact2, SORT_DESC, $EmplInteractionImpactData2);

        //get first here
        $topEmp[] = $EmplInteractionImpactData2;
        //  }
        //}
    }

//echo $sqlEmpNps;
    $storeListnew = array_unique($storeListnew);
    foreach ($topEmp as $key => $topValue) {

        //echo $key;
        if (!in_array($key, $storeListnew)) {
            unset($topEmp[$key]);
        } else {
            $toSort[] = $topValue['EmpImpact'];
        }
    }
    // array_multisort($toSort, SORT_DESC, $topEmp['EmpImpact']);
    usort($topEmp, function ($i, $j) {
        $a = $i['EmpImpact'];
        $b = $j['EmpImpact'];
        if ($a == $b)
            return 0;
        elseif ($a < $b)
            return 1;
        else
            return -1;
    });
    //  echo "<pre>";
    // print_r($toSort);
    //  print_r(array_filter($topEmp));
    //echo $sqlEmpNps;
    mysqli_close();
    return $topEmp;
}

function getChampTBC($report, $type, $shared, $from, $to, $remove, $storeType = 2, $isCombined = NULL, $rangeType = NULL, $reconOwnerID = NULL, $fromNpsPage = NULL, $empID = NULL) {
    global $connection;
    session_start();
    $role = $_SESSION['UserRoleID'];

    if ($remove == 'RemoveNull') {
        $removeSql = "and c.f_Status = 1 and d.f_Status = 1 AND a.f_ActivationExtra5 not in ('P000000','')";
    } elseif ($remove == 'IncludeALL') {
        $removeSql = "AND a.f_ActivationExtra5 not in ('P000000')";
    } else {
        $removeSql = " ";
    }

    if (!empty($empID) && !is_null($empID)) {
        $sqlEmpID = " AND d.f_EmpID = '{$empID}'";
    } else {
        $sqlEmpID = " ";
    }

    if ($rangeType == 'mtd') {
        $sqlWhenInteraction = " AND a.f_DateTimeResponseMelbourne between   '{$from} 00:00:00' AND '{$to} 23:59:59'";
        $sqlWhenEpisode = " AND a.f_DateTimeResponseMelbourne between   '{$from} 00:00:00' AND '{$to} 23:59:59'";
        $sqlWhenMobile = " AND a.f_DateTimeResponseMelbourne between   '{$from} 00:00:00' AND '{$to} 23:59:59'";
        $sqlWhenFixed = " AND a.f_DateTimeResponseMelbourne between  '{$from} 00:00:00' AND '{$to} 23:59:59'";
    } else {
        if (!is_null($to) || !is_null($from)) {


            $lastDate = date('Y-m-t', strtotime($to));

            $sqlWhenInteraction = " AND a.f_DateTimeResponseMelbourne BETWEEN CONCAT(DATE_FORMAT(DATE_SUB('{$lastDate}', INTERVAL 2 MONTH) ,'%Y-%m-01'),' ','00:00:00') AND '{$lastDate} 23:59:59'";
            $sqlWhenEpisode = " AND a.f_DateTimeResponseMelbourne BETWEEN CONCAT(DATE_FORMAT(DATE_SUB('{$lastDate}', INTERVAL 2 MONTH) ,'%Y-%m-01'),' ','00:00:00') AND '{$lastDate} 23:59:59'";
            $sqlWhenMobile = " AND a.f_DateTimeResponseMelbourne BETWEEN CONCAT(DATE_FORMAT(DATE_SUB('{$lastDate}', INTERVAL 2 MONTH) ,'%Y-%m-01'),' ','00:00:00') AND '{$lastDate} 23:59:59'";
            $sqlWhenFixed = " AND a.f_DateTimeResponseMelbourne BETWEEN CONCAT(DATE_FORMAT(DATE_SUB('{$lastDate}', INTERVAL 5 MONTH) ,'%Y-%m-01'),' ','00:00:00') AND'{$lastDate} 23:59:59'";


            // $firstday = date("Y-m-01", strtotime($to));
//            $mobileday = date('Y-m-01', strtotime("$lastData + 2  months ago"));
//            $fixedday = date('Y-m-01', strtotime("$lastData + 5  months ago"));
//
//            $sqlWhenInteraction = " AND a.f_DateTimeResponseMelbourne between   '{$mobileday}' AND '{$lastData}'";
//            $sqlWhenMobile = " AND a.f_DateTimeResponseMelbourne between   '{$mobileday}' AND '{$lastData}'";
//            $sqlWhenFixed = " AND a.f_DateTimeResponseMelbourne between  '{$fixedday}' AND '{$lastData}'";
        } else {
            $sqlWhenMobile = " AND a.f_DateTimeResponseMelbourne BETWEEN DATE_SUB(LAST_DAY(CURDATE()), INTERVAL 3 MONTH) AND LAST_DAY(CURDATE())";
            $sqlWhenFixed = "AND a.f_DateTimeResponseMelbourne BETWEEN DATE_SUB(LAST_DAY(CURDATE()), INTERVAL 6 MONTH) AND LAST_DAY(CURDATE())";
            $sqlWhenInteraction = " AND a.f_DateTimeResponseMelbourne BETWEEN DATE_SUB(LAST_DAY(CURDATE()), INTERVAL 1 MONTH) AND LAST_DAY(CURDATE())";
            $sqlWhenEpisode = " AND a.f_DateTimeResponseMelbourne BETWEEN DATE_SUB(LAST_DAY(CURDATE()), INTERVAL 1 MONTH) AND LAST_DAY(CURDATE())";
        }
    }

    //echo $sqlWhenMobile;
    // echo $sqlWhenInteraction;

    if ($isCombined == 'Yes') {
        $innerQuery = "GROUP BY d.f_UserID
                        ORDER BY d.f_UserID , a.f_DateTimeResponseMelbourne) w ON w.f_UserID = e.f_StoreOwnerUserID";
        $outerQuery = "GROUP BY EmpName
                        ORDER BY c.f_EmpID , a.f_DateTimeResponseMelbourne";
    } else {
        $innerQuery = " GROUP BY c.f_StoreID
                         ORDER BY c.f_StoreID , a.f_DateTimeResponseMelbourne) w ON w.f_StoreID = e.f_StoreID ";
        $outerQuery = " GROUP BY e.f_StoreID , a.f_ActivationExtra5
                        ORDER BY e.f_StoreID , a.f_DateTimeResponseMelbourne";
    }
//
//    if (!is_null($to) && !is_null($from)) {
//        $to = date("Y-m-d", strtotime($to));
//        $from = date("Y-m-d", strtotime($from));
//        $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
//    } else {
//        $sqlWhen = " AND MONTH(a.f_DateTimeResponseMelbourne) = MONTH(CURDATE())
//                     AND YEAR(a.f_DateTimeResponseMelbourne) = YEAR(CURDATE())";
//    }

    $sqlStoreType = " ";
    if (!is_null($storeType) && $storeType != "") {
        $sqlStoreType = " AND e.f_StoreTypeID = '{$storeType}'";
    }
    // echo $sqlWhen;
//    switch ($role) {
//        case '1':
//            $sqlSharedStore = " ";
//            break;
//        case '2':
//        case '3':
//            if ($shared == "no") {
//
//                if ($fromNpsPage == 'Yes') {
//                    $selectCollab = "SELECT f_StoreCollabWithUserID as 'CollabWith' FROM t_storecollab WHERE f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}'";
//                    $resultCollab = mysql_query($selectCollab, $connection);
//                    while ($row = mysql_fetch_assoc($resultCollab)) {
//                        $CollabWith[] = $row['CollabWith'];
//                    }
//                    if (count($CollabWith) >= 1) {
//                        array_push($CollabWith, $_SESSION['UserProfileID']);
//                        $whereInCollab = implode(', ', $CollabWith);
//                        $sqlSharedStore = " AND c.f_StoreOwnerUserID in ({$whereInCollab}) ";
//                        $sqlSharedEmp = " AND e.f_StoreOwnerUserID in ({$whereInCollab}) ";
//                    } else {
//                        $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
//                        $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
//                    }
//                } else {
//                    $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
//                    $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
//                }
//            } else {
//                $sqlSharedStore = " ";
//                $sqlSharedEmp = " ";
//            }
//            break;
//        case '4':
//            $query = $_SESSION['EmpProfileID'];
//            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
//        left join t_empnumber b on a.f_EmpID = b.f_EmpID
//        left join t_storelist c on b.f_StoreID = c.f_StoreID
//        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
//            $result = mysql_query($sqlGetOwnerID, $connection);
//            while ($row = mysql_fetch_assoc($result)) {
//                $data = $row['f_StoreOwnerUserID'];
//            }
//            $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$data}' ";
//            $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$data}'";
//            break;
//        case '5':
//            $query = $_SESSION['EmpProfileID'];
//            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
//        left join t_empnumber b on a.f_EmpID = b.f_EmpID
//        left join t_storelist c on b.f_StoreID = c.f_StoreID
//        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
//            $result = mysql_query($sqlGetOwnerID, $connection);
//            while ($row = mysql_fetch_assoc($result)) {
//                $OwnerID = $row['f_StoreOwnerUserID'];
//            }
//            if ($shared != "no") {
//                $sqlgetPartners = "select a.f_StoreOwnerUserID FROM t_storelist a
//            LEFT JOIN t_sharedstore b on a.f_StoreID = b.f_StoreID
//            WHERE b.f_ShareToUserID = '{$OwnerID}' group by a.f_StoreOwnerUserID";
//                $result = mysql_query($sqlgetPartners, $connection);
//                while ($row = mysql_fetch_assoc($result)) {
//                    $storePartners[] = $row['f_StoreOwnerUserID'];
//                }
//                foreach ($storePartners as $data) {
//                    $quotedString .= "'$data',";
//                }
//                $quotedString .= "'{$OwnerID}',";
//                $quotedString = trim($quotedString, ",");
////                $sqlSharedStore = " AND c.f_StoreOwnerUserID IN ({$quotedString},'$OwnerID')";
////                $sqlSharedEmp = " AND e.f_StoreOwnerUserID IN ({$quotedString},'$OwnerID')";
//                $sqlSharedStore = " ";
//                $sqlSharedEmp = " ";
//            } else {
//                $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$OwnerID}' ";
//                $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$OwnerID}' ";
//            }
//            break;
//        case '6':
//            $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$reconOwnerID}' ";
//            $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$reconOwnerID}' ";
//
//            break;
//    }

    switch ($role) {
        case '1':
            $sqlSharedStore = " ";
            break;
        case '2':
        case '3':
        case '4':
        case '5':
            if ($shared != "no") {
                $sqlSharedStore = " ";
            } else {
                $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$_SESSION['ClientOwnerID']}' ";
                $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$_SESSION['ClientOwnerID']}' ";
            }

            break;
        case '6':
            $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$reconOwnerID}' ";
            $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$reconOwnerID}' ";
            break;
    }

    $selectStorelIst = "SELECT f_StoreID,f_StoreListName FROM t_storelist WHERE f_Status = 1";
    $resStoreList = mysql_query($selectStorelIst);
    while ($datastoreList = mysql_fetch_assoc($resStoreList)) {
        $storeList[] = $datastoreList;
    }
    $i = 0;


    if ($report == 'Interaction') {
        $storeNPS = "StoreInteraction";
        $sqlWhen = $sqlWhenInteraction;
        $sqlReportProduct = " ";
        $sqlReportEmp = "SUM(CASE
                WHEN(
                    a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS EmpAdvocate,
        SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '7' AND '8')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS EmpPassive,
        SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS EmpDetractor,
        SUM(CASE
                WHEN
                    a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS EmpTotalSurvey,
        SUM(CASE
            WHEN  (a.f_LikelihoodToRecommendRetail IS NULL)
            AND  a.f_ActivationExtra5 != 'P000000' THEN 1  else 0
        END) as EmpNoScore,
       ((SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
            END) - SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
            END)) * 100 / SUM(CASE
                WHEN
                    a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
            END)) AS EmpInteraction, ";

        $sqlReportStore = "SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS StoreAdvocate,
        SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '7' AND '8')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS StorePassive,
        SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS StoreDetractor,
        SUM(CASE
                WHEN
                    a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS StoreInteractionTotalSurvey,
        ((SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) - SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END)) * 100 / SUM(CASE
                WHEN
                    a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END)) AS StoreInteraction ";
    } elseif ($report == 'Mobile') {
        $storeNPS = "StoreMobile";
        $sqlWhen = $sqlWhenMobile;
        $sqlReportProduct = " AND b.f_ProductID in ('1','2')";
        $sqlReportEmp = "SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) AS EmpAdvocate,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '7' AND '8')
            THEN 1 ELSE 0
        END) AS EmpPassive,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '0' AND '6')
            THEN 1 ELSE 0
        END) AS EmpDetractor,
        SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END) AS EmpTotalSurvey,
        SUM(CASE
            WHEN (a.f_RecommendRate is null)
            THEN 1  else 0
        END) as EmpNoScore,
        ((SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '9' AND '10')
                THEN 1 ELSE 0
            END) - SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '0' AND '6')
                THEN 1 ELSE 0
            END)) * 100 / SUM(CASE
                WHEN
                    a.f_RecommendRate BETWEEN 0 AND 10
                THEN 1 ELSE 0
            END)) AS EmpMobile, ";
        $sqlReportStore = "SUM(CASE
            WHEN (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) AS StoreAdvocate,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '7' AND '8')
            THEN 1 ELSE 0
        END) AS StorePassive,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '0' AND '6')
            THEN 1 ELSE 0
        END) AS StoreDetractor,
        SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END) AS StoreTotalSurvey,
        ((SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) - SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '0' AND '6')
                THEN 1 ELSE 0
        END)) * 100 / SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END)) AS StoreMobile";
    } elseif ($report == 'Fixed') {
        $storeNPS = "StoreFixed";
        $sqlWhen = $sqlWhenFixed;
        $sqlReportProduct = " AND b.f_ProductID in ('5','6','7','8','9','10','11','12','13','16','17','18','19','20')";
        $sqlReportEmp = " (SUM(CASE
                WHEN a.f_RecommendRate BETWEEN '9' AND '10' THEN 1
                ELSE 0
            END)) AS EmpAdvocate,
        (SUM(CASE
                WHEN a.f_RecommendRate BETWEEN '7' AND '8' THEN 1
                ELSE 0
            END)) AS EmpPassive,
        (SUM(CASE
                WHEN a.f_RecommendRate BETWEEN '0' AND '6' THEN 1
                ELSE 0
            END)) AS EmpDetractor,
        (SUM(CASE
                WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
                ELSE 0
            END) ) AS EmpTotalSurvey,
        (SUM(CASE
                WHEN (a.f_RecommendRate IS NULL) THEN 1
                ELSE 0
            END)) as EmpNoScore,
         (((SUM(CASE
        WHEN a.f_RecommendRate BETWEEN '9' AND '10' THEN 1
        ELSE 0
    END)) - (SUM(CASE
        WHEN a.f_RecommendRate BETWEEN '0' AND '6' THEN 1
        ELSE 0
    END))) * 100 / ((SUM(CASE
        WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
        ELSE 0
    END)))) AS EmpFixed, ";
        $sqlReportStore = "(SUM(CASE
                WHEN a.f_RecommendRate BETWEEN '9' AND '10' THEN 1
                ELSE 0
            END)) AS StoreAdvocate,
        (SUM(CASE
                WHEN a.f_RecommendRate BETWEEN '7' AND '8' THEN 1
                ELSE 0
            END)) AS StorePassive,
        (SUM(CASE
                WHEN a.f_RecommendRate BETWEEN '0' AND '6' THEN 1
                ELSE 0
            END)) AS StoreDetractor,
        (SUM(CASE
                WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
                ELSE 0
            END) ) AS StoreTotalSurvey,
        (SUM(CASE
                WHEN (a.f_RecommendRate IS NULL) THEN 1
                ELSE 0
            END)) as StoreNoScore,
         (((SUM(CASE
        WHEN a.f_RecommendRate BETWEEN '9' AND '10' THEN 1
        ELSE 0
    END)) - (SUM(CASE
        WHEN a.f_RecommendRate BETWEEN '0' AND '6' THEN 1
        ELSE 0
    END))) * 100 / ((SUM(CASE
        WHEN a.f_RecommendRate BETWEEN 0 AND 10 THEN 1
        ELSE 0
    END)))) AS StoreFixed";
    } elseif ($report == 'Episode') {
        $storeNPS = "StoreEpisode";
        $sqlWhen = $sqlWhenEpisode;
        $sqlReportProduct = " ";
        $sqlReportEmp = "SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) AS EmpAdvocate,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '7' AND '8')
            THEN 1 ELSE 0
        END) AS EmpPassive,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '0' AND '6')
            THEN 1 ELSE 0
        END) AS EmpDetractor,
        SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END) AS EmpTotalSurvey,
        SUM(CASE
            WHEN (a.f_RecommendRate is null)
            THEN 1  else 0
        END) as EmpNoScore,
        ((SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '9' AND '10')
                THEN 1 ELSE 0
            END) - SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '0' AND '6')
                THEN 1 ELSE 0
            END)) * 100 / SUM(CASE
                WHEN
                    a.f_RecommendRate BETWEEN 0 AND 10
                THEN 1 ELSE 0
            END)) AS EmpEpisode, ";
        $sqlReportStore = "SUM(CASE
            WHEN (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) AS StoreAdvocate,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '7' AND '8')
            THEN 1 ELSE 0
        END) AS StorePassive,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '0' AND '6')
            THEN 1 ELSE 0
        END) AS StoreDetractor,
        SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END) AS StoreTotalSurvey,
        ((SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) - SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '0' AND '6')
                THEN 1 ELSE 0
        END)) * 100 / SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END)) AS StoreEpisode";
    }

    // foreach ($storeList as $storeID) {
    $sqlEmpNps = "SELECT c.f_EmpID AS EmpID,
        CASE
        WHEN
             d.f_EmpFirstName  IS NULL
                AND (a.f_ActivationExtra5 IS NULL
                OR a.f_ActivationExtra5 = '')
        THEN
             'Unassigned'
        WHEN  d.f_EmpFirstName  IS NULL THEN a.f_ActivationExtra5
        ELSE CONCAT(d.f_EmpFirstName, ' ', d.f_EmpLastName)
    END AS EmpName,
    CASE
        WHEN
            c.f_EmpPNumber IS NULL
                AND (a.f_ActivationExtra5 IS NULL
                OR a.f_ActivationExtra5 = '')
        THEN
            'empty'
        WHEN f_EmpPNumber IS NULL THEN a.f_ActivationExtra5
        ELSE c.f_EmpPNumber
    END AS f_EmpPNumber,
        e.f_StoreListName AS Store,
        {$sqlReportEmp}

        w.* ,
         CONCAT(x.f_UserFirstName, ' ', x.f_UserLastName) AS StoreOwnerName,
	'images/user_icon.png'  as ImgUrl
        FROM t_surveysd a
        LEFT JOIN t_surveypd b on a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_empnumber c ON a.f_ActivationExtra5 = c.f_EmpPNumber and a.f_StoreID = c.f_StoreID
        LEFT JOIN t_emplist d ON c.f_EmpID = d.f_EmpID
        LEFT JOIN t_storelist e ON a.f_StoreID = e.f_StoreID
        LEFT JOIN (
        SELECT
        {$sqlReportStore}
        , c.f_StoreID , d.f_UserID
        FROM t_surveysd a
        LEFT JOIN t_surveypd b on a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID  AND c.f_Status = 1
        LEFT JOIN t_userlist d ON c.f_StoreOwnerUserID = d.f_UserID
        WHERE 1 = 1 AND b.f_CustomerBusinessUnitID = 2
            {$sqlWhen} {$sqlSharedStore} {$sqlReportProduct}
            {$innerQuery}

        LEFT JOIN t_userlist x ON e.f_StoreOwnerUserID = x.f_UserID
        WHERE 1 = 1
            {$sqlEmpID}
            {$removeSql} {$sqlWhen} {$sqlSharedEmp} {$sqlStoreType} {$sqlReportProduct}  AND b.f_CustomerBusinessUnitID = 2
            {$outerQuery} ";
    mysql_query("SET SQL_BIG_SELECTS=1");
    //  echo "<pre>";
//
    // echo $sqlEmpNps;
    //  echo "<br>";
//exit;
    $resultEmpNps = mysql_query($sqlEmpNps, $connection);
    $i = 0;
    while ($rowEmpNps = mysql_fetch_assoc($resultEmpNps)) {
        $storeListnew[] = $rowEmpNps['Store'];



        $EmplInteractionImpactData[$rowEmpNps['Store']][$i]['Data'] = $rowEmpNps;

        //Prepare Data needed for impact computation
        $NewStoreAdvocate = intval($rowEmpNps['StoreAdvocate']) - intval($rowEmpNps['EmpAdvocate']);
        $NewStorePassive = intval($rowEmpNps['StorePassive']) - intval($rowEmpNps['EmpPassive']);
        $NewStoreDetractor = intval($rowEmpNps['StoreDetractor']) - intval($rowEmpNps['EmpDetractor']);
        $NewStoreTotalSurvey = intval($NewStoreAdvocate) + intval($NewStoreDetractor) + intval($NewStorePassive);
        $NewStoreNPS = (($NewStoreAdvocate - $NewStoreDetractor) / $NewStoreTotalSurvey) * 100;
        $StoreNPS = $rowEmpNps[$storeNPS];
        //Employee Impact
        $EmpImpact = $StoreNPS - $NewStoreNPS;

        $EmplInteractionImpactData[$rowEmpNps['Store']][$i]['EmpImpact'] = $EmpImpact;

        if ($type == "AllEmployees") {
            $EmplInteractionImpactData2[$i]['Data'] = $rowEmpNps;
            $EmplInteractionImpactData2[$i]['EmpImpact'] = $EmpImpact;
            $EmplInteractionImpactData2[$i]['EmpNotInStore'] = $NewStoreNPS;
        }

        // $toSortTotalAdvo[] = $sort['IntAdvocate'];
        $toSorImpact[$rowEmpNps['Store']][] = $EmpImpact;
        $toSorImpact2[$i][] = $EmpImpact;
        $i++;
    }

    if ($type != "AllEmployees") {
        foreach ($toSorImpact as $key => $value) {
            // sort here
            array_multisort($toSorImpact[$key], SORT_DESC, $EmplInteractionImpactData[$key]);

            //get first here
            $topEmp[$key] = $EmplInteractionImpactData[$key][0];
            //  }
        }
    } else {
        // foreach ($toSorImpact as $key => $value) {
        // sort here
        array_multisort($toSorImpact2, SORT_DESC, $EmplInteractionImpactData2);

        //get first here
        $topEmp[] = $EmplInteractionImpactData2;
        //  }
        //}
    }

//echo $sqlEmpNps;
    $storeListnew = array_unique($storeListnew);
    foreach ($topEmp as $key => $topValue) {

        //echo $key;
        if (!in_array($key, $storeListnew)) {
            unset($topEmp[$key]);
        } else {
            $toSort[] = $topValue['EmpImpact'];
        }
    }
    // array_multisort($toSort, SORT_DESC, $topEmp['EmpImpact']);
    usort($topEmp, function ($i, $j) {
        $a = $i['EmpImpact'];
        $b = $j['EmpImpact'];
        if ($a == $b)
            return 0;
        elseif ($a < $b)
            return 1;
        else
            return -1;
    });
    //  echo "<pre>";
    // print_r($toSort);
    //  print_r(array_filter($topEmp));
    //echo $sqlEmpNps;
    mysqli_close();
    return $topEmp;
}

function getChampTLS($report, $type, $shared, $from, $to, $remove, $storeType = 1, $isCombined = NULL, $empID = NULL, $reconOwnerID = NULL, $fromNpsPage = NULL, $storeID = NULL, $reportRange = NULL) {
 
    global $connection;
    session_start();
    $role = $_SESSION['UserRoleID'];
    date_default_timezone_set('Australia/Melbourne');
    if ($remove == 'RemoveNull') {
        $removeSql = "and c.f_Status = 1 and d.f_Status = 1 AND a.f_ActivationExtra5 not in ('P000000','')";
    } elseif ($remove == 'IncludeALL') {
        $removeSql = "AND a.f_ActivationExtra5 not in ('P000000')  ";
    } elseif ($remove == 'IncludeALLEpisode') {
        $removeSql = "AND a.f_ActivationExtra5 not in ('P000000') ";
    } else {
        $removeSql = " ";
    }


    if ($isCombined == 'Yes') {
        $innerQuery = "GROUP BY d.f_UserID
                        ORDER BY d.f_UserID , a.f_DateTimeResponseMelbourne) w ON w.f_UserID = e.f_StoreOwnerUserID";
        $outerQuery = "GROUP BY EmpName
                        ORDER BY c.f_EmpID , a.f_DateTimeResponseMelbourne";
    } else {
        $innerQuery = " GROUP BY c.f_StoreID
                         ORDER BY c.f_StoreID , a.f_DateTimeResponseMelbourne) w ON w.f_StoreID = e.f_StoreID ";
        $outerQuery = " GROUP BY e.f_StoreID , a.f_ActivationExtra5
                        ORDER BY e.f_StoreID , a.f_DateTimeResponseMelbourne";
    }

    switch ($reportRange) {

        case 'MTD':
            if (!is_null($to) && !is_null($from)) {
                $to = date("Y-m-d", strtotime($to));
                $from = date("Y-m-d", strtotime($from));
                $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
            } else {
                $sqlWhen = " AND MONTH(a.f_DateTimeResponseMelbourne) = MONTH(CURDATE())
                             AND YEAR(a.f_DateTimeResponseMelbourne) = YEAR(CURDATE())";
            } break;
        case '4Week':
            $to = date("Y-m-d");
            // $from = date("Y-m-d", strtotime('4 weeks ago'));
            $from = date("Y-m-d", strtotime('-27 days'));
            $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
            break;
        case '3Months':
            $to = date("Y-m-d");
            $from = date("Y-m-d", strtotime('-3 Months'));
            $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
            break;
        case '3MMA':

            if (!is_null($to) && !is_null($from)) {
                $to = date("Y-m-d", strtotime($to));
                $from = date("Y-m-01", strtotime('first day of -2 Month', strtotime($to)));
            } else {
                $to = date("Y-m-d");
                $from = date("Y-m-01", strtotime('first day of -2 Month', strtotime($to)));
            }

            $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
            break;
        case 'HTD':
            $currentMonth = date('m');
            if (($currentMonth >= 1) && ($currentMonth <= 6)) {
                $from = date("Y-01-01");
                $to = date("Y-06-30");
            } else {
                $from = date("Y-07-01");
                $to = date("Y-12-31");
            }
            $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
            break;
        default:
            if (!is_null($to) && !is_null($from)) {
                $to = date("Y-m-d", strtotime($to));
                $from = date("Y-m-d", strtotime($from));
                $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
            } else {
                $sqlWhen = " AND MONTH(a.f_DateTimeResponseMelbourne) = MONTH(CURDATE())
                             AND YEAR(a.f_DateTimeResponseMelbourne) = YEAR(CURDATE())";
            }
            break;
    }
    if (!is_null($storeID) && !empty($storeID)) {
        $whereStoreIDe = "  and e.f_StoreID = '{$storeID}' ";
        $whereStoreIDc = "  and c.f_StoreID = '{$storeID}' ";
    } else {

        $whereStoreIDe = " ";
        $whereStoreIDc = " ";
    }

    $sqlStoreType = " ";
    // if (!is_null($storeType) && $storeType != "") {
    $sqlStoreType = " AND e.f_StoreTypeID = '1'";
    // }
    if (!empty($empID) && !is_null($empID)) {
        $sqlEmpID = " AND d.f_EmpID = '{$empID}'";
    }
//    switch ($role) {
//        case '1':
//            $sqlSharedStore = " ";
//            break;
//        case '2':
//        case '3':
//            if ($shared == "no") {
////                $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
////                $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
//                if ($fromNpsPage == 'Yes') {
//                    $selectCollab = "SELECT f_StoreCollabWithUserID as 'CollabWith' FROM t_storecollab WHERE f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}'";
//                    $resultCollab = mysql_query($selectCollab, $connection);
//                    while ($row = mysql_fetch_assoc($resultCollab)) {
//                        $CollabWith[] = $row['CollabWith'];
//                    }
//                    if (count($CollabWith) >= 1) {
//                        array_push($CollabWith, $_SESSION['UserProfileID']);
//                        $whereInCollab = implode(', ', $CollabWith);
//                        $sqlSharedStore = " AND c.f_StoreOwnerUserID in ({$whereInCollab}) ";
//                        $sqlSharedEmp = " AND e.f_StoreOwnerUserID in ({$whereInCollab}) ";
//                    } else {
//                        $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
//                        $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
//                    }
//                } else {
//                    $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
//                    $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
//                }
//            } else {
//                $sqlSharedStore = " ";
//            }
//            break;
//        case '4':
//            $query = $_SESSION['EmpProfileID'];
//            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
//        left join t_empnumber b on a.f_EmpID = b.f_EmpID
//        left join t_storelist c on b.f_StoreID = c.f_StoreID
//        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
//            $result = mysql_query($sqlGetOwnerID, $connection);
//            while ($row = mysql_fetch_assoc($result)) {
//                $data = $row['f_StoreOwnerUserID'];
//            }
//            $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$data}' ";
//            $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$data}'";
//            break;
//        case '5':
//            $query = $_SESSION['EmpProfileID'];
//            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
//        left join t_empnumber b on a.f_EmpID = b.f_EmpID
//        left join t_storelist c on b.f_StoreID = c.f_StoreID
//        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
//            $result = mysql_query($sqlGetOwnerID, $connection);
//            while ($row = mysql_fetch_assoc($result)) {
//                $OwnerID = $row['f_StoreOwnerUserID'];
//            }
//            if ($shared != "no") {
//                $sqlgetPartners = "select a.f_StoreOwnerUserID FROM t_storelist a
//            LEFT JOIN t_sharedstore b on a.f_StoreID = b.f_StoreID
//            WHERE b.f_ShareToUserID = '{$OwnerID}' group by a.f_StoreOwnerUserID";
//                $result = mysql_query($sqlgetPartners, $connection);
//                while ($row = mysql_fetch_assoc($result)) {
//                    $storePartners[] = $row['f_StoreOwnerUserID'];
//                }
//                foreach ($storePartners as $data) {
//                    $quotedString .= "'$data',";
//                }
//                $quotedString .= "'{$OwnerID}',";
//                $quotedString = trim($quotedString, ",");
//                $sqlSharedStore = " ";
//                $sqlSharedEmp = " ";
////                $sqlSharedStore = " AND c.f_StoreOwnerUserID IN ({$quotedString},'$OwnerID')";
////                $sqlSharedEmp = " AND e.f_StoreOwnerUserID IN ({$quotedString},'$OwnerID')";
//            } else {
//                $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$OwnerID}' ";
//                $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$OwnerID}' ";
//            }
//            break;
//        case '6':
//            $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$reconOwnerID}' ";
//            $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$reconOwnerID}' ";
//            break;
//    }

    switch ($role) {
        case '1':
            $sqlSharedStore = " ";
            break;
        case '2':
        case '3':
        case '4':
        case '5':
            if ($shared != "no") {
                $sqlSharedStore = " ";
            } else {
                $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$_SESSION['ClientOwnerID']}' ";
                $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$_SESSION['ClientOwnerID']}' ";
            }

            break;
        case '6':
            $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$reconOwnerID}' ";
            $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$reconOwnerID}' ";
            break;
    }


    if ($shared != 'no') {
        $privateStore = " and c.f_StoreOwnerUserID NOT IN (SELECT f_StoreOwnerUserID FROM t_privateowner) ";
        $privateEmp = " and e.f_StoreOwnerUserID NOT IN (SELECT f_StoreOwnerUserID FROM t_privateowner) ";
    }

    $selectStorelIst = "SELECT f_StoreID,f_StoreListName FROM t_storelist WHERE f_Status = 1";
    $resStoreList = mysql_query($selectStorelIst);
    while ($datastoreList = mysql_fetch_assoc($resStoreList)) {
        $storeList[] = $datastoreList;
    }
    $i = 0;


    if ($report == 'Interaction') {
        $storeNPS = "StoreInteraction";
        $sqlWhen = $sqlWhen;
        $sqlReportProduct = " AND b.f_SurveyID IN ('1','2','3','4','5','6') ";
        $sqlReportEmp = "SUM(CASE
                WHEN(
                    a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS EmpAdvocate,
        SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '7' AND '8')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS EmpPassive,
        SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS EmpDetractor,
        SUM(CASE
                WHEN
                    a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS EmpTotalSurvey,
        SUM(CASE
            WHEN  (a.f_LikelihoodToRecommendRetail IS NULL)
            AND  a.f_ActivationExtra5 != 'P000000' THEN 1  else 0
        END) as EmpNoScore,
       ((SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
            END) - SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
            END)) * 100 / SUM(CASE
                WHEN
                    a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
            END)) AS EmpInteraction, ";

        $sqlReportStore = "SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS StoreAdvocate,
        SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '7' AND '8')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS StorePassive,
        SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS StoreDetractor,
        SUM(CASE
                WHEN
                    a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS StoreInteractionTotalSurvey,
        ((SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) - SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END)) * 100 / SUM(CASE
                WHEN
                    a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END)) AS StoreInteraction ";
    } elseif ($report == 'Mobile') {
        $storeNPS = "StoreMobile";
        $sqlWhen = $sqlWhen;
        $sqlReportProduct = " AND b.f_SurveyID IN ('1','2','3','4','13','14','15')
                         AND b.f_ProductID IN ('1','2','3','14','39','40','36')";

        $sqlReportEmp = "SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) AS EmpAdvocate,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '7' AND '8')
            THEN 1 ELSE 0
        END) AS EmpPassive,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '0' AND '6')
            THEN 1 ELSE 0
        END) AS EmpDetractor,
        SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END) AS EmpTotalSurvey,
        SUM(CASE
            WHEN (a.f_RecommendRate is null)
            THEN 1  else 0
        END) as EmpNoScore,
        ((SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '9' AND '10')
                THEN 1 ELSE 0
            END) - SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '0' AND '6')
                THEN 1 ELSE 0
            END)) * 100 / SUM(CASE
                WHEN
                    a.f_RecommendRate BETWEEN 0 AND 10
                THEN 1 ELSE 0
            END)) AS EmpMobile, ";
        $sqlReportStore = "SUM(CASE
            WHEN (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) AS StoreAdvocate,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '7' AND '8')
            THEN 1 ELSE 0
        END) AS StorePassive,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '0' AND '6')
            THEN 1 ELSE 0
        END) AS StoreDetractor,
        SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END) AS StoreTotalSurvey,
        ((SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) - SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '0' AND '6')
                THEN 1 ELSE 0
        END)) * 100 / SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END)) AS StoreMobile";
    } elseif ($report == 'Fixed') {
        $storeNPS = "StoreFixed";
        $sqlWhen = $sqlWhen;
        $sqlReportProduct = " AND b.f_SurveyID IN ('1','2','3','4','6','13','14','15')
                         AND b.f_ProductID IN ('4','5','6','7','9','10','11','12','13','15','17','18','22','30','37','38','41','19','42')";
        $sqlReportEmp = "SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) AS EmpAdvocate,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '7' AND '8')
            THEN 1 ELSE 0
        END) AS EmpPassive,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '0' AND '6')
            THEN 1 ELSE 0
        END) AS EmpDetractor,
        SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END) AS EmpTotalSurvey,
        SUM(CASE
            WHEN (a.f_RecommendRate is null)
            THEN 1  else 0
        END) as EmpNoScore,
        ((SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '9' AND '10')
                THEN 1 ELSE 0
            END) - SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '0' AND '6')
                THEN 1 ELSE 0
            END)) * 100 / SUM(CASE
                WHEN
                    a.f_RecommendRate BETWEEN 0 AND 10
                THEN 1 ELSE 0
            END)) AS EmpFixed, ";
        $sqlReportStore = "SUM(CASE
            WHEN (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) AS StoreAdvocate,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '7' AND '8')
            THEN 1 ELSE 0
        END) AS StorePassive,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '0' AND '6')
            THEN 1 ELSE 0
        END) AS StoreDetractor,
        SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END) AS StoreTotalSurvey,
        ((SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) - SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '0' AND '6')
                THEN 1 ELSE 0
        END)) * 100 / SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END)) AS StoreFixed";
    } elseif ($report == 'Episode') {
        $storeNPS = "StoreEpisode";
        $sqlWhen = $sqlWhen;
        $sqlReportProduct = " ";
        $sqlReportEmp = "SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) AS EmpAdvocate,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '7' AND '8')
            THEN 1 ELSE 0
        END) AS EmpPassive,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '0' AND '6')
            THEN 1 ELSE 0
        END) AS EmpDetractor,
        SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END) AS EmpTotalSurvey,
        SUM(CASE
            WHEN (a.f_RecommendRate is null)
            THEN 1  else 0
        END) as EmpNoScore,
        ((SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '9' AND '10')
                THEN 1 ELSE 0
            END) - SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '0' AND '6')
                THEN 1 ELSE 0
            END)) * 100 / SUM(CASE
                WHEN
                    a.f_RecommendRate BETWEEN 0 AND 10
                THEN 1 ELSE 0
            END)) AS EmpEpisode, ";
        $sqlReportStore = "SUM(CASE
            WHEN (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) AS StoreAdvocate,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '7' AND '8')
            THEN 1 ELSE 0
        END) AS StorePassive,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '0' AND '6')
            THEN 1 ELSE 0
        END) AS StoreDetractor,
        SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END) AS StoreTotalSurvey,
        ((SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) - SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '0' AND '6')
                THEN 1 ELSE 0
        END)) * 100 / SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END)) AS StoreEpisode";
    }

    // foreach ($storeList as $storeID) {
    $sqlEmpNps = "SELECT c.f_EmpID AS EmpID,
        CASE
        WHEN
             d.f_EmpFirstName  IS NULL
                AND (a.f_ActivationExtra5 IS NULL
                OR a.f_ActivationExtra5 = '')
        THEN
             'Unassigned'
        WHEN  d.f_EmpFirstName  IS NULL THEN a.f_ActivationExtra5
        ELSE CONCAT(d.f_EmpFirstName, ' ', d.f_EmpLastName)
    END AS EmpName,
    CASE
        WHEN
            c.f_EmpPNumber IS NULL
                AND (a.f_ActivationExtra5 IS NULL
                OR a.f_ActivationExtra5 = '')
        THEN
            'empty'
        WHEN f_EmpPNumber IS NULL THEN a.f_ActivationExtra5
        ELSE c.f_EmpPNumber
    END AS f_EmpPNumber,
        e.f_StoreListName AS Store,
        {$sqlReportEmp}

        w.* ,
         CONCAT(x.f_UserFirstName, ' ', x.f_UserLastName) AS StoreOwnerName,
	'images/user_icon.png'  as ImgUrl
        FROM t_surveysd a
        LEFT JOIN t_surveypd b on a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_empnumber c ON a.f_ActivationExtra5 = c.f_EmpPNumber and a.f_StoreID = c.f_StoreID
        LEFT JOIN t_emplist d ON c.f_EmpID = d.f_EmpID
        LEFT JOIN t_storelist e ON a.f_StoreID = e.f_StoreID
        LEFT JOIN (
        SELECT
        {$sqlReportStore}
        , c.f_StoreID , d.f_UserID
        FROM t_surveysd a
        LEFT JOIN t_surveypd b on a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID  AND c.f_Status = 1
        LEFT JOIN t_userlist d ON c.f_StoreOwnerUserID = d.f_UserID
        WHERE 1 = 1

        {$sqlWhen} {$sqlSharedStore} {$sqlReportProduct} {$whereStoreIDc}  {$privateStore}
            {$innerQuery}

        LEFT JOIN t_userlist x ON e.f_StoreOwnerUserID = x.f_UserID
        WHERE 1 = 1
              {$sqlEmpID} {$whereStoreIDe}
            {$removeSql} {$sqlWhen} {$sqlSharedEmp} {$sqlStoreType} {$sqlReportProduct} {$privateEmp}
            {$outerQuery} ";
    mysql_query("SET SQL_BIG_SELECTS=1");
    $resultEmpNps = mysql_query($sqlEmpNps, $connection);

//    echo "<pre>";
//    echo $sqlEmpNps;
//    exit;
    $i = 0;
    while ($rowEmpNps = mysql_fetch_assoc($resultEmpNps)) {
        $storeListnew[] = $rowEmpNps['Store'];

        $EmplInteractionImpactData[$rowEmpNps['Store']][$i]['Data'] = $rowEmpNps;

        //Prepare Data needed for impact computation
        $NewStoreAdvocate = intval($rowEmpNps['StoreAdvocate']) - intval($rowEmpNps['EmpAdvocate']);
        $NewStorePassive = intval($rowEmpNps['StorePassive']) - intval($rowEmpNps['EmpPassive']);
        $NewStoreDetractor = intval($rowEmpNps['StoreDetractor']) - intval($rowEmpNps['EmpDetractor']);
        $NewStoreTotalSurvey = intval($NewStoreAdvocate) + intval($NewStoreDetractor) + intval($NewStorePassive);
        $NewStoreNPS = (($NewStoreAdvocate - $NewStoreDetractor) / $NewStoreTotalSurvey) * 100;
        $StoreNPS = $rowEmpNps[$storeNPS];
        //Employee Impact
        $EmpImpact = $StoreNPS - $NewStoreNPS;

        $EmplInteractionImpactData[$rowEmpNps['Store']][$i]['EmpImpact'] = $EmpImpact;

        if ($type == "AllEmployees") {
            $EmplInteractionImpactData2[$i]['Data'] = $rowEmpNps;
            $EmplInteractionImpactData2[$i]['EmpImpact'] = $EmpImpact;
            $EmplInteractionImpactData2[$i]['EmpNotInStore'] = $NewStoreNPS;
        }

        // $toSortTotalAdvo[] = $sort['IntAdvocate'];
        $toSorImpact[$rowEmpNps['Store']][] = $EmpImpact;
        $toSorImpact2[$i][] = $EmpImpact;
        $i++;
    }


    if ($type != "AllEmployees") {
        foreach ($toSorImpact as $key => $value) {
            // sort here
            array_multisort($toSorImpact[$key], SORT_DESC, $EmplInteractionImpactData[$key]);

            //get first here
            $topEmp[$key] = $EmplInteractionImpactData[$key][0];
            //  }
        }
    } else {
        // foreach ($toSorImpact as $key => $value) {
        // sort here
        array_multisort($toSorImpact2, SORT_DESC, $EmplInteractionImpactData2);

        //get first here
        $topEmp[] = $EmplInteractionImpactData2;
        //  }
        //}
    }

//echo $sqlEmpNps;
    $storeListnew = array_unique($storeListnew);
    foreach ($topEmp as $key => $topValue) {

        //echo $key;
        if (!in_array($key, $storeListnew)) {
            unset($topEmp[$key]);
        } else {
            $toSort[] = $topValue['EmpImpact'];
        }
    }
    // array_multisort($toSort, SORT_DESC, $topEmp['EmpImpact']);
    usort($topEmp, function ($i, $j) {
        $a = $i['EmpImpact'];
        $b = $j['EmpImpact'];
        if ($a == $b)
            return 0;
        elseif ($a < $b)
            return 1;
        else
            return -1;
    });
    // echo "<pre>";
    // print_r($toSort);
    //   print_r(array_filter($topEmp));
    //  echo $sqlEmpNps;
    mysqli_close();
    return $topEmp;
}

function getCombinedTLS($from, $to, $reportRange) {
    global $connection;
    session_start();
    $role = $_SESSION['UserRoleID'];
    date_default_timezone_set('Australia/Melbourne');

//    switch ($role) {
//        case '1':
//            $sqlSharedStore = " ";
//            break;
//        case '2':
//        case '3':
//            $OwnerID = $_SESSION['UserProfileID'];
//            break;
//        case '4':
//            $query = $_SESSION['EmpProfileID'];
//            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
//        left join t_empnumber b on a.f_EmpID = b.f_EmpID
//        left join t_storelist c on b.f_StoreID = c.f_StoreID
//        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
//            $result = mysql_query($sqlGetOwnerID, $connection);
//            while ($row = mysql_fetch_assoc($result)) {
//                $data = $row['f_StoreOwnerUserID'];
//            }
//            $OwnerID = $data;
//            break;
//        case '5':
//            $query = $_SESSION['EmpProfileID'];
//            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
//        left join t_empnumber b on a.f_EmpID = b.f_EmpID
//        left join t_storelist c on b.f_StoreID = c.f_StoreID
//        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
//            $result = mysql_query($sqlGetOwnerID, $connection);
//            while ($row = mysql_fetch_assoc($result)) {
//                $OwnerID = $row['f_StoreOwnerUserID'];
//            }
//            break;
//        case '6':
//            $OwnerID = $reconOwnerID;
//
//            break;
//    }

    switch ($role) {
        case '1':
            $OwnerID = " ";
            break;
        case '2':
        case '3':
        case '4':
        case '5':
            $OwnerID = $_SESSION['ClientOwnerID'];
            break;
        case '6':
            $OwnerID = $reconOwnerID;
            break;
    }



    switch ($reportRange) {

        case 'MTD':
            if (!is_null($to) && !is_null($from)) {
                $to = date("Y-m-d", strtotime($to));
                $from = date("Y-m-d", strtotime($from));
            } else {
                $to = date("Y-m-d");
                $from = date("Y-m-01");
            }
            break;
        case '4Week':
            $to = date("Y-m-d");
            $from = date("Y-m-d", strtotime('-27 days'));

            break;
        case '3Months':
            $to = date("Y-m-d");
            $from = date("Y-m-d", strtotime('-3 Months'));

            break;
        case 'HTD':
            $currentMonth = date('m');
            if (($currentMonth >= 1) && ($currentMonth <= 6)) {
                $from = date("Y-01-01");
                $to = date("Y-06-30");
            } else {
                $from = date("Y-07-01");
                $to = date("Y-12-31");
            }
            break;
        case '3MMA':
            if (!is_null($to) && !is_null($from)) {
                $to = date("Y-m-d", strtotime($to));
                $from = date("Y-m-01", strtotime('first day of -2 Month', strtotime($to)));
            } else {
                $to = date("Y-m-d");
                $from = date("Y-m-01", strtotime('first day of -2 Month', strtotime($to)));
            }
            break;
        default:
            if (!is_null($to) && !is_null($from)) {
                $to = date("Y-m-d", strtotime($to));
                $from = date("Y-m-d", strtotime($from));
            } else {
                $to = date("Y-m-d");
                $from = date("Y-m-01");
            }
            break;
    }


    $getMergeData = "CALL getTLSChampWithImpact('{$OwnerID}','{$from}','{$to}','fixed') ";
    mysql_query("SET SQL_BIG_SELECTS=1");
    $resultChampMerge = mysql_query($getMergeData, $connection);


    $i = 0;
    while ($rowChamp = mysql_fetch_assoc($resultChampMerge)) {
        $return[] = $rowChamp;
    }
    // echo "<pre>";
    // print_r($toSort);
    //   print_r(array_filter($topEmp));
    //  echo $sqlEmpNps;
    mysqli_close();
    return $return;
}

function getChampOthers($report, $type, $shared, $from, $to, $remove, $storeType = 3, $isCombined = NULL, $empID = NULL, $reconOwnerID = NULL, $fromNpsPage = NULL, $storeID = NULL, $reportRange = NULL) {
    global $connection;
    session_start();
    $role = $_SESSION['UserRoleID'];
    date_default_timezone_set('Australia/Melbourne');
    if ($remove == 'RemoveNull') {
        $removeSql = "and c.f_Status = 1 and d.f_Status = 1 AND a.f_ActivationExtra5 not in ('P000000','')";
    } elseif ($remove == 'IncludeALL') {
        $removeSql = "AND a.f_ActivationExtra5 not in ('P000000')  ";
    } elseif ($remove == 'IncludeALLEpisode') {
        $removeSql = "AND a.f_ActivationExtra5 not in ('P000000') ";
    } else {
        $removeSql = " ";
    }


    if ($isCombined == 'Yes') {
        $innerQuery = "GROUP BY d.f_UserID
                        ORDER BY d.f_UserID , a.f_DateTimeResponseMelbourne) w ON w.f_UserID = e.f_StoreOwnerUserID";
        $outerQuery = "GROUP BY EmpName
                        ORDER BY c.f_EmpID , a.f_DateTimeResponseMelbourne";
    } else {
        $innerQuery = " GROUP BY c.f_StoreID
                         ORDER BY c.f_StoreID , a.f_DateTimeResponseMelbourne) w ON w.f_StoreID = e.f_StoreID ";
        $outerQuery = " GROUP BY e.f_StoreID , a.f_ActivationExtra5
                        ORDER BY e.f_StoreID , a.f_DateTimeResponseMelbourne";
    }

    switch ($reportRange) {

        case 'MTD':
            if (!is_null($to) && !is_null($from)) {
                $to = date("Y-m-d", strtotime($to));
                $from = date("Y-m-d", strtotime($from));
                $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
            } else {
                $sqlWhen = " AND MONTH(a.f_DateTimeResponseMelbourne) = MONTH(CURDATE())
                             AND YEAR(a.f_DateTimeResponseMelbourne) = YEAR(CURDATE())";
            } break;
        case '4Week':
            $to = date("Y-m-d");
            // $from = date("Y-m-d", strtotime('4 weeks ago'));
            $from = date("Y-m-d", strtotime('-27 days'));
            $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
            break;
        case '3Months':
            $to = date("Y-m-d");
            $from = date("Y-m-d", strtotime('-3 Months'));
            $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
            break;
        case '3MMA':
            if (!is_null($to) && !is_null($from)) {
                $to = date("Y-m-d", strtotime($to));
                $from = date("Y-m-01", strtotime('first day of -2 Month', strtotime($to)));
            } else {
                $to = date("Y-m-d");
                $from = date("Y-m-01", strtotime('first day of -2 Month', strtotime($to)));
            }
            $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
            break;
        case 'HTD':
            $currentMonth = date('m');
            if (($currentMonth >= 1) && ($currentMonth <= 6)) {
                $from = date("Y-01-01");
                $to = date("Y-06-30");
            } else {
                $from = date("Y-07-01");
                $to = date("Y-12-31");
            }
            $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
            break;
        default:
            if (!is_null($to) && !is_null($from)) {
                $to = date("Y-m-d", strtotime($to));
                $from = date("Y-m-d", strtotime($from));
                $sqlWhen = " AND a.f_DateTimeResponseMelbourne BETWEEN '{$from} 00:00:00' and '{$to} 23:59:59' ";
            } else {
                $sqlWhen = " AND MONTH(a.f_DateTimeResponseMelbourne) = MONTH(CURDATE())
                             AND YEAR(a.f_DateTimeResponseMelbourne) = YEAR(CURDATE())";
            }
            break;
    }
    if (!is_null($storeID) && !empty($storeID)) {
        $whereStoreIDe = "  and e.f_StoreID = '{$storeID}' ";
        $whereStoreIDc = "  and c.f_StoreID = '{$storeID}' ";
    } else {

        $whereStoreIDe = " ";
        $whereStoreIDc = " ";
    }

    $sqlStoreType = " ";
    // if (!is_null($storeType) && $storeType != "") {
    $sqlStoreType = " AND e.f_StoreTypeID = '3'";
    // }
    if (!empty($empID) && !is_null($empID)) {
        $sqlEmpID = " AND d.f_EmpID = '{$empID}'";
    }
//    switch ($role) {
//        case '1':
//            $sqlSharedStore = " ";
//            break;
//        case '2':
//        case '3':
//            if ($shared == "no") {
////                $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
////                $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
//                if ($fromNpsPage == 'Yes') {
//                    $selectCollab = "SELECT f_StoreCollabWithUserID as 'CollabWith' FROM t_storecollab WHERE f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}'";
//                    $resultCollab = mysql_query($selectCollab, $connection);
//                    while ($row = mysql_fetch_assoc($resultCollab)) {
//                        $CollabWith[] = $row['CollabWith'];
//                    }
//                    if (count($CollabWith) >= 1) {
//                        array_push($CollabWith, $_SESSION['UserProfileID']);
//                        $whereInCollab = implode(', ', $CollabWith);
//                        $sqlSharedStore = " AND c.f_StoreOwnerUserID in ({$whereInCollab}) ";
//                        $sqlSharedEmp = " AND e.f_StoreOwnerUserID in ({$whereInCollab}) ";
//                    } else {
//                        $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
//                        $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
//                    }
//                } else {
//                    $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
//                    $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$_SESSION['UserProfileID']}' ";
//                }
//            } else {
//                $sqlSharedStore = " ";
//            }
//            break;
//        case '4':
//            $query = $_SESSION['EmpProfileID'];
//            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
//        left join t_empnumber b on a.f_EmpID = b.f_EmpID
//        left join t_storelist c on b.f_StoreID = c.f_StoreID
//        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
//            $result = mysql_query($sqlGetOwnerID, $connection);
//            while ($row = mysql_fetch_assoc($result)) {
//                $data = $row['f_StoreOwnerUserID'];
//            }
//            $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$data}' ";
//            $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$data}'";
//            break;
//        case '5':
//            $query = $_SESSION['EmpProfileID'];
//            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
//        left join t_empnumber b on a.f_EmpID = b.f_EmpID
//        left join t_storelist c on b.f_StoreID = c.f_StoreID
//        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
//            $result = mysql_query($sqlGetOwnerID, $connection);
//            while ($row = mysql_fetch_assoc($result)) {
//                $OwnerID = $row['f_StoreOwnerUserID'];
//            }
//            if ($shared != "no") {
//                $sqlgetPartners = "select a.f_StoreOwnerUserID FROM t_storelist a
//            LEFT JOIN t_sharedstore b on a.f_StoreID = b.f_StoreID
//            WHERE b.f_ShareToUserID = '{$OwnerID}' group by a.f_StoreOwnerUserID";
//                $result = mysql_query($sqlgetPartners, $connection);
//                while ($row = mysql_fetch_assoc($result)) {
//                    $storePartners[] = $row['f_StoreOwnerUserID'];
//                }
//                foreach ($storePartners as $data) {
//                    $quotedString .= "'$data',";
//                }
//                $quotedString .= "'{$OwnerID}',";
//                $quotedString = trim($quotedString, ",");
//                $sqlSharedStore = " ";
//                $sqlSharedEmp = " ";
////                $sqlSharedStore = " AND c.f_StoreOwnerUserID IN ({$quotedString},'$OwnerID')";
////                $sqlSharedEmp = " AND e.f_StoreOwnerUserID IN ({$quotedString},'$OwnerID')";
//            } else {
//                $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$OwnerID}' ";
//                $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$OwnerID}' ";
//            }
//            break;
//        case '6':
//            $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$reconOwnerID}' ";
//            $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$reconOwnerID}' ";
//            break;
//    }

    switch ($role) {
        case '1':
            $sqlSharedStore = " ";
            break;
        case '2':
        case '3':
        case '4':
        case '5':
            if ($shared != "no") {
                $sqlSharedStore = " ";
            } else {
                $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$_SESSION['ClientOwnerID']}' ";
                $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$_SESSION['ClientOwnerID']}' ";
            }

            break;
        case '6':
            $sqlSharedStore = " AND c.f_StoreOwnerUserID = '{$reconOwnerID}' ";
            $sqlSharedEmp = " AND e.f_StoreOwnerUserID = '{$reconOwnerID}' ";
            break;
    }

    $selectStorelIst = "SELECT f_StoreID,f_StoreListName FROM t_storelist WHERE f_Status = 1";
    $resStoreList = mysql_query($selectStorelIst);
    while ($datastoreList = mysql_fetch_assoc($resStoreList)) {
        $storeList[] = $datastoreList;
    }
    $i = 0;


    if ($report == 'Interaction') {
        $storeNPS = "StoreInteraction";
        $sqlWhen = $sqlWhen;
        $sqlReportProduct = " AND b.f_SurveyID IN ('1','2','3','4','5','6') ";
        $sqlReportEmp = "SUM(CASE
                WHEN(
                    a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS EmpAdvocate,
        SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '7' AND '8')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS EmpPassive,
        SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS EmpDetractor,
        SUM(CASE
                WHEN
                    a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS EmpTotalSurvey,
        SUM(CASE
            WHEN  (a.f_LikelihoodToRecommendRetail IS NULL)
            AND  a.f_ActivationExtra5 != 'P000000' THEN 1  else 0
        END) as EmpNoScore,
       ((SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
            END) - SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
            END)) * 100 / SUM(CASE
                WHEN
                    a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
            END)) AS EmpInteraction, ";

        $sqlReportStore = "SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS StoreAdvocate,
        SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '7' AND '8')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS StorePassive,
        SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS StoreDetractor,
        SUM(CASE
                WHEN
                    a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) AS StoreInteractionTotalSurvey,
        ((SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '9' AND '10')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END) - SUM(CASE
                WHEN
                    (a.f_LikelihoodToRecommendRetail BETWEEN '0' AND '6')
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END)) * 100 / SUM(CASE
                WHEN
                    a.f_LikelihoodToRecommendRetail BETWEEN 0 AND 10
                    AND a.f_ActivationExtra5 != 'P000000'
                THEN 1 ELSE 0
        END)) AS StoreInteraction ";
    } elseif ($report == 'Mobile') {
        $storeNPS = "StoreMobile";
        $sqlWhen = $sqlWhen;
        $sqlReportProduct = " AND b.f_SurveyID IN ('1','2','3','4','13','14','15')
                         AND b.f_ProductID IN ('1','2','3','14','39','40','36')";

        $sqlReportEmp = "SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) AS EmpAdvocate,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '7' AND '8')
            THEN 1 ELSE 0
        END) AS EmpPassive,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '0' AND '6')
            THEN 1 ELSE 0
        END) AS EmpDetractor,
        SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END) AS EmpTotalSurvey,
        SUM(CASE
            WHEN (a.f_RecommendRate is null)
            THEN 1  else 0
        END) as EmpNoScore,
        ((SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '9' AND '10')
                THEN 1 ELSE 0
            END) - SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '0' AND '6')
                THEN 1 ELSE 0
            END)) * 100 / SUM(CASE
                WHEN
                    a.f_RecommendRate BETWEEN 0 AND 10
                THEN 1 ELSE 0
            END)) AS EmpMobile, ";
        $sqlReportStore = "SUM(CASE
            WHEN (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) AS StoreAdvocate,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '7' AND '8')
            THEN 1 ELSE 0
        END) AS StorePassive,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '0' AND '6')
            THEN 1 ELSE 0
        END) AS StoreDetractor,
        SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END) AS StoreTotalSurvey,
        ((SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) - SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '0' AND '6')
                THEN 1 ELSE 0
        END)) * 100 / SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END)) AS StoreMobile";
    } elseif ($report == 'Fixed') {
        $storeNPS = "StoreFixed";
        $sqlWhen = $sqlWhen;
        $sqlReportProduct = " AND b.f_SurveyID IN ('1','2','3','4','6','13','14','15')
                         AND b.f_ProductID IN ('4','5','6','7','9','10','11','12','13','15','17','18','22','30','37','38','41','19','42')";
        $sqlReportEmp = "SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) AS EmpAdvocate,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '7' AND '8')
            THEN 1 ELSE 0
        END) AS EmpPassive,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '0' AND '6')
            THEN 1 ELSE 0
        END) AS EmpDetractor,
        SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END) AS EmpTotalSurvey,
        SUM(CASE
            WHEN (a.f_RecommendRate is null)
            THEN 1  else 0
        END) as EmpNoScore,
        ((SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '9' AND '10')
                THEN 1 ELSE 0
            END) - SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '0' AND '6')
                THEN 1 ELSE 0
            END)) * 100 / SUM(CASE
                WHEN
                    a.f_RecommendRate BETWEEN 0 AND 10
                THEN 1 ELSE 0
            END)) AS EmpFixed, ";
        $sqlReportStore = "SUM(CASE
            WHEN (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) AS StoreAdvocate,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '7' AND '8')
            THEN 1 ELSE 0
        END) AS StorePassive,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '0' AND '6')
            THEN 1 ELSE 0
        END) AS StoreDetractor,
        SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END) AS StoreTotalSurvey,
        ((SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) - SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '0' AND '6')
                THEN 1 ELSE 0
        END)) * 100 / SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END)) AS StoreFixed";
    } elseif ($report == 'Episode') {
        $storeNPS = "StoreEpisode";
        $sqlWhen = $sqlWhen;
        $sqlReportProduct = " ";
        $sqlReportEmp = "SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) AS EmpAdvocate,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '7' AND '8')
            THEN 1 ELSE 0
        END) AS EmpPassive,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '0' AND '6')
            THEN 1 ELSE 0
        END) AS EmpDetractor,
        SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END) AS EmpTotalSurvey,
        SUM(CASE
            WHEN (a.f_RecommendRate is null)
            THEN 1  else 0
        END) as EmpNoScore,
        ((SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '9' AND '10')
                THEN 1 ELSE 0
            END) - SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '0' AND '6')
                THEN 1 ELSE 0
            END)) * 100 / SUM(CASE
                WHEN
                    a.f_RecommendRate BETWEEN 0 AND 10
                THEN 1 ELSE 0
            END)) AS EmpEpisode, ";
        $sqlReportStore = "SUM(CASE
            WHEN (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) AS StoreAdvocate,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '7' AND '8')
            THEN 1 ELSE 0
        END) AS StorePassive,
        SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '0' AND '6')
            THEN 1 ELSE 0
        END) AS StoreDetractor,
        SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END) AS StoreTotalSurvey,
        ((SUM(CASE
            WHEN
                (a.f_RecommendRate BETWEEN '9' AND '10')
            THEN 1 ELSE 0
        END) - SUM(CASE
                WHEN
                    (a.f_RecommendRate BETWEEN '0' AND '6')
                THEN 1 ELSE 0
        END)) * 100 / SUM(CASE
            WHEN
                a.f_RecommendRate BETWEEN 0 AND 10
            THEN 1 ELSE 0
        END)) AS StoreEpisode";
    }

    // foreach ($storeList as $storeID) {
    $sqlEmpNps = "SELECT c.f_EmpID AS EmpID,
        CASE
        WHEN
             d.f_EmpFirstName  IS NULL
                AND (a.f_ActivationExtra5 IS NULL
                OR a.f_ActivationExtra5 = '')
        THEN
             'Unassigned'
        WHEN  d.f_EmpFirstName  IS NULL THEN a.f_ActivationExtra5
        ELSE CONCAT(d.f_EmpFirstName, ' ', d.f_EmpLastName)
    END AS EmpName,
    CASE
        WHEN
            c.f_EmpPNumber IS NULL
                AND (a.f_ActivationExtra5 IS NULL
                OR a.f_ActivationExtra5 = '')
        THEN
            'empty'
        WHEN f_EmpPNumber IS NULL THEN a.f_ActivationExtra5
        ELSE c.f_EmpPNumber
    END AS f_EmpPNumber,
        e.f_StoreListName AS Store,
        {$sqlReportEmp}

        w.* ,
         CONCAT(x.f_UserFirstName, ' ', x.f_UserLastName) AS StoreOwnerName,
	'images/user_icon.png'  as ImgUrl
        FROM t_surveysd a
        LEFT JOIN t_surveypd b on a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_empnumber c ON a.f_ActivationExtra5 = c.f_EmpPNumber and a.f_StoreID = c.f_StoreID
        LEFT JOIN t_emplist d ON c.f_EmpID = d.f_EmpID
        LEFT JOIN t_storelist e ON a.f_StoreID = e.f_StoreID
        LEFT JOIN (
        SELECT
        {$sqlReportStore}
        , c.f_StoreID , d.f_UserID
        FROM t_surveysd a
        LEFT JOIN t_surveypd b on a.f_SurveyPDID = b.f_SurveyPDID
        LEFT JOIN t_storelist c ON a.f_StoreID = c.f_StoreID  AND c.f_Status = 1
        LEFT JOIN t_userlist d ON c.f_StoreOwnerUserID = d.f_UserID
        WHERE 1 = 1

        {$sqlWhen} {$sqlSharedStore} {$sqlReportProduct} {$whereStoreIDc}
            {$innerQuery}

        LEFT JOIN t_userlist x ON e.f_StoreOwnerUserID = x.f_UserID
        WHERE 1 = 1
              {$sqlEmpID} {$whereStoreIDe}
            {$removeSql} {$sqlWhen} {$sqlSharedEmp} {$sqlStoreType} {$sqlReportProduct}
            {$outerQuery} ";
    mysql_query("SET SQL_BIG_SELECTS=1");
    $resultEmpNps = mysql_query($sqlEmpNps, $connection);

    // echo "<pre>";
    //  echo $sqlEmpNps;
    // exit;
    $i = 0;
    while ($rowEmpNps = mysql_fetch_assoc($resultEmpNps)) {
        $storeListnew[] = $rowEmpNps['Store'];

        $EmplInteractionImpactData[$rowEmpNps['Store']][$i]['Data'] = $rowEmpNps;

        //Prepare Data needed for impact computation
        $NewStoreAdvocate = intval($rowEmpNps['StoreAdvocate']) - intval($rowEmpNps['EmpAdvocate']);
        $NewStorePassive = intval($rowEmpNps['StorePassive']) - intval($rowEmpNps['EmpPassive']);
        $NewStoreDetractor = intval($rowEmpNps['StoreDetractor']) - intval($rowEmpNps['EmpDetractor']);
        $NewStoreTotalSurvey = intval($NewStoreAdvocate) + intval($NewStoreDetractor) + intval($NewStorePassive);
        $NewStoreNPS = (($NewStoreAdvocate - $NewStoreDetractor) / $NewStoreTotalSurvey) * 100;
        $StoreNPS = $rowEmpNps[$storeNPS];
        //Employee Impact
        $EmpImpact = $StoreNPS - $NewStoreNPS;

        $EmplInteractionImpactData[$rowEmpNps['Store']][$i]['EmpImpact'] = $EmpImpact;

        if ($type == "AllEmployees") {
            $EmplInteractionImpactData2[$i]['Data'] = $rowEmpNps;
            $EmplInteractionImpactData2[$i]['EmpImpact'] = $EmpImpact;
            $EmplInteractionImpactData2[$i]['EmpNotInStore'] = $NewStoreNPS;
        }

        // $toSortTotalAdvo[] = $sort['IntAdvocate'];
        $toSorImpact[$rowEmpNps['Store']][] = $EmpImpact;
        $toSorImpact2[$i][] = $EmpImpact;
        $i++;
    }


    if ($type != "AllEmployees") {
        foreach ($toSorImpact as $key => $value) {
            // sort here
            array_multisort($toSorImpact[$key], SORT_DESC, $EmplInteractionImpactData[$key]);

            //get first here
            $topEmp[$key] = $EmplInteractionImpactData[$key][0];
            //  }
        }
    } else {
        // foreach ($toSorImpact as $key => $value) {
        // sort here
        array_multisort($toSorImpact2, SORT_DESC, $EmplInteractionImpactData2);

        //get first here
        $topEmp[] = $EmplInteractionImpactData2;
        //  }
        //}
    }

//echo $sqlEmpNps;
    $storeListnew = array_unique($storeListnew);
    foreach ($topEmp as $key => $topValue) {

        //echo $key;
        if (!in_array($key, $storeListnew)) {
            unset($topEmp[$key]);
        } else {
            $toSort[] = $topValue['EmpImpact'];
        }
    }
    // array_multisort($toSort, SORT_DESC, $topEmp['EmpImpact']);
    usort($topEmp, function ($i, $j) {
        $a = $i['EmpImpact'];
        $b = $j['EmpImpact'];
        if ($a == $b)
            return 0;
        elseif ($a < $b)
            return 1;
        else
            return -1;
    });
    // echo "<pre>";
    // print_r($toSort);
    //   print_r(array_filter($topEmp));
    //  echo $sqlEmpNps;
    mysqli_close();
    return $topEmp;
}

function getCombinedOthers($from, $to, $reportRange) {
    global $connection;
    session_start();
    $role = $_SESSION['UserRoleID'];
    date_default_timezone_set('Australia/Melbourne');

//    switch ($role) {
//        case '1':
//            $sqlSharedStore = " ";
//            break;
//        case '2':
//        case '3':
//            $OwnerID = $_SESSION['UserProfileID'];
//            break;
//        case '4':
//            $query = $_SESSION['EmpProfileID'];
//            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
//        left join t_empnumber b on a.f_EmpID = b.f_EmpID
//        left join t_storelist c on b.f_StoreID = c.f_StoreID
//        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
//            $result = mysql_query($sqlGetOwnerID, $connection);
//            while ($row = mysql_fetch_assoc($result)) {
//                $data = $row['f_StoreOwnerUserID'];
//            }
//            $OwnerID = $data;
//            break;
//        case '5':
//            $query = $_SESSION['EmpProfileID'];
//            $sqlGetOwnerID = "select c.f_StoreOwnerUserID from t_userlogin a
//        left join t_empnumber b on a.f_EmpID = b.f_EmpID
//        left join t_storelist c on b.f_StoreID = c.f_StoreID
//        where a.f_EmpID ='{$query}' group by  c.f_StoreOwnerUserID limit 1 ";
//            $result = mysql_query($sqlGetOwnerID, $connection);
//            while ($row = mysql_fetch_assoc($result)) {
//                $OwnerID = $row['f_StoreOwnerUserID'];
//            }
//            break;
//        case '6':
//            $OwnerID = $reconOwnerID;
//
//            break;
//    }

    switch ($role) {
        case '1':
            $OwnerID = " ";
            break;
        case '2':
        case '3':
        case '4':
        case '5':
            $OwnerID = $_SESSION['ClientOwnerID'];
            break;
        case '6':
            $OwnerID = $reconOwnerID;
            break;
    }

    switch ($reportRange) {

        case 'MTD':
            if (!is_null($to) && !is_null($from)) {
                $to = date("Y-m-d", strtotime($to));
                $from = date("Y-m-d", strtotime($from));
            } else {
                $to = date("Y-m-d");
                $from = date("Y-m-01");
            }
            break;
        case '4Week':
            $to = date("Y-m-d");
            $from = date("Y-m-d", strtotime('-27 days'));

            break;
        case '3Months':
            $to = date("Y-m-d");
            $from = date("Y-m-d", strtotime('-3 Months'));

            break;
        case 'HTD':
            $currentMonth = date('m');
            if (($currentMonth >= 1) && ($currentMonth <= 6)) {
                $from = date("Y-01-01");
                $to = date("Y-06-30");
            } else {
                $from = date("Y-07-01");
                $to = date("Y-12-31");
            }
            break;
        case '3MMA':
            if (!is_null($to) && !is_null($from)) {
                $to = date("Y-m-d", strtotime($to));
                $from = date("Y-m-01", strtotime('first day of -2 Month', strtotime($to)));
            } else {
                $to = date("Y-m-d");
                $from = date("Y-m-01", strtotime('first day of -2 Month', strtotime($to)));
            }
            break;
        default:
            if (!is_null($to) && !is_null($from)) {
                $to = date("Y-m-d", strtotime($to));
                $from = date("Y-m-d", strtotime($from));
            } else {
                $to = date("Y-m-d");
                $from = date("Y-m-01");
            }
            break;
    }


    $getMergeData = "CALL getOthersChampWithImpact('{$OwnerID}','{$from}','{$to}') ";
    mysql_query("SET SQL_BIG_SELECTS=1");
    $resultChampMerge = mysql_query($getMergeData, $connection);


    $i = 0;
    while ($rowChamp = mysql_fetch_assoc($resultChampMerge)) {
        $return[] = $rowChamp;
    }



    // echo "<pre>";
    // print_r($toSort);
    //   print_r(array_filter($topEmp));
    //  echo $sqlEmpNps;
    mysqli_close();
    return $return;
}
