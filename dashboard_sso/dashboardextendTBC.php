<?php
session_start();
$storeType = $_GET['storeType'];
$role = $_SESSION['UserRoleID'];
//echo $role;
?>

<!--main tbc dashboard reports-->
<section class='reports'>
    <input type="hidden" id="storeType" name="storeType" value="<?php echo $storeType; ?>" />

    <div class="main-content featuredChamps no-bg">
        <div class="btn-group group1 pull-right" role="group" id="storechamp-group" aria-label="...">
            <!--<button type="button" id="StoreChampionInteraction" data-text="Interaction" class="btn btn-xs btn-primary ">Interaction</button>-->
            <button type="button" id="StoreChampionMobile" data-text="Mobile" class="btn btn-xs btn-warning">Mobile</button>
            <button type="button" id="StoreChampionFixed" data-text="Fixed" class="btn btn-xs btn-info">Fixed</button>
        </div>

        <h4 class="sub-header"><i class="fa fa-user fa-fw"></i> Featured Store Champions</h4>

        <div class="loader" id="StoreChampTbcloader"></div>

        <div id="featuredChampsInteraction">
            <div  id="StoreChampTbcContainer"> </div>
        </div>

    </div>

    <div class="main-content  npsTarget no-bg">
        <div class="btn-group group2 pull-right" role="group" id="storerank-group" aria-label="...">
            <!--<button type="button" id="StoreRankingInteraction" class="btn btn-xs btn-primary">Interaction</button>-->
            <button type="button" id="StoreRankingMobile" class="btn btn-xs btn-warning">Mobile</button>
            <button type="button" id="StoreRankingFixed" class="btn btn-xs btn-info">Fixed</button>
        </div>

        <h4 class="sub-header"><i class="fa fa-dashboard fa-fw"></i> Store Ranking</h4>

        <div class="loader" id="StoreRankingGoalsLoader"></div>

        <div id="TargetFade1">
            <div id="StoreRankingGoalsContainer" ></div>
        </div>
    </div>

    <div class="main-content">
        <h4 class="sub-header"><i class="fa fa-building fa-fw"></i> Store Ranking Summary</h4>

        <div class="loader" id="StoreRankingsLoader"></div>

        <div id="storeRanking">
            <div id="StoreRankings"></div>
        </div>
    </div>

    <!--    <div class="main-content">
            <h4 class="sub-header"><i class="fa fa-users fa-fw"></i> Interaction Champions Summary</h4>

            <div class="loader" id="InteractionChampLoader"></div>

            <div id="InteractionChampionRanking"> </div>
        </div>-->

    <div class="main-content">
        <h4 class="sub-header"><i class="fa fa-users fa-fw"></i> Mobile Episode Champions Summary</h4>

        <div class="loader" id="MobileChampLoader"></div>

        <div id="MobileChampionRanking"> </div>
    </div>

    <div class="main-content">
        <h4 class="sub-header"><i class="fa fa-users fa-fw"></i> Fixed Episode Champions Summary</h4>

        <div class="loader" id="FixedChampLoader"></div>

        <div id="FixedChampionRanking"> </div>
    </div>

    <?php if ($role == '4') { ?>
        <!--        <div class="main-content">
                    <h4 class="sub-header"><i class="fa fa-users fa-fw"></i>All Staff Interaction Ranking</h4>

                    <div class="loader" id="AllEmployeeInteractionLoader"></div>

                    <div id="AllInteraction">
                        <div id="AllEmployeeInteraction"> </div>
                    </div>
                </div>-->

        <div class="main-content">
            <h4 class="sub-header"><i class="fa fa-users fa-fw"></i>All Staff Episode Ranking</h4>

            <div class="loader" id="AllEmployeeEpisodeLoader"></div>

            <div id="AllEpisode">
                <div id="AllEmployeeEpisode"> </div>
            </div>
        </div>
    <?php } ?>
</section>

<script src="../includes/js/custom.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function() {

        $('#StoreChampTbcContainer', function() {
            var urlToLoad = "dashboard_sso/summaryTBC/FeaturedChampsMobile.php?";
            var divToHide = "#StoreChampTbcloader";
            var divToShow = "#StoreChampTbcContainer";
            reload(urlToLoad, divToHide, divToShow);
        }
        );

        $('#storechamp-group button').click(function() {
            var textval = $(this).data("text");
            var divToHide = "#StoreChampTbcloader";
            var divToShow = "#StoreChampTbcContainer";
            var setUrl = '';

            switch (textval) {
                case 'Interaction':
                    setUrl = "dashboard_sso/summaryTBC/FeaturedChampsInteraction.php?";
                    break;
                case 'Mobile':
                    setUrl = "dashboard_sso/summaryTBC/FeaturedChampsMobile.php?";
                    break;
                case 'Fixed':
                    setUrl = "dashboard_sso/summaryTBC/FeaturedChampsFixed.php?";
                    break;
            }

            reload(setUrl, divToHide, divToShow);
        });

        //
        $('#StoreInteractionGoals', function() {
            var urlToLoad = "dashboard_sso/summaryTBC/storeInteractionGoals.php?";
            var divToHide = "#StoreRankingGoalsLoader";
            var divToShow = "#StoreRankingGoalsContainer";
            reload(urlToLoad, divToHide, divToShow);
        }
        );
//
        $('#StoreRankings', function() {
            var urlToLoad = "dashboard_sso/summaryTBC/storeRankings.php?";
            var divToHide = "#StoreRankingsLoader";
            var divToShow = "#StoreRankings";
            reload(urlToLoad, divToHide, divToShow);
        }
        );

//        $('#InteractionChampionRanking', function() {
//            var urlToLoad = "dashboard_sso/summaryTBC/ChampionsInteractionRank.php?";
//            var divToHide = "#InteractionChampLoader";
//            var divToShow = "#InteractionChampionRanking";
//            reload(urlToLoad, divToHide, divToShow);
//        }
//        );

        $('#MobileChampionRanking', function() {
            var urlToLoad = "dashboard_sso/summaryTBC/ChampionsMobileRank.php?";
            var divToHide = "#MobileChampLoader";
            var divToShow = "#MobileChampionRanking";
            reload(urlToLoad, divToHide, divToShow);
        }
        );

        $('#FixedChampionRanking', function() {
            var urlToLoad = "dashboard_sso/summaryTBC/ChampionsFixedRank.php?";
            var divToHide = "#FixedChampLoader";
            var divToShow = "#FixedChampionRanking";
            reload(urlToLoad, divToHide, divToShow);
        }
        );

<?php if ($role == '4') { ?>

    //            $('#AllEmployeeInteraction', function() {
    //                var urlToLoad = "nps_sso/summaryTBC/employeeNps.php?";
    //                var divToHide = "#AllEmployeeInteractionLoader";
    //                var divToShow = "#AllEmployeeInteraction";
    //                reload(urlToLoad, divToHide, divToShow);
    //            }
    //            );

            $('#AllEmployeeEpisode', function() {
                var urlToLoad = "nps_sso/summaryTBC/employeeNpsEpisode.php?";
                var divToHide = "#AllEmployeeEpisodeLoader";
                var divToShow = "#AllEmployeeEpisode";
                reload(urlToLoad, divToHide, divToShow);
            }
            );
<?php } ?>

        // featuredChampsInteraction  featuredChampsMobile featuredChampsFixed


        $('#StoreRankingInteraction').click(function() {
            //alert('clicked');
            $('#FixedDiv').fadeOut('slow');
            $('#MobileDiv').fadeOut('slow');
            $('#InteractionDiv').fadeIn('slow');
        });
        $('#StoreRankingMobile').click(function() {
            // alert('clicked 1 ');
            $('#InteractionDiv').fadeOut('slow');
            $('#FixedDiv').fadeOut('slow');
            $('#MobileDiv').fadeIn('slow');
        });
        $('#StoreRankingFixed').click(function() {
            //alert('clicked 2');
            $('#MobileDiv').fadeOut('slow');
            $('#InteractionDiv').fadeOut('slow');
            $('#FixedDiv').fadeIn('slow');
        });

    });

    function reload(paramurl, loader, content) {
        var urlExtend = 'storeType=2';
        var urldata = paramurl + urlExtend;
        $(content).html('');
        $(loader).show();
        $.ajax({
            url: urldata,
            cache: false,
            success: function(html) {
                $(content).html('');
                $(content).append(html);
            },
            complete: function() {
                $(loader).hide();
            }
        });
    }
</script>
