<?php
include('nps_sso/function/NPSFunction.php');
require($_SERVER['DOCUMENT_ROOT'] . '/admin/function/SubFunction.php');

$SelectStoreTypes = SelectStoreTypes();

$storeTypes = checkStoreTypesLocal();

$date = new DateTime();
$date->setTimezone(new DateTimeZone('Australia/Melbourne')); // +04
?>

<!--Local dashboard container-->
<section class='dashboard'>
    <div class="row">
        <div class="col-lg-12">

            <h4 class="page-header">
                <i class="fa fa-file-word-o fa-fw"></i>Competitive Local Dashboard
            </h4>
        </div>


        <div  id="page-contents" class="page-contents" ></div>
</section>


<script type="text/javascript">
    $(document).ready(function() {


        var storeType = "";
        var url = "dashboard_sso/dashboardextendLocalOthers.php?storeType=others";

        $('#page-contents').load(url).fadeIn("slow");

        $('#StoreType').change(function() {
            var url = setLoad();
            $('#page-contents').html('').load(url).fadeIn("slow");
        });
    });
</script>








